#include <sys/io.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Exit code:
 * 0: close
 * 1: open
 */
int main()
{
  unsigned char ret;
  iopl(3);

  outb(0x00,0x48f);
  outb(0x87,0x2e);
  outb(0x87,0x2e);
  outb(0x07,0x2e);
  outb(0x07,0x2f);
  outb(0xf1,0x2e);

  ret = inb(0x2f);
  
  outb(0x08,0x48f);
  char status = (ret & 0x10)>>4;
  //printf("ret = %d, %d", ((ret & 0x10)>>4), ret );
  return (int)status;
}
