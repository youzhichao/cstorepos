#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <sys/io.h>

/*
Access J34 GPIO port
There are two PNP I/O port addresses that can be used to configure GPIO ports,

1) 0x2E - EFER (Extended Function Enable Register, for entering Extended Function Mode)
  		- EFIR (Extended Function Index Register, for identifying CR index number)
2) 0x2F - EFDR (Extended Function Data Register, for accessing desired CR)
*/

void openDrawer()
{
	unsigned char ret;

    if (iopl(3) < 0)
    {
        perror("iopl()");
        fprintf(stderr, "This program must be run as root\n");
        return;
    }

	outb(0x00,0x48f);

    // Enter Extended Function Mode
    outb(0x87, 0x2e);
    outb(0x87, 0x2e);

    // Select Logic Device 7
    outb(0x07, 0x2e);
    outb(0x07, 0x2f);

    outb(0xf1, 0x2e);
    ret = inb(0x2f);
    ret = (ret | 1);
    outb(ret, 0x2f);

    usleep(250000);

	outb(0xf1, 0x2e);
	ret = inb(0x2f);
	ret = (ret & 0xfe);
	outb(ret, 0x2f);

	outb(0x08, 0x48f);
}

/*
JNIEXPORT jchar JNICALL Java_hyi_jpos_services_FirichCashDrawerLocal_openCashDrawer(JNIEnv* env, jclass cl, jint no)
{
    if (iopl(3) < 0)
    {
        perror("iopl()");
        fprintf(stderr, "This program must be run as root\n");
        return(EXIT_FAILURE);
    }
    openDrawer();
}

JNIEXPORT jint JNICALL Java_hyi_jpos_services_FirichCashDrawerLocal_getDrawerState(JNIEnv* env, jclass cl, jint no)
{
    int bb,b;

    if (iopl(3) < 0)
    {
        perror("iopl()");
        fprintf(stderr, "This program must be run as root\n");
        return(EXIT_FAILURE);
    }
    sleep(1);
    bb = inb(IO_PORT);
    return bb;
}
*/

main(int argc, char* argv[])
{
	openDrawer();
}
