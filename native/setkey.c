// setkey.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/io.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <unistd.h>

/* KEYBOARD INTERFACE(8042) CONTROL PORT */
#define PORT_A		0x60        // 8042 KB Data Register
#define STATUS_PORT	0x64        // 8042 KB status Register

#define INPT_BUF_FULL	0x02    // 8042 Status 1:KB BUFFER FULL

int keymap[][3] = {
 {159    , 1    , 112},   // shift+F1  //
 {143    , 1    , 113},   // shift+F2  //
 {142    , 1    , 114},   // shift+F3  //
 {158    , 1    , 115},   // shift+F4  //
 {157    , 1    , 116},   // shift+F5  //
 {141    , 1    , 117},   // shift+F6  //
 {156    , 1    , 118},   // shift+F7  //
 {140    , 1    , 119},   // shift+F8  //
 {139    , 1    , 120},   // shift+F9  //
 {155    , 1    , 121},   // shift+F10 //
 {138    , 1    , 122},   // shift+F11 //
 {154    , 1    , 123},   // shift+F12 //
 {152    , 4    ,  38},   // alt+k     //
 { 80    , 4    ,  39},   // alt+l     //

 {175    , 0    , 112},   // F1        //
 {191    , 0    , 113},   // F2        //
 {190    , 0    , 114},   // F3        //
 {174    , 0    , 115},   // F4        //
 {173    , 0    , 116},   // F5        //
 {188    , 0    , 117},   // F6        //
 {172    , 0    , 118},   // F7        //
 {171    , 0    , 119},   // F8        //
 {170    , 0    , 120},   // F9        //
 {153    , 0    , 121},   // F10       //
 {137    , 0    , 122},   // F11       //
 {168    , 0    , 123},   // F12       //
 {160    , 1    ,  12},   // _         //
 {114    , 4    ,  18},   // alt-w    //

 {111    , 1    ,   2},   // !         //
 {110    , 1    ,   3},   // @         //
 {109    , 1    ,   4},   // #         //
 {189    , 1    ,   5},   // $         //
 {108    , 4    ,  47},   // alt-x     //
 {107    , 1    ,   7},   // ^         //
 {187    , 1    ,   8},   // &         //
 {186    , 1    ,   9},   // *         //
 {169    , 1    ,  10},   // (         //
 {185    , 1    ,  11},   // )         //
 {184    , 4    ,  52},   // alt-m     //
 {176    , 4    ,  51},   // alt-n     //
 {129    , 4    ,  25},   // alt-o     //
 {115    , 4    ,  26},   // alt-p     //

 { 95    , 4    ,  31},   // alt-a     //
 { 94    , 4    ,  50},   // alt-b     //
 { 93    , 4    ,  48},   // alt-c     //
 { 92    , 4    ,  33},   // alt-d     //
 { 91    , 4    ,  19},   // alt-e     //
 { 90    , 4    ,  34},   // alt-f     //
 {106    , 4    ,  35},   // alt-g     //
 {105    , 4    ,  36},   // alt-h     //
 {104    , 4    ,  24},   // alt-i     //
 { 88    , 4    ,  37},   // alt-j     //
 { 96    , 0    ,  12},   // -         //
 {177    , 0    ,  13},   // =         //
 {130    , 1    ,  13},   // +         //
 {131    , 1    ,  29},   // |         //

 { 79    , 0    ,  17},   // q         //
 { 78    , 0    ,  18},   // w         //
 { 77    , 0    ,  19},   // e         //
 { 76    , 0    ,  20},   // r         //
 { 75    , 0    ,  21},   // t         //
 { 74    , 0    ,  22},   // y         //
 { 89    , 0    ,  23},   // u         //
 { 72    , 0    ,  24},   // i         //
 { 64    , 0    ,  25},   // o         //
 { 65    , 0    ,  26},   // p         //
 {178    , 1    ,  17},   // Shift-Q   //
 {162    , 1    ,  23},   // Shift-U   //
 {163    , 1    ,  49},   // Shift-V   //
 {116    , 1    ,  18},   // Shift-W   //

 { 63    , 0    ,  31},   // a         //
 { 62    , 0    ,  32},   // s         //
 { 61    , 0    ,  33},   // d         //
 { 60    , 0    ,  34},   // f         //
 { 59    , 0    ,  35},   // g         //
 { 58    , 0    ,  36},   // h         //
 { 73    , 0    ,  37},   // j         //
 { 56    , 0    ,  38},   // k         //
 { 48    , 0    ,  39},   // l         //
 { 49    , 1    ,  40},   // :         //
 { 97    , 4    ,  17},   // alt-q     //
 {146    , 1    ,  41},   // "         //
 {165    , 0    ,  41},   // '         //
 {147    , 16   ,   2},   // Ctrl-1    //

 { 47    , 0    ,  46},   // z         //
 { 46    , 0    ,  47},   // x         //
 { 45    , 0    ,  48},   // c         //
 { 44    , 0    ,  49},   // v         //
 { 43    , 0    ,  50},   // b         //
 { 42    , 0    ,  51},   // n         //
 { 57    , 0    ,  52},   // m         //
 { 41    , 1    ,  53},   // <         //
 { 40    , 1    ,  54},   // >         //
 { 32    , 0    ,  53},   // ,         //
 { 33    , 1    ,  55},   // ?         //
 {161    , 0    ,  55},   // /         //
 {179    , 4    ,  46},   // Alt-z      //
 {145    , 4    ,  20},   // alt-r     //

 { 98    , 0    , 110},   // Esc       //
 { 83    , 1    ,  31},   // shift-a   //
 { 67    , 1    ,  50},   // shift-b   //
 { 66    , 1    ,  48},   // shift-c   //
 { 68    , 1    ,  33},   // shift-d   //
 { 69    , 1    ,  19},   // shift-e   //
 { 70    , 1    ,  34},   // shift-f   //
 { 71    , 0    ,  85},   // PageUp    //

 { 99    , 1    ,  35},   // shift-g    //
 {136    , 1    ,  36},   // shift-h    //
 {128    , 1    ,  15},   // Backspace  //
 {100    , 1    ,  37},   // shift-j    //
 { 84    , 1    ,  38},   // shift-k    //
 { 85    , 1    ,  39},   // shift-l    //
 { 86    , 1    ,  52},   // shift-m    //
 { 87    , 0    ,  86},   // PageDown   //

 {117    , 1    ,  51},   // shift-n    //
 {123    , 0    ,   8},   // 7          //
 {122    , 0    ,   9},   // 8          //
 {113    , 0    ,  10},   // 9          //
 {180    , 1    ,  25},   // shift-o    //
 {101    , 1    ,  26},   // shift-p    //
 {102    , 0    ,  83},   // arrow up   //
 {103    , 1    ,  20},   // shift-r    //

 {133    , 1    ,  32},   // shift-s   //
 {124    , 0    ,   5},   // 4         //
 {127    , 0    ,   6},   // 5         //
 {112    , 0    ,   7},   // 6         //
 {148    , 1    ,  21},   // shift-t   //
 {181    , 0    ,  79},   // arrow left   //
 {182    , 0    ,  84},   // arrow down//
 {183    , 0    ,  89},   // arrow right  //

 {132    ,16    ,  3},   // Ctrl-2    //
 {121    , 0    ,   2},   // 1         //
 {126    , 0    ,   3},   // 2         //
 {120    , 0    ,   4},   // 3         //
 { 36    , 1    ,  47},   // shift-x   //
 {149    , 1    ,  22},   // shift-y   //
 {150    , 1    ,  46},   // shift-z   //
 {151    , 4    ,  32},   // alt-s     //

 {144    , 4    ,  22},   // Alt-Y //
 {125    , 1    ,  36},   // Shift-h   //
 {119    , 4    ,  21},   // alt-t     //
 {118    , 0    ,  54},   // .         //
 {166    , 4    ,  23},   // alt-u     //
 {164    , 4    ,  49},   // alt-v     //
 { 34    , 4    ,  18},   // alt-w     //
 {167    , 0    , 108}    // Enter     //
};

int snd_data(unsigned char dt)
{
    int ix,iy;
    int ly;
	int	ret;

	for(ix = 0; ix < 3000; ix++)
		outb(0, 0xED);

	ly = 100000L;			// #2
	while (--ly) {
		ret = inb(STATUS_PORT) & INPT_BUF_FULL;
		if (!ret)
			break;
		outb(0, 0xED);		// #3
		outb(0, 0xED);		// #3
		outb(0, 0xED);		// #3
		outb(0, 0xED);		// #3
	}
	if (!ly) {
	    printf("inp failed");
	    return -2;
	}

    iy = 30000;
    while ((inb_p(STATUS_PORT) & INPT_BUF_FULL) && --iy) {
        ;
    }
    if (!iy)
        return -2;
    
    outb_p(dt, PORT_A);

    return 0;
}

int main(int argc, char* argv[])
{
    int i;
    long int c;
    int numkey;

    if (iopl(3) < 0) {
        perror("iopl()");
        fprintf( stderr, "This program must be run as root\n" );
        return;
    }              

    if (argc > 1) {
        for (i = 1; i < argc; i++) {
            c = strtol(argv[i], (char **)NULL, 10);
    	    snd_data((unsigned char)c);
        }
    } else {
        numkey = sizeof(keymap) / (sizeof(int) * 3);
        for (i = 0; i < numkey; i++) {
            printf("%d %d %d\n", keymap[i][0], keymap[i][1], keymap[i][2]);
            snd_data((unsigned char)0xDB);              // set key code command
            snd_data((unsigned char)keymap[i][0]);      // key position
            snd_data((unsigned char)keymap[i][1]);      // key modifier
            snd_data((unsigned char)keymap[i][2]);      // key code

            for (c = 0; c < 999999; c++)
                ;  // delay
        }
    }

    return 0;
}
