This is the workspace for developing CStore's POS on new POS machine model.

Because we lost the source code of CStore POS, what we can do is decompiling
the trouble part and update them into original cream.jar.

Directories:

src/
		source code
classes/
		all class files ready for decompiling
bin/
		a few new class files generating by Ecilpse
conf/
		conf files
doc/
		docs

decompile.sh
		script for decompiling class file

build.xml
		Ant build file for updating cream.jar
