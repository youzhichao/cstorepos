// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   Conversion.java

import java.io.*;
import java.util.Properties;

public class Conversion
{

    public Conversion()
    {
    }

    public static void main(String args[])
    {
        try
        {
            if (args.length > 0)
            {
                Properties pro = new Properties();
                File propFile = new File(System.getProperty("user.home") + File.separator
                    + "cream.conf");
                if (propFile.exists())
                    pro.load(new FileInputStream(propFile));
                pro.setProperty("StoreName", args[0]);
                if (args.length > 1)
                    pro.setProperty("StoreNumber", args[1]);
                if (args.length > 2)
                {
                    pro.setProperty("TerminalNumber", args[2]);
                    int ip = Integer.parseInt(args[2]);
                    pro.setProperty("DatabaseURL", "jdbc:mysql://192.168.1." + ip + "/cream");
                    Properties pro2 = new Properties();
                    File propFile2 = new File("/etc/sysconfig/network-scripts/ifcfg-eth0");
                    if (propFile2.exists())
                        pro2.load(new FileInputStream(propFile2));
                    pro2.setProperty("IPADDR", "192.168.1." + ip);
                    pro2.store(new FileOutputStream(propFile2), "IP Configuration");
                }
                pro.store(new FileOutputStream(propFile), "Cream Configuration");
            }
        }
        catch (IOException e)
        {
            System.err.println(e);
            e.printStackTrace();
        }
    }
}
