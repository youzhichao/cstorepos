// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   Convert.java

import java.io.*;
import java.sql.*;
import java.util.*;

public class Convert
{
    private static Connection oCon;
    private static Statement stmt;

    public Convert()
    {
    }

    private void initSQL()
    {
        try
        {
            Class.forName("org.gjt.mm.mysql.Driver");
            oCon = DriverManager
                .getConnection(
                    "jdbc:mysql://localhost/cream?useUnicode=true&characterEncoding=gb2312",
                    "root", "");
            stmt = oCon.createStatement();
            stmt.executeUpdate("DROP TABLE IF EXISTS property");
            stmt.executeUpdate("CREATE TABLE property ("
                + "name varchar(50) DEFAULT '' NOT NULL,"
                + "value varchar(50) DEFAULT '' NOT NULL, "
                + "PRIMARY KEY (name)) ENGINE=MyISAM DEFAULT CHARSET=utf8");

            stmt.executeUpdate("DROP TABLE IF EXISTS store");
            stmt.executeUpdate("CREATE TABLE store (" + "storeID varchar(6) NOT NULL default '',"
                + "storeChineseName varchar(10) default NULL,"
                + "storeEnglishName varchar(30) default NULL,"
                + "areaNumber varchar(5) default NULL,"
                + "telephoneNumber varchar(10) default NULL,"
                + "faxNumber varchar(10) default NULL, "
                + "lineNumber varchar(10) default NULL,"
                + "zip varchar(6) default NULL," 
                + "address varchar(60) default NULL,"
                + "dayBeginTime varchar(4) default NULL,"
                + "dayEndTime varchar(4) default NULL,"
                + "orderEndTime varchar(4) default NULL,"
                + "emailAddress varchar(40) default NULL,"
                + "ipAddress varchar(20) default NULL,"
                + "systemBeginDate date default NULL,"
                + "orderBeginDate date default NULL,"
                + "posBeginDate date default NULL,"
                + "storeOpenDate date default NULL,"
                + "storeEndDate date default NULL,"
                + "zoneID char(2) default NULL,"
                + "doID char(2) default NULL,"
                + "fmID char(2) default NULL,"
                + "PRIMARY KEY (storeID)"
                + ") ENGINE=MyISAM DEFAULT CHARSET=utf8");
        }
        catch (Exception exception)
        {
            System.out.println(exception.toString());
        }
    }

    private void writeTable(String s, String s1)
    {
        try
        {
            stmt.executeUpdate("INSERT INTO property VALUES ('" + s + "','" + s1 + "')");
        }
        catch (Exception exception)
        {
            System.out.println(exception.toString());
        }
    }

    public static void main(String args[])
    {
        Convert convert = new Convert();
        convert.initSQL();
        Properties properties = new Properties();
        try
        {
            File file = new File(System.getProperty("user.home") + File.separator + "cream.conf");
            properties.load(new FileInputStream(file));
        }
        catch (Exception exception)
        {
        }
        Set set = properties.keySet();
        String s;
        for (Iterator iterator = set.iterator(); iterator.hasNext(); convert.writeTable(s,
            properties.getProperty(s)))
        {
            s = (String)iterator.next();
            System.out.println(s + " ---- " + properties.getProperty(s));
        }
    }
}
