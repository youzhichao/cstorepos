/*jadclipse*/// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.

package com.hyi.tranrecall;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.*;
import java.util.*;

public class Util
{

    public Util()
    {
    }

    public static Connection getSCConnection()
    {
        Connection connection = null;
        try
        {
            Class.forName(SC_DRIVERS).newInstance();
            connection = DriverManager.getConnection(SC_URL, SC_USER, SC_PASSWORD);
        }
        catch(ClassNotFoundException classnotfoundexception)
        {
            System.out.println("ClassNotFoundException in getSCConnection, can not get connection!");
            classnotfoundexception.printStackTrace();
        }
        catch(InstantiationException instantiationexception)
        {
            System.out.println("InstantiationException in getSCConnection, can not get connection!");
            instantiationexception.printStackTrace();
        }
        catch(IllegalAccessException illegalaccessexception)
        {
            System.out.println("IllegalAccessException in getSCConnection, can not get connection!");
            illegalaccessexception.printStackTrace();
        }
        catch(SQLException sqlexception)
        {
            System.out.println("SQLException in getSCConnection, can not get connection!");
            sqlexception.printStackTrace();
        }
        return connection;
    }

    public static Connection getPOSConnection(String s)
    {
        Connection connection = null;
        String s1 = "";
        try
        {
            String s2 = "pos" + s + ".cstore.com.cn";
            s1 = InetAddress.getByName(s2).toString();
            s1 = s1.substring(s1.indexOf("/") + 1);
            String s3 = POS_URL + s1 + "/cream";
            Class.forName(POS_DRIVERS).newInstance();
            connection = DriverManager.getConnection(s3, POS_USER, POS_PASSWORD);
        }
        catch(UnknownHostException unknownhostexception)
        {
            System.out.println("UnknownHostException in getPOSConnection, unknow pos IP: " + s1 + " !");
            unknownhostexception.printStackTrace();
        }
        catch(ClassNotFoundException classnotfoundexception)
        {
            System.out.println("ClassNotFoundException in getPOSConnection, can not get connection!");
            classnotfoundexception.printStackTrace();
        }
        catch(InstantiationException instantiationexception)
        {
            System.out.println("InstantiationException in getPOSConnection, can not get connection!");
            instantiationexception.printStackTrace();
        }
        catch(IllegalAccessException illegalaccessexception)
        {
            System.out.println("IllegalAccessException in getPOSConnection, can not get connection!");
            illegalaccessexception.printStackTrace();
        }
        catch(SQLException sqlexception)
        {
            System.out.println("SQLException in getPOSConnection, can not get connection!");
            sqlexception.printStackTrace();
        }
        return connection;
    }

    public static HashMap loadConfFile(File file)
    {
        HashMap hashmap = new HashMap();
        Properties properties = new Properties();
        try
        {
            FileInputStream fileinputstream = new FileInputStream(file);
            properties.load(fileinputstream);
            Enumeration enumeration = properties.keys();
            int i = 0;
            String s;
            String s1;
            for(; enumeration.hasMoreElements(); hashmap.put(s, s1))
            {
                i++;
                s = (String)enumeration.nextElement();
                s1 = properties.getProperty(s);
            }

        }
        catch(FileNotFoundException filenotfoundexception)
        {
            filenotfoundexception.printStackTrace();
        }
        catch(IOException ioexception)
        {
            ioexception.printStackTrace();
        }
        return hashmap;
    }

    public static void main(String args[])
    {
        try
        {
            String s = "pos1.cstore.com.cn";
            String s1 = InetAddress.getByName(s).toString();
            s1 = s1.substring(s1.indexOf("/") + 1);
            System.out.println("@@posIP=" + s1);
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public static File logDir;
    public static String logDirStr;
    private static String SC_DRIVERS = "org.gjt.mm.mysql.Driver";
    private static String SC_URL = "jdbc:mysql://127.0.0.1/cake";
    private static String SC_USER = "root";
    private static String SC_PASSWORD = "";
    private static String POS_DRIVERS = "org.gjt.mm.mysql.Driver";
    private static String POS_URL = "jdbc:mysql://";
    private static String POS_USER = "root";
    private static String POS_PASSWORD = "";
    private static String headFilePath = "c:\\";
    private static String headFileName;
    private static String dtlFilePath = "c:\\";
    private static String dtlFileName;
    public static HashMap headFieldMap = new HashMap();
    public static HashMap dtlFieldMap = new HashMap();
    public static String headField[][] = {
        {
            "storeID", "STORENO"
        }, {
            "systemDate", "SYSDATE"
        }, {
            "posNumber", "POSNO"
        }, {
            "transactionNumber", "TMTRANSEQ"
        }, {
            "shiftCount", "SIGNONID"
        }, {
            "zSequenceNumber", "EODCNT"
        }, {
            "accountDate", "ACCDATE"
        }, {
            "transactionType", "TRANTYPE"
        }, {
            "dealType1", "DEALTYPE1"
        }, {
            "dealType2", "DEALTYPE2"
        }, {
            "dealType3", "DEALTYPE3"
        }, {
            "voidTransactionNumber", "VOIDSEQ"
        }, {
            "machineNumber", "TMCODEP"
        }, {
            "cashier", "CASHIER"
        }, {
            "invoiceHead", "INVNOHEAD"
        }, {
            "invoiceNumber", "INVNO"
        }, {
            "invoiceCount", "INVCNT"
        }, {
            "idNumber", "IDNO"
        }, {
            "detailCount", "DETAILCNT"
        }, {
            "customerCount", "CUSTCNT"
        }, {
            "inOurID", "`INOUT`"
        }, {
            "customerCategory", "CUSTID"
        }, {
            "saleMan", "SALEMAN"
        }, {
            "grossSaleTotalAmount", "GROSALAMT"
        }, {
            "grossSaleTax0Amount", "GROSALTX0AMT"
        }, {
            "grossSaleTax1Amount", "GROSALTX1AMT"
        }, {
            "grossSaleTax2Amount", "GROSALTX2AMT"
        }, {
            "grossSaleTax3Amount", "GROSALTX3AMT"
        }, {
            "grossSaleTax4Amount", "GROSALTX4AMT"
        }, {
            "siPlusTax1Amount", "SIPLUSAMT1"
        }, {
            "siPlusTax2Amount", "SIPLUSAMT2"
        }, {
            "siPlusTax3Amount", "SIPLUSAMT3"
        }, {
            "siPlusTax4Amount", "SIPLUSAMT4"
        }, {
            "discountTax0Amount", "SIPAMT0"
        }, {
            "discountTax1Amount", "SIPAMT1"
        }, {
            "discountTax2Amount", "SIPAMT2"
        }, {
            "discountTax3Amount", "SIPAMT3"
        }, {
            "discountTax4Amount", "SIPAMT4"
        }, {
            "mixAndMatchTax0Amount", "MNMAMT0"
        }, {
            "mixAndMatchTax1Amount", "MNMAMT1"
        }, {
            "mixAndMatchTax2Amount", "MNMAMT2"
        }, {
            "mixAndMatchTax3Amount", "MNMAMT3"
        }, {
            "mixAndMatchTax4Amount", "MNMAMT4"
        }, {
            "notIncludedTax0Sale", "RCPGIFAMT0"
        }, {
            "notIncludedTax1Sale", "RCPGIFAMT1"
        }, {
            "notIncludedTax2Sale", "RCPGIFAMT2"
        }, {
            "notIncludedTax3Sale", "RCPGIFAMT3"
        }, {
            "notIncludedTax4Sale", "RCPGIFAMT4"
        }, {
            "netSaleTax0Amount", "NETSALAMT0"
        }, {
            "netSaleTax1Amount", "NETSALAMT1"
        }, {
            "netSaleTax2Amount", "NETSALAMT2"
        }, {
            "netSaleTax3Amount", "NETSALAMT3"
        }, {
            "netSaleTax4Amount", "NETSALAMT4"
        }, {
            "daiShouAmount", "DAISHOUAMT"
        }, {
            "daiShouAmount2", "DAISHOUAMT2"
        }, {
            "daiFuAmount", "DAIFUAMT"
        }, {
            "payID1", "PAYNO1"
        }, {
            "payID2", "PAYNO2"
        }, {
            "payID3", "PAYNO3"
        }, {
            "payID4", "PAYNO4"
        }, {
            "payAmount1", "PAYAMT1"
        }, {
            "payAmount2", "PAYAMT2"
        }, {
            "payAmount3", "PAYAMT3"
        }, {
            "payAmount4", "PAYAMT4"
        }, {
            "tax0Amount", "TAXAMT0"
        }, {
            "tax1Amount", "TAXAMT1"
        }, {
            "tax2Amount", "TAXAMT2"
        }, {
            "tax3Amount", "TAXAMT3"
        }, {
            "tax4Amount", "TAXAMT4"
        }, {
            "changeAmount", "CHANGEAMT"
        }, {
            "spillAmount", "OVERAMT"
        }, {
            "exchangeDifference", "CHANGAMT"
        }, {
            "creditCardType", "CRDTYPE"
        }, {
            "creditCardNumber", "CRDNO"
        }, {
            "creditCardExpireDate", "CRDEND"
        }, {
            "employeeNumber", "EMPNO"
        }, {
            "memberID", "MEMBERID"
        }, {
            "annotatedID", "ANNOTATEDID"
        }, {
            "annotatedType", "ANNOTATEDTYPE"
        }, {
            "rebateChangeAmount", "rebateChangeAmount"
        }, {
            "rebateAmount", "rebateAmount"
        }, {
            "totalRebateAmount", "totalRebateAmount"
        }, {//gllg
            "itemDiscAmount0", "ITEMDISCAMT0"
        }, {
            "itemDiscAmount1", "ITEMDISCAMT1"
        }, {
            "itemDiscAmount2", "ITEMDISCAMT2"
        }, {
            "itemDiscAmount3", "ITEMDISCAMT3"
        }, {
            "itemDiscAmount4", "ITEMDISCAMT4"
        }
    };
    public static String dtlField[][] = {
        {
            "posNumber", "TMCODE"
        }, {
            "transactionNumber", "TMTRANSEQ"
        }, {
            "lineItemSequence", "ITEMSEQ"
        }, {
            "detailCode", "CODETX"
        }, {
            "itemVoid", "ITEMVOID"
        }, {
            "categoryNumber", "CATNO"
        }, {
            "midCategoryNumber", "MIDCATNO"
        }, {
            "microCategoryNumber", "MICROCATNO"
        }, {
            "thinCategoryNumber", "THINCATNO"
        }, {
            "pluNumber", "PLUNO"
        }, {
            "unitPrice", "UNITPRICE"
        }, {
            "quantity", "QTY"
        }, {
            "weight", "WEIGHT"
        }, {
            "discountID", "DISCNO"
        }, {
            "amount", "AMT"
        }, {
            "taxID", "TAXTYPE"
        }, {
            "taxAmount", "TAXAMT"
        }, {
            "itemNumber", "ITEMNO"
        }, {
            "originalPrice", "ORIGPRICE"
        }, {
            "discountType", "DISCTYPE"
        }, {
            "afterDiscountAmount", "AFTDSCAMT"
        }, {
            "discountRateID", "discountRateID"
        }, {
            "discountRate", "discountRate"
        }, {
            "rebateAmount", "rebateAmount"
        }
    };

    static 
    {
        headFileName = "tranhead.conf";
        dtlFileName = "trandtl.conf";
        File file = new File(".");
        File file1 = new File(".");
        file = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().length() - 1) + headFileName);
        file1 = new File(file1.getAbsolutePath().substring(0, file1.getAbsolutePath().length() - 1) + dtlFileName);
        headFieldMap = loadConfFile(file);
        dtlFieldMap = loadConfFile(file1);
    }
}


/*
	DECOMPILATION REPORT

	Decompiled from: D:\workspace\CStorePOS\lib\TranRecall.jar
	Total time: 70 ms
	Jad reported messages/errors:
	Exit status: 0
	Caught exceptions:
*/