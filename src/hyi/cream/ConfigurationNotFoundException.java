package hyi.cream;

public class ConfigurationNotFoundException extends Exception{

    public ConfigurationNotFoundException() {
        super();
    }

    public ConfigurationNotFoundException(String s) {
        super(s);
    }
}

 