package hyi.cream;

public class LineItemNotFoundException extends Exception{

    public LineItemNotFoundException() {
        super();
    }

    public LineItemNotFoundException(String s) {
        super(s);
    }
}

 