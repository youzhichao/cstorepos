package hyi.cream;

public class NoSuchPOSDeviceException extends Exception{

    public NoSuchPOSDeviceException() {
        super();
    }

    public NoSuchPOSDeviceException(String s) {
        super(s);
    }
}

 