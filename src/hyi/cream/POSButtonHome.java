
package hyi.cream;

import hyi.cream.uibeans.*;
import hyi.cream.event.*;
import hyi.cream.util.*;
import hyi.cream.state.*;
import hyi.cream.*;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.lang.*;
import java.awt.*;
import java.awt.event.*;
import java.math.*;

public class POSButtonHome extends Canvas implements POSButtonListener {
    private int row                          = 0;
	private int col                          = 0;
	private final int minimal                = 6;
	private final int maximal                = 11;
	private final String defaultImage        = "screenbutton0.jpg";
	private final String defaultRGB          = "255";
    private String className                 = "";
    private String buttonMode                = "";
	private String label                     = "";
    private String imageFile                 = "";
    private String parameters1               = "";
    private String parameters2               = "";
    private ArrayList configArrayList        = new ArrayList();
    private ArrayList buttonArrayList        = new ArrayList();
    private ArrayList listener               = new ArrayList();
    private POSButton pb                     = null;
    private boolean enabled                  = false;
    static POSButtonHome posButtonHome       = null;
    private int pageUpCode                   = 0;
    private int pageDownCode                 = 0;
    private int firstPageCode                = 0;
    private int lastPageCode                 = 0;
    private int clearCode                    = 0;
    private int enterCode                    = 0;
    private ArrayList numberCodeArray        = new ArrayList(); 
    private ArrayList numberButtonArray      = new ArrayList();
    private ArrayList spcButtonArray         = new ArrayList();

	public static POSButtonHome getInstance() {
        try {
			if (posButtonHome == null) {
                posButtonHome = new POSButtonHome();
            }
        } catch (ConfigurationNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Configuration file " + POSButtonHome.class + " is not found");
        } catch (InstantiationException ex) {
            CreamToolkit.logMessage(ex.toString());
            CreamToolkit.logMessage("Instantiation " + POSButtonHome.class);
        }
        return posButtonHome;
	}
	Class c         = null;

	public POSButtonHome() throws ConfigurationNotFoundException, InstantiationException {

        Class[] cla     = null;
		Object[] obj    = null;

        if (posButtonHome != null) {
            throw new InstantiationException();
        } else {
            posButtonHome = this;
        }

        //read POSButton configration file
        //construct all POSButton defined in the file
        File propFile = CreamToolkit.getConfigurationFile(POSButtonHome.class);
        try {
            FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = null;
            if (CreamProperties.getInstance().getProperty("Locale").equals("zh_TW")) {
                inst = new InputStreamReader(filein, "BIG5");
            } else if (CreamProperties.getInstance().getProperty("Locale").equals("zh_CN")) {
                inst = new InputStreamReader(filein, "GBK");
            }
            BufferedReader in = new BufferedReader(inst);
            String line;
            char ch = ' ';
            int m;
            
            do {
                do {
                    line = in.readLine();
                    if (line == null) {
                        return;
                    }
                    while (line.equals("")) {
                        line = in.readLine();
                    }
                    m = 0;
                    do {
                        ch = line.charAt(m);
                        m++;
                    } while ((ch == ' ' || ch == '\t')&& m < line.length());
                } while (ch == '#' || ch == ' ' || ch == '\t');

				StringTokenizer t = new StringTokenizer(line, ",", true);

				ArrayList strings = new ArrayList();

				while (t.hasMoreElements()) {
					String next = t.nextToken().trim();
					if (!next.equals(",")) {
						strings.add(next);
					} else {
                        if (t.hasMoreElements()) {
                            next = t.nextToken().trim();
                            while (next.equals(",")) {
                                strings.add("");
                                next = t.nextToken().trim();
                            }
                            strings.add(next);
                        }
					}
				}
				
				String type = (String)strings.get(5);

				if (strings.size() == minimal) {
					strings.add("");
					strings.add(defaultImage);
				} else if (strings.size() == minimal + 1) {
					strings.add(defaultImage);
				} else if (strings.size() == minimal + 2) {
					String bitmap = (String)strings.get(minimal + 1);
					if (bitmap.trim().equals("")) {
					   strings.set(minimal + 1, defaultImage);
					}
				} else if (strings.size() > minimal + 2 && !type.equalsIgnoreCase("k")) {
					String bitmap = (String)strings.get(minimal + 1);
					int base = 2;
					int differ = maximal - strings.size();
					if (differ > 0)
						for (int i = 0; i < differ; i++)
	    					strings.add("");

					String r = (String)strings.get(minimal + base);
					String g = (String)strings.get(minimal + base + 1);
					String b = (String)strings.get(minimal + base + 2);
					if (bitmap.trim().equals("")) {				
						if (!r.trim().equals("") || !g.trim().equals("") || !b.trim().equals("")) {
							if (r.trim().equals(""))
								strings.set(minimal + base, defaultRGB);
							if (g.trim().equals(""))
								strings.set(minimal + base + 1, defaultRGB);
							if (b.trim().equals(""))
								strings.set(minimal + base + 2, defaultRGB);
						} else
							strings.set(minimal + 1, defaultImage);
					} else {//set color information to empty string;
						strings.set(minimal + base, "");
						strings.set(minimal + base + 1, "");
						strings.set(minimal + base + 2, "");
					}
				}
				int differ = maximal - strings.size();
				for (int i = 0; i < differ; i++)
					strings.add(" ");

				c = Class.forName("hyi.cream.uibeans." + (String)strings.get(4));
				int len = strings.size();
				cla = new Class[len - 7];
				
				//1236
				ArrayList classParas = new ArrayList();

				for (int i = 0; i < strings.size(); i++) {
					 if (i == 1 || i == 2 || i == 3 || i == 6 || i > 10)
						 classParas.add(strings.get(i));
				}

				obj = classParas.toArray();
				for (int i = 0; i < classParas.size(); i++) {
					if (i < 3) {
						cla[i] = int.class;
						obj[i] = new Integer((String)obj[i]);
					} else if (i < 4) {
						cla[i] = String.class;
					} else {
						if (((String)strings.get(5)).equalsIgnoreCase("k") && i == 4)  {
							cla[i] = int.class;
                        	obj[i] = new Integer((String)obj[i]);
						} else {
						    cla[i] = String.class;
						}
					}
				}

                Constructor constructor = c.getConstructor(cla);
                Object retObject = constructor.newInstance(obj);
                pb = (POSButton)retObject;

				if (((String)strings.get(5)).equalsIgnoreCase("s")) {
				    int base = 2;
					String r = (String)strings.get(minimal + base);
					String g = (String)strings.get(minimal + base + 1);
					String b = (String)strings.get(minimal + base + 2);
					pb.becomeScreenButton((String)strings.get(7), (String)strings.get(0), r,g,b);
					pb.addPOSButtonListener(this);
				} else if (((String)strings.get(5)).equalsIgnoreCase("k")) {
					pb.becomeKeyboardButton();
					pb.addPOSButtonListener(this);
                    if (pb.getKeyCode() > 999) {
                        spcButtonArray.add(pb);
                    }
                }                         

                if (((String)strings.get(4)).equals("PageUpButton")) {
                    pageUpCode = Integer.parseInt((String)strings.get(11));
                } else if (((String)strings.get(4)).equals("PageDownButton")) {
                    pageDownCode = Integer.parseInt((String)strings.get(11));
                } else if (((String)strings.get(4)).equals("NumberButton")) {
                    numberCodeArray.add((String)strings.get(11));      
                    numberButtonArray.add(pb);
                } else if (((String)strings.get(4)).equals("ClearButton")) {
                    clearCode = Integer.parseInt((String)strings.get(11));
                } else if (((String)strings.get(4)).equals("EnterButton")) {
                    enterCode = Integer.parseInt((String)strings.get(11));
                } else if (((String)strings.get(4)).equals("FirstPageButton")) {
                    firstPageCode = Integer.parseInt((String)strings.get(11));
                } else if (((String)strings.get(4)).equals("LastPageButton")) {
                    lastPageCode = Integer.parseInt((String)strings.get(11));
                }
            } while (true);
        } catch (FileNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("File " + this + " is not found");
		} catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("IO Error at " + this);
		} catch (ClassNotFoundException e) {
			e.printStackTrace(CreamToolkit.getLogger());
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("Class " + c + " is not found");
        } catch (IllegalAccessException e) {
			CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("IllegalAccess at " + this);
        } catch (InstantiationException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Instantiation " + this);
		} catch (NoSuchMethodException e) {
			e.printStackTrace(CreamToolkit.getLogger());
			CreamToolkit.logMessage(c + " ! " + e.toString());
			CreamToolkit.logMessage("No such method at " + this);
        } catch (InvocationTargetException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Invocation target at " + this);
		}
	}

    public void setEventForwardEnabled(boolean enabled) {
        this.enabled = enabled;
    }   

    public synchronized void buttonPressed(POSButtonEvent e) {
        if (enabled == true) {
            StateMachine stateMachine = StateMachine.getInstance();
            //System.out.println("fire event from posbuttonhome");
			stateMachine.processEvent(e);
        }
    }

    public int getPageUpCode() {
        return pageUpCode;
    }

    public int getPageDownCode() {
        return pageDownCode;
    }

    public int getClearCode() {
        return clearCode;
    }

    public int getEnterCode() {
        return enterCode;
    }

    public int getFirstPageCode() {
        return firstPageCode;
    }

    public void setFirstPageCode(int firstPageCode) {
        this.firstPageCode = firstPageCode;
    }

    public int getLastPageCode() {
        return lastPageCode;
    }

    public void setLastPageCode(int lastPageCode) {
        this.lastPageCode = lastPageCode;
    }

    public void setEnterCode(int code) {
        enterCode = code;
    }

    public ArrayList getNumberCode() {
        return numberCodeArray;
    }

    public ArrayList getNumberButton() {
        return numberButtonArray;
    }

    public ArrayList getSpcButton() {
        return spcButtonArray;
    }
}
