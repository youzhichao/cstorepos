package hyi.cream;

import java.util.*;
import java.io.*;
import java.lang.reflect.*;

import jpos.*;
import jpos.events.*;

import hyi.cream.state.*;
import hyi.cream.util.*;

/*******************************************************************************
 * POSPeripheralHome class
 * NOTE: Container class to implement events listener and initialize device
 *       control instances.
 * @since 2000
 * @author slackware
 *******************************************************************************/

public class POSPeripheralHome extends Object implements DataListener, DirectIOListener,
                              ErrorListener,OutputCompleteListener,StatusUpdateListener{

    private static POSPeripheralHome instance = null;
    private static Hashtable HdeviceControlInstances;
    //Private fields which store the device control instance in a Hashtable
    private boolean eventForwardEnabled = false; //Flag to enable the event forwarding of
                                                 //POSPeripheralHome
    /** Default ctor */
    public POSPeripheralHome() throws JposException, ConfigurationNotFoundException, InstantiationException {
    //Call createOpenHash() to execute device opening manipulation and return a Hashtable which
    //contains all the device control instances. then assign it to the object: deviceControlInstances.
        if (instance != null)
            throw new InstantiationException (this.toString());
        else {
            /*createLineDisplayEntry();
            createPOSPrinterEntry();
            createToneIndicatorEntry();
            createKeyLockEntry();
            createPOSKeyBoardEntry();
            createCashDrawerEntry();*/
            HdeviceControlInstances = createOpenHash();
            instance = this;
        }
    }

    public static POSPeripheralHome getInstance() {
        try {
            if (instance == null)
                instance = new POSPeripheralHome();
        } catch (InstantiationException ie) {
            ie.printStackTrace(CreamToolkit.getLogger());
            return instance;
        } catch (ConfigurationNotFoundException ce) {
            ce.printStackTrace(CreamToolkit.getLogger());
            return null;
        } catch (JposException je) {
            je.printStackTrace(CreamToolkit.getLogger());
            return null;
        }
            return instance;
    }

    public void setEventForwardEnabled (boolean enabled) {
        this.eventForwardEnabled = enabled;
    }

    public boolean getEventForwardEnabled () {
        return eventForwardEnabled;
    }

    //implements the listener interface
    //event forwarding
    public void dataOccurred(DataEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine eventProcessing = StateMachine.getInstance();
        eventProcessing.processEvent(e);
    }

    public void directIOOccurred(DirectIOEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine eventProcessing = StateMachine.getInstance();
        eventProcessing.processEvent(e);
    }

    public void errorOccurred(ErrorEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine eventProcessing = StateMachine.getInstance();
        eventProcessing.processEvent(e);
    }

    public void outputCompleteOccurred(OutputCompleteEvent e) {
        if (!getEventForwardEnabled())
            return;
        StateMachine eventProcessing = StateMachine.getInstance();
        eventProcessing.processEvent(e);        
    }

    public void statusUpdateOccurred(StatusUpdateEvent e) {
        if (!getEventForwardEnabled())
			return;
//		if (e.getSource() instanceof CashDrawer
//			&& hyi.cream.dac.Transaction.getStatusFlag() != 0)
//			hyi.cream.dac.Transaction.setStatusFlag(1, true);
		StateMachine.getInstance().processEvent(e);
    }
    //end of event forwarding

    //Get Hashtable containing the instances created by this class, may be null
    public Hashtable getHash() {
        return HdeviceControlInstances;
    }

    //Methods to get a single device control instance from the hash table
    //Six posdevices available now
    public POSPrinter getPOSPrinter() throws NoSuchPOSDeviceException {
        if (HdeviceControlInstances.containsKey("POSPrinter")) {
            POSPrinter printer = (POSPrinter)HdeviceControlInstances.get("POSPrinter");
            return printer;
        } else
            throw new NoSuchPOSDeviceException();
    }

    public ToneIndicator getToneIndicator() throws NoSuchPOSDeviceException {
        if (HdeviceControlInstances.containsKey("ToneIndicator")) {
            ToneIndicator tone = (ToneIndicator)HdeviceControlInstances.get("ToneIndicator");
            return tone;
        } else
            throw new NoSuchPOSDeviceException();
    }

    public CashDrawer getCashDrawer() throws NoSuchPOSDeviceException {
        if (HdeviceControlInstances.containsKey("CashDrawer")) {
            CashDrawer cash = (CashDrawer)HdeviceControlInstances.get("CashDrawer");
            return cash;
        } else
            throw new NoSuchPOSDeviceException();
    }

    public LineDisplay getLineDisplay() throws NoSuchPOSDeviceException {
        if (HdeviceControlInstances.containsKey("LineDisplay")) {
            LineDisplay ld = (LineDisplay)HdeviceControlInstances.get("LineDisplay");
            return ld;
        } else
            throw new NoSuchPOSDeviceException();
    }

    public Keylock getKeylock() throws NoSuchPOSDeviceException {
        if (HdeviceControlInstances.containsKey("Keylock")) {
            Keylock keyLock = (Keylock)HdeviceControlInstances.get("Keylock");
            return keyLock;
        } else
            throw new NoSuchPOSDeviceException();
    }

    public POSKeyboard getPOSKeyboard() throws NoSuchPOSDeviceException {
       if (HdeviceControlInstances.containsKey("POSKeyboard")) {
            POSKeyboard keyBoard = (POSKeyboard)HdeviceControlInstances.get("POSKeyboard");
           return keyBoard;
       } else
           throw new NoSuchPOSDeviceException();
    }

    public PINPad getPINPad() throws NoSuchPOSDeviceException {
       if (HdeviceControlInstances.containsKey("PINPad")) {
            PINPad pinpad = (PINPad)HdeviceControlInstances.get("PINPad");
           return pinpad;
       } else
           throw new NoSuchPOSDeviceException();
    }

    public jpos.Scanner getScanner() throws NoSuchPOSDeviceException {
       if (HdeviceControlInstances.containsKey("Scanner")) {
    	   jpos.Scanner scanner = (jpos.Scanner)HdeviceControlInstances.get("Scanner");
           return scanner;
       } else
           throw new NoSuchPOSDeviceException();
    }

    public MSR getMSR() throws NoSuchPOSDeviceException {
       if (HdeviceControlInstances.containsKey("MSR")) {
            MSR msr = (MSR)HdeviceControlInstances.get("MSR");
           return msr;
       } else
           throw new NoSuchPOSDeviceException();
    }

    //Private methods for utility
    //Using the java.util.Properties to read posdevice.conf
    private Properties loadProperties() throws  ConfigurationNotFoundException {
        Properties properties = new Properties();
        try {
            File posdevicesFile = CreamToolkit.getConfigurationFile(POSPeripheralHome.class);
            //System.out.println(posdevicesFile.getAbsoluteFile());
            //File (filePath); //get the configuration file
            FileInputStream posdevicesInput = new FileInputStream (posdevicesFile);
            properties.load(posdevicesInput);
        } catch (FileNotFoundException fe) {
            fe.printStackTrace(CreamToolkit.getLogger());
            throw new ConfigurationNotFoundException("Can't access the availiable configuration!");
        } catch (IOException ie) {
            ie.printStackTrace(CreamToolkit.getLogger());
            throw new ConfigurationNotFoundException("Can't access the availiable configuration!");
        }
        return properties;
    }
    // the end of Private methods for utility
    
    //private void handleException (Exception e) {
        //System.out.println(e+this.toString());
    //}
  
    //Define a create method to return relevant control class according the class name given
    private BaseControl createInstance (String controlClassName) throws ConfigurationNotFoundException {
        BaseControl controlInstance = null;
        String deviceName = null;
        try {
            deviceName = "jpos." + controlClassName;
            Class controlClass = Class.forName(deviceName);
            controlInstance =(BaseControl)controlClass.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            throw new ConfigurationNotFoundException("Can't access the availiable configuration!");
        }
          catch (InstantiationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            throw new ConfigurationNotFoundException("Can't access the availiable configuration!");
        }
          catch (ClassNotFoundException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            throw new ConfigurationNotFoundException("Can't access the availiable configuration!");
        }
        return controlInstance;
    }

    //Create all instances and open relevant services by reading posdevices.conf and store them in a Hashtable
    public Hashtable createOpenHash() throws ConfigurationNotFoundException, JposException {
        Hashtable deviceTable = new Hashtable();
        Enumeration keys = null;
        BaseControl tempControl = null; //Temporary variable to store the control value
        String tempKey = null;          //Temporary variable to store the Key value
        Properties properties = loadProperties ();
        keys = properties.propertyNames();  //Get key enumeration
        for (int i=0; i<properties.size(); i++){
            tempKey =(String)keys.nextElement();
            tempControl = createInstance(tempKey);
        //Call createInstance() to create instance and open relevant services
            //String t = properties.getProperty(tempKey);
            //CreamToolkit.logMessage("010 " + properties.getProperty(tempKey));
            try {
                tempControl.open(properties.getProperty(tempKey));
                deviceTable.put(tempKey,tempControl);
            } catch (Exception e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        }//add them to the Hashtable
        return deviceTable;
    }

    //Method to close all instance in hash table 
    public void closeAll() throws JposException {
        if (HdeviceControlInstances!=null) {
            BaseControl tempControl = null;
            Enumeration elements = HdeviceControlInstances.elements();
            while (elements.hasMoreElements()) {
                tempControl = (BaseControl)elements.nextElement();
                tempControl.close();
            }
        }
    }

    public void clearAll() {
        if (HdeviceControlInstances!=null) {
            //closeAll();
            HdeviceControlInstances.clear();
            HdeviceControlInstances = null;
        }
    }

    //JposEntry part....................;
    /*private static void createPOSKeyBoardEntry() {
        JposEntry jposEntry = new SimpleEntry();
        jposEntry.addProperty(JposEntry.LOGICAL_NAME_PROP_NAME, "POSKeyboard");
        jposEntry.addProperty(JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.cream.jpos.CreamJposServiceInstanceFactory");
        jposEntry.addProperty(JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.cream.jpos.POSKeyboardService");
        jposEntry.addProperty(JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp.");
        jposEntry.addProperty(JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.tw");
        jposEntry.addProperty(JposEntry.DEVICE_CATEGORY_PROP_NAME, "POSKeyboard");
        jposEntry.addProperty(JposEntry.JPOS_VERSION_PROP_NAME, "1.4a");
        jposEntry.addProperty(JposEntry.PRODUCT_NAME_PROP_NAME, "POSKeyboard JavaPOS Service");
        jposEntry.addProperty(JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "Example POSKeyboard JavaPOS Service from HYI Corporation");
        jposEntry.addProperty(JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.tw");
        JposServiceLoader.getManager().getEntryRegistry().addJposEntry("POSKeyboard", jposEntry);
    }

    private static void createToneIndicatorEntry()  {
        JposEntry jposEntry = new SimpleEntry();
        jposEntry.addProperty( JposEntry.LOGICAL_NAME_PROP_NAME, "PcSpeaker" );
        jposEntry.addProperty( JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.jpos.loader.ServiceInstanceFactory" );
        jposEntry.addProperty( JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.jpos.services.ToneIndicatorPCSpeaker" );
        jposEntry.addProperty( JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp." );
        jposEntry.addProperty( JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.tw" );
        jposEntry.addProperty( JposEntry.DEVICE_CATEGORY_PROP_NAME, "ToneIndicator" );
        jposEntry.addProperty( JposEntry.JPOS_VERSION_PROP_NAME, "1.4a" );
        jposEntry.addProperty( JposEntry.PRODUCT_NAME_PROP_NAME, "ToneIndicator JavaPOS Service" );
        jposEntry.addProperty( JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "ToneIndicator, the PC speaker version of JavaPOS Service by HYI Corporation" );
        jposEntry.addProperty( JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.tw" );
        JposServiceLoader.getManager().getEntryRegistry().addJposEntry( "PcSpeaker", jposEntry );
   }

   private static void createPOSPrinterEntry() {
        JposEntry jposEntry = new SimpleEntry();
        jposEntry.addProperty( JposEntry.LOGICAL_NAME_PROP_NAME, "EpsonRPU420" );
        jposEntry.addProperty( JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.jpos.loader.ServiceInstanceFactory" );
        jposEntry.addProperty( JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.jpos.services.POSPrinterRPU420" );
        jposEntry.addProperty( "PORT_NAME", "COM2" );
        jposEntry.addProperty( JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp." );
        jposEntry.addProperty( JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.tw" );
        jposEntry.addProperty( JposEntry.DEVICE_CATEGORY_PROP_NAME, "POSPrinter" );
        jposEntry.addProperty( JposEntry.JPOS_VERSION_PROP_NAME, "1.4a" );
        jposEntry.addProperty( JposEntry.PRODUCT_NAME_PROP_NAME, "POSPrinter JavaPOS Service" );
        jposEntry.addProperty( JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "POSPrinter, the Epson RP-U420 of JavaPOS Service by HYI Corporation" );
        jposEntry.addProperty( JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.tw" );
        JposServiceLoader.getManager().getEntryRegistry().addJposEntry( "EpsonRPU420", jposEntry);
   }

   private static void createCashDrawerEntry() {
        JposEntry jposEntry = new SimpleEntry();
        jposEntry.addProperty( JposEntry.LOGICAL_NAME_PROP_NAME, "EpsonRPU420Drawer" );
        jposEntry.addProperty( JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.jpos.loader.ServiceInstanceFactory" );
        jposEntry.addProperty( JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.jpos.services.CashDrawerService" );
        jposEntry.addProperty( "PORT_NAME", "COM2" );
        jposEntry.addProperty( JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp." );
        jposEntry.addProperty( JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.tw" );
        jposEntry.addProperty( JposEntry.DEVICE_CATEGORY_PROP_NAME, "CashDrawer" );
        jposEntry.addProperty( JposEntry.JPOS_VERSION_PROP_NAME, "1.4a" );
        jposEntry.addProperty( JposEntry.PRODUCT_NAME_PROP_NAME, "POSPrinter JavaPOS Service" );
        jposEntry.addProperty( JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "POSPrinter, the Epson RP-U420 of JavaPOS Service by HYI Corporation" );
        jposEntry.addProperty( JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.tw" );
        JposServiceLoader.getManager().getEntryRegistry().addJposEntry( "EpsonRPU420Drawer", jposEntry);
   }

    private static void createKeyLockEntry() {
        JposEntry jposEntry = new SimpleEntry();

        jposEntry.addProperty(JposEntry.LOGICAL_NAME_PROP_NAME, "PartnerK78Keylock");
        jposEntry.addProperty(JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.jpos.ServiceInstanceFactory");
        jposEntry.addProperty(JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.jpos.PartnerK78Keylock");
        jposEntry.addProperty("COMP_CLASS_PROP_NAME", "hyi.cream.KeylockFrame");
        jposEntry.addProperty(JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp.");
        jposEntry.addProperty(JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.cn");
        jposEntry.addProperty(JposEntry.DEVICE_CATEGORY_PROP_NAME, "Keylock");
        jposEntry.addProperty(JposEntry.JPOS_VERSION_PROP_NAME, "1.4a");
        jposEntry.addProperty(JposEntry.PRODUCT_NAME_PROP_NAME, "Keylock JavaPOS Service");
        jposEntry.addProperty(JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "Example Keylock JavaPOS Service from HYI Corporation");
        jposEntry.addProperty(JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.cn");

        JposServiceLoader.getManager().getEntryRegistry().addJposEntry("PartnerK78Keylock", jposEntry);
    }

    private static void createLineDisplayEntry() {
        JposEntry jposEntry = new SimpleEntry();

        jposEntry.addProperty(JposEntry.LOGICAL_NAME_PROP_NAME, "Cd5220");
        jposEntry.addProperty(JposEntry.SI_FACTORY_CLASS_PROP_NAME, "hyi.jpos.CreamJposServiceInstanceFactory");
        jposEntry.addProperty(JposEntry.SERVICE_CLASS_PROP_NAME, "hyi.jpos.LineDisplayCD5220");
        jposEntry.addProperty("DevicePortName", "COM2");
        jposEntry.addProperty(JposEntry.VENDOR_NAME_PROP_NAME, "HYI, Corp.");
        jposEntry.addProperty(JposEntry.VENDOR_URL_PROP_NAME, "http://www.hyi.com.cn");
        jposEntry.addProperty(JposEntry.DEVICE_CATEGORY_PROP_NAME, "LineDisplay");
        jposEntry.addProperty(JposEntry.JPOS_VERSION_PROP_NAME, "1.4a");
        jposEntry.addProperty(JposEntry.PRODUCT_NAME_PROP_NAME, "LineDisplay JavaPOS Service");
        jposEntry.addProperty(JposEntry.PRODUCT_DESCRIPTION_PROP_NAME, "LineDisplay JavaPOS Service from HYI Corporation");
        jposEntry.addProperty(JposEntry.PRODUCT_URL_PROP_NAME, "http://www.hyi.com.cn");

        JposServiceLoader.getManager().getEntryRegistry().addJposEntry("Cd5220", jposEntry);
    }*/


    //main thread for testing purpose.....
    //Sample program for using the POSPeripheralHome and get relevant control instances
    /*public static void main(String[] args) {
        POSPeripheralHome jposHome = null;

       try {
        jposHome = POSPeripheralHome.getInstance();
           
            Hashtable allControlInstance = jposHome.getHash();
            //ToneIndicator-----------------------------------------------------
            ToneIndicator tone = (ToneIndicator)allControlInstance.get("ToneIndicator");
            tone.setDeviceEnabled(true);
            tone.setAsyncMode(true);
            tone.sound(5, 500);
            /*
            //POSPrinter--------------------------------------------------------
            POSPrinter printer = null;
            try {
                 printer = jposHome.getPOSPrinter();
            } catch (NoSuchPOSDeviceException ne) { }
            printer.setDeviceEnabled(true);
            printer.claim(0);
            printer.claim(-1);
            printer.claim(-1);
            //for (int i=0;i<4;i++)
            printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001Bd\u0004\n");
            //for (int i=0;i<21;i++)
            printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001Bd\u0014\n");
            for (int i=0;i<11;i++)
            printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
            printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001B|sL\u000C\n");
            //CashDrawer--------------------------------------------------------
            CashDrawer cash = (CashDrawer)allControlInstance.get("CashDrawer");
            cash.setDeviceEnabled(true);
            //cash.openDrawer();
            cash.waitForDrawerClose(50000,0,500,500);
            //
       } catch ( JposException je ) { System.out.println(je); }
          //catch ( ConfigurationNotFoundException ce ) { System.out.println(ce); }
        //JposEntryEditor.setDefaultFrameCloseOperation( JposEntryEditor.HIDE_ON_CLOSE );
        JposEntryEditor.setFrameVisible( true );
        System.out.println(jposHome.getClass().getName()+"'s Unit Test program!");
        return;
    }//*/
}



