package hyi.cream;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.math.*;
import java.sql.*;
import java.text.*;

import jpos.*;
import jpos.Scanner;
import jpos.events.*;

import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.util.*;
import hyi.cream.state.*;
import hyi.cream.inline.*;

/**
 * POSTerminalApplication. Main class of this POS application.
 *
 * @author Bruce
 */
public class POSTerminalApplication extends Frame {

    private static final long serialVersionUID = 1L;

    /**
     * <P>版本编号{Major}.{Minor}.{Revision}。
     * <P>编号变化规则：Major表示大的版本变动，Minor表示功能有所增修，Revision也表示功能有局部修改但是不会全面影响所有客户。
     *
     * <P>Change history:
     * <p>Ver 4.13.43 20090414
     *    <LI> 1. Fix a bug of showing changeamt to linedisplay.
     *
     * <p>Ver 4.13.42
     *    <LI> 1.解決全退后点击清除键后，交易没有改为正向的问题。
     *    <LI> 2.在小计面板上增加代收，代售的信息。
     *    <LI> 3.添加卡纸重印中的组合促销的信息
     * <p>Ver 4.13.41
     *    <LI> 1.解決si折扣，清除后再重新結帳引起的amount 与afteramount 不一致。
     *    <LI> 2.历史交易中除一般交易，被作废（前笔误打、重印、退货等）的交易，部分退货正项交易外，其他的交易类型都不打印出来
     *    <LI> 3.解决代售退货交易，数据库中无变更的记录
     *    <LI> 4.解决折扣后分摊金额因为四舍五入而在tranhead表中对应的si折扣金额有误差，而且将原来折扣后的差异加在最后一项商品上改为将折扣后的
     *           差异加在金额最大的一项商品上。
     *    <LI> 5.查询打印历史交易时，由原来的整个交易范围一次加载改为按交易序号加载，且将查询方式重新设计了一下，增加了查询首笔和末笔。 
     *    <LI> 6.查询打印历史交易由原来的数字键+确定键改为按数字键查询打印，且能够查询给定范围内的所有交易但只打印一些特殊的交易。  
     *    <LI> 7.查询打印历史交易中的代收交易时不再依赖于DaiShouSales2表中的数据。
     * <p>Ver 4.13.40
     *    <LI> 1. 解决指定更正和立即更正产生的税额和销售产生的税额不一致的问题
     *    <LI> 2. 修正卡纸重印中GROSALAMT错写的问题
     *    <LI> 3. 增加交易查询功能
     * <p>Ver 4.13.39
     *    <LI>1. Always print the receipt out while reprinting.
     *    <LI>2. Reprint old transaction.
     *    <LI>3. printing transaction when daishou2amt > 0 even ReceiptPrint = false
     *    <LI>4. Add a generateVoidTransaction flag to ReprintButton.
     *    <LI>5. printing X/Z report ignore ReceiptPrint flag.
     * <p>Ver 4.13.38
     *    <LI>Fix bug: 在日结完成后，如果shift中仍残存有其他1970年记录，在POS重启后，竟可以再做一次日结。现在修改了
     *        取得current shift的逻辑以避免这种问题发生。
     *    <LI>POS启动时在cream.log增加："POS startups... (version X.Y.Z)"。
     *    <LI>上传Z时不再insert posaccountdate记录。 
     * <p>Ver 4.13.37
     *    <LI>Fix a bug of QueryMaxTransactionNumber and QueryMaxZNumber.
     *    <LI>Run InlineServer by windows service.
     * <p>Ver 4.13.36
     *    <LI>解决交易保存失败问题：原因是!StateMachine的event处理改成asynchronous方式之后，多线程同时交错处理的副作用。
     *    <LI>解决连接Oracle SC会主档下传会失败的问题。  
     * <p>Ver 4.13.35
     *    <LI> Inline server support Oracle.
     *    <LI> The other two tables (daishousales2 & token_trandtl) are added to the mysqlcheck list.
     *    <LI> Remove the duplicated key error logs for the table of z and shift when recalculating transactions.
     *    <LI> Fix the displaying problem of ItemList, it ocassionally show nothing.
     *  
     * <P>Ver 4.13.33
     *    <LI> 解决Z帐序号会跳号问题：
     *         - 取当前Z的逻辑：看序號最大的那筆是否為197X年，如果是則返回他作為當前Z
     *         - 重算會把下一筆改成1970
     *    <LI> 解决查价时会去多馀的query rebate的问题
     *    <LI> 执行inline client时不去queryMaxTransactionNumber和queryMaxZNumber
     *    <LI> SC拒绝店号不同的POS连线
     *
     * <P>Ver 4.13.32
     *    <LI> 解決tranhead會生兩筆日結交易的問題。
     *         步驟：轉鑰匙，按[1]選日結，然後（很快速的、少於0.1秒的）按[確認]鍵。
     *
     * <P>Ver 4.13.12
     *    <LI> 修改OpenPrice商品折扣后金额填写有误的问题
     *   
     * <P>Ver 4.13.11
     *    <LI> 更正TranProcessorThread中更新历史库存时的错误
     *    <LI> 新增铁通通信费代收项目 
     * 
     * <P>Ver 4.13.10
     *    <LI> 增加 ThreadWatchDog 在Server里防止Server 端处理交易的线程死掉
     *    <LI>                     在ePOS里防止Client 线程死掉   
     *    <LI> 更新下传主档Cache功能，现在由eComm下传完主档以后调用Server -getmaster准备主档文件  
     *  
     * <P>Ver 4.13.09
     *    <LI>修改ZReport.createZReport()取得Z序号的逻辑。原逻辑不正确，如果Z上传失败会导致Z序号错误。
     *    <LI>修改ZReport.queryByDateTime()，预防万一在有多笔1970-01-01记录的情况下，也让它去取最大
     *        序号的那一笔。
     * 
     * <P>Ver 4.13.08
     *    <LI>加长Client的time-out时间。
     *    <LI>在校验代收条码时增加catch NumberFormatException。
     * 
     * <P>Ver 4.13.03
     *    <LI>(2004-01-12)
     *  
     * <P>Ver 4.13.02
     *    <LI>(2004-01-06)InlineServer接收到交易以后，先保存到临时文件，然后再重命名
     *    <LI>(2004-01-06)解决退货、交易取消计算来客数错误的问题 
     * 
     * <P>Ver 4.13.01
     *    <LI>(2003-12-25)解决交易序号跳号会使重算被中断的问题。
     *    <LI>(2003-12-25)解决纯代收应该不算来客数的问题。
     * 
     * <P>Ver 4.13
     *    <LI>(2003-12-25)增加解交易线程TranProcessThread，每一分钟解交易一次。
     *                    如果要使用此线程解读交易，则在Server端的cream.conf中：
     *                       PostProcessor=PostProcessorImplA
     *                    另外必须增加一个程序增加包：jdom.jar
     *    <LI>(2003-12-25)解决交易序号可能会变成0，还有交易重印后交易序号会重复的问题。
     *    <LI>(2003-12-25)存储交易前先记录配达单号，以避免配达单号重复的问题。
     *    <LI>(2003-12-25)为上海领袖服饰增加“营业员”按键。
     *    <LI>(2003-12-25)增加property "LicenseInfo"表示要显示的license info，如果"DontShowLicenseInfo"为"no"，
     *                    则会显示"LicenseInfo"内容，如果此"LicenseInfo"不存在，则显示resource字符"CompanyTitle"
     *
     * <P>Ver 4.12.05
     *    <LI>(2003-12-19)因为用JDK1.4.2 compile不过，将Protector移到protector package下
     *    <LI>(2003-12-24)解决MixAndMatchVersion=2时，若从结账画面返回至销售画面时，会因调用
     *                    Transaciton.initPaymentInfo()导致LineItem的AfterDiscountAmount的值变成与Amount相同
     *    <LI>(2003-12-24)解决部分退货会多打印payment的问题。
     *    <LI>(2003-12-24)解决keylock1.conf设置CheckCashierState时，做完退货操作后会死机的问题。
     *
     * <P>Ver 4.12.04
     *    <LI>(2003-12-12)修改盘点输入无法更正的问题。
     *    <LI>(2003-12-17)增加property "PrintImmediateRemove"(default is "no"),可设置立即更正是否需要打印。
     *    <LI>(2003-12-17)解决负项销售没有打印payment部分的问题。
     *    <LI>(2003-12-17)解决负项销售状态结束时应返回至Keylock菜单的问题。(必须更换statechart3.conf)
     *    <LI>(2003-12-17)解决负项销售找零金额计算错误的问题。
     *    <LI>(2003-12-17)解决SignOn状态无法再按〔收银员〕键进入收银员菜单的问题。(必须更换statechart3.conf)
     * 
     * <P>Ver 4.12.03
     *    <LI>(2003-12-05)修改ReceiptGenerator字符长度没有切准的问题。
     *
     * <P>Ver 4.12.02
     *    <LI>(2003-12-05)解决交易取消在使用receipt.conf时，会跑出很长一段纸的问题。
     *    <LI>(2003-12-05)解决cashier表没有CASRIGHTS字段的时候借零投库会死掉的问题。
     * 
     * <P>Ver 4.12.01
     *    <LI>(2003-12-04)修改syncTransaction查找本机相关序号时，增加"POSNO=xx"的条件。
     *                    这样可以让它即使在POS更换机号的时候，也不会让后台误认为有数据未上传。
     *    <LI>(2003-12-04)把检查单头单身不符的时间拉长为14天。
     * 
     * <P>Ver 4.12
     *    <LI>(2003-12-01)有两种决定会计日期的models:
     *                    如果AccountingDateShiftTime="afternoon"(default)，则超过换日时间后为(d+1)日；
     *                    否则(="morning")超过换日时间后仍为(d)日，在换日之前为(d-1)日。
     *    <LI>(2003-12-01)增加inline "putobject2" protocol，可以加快盘点数据上传速度。
     *    <LI>(2003-12-01)修正没有update "NextTransactionNumber"的问题。
     *    <LI>(2003-12-01)如果交易数据存储失败，仍将"NextTransactionNumber"往上加。以免在有残余交易记录
     *                    存在数据库的情况下，会导致一直无法成功存储的问题；并且禁止用户继续操作，必须叫修
     *                    或重启。
     *    <LI>(2003-12-01)在交易数据存储成功之后，才会执行打印动作。
     *                    结账操作程序：开抽屉->存储交易->打印支付部分。
     *                    所以如果数据存储失败，发票下方的支付部分不会打印出来。
     *    <LI>(2003-12-01)增加一个property "ShowAmountWithScale2" (default is "no"), 表示销售画面
     *                    是否金额要显示两位小数。
     *    <LI>(2003-12-01)现金支付也必须看是否为“自动结清余额”(payment.PAYTYPE第7位），如果是"0"的话,
     *                    在结账的时候不能直接按确认键就自动结账。(必须同时更换statechart3.conf）
     *    <LI>(2003-12-01)增加property "ChangeAmountUpperLimit"(default is 100)，如果找零金额大于等于此
     *                    值，则会报警。
     *    <LI>(2003-12-01)更换后台inline server使用的MySQL JDBC driver。
     *    <LI>(2003-12-01)修改第二次盘点时数据无法显示的问题。
     *    <LI>(2003-12-02)减少盘点录入时所使用的内存空间，这样在大量数据录入的情况下不易使速度变慢。
     *    <LI>(2003-12-02)盘点结束应回到原Keylock菜单。(必须同时更换statechart3.conf）
     *    <LI>(2003-12-02)输入的金额小于结帐金额时，在提示"付款金额不足"后，增加发出一声报警声。
     *    <LI>(2003-12-02)喜士多启用receipt.conf打印发票。并解决收执联和存根联应同步卷动的问题。
     *
     * <P>Ver 4.11.06
     *    <LI>(2003-11-26)修改queryMember时先查找commdl_member再查找member.
     *    <LI>(2003-11-26)修改还圆金发票格式。
     *    <LI>(2003-11-26)取消还圆金输入金额有2000元的上限。
     *
     * <P>Ver 4.11.05
     *    <LI>(2003-11-24)还元金（红利）输入金额不能超过2000元和非会员不能使用还元金支付要提示用户相应信息。
     *    <LI>(2003-11-24)盘底结束要回到原始KeylockState。
     *    <LI>(2003-11-24)查询单品时要显示还圆金比率。
     *    <LI>(2003-11-24)修改变价后还圆金没有重新计算的问题。
     *    <LI>(2003-11-24)修改发票中会员前日累计还圆金可能有错误的问题。
     * 
     * <P>Ver 4.11.04
     *    <LI>(2003-11-21)CStorePostProcessor:因为怀疑JRE可能造成停滞在lockObject，所以先拿掉，先舍弃更新库存可能不准的问题
     * 
     * <P>Ver 4.11.03
     *    <LI>(2003-11-21)还元金（红利）输入金额不能超过2000元。
     *    <LI>(2003-11-21)非会员不能使用还元金支付。
     *    <LI>(2003-11-21)丢弃property "NextTransactionSequenceNumber".
     *
     * <P>Ver 4.11.02
     *    <LI>(2003-11-19)增加POSPrinterTEC6400，梅林改用receipt.conf。
     *    <LI>(2003-11-19)让交班日结结束后可以回到原Keylock state。
     *
     * <P>Ver 4.11.01
     *    <LI>(2003-11-17)CStorePostProcessor在insert交易之前不lock lockObject，因为怀疑Java因失误
     *                    造成停滞在synchronized lockObject上。
     *    <LI>(2003-11-17)修改Tec6400CashDrawer, 改用busy wait的方式等待抽屉关闭，同样因为怀疑Java
     *                    会在notifyAll()的时候没有通知到wait()，使得偶有wait forever现象。
     *    <LI>(2003-11-18)增加property "ConfuseTranNumber"(缺省为"no")，表示是否使用乱数交易序号作为
     *                    显示和打印。若要在画面上显示乱数交易序号，在screenbanner.conf中把交易序号的
     *                    property name同时改为"PrintTransactionNumber"。
     *    <LI>(2003-11-18)借零、投库输入金额不可为零。
     *
     * <P>Ver 4.10
     *    <LI>(2003-11-04)增加Store.createCache(),避免在store表变化后下传会不起作用。
     *    <LI>(2003-11-04)加长Client的ACK_TIMEOUT_INTERVAL，可能会解决有时候上传Z以后会离线的问题，
     *                    因为原来Client在发出对象以后，只等5秒，现在加长为15秒。
     *    <LI>(2003-11-04)Inline server增加一个TransactionCheckerThread，用来检查是否存在tranhead与
     *                    trandetail笔数不符的交易，如果有的话将之删除，以便下次让inline client再重新
     *                    送上来。这样可以保证后台不会有partial transaction。
     *    <LI>(2003-11-04)Inline client第一次连到server后，会先通过queryMaxTransactionNumber和
     *                    queryMaxZNumber两个命令询问后台的交易序号和Z帐序号最大值。若后台的序号比前台
     *                    大，则起始序号必须用后台的最大序号加1。这样可以解决两个问题：
     *                    a. 若前台机器换机，那新装的POS机可以继续先前的序号往上加。这样总部不会收到有
     *                       相同序号的记录。
     *                    b. 若POS因不当关机或其他硬件因素导致已上传的交易丢失，也不会造成交易序号重复
     *                       的问题。
     *    <LI>(2003-11-06)增加property "UseFloppyToSaveData"(缺省为"no")，表示在交班或日结时若离线或
     *                    上传失败是否需要插软盘。
     *    <LI>(2003-11-10)Inline server property增加"UpdateInventory"(缺省为"yes")，表示在交易上传
     *                    时是否需要更新库存表。
     *    <LI>(2003-11-11)增加propert "ComputeDiscountRate"(缺省为"no")，表示是否计算trandetail中的
     *                    折扣率和折扣代号字段。可以减少在变价的时候在
     *                    cream.conf里面会有因为找不到distype表而抛出的意外log。
     *    <LI>(2003-11-11)避免结账后死机的问题：等待交易储存结束设time-out时间。
     *    <LI>(2003-11-11)在结账后若出现“请关上抽屉”，关上抽屉后会将讯息清除。
     *
     * <P>Ver 4.09
     *    1. (2003-10-30)再次修改交易资料删除历史数据时会误把10天前的都删掉。
     *    2. (2003-10-30)退货时若发现trandetail的数据一条都没有的时候，提示用户到后台退货。
     *    3. (2003-10-30)避免若有很多交易未上传会导致结账时等候，感觉像死机一样。
     *    4. (2003-10-30)修改价格为0时会无法变价的问题。
     *    5. (2003-10-30)上传交易时会检查交易头记录的身笔数和实际的身笔数是否一致，如果不一致，丢弃它。
     *    6. (2003-10-30)修改退货或交班可能会造成交易序号跳号的问题。
     *
     * <P>Ver 4.08
     *    (2003-10-23)修改交易资料删除历史数据时会误把10天前的都删掉。
     * 
     * <P>Ver 4.07
     *    1. (2003-10-19)修改上传历史depsales, daishousales, daishousales2时会计日期会填错的问题
     *    2. (2003-10-22)修改如果序号大的交易时间比序号小的时间早的话，会造成拼命上传交易的问题。
     * 
     * <P>Ver 4.06.02
     *    1. (2003-10-15)允许cream.jar时若带参数-version会显示程序版本号和machine ID。
     *
     * <P>Ver 4.06.01
     *    1. (2003-9-25)解决喜士多的“打印X帐”结束后会回到Keylock3菜单的问题。statechart3.conf和keylock2.conf要跟着修改。
     *    2. (2003-10-8)解决有可能因为机器速度慢，导致储存交易的线程还没结束之前交易数据就被清除了。
     *       此问题现象：会导致交易数据的序号不断重复，无法储存交易数据。
     *    3. (2003-10-8)解决删除trandetail过期数据因逻辑修改产生的bug。
     *       此问题现象：在cream.log中每天凌晨会看到什么"SYSDATE"字段不存在的错误信息，而且trandetail
     *       表中60天前的历史数据无法删除。
     * 
     * <P>Ver 4.06
     *    1. (2003-9-24)修改历史交易数据上传后，CStorePostProcessor要增加历史inv库存表销售数、和减少之后日期的期初库存数
     *    2. (2003-9-25)解决查询会员会始终要用户确认的问题(Client.commandLineMode default设为false)
     * 
     * <P>2003-09-08 (Ver 4.05)
     *    1. 防止变价时输入太大的数字，导致trandetail会有999999999999.99的超大数值。
     *    2. 在insert/update/delete数据库记录后，立即用"sync"命令flush OS buffer，以避免数据因不当
     *       关机而丢失。
     *    3. 交易中的tranhead.MNMCNT0用来记录组合促销成立的总次数，其他MNMCNT[1..4]废弃不用。
     *    4. 增加property "MaxTransactionNumber"(最大交易序号，到达此交易序号后归1，缺省为99999999)
     *       和"TransactionReserved"(交易保留天数，缺省为60)。
     *       修改inline "synctrans" protocol。
     *    5. 增加细分类。后台posdl_cat, posdl_plu, posdl_trandtl表增加thinCategoryNumber，前台cat,
     *       plu, trandetail表增加THINCATNO。
     *   10. 增加property "CanDaiShou2Return"(公共事业费代收是否允许退货（缺省为yes）)。
     *       -> 喜士多要求设置成no。
     *   11. 增加property "IgnoreKeylockWithinTransaction"(在交易输入当中忽略钥匙转动事件（缺省为no）)。
     *       -> 铠捷必需设置为yes。
     *   13. 增加软件防拷功能，需要输入注册码。
     *   14. ePOS's background is anti-aliased.
     *   15. 增加即时备份机制。增加property "MySQLRemoteBackupDirectory"(MySQL远端备份目录
     *       （rsync server directory）)，必需开启备份机的rsync server服务，并设置备份目录。
     *       config.conf可以增加一个功能:
     *         XX.从备份机回复数据库,recoverDBFromBackUp
     *       -> 此功能测试中，暂不对外开放。
     *   16. Remove unused items in CreamResource_zh_CN.
     *   17. (2003-9-18)修正可能因为单品数太多导致PLU下传时会time-out。
     *
     * <P>2003-8-25 (Ver 4.04.03)
     *    1. 防止更正记录的AfterDiscountAmount会不等于Amount(Transaction)(虽然不知道为何会有这种情况发生)。
     *    2. Trigger.shrinkLogFiles()时先close log files，以免会有log file无法写入的问题。
     *
     * <P>2003-8-19 (Ver 4.04.02)
     *    1. 为喜士多增加公共事业费代收的会计科目300(CStorePostProcessor)。
     *    2. 修改DacViewer.paint()有NullPointerException的问题。
     *
     * <P>2003-8-18 (Ver 4.04.02)
     *    修改重新计算功能，重新计算时,shift,z,depsales,daishousale 一并重新计算
     *
     * <P>2003-8-16 (Ver 4.04.01)
     *    修改公共事业费代收明细上传时，会计日期应取决于Z帐序号而不是当前的会计日。
     *
     * <P>2003-8-14 (Ver 4.04)
     *    修改秤重商品应该不可以修改数量的问题。
     *
     * <P>2003-8-10 (Ver 4.03) 
     *    增加property "DontNeedTurnKeyAtStart", 表示可以设置在程序开始的时候是否需要先转钥匙到OFF再转到销售位置。
     *
     * <P>2003-08-05 (Ver 4.3)
     *    1. 增加是否显示Button panel property (property "CreateButtonPanel").
     *    2. 增加是否显示mouse cursor property (property "ShowMouseCursor").
     *    3. 发布公共事业费代收功能。
     *    4. 手工主档下传若没有下传任何文件，则不需要重新启动。
     *    5. 解决shift帐的price open金额不对的问题。
     *    6. 按[0..9]* -> EnterButton 亦可输入商品。要配合更换statechart3.conf。
     *    7. 增加DiscountRateIDButton, DiscountRateButton.
     *
     * <P>2003-07-28 (Ver 4.2)
     *    1. 增加盘点录入功能。
     *
     * <P>2003-07-25 (Ver 4.1)
     *    1. 修改通报信息显示的线程。
     *    2. Z/Shift上传所填的会计日期，不要取当前的后台会计日期，而是以Z/Shift的结束时间和会计换日
     *       时间来决定。PostProcesser在收到Z之后，必须找到现金日报的那一个日期的数据，并且将该日的
     *       所有POS上传Z重新累加到现金日报资料。(注意：cake.calendar中必须要设置会计换日时间！）
     *    3. 接并口钱箱(CashDrawerPartner)若CheckDrawerClose设置成yes，不会造成死机。
     *
     * <P>2003-07-08 (Ver 4.0)
     *    1. New UI
     *    2. 所有config file目录全部取自property的“configDir”（若不存在则default为当前目录）。
     *
     * <P>2003-07-07 (Ver 3.10)
     *    修改磅秤商品金额数量不对的问题。
     * 
     * <P>2003-06-25 (Ver 3.09)
     *    1. 修改问题：修改指定更正的lineitem的税额字段可能为正值的问题。
     *    2. 修改SC的pricechghead的限量促销商品的实际销售量字段的缺省值设置为1。
     * 
     * <P>2003-06-23 (Ver 3.08)
     *    1. 修改若画面上看不到任何商品（例如可能被更正掉），仍允许输入会员编号。
     *    2. 增加限量促销功能。
     * 
     * <P>2003-05-29 (Ver3.05)
     *    1. 增加会员查询功能。
     *    2. 解决同一时间上传日结报表会导致后台现金日报表不正确的问题。
     *    3. 解决未signon之前应不可以重复选择日结的问题。
     */
    private static final String version = "4.13.43";

    private static ThreadWatchDog dog = ThreadWatchDog.getInstance();
    private boolean debug = false;
            
    private StateMachine stateMachine;
    private POSPeripheralHome posPeripheralHome;
    private Transaction currentTransaction;
    private SystemInfo systemInfo;
    private POSButtonHome posButtonHome;
    private ScreenBanner screenBanner;
    private Indicator messageIndicator;
    private Indicator warningIndicator;
    private Indicator informationIndicator;
    private ItemList itemList;
    private DacViewer dacViewer;
    private PopupMenuPane popupMenuPane;
    private PopupMenuPane1 popupMenuPane1;
    private SignOffForm signOffForm;
    private boolean buyerNumberPrinted;
    private boolean trainingMode;
    private boolean keyState;
    private boolean returnItemState;
    private java.util.ResourceBundle res = CreamToolkit.GetResource();
    private String cashierNumber = "";
    private boolean transactionEnd = true;
    private int keyPosition = 0;
    private boolean beginState = true;
    private boolean isAllReturn = false;
    private boolean checked = false;
    private boolean scanCashierNumber = false;
    private BigDecimal returnQty = new BigDecimal(0);
    private EPOSBackground backgroundPanel;

    public static Object waitForTransactionStored = new Object();
    static POSTerminalApplication posTerminalApplication;
    private static String newCashierID; //纪录高权限用户  add by lxf 2003.02.12
    private static java.util.List scaleBarCodeFormatList;
    //private boolean firstPaint = true;

    private Banner connectMsgBanner;
    private Banner pageMsgBanner;
    //private Banner totalMsgBanner;
    Panel totalMsgPanel = new Panel();
    private ProductInfoBanner productInfoBanner;
    private TotalAmountBanner totalAmountBanner;
    private Banner screenBanner2;
    private boolean payingPaneVisible = false;
    private PayingPane payingPane;
    private PayingPaneBanner payingPane1;
    private PayingPaneBanner payingPane2;
    private CreamProperties prop = CreamProperties.getInstance();
    
    /**
     * Singleton getter.
     */
    public static POSTerminalApplication getInstance() {
        try {
            if (posTerminalApplication == null) {
                posTerminalApplication = new POSTerminalApplication();
            }
        } catch (InstantiationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return posTerminalApplication;
    }

    /**
     * Constructor of POSTerminalApplication.
     */
    public POSTerminalApplication() throws InstantiationException {
        if (posTerminalApplication != null) {
            throw new InstantiationException();
        } else {
            setScanCashierNumber(
                prop.getProperty("ScanCashierNumber", "no").equals("yes"));

            posTerminalApplication = this;
        }
    }

    public static String getVersion() {
        return version;
    }

    /**
     * 纪录高权限用户. Added by lxf 2003-02-12
     */
    public static String getNewCashierID() {
        return newCashierID;
    }

    public static void setNewCashierID(String id) {
        newCashierID = id;
    }

    public static boolean ischeck = false;
    public void run() {
        boolean isRecalculating = false;
        if (args.length > 0 && args[0].equalsIgnoreCase("true"))
            isRecalculating = true;
            
        if (isRecalculating) {
            boolean b = getRecalParameter();
            if(!b) 
                System.exit(0);
            ischeck = true;
            recalcFromTransactions();
        } else {
            initialize();
        }
    }

    private void createPeripherals() {
        // MSR
        try {
            MSR msr = posPeripheralHome.getMSR();
            msr.claim(0);
            msr.setDeviceEnabled(true);
            msr.addDataListener(posPeripheralHome);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage("No MSR presented.");
        } catch (JposException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        // Scanner
        try {
            Scanner scanner = posPeripheralHome.getScanner();
            scanner.claim(0);
            scanner.setDeviceEnabled(true);
            scanner.setDataEventEnabled(true);
            scanner.setDecodeData(true);
            scanner.addDataListener(posPeripheralHome);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage("No Scanner presented.");
        } catch (JposException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        // Keylock
        try {
            Keylock keylock = posPeripheralHome.getKeylock();
            keylock.setDeviceEnabled(true);
            keylock.addStatusUpdateListener(posPeripheralHome);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage("No Keylock presented.");
        } catch (JposException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        // LineDisplay
        try {
            if (!posPeripheralHome.getLineDisplay().getClaimed()) {
                posPeripheralHome.getLineDisplay().claim(JposConst.JPOS_FOREVER);
            }
            posPeripheralHome.getLineDisplay().setDeviceEnabled(true);
            posPeripheralHome.getLineDisplay().clearText();
            String str = prop.getProperty("welcomeMessage");
            if (str != null)
                posPeripheralHome.getLineDisplay().displayText(str, 1);
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage("No LineDisplay presented.");
        } catch (JposException ex) {
            CreamToolkit.logMessage(ex.toString());
        }
    }

    public void layoutUIBeans() {
        if (screenBanner != null) {
            screenBanner.setBounds(
                EPOSBackground.getInstance().screenBannerX + 2,
                EPOSBackground.getInstance().screenBannerY,
                EPOSBackground.getInstance().screenBannerW - 2,
                EPOSBackground.getInstance().screenBannerH - 2);
            screenBanner.repaint();
        }
        if (payingPane != null) {
            payingPane.setBounds(
                EPOSBackground.getInstance().payingBannerX + 2,
                EPOSBackground.getInstance().payingBannerY,
                EPOSBackground.getInstance().payingBannerW - 2,
                EPOSBackground.getInstance().payingBannerH);
            payingPane.repaint();
        }
        if (payingPane1 != null) {
            payingPane1.setBounds(
                EPOSBackground.getInstance().payingBanner1X + 2,
                EPOSBackground.getInstance().payingBanner1Y,
                EPOSBackground.getInstance().payingBanner1W - 2,
                EPOSBackground.getInstance().payingBanner1H);
            payingPane1.repaint();
        }
        if (payingPane2 != null) {
            payingPane2.setBounds(
                EPOSBackground.getInstance().payingBanner2X + 2,
                EPOSBackground.getInstance().payingBanner2Y,
                EPOSBackground.getInstance().payingBanner2W - 2,
                EPOSBackground.getInstance().payingBanner2H);
            payingPane2.repaint();
        }
        if (itemList != null) {
            itemList.setBounds(
                EPOSBackground.getInstance().itemListX,
                EPOSBackground.getInstance().itemListY,
                EPOSBackground.getInstance().itemListW,
                EPOSBackground.getInstance().itemListH);
            dacViewer.setBounds(
                EPOSBackground.getInstance().itemListX,
                EPOSBackground.getInstance().itemListY,
                EPOSBackground.getInstance().itemListW,
                EPOSBackground.getInstance().itemListH);
            itemList.repaint();
        }
        if (informationIndicator != null) {
            informationIndicator.setBounds(
                EPOSBackground.getInstance().informationIndicatorX,
                EPOSBackground.getInstance().informationIndicatorY,
                EPOSBackground.getInstance().informationIndicatorW,
                EPOSBackground.getInstance().informationIndicatorH);
            informationIndicator.repaint();
        }
        if (warningIndicator != null) {
            warningIndicator.setBounds(
                EPOSBackground.getInstance().warningIndicatorX,
                EPOSBackground.getInstance().warningIndicatorY,
                EPOSBackground.getInstance().warningIndicatorW,
                EPOSBackground.getInstance().warningIndicatorH);
            warningIndicator.repaint();
        }
        if (messageIndicator != null) {
            messageIndicator.setBounds(
                EPOSBackground.getInstance().messageIndicatorX,
                EPOSBackground.getInstance().messageIndicatorY,
                EPOSBackground.getInstance().messageIndicatorW,
                EPOSBackground.getInstance().messageIndicatorH);
            messageIndicator.repaint();
        }
        if (connectMsgBanner != null) {
            connectMsgBanner.setBounds(
                EPOSBackground.getInstance().connectMessageX,
                EPOSBackground.getInstance().connectMessageY,
                EPOSBackground.getInstance().connectMessageW,
                EPOSBackground.getInstance().connectMessageH);
            connectMsgBanner.repaint();
        }
        if (pageMsgBanner != null) {
            pageMsgBanner.setBounds(
                EPOSBackground.getInstance().pageMessageX,
                EPOSBackground.getInstance().pageMessageY,
                EPOSBackground.getInstance().pageMessageW,
                EPOSBackground.getInstance().pageMessageH);
            pageMsgBanner.repaint();
        }
        if (productInfoBanner != null) {
            productInfoBanner.setBounds(
                EPOSBackground.getInstance().productInfoX + 2,
                EPOSBackground.getInstance().productInfoY,
                EPOSBackground.getInstance().productInfoW - 2,
                EPOSBackground.getInstance().productInfoH - 2);
            productInfoBanner.repaint();
        }
        if (totalAmountBanner != null) {
            totalAmountBanner.setBounds(
                EPOSBackground.getInstance().subtotalX + 3,
                EPOSBackground.getInstance().subtotalY,
                EPOSBackground.getInstance().subtotalW - 9,
                EPOSBackground.getInstance().subtotalH - 6);
            totalAmountBanner.repaint();
        }
        if (screenBanner2 != null) {
            screenBanner2.setBounds(
                EPOSBackground.getInstance().screenBanner2X,
                EPOSBackground.getInstance().screenBanner2Y,
                EPOSBackground.getInstance().screenBanner2W,
                EPOSBackground.getInstance().screenBanner2H);
            screenBanner2.repaint();
        }
    }

    private void initialize() {
        if (debug)
            System.out.println("PAINT> Mainframe initialize()...");

        CreamToolkit.logMessage("POS startups... (version " + version + ")");

        // determine frame size
        //resizeFrame_left();
        resizeFrame();

        try {
            String cursor = prop.getProperty("ShowMouseCursor", "no");
            Cursor c;
            if (cursor.equalsIgnoreCase("yes"))
                c = new Cursor(Cursor.DEFAULT_CURSOR);
            else
                c = Toolkit.getDefaultToolkit().createCustomCursor(
                    Toolkit.getDefaultToolkit().createImage(new byte[0]),
                    new Point(),
                    "");

            setTitle(prop.getProperty("ApplicationTitle", "ePOS By Hongyuan Software"));
            setCursor(c);
            setLayout(new BorderLayout());

            // Create background panel
            backgroundPanel = EPOSBackground.getInstance();
            backgroundPanel.setEnablePainting(true);
            backgroundPanel.setLayout(null);
            add(backgroundPanel, BorderLayout.CENTER);

            // "CreateButtonPanel"
            String createButtonPanel = prop.getProperty("CreateButtonPanel", "no");
            if (createButtonPanel.equalsIgnoreCase("yes")) {
                add(createButtonPanel(), BorderLayout.SOUTH);
            }

            //backgroundPanel.setVisible(true);
            setVisible(true);
            waitForEPOSBackgroundPaint();
            if (debug)
                System.out.println("PAINT> EPOSBackround painted.");

            // Creating some UIBeans...
            popupMenuPane = PopupMenuPane.getInstance();
            popupMenuPane.setCursor(c);
            popupMenuPane.centerPopupMenu();
            //gllg
            popupMenuPane1 = PopupMenuPane1.getInstance();
            popupMenuPane1.setCursor(c);
            popupMenuPane1.centerPopupMenu();
            //lineItemIndicator = new LineItemIndicator();
            //lineItemIndicator.setCursor(c);
            messageIndicator = new Indicator();
            messageIndicator.setCursor(c);
            warningIndicator = new Indicator(Color.red);
            warningIndicator.setCursor(c);
            informationIndicator = new Indicator(Indicator.SCROLL_MARQUEE2);
            informationIndicator.setCursor(c);
            itemList = new ItemList();
            itemList.setCursor(c);
            dacViewer = new DacViewer();
            dacViewer.setCursor(c);
            signOffForm = SignOffForm.getInstance();
            signOffForm.setCursor(c);
            systemInfo = new SystemInfo();

            // Create current Transaction object
            currentTransaction = Transaction.createCurrentTransaction();

            // Create POSPeripheralHome
            posPeripheralHome = POSPeripheralHome.getInstance();

            // Create POSButtonHome
            posButtonHome = POSButtonHome.getInstance();

            // Preparing peripherals...
            createPeripherals();

            // ScreenBanner
            systemInfo.setTransaction(currentTransaction);
            currentTransaction.addTransactionListener(systemInfo);
            screenBanner =
                new ScreenBanner(
                    EPOSBackground.getInstance().getTextBackgroundColor(),
                    Color.blue.darker().darker(),
                    Color.black);
            screenBanner.setCursor(c);
            screenBanner.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(screenBanner);
            backgroundPanel.add(screenBanner);

            // ItemList
            itemList.setSelectionMode(ItemList.SINGLE_SELECTION);
            currentTransaction.addTransactionListener(itemList);
            itemList.setTransaction(currentTransaction);
            backgroundPanel.add(itemList);
            backgroundPanel.add(dacViewer);

            // Indicators
            backgroundPanel.add(messageIndicator);
            backgroundPanel.add(warningIndicator);
            backgroundPanel.add(informationIndicator);

            //Bruce/20030317/ For Familymart: add "DontShowLicenseInfo"
            String d = prop.getProperty("DontShowLicenseInfo");
            if (d != null && d.equalsIgnoreCase("yes"))
                informationIndicator.setMessage("");
            else
                informationIndicator.setMessage(res.getString("CompanyTitle"));

            new Thread(messageIndicator).start();
            new Thread(warningIndicator).start();
            new Thread(informationIndicator).start();

            // Area 6 connect message
            connectMsgBanner =
                new Banner(
                    CreamToolkit.getConfigurationFile("connectMsgBanner"),
                    EPOSBackground.getInstance().getColor6(),
                    Color.black,
                    Color.black);
            connectMsgBanner.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(connectMsgBanner);
            connectMsgBanner.setCursor(c);
            backgroundPanel.add(connectMsgBanner);

            // Area 11 page & time messages
            pageMsgBanner =
                new Banner(
                    CreamToolkit.getConfigurationFile("pageMsgBanner"),
                    EPOSBackground.getInstance().getColor4(),
                    Color.black,
                    Color.black);
            pageMsgBanner.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(pageMsgBanner);
            pageMsgBanner.setCursor(c);
            backgroundPanel.add(pageMsgBanner);

            // productinfo message
            productInfoBanner =
                new ProductInfoBanner(
                    CreamToolkit.getConfigurationFile("productInfoBanner"),
                    EPOSBackground.getInstance().getTextBackgroundColor(),
                    Color.black,
                    Color.black);
            productInfoBanner.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(productInfoBanner);
            productInfoBanner.setCursor(c);
            backgroundPanel.add(productInfoBanner);

            // Area 8 total message
            totalAmountBanner =
                new TotalAmountBanner(
                    CreamToolkit.getConfigurationFile("totalAmountBanner"),
                    EPOSBackground.getInstance().getColor8(),
                    Color.black,
                    Color.black);
            totalAmountBanner.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(totalAmountBanner);
            totalAmountBanner.setCursor(c);
            backgroundPanel.add(totalAmountBanner);

            // screenbanner2
            screenBanner2 =
                new Banner(
                    CreamToolkit.getConfigurationFile("screenBanner2"),
                    EPOSBackground.getInstance().getColor8(),
                    Color.blue.darker().darker(),
                    Color.black);
            screenBanner2.setSystemInfo(systemInfo);
            systemInfo.addSystemInfoListener(screenBanner2);
            screenBanner2.setCursor(c);
            backgroundPanel.add(screenBanner2);
            
            // PayingPanes
            payingPane = new PayingPane();
            payingPane.setCursor(c);
            payingPane.setTransaction(currentTransaction);
            backgroundPanel.add(payingPane);
            systemInfo.addSystemInfoListener(payingPane);

            payingPane1 = new PayingPaneBanner(CreamToolkit.getConfigurationFile("payingPane1"));
            payingPane1.setCursor(c);
            payingPane1.setTransaction(currentTransaction);
            backgroundPanel.add(payingPane1);

            payingPane2 = new PayingPaneBanner(CreamToolkit.getConfigurationFile("payingPane2"));
            payingPane2.setCursor(c);
            payingPane2.setTransaction(currentTransaction);
            backgroundPanel.add(payingPane2);
            payingPane1.setVisible(false);
            payingPane2.setVisible(false);

            //currentTransaction.addTransactionListener(lineItemIndicator);
            //lineItemIndicator.repaint();

            // Create StateMachine
            setBeginState(true);
            stateMachine = StateMachine.getInstance();

            // Enable event forward
            posButtonHome.setEventForwardEnabled(true);
            posPeripheralHome.setEventForwardEnabled(true);
            stateMachine.setEventProcessEnabled(true);

            layoutUIBeans();

            screenBanner.setVisible(true);
            itemList.setVisible(true);
            dacViewer.setVisible(false);
            payingPane1.setVisible(false);
            payingPane2.setVisible(false);
            productInfoBanner.setVisible(true);
            totalAmountBanner.setVisible(true);
            messageIndicator.setVisible(true);
            warningIndicator.setVisible(true);
            informationIndicator.setVisible(true);
            informationIndicator.repaint();

            CreamPrinter.getInstance().setPrintEnabled(
                prop.getProperty("ReceiptPrint", "yes").equalsIgnoreCase("yes"));
            CreamPrinter.getInstance().printFirstReceipt();
            setKeyPosition(5);

        } catch (ConfigurationNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
        }

        // setting AppFrame
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                try {
                    dog.stop();
                    Client.getInstance().closeConnection();
                    posPeripheralHome.getLineDisplay().clearText();
                    posPeripheralHome.getLineDisplay().release();
                    posPeripheralHome.getLineDisplay().close();
                    CreamToolkit.releaseAll();
                    CreamToolkit.closeLog();
                } catch (Exception ex) {
                }
                System.exit(0);
            }
        });

        final Thread thread = new ShowTime(screenBanner2);
        thread.start();

        //Bruce/2003-08-10
        boolean pass = true;
        if (pass) {
            String dontNeedTurnKeyAtStart = prop.getProperty("DontNeedTurnKeyAtStart", "no");
            if (dontNeedTurnKeyAtStart.equalsIgnoreCase("yes")) {
                stateMachine.processEvent(new StatusUpdateEvent(new FakeKeylock(1), 0));
                stateMachine.processEvent(new StatusUpdateEvent(new FakeKeylock(2), 0));
            }
        }
    }

    private Panel createButtonPanel() {
        Panel buttonPanel = new Panel();
        buttonPanel.setLayout(new GridLayout(2, 8));

        createCommandButton(buttonPanel, "ESC 清除／回退", new ClearButton(0, 0, 0, "Clear"));
        createCommandButton(buttonPanel, "F1 暂停交易", new EmptyButton(0, 0, 0, "F1"));
        createCommandButton(buttonPanel, "F2 销售模式", new EmptyButton(0, 0, 0, "F2"));
        createCommandButton(buttonPanel, "F3 训练模式", new EmptyButton(0, 0, 0, "F3"));
        createCommandButton(buttonPanel, "F4 日结／盘点", new EmptyButton(0, 0, 0, "F4"));
        createCommandButton(buttonPanel, "F5 设置", new EmptyButton(0, 0, 0, "F6")); //系统设置的keylock pos is 6
        createCommandButton(buttonPanel, "Ｓ 营业员", new SalesmanButton(0, 0, 0, "Salesman"));
        createCommandButton(buttonPanel, "Ｋ 支票", new PaymentButton(0, 0, 0, "Check", "01"));
        createCommandButton(buttonPanel, "Ｃ 收银员作业", new CheckInOutButton(0, 0, 0, "Cashier"));

        createCommandButton(buttonPanel, "Ｑ 查价", new InquiryButton(0, 0, 0, "Inquiry"));
        createCommandButton(buttonPanel, "Ｏ 变价", new OverrideAmountButton(0, 0, 0, "Override"));
        createCommandButton(buttonPanel, "Ｐ 折扣率", new DiscountRateButton(0, 0, 0, "Disc"));
        createCommandButton(buttonPanel, "Ｎ 折扣号", new DiscountRateIDButton(0, 0, 0, "DiscID"));
        createCommandButton(buttonPanel, "／ 更正", new RemoveButton(0, 0, 0, "Remove"));
        createCommandButton(buttonPanel, "＊ 数量", new QuantityButton(0, 0, 0, "Qty"));
        createCommandButton(buttonPanel, "＋ 小计", new AgeLevelButton(0, 0, 0, "Nan", "1"));
        createCommandButton(buttonPanel, "Ｒ 营业日报表", new DailyReportButton(0, 0, 0, "SalesReport"));
        createCommandButton(buttonPanel, "Enter 确认/现金", new EnterButton(0, 0, 0, "Enter"));
        return buttonPanel;
    }

    private class FakeKeylock extends Keylock {
        int keyPos;
        FakeKeylock(int keyPos) {
            this.keyPos = keyPos;
        }
        public int getKeyPosition() {
            return keyPos;
        }
    }

    private void createCommandButton(Panel buttonPanel, String label, final POSButton button) {
        Color bgColor = new Color(165 + 30, 178 + 30, 255);
        Button cmdButton = new Button(label);
        cmdButton.setFont(new Font("宋体", Font.PLAIN, 14));
        cmdButton.setBackground(bgColor);
        cmdButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                StateMachine stateMachine = StateMachine.getInstance();
                if (button.getLabel().startsWith("F")) {
                    int keyPos = Integer.parseInt(button.getLabel().substring(1));
                    stateMachine.processEvent(new StatusUpdateEvent(
                        new FakeKeylock(keyPos), 0));
                } else {
                    stateMachine.processEvent(new POSButtonEvent(button));
                }
            }
        });
        buttonPanel.add(cmdButton);
    }

    class ShowTime extends Thread {
        Banner screenBanner2 = null;
        public ShowTime(Banner screenBanner2) {
            this.screenBanner2 = screenBanner2;
        }
        public void run() {
            while (true) {
                if (screenBanner2 != null)
                    screenBanner2.repaint();
                try {
                    sleep(1000 * 60);
                } catch (InterruptedException ie) {
                }
            }
        }
    }

    /**
     * Wait for EPOSBackground finished its first paint().
     */
    private void waitForEPOSBackgroundPaint() {
        synchronized (EPOSBackground.getInstance()) {
            try {
                EPOSBackground.getInstance().wait(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setCurrentCashierNumber(String number) {
        cashierNumber = number;
    }

    public String getCurrentCashierNumber() {
        return cashierNumber;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction curTransaction) {
        currentTransaction = curTransaction;
    }

    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    public POSButtonHome getPOSButtonHome() {
        return posButtonHome;
    }

    public POSPeripheralHome getPOSPeripheralHome() {
        return posPeripheralHome;
    }

    /*public ButtonPanel getButtonPanel() {
        return buttonPanel;
    }*/

    public ScreenBanner getScreenBanner() {
        return screenBanner;
    }

    //public LineItemIndicator getLineItemIndicator() {
    //    return lineItemIndicator;
    //}

    public Indicator getMessageIndicator() {
        return messageIndicator;
    }

    public Indicator getWarningIndicator() {
        return warningIndicator;
    }

    public Indicator getInformationIndicator() {
        return informationIndicator;
    }

    public ItemList getItemList() {
        return itemList;
    }

    public DacViewer getDacViewer() {
        return dacViewer;
    }

    public PayingPane getPayingPane() {
        return payingPane;
    }

    public Frame getAppFrame() {
        return this;
    }

    public PopupMenuPane getPopupMenuPane() {
        return popupMenuPane;
    }

    public PopupMenuPane1 getPopupMenuPane1() {
        return popupMenuPane1;
    }

    public SignOffForm getSignOffForm() {
        return signOffForm;
    }

    public void setBuyerNumberPrinted(boolean buyerNumberPrinted) {
        this.buyerNumberPrinted = buyerNumberPrinted;
    }

    public boolean getBuyerNumberPrinted() {
        return buyerNumberPrinted;
    }

    public boolean getTrainingMode() {
        return trainingMode;
    }

    public void setTraningMode(boolean training) {
        trainingMode = training;
        if (training) {
            CreamPrinter.getInstance().setPrintEnabled(false);
        } else {
            boolean enablePrint = "yes".equalsIgnoreCase(prop.getProperty("ReceiptPrint", "yes"));
            CreamPrinter.getInstance().setPrintEnabled(enablePrint);
        }
    }

    public Panel getTotalMsgPanel() {
        return totalMsgPanel;
    }

    private void resizeFrame_left() {
        int width, height;
       setSize(640, 520);
       setLocation(-640,0);
  }
    private void resizeFrame() {
        int width, height;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        width = (int)screenSize.getWidth();
        height = (int)screenSize.getHeight();
        if (System.getProperties().get("os.name").toString().indexOf("Windows") != -1) {
        //backup window setting by gllg
//            width -= 20;
//            height -= 60;
//            setSize(width, height);
//            setLocation((screenSize.width - width) / 2, (screenSize.height - height) / 2 - 20);
            //for double screen
            setSize(640, 520);
//            setLocation(-640,-27);
//            setLocation(640,-27);
            setLocation(0, 0);
        } else {
            setSize(width, height);
            setLocation((screenSize.width - width) / 2, (screenSize.height - height) / 2);
        }
//        if (screenSize.getWidth() == 1024) {
//            width = 1024 - 20; //650;
//            height = 768 - 80; //490;
//        } else {
//            width = (int)screenSize.getWidth();
//            height = (int)screenSize.getHeight();
//        }
//        setSize(width, height);
//        setLocation((screenSize.width - width) / 2, (screenSize.height - height) / 2);
    }

    public void setKeyState(boolean keyState) {
        this.keyState = keyState;
    }

    public boolean getkeyState() {
        return keyState;
    }

    public boolean getReturnItemState() {
        return returnItemState;
    }

    public void setReturnItemState(boolean returnItemState) {
        this.returnItemState = returnItemState;
    }

    public boolean getTransactionEnd() {
        return transactionEnd;
    }

    public void setTransactionEnd(boolean transactionEnd) {
        //System.out.println("setTransactionEnd(" + transactionEnd + ")");
        this.transactionEnd = transactionEnd;
        //if (transactionEnd)
        //    setReturnItemState(false);
    }

    public int getKeyPosition() {
        return keyPosition;
    }

    public void setKeyPosition(int keyPosition) {
        stateMachine.setTurnable(0);
        this.keyPosition = keyPosition;
    }

    public boolean getBeginState() {
        return beginState;
    }

    public void setBeginState(boolean beginState) {
        this.beginState = beginState;
    }

    private boolean enabledPopupMenu = false;
    public boolean getEnabledPopupMenu() {
        return enabledPopupMenu;
    }

    public void setEnabledPopupMenu(boolean enabledPopupMenu) {
        this.enabledPopupMenu = enabledPopupMenu;
    }

    public boolean getIsAllReturn() {
        return isAllReturn;
    }

    public void setIsAllReturn(boolean isAllReturn) {
        this.isAllReturn = isAllReturn;
    }

    public boolean getChecked() {
        return checked;
    }

    public boolean getPayingPaneVisible() {
        return payingPaneVisible;
    }

    public void setPayingPaneVisible(boolean visible) {
        payingPaneVisible = visible;

        if (visible) {

//            this.backgroundPanel.setEnablePainting(false);
//            new Thread(new Runnable() {
//                public void run() {
//                    try {
//                        //System.out.println("\nEPOSBackground wait for paint...");
//                        synchronized (EPOSBackground.getInstance().waitForDrawing) {
//                            EPOSBackground.getInstance().waitForDrawing.wait();
//                            backgroundPanel.setEnablePainting(true);
//                            backgroundPanel.repaint();
//                        }
//                    } catch (InterruptedException e) {
//                    }
//                }
//            }).start();
//            Thread.yield();

            screenBanner.setVisible(false);
            itemList.setVisible(false);
            productInfoBanner.setVisible(false);
            totalAmountBanner.setVisible(false);
            payingPane1.setVisible(true);
            payingPane2.setVisible(true);
        } else {
            payingPane1.setVisible(false);
            payingPane2.setVisible(false);
            screenBanner.setVisible(true);
            itemList.setVisible(true);
            productInfoBanner.setVisible(true);
            totalAmountBanner.setVisible(true);
            repaint();
            backgroundPanel.repaint();
        }

        repaint();
        backgroundPanel.repaint();
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean getScanCashierNumber() {
        return scanCashierNumber;
    }

    public void setScanCashierNumber(boolean scanCashierNumber) {
        this.scanCashierNumber = scanCashierNumber;
    }

    public BigDecimal getReturnQty() {
        return returnQty;
    }

    public void setReturnQty(BigDecimal returnQty) {
        this.returnQty = returnQty;
    }

    /**
     * ePOS Entry point.
     */
    public static void main(String[] args) {

        POSTerminalApplication.args = args;

        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("-version")) {
                System.out.println("Version: " + getVersion());
                return;
            }
        }
        try {
            //Meyer/2002-02-11/ Modified the getting of ServerName 
            String serverName =
                CreamProperties.getInstance().getProperty("SCIPAddress", Server.INLINE_SERVER_DOMAIN_NAME);
            if (serverName.equals(""))
                serverName = Server.INLINE_SERVER_DOMAIN_NAME;

            POSTerminalApplication app = new POSTerminalApplication();
            DacTransfer.getInstance();

            // startup a inline client (singleton)
            Client clt = new Client(serverName, Server.INLINE_SERVER_TCP_PORT);
            clt.setName("Client");
//            
            dog.put(clt);
            dog.start();
            
            app.run();

        } catch (InstantiationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    //ZhaoHong 2003-08-15
    //最近一段时间交易的分段信息　按照EODCNT分段
    private static ArrayList lastestTrans = new ArrayList();

    private static String bnumber = "";
    private static String enumber = "";
    private static String shiftbnumber = "";
    private static String shiftenumber = "";
    public static String args[];
    private ShiftReport shift = null;
    private ZReport z = null;
    private java.util.Date busiDate = new java.util.Date();    
    private java.util.Date d = new java.util.Date();

    private boolean getRecalParameter() {
        boolean rtnValue = false;
        
        String message = "请选择重算的交易区间或输入 'q' 退出";
        String message2 = "最近一段时间交易情况如下:";
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String cmd = "";
        if (getLastestTransactions()) {
            HashMap mp = null;
            String msg = "";
            System.out.println("\n" + message2 + "\n");    
            for (int i = 0; i < lastestTrans.size(); i++) {
                mp = (HashMap)lastestTrans.get(i);
                msg = "" + (i + 1) + ".  "  + mp.get("BTranNo") 
                    + "(" + mp.get("BTranTime") + ")"
                    + "  --  " + mp.get("ETranNo")
                    + "(" + mp.get("ETranTime") + ")"
                    + "  Z = " + mp.get("ZNumber");
                System.out.println(msg);    
            }

            while (true) {
                System.out.println("\n" + message + "\n");                    
                
                System.out.print(">");
                
                try {
                    cmd = reader.readLine();
                    
                    if(cmd.equalsIgnoreCase("q")) {
                        System.out.println("Bye");
                        break;
                    } else if (Integer.parseInt(cmd) -1 < lastestTrans.size()) {
                        mp =  (HashMap)lastestTrans.get(Integer.parseInt(cmd) -1);
                        bnumber = mp.get("BTranNo").toString();  
                        enumber = mp.get("ETranNo").toString();                          

                        //录入会计日期
                        while (true) {
                            String message3 = "请输入会计日期:";
                            
                            System.out.println(message3);                    
                            System.out.print(">");
                            
                            try {
                                cmd = reader.readLine();
                                if(cmd.equalsIgnoreCase("q")) {
                                    System.out.println("Bye");
                                    break;     
                                } else if (cmd == null) {
                                    busiDate = new java.util.Date();    
                                } else 
                                    busiDate = java.sql.Date.valueOf(cmd);
                                break;
                            } catch (IllegalArgumentException e) {
                                continue;
                            } catch (IOException e) {
                                e.printStackTrace();
                                break;
                            }
                        }
                                                  
                        rtnValue = true;
                        break;    
                    } else {
                        continue;
                    }
                } catch (IOException ioe){
                    ioe.printStackTrace();
                }catch (NumberFormatException e) {
                    continue;
                }
            }
                
        }
        
        return rtnValue;
    }

	//取最近一段时间的交易情况
	private boolean getLastestTransactions() {
		 boolean rtnValue = false;
		 if (lastestTrans.size() > 0) 
			return true;
         
		 try{
			Connection con = CreamToolkit.getPooledConnection();
			Statement stmt = con.createStatement();
			//取最近15个交易日
			int baseZNumber = ZReport.getCurrentZNumber() - 40;
			if (baseZNumber < 0) 
				baseZNumber = 0;
			String sql = "select TMTRANSEQ, EODCNT, SYSDATE from tranhead "
					   + "where EODCNT >= " + baseZNumber + " "
					   + "order by EODCNT desc, SYSDATE";
            
			ResultSet rst = stmt.executeQuery(sql);
            
			int a = -1;
			HashMap hm = null;
			while (rst.next()) {
			   int minTranNo = 0, maxTranNo = 0;
			   if (a == -1 || a != rst.getInt("EODCNT")) {
					hm = new HashMap();
					lastestTrans.add(hm);
                    
					a = rst.getInt("EODCNT");
					minTranNo = rst.getInt("TMTRANSEQ");
					maxTranNo = minTranNo;
					hm.put("ZNumber", new Integer(a));
					hm.put("BTranNo", new Integer(minTranNo));
					hm.put("ETranNo", new Integer(maxTranNo));
					hm.put("BTranTime", rst.getObject("SYSDATE"));
					hm.put("ETranTime", rst.getObject("SYSDATE"));
				} else {
//					  if (rst.getInt("TMTRANSEQ") < minTranNo) {
//						  minTranNo = rst.getInt("TMTRANSEQ");
//						  hm.put("BTranNo", new Integer(minTranNo));
//						  hm.put("BTranTime", rst.getObject("SYSDATE"));
//					  }
                    
//					  if (rst.getInt("TMTRANSEQ") > maxTranNo) {
					maxTranNo = rst.getInt("TMTRANSEQ");
					hm.put("ETranNo", new Integer(maxTranNo));
					hm.put("ETranTime", rst.getObject("SYSDATE"));
//					  }
				}

			} // end while
			rst.close();
			rst = null;
			con.close();
			con = null;
			rtnValue = true;
            
		 } catch (SQLException e) {
			 e.printStackTrace();
		 } catch (Exception e) {
			 e.printStackTrace();
		 }

		 return rtnValue;
	}

	public void recalcFromTransactions() {

		//  initial
		try {
			screenBanner = new ScreenBanner();
			messageIndicator = new Indicator();
			warningIndicator = new Indicator();
			informationIndicator = new Indicator(2);
			itemList = new ItemList();
			popupMenuPane = PopupMenuPane.getInstance();
			systemInfo = new SystemInfo();
			//payingPane = new PayingPane();
		} catch (ConfigurationNotFoundException e) {
			//e.printStackTrace(CreamToolkit.getLogger());
			System.out.println(e);
		}
		currentTransaction = Transaction.createCurrentTransaction();
		//posPeripheralHome = POSPeripheralHome.getInstance();
		//posButtonHome = POSButtonHome.getInstance();
		stateMachine = StateMachine.getInstance();

		System.out.println();
		System.out.println("#       this is information of parameters :");
		System.out.println("        enclosing   : " + bnumber + " - " + enumber);
        CreamToolkit.logMessage("Recalc report: " + bnumber + " - " + enumber);

		//  begin calculate
		System.out.println();
		System.out.println("# begin ......");
//		d = CreamToolkit.getInitialDate();
		java.util.Date bd = new java.util.Date();
		java.util.Date ed = new java.util.Date();
		java.util.Date shiftbd = new java.util.Date();
		java.util.Date shifted = new java.util.Date();
        DateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
		String cnumber = "";
		String zbnumber = "";
		String zenumber = "";
        
		//property 表中设置的系统最大交易序号
		int maxTranNumber = Transaction.getMaxTranNumberInProperty();
		boolean bn = Integer.parseInt(bnumber) <= Integer.parseInt(enumber);
        //gllg
        String UpdPropWhenRecalc = CreamProperties.getInstance().getProperty("UpdPropWhenRecalc", "no");
        boolean isUpdProperty = "yes".equalsIgnoreCase(UpdPropWhenRecalc) ||
            "true".equalsIgnoreCase(UpdPropWhenRecalc);

        for (int i = Integer.parseInt(bnumber); i <= (bn?Integer.parseInt(enumber):Integer.parseInt(enumber) + maxTranNumber); i++) {

			//  get current transaction
			System.out.println("query " + ((i <= maxTranNumber)?i + "":(i-maxTranNumber) + ""));
			currentTransaction = Transaction.queryByTransactionNumber((i <= maxTranNumber)?i + "":(i-maxTranNumber) + "");

			//  check dealtype2 = '8' (sign on)
			//                  = '9' (sign off)
			//                  = 'D' (eod)
			//处理跳号
            if (currentTransaction == null) 
                continue;
            
            String dealtype2 = currentTransaction.getDealType2();

			if (i == Integer.parseInt(bnumber)) {
				bd = currentTransaction.getSystemDateTime();
				zbnumber = (i <= maxTranNumber)?i + "":(i-maxTranNumber) + "";

				try {
					//Create new ZReport
					z = new ZReport();
					//z.setSequenceNumber(currentTransaction.getSignOnNumber());
					z.setSequenceNumber(currentTransaction.getZSequenceNumber());
					z.setBeginSystemDateTime(new java.util.Date());
					z.setEndSystemDateTime(CreamToolkit.getInitialDate());
					//z.setBeginTransactionNumber(new Integer(100));
					//z.setEndTransactionNumber(new Integer(100));
                    //gllg
                    if (isUpdProperty) {
                        z.setBeginTransactionNumber(new Integer(bnumber), true);
                        z.setEndTransactionNumber(new Integer(enumber), true);
                    } else {
                        z.setBeginTransactionNumber(new Integer(bnumber), false);
                        z.setEndTransactionNumber(new Integer(enumber), false);
                    }
                    if (isUpdProperty) {
                        z.setBeginInvoiceNumber("0", true);
                        z.setEndInvoiceNumber("0", true);
                    } else {
                        z.setBeginInvoiceNumber("0", false);
                        z.setEndInvoiceNumber("0", false);
                    }

                    //prop.getProperty("NextInvoiceNumber"));

					//prop.getProperty("NextInvoiceNumber"));
					z.setMachineNumber(prop.getProperty("TerminalPhysicalNumber"));

					//gllg
					z.deleteByPrimaryKey();
					z.insert();
					

					//System.out.println(b + " = " + shift.getSequenceNumber() + " <<<>>> " + shift.getZSequenceNumber());
					z = (ZReport)z.queryByPrimaryKey();
                    
					//delete existed DepSales
					Iterator itr = DepSales.queryBySequenceNumber(currentTransaction.getZSequenceNumber());
                    
					if (itr != null) {
						while (itr.hasNext()) {
							try {
							   ((DepSales)itr.next()).deleteByPrimaryKey(); 
							} catch (Exception depException) {
                    
							}

						}
					}
                    
					//Create DepSales
					DepSales.createDepSales(currentTransaction.getZSequenceNumber().intValue());
                    
					//delete existed DaishouSales  代售
					Iterator iter = DaishouSales.getCurrentDaishouSales(currentTransaction.getZSequenceNumber().intValue());
					while (iter !=null && iter.hasNext()) {
						try {
							((DaishouSales)iter.next()).deleteByPrimaryKey();
						}catch (Exception en) {
						}
					}
					//create new DaishouSales
					DaishouSales.createDaishouSales(currentTransaction.getZSequenceNumber().intValue());
					Iterator iterator = DaishouSales.getCurrentDaishouSales(currentTransaction.getZSequenceNumber().intValue());
					while (iterator !=null && iterator.hasNext()) {
						try {
							DaishouSales dss = (DaishouSales)iterator.next();
                            dss.setAccountDate(busiDate);
							dss.update();
						}catch (Exception en) {
						}
					}                  
                    
				} catch (InstantiationException e) {
					System.out.println(e);
					System.exit(0);
				}

			} else if (i == (bn?Integer.parseInt(enumber):Integer.parseInt(enumber) + maxTranNumber) 
						|| dealtype2.equalsIgnoreCase("D")) {
				ed = currentTransaction.getSystemDateTime();
				zenumber = (i <= maxTranNumber)?i + "":(i-maxTranNumber) + "";
			}

			if (dealtype2.equalsIgnoreCase("8")) {

				shiftbd = currentTransaction.getSystemDateTime();

				try {
					shift = new ShiftReport();
					shift.setSequenceNumber(currentTransaction.getSignOnNumber());
					shift.setZSequenceNumber(currentTransaction.getZSequenceNumber());
					shift.setSignOnSystemDateTime(new java.util.Date());
					shift.setSignOffSystemDateTime(CreamToolkit.getInitialDate());
//					shift.setBeginTransactionNumber(new Integer(100));
//					shift.setEndTransactionNumber(new Integer(100)););
					//gllg
					if (isUpdProperty) {
                        shift.setBeginTransactionNumber(new Integer(bnumber), true);
                        shift.setEndTransactionNumber(new Integer(enumber), true);

                    } else {
                        shift.setBeginTransactionNumber(new Integer(bnumber), false);
                        shift.setEndTransactionNumber(new Integer(enumber), false);

                    }

                    if (isUpdProperty) {
                        shift.setBeginInvoiceNumber("0", true);
                        shift.setEndInvoiceNumber("0", true);
                    } else {
                        shift.setBeginInvoiceNumber("0", false);
                        shift.setEndInvoiceNumber("0", false);
                    }
                    //prop.getProperty("NextInvoiceNumber"));
					//prop.getProperty("NextInvoiceNumber"));
					shift.setMachineNumber(
						prop.getProperty("TerminalPhysicalNumber"));

					//Bruce/20030417
					//借零金额初始值 for 灿坤
					String initialCashInAmount = (String)CreamProperties.getInstance().getProperty("InitialCashInAmount");
					if (initialCashInAmount != null) {
						shift.setCashInAmount(new BigDecimal(initialCashInAmount));
						shift.setCashInCount(new Integer(1));
					}
                    //gllg 
					shift.deleteByPrimaryKey();
					shift.insert();


					//System.out.println(b + " = " + shift.getSequenceNumber() + " <<<>>> " + shift.getZSequenceNumber());
					shift = (ShiftReport)shift.queryByPrimaryKey();

				} catch (InstantiationException e) {
					System.out.println(e);
					System.exit(0);
				}
				shiftbnumber = (i <= maxTranNumber)?i + "":(i-maxTranNumber) + "";

				cnumber = currentTransaction.getCashierNumber();
			} else if (dealtype2.equalsIgnoreCase("9")) {

					shiftenumber = (i <= maxTranNumber)?i + "":(i-maxTranNumber) + "";
					shifted = currentTransaction.getSystemDateTime();
					if (isUpdProperty) {
                        shift.setBeginTransactionNumber(Integer.valueOf(shiftbnumber), true);
                        shift.setEndTransactionNumber(Integer.valueOf(shiftenumber), true);
                    } else {
                        shift.setBeginTransactionNumber(Integer.valueOf(shiftbnumber), false);
                        shift.setEndTransactionNumber(Integer.valueOf(shiftenumber), false);
                    }
                    shift.setAccountingDate(busiDate);
					shift.setSignOnSystemDateTime(shiftbd);
					shift.setSignOffSystemDateTime(shifted);
					shift.setCashierNumber(cnumber);
					//shift.
					//System.out.println(shift.getSequenceNumber() + " <<<>>> " + shift.getZSequenceNumber());
					shift.setUploadState("2");
					if (shift.update()) {
						System.out.println();
						System.out.println("#     this is some information of shift :");
						System.out.println("# shift accdate = " + yyyyMMdd.format(shift.getAccountingDate()));
						System.out.println("# shift number = " + shift.getSequenceNumber());
						System.out.println("# shift eodcnt = " + shift.getZSequenceNumber());
						System.out.println("# shift bnumber = " + shift.getBeginTransactionNumber());
						System.out.println("# shift enumber = " + shift.getEndTransactionNumber());
						System.out.println("# shift upload = " + shift.getUploadState());
						System.out.println(
							"# shift sale amount = "
								+ (shift
									.getNetSalesAmount1()
									.add(
										shift.getNetSalesAmount2().add(
											shift.getNetSalesAmount3().add(shift.getNetSalesAmount4())))));
					} else {
						System.out.println("#   fail fail fail fail fail fail: shift insert");
						System.exit(0);
					}

			} else if (dealtype2.equalsIgnoreCase("D")) {

                    if (isUpdProperty) {
                        z.setBeginTransactionNumber(Integer.valueOf(zbnumber), true);
                    } else {
                        z.setBeginTransactionNumber(Integer.valueOf(zbnumber), false);
                    }
                    System.out.println("zenumber=" + zenumber);

					if (isUpdProperty) {
                        z.setEndTransactionNumber(Integer.valueOf(zenumber), true);
                    } else {
                        z.setEndTransactionNumber(Integer.valueOf(zenumber), false);
                    }
                    z.setBeginSystemDateTime(bd);
					z.setEndSystemDateTime(ed);

                    // 根据用户输入的日期设置会计日期
                    z.setAccountingDate(busiDate);

					z.setCashierNumber(cnumber);
					z.setTransactionCount(
						new Integer(Integer.parseInt(zenumber) - Integer.parseInt(zbnumber) + 1));
					z.setUploadState("2");
					//z.update();
					if (z.update()) {
						System.out.println();
						System.out.println("#     this is some information of z :");
						System.out.println("# z accdate = " + yyyyMMdd.format(z.getAccountingDate()));
						System.out.println("# z eodcnt = " + z.getSequenceNumber());
						System.out.println("# z bnumber = " + z.getBeginTransactionNumber());
						System.out.println("# z enumber = " + z.getEndTransactionNumber());
						System.out.println("# z bdate = " + z.getBeginSystemDateTime());
						System.out.println("# z edate = " + z.getEndSystemDateTime());
						System.out.println("# z upload = " + z.getUploadState());
						System.out.println(
							"# z sale amount = "
								+ (z
									.getNetSalesAmount1()
									.add(
										z.getNetSalesAmount2().add(
											z.getNetSalesAmount3().add(z.getNetSalesAmount4())))));
					} else {
						System.out.println("#   fail fail fail fail fail fail: z insert");
						System.exit(0);
					}
					//Save DepSales   
                    DepSales.updateAll(busiDate);
			}

			//  continue
			try {
				currentTransaction.tran_Processing(true);
				System.out.println(
					"# "
						+ ((i <= maxTranNumber)?i :(i-maxTranNumber)) 
						+ " ("
						+ currentTransaction.getDealType1()
						+ ", "
						+ currentTransaction.getDealType2()
						+ ", "
						+ currentTransaction.getDealType3()
						+ ") --- OK");
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		System.out.println("# finished.");
		System.exit(0);

	}

    public ShiftReport getNewShift() {
        return shift;
    }

    public ZReport getNewZ() {
        return z;
    }

    public java.util.Date getNewDate() {
        return d;
    }

    public PayingPaneBanner getPayingPane1() {
        return payingPane1;
    }

    public PayingPaneBanner getPayingPane2() {
        return payingPane2;
    }

    public void update(Graphics g) {
        paint(g);
    }

    /*	
    public void update(Graphics g) {
    	if (isShowing()) {
    		paint(g);
    	}
    }
    */

    /**
     * 
     * @author ll 2003.04.17
     * input : property.ScaleBarCodeFormat :　2IIIIIIWW.WWWPPP.PPC
     *   开头的数值:前置码   
     *   I:plu编号   
     *   W:商品重量  
     *   P:商品金额
     *   C:检核码 
     *   .:是小数点(在实际条码中不存在)
     * 
     * output : java.util.List
     *    list.get(0) Integer : 条码总长(不包括小数点)
     * 	  list.get(1) String  : 前置码
     *    list.get(2) Integer : plu编号开始位置
     *    list.get(3) Integer : plu编号结束位置
     *    list.get(4) Integer : 商品重量开始位置
     *    list.get(5) Integer : 商品重量结束位置
     *    list.get(6) Integer : 商品重量的小数点位置
     *    list.get(7) Integer : 商品金额开始位置
     *    list.get(8) Integer : 商品金额结束位置
     *    list.get(9) Integer : 商品金额的小数点位置
     */
    public java.util.List getScaleBarCodeFormat() {
        try {
            if (scaleBarCodeFormatList != null
                && !scaleBarCodeFormatList.isEmpty()
                && scaleBarCodeFormatList.size() == 10)
                return scaleBarCodeFormatList;
            scaleBarCodeFormatList = new Vector();
            String scaleBarCodeFormat = prop.getProperty("ScaleBarCodeFormat");
            if (scaleBarCodeFormat == null)
                return null; //Bruce/20030505/ prevent a NullPointerException problem
            String pref = "";
            int length = scaleBarCodeFormat.length();
            int totalLength = 0;

            char flag = scaleBarCodeFormat.charAt(totalLength);
            while (flag <= '9' && flag >= '0' && totalLength < length) {
                pref += String.valueOf(flag);
                totalLength++;
                flag = scaleBarCodeFormat.charAt(totalLength);
            }

            totalLength = 0;
            for (int i = 0; i < length; i++) {
                flag = scaleBarCodeFormat.charAt(i);
                if (flag != '.')
                    totalLength++;
            }

            java.util.List iList = getPosition(scaleBarCodeFormat, 'I');
            java.util.List wList = getPosition(scaleBarCodeFormat, 'W');
            java.util.List pList = getPosition(scaleBarCodeFormat, 'P');

            scaleBarCodeFormatList.add(new Integer(totalLength));
            scaleBarCodeFormatList.add(pref);
            scaleBarCodeFormatList.add(iList.get(1));
            scaleBarCodeFormatList.add(iList.get(2));
            scaleBarCodeFormatList.add(wList.get(1));
            scaleBarCodeFormatList.add(wList.get(2));
            scaleBarCodeFormatList.add(wList.get(3));
            scaleBarCodeFormatList.add(pList.get(1));
            scaleBarCodeFormatList.add(pList.get(2));
            scaleBarCodeFormatList.add(pList.get(3));
        } catch (Exception e) {
            scaleBarCodeFormatList = null;
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return scaleBarCodeFormatList;
    }

    /**
     * 
     * @param source
     * @param flag
     * @return java.util.List
     *     list(0) : flag
     * 	   list(1) : start pos
     * 	   list(2) : end pos
     * 	   list(3) : comma pos
     */
    private java.util.List getPosition(String source, char flag) {
        java.util.List list = new Vector();
        int index = source.indexOf(flag);
        if (index < 0) {
            list.add(String.valueOf(flag));
            list.add(new Integer(0));
            list.add(new Integer(0));
            list.add(new Integer(0));
            return list;
        }

        int index1 = source.indexOf(".");
        int prefCount = 0;
        if (index > index1) {
            while (index1 >= 0) {
                prefCount++;
                index1 = source.substring(index1 + 1, index).indexOf(".");
            }
        }

        boolean has = true;
        int length = 0;
        int i = index;
        int pos = 0;
        while (i < source.length() && has) {
            char tmp = source.charAt(i);
            if (tmp == flag) {
                length++;
            } else if (tmp == '.') {
                pos = length + 1;
            } else {
                has = false;
            }
            i++;
        }
        list.add(String.valueOf(flag));
        list.add(new Integer(index - prefCount));
        list.add(new Integer(index - prefCount + length));
        list.add(new Integer(pos - 1));
        return list;
    }

    public TotalAmountBanner getTotalAmountBanner() {
        return totalAmountBanner;
    }
}
