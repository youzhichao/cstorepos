/*jadclipse*/// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) radix(10) lradix(10) 
// Source File Name:   SystemInfo.java
package hyi.cream;

import hyi.cream.dac.Cashier;
import hyi.cream.dac.Inventory;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Message;
import hyi.cream.dac.Store;
import hyi.cream.dac.Transaction;
import hyi.cream.event.SystemInfoEvent;
import hyi.cream.event.SystemInfoListener;
import hyi.cream.event.TransactionEvent;
import hyi.cream.event.TransactionListener;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.state.InventoryIdleState;
import hyi.cream.state.StateMachine;
import hyi.cream.uibeans.Indicator;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import java.awt.Component;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

// Referenced classes of package hyi.cream:
//            POSTerminalApplication

public class SystemInfo extends Thread implements TransactionListener {

    private static ResourceBundle res = CreamToolkit.GetResource();
    private Transaction trans;
    private SystemInfoListener systemInfoListener;
    private String storeName;
    private String storeNumber;
    private String buyerID;
    private String zNumber;
    private String invoiceNumber;
    private Date businessDate;
    private String terminalID;
    private String transactionNumber;
    private String cashierNumber;
    private String cashierName;
    private String memberNumber;
    private String peiDaNumber;
    private ArrayList listener;
    private static boolean isDaiFu = false;
    private static boolean connected = false;
    private static int inter = -2;
    private static boolean dontShowLicenseInfo;
    private String salesManNumber;
    private String pageNumber;
    private String currentLineItemName;
    private String currentLineItemAmount;
    private String subtotalAmount;    //小计
    private String totalMMAmount;  //折扣
    private String salesAmount;    //总计
    private String daishouAmount;  //代收
    private String daishouAmount2;  //代售
    private String balanceAmount; //余额
    private String changeAmount;   //找零
    private String companyName;
    private static HashMap showMsgs = new HashMap();
    private static boolean showMessageIsRunning;

    public SystemInfo() {
        trans = null;
        systemInfoListener = null;
        storeName = "";
        storeNumber = "";
        buyerID = "";
        zNumber = "";
        invoiceNumber = "";
        businessDate = null;
        terminalID = "";
        transactionNumber = "";
        cashierNumber = "";
        cashierName = "";
        memberNumber = "";
        peiDaNumber = "";
        listener = new ArrayList();
        salesManNumber = "";
        pageNumber = "1";
        currentLineItemName = "";
        currentLineItemAmount = "";
        subtotalAmount = "";
        totalMMAmount = "";
        salesAmount = "";
        balanceAmount = "";
        changeAmount = "";
        companyName = CreamProperties.getInstance().getProperty("CompanyName", "\u6CD3\u8FDC\u4FBF\u5229\u5E97");
        //Bruce/20030317
        // For Familymart: add "DontShowLicenseInfo"
        String d = CreamProperties.getInstance().getProperty("DontShowLicenseInfo");
        dontShowLicenseInfo = d != null && d.equalsIgnoreCase("yes");
        start();
    }

    /**
     * Sets the associated transaction object. It'll also register itself
     * as the transaction listener of the transaction object.
     * @param trans the transaction object
     */
    public void setTransaction(Transaction trans) {
        this.trans = trans;
    }

    /**
     * Get the store name.
     * @return the store name.
     */
    public String getStoreName() {
        if (POSTerminalApplication.getInstance().getTrainingMode())
            return CreamToolkit.GetResource().getString("TrainingMode");
        else
            return Store.getStoreChineseName();
    }

    /**
     * Get the store name and number
     * @return the store name.
     */
    public String getStoreNameAndNumber() {
        if (POSTerminalApplication.getInstance().getTrainingMode())
            return CreamToolkit.GetResource().getString("TrainingMode");
        else
            return Store.getStoreChineseName() + "(" + Store.getStoreID() + ")";
    }

    public boolean getIsDaiFu() {
        return isDaiFu;
    }

    public void setIsDaiFu(boolean isDaiFu) {
        SystemInfo.isDaiFu = isDaiFu;
    }

    /**
     * Get the store number.
     * @return the store number.
     */
    public String getStoreNumber() {
        storeNumber = Store.getStoreID();
        return storeNumber;
    }

    /**
     * Get the buyer's 统一编号.
     * @return the buyer's 统一编号.
     */
    public String getBuyerID() {
        buyerID = trans.getBuyerNumber();
        if (buyerID == null)
            return "";
        else
            return buyerID;
    }

    public String getPeiDaNumber() {
        String type = trans.getAnnotatedType();
        if (type != null && type.equalsIgnoreCase("P")) {
            peiDaNumber = trans.getAnnotatedId();
            if (peiDaNumber == null)
                return "";
            else
                return peiDaNumber;
        } else {
            return "";
        }
    }

    private static void extractMsg(Object msgs[]) {
        for (int i = 0; i < msgs.length; i++) {
            Message msg = (Message) msgs[i];
            showMsgs.put(msg.getId() + msg.getSource(), msg.getMessage());
        }

    }

    private static synchronized void setShowMessageIsRunning(boolean b) {
        showMessageIsRunning = b;
    }

    private static synchronized boolean getShowMessageIsRunning() {
        return showMessageIsRunning;
    }

    public void run() {
        do {
            POSTerminalApplication app = POSTerminalApplication.getInstance();
            try {
                Thread.sleep(20000L);
                Object msgs[] = (Object[]) null;
                StringBuffer messageString = new StringBuffer();
                Client client = Client.getInstance();
                if (client.isConnected())
                    synchronized (client) {
                        if (client.isConnected()) {
                            msgs = client.downloadDac0("hyi.cream.dac.Message");
                            try {
                                client.sendMessage("OK");
                            } catch (IOException e1) {
                                e1.printStackTrace(Client.getLogger());
                            }
                        }
                    }
                if (msgs != null) {
                    for (int i = 0; i < msgs.length; i++) {
                        Message msg = (Message) msgs[i];
                        messageString.append(msg.getMessage());
                        messageString.append("          ");
                    }

                }
                if (messageString.length() == 0) {
                    String licenseInfo = CreamProperties.getInstance().getProperty("LicenseInfo");
                    if (licenseInfo == null)
                        licenseInfo = res.getString("CompanyTitle");
                    app.getInformationIndicator().setMessage(dontShowLicenseInfo ? "" : licenseInfo);
                } else {
                    app.getInformationIndicator().setMessage(messageString.toString());
                }
            } catch (ClientCommandException clientcommandexception) {
            } catch (InterruptedException interruptedexception) {
            }
        } while (true);
    }

//      static class ShowMessage implements Runnable {
//      public void run() {
//          try {
//              if (!showMsgs.isEmpty()) {
//                  if (POSTerminalApplication.getInstance().getInformationIndicator().getDisplay())
//                      return;
//                  Iterator itr = showMsgs.keySet().iterator();
//                  if (itr.hasNext()) {
//                      Object nextKey = itr.next();
//                      String next = (String)showMsgs.remove(nextKey);
//                      //(String)showMsgs.get(nextKey);
//                      POSTerminalApplication.getInstance().getInformationIndicator().setMessage(next);
//                  }
//                  return;
//              } else {
//                  if (!POSTerminalApplication.getInstance().getInformationIndicator().getDisplay()) {
//                      if (dontShowLicenseInfo)
//                          POSTerminalApplication.getInstance().getInformationIndicator().setMessage("");
//                      else
//                          POSTerminalApplication.getInstance().getInformationIndicator().setMessage(
//                              res.getString("CompanyTitle"));
//                  }
//              }
//              if (!Client.getInstance().isConnected())
//                  return;
//    
//              Client client = Client.getInstance();
//              Object[] msgs = null;
//              synchronized (client) {
//                  msgs = client.downloadDac0("hyi.cream.dac.Message");
//                  try {
//                      client.sendMessage("OK");
//                  } catch (IOException e1) {
//                      e1.printStackTrace(Client.getLogger());
//                  }
//              }
//              if (msgs != null)
//                  extractMsg(msgs);
//          } catch (ClientCommandException e) {
//              return;
//          } finally {
//              setShowMessageIsRunning(false);
//          }
//      }
//    }
//    
//    static public void showMsg() {
//      if (!getShowMessageIsRunning()) {
//          Thread show = new Thread(new ShowMessage());
//          show.start();
//          setShowMessageIsRunning(true);
//      }
//    }

    /**
     * Get the Connection status between SC and POS.
     * @return the status string.
     */
    public static String getConnected() {
        //Bruce/20030318
        // 1. 解决连、离线的字样和图标不同步的问题
        // 2. 延长取得后台通报信息的间隔
        connected = Client.getInstance().isConnected();
//      showMsg();
//      if (inter > 10 || inter == 0) {
//          inter = 1;
//          //connected = DacTransfer.getInstance().isConnected();
//          //DacTransfer.getInstance().showMsg();
//          showMsg();
//      } else {
//          inter++;
//      }
        if (connected)
            return res.getString("Online");
        else
            return res.getString("Offline");
    }

    public String getTransactionType() {
        //Display transaction type in Itemlist head
        if (getIsDaiFu())
            return CreamToolkit.GetResource().getString("DaiFu");
        String type = "";
        if (trans != null)
            type = trans.getDealType2();
        if (type == null || type.trim().length() == 0)
            type = "0";
        ResourceBundle res = CreamToolkit.GetResource();
        String displayName = res.getString("td" + type);
        return displayName;
        //Rectangle2D r2 = fm.getStringBounds(displayName, og);

        //BigDecimal strl = new BigDecimal(r2.getWidth());
        //int length = (this.getWidth() - strl.intValue()) / 2;

        //og.setColor(Color.white);
        //og.drawString(displayName, length, 28);
        // end Display transaction type in Itemlist head
    }

    public String getZNumber() {
        zNumber = CreamProperties.getInstance().getProperty("ZNumber");
        return zNumber;
    }

    /**
     * Get the invoice number.
     * @return the invoice number.
     */
    public String getInvoiceNumber() {
        invoiceNumber = CreamProperties.getInstance().getProperty("NextInvoiceNumber");
        return invoiceNumber;
    }

    /**
     * Get the business date.
     * @return the business date.
     */
    public Date getBusinessDate() {
        businessDate = new Date();
        return businessDate;
    }

    /**
     * Get the terminal ID.
     * @return the terminal ID.
     */
    public String getTerminalID() {
        Integer tm = trans.getTerminalNumber();
        if (tm == null)
            return "??";
        String tmno = tm.toString();
        if (tmno.length() == 1)
            tmno = "0" + tmno;
        return tmno;
    }

    /**
     * Get the sequence number of the transaction.
     * @return the transaction number.
     */
    public String getTransactionNumber() {
        transactionNumber = String.valueOf(Transaction.getNextTransactionNumber());
        //CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber");
        if (transactionNumber.equals("-1") && trans != null && !trans.getDealType2().equalsIgnoreCase("D")) {
            POSTerminalApplication.getInstance().getWarningIndicator().setMessage(res.getString("LostSQLConnection"));
            StateMachine.getInstance().setEventProcessEnabled(false);
            try {
                Thread.sleep(10000L);
            } catch (InterruptedException e) {
                CreamToolkit.logMessage(e.getMessage());
            }
            System.exit(1);
        }
        for (; transactionNumber.length() < 8; transactionNumber = "0" + transactionNumber)
            ;
//        if (transactionNumber.length() > 8) {
//            transactionNumber = transactionNumber.substring(transactionNumber.length() - 5);
//        }
        return transactionNumber;
    }

    /**
     * 获取显示的交易号码，可能是经过打乱的
     * @return String
     */
    public String getPrintTransactionNumber() {
        return trans.getPrintTranNumber();
    }

    /**
     * Get the cashier number.
     * 
     * @return the cashier number.
     */
    public String getCashierNumber() {
        return CreamProperties.getInstance().getProperty("CashierNumber");
    }

    /**
     * Get the cashier name.
     * 
     * @return the cashier name.
     */
    public String getCashierName() {
        Cashier cashier = Cashier.queryByCashierID(getCashierNumber());
        if (cashier == null)
            return "";
        else
            return cashier.getCashierName();
    }

    /**
     * Get the drawer number.
     * @return the drawer number.
     */
    public String getMemberNumber() {
        memberNumber = trans.getMemberID();
        if (memberNumber == null)
            memberNumber = "";
        return memberNumber;
    }

    /**
     * Invoked when transaction has been changed.
     * @param e an event object represents the changes.
     */
    public void transactionChanged(TransactionEvent e) {
        setTransaction((Transaction) e.getSource());
        fireEvent(new SystemInfoEvent(this));
    }

    public void addSystemInfoListener(SystemInfoListener s) {
        listener.add(s);
    }

    public void fireEvent(SystemInfoEvent e) {
        SystemInfoListener s;
        for (Iterator iter = listener.iterator(); iter.hasNext(); s.systemInfoChanged(e))
            s = (SystemInfoListener) iter.next();

    }

    /**
     * @return String
     */
    public String getSalesManNumber() {
        return salesManNumber;
    }

    /**
     * Sets the salesManNumber.
     * @param salesManNumber The salesManNumber to set
     */
    public void setSalesManNumber(String salesManNumber) {
        this.salesManNumber = salesManNumber;
        fireEvent(new SystemInfoEvent(this));
    }

    /*
     * 页数
     */
    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
        fireEvent(new SystemInfoEvent(this));
    }

    public String getPageNumber() {
        return " " + pageNumber;
    }

    public String getCurrentLineItemName() {
        if (POSTerminalApplication.getInstance().getDacViewer().isVisible()) {
            Inventory inv = InventoryIdleState.getCurrentInventory();
            return inv == null ? "" : inv.getDescription();
        }
        if (trans != null && trans.getCurrentLineItem() != null)
            return trans.getCurrentLineItem().getDescriptionAndSpecification();
        else
            return "";
    }

    //gllg
    public String getCurrentLineItemAmount() {
        if (POSTerminalApplication.getInstance().getDacViewer().isVisible()) {
            Inventory inv = InventoryIdleState.getCurrentInventory();
            return inv == null ? "" : inv.getUnitPrice().toString();
        }
        if (trans != null && trans.getCurrentLineItem() != null)
            return trans.getCurrentLineItem().getAfterItemDsctAmount()
                .setScale(2, BigDecimal.ROUND_HALF_UP).toString();
        else
            return "";
    }
    
    //gllg
    public String getLineItemDsctAmtTotal(){
        return trans.getLineItemDsctAmtTotal().toString();
    }

    public void setSubtotalAmount(String subtotalAmount) {
        this.subtotalAmount = subtotalAmount;
        fireEvent(new SystemInfoEvent(this));
    }

    public String getSubtotoalAmount() {
        return subtotalAmount;
    }

    public void setTotalMMAmount(String totalMMAmount) {
        this.totalMMAmount = totalMMAmount;
        fireEvent(new SystemInfoEvent(this));
    }

    public String getTotoalMMAmount() {
        return totalMMAmount;
    }

    public void setSalesAmount(String salesAmount) {
    	this.salesAmount = salesAmount;
    	fireEvent(new SystemInfoEvent(this));
    }
    
    public String getSalesAmount() {
    	return salesAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
        fireEvent(new SystemInfoEvent(this));
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setChangeAmount(String changeAmount) {
        this.changeAmount = changeAmount;
        fireEvent(new SystemInfoEvent(this));
    }

    public String getChangeAmount() {
        return changeAmount;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getTranTypeName() {
        String tranType = trans.getTransactionType();
        if (tranType == null || tranType.length() == 0)
            return "";
        String type = tranType.substring(0, 1);
        if ("1".equals(type)) {
            return res.getString("TranTypeName1");
        } else {
            return res.getString("TranTypeName0");
        }
    }

}

/*******************************************************************************
 * DECOMPILATION REPORT ***
 * 
 * DECOMPILED FROM: D:\workspace7\CStorePOS\classes\cream.jar
 * 
 * 
 * TOTAL TIME: 390 ms
 * 
 * 
 * JAD REPORTED MESSAGES/ERRORS:
 * 
 * 
 * EXIT STATUS: 0
 * 
 * 
 * CAUGHT EXCEPTIONS:
 * 
 ******************************************************************************/


