package hyi.cream;

public class TooManyLineItemsException extends Exception{

    public TooManyLineItemsException() {
        super();
    }

    public TooManyLineItemsException(String s) {
        super(s);
    }
}

