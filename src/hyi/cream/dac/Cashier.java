package hyi.cream.dac;

import java.util.*;
import java.io.*;

/**
 * Cashier definition class.
 *
 * @author Slackware, Bruce
 * @version 1.5
 */
public class Cashier extends DacBase implements Serializable {//Read only
                                      //Support cache mechanism
    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in n
     * ew inline:
     *    Add: public static Collection getAllObjectsForPOS()
     */
    transient public static final String VERSION = "1.5";

    static final String tableName = "cashier";
    transient static private Set cache;
    private static ArrayList primaryKeys = new ArrayList();

    static {
        if (!hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("CASNO");
            createCache();
        }
    }

    //constructor
    public Cashier () throws InstantiationException {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public static void createCache() {
        cache = new HashSet();
        Iterator itr = getMultipleObjects(Cashier.class, "SELECT * FROM " + tableName);
        if (itr != null) {
            while (itr.hasNext()) {
                cache.add(itr.next());
            }
        } else {
            cache = null;
        }
    }

    public static Cashier queryByCashierID(String cashierID) {
        /*return (Cashier)getSingleObject(Cashier.class,
            "SELECT * FROM " + tableName + " WHERE CASNO='" + cashierID + "'");//*/
        if (cache == null)
            return null;
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            Cashier p = (Cashier)itr.next();
            if (p.getCashierNumber().equals(cashierID)){
            return p;
            }
        }
        return null;
    }

    public static Cashier CheckValidCashier(String cashierID, String password,String level) {
       if (cache == null)
            return null;
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            Cashier p = (Cashier)itr.next();
            if (p.getCashierNumber().equals(cashierID)){
               if (p.getPassword().equals(password))
                  if (Integer.parseInt(p.getCashierLevel())<=Integer.parseInt(level))
                  return p;
            }
        }
        return null;
    }
    public static Cashier CheckValidCashier(String cashierID, String password) {
        /*return (Cashier)getSingleObject(Cashier.class,
            "SELECT * FROM " + tableName + " WHERE CASNO='" + cashierID + "'");//*/
        if (cache == null)
            return null;
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            Cashier p = (Cashier)itr.next();
            if (p.getCashierNumber().equals(cashierID)){
               if (p.getPassword().equals(password))
                  return p;
            }
        }
        return null;
    }
    //end of static block

    //Override equals() of Object;
    public boolean equals(Object obj) {
        if ( !(obj instanceof Cashier))
            return false;
        return getCashierNumber().equals(((Cashier)obj).getCashierNumber());
    }

    //Properties
    public String getCashierNumber () {
        return (String) getFieldValue("CASNO");
    }

    public String getCashierName () {
        return (String) getFieldValue("CASNAME");
    }

    public String getPassword () {
        return (String) getFieldValue("CASPASS");
    }

    public String getCashierLevel (){
       return (String) getFieldValue("CASLEVEL");
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }
    
	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("cashierID", "CASNO");
        fieldNameMap.put("cashierName", "CASNAME");
        fieldNameMap.put("cashierPassword", "CASPASS");
        fieldNameMap.put("cashierLevel", "CASLEVEL");		
		// 2003/02/09, Added by Meyer
        fieldNameMap.put("cashierRights", "CASRIGHTS");  
        
		return fieldNameMap;
	}

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {      
        return DacBase.getMultipleObjects(hyi.cream.dac.Cashier.class,
            "SELECT * FROM posdl_cashier", getScToPosFieldNameMap());
    }
}
