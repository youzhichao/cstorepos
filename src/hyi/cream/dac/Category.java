package hyi.cream.dac;

import java.util.*;
import java.math.*;
import java.io.*;

/**
 * Category definition class.
 *
 * @author Slackware, Bruce
 * @version 1.5
 */
public class Category extends DacBase implements Serializable {

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public static Collection getAllObjectsForPOS()
     */
    transient public static final String VERSION = "1.5";

    static final String tableName = "cat";
    private static ArrayList primaryKeys = new ArrayList();

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    static {
        primaryKeys.add("CATNO");
        primaryKeys.add("MIDCATNO");
        primaryKeys.add("MICROCATNO");
    }
    //constructor
    public Category () throws InstantiationException {
    }

    public static Category queryByCategoryNumber(String catNo) {
        return (Category)getSingleObject(Category.class,
            "SELECT * FROM " + tableName + " WHERE CATNO='" + catNo + "'");
    }

    public static Category queryByCategoryNumber(String catNo, String MidCategoryNumber) {
        return (Category)getSingleObject(Category.class,
            "SELECT * FROM " + tableName + " WHERE CATNO='" + catNo + "'" + " and "
            + " MIDCATNO='" + MidCategoryNumber + "'" + " and "
            + " MICROCATNO='" + "'" );
    }

    public static Category queryByCategoryNumber(String catNo, String MidCategoryNumber, String MicroCategoryNumber) {
        return (Category)getSingleObject(Category.class,
            "SELECT * FROM " + tableName + " WHERE CATNO='" + catNo + "'" + " and "
            + " MIDCATNO='" + MidCategoryNumber + "'" + " and "
            + " MICROCATNO='" + MicroCategoryNumber + "'");
    }
    //*/

    //Get properties value:
    public String getCategoryNumber() {
        return (String)getFieldValue("CATNO");
    }

    public String getMidCategoryNumber () {
        return (String)getFieldValue("MIDCATNO");//	VARCHAR(4)	N
    }

    public String getMicroCategoryNumber() {
        return (String)getFieldValue("MICROCATNO");// VARCHAR(4) N
    }

    public String getThinCategoryNumber() {
        return (String)getFieldValue("THINCATNO");// VARCHAR(4) N
    }

    public String getScreenName() {
        return (String)getFieldValue("CATNAME");
    }

    public String getPrintName() {
        return (String)getFieldValue("CATPNAME");//	VARCHAR(16)	N
    }

    public String getTaxType() {
        return (String)getFieldValue("DEPTAX");	//CHAR(1) N
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("categoryNumber", "CATNO");
        fieldNameMap.put("midCategoryNumber", "MIDCATNO");
        fieldNameMap.put("microCategoryNumber", "MICROCATNO");
        fieldNameMap.put("thinCategoryNumber", "THINCATNO");
        fieldNameMap.put("categoryName", "CATNAME");
        fieldNameMap.put("taxID", "DEPTAX");        
        return fieldNameMap;
	}
    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {

        return DacBase.getMultipleObjects(hyi.cream.dac.Category.class,
            "SELECT * FROM posdl_cat", getScToPosFieldNameMap());
    }
}
