/**
 * DAC class
 * @since 2000
 * @author slackware
 */

package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.text.*;
import java.io.*;
import hyi.cream.util.*;

public class CategorySales extends DacBase implements Serializable {//R&W

	static final String tableName = "catsales";
	static CategorySales staticCS  = null;
	static Iterator currentCategorySales = null;
	boolean addCustom = false;
    BigDecimal mmAmount = new BigDecimal(0);

	private static ArrayList primaryKeys = new ArrayList();

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	static {
		primaryKeys.add("CATNO");
		primaryKeys.add("MIDCATNO");
		primaryKeys.add("MICROCATNO");
		primaryKeys.add("ZCNT");
	}                           

	public static CategorySales queryByDateTime(java.util.Date date) {
		SimpleDateFormat df = (SimpleDateFormat)SimpleDateFormat.getDateTimeInstance();
		//System.out.println("SELECT * FROM " + tableName + " WHERE SYSDATETIME='" + df.format(date).toString() + "'");
		return (CategorySales)getSingleObject(CategorySales.class,
			"SELECT * FROM " + tableName + " WHERE SYSDATETIME='" + df.format(date).toString() + "'");
	}        

    public static Iterator queryByCatID(String catID, Date date) {
        String zcnt = "";
        ArrayList catArray = new ArrayList();
        if (date != null) {
            Iterator zIter = ZReport.queryByDateTime(date);
            //Iterator catIter = null;
            //CategorySales cat = null;
            while (zIter.hasNext()) {
                zcnt = ((ZReport)zIter.next()).getSequenceNumber().toString();
                catArray.add((CategorySales)getSingleObject(CategorySales.class, "select * from "
                    +  tableName + " where CATNO='" + catID + "' AND MIDCATNO='' AND ZCNT=" + zcnt));
            }
            return catArray.iterator();
        } else {
            zcnt = ZReport.getOrCreateCurrentZReport().getSequenceNumber().toString();
            catArray.add((CategorySales)getSingleObject(CategorySales.class, "select * from "
                +  tableName + " where CATNO='" + catID + "' AND MIDCATNO='' AND ZCNT=" + zcnt));
            return catArray.iterator();
        }
    }           

    public static Iterator queryByCatIDAndMidCatID(String catID,
            String midcatID, Date date) {
        String zcnt = "";
        ArrayList catArray = new ArrayList();
        if (date != null) {
            Iterator zIter = ZReport.queryByDateTime(date);
            //Iterator catIter = null;
            //CategorySales cat = null;
            while (zIter.hasNext()) {
                zcnt = ((ZReport)zIter.next()).getSequenceNumber().toString();
                catArray.add((CategorySales)getSingleObject(CategorySales.class, "select * from "
                    +  tableName + " where CATNO='" + catID + "' AND MIDCATNO='"
                    + midcatID + "' AND MICROCATNO='' AND ZCNT=" + zcnt));
            }
            return catArray.iterator();
        } else {
            zcnt = ZReport.getOrCreateCurrentZReport().getSequenceNumber().toString();
            catArray.add((CategorySales)getSingleObject(CategorySales.class, "select * from "
                +  tableName + " where CATNO='" + catID + "' AND MIDCATNO='"
                + midcatID + "' AND MICROCATNO='' AND ZCNT=" + zcnt));
            return catArray.iterator();
        }
    }

    public static Iterator queryByCatIDAndMidCatIDAndMicroCatID(String catID,
            String midcatID, String microcatID, Date date) {
        String zcnt = "";
        ArrayList catArray = new ArrayList();
        if (date != null) {
            Iterator zIter = ZReport.queryByDateTime(date);
            //Iterator catIter = null;
            //CategorySales cat = null;
            while (zIter.hasNext()) {
                zcnt = ((ZReport)zIter.next()).getSequenceNumber().toString();
                catArray.add((CategorySales)getSingleObject(CategorySales.class,
                    "select * from " +  tableName + " where CATNO='" + catID
                    + "' AND MIDCATNO='" + midcatID + "' AND MICROCATNO='"
                    + microcatID + "' AND ZCNT=" + zcnt));
            }
            return catArray.iterator();
        } else {
            zcnt = ZReport.getOrCreateCurrentZReport().getSequenceNumber().toString();
            catArray.add((CategorySales)getSingleObject(CategorySales.class,
                "select * from " +  tableName + " where CATNO='" + catID
                + "' AND MIDCATNO='" + midcatID + "' AND MICROCATNO='"
                + microcatID + "' AND ZCNT=" + zcnt));
            return catArray.iterator();
        }
    }

	public static Iterator getCurrentCategorySales() {
		if (currentCategorySales == null) {
			String z = CreamProperties.getInstance().getProperty("ZNumber");
			Iterator itr = getMultipleObjects(CategorySales.class, "select * from " +  tableName + " where ZCNT = " + z );
			if (itr != null)
				currentCategorySales = itr;
			else
				currentCategorySales = constructFromCategory();
		}
		return currentCategorySales;
	}

	public static boolean updateAll() {
		boolean result = true;
		Iterator itr =  getCurrentCategorySales();
		while (itr.hasNext()) {
			CategorySales catSale = (CategorySales)itr.next();
			if (!catSale.update())
			    result = false;
		}
		return result;
	}

	public static boolean updateAll_Z() {
		boolean result = true;
		Iterator itr =  getCurrentCategorySales();
		while (itr.hasNext()) {
			CategorySales catSale = (CategorySales)itr.next();
			catSale.setSystemDateTime(new Date());
			if (!catSale.update())
			    result = false;
		}
		return result;
	}
	
	public CategorySales() throws InstantiationException {
		setSystemDateTime(CreamToolkit.getInitialDate());
		setStoreNumber(CreamProperties.getInstance().getProperty("StoreNumber"));
		setTerminalNumber(Integer.decode(CreamProperties.getInstance().getProperty("TerminalNumber")));
		setSequenceNumber(Integer.decode(CreamProperties.getInstance().getProperty("ZNumber")));
	}

	public static Iterator constructFromCategory() {
		 Iterator itr = getMultipleObjects(Category.class, "select * from " + Category.tableName);
		 ArrayList it = new ArrayList();
		 while (itr.hasNext()) {
			 Category cat = (Category)itr.next();
			 try {
				 staticCS = new CategorySales();
			 } catch (InstantiationException ie) {  }//
			 staticCS.setCategoryNumber(cat.getCategoryNumber());
			 staticCS.setMidCategoryNumber(cat.getMidCategoryNumber());
			 staticCS.setMicroCategoryNumber(cat.getMicroCategoryNumber());
			 it.add(staticCS);
		 }
		 return it.iterator();
	}
	
	public static ArrayList getAvailableCategory(LineItem li) {
		Iterator itr =  getCurrentCategorySales();
		ArrayList array = new ArrayList();
		while (itr.hasNext()) {
			CategorySales catSale = (CategorySales)itr.next();
			if (catSale.belongTo(li))
				array.add(li);
		}
		return array;
	}

	public static ArrayList getAvailableCategory(PLU li) {
		Iterator itr =  getCurrentCategorySales();
		ArrayList array = new ArrayList();
		while (itr.hasNext()) {
			CategorySales catSale = (CategorySales)itr.next();
			if (catSale.belongTo(li))
				array.add(li);
		}
		return array;
	}


	public static Iterator getAllCategorySales() {
		String z = CreamProperties.getInstance().getProperty("ZNumber");
		Iterator itr = getMultipleObjects(CategorySales.class, "select * from " +  tableName + " where ZCNT = " + z );
		if (itr != null)
			return itr;
		itr = CategorySales.constructFromCategory();
		while (itr.hasNext()) {
		   CategorySales catSale = (CategorySales)itr.next();
		   if (!catSale.insert()) {
			  catSale.queryByPrimaryKey();
		   }
		}
		return itr;
	}

	public void setCustomAdd(boolean add){
		addCustom = add;
	}

	public boolean getCustomAdd() {
        return addCustom;
	}

	public boolean belongTo (LineItem lineItem) {
		String c = lineItem.getCategoryNumber();
		String m1 = lineItem.getMidCategoryNumber();
		String m2 = lineItem.getMicroCategoryNumber();
		if (this.getCategoryNumber().equals(c)
			&& this.getMidCategoryNumber().equals(m1)
			&& this.getMicroCategoryNumber().equals(m2))
			return true;
		return false;
	}

	public boolean belongTo (PLU lineItem) {
		String c = lineItem.getCategoryNumber();
		String m1 = lineItem.getMidCategoryNumber();
		String m2 = lineItem.getMicroCategoryNumber();
		if (this.getCategoryNumber().equals(c)
			&& this.getMidCategoryNumber().equals(m1)
			&& this.getMicroCategoryNumber().equals(m2))
			return true;
		return false;
	}

    /*public void setCatPlu(PLU plu) {
        String catID = plu.getCategoryNumber();
        while (catID.length() < 4) {
            catID = "0" + catID;
        }
        String midcatID =plu.getMidCategoryNumber();
        while (midcatID.length() < 4) {
            midcatID = "0" + midcatID;
        }
        String microcatID = plu.getMicroCategoryNumber();
        while (microcatID.length() < 4) {
            microcatID = "0" + microcatID;
        }
        String ID = catID + midcatID + microcatID;
    }*/

    public void setMMTempAmount(BigDecimal mmAmount) {
        this.mmAmount = this.mmAmount.add(mmAmount);
    }

    public void updateMM() {
        setMMAmount(getMMAmount().add(mmAmount));
    }



	//Get properties value	
//SystemDateTime	SYSDATETIME	DATETIME
	public Date getSystemDataTime() {
		return (Date)getFieldValue("SYSDATETIME");
	}
	public void setSystemDateTime(Date SystemDateTime){
		setFieldValue("SYSDATETIME", SystemDateTime);
	}
	
//SequenceNumber	SHIFTCNT	TINYINT UNSIGNED	N
	public Integer getSequenceNumber() {
		return (Integer)getFieldValue("ZCNT");
	}

	public void setSequenceNumber(Integer SQNumber) {
		setFieldValue("ZCNT", SQNumber);
	}
//StoreNumber	STORECODE	CHAR(6)	N
	public String getStoreNumber() {
		return (String)getFieldValue("STORECODE");
	}

	public void setStoreNumber(String storeNumber) {
		setFieldValue("STORECODE", storeNumber);
	}

//TerminalNumber	TMCODE	TINYINT UNSIGNED	N
	public Integer getTerminalNumber() {
	    return (Integer)getFieldValue("TMCODE");
	}
	public void setTerminalNumber(Integer TMCODE) {
        setFieldValue("TMCODE", TMCODE);
	}

//UploadState
    public String getUploadState() {
        return (String)getFieldValue("TCPFLG");
    }

    public void setUploadState(String tcpflg) {
        setFieldValue("TCPFLG", tcpflg);
    }

//TransactionCount
    public Integer getTransactionCount() {
        return (Integer)getFieldValue("TRANCNT");
    }

    public void setTransactionCount(Integer trancnt) {
        setFieldValue("TRANCNT", trancnt);
    }

//GrossSalesTotalAmount
    public BigDecimal getGrossSalesTotalAmount() {
        return (BigDecimal)getFieldValue("GROSSAMT");
    }

    public void setGrossSalesTotalAmount(BigDecimal grossamt) {
        setFieldValue("GROSSAMT", grossamt);
    }

//PlusCount
    public Integer getPlusCount() {
        return (Integer)getFieldValue("PLUSCOUNT");
    }

    public void setPlusCount(Integer pluscount) {
        setFieldValue("PLUSCOUNT", pluscount);
    }

//PlusAmount
    public BigDecimal getPlusAmount() {
        return (BigDecimal)getFieldValue("PLUSAMOUNT");
    }

    public void setPlusAmount(BigDecimal plusamount) {
        setFieldValue("PLUSAMOUNT", plusamount);
    }

	//--------------------------------------------------------------------------
	//CategoryNumber	CATNO CHAR(4)	N
	public String getCategoryNumber() {
		return (String)getFieldValue("CATNO");//CategoryNumber	CATNO CHAR(4)	N
	}

	public void setCategoryNumber(String CategoryNumber) {
		setFieldValue("CATNO", CategoryNumber);//CategoryNumber	CATNO CHAR(4)	N
	}

	//MidCategoryNumber MIDCATNO	VARCHAR(4)	N
	public String getMidCategoryNumber() {
		return (String)getFieldValue("MIDCATNO");//MidCategoryNumber MIDCATNO	VARCHAR(4)	N
	}

	public void setMidCategoryNumber(String MidCategoryNumber) {
	    setFieldValue("MIDCATNO", MidCategoryNumber);//MidCategoryNumber MIDCATNO	VARCHAR(4)	N
	}

    //MicroCategoryNumber MICROCATNO VARCHAR(4)	N
	public String getMicroCategoryNumber() {
		return (String)getFieldValue("MICROCATNO");//MicroCategoryNumber MICROCATNO VARCHAR(4)	N
	}

	public void setMicroCategoryNumber(String MicroCategoryNumber) {
		setFieldValue("MICROCATNO", MicroCategoryNumber);//MicroCategoryNumber MICROCATNO VARCHAR(4)	N
	}
	//*************************************************************************

	

	public BigDecimal getAmount() {
		return (BigDecimal)getFieldValue("AMOUNT");
	}

	public void setAmount(BigDecimal Amount) {
		setFieldValue("AMOUNT", Amount);
	}
	//

	public Integer getDiscountCount() {
		return (Integer)getFieldValue("DCOUNT");
	}

	public void setDiscountCount(Integer DiscountCount) {
		setFieldValue("DCOUNT", DiscountCount);
	}
	//

	public BigDecimal getDiscountAmount() {
		return (BigDecimal)getFieldValue("DAMOUNT");//DiscountAmount	DAMOUNT	DECIMAL(10,2)	N
	}
	public void setDiscountAmount(BigDecimal DiscountAmount) {
		 setFieldValue("DAMOUNT", DiscountAmount);//DiscountAmount	DAMOUNT	DECIMAL(10,2)	N
	}
	//

	public Integer getMMCount() {
		return (Integer)getFieldValue("MMCOUNT");//MMCount MMCOUNT INT	N
	}
	public void setMMCount(Integer MMount) {
	    setFieldValue("MMOUNT", MMount);
	}
	//

	public BigDecimal getMMAmount() {
		return (BigDecimal)getFieldValue("MMAmount"); //MMAmount MMAMOUNT DECIMAL(10,2)	N
	}
	public void setMMAmount(BigDecimal MMAmount) {
		setFieldValue("MMAmount", MMAmount); //MMAmount MMAMOUNT DECIMAL(10,2)	N
	}
	//

	public Integer getCustomCount() {
		return (Integer)getFieldValue("CUSTCOUNT");
	}//CustomerCount CUSTCOUNT	INT	N

	public void setCustomCount(Integer CustomCount) {
		setFieldValue("CUSTCOUNT", CustomCount);
	}//CustomerCount CUSTCOUNT	INT	N

	public String getInsertUpdateTableName() {
		return tableName;
	}

}
