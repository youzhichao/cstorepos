package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.io.*;

/**
 * CombDiscountType definition class.
 *
 * @author Meyer 
 * @version 1.0
 */
public class CombDiscountType extends DacBase implements Serializable {

    /**      
     * 更新记录
     *
     * Meyer / 1.0 / 2003-01-02
     * 		1. Create the class file
     * 
     */
    
    
    transient public static final String VERSION = "1.0";

    static final String tableName = "combination_discount_type";
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("CDT_ID");
    }

    /**
     * Constructor
     */
    public CombDiscountType() throws InstantiationException {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static CombDiscountType queryByID (String cdtID) {
        return (CombDiscountType)getSingleObject(CombDiscountType.class,
            " SELECT * FROM " + tableName + 
            " WHERE CDT_ID='" + cdtID + "'");
    }

    public String getCdtID() {
        return (String)getFieldValue("CDT_ID");
    }

    public String getCdtName() {
        return (String)getFieldValue("CDT_NAME");
    }   

	/**
	 * Meyer/2003-02-21/Modified
	 * 优先级别值为字符整数,数值越小优先级别越高
	 * 		1>2>3>4>5>6>7...
	 * 
	 */
    public String getPriority() {
        return (String)getFieldValue("CDT_PRIORITY");
    }


    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
 		Map fieldNameMap = null;			      
	    return DacBase.getMultipleObjects(hyi.cream.dac.CombDiscountType.class,
	    	"SELECT * FROM posdl_combination_discount_type", fieldNameMap);
    }
    
    
    /*
     * 测试方法
     */
    public static void main(String [] args) {
    	/*
    	CombDiscountType cdt = CombDiscountType.queryByID(args[0]);
    	System.out.println(cdt.getCdtID());
    	System.out.println(cdt.getCdtName());
    	System.out.println(cdt.getPriority());
    	*/
    
    	try {
			Class instanceClass = Class.forName( "hyi.jpos.loader.ServiceInstanceFactory" );
		} catch (ClassNotFoundException e) {
		}


    }
}

