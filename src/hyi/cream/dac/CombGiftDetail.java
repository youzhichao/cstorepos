package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.io.*;

/**
 * CombGiftDetail definition class.
 *
 * @author Meyer 
 * @version 1.0
 */
public class CombGiftDetail extends DacBase implements Serializable {

    /**
     * 更新记录
     * 
     * Meyer / 1.0 / 2003-01-02
     * 		1. Create the class file
     * 
     */
    
    
    
    transient public static final String VERSION = "1.0";

    static final String tableName = "combination_gift_detail";
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("CGD_CPH_ID");
        primaryKeys.add("CGD_GIFT_ID");
    }

    /**
     * Constructor
     */
    public CombGiftDetail() throws InstantiationException {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static Iterator queryByCphID(String aCphID) {
        return CombGiftDetail.getMultipleObjects(
        	CombGiftDetail.class,
            " SELECT * FROM combination_gift_detail" + 
            " WHERE CGD_CPH_ID='" + aCphID + "'"
      	);
    }

    public String getCphID() {
        return (String)getFieldValue("CGD_CPH_ID");
    }

    public String getPluCode() {
        return (String)getFieldValue("CGD_GIFT_ID");
    }

	/**
	 * Meyer/2003-02-21/Modified because of LogicChina's requirement 
	 * 		datatype of CGD_QUANTITY in db cahnge to decimal(10,2)
	 */
    public Integer getQuantity() {
        Object value = getFieldValue("CGD_QUANTITY");
        if (value instanceof BigDecimal)
            return new Integer(
            	((BigDecimal)getFieldValue("CGD_QUANTITY")).intValue());
        else
            return (Integer)value;
    }


    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
 		Map fieldNameMap = null;			      
	    return DacBase.getMultipleObjects(hyi.cream.dac.CombGiftDetail.class,
	    	"SELECT * FROM posdl_combination_gift_detail", fieldNameMap);
    }
    
    /*
     * 测试方法
     */
    public static void main(String [] args) {
    	Iterator cgdIter = CombGiftDetail.queryByCphID(args[0]);
    	while (cgdIter.hasNext()) {
	    	CombGiftDetail cgd = (CombGiftDetail)cgdIter.next();
	    	System.out.println(cgd.getCphID());
	    	System.out.println(cgd.getPluCode());
	    	System.out.println(cgd.getQuantity());    	
    	}

    }
    
}

