package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.io.*;

/**
 * CombPromotionHeader definition class.
 *
 * @author Meyer 
 * @version 1.0
 */
public class CombPromotionHeader extends DacBase implements Serializable {

    /**
     * 更新记录
     * 
     * Meyer / 1.0 / 2003-01-02
     * 		1. Create the class file
     * 
     */
    
    
    transient public static final String VERSION = "1.0";

    static final String tableName = "combination_promotion_header";
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("CPH_ID");
    }

    /**
     * Constructor
     */
    public CombPromotionHeader() throws InstantiationException {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static CombPromotionHeader queryByID (String cphID) {
        return (CombPromotionHeader)getSingleObject(CombPromotionHeader.class,
            " SELECT * FROM " + tableName + 
            " WHERE CPH_ID='" + cphID + "'");
    }

    public String getCphID() {
        return (String)getFieldValue("CPH_ID");
    }

/**
 * Meyer / 2003-02-21 / Commented because of no use at now
    public Date getBeginDate() {
        return (Date)getFieldValue("CPH_SALES_SDATE");
    }

    public Date getEndDate() {
        return (Date)getFieldValue("CPH_SALES_EDATE");
    }

    public Date getBeginTime() {
        return (Date)getFieldValue("CPH_DISCOUNT_SDATE");
    }

    public Date getEndTime() {
        return (Date)getFieldValue("CPH_DISCOUNT_EDATE");
    }

    public Date getBeginTime2() {
        return (Date)getFieldValue("CPH_DISCOUNT_SDATE2");
    }
    
    public Date getEndTime2() {
        return (Date)getFieldValue("CPH_DISCOUNT_EDATE2");
    }

    public boolean isMondayMatch() {
        return "1".equals((String)getFieldValue("CPH_MONDAY"));
    }

    public boolean isTuesdayMatch() {
        return "1".equals((String)getFieldValue("CPH_TUESDAY"));
    }

    public boolean isWednesdayMatch() {
        return "1".equals((String)getFieldValue("CPH_WEDNESDAY"));
    }
    
    public boolean isThursdayMatch() {
        return "1".equals((String)getFieldValue("CPH_THURSDAY"));
    }
       
    public boolean isFridayMatch() {
        return "1".equals((String)getFieldValue("CPH_FRIDAY"));
    }
       
    public boolean isSaturdayMatch() {
        return "1".equals((String)getFieldValue("CPH_SATURDAY"));
    }
       
    public boolean isSundayMatch() {
        return "1".equals((String)getFieldValue("CPH_SUNDAY"));
    }

*/
	/**
	 * 折让方式（对应COMBINATION_DISCOUNT_TYPE档)
	 */   
    public String getDiscountType() {
        return (String)getFieldValue("CPH_DISCOUNT_TYPE");
    }

	/**
	 * 打折最小销售金额
	 */
    public BigDecimal getMinSalesAmount() {
        return (BigDecimal)getFieldValue("CPH_SALES_AMOUNT");
    }
    
    /**
     * 折让金额
     */
    public BigDecimal getDiscountAmount() {
        return (BigDecimal)getFieldValue("CPH_DISCOUNT_AMOUNT");
    }

	/**
	 * 促销优惠折扣比例为一整数,表示折扣的百分比数
	 * 		如 10 表示 10%
	 * 
	 */
    public Integer getDiscountPercentage() {
        return (Integer)getFieldValue("CPH_DISCOUNT_PERCENTAGE");
    }


    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
 		Map fieldNameMap = null;			      
	    return DacBase.getMultipleObjects(hyi.cream.dac.CombPromotionHeader.class,
	    	"SELECT * FROM posdl_combination_promotion_header", fieldNameMap);
    }
    
    /*
     * 测试方法
     */
    public static void main(String [] args) {
    	CombPromotionHeader cph = CombPromotionHeader.queryByID(args[0]);
    	System.out.println("getCphID " + cph.getCphID());
/*
 *     	System.out.println("getBeginDate " + cph.getBeginDate());
    	System.out.println("getEndDate " + cph.getEndDate());
    	System.out.println("getBeginTime " + cph.getBeginTime());
    	System.out.println("getEndTime " + cph.getEndTime());
    	System.out.println("getBeginTime2 " + cph.getBeginTime2());
    	System.out.println("getEndTime2 " + cph.getEndTime2());   	
    	System.out.println("getDiscountAmount " + cph.getDiscountAmount());
      	System.out.println("getDiscountPercentage " + cph.getDiscountPercentage());
      	System.out.println("getDiscountType " + cph.getDiscountType());
      	System.out.println("isFridayMatch " + cph.isFridayMatch());
      	System.out.println("isMondayMatch " + cph.isMondayMatch()); 	
*/
    }
}

