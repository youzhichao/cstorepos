package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.io.*;

/**
 * CombSaleDetail definition class.
 *
 * @author Meyer 
 * @version 1.0
 */
public class CombSaleDetail extends DacBase implements Serializable {

    /**
     * Meyer / 1.0 / 2003-01-02
     * 		1. Create the class file
     * 
     * 
     */
    
    
    transient public static final String VERSION = "1.0";

    static final String tableName = "combination_sale_detail";
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("CSD_CPH_ID");
        primaryKeys.add("CSD_ARTICLE_ID");
    }

    /**
     * Constructor
     */
    public CombSaleDetail() throws InstantiationException {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static CombSaleDetail queryByID (String cphID, String articleID) {
        return (CombSaleDetail)getSingleObject(CombSaleDetail.class,
            " SELECT * FROM " + tableName + 
            " WHERE CSD_CPH_ID='" + cphID + "'" + 
            " AND CSD_ARTICLE_ID='" + articleID + "'");
    }

    public static Iterator queryCphIDByArticleID (String anArticleID) {
        String sql =             
        	" SELECT DISTINCT a.CSD_CPH_ID" + 
            " FROM combination_sale_detail a,combination_promotion_header b" + 
            " WHERE a.CSD_CPH_ID=b.CPH_ID" + 
            " AND a.CSD_ARTICLE_ID='" + anArticleID + "'" + 
            " AND CURRENT_DATE() BETWEEN b.CPH_SALES_SDATE AND b.CPH_SALES_EDATE" + 
            " AND(CURRENT_TIME() BETWEEN b.CPH_DISCOUNT_SDATE AND b.CPH_DISCOUNT_EDATE" + 
            " 	OR CURRENT_TIME() BETWEEN b.CPH_DISCOUNT_SDATE2 AND b.CPH_DISCOUNT_EDATE2" + 
            " )" + 
            " AND LEFT(SUBSTRING(CONCAT(b.CPH_MONDAY,b.CPH_TUESDAY,b.CPH_WEDNESDAY," + 
            " b.CPH_THURSDAY,b.CPH_FRIDAY,b.CPH_SATURDAY,b.CPH_SUNDAY),WEEKDAY(NOW())+1),1)=1";
        
        return CombSaleDetail.getMultipleObjects(CombSaleDetail.class,sql);
    }
    
    public static Iterator queryByCphID (String aCphID) {
        return CombSaleDetail.getMultipleObjects(
        	CombSaleDetail.class,
            " SELECT * FROM combination_sale_detail WHERE CSD_CPH_ID='" + aCphID + "'");
    }
    
    
    public static boolean isGroupPromotion(String aCphID) {
        CombSaleDetail csDetail = (CombSaleDetail)
        	CombSaleDetail.getSingleObject(CombSaleDetail.class,
            	" SELECT * FROM combination_sale_detail WHERE CSD_CPH_ID='" + aCphID + "'");
		String groupID = csDetail.getGroupID();
		int groupQty = csDetail.getGroupQty().intValue();
		return (!groupID.equals("") && groupQty > 0);         
    }
    
    




    public String getCphID() {
        return (String)getFieldValue("CSD_CPH_ID");
    }

    public String getPluCode() {
        return (String)getFieldValue("CSD_ARTICLE_ID");
    }

	/**
	 * Meyer/2003-02-21/Modified because of LogicChina's requirement 
	 * 		datatype of CSD_QUANTITY in db cahnge to decimal(10,2)
	 */
    public Integer getQuantity() {
        return new Integer(
        	((BigDecimal)getFieldValue("CSD_QUANTITY")).intValue()
        );
    }

    public String getGroupID() {
    	Object obj = getFieldValue("CSD_GROUP_ID");
    	String groupID = "";    	
    	if (obj != null) {
    		groupID = ((Integer)obj).toString();
    	}
        return groupID;
    }
    
	/**
	 * 
	 * Meyer/2003-02-28/Modified because of LogicChina's requirement 
	 * 		datatype of CSD_GROUP_QTY in db change back to integer
	 * 
	 * 
	 * Meyer/2003-02-21/Modified because of LogicChina's requirement 
	 * 		datatype of CSD_GROUP_QTY in db change to decimal(10,2)
	 */
    public Integer getGroupQty() {
        return (Integer)getFieldValue("CSD_GROUP_QTY");
    }


    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
 		Map fieldNameMap = null;			      
	    return DacBase.getMultipleObjects(hyi.cream.dac.CombSaleDetail.class,
	    	"SELECT * FROM posdl_combination_sale_detail", fieldNameMap);
    }
    
    
    /*
     * 测试方法
     */
    public static void main(String [] args) {
    	CombSaleDetail csd = null;
    	Iterator csdIter = null;

		System.out.println("*** queryByID: " + args[0] + "/" + args[1]);
		csd = CombSaleDetail.queryByID(args[0], args[1]);
    	System.out.println("getCphID " + csd.getCphID());
    	System.out.println("getPluCode " + csd.getPluCode());
    	System.out.println("getQuantity " + csd.getQuantity());
    	System.out.println("getGroupID " + csd.getGroupID());
    	System.out.println("getGroupQty " + csd.getGroupQty());		
    	
    	    	
		System.out.println("*** queryByCphID: " + args[0]);
    	csdIter = CombSaleDetail.queryByCphID(args[0]);
		while (csdIter.hasNext()) {
	    	csd = (CombSaleDetail)csdIter.next();
	    	System.out.println("getCphID " + csd.getCphID());
	    	System.out.println("getPluCode " + csd.getPluCode());
	    	System.out.println("getQuantity " + csd.getQuantity());
	    	System.out.println("getGroupID " + csd.getGroupID());
	    	System.out.println("getGroupQty " + csd.getGroupQty());
		}
		
		
		System.out.println("*** queryCphIDByArticleID: " + args[1]);
    	csdIter = CombSaleDetail.queryCphIDByArticleID(args[1]);
		while (csdIter.hasNext()) {
	    	csd = (CombSaleDetail)csdIter.next();
	    	System.out.println("getCphID " + csd.getCphID());
		}
		
		System.out.println("*** isGroupPromotion: " + args[0]);
		System.out.println("isGroupPromotion " + CombSaleDetail.isGroupPromotion(args[0]));
    }
    
}

