// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst
// Source File Name:   DacBase.java

package hyi.cream.dac;

import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import hyi.cream.inline.Server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Types;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Base class of DAC(Data Access Class).
 *
 * @author Dai, Bruce
 * @version 1.5
 */
public abstract class DacBase implements Serializable, Cloneable {
    /**
     * Meyer/1.5/2003-02-20 Add method: getScToPosFieldNameMap(),
     * getPosToScFieldNameArray()
     *
     * /Bruce/1.5/2002-03-10/ Add/Modify some methods for preparing to use in
     * new inline: A public Map getFieldMap() A public static List
     * getMultipleObjects(Class classObject, String selectStatement, Map
     * fieldNameMap) M public boolean insert() A public boolean insert(boolean
     * needQueryAgain) A public boolean deleteAll() A public boolean
     * copyFile(String source, String destination) A public boolean backupAll()
     * A public boolean restoreAll() A public boolean
     * getUseClientSideTableName() A public void
     * setUseClientSideTableName(boolean useCleintSideTableName) A public String
     * getInsertSqlStatement(boolean useCleintSideTableName) M public boolean
     * update(): use StringBuffer instead of String M public boolean
     * insert(boolean needQueryAgain): use StringBuffer instead of String
     */
    public static final transient String VERSION = "1.5";

    protected Map fieldMap;

    transient boolean useCleintSideTableName;

    static transient boolean useJVM = CreamProperties.getInstance().getProperty("InlineClientUseJVM", "yes").equalsIgnoreCase("yes");

    static final transient BigDecimal bigDecimal0 = new BigDecimal("0.00");

    static final transient Integer integer0 = new Integer(0);

    static final transient SimpleDateFormat dfyyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");

    static final transient SimpleDateFormat dfyyyyMMddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static boolean windows = System.getProperties().get("os.name").toString().indexOf("Windows") != -1;

    private HashSet dirtyFields;

    private boolean allFieldsDirty;

    static private boolean isServerSideOracle;

    static public boolean isServerSideOracle() {
        return isServerSideOracle;
    }

    DacBase() throws InstantiationException {
        fieldMap = new HashMap();

        // a set always contains upper-case string
        dirtyFields = new HashSet() {
            private static final long serialVersionUID = 1L;

            public boolean add(Object value) {
                return super.add(((String)value).toUpperCase());
            }

            public boolean contains(Object value) {
                return super.contains(((String)value).toUpperCase());
            }

        };

        fieldMap = new UppercaseMap();

        isServerSideOracle = Server.serverExist() &&
            "oracle".equalsIgnoreCase(CreamProperties.getInstance().getProperty("DBType"));
    }

    void setAllFieldsDirty(boolean b) {
        allFieldsDirty = b;
    }

    boolean isAllFieldsDirty() {
        return allFieldsDirty;
    }

    public Map getFieldMap() {
        return fieldMap;
    }

    public Map getValues() {
        Map newMap = new UppercaseMap();
        newMap.putAll(fieldMap);
        return newMap;
    }

    public Object clone() {
        try {
            DacBase dac = (DacBase) super.clone();
            Map fieldMap = dac.getFieldMap();
            dac.fieldMap = new HashMap();
            dac.fieldMap.putAll(fieldMap);
            return dac;
        } catch (CloneNotSupportedException e) {
            // Cannot happen
            return null;
        }
    }

    /**
     * Define primary key fields.
     */
    public abstract List getPrimaryKeyList();

    /**
     * Query a DAC object by the primary key.
     */
    public DacBase queryByPrimaryKey() {
        List primaryKeyList = getPrimaryKeyList();
        String selectStatement = "select * from " + getInsertUpdateTableName() + " where ";
        for (int i = 0; i < primaryKeyList.size(); i++) {
            String fieldname = (String) primaryKeyList.get(i);
            if (i > 0)
                selectStatement = selectStatement + " and ";
            if (fieldMap.get(fieldname) instanceof Date) {
                SimpleDateFormat DF = (SimpleDateFormat) DateFormat.getDateTimeInstance();
                Date d = (Date) fieldMap.get(fieldname);
                selectStatement = selectStatement + fieldname + "=" + "'" + DF.format(d).toString() + "'";
            } else {
                selectStatement = selectStatement + fieldname + "=" + "'" + fieldMap.get(fieldname) + "'";
            }
        }
        // CreamToolkit.logMessage("" + selectStatement);
        return getSingleObject(selectStatement);
    }

    public boolean deleteByPrimaryKey() {
        List primaryKeyList = getPrimaryKeyList();
        String deleteStatement = "delete from " + getInsertUpdateTableName() + " where ";
        for (int i = 0; i < primaryKeyList.size(); i++) {
            String fieldname = (String) primaryKeyList.get(i);
            if (i > 0)
                deleteStatement = deleteStatement + " and ";
            if (fieldMap.get(fieldname) instanceof Date) {
                SimpleDateFormat DF = (SimpleDateFormat) DateFormat.getDateTimeInstance();
                Date d = (Date) fieldMap.get(fieldname);
                deleteStatement = deleteStatement + fieldname + "=" + "'" + DF.format(d).toString() + "'";
            } else {
                deleteStatement = deleteStatement + fieldname + "=" + "'" + fieldMap.get(fieldname) + "'";
            }
        }

        CreamToolkit.logMessage("" + deleteStatement);
        Connection connection = null;
        Statement statement = null;
        boolean result = true;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.executeUpdate(deleteStatement);
        } catch (SQLException e) {
            result = false;
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                result = false;
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
            flushOSBuffer();
        }
        return result;
    }

    public static Map getValueOfStatement(String selectStatement) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        HashMap map = new HashMap();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            // CreamToolkit.logMessage("" + resultSet.previous());
            if (resultSet.next()) {
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object value = resultSet.getObject(i);
                    int t = metaData.getColumnType(i);
                    map.put(metaData.getColumnName(i), convertType(value, t));
                }
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
        return map;
    }

    DacBase getSingleObject(String selectStatement) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            // CreamToolkit.logMessage("" + resultSet.previous());
            if (resultSet.next()) {
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object value = resultSet.getObject(i);
                    int t = metaData.getColumnType(i);
                    fieldMap.put(metaData.getColumnName(i), convertType(value, t));
                }
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException1: " + this);
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
        return this;
    }

    /**
     * Update the current DAC object into database(locate the record by the
     * primary key).
     *
     * @return false if failed, true otherwise.
     */
    public boolean update() {
        if (!isAllFieldsDirty() && dirtyFields.isEmpty())
            return true;

        String tableName = getInsertUpdateTableName();
        Iterator iterator = fieldMap.keySet().iterator();
        Connection connection = null;
        Statement statement = null;
        Object fieldValue = null;
        String fieldName = "";
        boolean result = true;
        StringBuffer nameParams = new StringBuffer(200);
        while (iterator.hasNext()) {
            fieldName = (String) iterator.next();
            // Meyer/2003-03-11/add field into update sql statement only when
            // the field existed
            if (isFieldExist(fieldName))

                if (!isAllFieldsDirty() // 如果call过makeAllFieldsDirty()，则全部进入update statement
                        && !dirtyFields.contains(fieldName))
                        continue;

                if (getFieldValue(fieldName) == null) {
                    fieldValue = new Object();
                } else {
                    fieldValue = getFieldValue(fieldName);
                    if (fieldValue instanceof Date) {
                        SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date d = (Date) fieldValue;
                        nameParams.append(escapeFieldName(fieldName) + "='" + DF.format(d).toString() + "',");
                    } else if (fieldValue instanceof String){
                        nameParams.append(escapeFieldName(fieldName) + " = '" + escapeSpecialCharacter((String) fieldValue) + "',");
                    } else {
                        nameParams.append(escapeFieldName(fieldName) + " = " + fieldValue + ",");
                    }
                }
        }
        StringBuffer updateString = new StringBuffer(300);
        updateString.append("update " + tableName + " set " + nameParams.substring(0, nameParams.length() - 1) + " where ");
        List primaryKeyList = getPrimaryKeyList();
        for (int i = 0; i < primaryKeyList.size(); i++) {
            String fieldname = (String) primaryKeyList.get(i);
            if (i > 0)
                updateString.append(" and ");
            updateString.append(fieldname + "=\"" + fieldMap.get(fieldname) + "\"");
        }

        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.execute(updateString.toString());
        } catch (SQLException e) {
            result = false;
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                result = false;
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
            flushOSBuffer();
            dirtyFields.clear();
            setAllFieldsDirty(false);
        }
        return result;
    }

    public static void executeQuery(String query) {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        }
    }

    private static String escapeSpecialCharacter(String fieldData) {
        for (int fromIndex = 0; (fromIndex = fieldData.indexOf("'", fromIndex)) != -1; fromIndex += 2)
            fieldData = fieldData.substring(0, fromIndex + 1) + "'" + fieldData.substring(fromIndex + 1, fieldData.length());

        for (int fromIndex = 0; (fromIndex = fieldData.indexOf("\\", fromIndex)) != -1; fromIndex += 2)
            fieldData = fieldData.substring(0, fromIndex + 1) + "\\" + fieldData.substring(fromIndex + 1, fieldData.length());

        return fieldData;
    }

    /**
     * Constructs a DAC object from a row in ResultSet.
     */
    void constructFromResultSet(ResultSet resultSet) throws SQLException {
        // ResultSetMetaData metaData = resultSet.getMetaData();
        // for(int i = 1; i <= metaData.getColumnCount(); i++)
        // fieldMap.put(metaData.getColumnName(i), resultSet.getObject(i));
        ResultSetMetaData metaData = resultSet.getMetaData();
        int t = 0;
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            try {
                Object o = resultSet.getObject(i);
                t = metaData.getColumnType(i);
                fieldMap.put(metaData.getColumnName(i), convertType(o, t));
            } catch (SQLException e) {
                if (e.getMessage().indexOf("'0000-00-00'") != -1) {
                    fieldMap.put(metaData.getColumnName(i), new Date(0));
                } else
                    throw e;
            }
        }
    }

    /**
     * Get field value object by field name.
     */
    public Object getFieldValue(String fieldName) {
        Object value = fieldMap.get(fieldName);
        if (value == null)
            value = fieldMap.get(fieldName.toLowerCase());

        // Bruce/20080303 從Nitori版本搬过来用 --
        // Bruce/20051006> 解决使用新版本JDBC driver的问题
        if (value instanceof Short)
            value = new Integer(((Short) value).intValue());
        else if (value instanceof Long)
            value = new Integer(((Long) value).intValue());

        return value;
    }

    /**
     * Get a non-null BigDecimal field value.
     */
    public BigDecimal getBigDecimalFieldValue(String fieldName) {
        BigDecimal x = (BigDecimal)getFieldValue(fieldName);
        return x == null ? new BigDecimal("0.00") : x;
    }

    public void setFieldValue(String fieldName, Object fieldValue) {
        if (fieldName.equals("")) {
            return;
        } else {
            fieldMap.put(fieldName, fieldValue);
            dirtyFields.add(fieldName);
            return;
        }
    }

    /**
     * Query by a SELECT statement and construct a DAC object with type
     * 'classObject'. classObject must be derived from DacBase.
     */
    static DacBase getSingleObject(Class classObject, String selectStatement) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        DacBase dacObject = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            if (resultSet.next()) {
                dacObject = (DacBase) classObject.newInstance();
                dacObject.constructFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            CreamToolkit.logMessage(e);
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
        return dacObject;
    }

    public static Iterator getMultipleObjects(Class classObject, String selectStatement) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        DacBase dacObject = null;
        ArrayList collection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            for (resultSet = statement.executeQuery(selectStatement); resultSet.next(); collection.add(dacObject)) {
                dacObject = (DacBase) classObject.newInstance();
                dacObject.constructFromResultSet(resultSet);
                if (collection == null)
                    collection = new ArrayList();
            }

        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            CreamToolkit.logMessage(e);
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
        if (collection != null)
            return collection.iterator();
        else
            return null;
    }

    /**
     * Meyer / 2003-02-12
     *         接收一个Map，将其Key Object 转为小写的String后，连同原有Value建立新Map返回
     *
     */
    public static Map toLowerCaseMap(Map origMap) {
        Map lowerCaseMap = new HashMap();
        Iterator iter = origMap.keySet().iterator();
        Object key;
        while (iter.hasNext()) {
            key = iter.next();
            lowerCaseMap.put(key.toString().toLowerCase(), origMap.get(key));
        }
        return lowerCaseMap;
    }

    /**
     *
     * Meyer / 2003-02-12
     *         修改origFieldNameMap为小写Key的Map, 并按小写Key获取Value
     *         改正由于DAC子类中, getAllObjectsForPOS()方法提供的fieldName与实际
     *         数据库大小写不符导致的bug
     *
     * Meyer / 2003-02-10
     *         Modified: when the fieldNameMap is null,
     *         return the collection for the same fieldNameMap as orignal table
     *
     * Get multiple DAC object with field name converted
     *
     */
    public static List getMultipleObjects(Class classObject, String selectStatement, Map origFieldNameMap) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        DacBase dacObject = null;
        ArrayList collection = new ArrayList();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            for (resultSet = statement.executeQuery(selectStatement); resultSet.next(); collection.add(dacObject)) {
                dacObject = (DacBase) classObject.newInstance();
                if (origFieldNameMap != null) {
                    //Meyer / 2003-02-12
                    Map fieldNameMap = toLowerCaseMap(origFieldNameMap);
                    ResultSetMetaData metaData = resultSet.getMetaData();
                    for (int i = 1; i <= metaData.getColumnCount(); i++) {
                        // Meyer / 2003-02-12
                        String convertedFieldName = (String) fieldNameMap.get(metaData.getColumnName(i).toLowerCase());
                        if (convertedFieldName != null) {
                            Object o = resultSet.getObject(i);
                            //Bruce> Fix a problem when field type is "nvarchar" in MSDE while using
                            //       JDBC-ODBC connection.
                            if (o == null) {
                                try {
                                    o = resultSet.getString(i);
                                } catch (SQLException sqlexception) {
                                }
                            }
                            int t = metaData.getColumnType(i);

                            dacObject.setFieldValue(convertedFieldName, convertType(o, t));
                        }
                      //  System.out.println("-------------------: "+metaData.getColumnName(i).toString());
                    }

                } else {
                    dacObject.constructFromResultSet(resultSet);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
        if (collection != null)
            return collection;
        else
            return null;
    }

    public void clear() {
        fieldMap.clear();
    }

    public abstract String getInsertUpdateTableName();

    public boolean getUseClientSideTableName() {
        return useCleintSideTableName;
    }

    public void setUseClientSideTableName(boolean useCleintSideTableName) {
        this.useCleintSideTableName = useCleintSideTableName;
    }

    /**
     * Give a field list by table created sequence. Used by DacBase.getInsertSqlStatement().
     *
     * @return Iterator represented a field list.
     */
    public Iterator getSequentialFieldList() {
        return null;
    }

    public String getDeleteSqlStatement(boolean useCleintSideTableName) {
        setUseClientSideTableName(true);
        String tableName = getInsertUpdateTableName();
        setUseClientSideTableName(false);
        List primaryKeyList = getPrimaryKeyList();
        String deleteStatement = "delete from " + tableName + " where ";
        for (int i = 0; i < primaryKeyList.size(); i++) {
            String fieldname = (String) primaryKeyList.get(i);
            if (i > 0)
                deleteStatement = deleteStatement + " and ";
            if (fieldMap.get(fieldname) instanceof Date) {
                SimpleDateFormat DF = (SimpleDateFormat) DateFormat.getDateTimeInstance();
                Date d = (Date) fieldMap.get(fieldname);
                deleteStatement = deleteStatement + fieldname + "=" + "'" + DF.format(d).toString() + "'";
            } else {
                deleteStatement = deleteStatement + fieldname + "=" + "'" + fieldMap.get(fieldname) + "'";
            }
        }

        return deleteStatement;
    }

    public String getInsertSqlStatement(boolean useCleintSideTableName) {
        setUseClientSideTableName(true);
        String tableName = getInsertUpdateTableName();
        setUseClientSideTableName(false);
        Iterator iterator = getSequentialFieldList();
        boolean needFieldNames = false;
        // 2003-12-16 By ZhaoHong
        if (iterator == null){
            iterator = fieldMap.keySet().iterator();
            // needFieldNames = true;
        }
        needFieldNames = true;
        Object fieldValue = null;
        String fieldName = "";
        StringBuffer nameParams = new StringBuffer(200);
        StringBuffer valueParams = new StringBuffer(300);

        // 只有在後端而且useCleintSideTableName=true（需要生成client端用的SQL語句）時，才不需檢查欄位是否存在
        boolean dontHaveToCheckFieldExistence = Server.serverExist() && useCleintSideTableName;

        while (iterator.hasNext()) {
            fieldName = (String) iterator.next();

            if (!dontHaveToCheckFieldExistence && !isFieldExist(fieldName))
                continue;

            if (getFieldValue(fieldName) == null) {
                fieldValue = new Object();
                if (!needFieldNames) {
                    valueParams.append('\'');
                    valueParams.append('\'');
                    valueParams.append(',');
                }
            } else {
                fieldValue = getFieldValue(fieldName);
                if (fieldValue instanceof Date) {
                    SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date d = (Date) fieldValue;
                    if (needFieldNames) {
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(',');
                    }

                    if (!useCleintSideTableName && isServerSideOracle)
                        valueParams.append("TO_DATE(");

                    valueParams.append('\'');
                    valueParams.append(DF.format(d).toString());
                    valueParams.append('\'');
                    if (!useCleintSideTableName && isServerSideOracle)
                        valueParams.append(",'YYYY-MM-DD HH24:MI:SS')");
                    valueParams.append(',');

                } else if (fieldValue instanceof String) {
                    if (needFieldNames) {
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(',');
                    }
                    valueParams.append('\'');
                    valueParams.append(escapeSpecialCharacter((String) fieldValue));
                    valueParams.append('\'');
                    valueParams.append(',');
                } else {
                    if (needFieldNames) {
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(',');
                    }
                    valueParams.append('\'');
                    valueParams.append(fieldValue);
                    valueParams.append('\'');
                    valueParams.append(',');
                }
            }
        }
        if (needFieldNames && nameParams.length() == 0 || valueParams.length() == 0)
            return "";
        if (needFieldNames)
            return "insert into " + tableName + "(" + nameParams.substring(0, nameParams.length() - 1) + ")"
            + "values (" + valueParams.substring(0, valueParams.length() - 1) + ")";
        else
            return "insert into " + tableName
            + " values (" + valueParams.substring(0, valueParams.length() - 1) + ")";
    }

    public boolean insert() {
        return insert(true);
    }

    public boolean insert(boolean needQueryAgain) {
        String tableName = getInsertUpdateTableName();
        Iterator iterator = fieldMap.keySet().iterator();
        Connection connection = null;
        Statement statement = null;
        Object fieldValue = null;
        String fieldName = "";
        boolean result = true;
        StringBuffer nameParams = new StringBuffer(200);
        StringBuffer valueParams = new StringBuffer(300);
        while (iterator.hasNext()) {
            fieldName = (String) iterator.next();
            //Meyer/2003-03-11/add field into insert sql statement only when the field existed
            if (isFieldExist(fieldName))
                if (getFieldValue(fieldName) == null) {
                    fieldValue = new Object();
                } else {
                    fieldValue = getFieldValue(fieldName);
                    if (fieldValue instanceof Date) {
                        SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date d = (Date) fieldValue;
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(",");
                        if (isServerSideOracle)
                            valueParams.append("TO_DATE(");
                        valueParams.append("'");
                        valueParams.append(DF.format(d).toString());
                        valueParams.append("'");
                        if (isServerSideOracle)
                            valueParams.append(",'YYYY-MM-DD HH24:MI:SS')");
                        valueParams.append(",");
                    } else if (fieldValue instanceof String) {
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(",");
                        valueParams.append("'");
                        valueParams.append(escapeSpecialCharacter((String) fieldValue));
                        valueParams.append("',");
                    } else {
                        nameParams.append(escapeFieldName(fieldName));
                        nameParams.append(",");
                        valueParams.append("'");
                        valueParams.append(fieldValue.toString());
                        valueParams.append("',");
                    }
                }
        }
        String insertString = "insert into " + tableName + "(" + nameParams.substring(0, nameParams.length() - 1) + ")" + "values (" + valueParams.substring(0, valueParams.length() - 1) + ")";
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.execute(insertString);
        } catch (SQLException e) {
            result = false;
            CreamToolkit.logMessage("ERROR OBJECT=" + this);
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("Statement: " + insertString);
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                result = false;
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("Statement: " + insertString);
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
            if (result && needQueryAgain)
                queryByPrimaryKey();
            flushOSBuffer();
        }
        return result;
    }

    /**
     * Flush OS's buffer by "sync" command. Only on Linux.
     */
    private void flushOSBuffer() {
        try {
            if (!windows)
                Runtime.getRuntime().exec("/bin/sync").waitFor();
        } catch (IOException ioexception) {
        } catch (InterruptedException interruptedexception) {
        }
    }

    /**
     * Delete all records in the table.
     *
     * @return false if failed, true otherwise.
     */
    public boolean deleteAll() {
        Connection connection = null;
        Statement statement = null;
        boolean result = true;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.execute("DELETE FROM " + getInsertUpdateTableName());
        } catch (SQLException e) {
            result = false;
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                result = false;
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
            flushOSBuffer();
        }
        return result;
    }

    public boolean copyFile(String source, String destination) {
        File src = new File(source);
        if (!src.exists())
            return false;
        File dest = new File(destination);
        try {
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(src));
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(dest));
            int c;
            //System.out.println("Copy file " + source + " to " + destination);
            while ((c = in.read()) != -1)
                out.write(c);
            in.close();
            out.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Backup table files. Copy database files from "MySQLDataDirectory" to
     * "MySQLBackupDirectory".
     *
     * @return true if sucessfully backup, false otherwise.
     */
    public boolean backupAll() {
        String dataDir = CreamProperties.getInstance().getProperty("MySQLDataDirectory");
        String backupDir = CreamProperties.getInstance().getProperty("MySQLBackupDirectory");
        if (dataDir == null || backupDir == null) {
            CreamToolkit.logMessage("You must setup MySQLDataDirectory and MySQLBackupDirectory in conf file.");
            CreamProperties.getInstance().setProperty("MySQLDataDirectory", "/var/lib/mysql");
            CreamProperties.getInstance().setProperty("MySQLBackupDirectory", "/tmp");
            CreamProperties.getInstance().deposit();
            dataDir = "/var/lib/mysql";
            backupDir = "/tmp";
        }
        if (!dataDir.endsWith(File.separator))
            dataDir = dataDir + File.separator;
        String url = CreamProperties.getInstance().getProperty("DatabaseURL");
        dataDir = dataDir + url.substring(url.lastIndexOf('/') + 1);
        if (!dataDir.endsWith(File.separator))
            dataDir = dataDir + File.separator;
        if (!backupDir.endsWith(File.separator))
            backupDir = backupDir + File.separator;
        File dir = new File(dataDir);
        File all[] = dir.listFiles(new FileSelector(dir, getInsertUpdateTableName() + "."));

        try {
            for (int i = 0; i < all.length; i++)
                if (!copyFile(all[i].getAbsolutePath(), backupDir + all[i].getName()))
                    return false;

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Restore table files. Copy database files from "MySQLBackupDirectory" to
     * "MySQLDataDirectory".
     *
     * @return true if sucessfully restored, false otherwise.
     */
    public boolean restoreAll() {
        String dataDir = CreamProperties.getInstance().getProperty("MySQLDataDirectory");
        String backupDir = CreamProperties.getInstance().getProperty("MySQLBackupDirectory");
        if (dataDir == null || backupDir == null) {
            CreamToolkit.logMessage("You must setup MySQLDataDirectory and MySQLBackupDirectory in conf file.");
            CreamProperties.getInstance().setProperty("MySQLDataDirectory", "/var/lib/mysql");
            CreamProperties.getInstance().setProperty("MySQLBackupDirectory", "/tmp");
            CreamProperties.getInstance().deposit();
        }
        if (!dataDir.endsWith(File.separator))
            dataDir = dataDir + File.separator;
        String url = CreamProperties.getInstance().getProperty("DatabaseURL");
        dataDir = dataDir + url.substring(url.lastIndexOf('/') + 1);
        if (!dataDir.endsWith(File.separator))
            dataDir = dataDir + File.separator;
        if (!backupDir.endsWith(File.separator))
            backupDir = backupDir + File.separator;
        File dir = new File(backupDir);
        File all[] = dir.listFiles(new FileSelector(dir, getInsertUpdateTableName() + "."));
        for (int i = 0; i < all.length; i++)
            //File currentFile = all[i];
            if (!copyFile(all[i].getAbsolutePath(), dataDir + all[i].getName()))
                return false;

        return true;
    }

    public static Map getScToPosFieldNameMap() {
        return new HashMap();
    }

    public static String[][] getPosToScFieldNameArray() {
        return new String[0][0];
    }

    /**
     * Meyer/2003-03-11/
     *
     */
    protected Collection getExistedFieldList() {
        return null;
    }

    public boolean isFieldExist(String fieldName) {
        boolean result = true;
        Collection existedFieldList = getExistedFieldList();
		if (existedFieldList != null) {
          //case insensitive for oracle   gllg Sep 09 2008 
          //if (isServerSideOracle)
            result = existedFieldList.contains(fieldName.toUpperCase());
          //else
          //  result = existedFieldList.contains(fieldName);

        }
        return result;
    }

    public static boolean isTableExisted(String tableName) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT * FROM " + tableName + " WHERE 1=2");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace();
            }
        }
        return resultSet != null;
    }

    /**
     * Meyer/2003-02-20
     *         增加getExistedFieldList
     *         根据tableName得到fieldNameList
     */
    public static final Collection getExistedFieldList(String tableName) {
        Collection existedFieldList = new ArrayList();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT * FROM " + tableName + " WHERE 1=2");
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++)
                existedFieldList.add(metaData.getColumnName(i).toUpperCase());

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace();
            }
        }
        return existedFieldList;
    }

    /**
     * Retrieve the record data by the format:
     *  "[class_name]\t[first_field]\t[second_field]\t...\n" +
     *  "[class_name]\t[first_field]\t[second_field]\t...\n" +
     *  ...
     */
    public static String getTabSeperatedData(String className, String selectStatement) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        StringBuffer retBuffer = new StringBuffer();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (; resultSet.next(); retBuffer.append('\n')) {
                retBuffer.append(className);
                retBuffer.append('\t');
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    retBuffer.append(resultSet.getString(i));
                    if (i != metaData.getColumnCount())
                        retBuffer.append('\t');
                }

            }

        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
        return retBuffer.toString();
    }

    private static String toStringData(String str) {
        return str != null && str.length() != 0 && !str.equalsIgnoreCase("null") ? str : "";
    }

    private static BigDecimal toBigDecimal(String str) {
        try {
            if (str == null || str.length() == 0 || str.equalsIgnoreCase("null")) {
                return bigDecimal0;
            } else {
                //return new BigDecimal(str).setScale(2);
                return new BigDecimal(str);
            }
        } catch (NumberFormatException e) {
            return bigDecimal0;
        }
    }

    private static Integer toInteger(String str) {
        return str != null && str.length() != 0 && !str.equalsIgnoreCase("null") ? new Integer(str) : integer0;
    }

    private static Boolean toBoolean(String str) {
        return str != null && str.length() != 0 && !str.equalsIgnoreCase("null") ? !str.equalsIgnoreCase("Y") && !str.equals("true") ? Boolean.FALSE : Boolean.TRUE : Boolean.FALSE;
    }

    private static java.sql.Date toDate(String str) {
        if (str == null || str.length() == 0 || str.equalsIgnoreCase("null"))
            return null;
        try {
            Date d = null;
            if (str.length() == 10)
                d = dfyyyyMMdd.parse(str);
            //if (!useJVM)
            //    d = new java.util.Date(d.getTime() + 16 * 60 * 60 * 1000);
            else if (str.length() == 19)
                d = dfyyyyMMddHHmmss.parse(str);
            if (d == null)
                return null;
            else
                return new java.sql.Date(d.getTime());
        } catch (ParseException e) {
            return null;
        }
    }

    private static Time toTime(String str) {
        if (str == null || str.length() == 0 || str.equalsIgnoreCase("null"))
            return null;
        Time t = new Time(0L);
        if (str.lastIndexOf(":") != -1)
            try {
                StringTokenizer st0 = new StringTokenizer(str, ":");
                String hour = st0.nextToken().trim();
                String minute = st0.nextToken().trim();
                String second = "0";
                if (hour.equals("24")) {
                    hour = "23";
                    minute = "59";
                }
                if (!st0.hasMoreTokens()) {
                    second = "0";
                } else {
                    second = st0.nextToken().trim();
                    if (second == null || second.length() == 0)
                        second = "0";
                }
                Date d = new Date(Integer.parseInt(hour) * 60 * 60 * 1000 + Integer.parseInt(minute) * 60 * 1000 + Integer.parseInt(second) * 1000);
                if (!useJVM)
                    d = new Date(d.getTime() + 0x36ee800L);
                t = new Time(d.getTime());
                return t;
            } catch (NoSuchElementException e) {
                CreamToolkit.logMessage(e);
                return t;
            } catch (NumberFormatException e) {
                CreamToolkit.logMessage(e);
                return t;
            }
        try {
            if (str.length() >= 4) {
                String hour = str.substring(0, 2);
                String minute = str.substring(2, 4);
                String second = str.substring(4, 5);
                if (hour.equals("24")) {
                    hour = "23";
                    minute = "59";
                }
                if (second == null || second.length() == 0)
                    second = "00";
                Date d = new Date(Integer.parseInt(hour) * 60 * 60 * 1000 + Integer.parseInt(minute) * 60 * 1000 + Integer.parseInt(second) * 1000);
                if (!useJVM)
                    d = new Date(d.getTime() + 0x36ee800L);
                t = new Time(d.getTime());
            }
        } catch (NumberFormatException e) {
            CreamToolkit.logMessage(e);
        }
        return t;
    }

    private static String nextToken(StringTokenizer st) {
        String token = st.nextToken();
        if (token.equals("\t"))
            return "";
        try {
            st.nextToken();
        } catch (NoSuchElementException nosuchelementexception) {
            // the last token don't have delimiter at end
        }
        return token;
    }

    /**
     * Construct DacBase object from the record data by the format:
     *  "[class_name]\t[first_field]\t[second_field]\t..."
     *
     * @param data record data
     * @param fieldNames a two-dimensional array contains field names and field types.
     *
     * @return DacBase object
     */
    public static DacBase constructFromTabSeperatedData(String data, Object fieldNames[][]) {
        String line = data;
        StringTokenizer tokenizer = new StringTokenizer(line, "\t", true);
        // first token is class name
        if (!tokenizer.hasMoreTokens())
            return null;
        DacBase dacObject;
        try {
            dacObject = (DacBase) Class.forName(tokenizer.nextToken()).newInstance();
            tokenizer.nextToken(); //skip the next delimiter
        } catch (Exception e) {
            return null;
        }
        Map fieldMap = dacObject.getFieldMap();
        fieldMap.clear();
        int i = 0;
        Object valueObject;
        for (; tokenizer.hasMoreTokens(); fieldMap.put((String) fieldNames[i++][0], valueObject)) {
            String value = nextToken(tokenizer);
            valueObject = null;
            if (fieldNames[i][1].equals(java.lang.String.class))
                valueObject = toStringData(value);
            else if (fieldNames[i][1].equals(java.lang.Integer.class))
                valueObject = toInteger(value);
            else if (fieldNames[i][1].equals(java.math.BigDecimal.class))
                valueObject = toBigDecimal(value);
            else if (fieldNames[i][1].equals(java.sql.Date.class))
                valueObject = toDate(value);
            else if (fieldNames[i][1].equals(java.sql.Time.class))
                valueObject = toTime(value);
            else if (fieldNames[i][1].equals(java.lang.Boolean.class))
                valueObject = toBoolean(value);
        }

        return dacObject;
    }

    public void clearFieldMap() {
        fieldMap = null;
    }

    private static Object convertType(Object value, int type) {
        if (type == Types.INTEGER || type == Types.BIGINT) {
            if (value instanceof Long) {
                value = new Integer(((Long) value).intValue());
            }
        }
        return value;
    }

    public String escapeFieldName(String name) {
        // "INOUT" is the keywords in Mysql 5
        if (name.equals("INOUT"))
            return "`" + name + "`";
        else
            return name;
    }


    private class FileSelector implements FilenameFilter {

        private File dir;

        private String name;

        public boolean accept(File dir, String name) {
            return name.trim().startsWith(this.name.trim()) && dir.equals(this.dir);
        }

        public FileSelector(File dir, String name) {
            this.dir = null;
            this.name = null;
            this.dir = dir;
            this.name = name;
        }
    }

    protected static Integer retrieveInteger(Object number) {
        if (number == null)
            return new Integer(0);
        else if (number instanceof Integer)
            return (Integer)number;
        else if (number instanceof Long)
            return new Integer(((Long)number).intValue());
        else if (number instanceof Short)
            return new Integer(((Short)number).intValue());
        else if (number instanceof BigDecimal)
            return new Integer(((BigDecimal)number).intValue());
        else if (number instanceof Byte)
            return new Integer(((Byte) number).intValue());
        else
            return new Integer(0);
    }

    private static class UppercaseMap extends HashMap {
        private static final long serialVersionUID = 1L;

        public Object get(Object o) {
            if (isServerSideOracle)
                return super.get(((String)o).toUpperCase());
            else
                return super.get(o);
        }

        public Object put(Object o, Object o1) {
            if (isServerSideOracle)
                return super.put(((String)o).toUpperCase(), o1);
            else
                return super.put(o, o1);
        }
    }
}
