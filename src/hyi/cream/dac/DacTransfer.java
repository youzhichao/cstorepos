package hyi.cream.dac;

import java.sql.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.io.*;
import java.util.zip.*;
import java.lang.reflect.*;
import java.rmi.*;
import java.rmi.registry.*;
import hyi.cream.POSTerminalApplication;

import hyi.cream.util.*;
//import hyi.rmi.InlineConnector;

public class DacTransfer {
    public static final String encoding          = "GBK";
    public static final int LOCAL                = 100;
    public static final int REMOTE               = LOCAL + 1;
    //float ty = 1.3;
    //byte g = 128;


    private static DacTransfer instance          = null;
    private static Trigger t                     = null;
    private static Map keyMap                           = null;
    private static Properties upload_tables      = null;
    private static Properties download_tables    = null;
//    private static InlineConnector inlineServer  = null;
    private static String RMI_URL                = CreamProperties.getInstance().getProperty("RMI_URL");
    public  final static String path             = CreamToolkit.getConfigDir() + "serialized" + File.separator;
    private static ResourceBundle res            = null;
    private boolean sToZPrompt = false;
    private static ArrayList executable          = null;
    public static final int DATE_ACCDATE         = 200;
    public static final int DATE_BIZDATE         = DATE_ACCDATE + 1;
    public static final int DATE_INFDATE         = DATE_ACCDATE + 2;
    public static final int DATE_ALLDATE         = DATE_ACCDATE + 3;
    private static HashMap showMsgs              = new HashMap();
    static int downloadStatus                    = 0;
    static Object mutex                          = new Object();
    transient static final SimpleDateFormat dfyyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
    transient static final SimpleDateFormat dfyyyyMMddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    static {
        upload_tables = new Properties();
        download_tables = new Properties();

        //Bruce/20030113> Discard upload_tables.conf
        upload_tables.setProperty("tranhead", "posul_tranhead");
        upload_tables.setProperty("trandetail", "posul_trandtl");
        upload_tables.setProperty("shift", "posul_shift");
        upload_tables.setProperty("z", "posul_z");
        upload_tables.setProperty("depsales", "posul_depsales");
        upload_tables.setProperty("daishousales", "posul_daishousales");
        upload_tables.setProperty("cashform", "posul_cashform");

        //try {
        //    upload_tables.load(new FileInputStream(CreamToolkit.getConfigDir() + "upload_tables.conf"));
        //    download_tables.load(new FileInputStream(CreamToolkit.getConfigDir() + "download_tables.conf"));
        //} catch (FileNotFoundException fe) { fe.printStackTrace(); }
        //  catch (IOException ie) { ie.printStackTrace(); }
        executable  = new ArrayList();
        res = CreamToolkit.GetResource();
	    
    }

    public DacTransfer() throws InstantiationException {
        if (instance != null)
            throw new InstantiationException (this.toString());
        else {
            System.setSecurityManager(new RMISecurityManager());
            instance = this;
            //isConnected();
            if (t == null)
                t = Trigger.getInstance();

        }
    }

    /*
    private void init() throws Exception {
        inlineServer = (InlineConnector)Naming.lookup(RMI_URL);
        //System.out.println(inlineServer);
    }
    */

    synchronized public static DacTransfer getInstance() {
        try {
            if (instance == null)
                instance = new DacTransfer();
        } catch (Exception ie){
            ie.printStackTrace(CreamToolkit.getLogger());
            return instance;
        }
        return instance;
    }

    private void exec() {
        for (int i = executable.size() - 1; i >= 0; i--) {
            Runtime currentApp = Runtime.getRuntime();
            String command = (String)executable.get(i);
            StringTokenizer token = new StringTokenizer(command, " ");
            ArrayList commandLine = new ArrayList();
            while (token.hasMoreTokens()) {
                commandLine.add(token.nextToken());
            }
            String [] com = new String [commandLine.size()];
            for(int j = 0; j < commandLine.size(); j++) {
                com[j] = (String)commandLine.get(j);//{"edit.com", "d:\\e.log"};//"c:\\err.log", "a:\\"
            }
            try {
                currentApp.exec(com);
            } catch (IOException e) {
                e.printStackTrace(CreamToolkit.getLogger());
                continue;
            }
            executable.remove(i);
        }
    }
    //End of wrapped methods for RMI Server

    public boolean getStoZPrompt() {
        return sToZPrompt;
    }

    public void setStoZPrompt(boolean sToZPrompt) {
        this.sToZPrompt = sToZPrompt;
    }

    /*
     *Common utility methods for uploading...
     */
    //get the relevant field name mapping between local and remote machine
    private static Map getMapByType(DacBase dac) throws Exception {
        if (!upload_tables.containsKey(dac.getInsertUpdateTableName()))
            throw new Exception ("Invalid upload table:" + dac.getInsertUpdateTableName());
        Properties prop = new Properties();
        String str = dac.getClass().getName();
        str = str.substring(str.lastIndexOf(".") + 1);
        str = str.toLowerCase();
        prop.load(new FileInputStream(CreamToolkit.getConfigDir() + str + ".dac"));
        return prop;
    }

    //Transit the DAC object's map to the new map value
    private static HashMap constructFromDac(DacBase dac) {
        HashMap newMap = new HashMap();
        Map oldMap = dac.getValues();
        try {
            keyMap = getMapByType(dac);
        } catch (Exception e) { e.printStackTrace(); return null;}
        Iterator itr = keyMap.keySet().iterator();
        while (itr.hasNext()) {
            Object keyObj = itr.next();
            if (oldMap.containsKey(keyObj))
                newMap.put(keyMap.get(keyObj),oldMap.get(keyObj));
            else {
                Object value = null;
                try {
                    //System.out.println("get" + keyObj);
                    Method getMethod = dac.getClass().getDeclaredMethod("get" + keyObj,
                        new Class[0]);
                    //System.out.println(getMethod);
                    value = getMethod.invoke(dac, new Object[0]);
                    newMap.put(keyMap.get(keyObj), value);
                }
                catch (NoSuchMethodException e) {
                    //e.printStackTrace(CreamToolkit.getLogger());
                    //CreamToolkit.logMessage("No such method at " + this);
                    continue;
                } catch (InvocationTargetException e) {
                    e.printStackTrace(CreamToolkit.getLogger());
//                    CreamToolkit.logMessage("Invocation target exception at " + this);
                    continue;
                } catch (IllegalAccessException e) {
                    CreamToolkit.logMessage(e.toString());
//                    CreamToolkit.logMessage("Illegal access exception at " + this);
                    continue;
                }

            }
        }
        return newMap;
    }

    private HashMap constructFromResultSet(ResultSet resultSet) throws SQLException {
        HashMap newMap = new HashMap();
        if (resultSet.next()) {
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                Object obj = metaData.getColumnName(i);
                newMap.put(obj.toString().toLowerCase(), resultSet.getObject(i));
            }
        }
        return newMap;
    }

    private ArrayList constructFromResultSet2(ResultSet resultSet) throws SQLException {
        ArrayList table = new ArrayList();
        HashMap newMap = null;//Map nameMap = getFieldNameMapByName(localName);
        while (resultSet.next()) {
            newMap = new HashMap();
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                Object obj = metaData.getColumnName(i);
                newMap.put(obj.toString().toLowerCase(), resultSet.getObject(i));
            }
            table.add(newMap);
        }
        return table;
    }

    //get the new primary key list according to the DAC object
    private List getPrimaryKeyList(DacBase dac) {
        List oldList = dac.getPrimaryKeyList();
        ArrayList newList = new ArrayList();
        try {
            keyMap = getMapByType(dac);
        } catch (Exception e) { }
        ListIterator l = oldList.listIterator();
        while (l.hasNext()) {
            Object keyObj = l.next();
            if (keyMap.containsKey(keyObj))
                newList.add(keyMap.get(keyObj));
            else
                newList.add(keyObj);
        }
        return newList;
    }

    public static void executeQuery(String query) {
        //System.out.println(selectStatement);
        Connection connection  = null;
        Statement  statement   = null;
        try {
            connection = CreamToolkit.getPooledConnection2();
            statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            if (connection != null)
                CreamToolkit.releaseConnection2(connection);
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        }
    }

    public static Map getValueOfStatement(String selectStatement, int location) {
        Connection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        HashMap map = new HashMap();
        try {
            if (location == REMOTE)
                connection = CreamToolkit.getPooledConnection2();
            else
                connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            //System.out.println(resultSet.previous());
            if (resultSet.next()) {
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    map.put(metaData.getColumnName(i), resultSet.getObject(i));
                }
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            e.printStackTrace();//CreamToolkit.getLogger()
        } finally {
            if (connection != null) {
                if (location == REMOTE)
                    CreamToolkit.releaseConnection2(connection);
                else
                    CreamToolkit.releaseConnection(connection);
            }
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                e.printStackTrace(CreamToolkit.getLogger());
            }

            return map;
        }
    }

    public static ArrayList getSingleCollectionOfStatement(String selectStatement, int location) {
        System.out.println(selectStatement);
        Connection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        ArrayList collection   = new ArrayList();
        try {
            if (location == REMOTE)
                connection = CreamToolkit.getPooledConnection2();
            else
                connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            while (resultSet.next()) {
                //HashMap map = new HashMap();
                for (int i = 1; i <= metaData.getColumnCount(); i++)
                    collection.add(resultSet.getObject(i));
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            if (connection != null) {
                if (location == REMOTE)
                    CreamToolkit.releaseConnection2(connection);
                else
                    CreamToolkit.releaseConnection(connection);
            }
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();

            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                e.printStackTrace(CreamToolkit.getLogger());
            }
            return collection;
        }
    }


    public static ArrayList getCollectionOfStatement(String selectStatement, int location) {
        //System.out.println(selectStatement);
        Connection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        ArrayList collection   = new ArrayList();
        try {
            if (location == REMOTE)
                connection = CreamToolkit.getPooledConnection2();
            else
                connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            while (resultSet.next()) {
                HashMap map = new HashMap();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    map.put(metaData.getColumnName(i).toLowerCase(), resultSet.getObject(i));
                }
                collection.add(map);
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            if (connection != null) {
                if (location == REMOTE)
                    CreamToolkit.releaseConnection2(connection);
                else
                    CreamToolkit.releaseConnection(connection);
            }
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                e.printStackTrace(CreamToolkit.getLogger());
            }
            return collection;
        }
    }


    //If a transaction was not uploaded successfully, the application will leave
    //its number as a entry in the list for later uploading.
    public ArrayList getUploadFailedList () {
        ArrayList uList = new ArrayList();
        try {
            BufferedReader buffer = new BufferedReader(new FileReader(CreamToolkit.getConfigDir() + "uploadFailedList.log"));
            while (buffer.ready()) {
                String list = buffer.readLine();
                if (list != null) {
                    if (list.trim().length() == 0)
                        continue;
                    uList.add(list);
                }
            }
        } catch (FileNotFoundException fe) { fe.printStackTrace(); }
          catch (IOException ie) { ie.printStackTrace(); }
        System.out.println(uList);
        return uList;
    }

    public void setUploadFailedList(ArrayList uList) {
        Iterator itr = uList.iterator();
        try {
            BufferedWriter buffer = new BufferedWriter(
                new OutputStreamWriter(
                    new FileOutputStream(
                        CreamToolkit.getConfigDir() + "uploadFailedList.log"), CreamToolkit.getEncoding()));
                //new FileWriter(CreamToolkit.getConfigDir() + "uploadFailedList.log"));

            while (itr.hasNext()) {
                String list =(String)itr.next();
                buffer.write(list.trim() + "\r\n");
            }
            buffer.flush();
        } catch (FileNotFoundException fe) { fe.printStackTrace(); }
          catch (IOException ie) { ie.printStackTrace(); }
    }

    //Detect if there is primary key conflict in database
    public boolean getPrimaryKeyConflict(DacBase dac, int location) {
        String tableName = null;
        if (location == LOCAL)
            tableName = dac.getInsertUpdateTableName();
        else if (location == REMOTE)
            tableName = upload_tables.getProperty(dac.getInsertUpdateTableName());
        if (tableName == null)
            return true;
        String str = "SELECT * FROM " + tableName + " WHERE ";
        List primaryKeyList = null;
        Map fieldMap  = null;
        if (location == LOCAL) {
            primaryKeyList = dac.getPrimaryKeyList();
            fieldMap  = dac.getValues();
        } else {
            primaryKeyList = getPrimaryKeyList(dac);
            fieldMap = constructFromDac(dac);
        }
        for (int i =0; i< primaryKeyList.size(); i++ ) {
            String fieldname = (String)primaryKeyList.get(i);
            if ( i > 0 ) {
                str += " AND ";
            }
            str += fieldname + "=" + "\"" + fieldMap.get(fieldname)+ "\"";
        }
        //System.out.println(str);
        Connection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        boolean conflict = false;
        try {
            if (location == REMOTE)
               connection = CreamToolkit.getPooledConnection2();
            else if (location == LOCAL)
               connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(str);
            if (resultSet == null)
                conflict = false;
            else {
                if (resultSet.next())
                    conflict = true;
                else
                    conflict = false;
            }
        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException1: " + this);
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null) {
                if (location == REMOTE)
                    CreamToolkit.releaseConnection2(connection);
                else
                    CreamToolkit.releaseConnection(connection);
            }
        }
        return conflict;
    }

    private void delete(DacBase dac, int location) {
        String tableName = null;
        if (location == LOCAL)
            tableName = dac.getInsertUpdateTableName();
        else if (location == REMOTE)
            tableName = upload_tables.getProperty(dac.getInsertUpdateTableName());
        if (tableName == null)
            return;
        String str = "DELETE FROM " + tableName + " WHERE ";
        List primaryKeyList = null;
        Map fieldMap  = null;
        if (location == LOCAL) {
            primaryKeyList = dac.getPrimaryKeyList();
            fieldMap  = dac.getValues();
        } else {
            primaryKeyList = getPrimaryKeyList(dac);
            fieldMap = constructFromDac(dac);
        }
        for (int i =0; i< primaryKeyList.size(); i++ ) {
            String fieldname = (String)primaryKeyList.get(i);
            if ( i > 0 ) {
                str += " AND ";
            }
            str += fieldname + "=" + "\"" + fieldMap.get(fieldname)+ "\"";
        }
        //System.out.println(str);
        Connection connection  = null;
        Statement  statement   = null;
        //ResultSet  resultSet   = null;
        try {
            if (location == REMOTE)
               connection = CreamToolkit.getPooledConnection2();
            else if (location == LOCAL)
               connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.executeUpdate(str);
        } catch (SQLException e) {
            CreamToolkit.logMessage("error SQL statement: " + str);
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("error SQL statement: " + str);
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null) {
                if (location == REMOTE)
                    CreamToolkit.releaseConnection2(connection);
                else
                    CreamToolkit.releaseConnection(connection);
            }
        }
    }

    private boolean insert(HashMap fieldMap, String tableName) {
        Iterator iterator               = ((Set)fieldMap.keySet()).iterator();
        //String nameParam                = "";
        //String valueParam               = "";
        Object fieldValue               = null;
        String fieldName                = "";
        //ArrayList valueArray            = new ArrayList();
        boolean result                  = true;
        Connection connection2  = null;
        Statement  statement   = null;
        String nameParams = "";
        String valueParams = "";
        while (iterator.hasNext()) {
            fieldName = (String)iterator.next();
            if (fieldMap.get(fieldName) == null) {
                fieldValue = new Object();
            } else {
                fieldValue = fieldMap.get(fieldName);
                if (fieldValue instanceof java.util.Date) {
                    SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    java.util.Date d = (java.util.Date)fieldValue;
                    nameParams  += fieldName + ",";
                    valueParams += "'" + DF.format(d).toString() + "',";
                } else if (fieldValue instanceof String) {
                    nameParams  += fieldName + ",";
                    valueParams += "'" + fieldValue + "',";
                } else {
                   nameParams  += fieldName + ",";
                   valueParams += "'" + fieldValue + "',";
                }
            }
        }
        String insertString = "INSERT INTO " + tableName + "(" + nameParams.substring(0, nameParams.length()-1) + ")"
            + " VALUES (" + valueParams.substring(0,  valueParams.length()-1) + ")";

        try	{
            connection2 = CreamToolkit.getPooledConnection2();
            statement = connection2.createStatement();
            statement.execute(insertString);
        } catch (SQLException e) {
            result = false;
            CreamToolkit.logMessage("error SQL statement: " + insertString);
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            if (connection2 != null)
                CreamToolkit.releaseConnection2(connection2);
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                result = false;
                CreamToolkit.logMessage("error SQL statement: " + insertString);
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                e.printStackTrace(CreamToolkit.getLogger());
            }
            return result;
        }
    }
    //end of utility

    //Main uploading related methods used by application
    /**
     * Upload the DAC object into remote database
     * @return status code include:
     *    -2 --  not inserted due to conflict
     *    -1 --  no conflict but failed due to other exception
     *     1 --  insert into database successfully after deleted original record due to conflict
     *     0 --  insert into database successfully and no conflict
     */
    public int upload(DacBase dac, boolean rmWhenConflict) { //not update, but insert
        boolean conflict = getPrimaryKeyConflict(dac, REMOTE);
        int status = 0;
        if (conflict) {
            if (rmWhenConflict) {
                delete(dac, REMOTE);
                status = 1;
            } else
                return -2;
        }
        Map fieldMap                    = constructFromDac(dac);
        String tableName                = upload_tables.getProperty(dac.getInsertUpdateTableName());
        Iterator iterator               = ((Set)fieldMap.keySet()).iterator();
        Object fieldValue               = null;
        String fieldName                = "";
        String nameParams = "";
        String valueParams = "";
        Connection connection           = null;
        Statement statement             = null;
        while (iterator.hasNext()) {
            fieldName = (String)iterator.next();
            if (!fieldMap.containsKey(fieldName)) {
                fieldValue = new Object();
            } else {
                fieldValue = fieldMap.get(fieldName);
                if (fieldValue instanceof java.util.Date) {
                    SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    java.util.Date d = (java.util.Date)fieldValue;
                    nameParams  += fieldName + ",";
                    valueParams += "'" + DF.format(d).toString() + "',";
                } else if (fieldValue instanceof String) {
                    nameParams  += fieldName + ",";
                    valueParams += "'" + fieldValue + "',";
                } else {
                   nameParams  += fieldName + ",";
                   valueParams += "'" + fieldValue + "',";
                }
            }
        }
        String insertString = "INSERT INTO " + tableName + "(" + nameParams.substring(0, nameParams.length()-1) + ")"
            + " VALUES (" + valueParams.substring(0,  valueParams.length()-1) + ")";
        //System.out.println(insertString);
        try {
            connection = CreamToolkit.getPooledConnection2();
            statement = connection.createStatement();
            statement.execute(insertString);
        } catch (SQLException e) {
            CreamToolkit.logMessage("error SQL statement: " + insertString);
            status = -1;
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            if (connection != null)
                CreamToolkit.releaseConnection2(connection);
            try {
                if (statement != null)
                    statement.close();

            } catch (SQLException e) {
                status = -1;
                CreamToolkit.logMessage("error SQL statement: " + insertString);
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                e.printStackTrace(CreamToolkit.getLogger());
            }
            return status;
        }
    }

    private Integer getIntValue(String sql, int location) {
        //System.out.println("sql" +  sql);
        Map rmax = getValueOfStatement(sql, location);
        System.out.println(rmax);
        Iterator itr = null;
        Integer r = null;
        if (rmax.isEmpty())
            r = new Integer(-1);
        else {
            itr = rmax.keySet().iterator();
            r = (Integer)rmax.get(itr.next());
            if (r == null)
                r = new Integer(-1);
        }
        return r;
    }

/*
storeID
accountDate
posNumber
zSequenceNumber
*/
    public boolean insertPosAccountDate (ZReport z) {
        HashMap newRec = new HashMap();
        newRec.put("storeID", z.getStoreNumber().trim());
        newRec.put("accountDate", z.getAccountingDate());
        newRec.put("posNumber", z.getTerminalNumber());
        newRec.put("zSequenceNumber", z.getSequenceNumber());
        return insert(newRec, "posaccountdate");
    }


    public int backup(String tableName) throws Exception {
        /*if (2 > 0) {
            return 0;
        }*/
        String dirName = CreamProperties.getInstance().getProperty("MYSQL_BASE");
        if (dirName == null) {
            dirName = "/var/lib/mysql/";
            CreamProperties.getInstance().setProperty("MYSQL_BASE", dirName);
            CreamProperties.getInstance().deposit();
        }
        if (!dirName.endsWith(File.separator))
            dirName += File.separator;
            //DatabaseURL=jdbc\:mysql\://192.168.1.41/cream0608
        String url = CreamProperties.getInstance().getProperty("DatabaseURL");
        dirName += url.substring(url.lastIndexOf('/') + 1);
        if (!dirName.endsWith(File.separator))
            dirName += File.separator;
        /*
        String temp = CreamToolkit.getConfigDir() + "hyitmp";
        File dir = new File(temp);
        if (!dir.exists()) {
            dir.mkdir();
        }
        */
        String tardir = CreamProperties.getInstance().getProperty("MYSQL_BASE");
        if (!tardir.endsWith(File.separator))
            tardir += File.separator;
        //String fileName = dirName + tableName + dirName  + tableName + ".MYI " + dirName + tableName + ".MYD ";
        String command = new String("tar zcvf " + tardir + tableName  + ".tar.gz ");
        File dir = new File(dirName);
        File [] all = dir.listFiles(new FileSelector(dir, tableName));
        for (int i = 0; i < all.length; i++) {
            File currentFile = all[i];
            //System.out.println(currentFile);
            command += currentFile.getAbsolutePath() + " ";
        }
        System.out.println(command);
        StringTokenizer token = new StringTokenizer(command, " ");
        String com [] = new String [token.countTokens()];
        int i = 0;
        while (token.hasMoreTokens()) {
            com [i++] = token.nextToken();
        }
        return exec(com);
    }

    class FileSelector implements FilenameFilter {
        private File dir    = null;
        private String name = null;

        public FileSelector (File dir, String name) {
            this.dir = dir;
            this.name = name;
        }

        public boolean accept(File dir, String name)  {
            if (name.trim().startsWith(this.name.trim())
                && dir.equals(this.dir))
                return true;
            else
                return false;

        }
    }


    void recover() {
        String tardir = CreamProperties.getInstance().getProperty("MYSQL_BASE");
        if (!tardir.endsWith(File.separator))
            tardir += File.separator;
        File root = new File (tardir);
        //if (root == null)
           //root = File.listRoots()[1];
        File [] all = root.listFiles();
        if (all == null)
            return;
        for (int i = 0; i < all.length; i++) {
            File currentFile = all[i];
            if (currentFile.isDirectory()
                || !currentFile.getName().endsWith("tar.gz"))
                continue;
            String command = new String("tar zxvf " + tardir + currentFile.getName() + " -C " + File.separator );
            System.out.println(command);
            StringTokenizer token = new StringTokenizer(command, " ");
            String com [] = new String [token.countTokens()];
            int j = 0;
            while (token.hasMoreTokens()) {
                com [j++] = token.nextToken();
            }
            try {
                if (exec(com) == 0)
                    currentFile.delete();
            } catch (Exception  e) { }
        }
    }

    static int exitStatus  = -10000;

    static int exec(String [] com) throws Exception {
        exitStatus = -10000;
        Runtime currentApp = Runtime.getRuntime();
            Process c = currentApp.exec(com);
            exitStatus = c.waitFor();
            exitStatus = c.exitValue();
        return exitStatus;
    }

    /**
     * Update the current DAC object into remote database(locate the record by
     * the primary key).
     * @return false if failed, true otherwise.
     */
    public boolean update(DacBase dac) {
        Map fieldMap                    = constructFromDac(dac);
        String tableName                = upload_tables.getProperty(dac.getInsertUpdateTableName());
        Iterator iterator               = ((Set)fieldMap.keySet()).iterator();
        Object fieldValue               = null;
        String fieldName                = "";
        boolean result                  = true;
        Connection connection           = null;
        Statement statement             = null;
        String nameParams = "";
        while (iterator.hasNext()) {
            fieldName = (String)iterator.next();
            if (fieldMap.get(fieldName) == null) {
                fieldValue = new Object();
            } else {
                fieldValue = fieldMap.get(fieldName);
                if (fieldValue instanceof java.util.Date) {
                    SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    java.util.Date d = (java.util.Date)fieldValue;
                    nameParams  += fieldName + "=" + "'" + DF.format(d).toString() + "',";
                } else if (fieldValue instanceof String) {
                    nameParams += fieldName + " = " + "'" + fieldValue + "',";
                } else {
                    nameParams += fieldName + " = " +  fieldValue + ",";
                }
            }
        }
        String updateString = "UPDATE " + tableName + " SET " + nameParams.substring(0, nameParams.length()-1) + "  WHERE ";
        List primaryKeyList = getPrimaryKeyList(dac);
        for (int i =0; i< primaryKeyList.size(); i++ ) {
            String fieldname = (String)primaryKeyList.get(i);
            if ( i > 0 ) {
                updateString += " and ";
            }
            updateString += fieldname + "=" + "\"" + fieldMap.get(fieldname)+ "\"";
        }
        //System.out.println(updateString);
        try {
            connection = CreamToolkit.getPooledConnection2();
            statement = connection.createStatement();
            statement.execute(updateString);
        } catch (SQLException e) {
            result = false;
            CreamToolkit.logMessage("error SQL statement: " + updateString);
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                result = false;
                CreamToolkit.logMessage("error SQL statement: " + updateString);
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection2(connection);
            return result;
        }
    }

    public void clear() {
        //Connection connection     = null;
        //Statement statement       = null;
        //ResultSet resultSet       = null;
        //dacObject                 = null;
        keyMap.clear();
    }

    public static void reboot() {
        try {
            hyi.cream.inline.Client.getInstance().processCommand("quit");
            CreamToolkit.releaseAll();
            //{SYSTEM-DEPENDENT}
            String [] re = {"reboot"};
            exec(re);
            System.exit(0);
        } catch (Exception e) {
        }
    }

    public static void shutdown(int s) {
        CreamToolkit.logMessage("The system is going to shutdown in " + s + " seconds!");
        final int se = s * 1000;
        Runnable shut = new Runnable() {
            public void run() {
                CreamToolkit.logMessage("Shutdown thread entered!");
                //if (se == 0)
                //    System.exit(0);
                try {
                    Thread.sleep(se);
                    try {
                        hyi.cream.inline.Client.getInstance().processCommand("quit");
                        CreamToolkit.releaseAll();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception ue) {
                    ue.printStackTrace();
                } finally {
                   System.exit(0);
                }
            }
        };
        new Thread(shut).start();
    }


    public void combine() {
        Map old = getValueOfStatement("select * from posul_z where zsequencenumber = 86", LOCAL);
        Map newV = getValueOfStatement("select * from posul_z where zsequencenumber = 250", LOCAL);
        Iterator itr = old.keySet().iterator();
        HashMap finall = new HashMap();
        while (itr.hasNext()) {
            String next = (String)itr.next();
            if (old.get(next) instanceof BigDecimal) {
                 BigDecimal oldvalue = (BigDecimal)old.get(next);
                 BigDecimal newvalue = (BigDecimal)newV.get(next);
                 BigDecimal finalvalue = newvalue.add(oldvalue.negate());
                 finall.put(next, finalvalue);
            } else {
                 finall.put(next, newV.get(next));
            }
        }
        finall.put("zSequenceNumber", new Integer(251));
        this.insert(finall, "posul_z");
    }
	
    private static String nextToken(StringTokenizer st) {
        String token = st.nextToken();
        if (token.equals("\t")) {
            return "";
        } else {
            try {
                st.nextToken();     // get the delimiter
            } catch (NoSuchElementException e) {
                // the last token don't have delimiter at end
            }
            return token;
        }
    }
	
    private static String toString(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ? "": str);
    }

    private static BigDecimal toBigDecimal(String str) {
        try {
            if (str == null || str.length() == 0 || str.equalsIgnoreCase("null"))
                return new BigDecimal(0);
            else {
                return new BigDecimal(str).setScale(2);
            }
        } catch (NumberFormatException e) {
            return new BigDecimal(0);
        }
    }

    private static Integer toInteger(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ?
            new Integer(0) : new Integer(str));
    }

    private static Long toLong(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ?
            new Long(0) : new Long(str));
    }

	private static Double toDouble(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ?
            new Double(0) : new Double(str));
	}
	
	private static Float toFloat(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ?
            new Float(0) : new Float(str));
	}
	
    private static Boolean toBoolean(String str) {
        return (str == null || str.length() == 0 || str.equalsIgnoreCase("null") ? Boolean.FALSE :
            (str.equalsIgnoreCase("Y") || str.equals("true")) ? Boolean.TRUE : Boolean.FALSE);
    }

    private static java.sql.Date toDate(String str) {
        if (str == null || str.length() == 0 || str.equalsIgnoreCase("null"))
            return null;
        try {
            java.util.Date d = null;
            if (str.length() == 10) {
                d = dfyyyyMMdd.parse(str);
                //if (!useJVM)
                //    d = new java.util.Date(d.getTime() + 16 * 60 * 60 * 1000);
            } else if (str.length() == 19) {
                d = dfyyyyMMddHHmmss.parse(str);
            }
            if (d == null) 
                return null;
            else
                return new java.sql.Date(d.getTime());
        } catch (ParseException e) {
            return null;
        }
    }

    private static java.sql.Time toTime(String str) {
        if (str == null || str.length() == 0 || str.equalsIgnoreCase("null"))
            return null;
        java.sql.Time t = new java.sql.Time(0);
        if (str.lastIndexOf(":") != -1) {
            try {
                StringTokenizer st0 = new StringTokenizer(str, ":");
                String hour = st0.nextToken().trim();
                String minute = st0.nextToken().trim();
                String second = "0";
                if (hour.equals("24")){
                    hour="23";
                    minute="59";
                }   
                if (!st0.hasMoreTokens()) {
                    second="0";   
                } else {
                    second = st0.nextToken().trim();
                    if (second == null || second.length() == 0)
                        second = "0";
                }
                java.util.Date d = new java.util.Date(
                    Integer.parseInt(hour) * 60 * 60 * 1000 +
                    Integer.parseInt(minute) * 60 * 1000 +
                    Integer.parseInt(second) * 1000);
                t = new java.sql.Time(d.getTime());
                return t;
            } catch (NoSuchElementException e) {
                CreamToolkit.logMessage(" nosunchelement "+e);
            } catch(NumberFormatException e2){
                CreamToolkit.logMessage(" numberformatexception "+e2);
            } finally {
                //CreamToolkit.logMessage(" time = "+t);
                return t;
                //return new java.sql.Time(0);
            }
        } else {
            try {
                if (str.length() >= 4) {
                    String hour = str.substring(0, 2);
                    String minute = str.substring(2, 4);
                    String second = str.substring(4,5);
                    if (hour.equals("24")) {
                        hour="23";
                        minute="59";
                    }
                    if (second == null || second.length() == 0)
                        second = "00";
                    java.util.Date d = new java.util.Date(
                        Integer.parseInt(hour) * 60 * 60 * 1000 +
                        Integer.parseInt(minute) * 60 * 1000 +
                        Integer.parseInt(second) * 1000);
                    t = new java.sql.Time(d.getTime());
                    return t;
                }        
            } catch (NumberFormatException e) {
            } finally {
                //CreamToolkit.logMessage("toTime(): time=" +t );
                return t;
                //return new java.sql.Time(0);
            }
        }
    }
	
}

