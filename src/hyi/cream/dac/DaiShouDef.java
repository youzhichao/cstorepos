/*
 * Created on 2003-6-17
 *
 */
package hyi.cream.dac;

import java.io.*;
import java.util.*;
import java.math.*;
import hyi.cream.util.*;

/**
 * @author 
 * 
 */
public class DaiShouDef extends DacBase implements Serializable {

    transient public static final String VERSION = "1.5";
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("ID");
    }

    public DaiShouDef() throws InstantiationException {

    }

    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public String getInsertUpdateTableName() {
        if (getUseClientSideTableName())
            return "daishoudef";
        else if (hyi.cream.inline.Server.serverExist())
            return "posdl_daishoudef";
        else
            return "daishoudef";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posdl_daishoudef";
        else
            return "daishoudef";
    }

    public static DaiShouDef queryByID(String id) {
        DaiShouDef daiShouDef;
        String sel = "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE ID = '" + id + "'";
        daiShouDef = (DaiShouDef) getSingleObject(DaiShouDef.class, sel);

        return daiShouDef;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof DaiShouDef))
            return false;
        DaiShouDef objCp = (DaiShouDef) obj;
        return this.getID().equals(objCp.getID());
    }

    /**
     * 取出所有代收定义项目
     * 
     * @return Collection
     */
    public static Collection getAllDaiShouDefs() {
        String sql = "SELECT * FROM " + getInsertUpdateTableNameStaticVersion();
        Collection dacs = DacBase.getMultipleObjects(DaiShouDef.class, sql, null);

        return dacs;
    }

    /**
     * 从SC 中取出数据为下载到POS系统做准备
     * 
     * @return Collection
     */
    public static Collection getAllObjectsForPOS() {
        String sql = "SELECT * FROM posdl_daishoudef";
        Collection dacs = DacBase.getMultipleObjects(DaiShouDef.class, sql, null);

        return dacs;
    }

    /**
     * 检查 prx 是否是该代收类型的前缀
     * @param prx
     * @return boolean
     */
    public boolean containsPrefix(String prx) {
        String prefix = getPrefix();
        String tmpString, startPrx, endPrx;
        if (prefix == null)
            return true;

        StringTokenizer token1 = new StringTokenizer(prefix, ",");
        while (token1.hasMoreTokens()) {
            tmpString = token1.nextToken();
            //if (tmpString.equals(prx))
            if (prx.startsWith(tmpString))
                return true;
            
            // 1230-1240
            int i = tmpString.indexOf("-");
            if (i != -1) {
                startPrx = tmpString.substring(0, i);
                endPrx = tmpString.substring(i + 1);
                String prxx = prx.substring(0, startPrx.length());
                if (prxx.compareTo(startPrx) >= 0 && prxx.compareTo(endPrx) <= 0)
                    return true;
            }
        }
        return false;
    }

    /**
     * To check if the barcode is valid.
     * @since 2003-06-23
     * @param barcode
     * @return boolean
     */
    public boolean isBarcodeValid(String barcode) {
        String chechStr;
        int beginIndex, endIndex, sum = 0;
        boolean isValid = false;

        String chk = getCDMethod();
        if (chk == null)
            return true;

        beginIndex = getCDStartPos().intValue() - 1;
        if (beginIndex < 0)
            return true;
            
        try {
            switch (chk.charAt(0)) {
                case '1' :
                    /* 校验位前的奇数位数字×3 ＋ 偶数位数字×1 ＋ 校验位之和的个位数为0 */
                    chechStr = barcode.substring(beginIndex, beginIndex + 1);
                    for (int i = 0; i < beginIndex; i++) {
                        if ((i+1) % 2 == 0)
                            sum += barcode.charAt(i) - 48; //'0'的ASCII码为48
                        else
                            sum += 3 * (barcode.charAt(i) - 48);
                    }
                    sum += Integer.parseInt(chechStr);
                    if (sum % 10 == 0)
                        isValid = true;
                    break;

                    /* 校验位前的数字和的最后一位等于校验位*/
                case '2' :
                    chechStr = barcode.substring(beginIndex, beginIndex + 1);
                    for (int i = 0; i < beginIndex; i++)
                        sum += barcode.charAt(i) - 48;
                    if (sum % 10 == Integer.parseInt(chechStr))
                        isValid = true;
                    break;

                    /* 校验位前的数字之和的最后两位等于校验位 */
                case '3' :
                    chechStr = barcode.substring(beginIndex, beginIndex + 2);
                    for (int i = 0; i < beginIndex; i++)
                        sum += barcode.charAt(i) - 48;
                    if (sum % 100 == Integer.parseInt(chechStr))
                        isValid = true;
                    break;
                case '4':  //铁通公司通信费
                    //最后两位为校验位,即33,34位
                    chechStr = barcode.substring(beginIndex, beginIndex + 2);
                    //前32位的权值，计算方式为w[i] = MOD(10*w[i+1], 97)  第32位w[31] = 3.
                    int[] w = {28,61,74,85,57, 93,19,31,71,75,
                               56,25,51,73,17, 89,38,62,45,53,
                               15,50,5, 49,34, 81,76,27,90,9,  30,3};
                    //计算每一位与相应的加权因子的乘积然后求和
                    for (int i = 0; i < 32; i++ ) {
                        sum += (barcode.charAt(i)- 48) * w[i];  
                    }
                    //取模数  用97除上一步计算出来的和
                    int mod = sum % 97;
                    //用98减去模数
                    int check = 98 - mod;                
                    //余数不足两位前面补0即为校验码
                    String s = "0" + check;
                    //如果算出的校验码跟barcode中取出来的相同就是正确的barcode
                    if (s.endsWith(chechStr))
                        isValid = true;
                    break;
                    
                default :
            }
            
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (NumberFormatException e) {
            e.printStackTrace(CreamToolkit.getLogger());
//          System.out.println("  Amount = -1" + barcode) ;
        }
        
        return isValid;

    }

    /******** getters and setters *********************/

    /**
     * 取出输入条码的金额
     */
    public BigDecimal getAmount(String barcode) {
        int beginIndex, endIndex, amountDecimal;
        String amountString;
        
        // 如果不是以数字开头，去掉这一位
        if (barcode.charAt(0) < '0' || barcode.charAt(0) > '9')  
            barcode = barcode.substring(1);
            
        beginIndex = getAmountStartPos().intValue() - 1;
        endIndex = getAmountLen().intValue() + beginIndex;
        amountDecimal = getAmountDecimal().intValue();
        try {
            amountString = barcode.substring(beginIndex, endIndex);
            return new BigDecimal(amountString).movePointLeft(amountDecimal);

        } catch (Exception e) {
            return new BigDecimal(-1);
        }
    }

    public String getID() {
        return (String) getFieldValue("ID");
    }

    public void setID(String id) {
        setFieldValue("ID", id);
    }

    public String getName() {
        return (String) getFieldValue("Name");
    }

    public void setName(String name) {
        setFieldValue("Name", name);
    }

    public String getItemNumber() {
        return (String) getFieldValue("ItemNumber");
    }

    public void setItemNumber(String itemNumber) {
        setFieldValue("ItemNumber", itemNumber);
    }

    public Integer getBarcodeLen() {
        return (Integer) getFieldValue("BarcodeLen");
    }

    public void setBarcodeLen(Integer barcodeLen) {
        setFieldValue("BarcodeLen", barcodeLen);
    }

    public Integer getAmountStartPos() {
        return (Integer) getFieldValue("AmountStartPos");
    }

    public void setAmountStartPos(Integer amountStartPos) {
        setFieldValue("AmountStartPos", amountStartPos);
    }

    public Integer getAmountLen() {
        return (Integer) getFieldValue("AmountLen");
    }

    public void setAmountLen(Integer amountLen) {
        setFieldValue("AmountLen", amountLen);
    }

    public Integer getAmountDecimal() {
        return (Integer) getFieldValue("AmountDecimal");
    }

    public void setAmountDecimal(Integer amountDecimal) {
        setFieldValue("AmountDecimal", amountDecimal);
    }

    public Integer getCDStartPos() {
        return (Integer) getFieldValue("CDStartPos");
    }

    public void setCDStartPos(Integer cdStartPos) {
        setFieldValue("CDStartPos", cdStartPos);
    }

    public String getCDMethod() {
        return (String) getFieldValue("CDMethod");
    }

    public void setCDMethod(Character method) {
        setFieldValue("CDMethod", method);
    }

    public String getPrefix() {
        return (String) getFieldValue("Prefix");
    }

    public void setPrefix(String prefix) {
        setFieldValue("Prefix", prefix);
    }

    public int getPrefixLength() {
        if (getPrefix() == null)
            return 0;
        StringTokenizer token = new StringTokenizer(getPrefix(), ",-");
        if (token.hasMoreTokens())
            return token.nextToken().length();
        else
            return 0;
    }
}
