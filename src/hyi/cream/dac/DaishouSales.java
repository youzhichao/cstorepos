
// Copyright (c) 2000 hyi
package hyi.cream.dac;

import hyi.cream.dac.*;
import hyi.cream.util.*;
import java.util.*;
import java.math.*;
import java.io.*;

/**
 * 代收明细 class.
 * <P>
 * @author dai
 */
public class DaishouSales extends DacBase implements Serializable {

    /**
     * Constructor
     */
    transient public static final String VERSION = "1.5";

    static final String tableName = "daishousales";
    private static ArrayList primaryKeys = new ArrayList();

    static {
        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("storeID");
            primaryKeys.add("accountDate");
            primaryKeys.add("posNumber");
            primaryKeys.add("zSequenceNumber");
            primaryKeys.add("firstNumber");
            primaryKeys.add("secondNumber");
        } else {
            primaryKeys.add("posNumber");
            primaryKeys.add("zSequenceNumber");
            primaryKeys.add("firstNumber");
            primaryKeys.add("secondNumber");
        }
    }

    /**
     * Constructor
     */
    public DaishouSales () throws InstantiationException {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_daishousales";
        else
            return "daishousales";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_daishousales";
        else
            return "daishousales";
    }

    public static DaishouSales queryByFirstNumberAndSecondNumber(String znumber, String s1, String s2) {
        return (DaishouSales)getSingleObject(DaishouSales.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
                " WHERE zSequenceNumber=" + znumber + " AND firstNumber=" +
                s1 + " AND secondNumber=" + s2);
    }

    public static void createDaishouSales(int znumber) {

        //  get firstnumber and firstname from table: reason
        Iterator ite1 = Reason.queryByreasonCategory("08");
        if (ite1 == null) {
            return;
        }
        Map daishou1Map = new HashMap();
        Reason r = null;
        while (ite1.hasNext()) {
            r = (Reason)ite1.next();
            daishou1Map.put(r.getreasonNumber(), r.getreasonName());
        }
        Iterator ite2 = daishou1Map.keySet().iterator();
        if (ite2 == null) {
            return;
        }
        String key = "";
        String value = "";
        String fileName = "";
        while (ite2.hasNext()) {
            key = (String)ite2.next();
            value = (String)daishou1Map.get(key);

            //  get secondnumber and secondname from file: /root/daishou[x].conf
            fileName = "daishou" + Integer.valueOf(key).intValue() + ".conf";
			fileName = CreamToolkit.getConfigDir() + fileName;

            File propFile = new File(fileName);
            ArrayList menuArrayList = new ArrayList();
            try {
                FileInputStream filein = new FileInputStream(propFile);
                InputStreamReader inst = null;
                if (CreamProperties.getInstance().getProperty("Locale").equals("zh_TW")) {
                    inst = new InputStreamReader(filein, "BIG5");
                } else if (CreamProperties.getInstance().getProperty("Locale").equals("zh_CN")) {
                    inst = new InputStreamReader(filein, "GBK");
                }
                BufferedReader in = new BufferedReader(inst);
                String line;
                char ch = ' ';
                int i;

                do {
                    do {
                        line = in.readLine();
                        if (line == null) {
                            break;
                        }
                        while (line.equals("")) {
                            line = in.readLine();
                        }
                        i = 0;
                        do {
                            ch = line.charAt(i);
                            i++;
                        } while ((ch == ' ' || ch == '\t')&& i < line.length());
                    } while (ch == '#' || ch == ' ' || ch == '\t');

                    if (line == null) {
                        break;
                    }
                    menuArrayList.add(line);
                } while (true);
            } catch (FileNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("File is not found: " + propFile + ", at daishousales");
                continue;
            } catch (IOException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("IO exception at daishousales");
                continue;
            }

            for (int j = 0; j < menuArrayList.size(); j++) {
                String line = (String)menuArrayList.get(j);
                StringTokenizer t = new StringTokenizer(line, ",.");
                String key2 = t.nextToken();
                String pluN = t.nextToken();
                String value2 = t.nextToken();

                try {
                    DaishouSales d = new DaishouSales();
                    d.setPosNumber(CreamProperties.getInstance().getProperty("TerminalNumber")); //Bruce
                    d.setAccountDate(new Date());   //Bruce
                    d.setZSequenceNumber(znumber);
                    d.setFirstNumber(key);
                    d.setFirstName(value);
                    d.setSecondNumber(key2);
                    d.setSecondName(value2.trim());
                    d.setPosAmount(new BigDecimal(0));
                    d.setPosCount(0);
                    d.insert();
                } catch (InstantiationException e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }
            }
        }
    }

    public static Iterator getCurrentDaishouSales(int znumber) {
        return getMultipleObjects(DaishouSales.class,
            "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
            " WHERE zSequenceNumber = " + znumber);
    }

    /**
     * only used at the sc side, do nothing for pos side 
     *
     */
    public static void deleteBySequenceNumber(int zNumber, int posNumber) {
        if (!hyi.cream.inline.Server.serverExist()) 
            return;
        String deleteSql = "DELETE FROM " + getInsertUpdateTableNameStaticVersion() 
                          + " WHERE zSequenceNumber =" + zNumber 
                          + " AND posNumber = " + posNumber;    
        executeQuery(deleteSql);
    }

    //  properties
    public void setPosNumber(String posnumber) {
        setFieldValue("posNumber", Integer.valueOf(posnumber));
    }

    public int getPosNumber() {
        return ((Integer)getFieldValue("posNumber")).intValue();
    }

    public void setZSequenceNumber(int znumber) {
        setFieldValue("zSequenceNumber", new Integer(znumber));
    }

    public int getZSequenceNumber() {
        return ((Integer)getFieldValue("zSequenceNumber")).intValue();
    }

    public void setAccountDate(Date accdate) {
        setFieldValue("accountDate", accdate);
    }

    public Date getAccountDate() {
        return (Date)getFieldValue("accountDate");
    }

    public void setFirstNumber(String fnumber) {
        setFieldValue("firstNumber", fnumber);
    }

    public String getFirstNumber() {
        return (String)getFieldValue("firstNumber");
    }

    public void setFirstName(String fname) {
        setFieldValue("firstName", fname);
    }

    public String getFirstName() {
        return (String)getFieldValue("firstName");
    }

    public void setSecondNumber(String snumber) {
        setFieldValue("secondNumber", snumber);
    }

    public String getSecondNumber() {
        return (String)getFieldValue("secondNumber");
    }

    public void setSecondName(String sname) {
        setFieldValue("secondName", sname);
    }

    public String getSecondName() {
        return (String)getFieldValue("secondName");
    }

    public void setPosCount(int count) {
        setFieldValue("posCount", new Integer(count));
    }

    public int getPosCount() {
        return ((Integer)getFieldValue("posCount")).intValue();
    }

    public void setPosAmount(BigDecimal amount) {
        setFieldValue("posAmount", amount);
    }

    public BigDecimal getPosAmount() {
        return (BigDecimal)getFieldValue("posAmount");
    }

    public void setTcpflg(String tcpflg) {
        setFieldValue("tcpflg", tcpflg);
    }

    public String getTcpflg() {
        return (String)getFieldValue("tcpflg");
    }

    //Bruce/2002-4-27

    public static Iterator getUploadFailedList() {
        String failFlag = "2";
        return getMultipleObjects(DaishouSales.class,
            "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
            " WHERE tcpflg='" + failFlag + "'");
    }

    public String getStoreID() {
        return CreamProperties.getInstance().getProperty("StoreNumber");
    }

    public BigDecimal getSCCount() {
        if (hyi.cream.inline.Server.serverExist())
            return (BigDecimal)getFieldValue("scCount");
        else
            return new BigDecimal(0.0);
    }

    public BigDecimal getSCAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (BigDecimal)getFieldValue("scAmount");
        else
            return new BigDecimal(0.0);
    }

	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as String[][] 
	 */
	public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
            {"StoreID", "storeID"},
            {"posNumber", "posNumber"},
            {"zSequenceNumber", "zSequenceNumber"},
            {"accountDate", "accountDate"},
            {"firstNumber", "firstNumber"},
            {"firstName", "firstName"},
            {"secondNumber", "secondNumber"},
            {"secondName", "secondName"},
            {"posCount", "posCount"},
            {"posAmount", "posAmount"},
            {"scCount", "scCount"},
            {"scAmount", "scAmount"}
		};
	}
	
    /**
     * Clone DaishouSales objects for SC with converted field names.
     *
     * This method is only used at POS side.
     */
    public static Object[] cloneForSC(Iterator iter) {
        String[][] fieldNameMap = getPosToScFieldNameArray();

        try {
            ArrayList objArray = new ArrayList();
            while (iter.hasNext()) {
                DaishouSales ds = (DaishouSales)iter.next();
                System.out.println("daishouid1=" + ds.getFirstNumber() + ", daishouid2=" + ds.getSecondNumber());
                DaishouSales clonedDS = new DaishouSales();
                for (int i = 0; i < fieldNameMap.length; i++) {
                    Object value = ds.getFieldValue(fieldNameMap[i][0]);
                    if (value == null) {
                        try {
                            value = ds.getClass().getDeclaredMethod(
                                "get" + fieldNameMap[i][0], new Class[0]).invoke(ds, new Object[0]);
                        } catch (Exception e) {
                            value = null;
                        }
                    }
                    clonedDS.setFieldValue(fieldNameMap[i][1], value);
                }
                objArray.add(clonedDS);
            }
            return objArray.toArray();
        } catch (InstantiationException e) {
            return null;
        }
    }
}
