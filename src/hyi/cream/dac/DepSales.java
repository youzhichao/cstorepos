package hyi.cream.dac;

import java.io.*;
import java.util.*;
import java.math.*;
import java.text.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;

/**
 * Dep definition class.
 *
 * @author Dai, Bruce
 * @version 1.5
 */
public class DepSales extends DacBase implements Serializable {

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public static Object[] cloneForSC(Iterator iter)
     */
    transient public static final String VERSION = "1.5";

    static final String tableName = "depsales";
    private static ArrayList primaryKeys = new ArrayList();
    transient private static ArrayList depArray = new ArrayList();

    static {
        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("storeID");
            primaryKeys.add("accountDate");
            primaryKeys.add("posNumber");
            primaryKeys.add("zSequenceNumber");
            primaryKeys.add("depID");
        } else {
            primaryKeys.add("posNumber");
            primaryKeys.add("sequenceNumber");
            primaryKeys.add("depID");
        }
    }

    /**
     * Constructor
     */
    public DepSales () throws InstantiationException {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_depsales";
        else
            return "depsales";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_depsales";
        else
            return "depsales";
    }

//    public static DepSales queryByDepID(String s) {
//        if (depArray.size() == 0) {
////            SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
////            Iterator ite = getMultipleObjects(DepSales.class,
////                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
////                " WHERE accountDate='" +
////                df.format(CreamToolkit.getInitialDate()) + "'");
//            int curZNumber = ZReport.getCurrentZNumber();
//            Iterator ite = queryBySequenceNumber(new Integer(curZNumber));
//            if (ite != null) {
//                while (ite.hasNext()) {
//                    depArray.add((DepSales)ite.next());
//                }
//            } else {
//                createDepSales(curZNumber );
//            }
//        }
//        DepSales dps = null;
//        for (int i = 0; i < depArray.size(); i++) {
//            dps = (DepSales)depArray.get(i);
//            if (dps.getDepID().equalsIgnoreCase(s)) {
//                break;
//            }
//        }
//        return dps;
//    }

    public static DepSales queryByDepID(Integer zNumber, String depID) {
        DepSales depSales = null;
        if (depArray.size() == 0) {
            Iterator itr = queryBySequenceNumber(zNumber);
            //DepSales 存在
            if (itr != null) {
                while(itr.hasNext()) {
                    DepSales dps = (DepSales)itr.next();
                    depArray.add(dps);
                }
                //没有找到该depID 表明商品的DepID设置有错误
            } else{
                //DepSales 不存在 创建以后再取
                createDepSales(zNumber.intValue());
            }
        } 
    
        for (int i = 0; i < depArray.size(); i++) {
            DepSales dps = (DepSales)depArray.get(i);
            if (dps.getDepID().equalsIgnoreCase(depID)) {
                depSales = dps;
                break;
            }
        }
        
        return depSales;
    }
    
    public static Iterator createDepSales(int znumber) {
        depArray.clear();
        Iterator ite = Dep.getDepIDs();
        String depID = "";
        DepSales dps = null;
        if (ite != null) {
            while (ite.hasNext()) {
                depID = ((Dep)ite.next()).getDepID();
                try {
                    dps = new DepSales();
                    dps.setStoreNumber(CreamProperties.getInstance().getProperty("StoreNumber"));
                    dps.setAccountDate(CreamToolkit.getInitialDate());
                    dps.setDepID(depID);
                    dps.setPOSNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("TerminalNumber")));
                    dps.setSequenceNumber(znumber);
                    dps.setUploadState("0");
                    dps.setGrossSaleTotalAmount(new BigDecimal(0));
                    dps.setDiscountTotalAmount(new BigDecimal(0));
                    dps.setSiPlusTotalAmount(new BigDecimal(0));
                    dps.setNotIncludedTotalSale(new BigDecimal(0));
                    dps.setMixAndMatchTotalAmount(new BigDecimal(0));
                    dps.setNetSaleTotalAmount(new BigDecimal(0));
                    depArray.add(dps);
                    dps.insert();
                } catch (InstantiationException e) {
                    CreamToolkit.logMessage(e + "");
                }
            }
        }
        return depArray.iterator();
    }

    public static boolean updateAll(Date d) {
        DepSales dps = null;
        boolean b = true;
        for (int i = 0; i < depArray.size(); i++) {
            dps = (DepSales)depArray.get(i);
            dps.setAccountDate(d);
            if (!dps.update()) {
                b = false;
            }
        }
        return b;
    }

    public static Iterator queryBySequenceNumber(Integer number) {
        return getMultipleObjects(DepSales.class,
            "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
            " WHERE sequenceNumber = " + number + " ORDER BY depID");
    }

    /**
     * only used at the sc side, do nothing for pos side 
     *
     */
    public static void deleteBySequenceNumber(int zNumber, int posNumber) {
        if (!hyi.cream.inline.Server.serverExist()) 
            return;
        String deleteSql = "DELETE FROM " + getInsertUpdateTableNameStaticVersion() 
                         + " WHERE zSequenceNumber =" + zNumber 
                         + " AND posNumber = " + posNumber;        
        executeQuery(deleteSql);
    }
    
    //  properties

    //storeNumber,storeNumber,CHAR(6),N,店号
    public void setStoreNumber(String s) {
        this.setFieldValue("storeNumber", s);
    }

    public String getStoreNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)this.getFieldValue("storeID");
        else
            return (String)this.getFieldValue("storeNumber");
    }

    //accountDate,accountDate,DATE,N,日结系统日期
    public void setAccountDate(Date d) {
        this.setFieldValue("accountDate", d);
    }

    public Date getAccountDate() {
        if (hyi.cream.inline.Server.serverExist())
            return (Date)this.getFieldValue("accountDate");
        else
            return (Date)this.getFieldValue("accountDate");
    }

    //sequenceNumber,sequenceNumber,INT UNSIGNED,N,Z帐序号
    public void setSequenceNumber(int i) {
        this.setFieldValue("sequenceNumber", new Integer(i));
    }

    public int getSequenceNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return ((Integer)this.getFieldValue("zSequenceNumber")).intValue();
        else
            return ((Integer)this.getFieldValue("sequenceNumber")).intValue();
    }

    //posNumber,posNumber,TINYINT UNSIGNED,N,收银机机号
    public void setPOSNumber(int i) {
        this.setFieldValue("posNumber", new Integer(i));
    }

    public int getPOSNumber() {
        return ((Integer)this.getFieldValue("posNumber")).intValue();
    }

    //uploadState,uploadState,ENUM("0","1"，"2"),N,上传FLG
    public void setUploadState(String s) {
        this.setFieldValue("uploadState", s);
    }

    public String getUploadState() {
        return (String)this.getFieldValue("uploadState");
    }

    //depID,depID,CHAR(2),N,帐务编号
    public void setDepID(String s) {
        this.setFieldValue("depID", s);
    }

    public String getDepID() {
        return (String)this.getFieldValue("depID");
    }

    //grossSaleTotalAmount,grossSaleTotalAmount,DECIMAL(12,2),营业毛额合计
    public void setGrossSaleTotalAmount(BigDecimal b) {
        this.setFieldValue("grossSaleTotalAmount", b);
    }

    public BigDecimal getGrossSaleTotalAmount() {
        return (BigDecimal)this.getFieldValue("grossSaleTotalAmount");
    }

    //siPlusTotalAmount,siPlusTotalAmount,DECIMAL(12,2),SI总加成金额合计
    public void setSiPlusTotalAmount(BigDecimal b) {
        this.setFieldValue("siPlusTotalAmount", b);
    }

    public BigDecimal getSiPlusTotalAmount() {
        return (BigDecimal)this.getFieldValue("siPlusTotalAmount");
    }

    //discountTotalAmount,discountTotalAmount,DECIMAL(12,2),SI总折扣金额合计
    public void setDiscountTotalAmount(BigDecimal b) {
        this.setFieldValue("discountTotalAmount", b);
    }

    public BigDecimal getDiscountTotalAmount() {
        return (BigDecimal)this.getFieldValue("discountTotalAmount");
    }

    //mixAndMatchTotalAmount,mixAndMatchTotalAmount,DECIMAL(12,2),总M&M折让金额合计
    public void setMixAndMatchTotalAmount(BigDecimal b) {
        this.setFieldValue("mixAndMatchTotalAmount", b);
    }

    public BigDecimal getMixAndMatchTotalAmount() {
        return (BigDecimal)this.getFieldValue("mixAndMatchTotalAmount");
    }

    //notIncludedTotalSale,notIncludedTotalSale,DECIMAL(12,2),已开发票支付金额合计
    public void setNotIncludedTotalSale(BigDecimal b) {
        this.setFieldValue("notIncludedTotalSale", b);
    }

    public BigDecimal getNotIncludedTotalSale() {
        return (BigDecimal)this.getFieldValue("notIncludedTotalSale");
    }

    //netSaleTotalAmount,netSaleTotalAmount,DECIMAL(12,2),发票金额合计
    public void setNetSaleTotalAmount(BigDecimal b) {
        this.setFieldValue("netSaleTotalAmount", b);
    }

    public BigDecimal getNetSaleTotalAmount() {
        return (BigDecimal)this.getFieldValue("netSaleTotalAmount");
    }

    public static Object[] getCurrentDepSales() {
        if (depArray.size() == 0) {
            int curZNumber = ZReport.getCurrentZNumber();
            Iterator ite = queryBySequenceNumber(new Integer(curZNumber));

            if (ite != null) {
                while (ite.hasNext()) {
                    depArray.add((DepSales)ite.next());
                }
            } else {
                createDepSales(curZNumber);
            }
        }
        return depArray.toArray();
    }

    //Bruce/2002-1-28
    public static Iterator getUploadFailedList() {
        String failFlag = "2";
        return getMultipleObjects(DepSales.class,
            "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
            " WHERE uploadState='" + failFlag + "'");

    }

	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as String[][] 
	 */
	public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
            {"storeNumber", "storeID"},
            {"accountDate", "accountDate"},
            {"sequenceNumber", "zSequenceNumber"},
            {"posNumber", "posNumber"},
            {"uploadState", "uploadState"},
            {"depID", "depID"},
            {"grossSaleTotalAmount", "grossSaleTotalAmount"},
            {"siPlusTotalAmount", "siPlusTotalAmount"},
            {"discountTotalAmount", "discountTotalAmount"},
            {"mixAndMatchTotalAmount", "mixAndMatchTotalAmount"},
            {"notIncludedTotalSale", "notIncludedTotalSale"},
            {"netSaleTotalAmount", "netSaleTotalAmount"}
		};
	}
	
    /**
     * Clone DepSales objects for SC with converted field names.
     *
     * This method is only used at POS side.
     */
    public static Object[] cloneForSC(Iterator iter) {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            ArrayList objArray = new ArrayList();
            while (iter.hasNext()) {
                DepSales ds = (DepSales)iter.next();
                //System.out.println("depid=" + ds.getDepID());
                DepSales clonedDS = new DepSales();
                for (int i = 0; i < fieldNameMap.length; i++) {
                    Object value = ds.getFieldValue(fieldNameMap[i][0]);
                    if (value == null) {
                        try {
                            value = ds.getClass().getDeclaredMethod(
                                "get" + fieldNameMap[i][0], new Class[0]).invoke(ds, new Object[0]);
                        } catch (Exception e) {
                            value = null;
                        }
                    }
                    clonedDS.setFieldValue(fieldNameMap[i][1], value);
                }
                objArray.add(clonedDS);
            }
            return objArray.toArray();
        } catch (InstantiationException e) {
            return null;
        }
    }
    
    public boolean update() {
    	return super.update();
    }

}
