package hyi.cream.dac;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * Discount type (单品折扣率定义档) class.
 *
 * @author Bruce
 */
public class DiscountType extends DacBase implements Serializable {

    static final String tableName = "distype";
    transient static private Set cache;
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("disNo");
    }

    public DiscountType() throws InstantiationException {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        if (getUseClientSideTableName())
            return "distype";
        else if (hyi.cream.inline.Server.serverExist())
            return "posdl_distype";
        else
            return "distype";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posdl_distype";
        else
            return "distype";
    }

    public static DiscountType queryByID(String id) {
        return (DiscountType)DacBase.getSingleObject(DiscountType.class,
            "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() 
            + " WHERE disNo='" + id + "'");
    }

    public static DiscountType queryByDiscountRate(BigDecimal rate) {
        Iterator iter = DacBase.getMultipleObjects(DiscountType.class,
            "SELECT * FROM " + getInsertUpdateTableNameStaticVersion());
        while (iter != null && iter.hasNext()) {
            DiscountType dis = (DiscountType)iter.next();
            if (rate.compareTo(dis.getUpLimit()) <= 0 
                && rate.compareTo(dis.getLowLimit()) >= 0) {
                return dis;
            }
        }
        return null;
    }

    // properties

    public String getID() {
        return (String)this.getFieldValue("disNo");
    }

    public BigDecimal getUpLimit() {
        return (BigDecimal)this.getFieldValue("disUpLimit");
    }

    public BigDecimal getLowLimit() {
        return (BigDecimal)this.getFieldValue("disLowLimit");
    }


//    /**
//     * Return fieldName map from SC to POS. 
//     */
//    public static Map getScToPosFieldNameMap() {
//        Collection dfs = DacBase.getExistedFieldList(getInsertUpdateTableNameStaticVersion());
//        Map fieldNameMap = new HashMap();
//        Iterator iter = dfs.iterator();
//        while (iter.hasNext()) {
//            String fieldName = (String)iter.next();
//            fieldNameMap.put(fieldName, fieldName);
//        }
//        return fieldNameMap;
//    }

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        return DacBase.getMultipleObjects(hyi.cream.dac.DiscountType.class,
            "SELECT * FROM posdl_distype", null);
    }
}
