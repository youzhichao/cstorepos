/**
 * DAC class
 * @since 2000
 * @author slackware
 */

package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.io.*;

public class ExchangeRate extends DacBase implements Serializable {//Read only
	//RATEID
	//static method and initializer
	private static ArrayList primaryKeys = new ArrayList();
	
	public List getPrimaryKeyList() {
		return  primaryKeys;
	}
	
	static final String tableName = "exchangerate";
	
	static {
		primaryKeys.add("RATEID");
	}
		
	public static ExchangeRate queryByCurrencyID (String ID) {
		return (ExchangeRate)getSingleObject(ExchangeRate.class,
			"SELECT * FROM " + tableName + " WHERE RATEID='" + ID + "'");
	}
	//end
	
	public ExchangeRate () throws InstantiationException {
	}
	//Get properties value:
	public String getCurrencyID() {
		return (String)getFieldValue("RATEID");//CurrencyID	RATEID	CHAR(2)
	}

	public String getCurrencyName() {
		return (String)getFieldValue("NAME");//CurrencyName	NAME VARCHAR(12)
	}

	public BigDecimal getExchangeRate() {
		return (BigDecimal)getFieldValue("RATE");//ExchangeRate	RATE DECIMAL(7,3)
	}


/*
Class Property Name	Field name	Type
CurrencyID RATEID CHAR(2)
CurrencyName NAME VARCHAR(12)
ExchangeRate RATE DECIMAL(7,3)
//*/

	public String getInsertUpdateTableName() {
		return tableName;
	}

}
