package hyi.cream.dac;

import java.math.*;
import java.text.*;
import java.util.*;
import java.io.*;
import hyi.cream.util.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class HourlySales extends DacBase implements Serializable {

	private static ArrayList primaryKeys = new ArrayList();
	
	public List getPrimaryKeyList() {
		return  primaryKeys;
	}

	private static HourlySales currentHourlySales = null;
	
	static final String tableName = "hourlysales";
	
	static {
		primaryKeys.add("TMCODE");
		primaryKeys.add("ZCNT");
	}
		
	public static HourlySales queryBySequenceNumber (String ID) {
		return (HourlySales)getSingleObject(HourlySales.class,
			"SELECT * FROM " + tableName + " WHERE ZCNT='" + ID + "'");
	}                                                      
		
	public static HourlySales queryByTerminalNumber (String ID) {
		return (HourlySales)getSingleObject(HourlySales.class,
			"SELECT * FROM " + tableName + " WHERE TMCODE='" + ID + "'");
	}                   

	public String getInsertUpdateTableName() {
		return tableName;
	}

    /**
     * Constructor
     */
	public HourlySales() throws InstantiationException {
		setAccountingDate(CreamToolkit.getInitialDate());
		setSystemDateTime(new Date());
		setSequenceNumber(Integer.decode(CreamProperties.getInstance().getProperty("ShiftNumber")));
		setStoreNumber(CreamProperties.getInstance().getProperty("StoreNumber"));
		setTerminalNumber(Integer.decode(CreamProperties.getInstance().getProperty("TerminalNumber")));
		setUploadState("0");
	}

	public static HourlySales queryByDateTime(java.util.Date date) {
		SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
		//System.out.println("SELECT * FROM " + tableName + " WHERE SYSDATETIME='" + df.format(date).toString() + "'");
		return (HourlySales )getSingleObject(HourlySales .class,
			"SELECT * FROM " + tableName + " WHERE ACCDATE='" + df.format(date).toString() + "'");
	}

	public static HourlySales getCurrentHourlySales() {
		if (currentHourlySales == null) {
			HourlySales hourly = HourlySales.queryByDateTime(CreamToolkit.getInitialDate());
			if (hourly != null)
				currentHourlySales = hourly;
			else {
				currentHourlySales = createHourlySales();
				currentHourlySales.insert();
			}
		}
		return currentHourlySales;
	}

	public static HourlySales createHourlySales() {
		try {
			currentHourlySales = new HourlySales();
		} catch (InstantiationException ie) { CreamToolkit.logMessage(ie.toString()); }//
		return currentHourlySales;
	}

    /**
     * table's properties
     */

//表头

//AccountingDate
    public Date getAccountingDate() {
        return (Date)getFieldValue("ACCDATE");
    }

    public void setAccountingDate(Date accdate) {
        setFieldValue("ACCDATE", accdate);
    }

//SystemDateTime
    public Date getSystemDateTime() {
        return (Date)getFieldValue("SYSDATETIME");
    }

    public void setSystemDateTime(Date sysdatetime) {
        setFieldValue("SYSDATETIME", sysdatetime);
    }

//SequenceNumber
    public Integer getSequenceNumber() {
        return (Integer)getFieldValue("ZCNT");
    }

    public void setSequenceNumber(Integer zcnt) {
        setFieldValue("ZCNT", zcnt);
    }

//StoreNumber
    public String getStoreNumber() {
        return (String)getFieldValue("STORECODE");
    }

    public void setStoreNumber(String storecode) {
        setFieldValue("STORECODE", storecode);
    }

//TerminalNumber
    public Integer getTerminalNumber() {
        return (Integer)getFieldValue("TMCODE");
    }

    public void setTerminalNumber(Integer tmcode) {
        setFieldValue("TMCODE", tmcode);
    }

//UploadState	TCPFLG	ENUM("0","1","2")	N
	public String getUploadState() {
		return (String)getFieldValue("TCPFLG");
	}

	public void setUploadState(String TcpFlag) {
		 setFieldValue("TCPFLG", TcpFlag);
	}

//时段销售

//Amount00
    public BigDecimal getAmount00() {
        return (BigDecimal)getFieldValue("AMT00");
    }

    public void setAmount00(BigDecimal amt00) {
        setFieldValue("AMT00", amt00);
    }

//Quantity00
    public Integer getQuantity00() {
        return (Integer)getFieldValue("QTY00");
    }

    public void setQuantity00(Integer qty00) {
        setFieldValue("QTY00", qty00);
    }

//Amount01
    public BigDecimal getAmount01() {
        return (BigDecimal)getFieldValue("AMT01");
    }

    public void setAmount01(BigDecimal amt01) {
        setFieldValue("AMT01", amt01);
    }

//Quantity01
    public Integer getQuantity01() {
        return (Integer)getFieldValue("QTY01");
    }

    public void setQuantity01(Integer qty01) {
        setFieldValue("QTY01", qty01);
    }     

//Amount02
    public BigDecimal getAmount02() {
        return (BigDecimal)getFieldValue("AMT02");
    }

    public void setAmount02(BigDecimal amt02) {
        setFieldValue("AMT02", amt02);
    }

//Quantity02
    public Integer getQuantity02() {
        return (Integer)getFieldValue("QTY02");
    }

    public void setQuantity02(Integer qty02) {
        setFieldValue("QTY02", qty02);
    }

//Amount03
    public BigDecimal getAmount03() {
        return (BigDecimal)getFieldValue("AMT03");
    }

    public void setAmount03(BigDecimal amt03) {
        setFieldValue("AMT03", amt03);
    }

//Quantity03
    public Integer getQuantity03() {
        return (Integer)getFieldValue("QTY03");
    }

    public void setQuantity03(Integer qty03) {
        setFieldValue("QTY03", qty03);
    }

//Amount04
    public BigDecimal getAmount04() {
        return (BigDecimal)getFieldValue("AMT04");
    }

    public void setAmount04(BigDecimal amt04) {
        setFieldValue("AMT04", amt04);
    }

//Quantity04
    public Integer getQuantity04() {
        return (Integer)getFieldValue("QTY04");
    }

    public void setQuantity04(Integer qty04) {
        setFieldValue("QTY04", qty04);
    }

//Amount05
    public BigDecimal getAmount05() {
        return (BigDecimal)getFieldValue("AMT05");
    }

    public void setAmount05(BigDecimal amt05) {
        setFieldValue("AMT05", amt05);
    }

//Quantity05
    public Integer getQuantity05() {
        return (Integer)getFieldValue("QTY05");
    }

    public void setQuantity05(Integer qty05) {
        setFieldValue("QTY05", qty05);
    }

//Amount06
    public BigDecimal getAmount06() {
        return (BigDecimal)getFieldValue("AMT06");
    }

    public void setAmount06(BigDecimal amt06) {
        setFieldValue("AMT06", amt06);
    }

//Quantity06
    public Integer getQuantity06() {
        return (Integer)getFieldValue("QTY06");
    }

    public void setQuantity06(Integer qty06) {
        setFieldValue("QTY06", qty06);
    }

//Amount07
    public BigDecimal getAmount07() {
        return (BigDecimal)getFieldValue("AMT07");
    }

    public void setAmount07(BigDecimal amt07) {
        setFieldValue("AMT07", amt07);
    }

//Quantity07
    public Integer getQuantity07() {
        return (Integer)getFieldValue("QTY07");
    }

    public void setQuantity07(Integer qty07) {
        setFieldValue("QTY07", qty07);
    }

//Amount08
    public BigDecimal getAmount08() {
        return (BigDecimal)getFieldValue("AMT08");
    }

    public void setAmount08(BigDecimal amt08) {
        setFieldValue("AMT08", amt08);
    }

//Quantity08
    public Integer getQuantity08() {
        return (Integer)getFieldValue("QTY08");
    }

    public void setQuantity08(Integer qty08) {
        setFieldValue("QTY08", qty08);
    }

//Amount09
    public BigDecimal getAmount09() {
        return (BigDecimal)getFieldValue("AMT09");
    }

    public void setAmount09(BigDecimal amt09) {
        setFieldValue("AMT09", amt09);
    }

//Quantity09
    public Integer getQuantity09() {
        return (Integer)getFieldValue("QTY09");
    }

    public void setQuantity09(Integer qty09) {
        setFieldValue("QTY09", qty09);
    }

//Amount10
    public BigDecimal getAmount10() {
        return (BigDecimal)getFieldValue("AMT10");
    }

    public void setAmount10(BigDecimal amt10) {
        setFieldValue("AMT10", amt10);
    }

//Quantity10
    public Integer getQuantity10() {
        return (Integer)getFieldValue("QTY10");
    }

    public void setQuantity10(Integer qty10) {
        setFieldValue("QTY10", qty10);
    }

//Amount11
    public BigDecimal getAmount11() {
        return (BigDecimal)getFieldValue("AMT11");
    }

    public void setAmount11(BigDecimal amt11) {
        setFieldValue("AMT11", amt11);
    }

//Quantity11
    public Integer getQuantity11() {
        return (Integer)getFieldValue("QTY11");
    }

    public void setQuantity11(Integer qty11) {
        setFieldValue("QTY11", qty11);
    }

//Amount12
    public BigDecimal getAmount12() {
        return (BigDecimal)getFieldValue("AMT12");
    }

    public void setAmount12(BigDecimal amt12) {
        setFieldValue("AMT12", amt12);
    }

//Quantity12
    public Integer getQuantity12() {
        return (Integer)getFieldValue("QTY12");
    }

    public void setQuantity12(Integer qty12) {
        setFieldValue("QTY12", qty12);
    }

//Amount13
    public BigDecimal getAmount13() {
        return (BigDecimal)getFieldValue("AMT13");
    }

    public void setAmount13(BigDecimal amt13) {
        setFieldValue("AMT13", amt13);
    }

//Quantity13
    public Integer getQuantity13() {
        return (Integer)getFieldValue("QTY13");
    }

    public void setQuantity13(Integer qty13) {
        setFieldValue("QTY13", qty13);
    }

//Amount14
    public BigDecimal getAmount14() {
        return (BigDecimal)getFieldValue("AMT14");
    }

    public void setAmount14(BigDecimal amt14) {
        setFieldValue("AMT14", amt14);
    }

//Quantity14
    public Integer getQuantity14() {
        return (Integer)getFieldValue("QTY14");
    }

    public void setQuantity14(Integer qty14) {
        setFieldValue("QTY14", qty14);
    }

//Amount15
    public BigDecimal getAmount15() {
        return (BigDecimal)getFieldValue("AMT15");
    }

    public void setAmount15(BigDecimal amt15) {
        setFieldValue("AMT15", amt15);
    }

//Quantity15
    public Integer getQuantity15() {
        return (Integer)getFieldValue("QTY15");
    }

    public void setQuantity15(Integer qty15) {
        setFieldValue("QTY15", qty15);
    }

//Amount16
    public BigDecimal getAmount16() {
        return (BigDecimal)getFieldValue("AMT16");
    }

    public void setAmount16(BigDecimal amt16) {
        setFieldValue("AMT16", amt16);
    }

//Quantity16
    public Integer getQuantity16() {
        return (Integer)getFieldValue("QTY16");
    }

    public void setQuantity16(Integer qty16) {
        setFieldValue("QTY16", qty16);
    }

//Amount17
    public BigDecimal getAmount17() {
        return (BigDecimal)getFieldValue("AMT17");
    }

    public void setAmount17(BigDecimal amt17) {
        setFieldValue("AMT17", amt17);
    }

//Quantity17
    public Integer getQuantity17() {
        return (Integer)getFieldValue("QTY17");
    }

    public void setQuantity17(Integer qty17) {
        setFieldValue("QTY17", qty17);
    }

//Amount18
    public BigDecimal getAmount18() {
        return (BigDecimal)getFieldValue("AMT18");
    }

    public void setAmount18(BigDecimal amt18) {
        setFieldValue("AMT18", amt18);
    }

//Quantity18
    public Integer getQuantity18() {
        return (Integer)getFieldValue("QTY18");
    }

    public void setQuantity18(Integer qty18) {
        setFieldValue("QTY18", qty18);
    }

//Amount19
    public BigDecimal getAmount19() {
        return (BigDecimal)getFieldValue("AMT19");
    }

    public void setAmount19(BigDecimal amt19) {
        setFieldValue("AMT19", amt19);
    }

//Quantity19
    public Integer getQuantity19() {
        return (Integer)getFieldValue("QTY19");
    }

    public void setQuantity19(Integer qty19) {
        setFieldValue("QTY19", qty19);
    }

//Amount20
    public BigDecimal getAmount20() {
        return (BigDecimal)getFieldValue("AMT20");
    }

    public void setAmount20(BigDecimal amt20) {
        setFieldValue("AMT20", amt20);
    }

//Quantity20
    public Integer getQuantity20() {
        return (Integer)getFieldValue("QTY20");
    }

    public void setQuantity20(Integer qty20) {
        setFieldValue("QTY20", qty20);
    }

//Amount21
    public BigDecimal getAmount21() {
        return (BigDecimal)getFieldValue("AMT21");
    }

    public void setAmount21(BigDecimal amt21) {
        setFieldValue("AMT21", amt21);
    }

//Quantity21
    public Integer getQuantity21() {
        return (Integer)getFieldValue("QTY21");
    }

    public void setQuantity21(Integer qty21) {
        setFieldValue("QTY21", qty21);
    }

//Amount22
    public BigDecimal getAmount22() {
        return (BigDecimal)getFieldValue("AMT22");
    }

    public void setAmount22(BigDecimal amt22) {
        setFieldValue("AMT22", amt22);
    }

//Quantity22
    public Integer getQuantity22() {
        return (Integer)getFieldValue("QTY22");
    }

    public void setQuantity22(Integer qty22) {
        setFieldValue("QTY22", qty22);
    }

//Amount23
    public BigDecimal getAmount23() {
        return (BigDecimal)getFieldValue("AMT23");
    }

    public void setAmount23(BigDecimal amt23) {
        setFieldValue("AMT23", amt23);
    }

//Quantity23
    public Integer getQuantity23() {
        return (Integer)getFieldValue("QTY23");
    }

    public void setQuantity23(Integer qty23) {
        setFieldValue("QTY23", qty23);
	}

	public void setAmtByTime(BigDecimal salesAmt, int qty) {
	    Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat ("HH");
		String nowHour = df.format(date);
		BigDecimal amt = (BigDecimal)getFieldValue("AMT" + nowHour);
		//System.out.println("AMT" + nowHour);
		Integer quantity = (Integer)getFieldValue("QTY" + nowHour);
		/*System.out.println(this.getValues());
		System.out.println(salesAmt);
		System.out.println(amt);*/
		setFieldValue("AMT" + nowHour, salesAmt.add(amt));
		qty = qty + quantity.intValue();
		setFieldValue("QTY" + nowHour, new Integer(qty));
	}

	/*
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();Calendar call = Calendar.getInstance();
		System.out.println(cal.get(cal.SECOND));
		try {
		    Thread.currentThread().sleep(2000);
		} catch (Exception e) {}
		System.out.println(cal.get(cal.SECOND));
		try {
			Thread.currentThread().sleep(2000);
		} catch (Exception e) {}
		
		System.out.println(call.get(cal.SECOND));
	}*/
}

 
