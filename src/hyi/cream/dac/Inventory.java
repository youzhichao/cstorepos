package hyi.cream.dac;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.math.*;

import hyi.cream.util.*;

/**
 * Inventory class.
 *
 * @author Bruce
 * @version 1.0
 */
public class Inventory extends DacBase implements Serializable {

    transient public static final String VERSION = "1.0";

    private static ArrayList primaryKeys = new ArrayList();
    private String description;

    //private String storeID;
    //private Date busiDate;
    private Integer itemSequence;
    //private String stockNumber;
    private String itemNumber;
    private BigDecimal actStockQty;
    private BigDecimal storeUnitPrice;
    private BigDecimal totalPriceAmt;

    static {
        primaryKeys.add("itemSequence");
    }

    public Inventory() throws InstantiationException {
    }

    private BigDecimal trimTrailingZero(BigDecimal x) {
        String xv = x.toString();
        if (xv.lastIndexOf(".") != -1) {
            int i = xv.length() - 1;
            while (xv.charAt(i) == '0') {
                xv = xv.substring(0, i);
                i--;
            }
            return new BigDecimal(xv);
        } else {
            return x;
        }
    }
    
    public Object clone() {
        //Bruce/2003-12-12
        Inventory inv =  queryByItemSequence("" + getItemSequence());
        PLU plu = PLU.queryByInStoreCode("" + inv.getItemNumber());
        if (plu != null)
            inv.setDescription(plu.getScreenName());
        inv.setUnitPrice(inv.getUnitPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
        inv.setActStockQty(trimTrailingZero(inv.getActStockQty()));
        //inv.setAmount(inv.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
        inv.setUpdateDateTime(new Date());
        return inv;
    }

    public String toString() {
        return "Inventory(POS=" + this.getPOSNumber() + ", Seq=" 
            + this.getItemSequence().toString() + ", ItemNo=" 
            + this.getItemNumber() + ", Qty="
            + this.getActStockQty() + ")";
    }
    
    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_inventory";
        else
            return "inventory";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_inventory";
        else
            return "inventory";
    }

    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public static Inventory queryByItemSequence(String itemNo) {
        return (Inventory)getSingleObject(
            Inventory.class,
            "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE itemSequence=" + itemNo);
    }

    public static int queryMaxItemSequence() {
        Integer seq;

        Map map = getValueOfStatement("SELECT MAX(itemSequence) AS ID FROM inventory");
        if (map != null && (seq = (Integer)map.get("ID")) != null)
            return seq.intValue();
        else
            return 0;
    }

    public static int queryNextItemSequence() {
        return queryMaxItemSequence() + 1;
    }

    public static int queryRecordCount() {
        Map map = getValueOfStatement("SELECT COUNT(*) AS RECCNT FROM inventory");
        if (map != null) {
            return ((Integer)map.get("RECCNT")).intValue();
        } else {
            return 0;
        }
    }

    public static int queryRecordCountNotUploaded() {
        Map map = getValueOfStatement("SELECT COUNT(*) AS RECCNT FROM inventory WHERE uploadState != '1'");
        if (map != null) {
            return ((Integer)map.get("RECCNT")).intValue();
        } else {
            return 0;
        }
    }

    public static Iterator queryAll() {
        return getMultipleObjects(Inventory.class, "SELECT * FROM inventory");
    }

    public static Iterator queryUnloadRecords() {
        return getMultipleObjects(Inventory.class, "SELECT * FROM inventory WHERE uploadState != '1'");
    }

    /**
     * 将显示会用到的DAC fields写到class fields，并将原DAC中的field map丢弃，
     * 以节约memory overhead。
     */
    public void discardFieldMap() {
        //storeID = (String)getFieldValue("storeID");
        //busiDate = (Date)getFieldValue("busiDate");
        itemSequence = (Integer)getFieldValue("itemSequence");
        //posNumber = Integer.parseInt(getFieldValue("posNumber").toString());
        //stockNumber = (String)getFieldValue("stockNumber");
        itemNumber = (String)getFieldValue("itemNumber");
        actStockQty = (BigDecimal)getFieldValue("actStockQty");
        storeUnitPrice = (BigDecimal)getFieldValue("storeUnitPrice");
        totalPriceAmt = (BigDecimal)getFieldValue("totalPriceAmt");
        //uploadState = (String)getFieldValue("uploadState");
        
        clearFieldMap();
    }

    // properties

    public String getStoreID() {
        return (String)getFieldValue("storeID");
    }

    public void setStoreID(String storeID) {
        setFieldValue("storeID", storeID);
    }

    public Date getBusiDate() {
        return (Date)getFieldValue("busiDate");
    }

    public void setBusiDate(java.util.Date busiDate) {
        setFieldValue("busiDate", busiDate);
    }

    public Integer getItemSequence() {
        return (itemSequence != null) ? itemSequence : (Integer)getFieldValue("itemSequence");
    }

    public void setItemSequence(int seq) {
        setFieldValue("itemSequence", new Integer(seq));
    }

    public int getPOSNumber() {
        return Integer.parseInt(getFieldValue("posNumber").toString());
    }

    public void setPOSNumber(int posNumber) {
        setFieldValue("posNumber", new Integer(posNumber));
    }

    public String getStockNumber() {
        return (String)getFieldValue("stockNumber");
    }

    public void setStockNumber(String stockNumber) {
        setFieldValue("stockNumber", stockNumber);
    }

    public String getItemNumber() {
        return (itemNumber != null) ? itemNumber : (String)getFieldValue("itemNumber");
    }

    public void setItemNumber(String itemNumber) {
        setFieldValue("itemNumber", itemNumber);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getActStockQty() {
        return (actStockQty != null) ? actStockQty : (BigDecimal)getFieldValue("actStockQty");
    }

    public void setActStockQty(BigDecimal actStockQty) {
        setFieldValue("actStockQty", actStockQty);
    }

    //public BigDecimal getActGiftQTY() {
    //    return (BigDecimal)getFieldValue("actGiftQty");
    //}
    //
    //public void setActGiftQTY(BigDecimal actGiftQty) {
    //    setFieldValue("actGiftQty", actGiftQty);
    //}

    public BigDecimal getUnitPrice() {
        return (storeUnitPrice != null) ? storeUnitPrice : (BigDecimal)getFieldValue("storeUnitPrice");
    }

    public void setUnitPrice(BigDecimal unitprice) {
        setFieldValue("storeUnitPrice", unitprice);
    }

    //public BigDecimal getCost() {
    //    return (BigDecimal)getFieldValue("cost");
    //}
    //
    //public void setCost(BigDecimal cost) {
    //    setFieldValue("cost", cost);
    //}

    public BigDecimal getAmount() {
        return (totalPriceAmt != null) ? totalPriceAmt : (BigDecimal)getFieldValue("totalPriceAmt");
    }

    public void setAmount(BigDecimal amount) {
        setFieldValue("totalPriceAmt", amount);
    }

    public String getUploadState() {
        return (String)getFieldValue("uploadState");
    }

    public void setUploadState(String updateState) {
        setFieldValue("uploadState", updateState);
    }

    public String getUpdateUserID() {
        return (String)getFieldValue("updateUserID");
    }

    public void setUpdateUserID(String updateUserId) {
        setFieldValue("updateUserID", updateUserId);
    }

    public java.util.Date getUpdateDateTime() {
        return (java.util.Date)getFieldValue("updateDateTime");
    }

    public void setUpdateDateTime(java.util.Date updateDateTime) {
        setFieldValue("updateDateTime", updateDateTime);
    }

    public static boolean updateUploadState(String status) {
        Connection connection = null;
        Statement statement = null;
        boolean result = true;

        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.execute("UPDATE inventory SET uploadState='" + status + "' WHERE uploadState<>'1'");
        } catch (SQLException e) {
            result = false;
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                result = false;
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);

            return result;
        }
    }

    /**
     * Clone Inventory objects for SC with converted field names.
     *
     * This method is only used at POS side.
     */
    public static Object[] cloneForSC(Iterator iter) {
        ArrayList objArray = new ArrayList();
        while (iter.hasNext())
            objArray.add(iter.next());

        return objArray.toArray();
    }

    /**
     * Get inventory data by the following format:
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  ...
     */
    public static String getTabSeperatedData() {
        return getTabSeperatedData("hyi.cream.dac.Inventory", 
            "SELECT storeID, busiDate, itemSequence, inventoryNumber, posNumber, stockNumber, itemNumber, actStockQty," +
            "actGiftQTY, storeUnitPrice, cost, totalCostAmt, totalPriceAmt, uploadState, updateUserID, updateDateTime FROM inventory");
    }

    /**
     * Get the unsent inventory data by the following format:
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  ...
     */
    public static String getUnsentTabSeperatedData() {
        return getTabSeperatedData("hyi.cream.dac.Inventory", 
            "SELECT storeID, busiDate, itemSequence, inventoryNumber, posNumber, stockNumber, itemNumber, actStockQty," +
            "actGiftQTY, storeUnitPrice, cost, totalCostAmt, totalPriceAmt, uploadState, updateUserID, updateDateTime FROM inventory" +
            " WHERE uploadState != '1'");
    }

    /**
     * Construct DacBase objects from the record data by the format:
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  "hyi.cream.dac.Inventory\t[1st_field]\t[2nd_field]\t...\n" +
     *  ...
     * 
     * @param sThread ServerThread used to call its processObjectGot() after constructing
     *                 an Inventory object.
     * @param data A String with Inventory data in above format.
     */
    public static void constructFromTabSeperatedData(
        hyi.cream.inline.ServerThread sThread, String data) {

        String line;
        BufferedReader reader = new BufferedReader(new StringReader(data));
        Object[][] inventoryFields = {
            {"storeID", String.class},
            {"busiDate", java.sql.Date.class},
            {"itemSequence", Integer.class},
            {"inventoryNumber", String.class},
            {"posNumber", Integer.class},
            {"stockNumber", String.class},
            {"itemNumber", String.class},
            {"actStockQty", BigDecimal.class},
            {"actGiftQTY", BigDecimal.class},
            {"storeUnitPrice", BigDecimal.class},
            {"cost", BigDecimal.class},
            {"totalCostAmt", BigDecimal.class},
            {"totalPriceAmt", BigDecimal.class},
            {"uploadState", String.class},
            {"updateUserID", String.class},
            {"updateDateTime", java.sql.Date.class},
        };            

        try {
            Object[] inventoryDac = new Object[1];
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("hyi.cream.dac.Inventory"))
                    inventoryDac[0] = constructFromTabSeperatedData(line, inventoryFields);
                if (inventoryDac[0] != null)
                    sThread.processObjectGot(inventoryDac); // this will insert into database
            }
        } catch (IOException e) {
        }
        return;
    }
}
