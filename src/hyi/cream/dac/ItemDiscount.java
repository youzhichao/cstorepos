
package hyi.cream.dac;  

import java.math.*;
import java.util.*;
import java.io.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class ItemDiscount extends DacBase implements Serializable {
	private static ArrayList primaryKeys = new ArrayList();
	
	public List getPrimaryKeyList() {
		return  primaryKeys;
	}
	
	static final String tableName = "ITEMDISCOUNT";
	
	static {
		primaryKeys.add("ID");
	}
		
	public static ItemDiscount queryByID (String ID) {
		return (ItemDiscount)getSingleObject(ItemDiscount.class,
			"SELECT * FROM " + tableName + " WHERE ID='" + ID + "'");
	}

	public String getInsertUpdateTableName() {
		return tableName;
	}

    /**
     * Constructor
     */
    public ItemDiscount() throws InstantiationException {
    }

    /**
     * table's properties
     */ 
	public String getID() {
		return (String)getFieldValue("ID");
	}

	public String getScreenName() {
		return (String)getFieldValue("SNAME");
	}

	public String getPrintName() {
		return (String)getFieldValue("PNAME");
	}

	public String getType() {
		return (String)getFieldValue("TYPE");
	}

	public BigDecimal getValue() {
		return (BigDecimal)getFieldValue("DVALUE");
	}

	public String getBase() {
		return (String)getFieldValue("DBASE");
	}

	public String getAttribute() {
	    return (String)getFieldValue("ATTR");
	}
}

 