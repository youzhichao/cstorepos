package hyi.cream.dac;

import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;

import java.awt.Color;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;
/**
 * Transaction detail DAC class.
 *
 * @author Dai, Slackware, Bruce, Meyer
 * @version 1.6
 */
public class LineItem extends DacBase implements Serializable, Cloneable {

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public LineItem cloneForSC()
     * 
     * Meyer/1.6/2003-01-16/
     *    Add some methods for calculating apportion and present M&M
     *  
     */
    transient public static final String VERSION = "1.6";

    private static ArrayList primaryKeys = new ArrayList();

    private String description      = "";
    transient private boolean printed         = false;
    transient private BigDecimal afterSIAmount= null;
    private Hashtable SIAmtList     = new Hashtable();
    transient private Color lineItemColor     = new Color(255, 255, 255);
    private String siNo             = "";
    private java.util.Date systemDate = CreamToolkit.getInitialDate();
    transient private BigDecimal tada = new BigDecimal(0);
    transient private String tdt = "";
    transient private String tdn = "";
    private int daishouNumber = 0;
    private int daishouDetailNumber = 0;
    private String daishouDetailName = "";
    
    private String daiShouBarcode = "";
    private String daiShouID = "";
    private String descriptionAndSpecification;
    transient private boolean commit;

    private String season;
    private String style;
    private String color;
    private String size;
    private String itemBrand;
    private String sizeCategory;
    private String invCycleNo;
    private String smallUnit;

    //for itemDiscount  gllg
    private transient BigDecimal afterItemDsctUnitPrice = new BigDecimal("0.00");
    
    
    //Meyer/2003-02-10/ 在组合促销中使用
    private int noMatchedQty = Integer.MIN_VALUE; //此明细中，未被组合促销分摊，赠送过的商品数量

//    private static final String daiShouVersion = 
//        CreamProperties.getInstance().getProperty("DaiShouVersion") == null?
//        "" : CreamProperties.getInstance().getProperty("DaiShouVersion");
        
    //Meyer/2003-03-11/
    private static final Collection existedFieldList = getExistedFieldList(getInsertUpdateTableNameStaticVersion());

    static {        
        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("storeID");
            primaryKeys.add("posNumber");
            primaryKeys.add("transactionNumber");
            primaryKeys.add("lineItemSequence");
            primaryKeys.add("systemDate");
        } else {
            primaryKeys.add("TMTRANSEQ");
            primaryKeys.add("TMCODE");
            primaryKeys.add("ITEMSEQ");
        }
    }

    public Collection getExistedFieldList() {
        return existedFieldList;
    }

    public LineItem() throws InstantiationException {
        this.setTaxAmount(new BigDecimal(0));
    }

    public String toString() {
        return "LineItem (pos=" + this.getTerminalNumber() +
            ",no=" + this.getTransactionNumber() +
            ",seq=" + this.getLineItemSequence() + 
            ",unitprice=" + this.getUnitPrice() +
            ",qty=" + this.getQuantity() +
            ",amt=" + this.getAmount() +
            ",aftamt=" + this.getAfterDiscountAmount() + ")";
    }

    public Object clone() {
        return super.clone();
    }

    public java.util.List getPrimaryKeyList() {
        return  primaryKeys;
    }
    
    public static Iterator queryByTransactionNumber(String transactionNumber) {
        Iterator itr = getMultipleObjects(LineItem.class,
            "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE TMTRANSEQ='" + transactionNumber + "'");

        ArrayList newArrayList = new ArrayList();
        if (itr == null)
            return newArrayList.iterator();
        while (itr.hasNext()) {
            LineItem lineItem = (LineItem)itr.next();
            PLU plu = PLU.queryByPluNumber(lineItem.getPluNumber());
            if (plu == null) {
                if (lineItem.getPluNumber() != null) {
                    if (lineItem.getPluNumber().length() > 0) {
                        //'08'：代售项目　'09'：代付项目'10'：PaidIn项目'11'：PaidOut项目
                        String cat = "";
                        //代售
                        if (lineItem.getDetailCode().equals("I"))
                            cat = "08";
                        else if(lineItem.getDetailCode().equals("O")) {
                        	//查询打印历史交易中的代收交易时不再依赖于DaiShouSales2表中的数据。                        	
//                            DaiShouSales2 dss = DaiShouSales2.queryDaiShouSales(
//                                    lineItem.getTransactionNumber().intValue(), 
//                                    lineItem.getLineItemSequence()
//                                    );
//                           if (dss != null) {
//                               DaiShouDef dsd = DaiShouDef.queryByID(dss.getID());
//                               lineItem.setDescription(dsd.getName());
//                               lineItem.setDaiShouBarcode(dss.getBarcode());
//                               lineItem.setDaiShouID(dss.getID());
//                            }
                        	// 代收交易保存時，已經把barcod 保存在 lineItem.DiscountNumber中
                        	//                 把代收ID  保存在 lineItem.PluNumber中
                        	DaiShouDef dsd = DaiShouDef.queryByID(lineItem.getPluNumber());
                        	if (dsd != null) {
                              lineItem.setDescription(dsd.getName());
                              lineItem.setDaiShouBarcode(lineItem.getDiscountNumber());
                              lineItem.setDaiShouID(lineItem.getPluNumber());
                           }
                        }
                        else if (lineItem.getDetailCode().equals("Q"))
                            cat = "09";
                        else if (lineItem.getDetailCode().equals("N"))
                            cat = "10";
                        else if (lineItem.getDetailCode().equals("T"))
                            cat = "11";
                        Reason r = Reason.queryByreasonNumberAndreasonCategory(lineItem.getPluNumber(), cat);
                        if (r != null)
                            lineItem.setDescription(r.getreasonName());
                    } else {
                        /*TaxType t = TaxType.queryByTaxID(lineItem.getTaxType());
                        if (t != null) {
                            lineItem.setDescription(t.getTaxName());
                        }*/
                        Dep dep = Dep.queryByDepID(lineItem.getMicroCategoryNumber());
                        if (dep != null) {
                            lineItem.setDescription(dep.getDepName());
                        }
                    }
                }
            } else {
                String name = CreamProperties.getInstance().getProperty("PluPrintField");
                if ((name == null) || (name == ""))
                    name = "ScreenName";
                if (name.equals("ScreenName"))
                    lineItem.setDescription(plu.getScreenName());
                else
                    lineItem.setDescription(plu.getPrintName());
            }//
            newArrayList.add(lineItem);
        }
        return newArrayList.iterator();
    }

    public static Iterator queryByTransactionNumber(int posNumber, String transactionNumber) {
        Iterator itr = null;
        if (hyi.cream.inline.Server.serverExist())
            itr = getMultipleObjects(LineItem.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
                " WHERE PosNumber=" + posNumber +
                " AND TransactionNumber=" + transactionNumber);
        else
            itr = getMultipleObjects(LineItem.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
                " WHERE TMTRANSEQ='" + transactionNumber + "'");

        ArrayList newArrayList = new ArrayList();
        if (itr == null)
            return newArrayList.iterator();
        while (itr.hasNext()) {
            LineItem lineItem = (LineItem)itr.next();
            PLU plu = PLU.queryByPluNumber(lineItem.getPluNumber());
            if (plu == null) {
                if (lineItem.getPluNumber() != null) {
                    if (lineItem.getPluNumber().length() > 0) {
                        //'08'：代售项目'09'：代付项目'10'：PaidIn项目'11'：PaidOut项目
                        String cat = "";
                        //代售
                        if (lineItem.getDetailCode().equals("I"))
                            cat = "08";
                        else if(lineItem.getDetailCode().equals("O")) {
                         //公共事业费代收
//                        	查询打印历史交易中的代收交易时不再依赖于DaiShouSales2表中的数据。                        	
//                            DaiShouSales2 dss = DaiShouSales2.queryDaiShouSales(
//                                    lineItem.getTransactionNumber().intValue(), 
//                                    lineItem.getLineItemSequence()
//                                    );
//                           if (dss != null) {
//                               DaiShouDef dsd = DaiShouDef.queryByID(dss.getID());
//                               lineItem.setDescription(dsd.getName());
//                               lineItem.setDaiShouBarcode(dss.getBarcode());
//                               lineItem.setDaiShouID(dss.getID());
//                            }
                        	
                        	// 代收交易保存時，已經把barcod 保存在 lineItem.DiscountNumber中
                        	//                 把代收ID  保存在 lineItem.PluNumber中
                        	DaiShouDef dsd = DaiShouDef.queryByID(lineItem.getPluNumber());
                        	if (dsd != null) {
                              lineItem.setDescription(dsd.getName());
                              lineItem.setDaiShouBarcode(lineItem.getDiscountNumber());
                              lineItem.setDaiShouID(lineItem.getPluNumber());
                           }
                        
                        }
                        else if (lineItem.getDetailCode().equals("Q"))
                            cat = "09";
                        else if (lineItem.getDetailCode().equals("N"))
                            cat = "10";
                        else if (lineItem.getDetailCode().equals("T"))
                            cat = "11";
                        Reason r = Reason.queryByreasonNumberAndreasonCategory(lineItem.getPluNumber(), cat);
                        if (r != null)
                            lineItem.setDescription(r.getreasonName());
                    } else {
                        TaxType t = TaxType.queryByTaxID(lineItem.getTaxType());
                        if (t != null) {
                            lineItem.setDescription(t.getTaxName());
                        }
                    }
                }
            } else {
                String name = CreamProperties.getInstance().getProperty("PluPrintField");
                if (name == null)
                    name = "ScreenName";
                if (name.equals("ScreenName"))
                    lineItem.setDescription(plu.getScreenName());
                else
                    lineItem.setDescription(plu.getPrintName());
            }//
            newArrayList.add(lineItem);
        }
        return newArrayList.iterator();
    }

    public LineItem(int i) throws InstantiationException {
        // constructor for doing nothing
    }

    public String getInsertUpdateTableName() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_trandtl";
        else
            return "trandetail";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (hyi.cream.inline.Server.serverExist())
            return "posul_trandtl";
        else
            return "trandetail";
    }

    public boolean getPrinted() {
        return printed;
    }

    public void setPrinted(boolean printed) {
        this.printed = printed;
    }

    public java.util.Date getSystemDateTime() {
        if (hyi.cream.inline.Server.serverExist())
            return (java.util.Date)getFieldValue("systemDate");
        else
            return systemDate;
    }

    public void setSystemDateTime(java.util.Date sd) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("systemDate", sd);
        systemDate = sd;
    }

    public String getStoreID() {
        return CreamProperties.getInstance().getProperty("StoreNumber");
    }

    public BigDecimal getAfterSIAmount() {
        return afterSIAmount;
    }

    public void setAfterSIAmount(BigDecimal amt) {
        afterSIAmount = amt;
    }

    public Hashtable getSIAmtList() {
        return SIAmtList;
    }

    public void setSIAmtList(String siID, BigDecimal amt) {
        if (amt == null || siID == null) {
            SIAmtList.clear();
            return;
        }
        SIAmtList.put(siID, amt);
    }

    public boolean isDiscountedBySI(SI si) {
        boolean b = false;
        String discNo = this.getDiscountNumber();
        if (discNo != null) { 
            StringTokenizer token = new StringTokenizer(discNo, ",");
            while (token.hasMoreTokens()) {
                String strTmp = (String)token.nextToken();
                if (strTmp.startsWith("S")) {
                    String siID = strTmp.substring(1);
                    if (siID.equals(si.getSIID())) {
                        b = true;
                        break;
                    }    
                }
            } //end while
        }
        return b;
    }
    
    public Iterator getSIiterator() {
        if (this.getDetailCode().equals("D"))
           return null;
        ArrayList allSI = new ArrayList();
        PLU plu = PLU.queryByPluNumber(this.getPluNumber());
        Iterator itr = getMultipleObjects(SI.class,"select * from " + SI.getInsertUpdateTableNameStaticVersion());
        while (itr.hasNext()) {
            SI si = (SI)itr.next();
            if (si.contains(plu))
                allSI.add(si);
        }
        return allSI.iterator();
    }

    public void makeNegativeValue() {
        Iterator itr = this.getValues().keySet().iterator();
        while (itr.hasNext()) {
            Object key = itr.next();
            Object value = this.getValues().get(key);
            if (value != null) {
                if (value instanceof BigDecimal) {
                    BigDecimal big = (BigDecimal)value;
                    String field = (String)key;
//                    if (getDetailCode().equals("I")    //代售
//                        || getDetailCode().equals("O") //公共事业费代收   
//                        || getDetailCode().equals("Q")
//                        || getDetailCode().equals("N") 
//                        || getDetailCode().equals("T")) {
//                        if (field.compareToIgnoreCase("UNITPRICE") == 0
//                            || field.compareToIgnoreCase("WEIGHT") == 0)
//                            continue;
//                    } else {
//                        if (field.compareToIgnoreCase("UNITPRICE") == 0
//                            || field.compareToIgnoreCase("WEIGHT") == 0
//                            || field.compareToIgnoreCase("ORIGPRICE") == 0)  //Bruce/20030426
//                            continue;
//                    }
                    if (field.compareToIgnoreCase("UNITPRICE") == 0
                        || field.compareToIgnoreCase("ORIGPRICE") == 0)  
                        continue;
    
                    big = big.negate();
                    setFieldValue(field, big);
                }
            }
        }
    }

    /**
     *       table's properties
     */

    public Integer getTransactionNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("transactionNumber");
        else
            return (Integer)getFieldValue("TMTRANSEQ");
    }

    public void setTransactionNumber(Integer tmtranseq) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("transactionNumber", tmtranseq);
        else
            setFieldValue("TMTRANSEQ", tmtranseq);
    }

    public Integer getTerminalNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (Integer)getFieldValue("posNumber");
        else
            return (Integer)getFieldValue("TMCODE");
    }

    public void setTerminalNumber(Integer tmcode) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("posNumber",tmcode);
        else
            setFieldValue("TMCODE",tmcode);
    }

    /*public int getLineItemSequence() {
        if (getFieldValue("ITEMSEQ") == null)
            return -1;
        if (getFieldValue("ITEMSEQ").getClass() == Integer.class)
            return ((Integer)getFieldValue("ITEMSEQ")).intValue();
        else if (getFieldValue("ITEMSEQ").getClass() == Short.class)
            return ((Short)getFieldValue("ITEMSEQ")).intValue();
        return -1;
    }*/
    public int getLineItemSequence() {
        if (hyi.cream.inline.Server.serverExist()) {
            if(getFieldValue("lineItemSequence") == null)
                return -1;
            return ((Integer)getFieldValue("lineItemSequence")).intValue();
        } else {
            if(getFieldValue("ITEMSEQ") == null)
                return -1;
            return ((Integer)getFieldValue("ITEMSEQ")).intValue();
        }
    }

    int displayNumber = 0;

    public int getDisplayedLineItemSequence() {
        return displayNumber;
    }

    public void setDisplayedLineItemSequence(int no) {
        displayNumber = no;
    }

    public void setLineItemSequence(int itemseq) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("lineItemSequence", new Integer(itemseq));
        else
            setFieldValue("ITEMSEQ", new Integer(itemseq));
    }

    public String getDetailCode() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("detailCode");
        else
            return (String)getFieldValue("CODETX");
    }

    public void setDetailCode(String codetx) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("detailCode", codetx);
        else
            setFieldValue("CODETX", codetx);
    }

    public String getDetailCode1() {
        return (String)getFieldValue("CODER");
    }

    public void setDetailCode1(String coder) {
        setFieldValue("CODER", coder);
    }

    public String getCategoryNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("categoryNumber");
        else
            return (String)getFieldValue("CATNO");
    }

    public void setCategoryNumber(String catno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("categoryNumber", catno);
        else
            setFieldValue("CATNO", catno);
    }

    public String getMidCategoryNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("midCategoryNumber");
        else
            return (String)getFieldValue("MIDCATNO");
    }

    public void setMidCategoryNumber(String midcatno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("midCategoryNumber", midcatno);
        else
            setFieldValue("MIDCATNO", midcatno);
    }

    public String getMicroCategoryNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("microCategoryNumber");
        else
            return (String)getFieldValue("MICROCATNO");
    }

    public void setMicroCategoryNumber(String microcatno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("microCategoryNumber", microcatno);
        else
            setFieldValue("MICROCATNO", microcatno);
    }

    public String getThinCategoryNumber() {
        String value;
        if (hyi.cream.inline.Server.serverExist())
            value = (String)getFieldValue("thinCategoryNumber");
        else
            value = (String)getFieldValue("THINCATNO");
        return (value == null) ? "" : value;
    }

    public void setThinCategoryNumber(String thincatno) {
        String fieldName = (hyi.cream.inline.Server.serverExist()) ? "thinCategoryNumber" : "THINCATNO";
        if (!isFieldExist(fieldName))
            return;
        setFieldValue(fieldName, thincatno);
    }

    public String getPluNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("pluNumber");
        else
            return (String)getFieldValue("PLUNO");
    }

    public void setPluNumber(String pluno) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("pluNumber", pluno);
        else
            setFieldValue("PLUNO", pluno);
    }

    public BigDecimal getUnitPrice() {
        if (hyi.cream.inline.Server.serverExist())
            return (BigDecimal)getFieldValue("unitPrice");
        else
            return (BigDecimal)getFieldValue("UNITPRICE");
    }

    public void setUnitPrice(BigDecimal unitprice) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("unitPrice", unitprice);
        else
            setFieldValue("UNITPRICE", unitprice);
    }

    public BigDecimal getQuantity() {
        if (hyi.cream.inline.Server.serverExist())
            return (BigDecimal)getFieldValue("quantity");
        else
            return (BigDecimal)getFieldValue("QTY");
    }

    public void setQuantity(BigDecimal qty) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("quantity", qty);
        else
            setFieldValue("QTY", qty);

        //Meyer
        this.noMatchedQty = qty.intValue();
    }
    
    
    public BigDecimal getAfterRebateAmount() {
        BigDecimal bd = (BigDecimal)getFieldValue("afterRebateAmount");
        if (bd != null) 
              return bd;
          else
              return new BigDecimal(0);
    }

    public void setAfterRebateAmount(BigDecimal afterRebateAmt) {
        setFieldValue("afterRebateAmount", afterRebateAmt);
    }

    public BigDecimal getRebateAmount() {
        BigDecimal bd = (BigDecimal)getFieldValue("rebateAmount");
        if (bd != null) 
            return bd;
        else
            return new BigDecimal(0);
    }

    public void setRebateAmount(BigDecimal rebateAmt) {
        setFieldValue("rebateAmount", rebateAmt);
    }

    public BigDecimal getAddRebateAmount() {
        BigDecimal bd = (BigDecimal)getFieldValue("addRebateAmount");
        if (bd != null) 
            return bd;
        else
            return new BigDecimal(0);
    }

    public void setAddRebateAmount(BigDecimal addRebateAmt) {
        setFieldValue("addRebateAmount", addRebateAmt);
    }
   
    private BigDecimal rebateRate = new BigDecimal(0.0);
    private BigDecimal originalAfterDiscountAmount;

    public void setRebateRate(BigDecimal rate) {
        rebateRate = rate;
    }
    
    public BigDecimal getRebateRate() {
        return rebateRate;
    }
    
    
    public BigDecimal getAmount() {
        if (getDetailCode() == null)
            return new BigDecimal(0);
        if (getDetailCode().equals("D") || getDetailCode().equals("M")
            || (
                getWeight() != null &&      //Bruce/20030422 fix a bug
                getWeight().compareTo(new BigDecimal(0)) != 0  //when return the value of amount is negative
               )
           ) {
            if (hyi.cream.inline.Server.serverExist())
                return (BigDecimal)getFieldValue("amount");
            else
                return (BigDecimal)getFieldValue("AMT");
        }
        BigDecimal amt = (getUnitPrice().multiply(getQuantity()))
            .setScale(2, BigDecimal.ROUND_HALF_UP);
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("amount", amt);
        else
            setFieldValue("AMT", amt);
        return amt;
    }
    
    //gllg
    public BigDecimal getAfterItemDsctAmount(){
        if(getDiscountType() != null && getDiscountType().equalsIgnoreCase("C"))
            return getAfterDiscountAmount(); //getAfterItemDsctUnitPrice().multiply(getQuantity()).setScale(2, BigDecimal.ROUND_HALF_UP);
        else
            return getAmount();
    }
    
    //gllg
    public BigDecimal getItemDsctAmt(){
            return getOriginalPrice().multiply(getQuantity()).subtract(getAfterDiscountAmount());
    }
    
    //gllg
    public BigDecimal getItemDsctCnt(){
        if ("C".equals(getDiscountType())){
            return getQuantity();
        }
        else
                return bigDecimal0;
    }
    

    public void setAmount(BigDecimal amt) {
        amt = amt.setScale(2, 4);
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("amount", amt);
        else
            setFieldValue("AMT", amt);
        if (getAfterSIAmount() == null) {
           afterSIAmount = amt;
        }
    }

    public String getTaxType() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("taxID");
        else
            return (String)getFieldValue("TAXTYPE");
    }

    public void setTaxType(String taxtype) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxID", taxtype);
        else
            setFieldValue("TAXTYPE", taxtype);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //Add in second iteration
    //    TaxAmount                税额
    public BigDecimal getTaxAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (BigDecimal)getFieldValue("taxAmount");
        else
            return (BigDecimal)getFieldValue("TAXAMT");
    }
    public void setTaxAmount(BigDecimal TaxAmount) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("taxAmount", TaxAmount);
        else
            setFieldValue("TAXAMT", TaxAmount);
    }

    public BigDecimal getAfterDiscountAmount() {
        if (hyi.cream.inline.Server.serverExist())
            return (BigDecimal)getFieldValue("afterDiscountAmount");
        else
            return (BigDecimal)getFieldValue("AFTDSCAMT");
    }

    public void setAfterDiscountAmount(BigDecimal discamt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("afterDiscountAmount", discamt);
        else
            setFieldValue("AFTDSCAMT", discamt);
    }

    public String getDiscountType() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("discountType");
        else
            return (String)getFieldValue("DISCTYPE");
    }

    public void setDiscountType(String disctype) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountType", disctype);
        else
            setFieldValue("DISCTYPE", disctype);
    }

    public String getDiscountNumber() {
        if (hyi.cream.inline.Server.serverExist())
            return (String)getFieldValue("discountID");
        else
            return (String)getFieldValue("DISCNO");
    }

    public void setDiscountNumber(String discountNumber) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("discountID",discountNumber);
        else
            setFieldValue("DISCNO",discountNumber);
    }

    /**
     * 根据DiscountNumber 得到所有折扣类型
     * 其中DiscountNumber的形式为 Mxxx,Mxxx,Sxx,Sxx,Sxx
     * @since 2003-07-15
     * @return ArrayList
     */
    public ArrayList getDiscTypeObjects() {
        ArrayList dos = new ArrayList();
        String discountNumber = getDiscountNumber();
        String id = "";
        StringTokenizer stn = new StringTokenizer(discountNumber,",");
        while (stn.hasMoreTokens()) {
            id = stn.nextToken();
            if (id.startsWith("M")) {
            //暂时不处理组合促销
            } else if (id.startsWith("S")) { //　SI 折扣
                SI si = SI.queryBySIID(id.substring(1));
                if (si != null)
                    dos.add(si);
            }
        }
        return dos;
    }

    public void setDescriptionAndSpecification(String str) {
        descriptionAndSpecification = str;
    }

    public String getDescriptionAndSpecification() {
        if (descriptionAndSpecification != null)
            return descriptionAndSpecification; // get cached one

        if (getPluNumber() == null)
            return "";
        PLU plu = PLU.queryBarCode(this.getPluNumber());
        if (plu == null) {
            if (getDescription() != null) {
                descriptionAndSpecification = getDescription();
                return descriptionAndSpecification;
            } else
                return "";
        } else {
            descriptionAndSpecification = plu.getNameAndSpecification();
            return descriptionAndSpecification;
        }
    }
    
    private void getAdditionalInfo() {
        if (getItemNumber() == null)
            return;
        PLU plu = PLU.queryByItemNumber(getItemNumber());
        if (plu == null) {
            invCycleNo = "";
            sizeCategory = "";
            itemBrand = "";
            size = "";
            color = "";
            style = "";
            season = "";
        } else {
            invCycleNo = plu.getInvCycleNo();
            sizeCategory = plu.getSizeCategory();
            itemBrand = plu.getItemBrand();
            size = plu.getSize();
            color = plu.getColor();
            style = plu.getStyle();
            season = plu.getSeason();
        }
    }

    public String getInvCycleNo() {
        if (invCycleNo != null)
            return invCycleNo;
        getAdditionalInfo();
        return invCycleNo;
    }

    public String getSizeCategory() {
        if (sizeCategory != null)
            return sizeCategory;
        getAdditionalInfo();
        return sizeCategory;
    }

    public String getItemBrand() {
        if (itemBrand != null)
            return itemBrand;
        getAdditionalInfo();
        return itemBrand;
    }

    public String getSize() {
        if (size != null)
            return size;
        getAdditionalInfo();
        return size;
    }

    public String getColor() {
        if (color != null)
            return color;
        getAdditionalInfo();
        return color;
    }

    public String getStyle() {
        if (style != null)
            return style;
        getAdditionalInfo();
        return style;
    }

    public String getSeason() {
        if (season != null)
            return season;
        getAdditionalInfo();
        return season;
    }

    public void setSmallUnit(String value) {
        smallUnit = value;
    }

    public void setInvCycleNo(String value) {
        invCycleNo = value;
    }

    public void setSizeCategory(String value) {
        sizeCategory = value;
    }

    public void setItemBrand(String value) {
        itemBrand = value;
    }

    public void setSize(String value) {
        size = value;
    }

    public void setColor(String value) {
        color = value;
    }

    public void setStyle(String value) {
        style = value;
    }

    public void setSeason(String value) {
        season = value;
    }

    //Removed    ITEMVOID    ENUM("0","1")    N    更正flag
    //ITEMVOID is "1" if this line item has been removed（被更正）.
    //Removed property is true if this line item has been removed.
    public boolean getRemoved() {
        String r = null;
        if (hyi.cream.inline.Server.serverExist())
            r = (String)getFieldValue("itemVoid");
        else
            r = (String)getFieldValue("ITEMVOID");
        if (r == null) {
            return false;
        }
        if (r.length() == 0) {
            return false;
        }
        if (r.equals("1"))
            return true;
        else
            return false;
    }

    public void setRemoved(boolean removed) {
        /*
        if (removed)
            setFieldValue("ITEMVOID", "1");
        else
            setFieldValue("ITEMVOID", "0");
        */
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("itemVoid", ((removed) ? "1" : "0"));
        else
            setFieldValue("ITEMVOID", ((removed) ? "1" : "0"));
    }

    public Color getLineItemColor() {
        return lineItemColor;
    }

    public void setLineItemColor(Color c) {
        lineItemColor = c;
    }

    public String getSINo() {
        return siNo;
    }

    public void setSINo(String sino) {
        this.siNo = sino;
    }

    public BigDecimal getTempAfterDiscountAmount() {
        return tada;
    }

    public void setTempAfterDiscountAmount(BigDecimal discamt) {
        tada = discamt;
    }

    public String getTempDiscountType() {
        return tdt;
    }

    public void setTempDiscountType(String disctype) {
        tdt = disctype;
    }

    public String getTempDiscountNumber() {
        return tdn;
    }

    public void setTempDiscountNumber(String discountNumber) {
        tdn = discountNumber;
    }

    public boolean isOpenPrice() {
        if (this.getPluNumber() == null)
            return false;
        PLU plu = PLU.queryByPluNumber(getPluNumber());
        if (plu == null)
            return false;
        return plu.isOpenPrice();
    }

    public void updateMM() {
        if (tada == null) 
            tada = new BigDecimal(0);
            
        if (this.getAfterDiscountAmount() == null) {
            this.setAfterDiscountAmount(this.getAmount().add(tada));
        } else {
            this.setAfterDiscountAmount(this.getAfterDiscountAmount().add(tada));
        }
        String t = tdt;
        String n = tdn;
        if (t != null && n.length()>0) 
            this.setDiscountType(t);
        
        if (n != null && n.length()>0) { 
            if (this.getDiscountNumber() == null)  
                this.setDiscountNumber("M" + n);    
            else 
                this.setDiscountNumber(this.getDiscountNumber() + ",M" + n);
        }
        
        tada = new BigDecimal(0);
        tdt = "";
        tdn = "";
        //System.out.println("type1 = " + getDiscountType());
    }

    public void setDaishouNumber(int daishouNumber) {
        this.daishouNumber = daishouNumber;
    }

    public int getDaishouNumber() {
        return daishouNumber;
    }

    public LineItem cloneForSC() {
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            LineItem clonedLineItem = new LineItem(0);
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = this.getFieldValue(fieldNameMap[i][0]);
                if (value == null) {
                    try {
                        value = this.getClass().getDeclaredMethod(
                            "get" + fieldNameMap[i][0], new Class[0]).invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedLineItem.setFieldValue(fieldNameMap[i][1], value);
            }
            return clonedLineItem;
        } catch (InstantiationException e) {
            return null;
        }
    }

    static public void deleteOutdatedData() {
        //需要保留的天数
        int tranReserved = Transaction.getTransactionReserved();
        String today = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date());
        long l = java.sql.Date.valueOf(today).getTime() - tranReserved * 1000L * 3600 * 24;
        String baseTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.sql.Date(l));
        
        Connection connection = null;
        Statement statement = null;
        //ResultSet  resultSet = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            
            if (hyi.cream.inline.Server.serverExist()) {
                statement.executeUpdate(
                    "DELETE FROM posul_trandtl "
                  + " WHERE systemDate < '" + baseTime + "'");
            } else {
                Iterator itr = Transaction.getOutdateTranNumbers();
                while (itr != null && itr.hasNext()) {
                    statement.executeUpdate(
                        "DELETE FROM trandetail WHERE TMTRANSEQ = " + (String)itr.next());
                }
            }
            
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
    }

    public void setDaishouDetailNumber(int n1) {
        daishouDetailNumber = n1;
    }

    public int getDaishouDetailNumber() {
        return daishouDetailNumber;
    }

    public void setDaishouDetailName(String s) {
        daishouDetailName = s;
    }

    public String getDaishouDetailName() {
        return daishouDetailName;
    }
    
    /**
     * Meyer / 2003-02-19 / Add field : itemNumber, originalPrice
     *
     */
    public String getItemNumber() {   
        //String
        if (hyi.cream.inline.Server.serverExist()) 
            return (String)getFieldValue("itemNumber");
        else
            return (String)getFieldValue("ITEMNO");
    }

    public void setItemNumber(String anItemNumber) {
        String fieldName = "";
        if (hyi.cream.inline.Server.serverExist()) {

            fieldName = "itemNumber";
        } else {
            fieldName = "ITEMNO";
        }
        if (isFieldExist(fieldName)) {
            setFieldValue(fieldName, anItemNumber);
        }
    }
    
    public BigDecimal getOriginalPrice() {
        if (hyi.cream.inline.Server.serverExist()) 
            return (BigDecimal)getFieldValue("originalPrice");
        else
            return (BigDecimal)getFieldValue("ORIGPRICE");
    }

    public void setOriginalPrice(BigDecimal anOriginalPrice) {
        String fieldName = "";
        if (hyi.cream.inline.Server.serverExist()) {
            fieldName = "originalPrice";
        } else {
            fieldName = "ORIGPRICE";
        }
        
        if (isFieldExist(fieldName)) {
            setFieldValue(fieldName, anOriginalPrice);
        }
    }

    /**
     * Set 折扣率代号。
     * 
     * @param id 折扣率代号
     */
    public void setDiscountRateID(String id) {
        setFieldValue("discountRateID", id);
    }

    /**
     * Get 折扣率代号。
     * 
     * @return 折扣率代号
     */
    public String getDiscountRateID() {
        return (String)getFieldValue("discountRateID");
    }

    /**
     * Set 折扣率。
     * 
     * @param rate 折扣率
     */
    public void setDiscountRate(BigDecimal rate) {
        setFieldValue("discountRate", rate);
    }

    /**
     * Get 折扣率。
     * 
     * @return 折扣率
     */
    public BigDecimal getDiscountRate() {
        return (BigDecimal)getFieldValue("discountRate");
    }

    /**
     * 按金额权重分摊折扣金额，更新交易明细的 "aftDscAmt" - 折让后金额
     * @param leaveApportionQty - int，可供分摊的商品数量
     * @param totalAmt - BigDecimal，分摊涉及的总金额
     * @param totalDiscAmt - BigDecimal，要分摊的总折扣金额
     * @param leaveDiscAmt - BigDecimal，可供分摊的剩余折扣金额
     *
     */
    public int apportion(boolean isLastApportionPlu, int leaveApportionQty, BigDecimal totalAmt,
        BigDecimal totalDiscAmt, BigDecimal leaveDiscAmt) {
        //初始化 noMatchedQty
        initNoMatchedQty();
        int matchedQty = (noMatchedQty > leaveApportionQty) ? leaveApportionQty : noMatchedQty;

        System.out.println("分摊折让: pluCode=" + this.getItemNumber());
        if (isLastApportionPlu && leaveApportionQty == matchedQty) {
            System.out.println("    The Last Apportion Plu");
        }
        System.out.println("    Before: totalQty=" + this.getQuantity() +
            "/leaveQty=" + leaveApportionQty +
            "/totalDiscAmt=" + totalDiscAmt +
            "/leaveDiscAmt=" + leaveDiscAmt);


        noMatchedQty -= matchedQty;
        leaveApportionQty = leaveApportionQty - matchedQty;

        /*
         * 分摊:
         * matchedDiscAmt = ((matchedQty * unitPrice) / totalAmt) * totalDiscAmt
         *
         * 当isLastApportionPlu 并且 noMatchedQty = 0 时，matchedDiscAmt = leaveDiscAmt,
         * 将剩下的折让全部赋给最后一笔交易明细
         */
        BigDecimal matchedDiscAmt = new BigDecimal(0);
        if (isLastApportionPlu && leaveApportionQty == 0) {
            matchedDiscAmt = leaveDiscAmt;
        } else {
            matchedDiscAmt = this.getUnitPrice().multiply(
                new BigDecimal(matchedQty)).divide(
                totalAmt, 4, BigDecimal.ROUND_HALF_UP).multiply(
                totalDiscAmt).setScale(2, BigDecimal.ROUND_HALF_UP);
        }

        leaveDiscAmt = leaveDiscAmt.subtract(matchedDiscAmt);

        System.out.println("    After: matchedQty=" + matchedQty +
            "/leaveQty=" + leaveApportionQty +
            "/matchedDiscAmt=" + matchedDiscAmt +
            "/leaveDiscAmt=" + leaveDiscAmt);

        //update trandetail: TAXAMT, AFTDSCAMT, DISCTYPE
        BigDecimal aftDscAmt = this.getAfterDiscountAmount().subtract(matchedDiscAmt);
        BigDecimal taxAmt = LineItem.calculateTaxAmt(TaxType.queryByTaxID(this.getTaxType()), aftDscAmt);
        this.setAfterDiscountAmount(aftDscAmt);
        this.setTaxAmount(taxAmt);
        this.setDiscountType("M");

        return matchedQty;
    }


    /**
     * 赠送：
     * @param leaveGiftQty - int, 可供赠送的商品数量
     *
     */
    public int present(int leaveGiftQty) {
        System.out.println("赠送：pluCode=" + this.getItemNumber());
        System.out.println("    Before: totalQty=" + this.getQuantity() +
            "/leaveGiftQty=" + leaveGiftQty);
        //初始化 noMatchedQty
        initNoMatchedQty();
        int matchedQty = (noMatchedQty > leaveGiftQty) ? leaveGiftQty : noMatchedQty;
        BigDecimal matchedGiftAmt = new BigDecimal(0);
        matchedGiftAmt = this.getUnitPrice().multiply(
            new BigDecimal(matchedQty)).setScale(2,BigDecimal.ROUND_HALF_UP);
        noMatchedQty -= matchedQty;
        leaveGiftQty = leaveGiftQty - matchedQty;

        System.out.println("    After: matchedQty=" + matchedQty +
            "/leaveGiftQty=" + leaveGiftQty +
            "/matchedGiftAmt=" + matchedGiftAmt);

        //update trandetail: TAXAMT, AFTDSCAMT, DISCTYPE
        BigDecimal aftDscAmt = this.getAfterDiscountAmount().subtract(matchedGiftAmt);

        BigDecimal taxAmt = LineItem.calculateTaxAmt(TaxType.queryByTaxID(this.getTaxType()), aftDscAmt);
        this.setAfterDiscountAmount(aftDscAmt);
        this.setTaxAmount(taxAmt);
        this.setDiscountType("M");
        
        return matchedQty;
    }

    /**
     * 根据taxType与amt 计算taxAmt
     */
    public static BigDecimal calculateTaxAmt(TaxType taxType, BigDecimal amt) {
        BigDecimal taxAmt = new BigDecimal("0");        
        
        int round = 0;
        String taxRound = taxType.getRound();
        if (taxRound.equalsIgnoreCase("R")) {
            round = BigDecimal.ROUND_HALF_UP;
        } else if (taxRound.equalsIgnoreCase("C")) {
            round = BigDecimal.ROUND_UP;
        } else if (taxRound.equalsIgnoreCase("D")) {
            round = BigDecimal.ROUND_DOWN;
        }
        BigDecimal taxPercent = taxType.getPercent();
        int taxDigit = taxType.getDecimalDigit().intValue();
        
        String type = taxType.getType();
        if (type.equalsIgnoreCase("1")) {
            taxAmt = (amt.multiply(taxPercent)).divide(
                new BigDecimal("1").add(taxPercent), 2
            ).setScale(taxDigit, round);
        } else if (type.equalsIgnoreCase("2")) {
            taxAmt = (amt.multiply(taxPercent)).setScale(taxDigit, round);
        } else {
            taxAmt = new BigDecimal("0.00").setScale(taxDigit, round);
        }
        return taxAmt;
    }


    /**
     * Init the noMatchedQty.
     */
    public void initNoMatchedQty() {
        if (noMatchedQty == Integer.MIN_VALUE) {
            this.noMatchedQty = this.getQuantity().intValue();
        }
    }

    /*
     *
     *
     *
     */
    public void resetNoMatchedQty() {
        this.noMatchedQty = this.getQuantity().intValue();
    }

    private static String[][] posToScFieldNameArray;

    /**
     * Meyer/2003-02-20
     * return fieldName map of PosToSc as String[][]
     */
    public static  String[][] getPosToScFieldNameArray() {
        if (posToScFieldNameArray == null) {
            posToScFieldNameArray = new String[][] {
                {"StoreID", "storeID"},
                {"SystemDateTime", "systemDate"},
                {"TMCODE", "posNumber"},
                {"TMTRANSEQ", "transactionNumber"},
                {"ITEMSEQ", "lineItemSequence"},
                {"CODETX", "detailCode"},
                {"ITEMVOID", "itemVoid"},
                {"CATNO", "categoryNumber"},
                {"MIDCATNO", "midCategoryNumber"},
                {"MICROCATNO", "microCategoryNumber"},
                {"THINCATNO", "thinCategoryNumber"},
                {"PLUNO", "pluNumber"},
                {"UNITPRICE", "unitPrice"},
                {"QTY", "quantity"},
                {"WEIGHT", "weight"},
                {"DISCNO", "discountID"},
                {"AMT", "amount"},
                {"TAXTYPE", "taxID"},
                {"TAXAMT", "taxAmount"},
                //Meyer / 2003-02-19 / Add 2 Field

                {"ITEMNO", "itemNumber"},
                {"ORIGPRICE", "originalPrice"},
                //Meyer/2003-03-10/add 1 field
                {"DISCTYPE", "discountType"},
                //Meyer/2003-03-11/Add 1 field
                {"AFTDSCAMT","afterDiscountAmount"},
                {"discountRateID","discountRateID"},
                {"discountRate","discountRate"},
                {"rebateAmount", "rebateAmount"},
                {"addRebateAmount", "addRebateAmount"},
                {"afterRebateAmount", "afterRebateAmount"}
            };
        }
        return posToScFieldNameArray;
    }

    public BigDecimal getDiscountAmount() {
        BigDecimal quantity = getQuantity();
        BigDecimal originalPrice = getOriginalPrice();
        if(getDiscountType() != null && getDiscountType().equalsIgnoreCase("C"))
            return getOriginalPrice().multiply(getQuantity()).add(getAfterDiscountAmount().negate());
        else
            return originalPrice.add(getUnitPrice().negate()).multiply(quantity);
    }

    public BigDecimal getNegativeDiscountAmount() {
        return getDiscountAmount().negate();
    }

    public BigDecimal getWeight() {
        if (hyi.cream.inline.Server.serverExist())
            return (BigDecimal)getFieldValue("weight");
        else
            return (BigDecimal)getFieldValue("WEIGHT");
    }

    public void setWeight(BigDecimal weight) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("weight", weight);
        else
            setFieldValue("WEIGHT", weight);
    }

    public void setDaiShouBarcode(String barcode) {
        this.daiShouBarcode = barcode;
    }
    
    public String getDaiShouBarcode() {
        return daiShouBarcode;
    }
    
    public void setDaiShouID(String id) {
        this.daiShouID = id;
    }
    
    public String getDaiShouID() {
        return daiShouID;
    }
    
    public boolean isCommit() {
        return commit;
    }

    public void setCommit(boolean commit) {
        this.commit = commit;
    }

    public void setOriginalAfterDiscountAmount(BigDecimal value) {
        originalAfterDiscountAmount = value;
    }

    public BigDecimal getOriginalAfterDiscountAmount() {
        return originalAfterDiscountAmount;
    }

    public BigDecimal getAfterItemDsctUnitPrice() {
        return afterItemDsctUnitPrice;
    }

    public void setAfterItemDsctUnitPrice(BigDecimal afterItemDsctPrice) {
        this.afterItemDsctUnitPrice = afterItemDsctPrice;
    }
}
