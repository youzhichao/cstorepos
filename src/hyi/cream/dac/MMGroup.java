package hyi.cream.dac;
import java.io.*;
import java.util.*;
/**
 * @author Wissly
 * @version 1.0
 */
public class MMGroup extends DacBase implements Serializable{
    transient public static final String VERSION = "1.5";
    static final String tableName = "mmgroup";
    private static ArrayList primaryKeys = new ArrayList();
    static {
        primaryKeys.add("Mmid");
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }
    public static Iterator queryByID(String Mmid) {
        return getMultipleObjects(MMGroup.class, "SELECT * FROM "
            + tableName + " WHERE Mmid='" + Mmid  + "'");
    }
    public static Iterator queryByITEMNO(String Item) {
        return getMultipleObjects(MMGroup.class, "SELECT * FROM "
            + tableName + " WHERE Item ='" + Item  + "'");
    }

    /*
    public static MMGroup queryByID (String ID) {
        return (MMGroup)getSingleObject(MMGroup.class,
        "SELECT * FROM " + tableName + " WHERE ID='" + ID + "'");
    }
*/

    public String getID() {
        return (String)getFieldValue("Mmid");
    }

    public String getITEMNO() {
        return (String)getFieldValue("Item");
    }
    public String getGROUPNUM() {
        return (String)getFieldValue("Groupno");
    }

    public MMGroup() throws InstantiationException{

    }

	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("MmID", "Mmid");
        fieldNameMap.put("Groupno", "Groupno");
        fieldNameMap.put("Item", "Item");
        return fieldNameMap;	
	}
	
	/**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {

        return DacBase.getMultipleObjects(hyi.cream.dac.MMGroup.class,
            "SELECT * FROM posdl_mmgroup", getScToPosFieldNameMap());
    }
    
    
    /**
     * unit test
     */
    public static void main(String[] args) {
    	Iterator iter = MMGroup.getAllObjectsForPOS().iterator();
    	while (iter.hasNext()) {
    		System.out.println(iter.next());
    	}
    }
}