/**
 * DAC class
 * @since 2000
 * @author slackware
 */

package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.io.*;
import hyi.cream.inline.*;
import hyi.cream.util.CreamToolkit;

public class Member extends DacBase implements Serializable {//Read only

	static final String tableName = "member";
	private static ArrayList primaryKeys = new ArrayList();
    private boolean needConfirm; 
	
	public List getPrimaryKeyList() {
		return  primaryKeys;
	}

	static {
		primaryKeys.add("memberID");
	}
	
	//constructor
	public Member() throws InstantiationException {
	}

	//Get properties value:
	public String getID() {
		return (String)getFieldValue("memberID");
	}

    public String getMemberCardID() {
        return (String)getFieldValue("idCardNumber");
    }

    public String getIdentityID() {
        return (String)getFieldValue("memberemcid");
    }

    public String getTelephoneNumber() {
        return (String)getFieldValue("telNumber");
    }

	public String getName() {
		return (String)getFieldValue("memberName");
	}

	public String getInsertUpdateTableName() {
		return tableName;
	}

    public BigDecimal getMemberTotalBonus() {
        return (BigDecimal)getFieldValue("membertotalbonus");
    }
    
    public BigDecimal getMemberActionBonus() {
        return (BigDecimal)getFieldValue("memberactionbonus");
    }
    /**
     * Query object method. This method will be invoked by ServerThread when
     * receiving "getObject hyi.cream.dac.Member [memberID]" command.
     * 
     * @param date Can be member ID, member card ID, telephone number, or identity ID.
     * @return Collection Return a Collection contains a Member DAC object.
     */
    public static Collection queryObject(String data) {
        List list = new ArrayList();

        if (data.length() == 12) { // maybe it is a first 12 digits of idCardNumber
            // Try treat it as a idCardNumber...
            Member member = (Member)DacBase.getSingleObject(Member.class,
                "SELECT * FROM commdl_member WHERE updateBeginDate = '1970-01-01' AND " 
              + "sequenceNumber = 1 AND "
              + "idCardNumber = '"+ data + CreamToolkit.getEAN13CheckDigit(data) + "'");
            
            if (member != null) {
                list.add(member);
                return list;
            }
            
            member = (Member)DacBase.getSingleObject(Member.class,
                "SELECT * FROM member WHERE idCardNumber='" + 
                data + CreamToolkit.getEAN13CheckDigit(data) + "'");
            if (member != null) {
                list.add(member);
                return list;
            }

        }

        //check commdl_member
        Member member = (Member)DacBase.getSingleObject(Member.class,
                "SELECT * FROM commdl_member WHERE updateBeginDate = '1970-01-01' AND " 
              + "sequenceNumber = 1 AND ("
              + "idCardNumber = '"+ data + "' OR "
              + "memberId = '"+ data + "' OR "
              + "telNumber = '"+ data + "')"
              );
        
        if (member != null) {
            list.add(member);
            return list;
        }
            
        // Try treat it as a idCardNumber...
        member = (Member)DacBase.getSingleObject(Member.class,
              "SELECT * FROM member WHERE idCardNumber='" + data + "' OR "
            + "memberId = '"+ data + "' OR "
            + "telNumber = '"+ data + "' OR "
            + "memberemcid='" + data + "'"            
            );
            
        if (member != null) {
            list.add(member);
            return list;
        }

        // Try treat it as a memberID...
//        member = (Member)DacBase.getSingleObject(Member.class,
//            "SELECT * FROM member WHERE memberID='" + data + "'");
//        if (member != null) {
//            list.add(member);
//            return list;
//        }
//
//
//        // Try treat it as telNumber...
//        member = (Member)DacBase.getSingleObject(Member.class,
//            "SELECT * FROM member WHERE telNumber='" + data + "'");
//        if (member != null) {
//            list.add(member);
//            return list;
//        }
//
//        // Try treat it as memberemcid()...
//        member = (Member)DacBase.getSingleObject(Member.class,
//            "SELECT * FROM member WHERE memberemcid='" + data + "'");
//        if (member != null) {
//            list.add(member);
//            return list;
//        }
        return list; 
    }

    /**
     * Query member data by a given ID.
     */
    public static Member queryByMemberID(String id) {
        if (!Client.getInstance().isConnected()) {
            return tryCreateAnUnkownMember(id);

        } else {
            // Send command "queryMember" to inline server.
            try {
                Client client = Client.getInstance();
                client.processCommand("queryMember " + id);
                Member member = (Member)client.getReturnObject();
                if (member != null) {
                    member.setNeedConfirm(false);  // No need user confirmation
                    return member;
                } else {                     
                    return tryCreateAnUnkownMember(id);
                }
            } catch (ClientCommandException e) {
                return null;
            }
        }
    }

    private static Member tryCreateAnUnkownMember(String id) {
        // 如果非会员打头编号，拒绝生成Member object
        if (!id.startsWith("2991") && !id.startsWith("2990"))
            return null;

        // 如果没有输入校验码，则先帮它补上
        if (id.length() == 12)
            id += CreamToolkit.getEAN13CheckDigit(id);
        else if (!CreamToolkit.checkEAN13(id))
            return null;   // 如果足13码，但是检验码不对，也拒绝生成Member object

        Member member;
        try {
            member = new Member();
            member.setFieldValue("memberID", id.substring(4, 12));
            member.setFieldValue("memberName", "未知");
            member.setFieldValue("idCardNumber", id);
            member.setFieldValue("telNumber", "未知");
            member.setFieldValue("memberemcid", "未知");
            member.setFieldValue("membertotalbonus", new BigDecimal("0.00"));
            member.setFieldValue("memberactionbonus", new BigDecimal("0.00"));
            member.setNeedConfirm(true);    // need user confirmation
            return member;
        } catch (InstantiationException e) {
            return null;   // impossible!
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * 在离线状态下，若前台只输入前12码会员卡号，则必需由用户确认是否接受此会员卡号，
     * 在这种情况下，needConfirm会被设置为true。
     * 
     * @return boolean 是否需要让用户确认接受此会员卡号
     */
    public boolean isNeedConfirm() {
        return needConfirm;
    }

    /**
     * 在离线状态下，若前台只输入前12码会员卡号，则必需由用户确认是否接受此会员卡号，
     * 在这种情况下，needConfirm会被设置为true。
     * 
     * @param needConfirm 是否需要让用户确认接受此会员卡号
     */
    public void setNeedConfirm(boolean needConfirm) {
        this.needConfirm = needConfirm;
    }
}
