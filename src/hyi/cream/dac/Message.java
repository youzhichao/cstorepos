package hyi.cream.dac;

import java.io.*;
import java.util.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;

/**
 * Message class.
 *
 * @author Bruce
 * @version 1.5
 */
public class Message extends DacBase implements Serializable {

    transient public static final String VERSION = "1.5";

    static final String tableName = "commdl_message";
    private static ArrayList primaryKeys = new ArrayList();
    private static boolean enableMessageDownload;
    private static List emptyList = new ArrayList();

    static {
        primaryKeys.add("id");
        primaryKeys.add("source");
        
        String s = CreamProperties.getInstance().getProperty("EnableMessageDownload");
        if (s != null && s.equalsIgnoreCase("yes"))
            enableMessageDownload = true;
    }

    /**
     * Constructor
     */
    public Message() throws InstantiationException {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    // properties

    public int getId() {
        return ((Integer)this.getFieldValue("id")).intValue();
    }

    public String getSource() {
        return (String)this.getFieldValue("source");
    }

    public String getMessage() {
        return (String)this.getFieldValue("message");
    }

    // for Serializable
    public String getInsertUpdateTableName() {
        return tableName;
    }
    
	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("id", "id");
        fieldNameMap.put("source", "source");
        fieldNameMap.put("message", "message");
        return fieldNameMap;	
	}

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        if (!enableMessageDownload)
            return emptyList;         

        return DacBase.getMultipleObjects(
            hyi.cream.dac.Message.class,
            "SELECT id, source, message FROM commdl_message"
                + " WHERE downloadtopos = '1' AND status = '1'"
                + " AND"
                + " (NOW() BETWEEN displayBeginDate AND displayEndDate"
                + "  OR (DATE_FORMAT(displayBeginDate, '%Y-%m-%d') = '1970-01-01'"
                + "     AND (TIME_FORMAT(NOW(), '%H:%i:%s') BETWEEN"
                + "          TIME_FORMAT(displayBeginDate, '%H:%i:%s')"
                + "          AND TIME_FORMAT(displayEndDate, '%H:%i:%s')"
                + "         )"
                + "     )"
                + " )",
            getScToPosFieldNameMap());
    }
}
