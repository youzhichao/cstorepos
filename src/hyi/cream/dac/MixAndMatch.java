package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.io.*;

/**
 * MixAndMatch definition class.
 *
 * @author Dai, Bruce
 * @version 1.5
 */
public class MixAndMatch extends DacBase implements Serializable {

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public static Collection getAllObjectsForPOS()
     */
    transient public static final String VERSION = "1.5";

    static final String tableName = "mixandmatch";
    private static ArrayList primaryKeys = new ArrayList();

    static {
        primaryKeys.add("ID");
    }

    /**
     * Constructor
     */
    public MixAndMatch() throws InstantiationException {
    }

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static MixAndMatch queryByID (String ID) {
        return (MixAndMatch)getSingleObject(MixAndMatch.class,
            "SELECT * FROM " + tableName + " WHERE ID='" + ID + "'");
    }

    public String getID() {
        return (String)getFieldValue("ID");
    }

    public String getScreenName() {
        return (String)getFieldValue("SNAME");
    }

    public String getPrintName() {
        return (String)getFieldValue("PNAME");
    }

    public Date getBeginDate() {
        return (Date)getFieldValue("BDATE");
    }

    public Date getEndDate() {
        return (Date)getFieldValue("EDATE");
    }

    public Date getBeginTime() {
        return (Date)getFieldValue("BTIME");
    }

    public Date getEndTime() {
        return (Date)getFieldValue("ETIME");
    }

    public String getWeekCycle() {
        return (String)getFieldValue("CYCLE");
    }

    public String getType() {
        return (String)getFieldValue("TYPE");
    }

    public String getPriceType() {
        return (String)getFieldValue("PRCTYPE");
    }

    public BigDecimal getDiscountAmount() {
        return (BigDecimal)getFieldValue("AMOUNT");
    }

    public Integer getDiscountLimit() {
        return (Integer)getFieldValue("LIMITS");
    }

    public BigDecimal getTotalAmount() {
        return (BigDecimal)getFieldValue("TAMOUNT");
    }

    public Integer getGroup1Quantity() {
        return (Integer)getFieldValue("GROUP1QTY");
    }

    public Integer getGroup2Quantity() {
        return (Integer)getFieldValue("GROUP2QTY");
    }

    public Integer getGroup3Quantity() {
        return (Integer)getFieldValue("GROUP3QTY");
    }

    public Integer getGroup4Quantity() {
        return (Integer)getFieldValue("GROUP4QTY");
    }

    public Integer getGroup5Quantity() {
        return (Integer)getFieldValue("GROUP5QTY");
    }

    public Integer getGroup6Quantity() {
        return (Integer)getFieldValue("GROUP6QTY");
    }

    public Integer getPack1Quantity() {
        return (Integer)getFieldValue("PACK1QTY");
    }

    public BigDecimal getPack1Amount() {
        return (BigDecimal)getFieldValue("PACK1AMT");
    }

    public Integer getPack2Quantity() {
        return (Integer)getFieldValue("PACK2QTY");
    }

    public BigDecimal getPack2Amount() {
        return (BigDecimal)getFieldValue("PACK2AMT");
    }

    public Integer getPack3Quantity() {
        return (Integer)getFieldValue("PACK3QTY");
    }

    public BigDecimal getPack3Amount() {
        return (BigDecimal)getFieldValue("PACK3AMT");
    }

    public Integer getPack4Quantity() {
        return (Integer)getFieldValue("PACK4QTY");
    }

    public BigDecimal getPack4Amount() {
        return (BigDecimal)getFieldValue("PACK4AMT");
    }

    public Integer getPack5Quantity() {
        return (Integer)getFieldValue("PACK5QTY");
    }

    public BigDecimal getPack5Amount() {
        return (BigDecimal)getFieldValue("PACK5AMT");
    }

    public Integer getPack6Quantity() {
        return (Integer)getFieldValue("PACK6QTY");
    }

    public BigDecimal getPack6Amount() {
        return (BigDecimal)getFieldValue("PACK6AMT");
    }

    public Integer getPack7Quantity() {
        return (Integer)getFieldValue("PACK7QTY");
    }

    public BigDecimal getPack7Amount() {
        return (BigDecimal)getFieldValue("PACK7AMT");
    }

    public Integer getPack8Quantity() {
        return (Integer)getFieldValue("PACK8QTY");
    }

    public BigDecimal getPack8Amount() {
        return (BigDecimal)getFieldValue("PACK8AMT");
    }

    public String getLinkedMixandMatch() {
        return (String)getFieldValue("LINKMMID");
    }

	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("mmID", "ID");
        fieldNameMap.put("screenName", "SNAME");
        fieldNameMap.put("printName", "PNAME");
        fieldNameMap.put("beginDate", "BDATE");
        fieldNameMap.put("endDate", "EDATE");
        fieldNameMap.put("beginTime", "BTIME");
        fieldNameMap.put("endTime", "ETIME");
        fieldNameMap.put("weekCycle", "CYCLE");
        fieldNameMap.put("type", "TYPE");
        fieldNameMap.put("priceType", "PRCTYPE");
        fieldNameMap.put("discountAmount", "AMOUNT");
        fieldNameMap.put("discountLimit", "LIMITS");
        fieldNameMap.put("totalAmount", "TAMOUNT");
        fieldNameMap.put("group1Quantity", "GROUP1QTY");
        fieldNameMap.put("group2Quantity", "GROUP2QTY");
        fieldNameMap.put("group3Quantity", "GROUP3QTY");
        fieldNameMap.put("group4Quantity", "GROUP4QTY");
        fieldNameMap.put("group5Quantity", "GROUP5QTY");
        fieldNameMap.put("group6Quantity", "GROUP6QTY");
        fieldNameMap.put("pack1Quantity", "PACK1QTY");
        fieldNameMap.put("pack1Amount", "PACK1AMT");
        fieldNameMap.put("pack2Quantity", "PACK2QTY");
        fieldNameMap.put("pack2Amount", "PACK2AMT");
        fieldNameMap.put("pack3Quantity", "PACK3QTY");
        fieldNameMap.put("pack3Amount", "PACK3AMT");
        fieldNameMap.put("pack4Quantity", "PACK4QTY");
        fieldNameMap.put("pack4Amount", "PACK4AMT");
        fieldNameMap.put("pack5Quantity", "PACK5QTY");
        fieldNameMap.put("pack5Amount", "PACK5AMT");
        fieldNameMap.put("pack6Quantity", "PACK6QTY");
        fieldNameMap.put("pack6Amount", "PACK6AMT");
        fieldNameMap.put("pack7Quantity", "PACK7QTY");
        fieldNameMap.put("pack7Amount", "PACK7AMT");
        fieldNameMap.put("pack8Quantity", "PACK8QTY");
        fieldNameMap.put("pack8Amount", "PACK8AMT");
        fieldNameMap.put("linkedMixandMatch", "LINKMMID");
        return fieldNameMap;	
	}


    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        return DacBase.getMultipleObjects(hyi.cream.dac.MixAndMatch.class,
            "SELECT * FROM posdl_mm", getScToPosFieldNameMap());
    }
}

