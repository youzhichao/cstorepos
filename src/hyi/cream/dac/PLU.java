/*jadclipse*/// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
package hyi.cream.dac;

import hyi.cream.POSTerminalApplication;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Referenced classes of package hyi.cream.dac:
//            DacBase, Transaction

/**
 * Item PLU(Price LookUp) DAC class.
 *
 * @author Dai, Slackware, Bruce
 * @version 1.5
 */
public class PLU extends DacBase implements Serializable {
    // Read only
    // TODO Too many item to cache
    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *       M  public String getInsertUpdateTableName()
     *       A  public static String getInsertUpdateTableNameStaticVersion()
     *       M  public static PLU queryByPluNumber(String pluNumber)
     *       M  public static PLU queryBarCode(String barCode)
     *       A  public static Collection getAllObjectsForPOS()
     *       A  private static Integer[] getPluSpecialValue(String k, String s, String o)
     * 
     *  Meyer / 2003-01-10
     *      增加方法 getSalingPrice()，取得当前的销售单价
     * 
     */
    public static final transient String VERSION = "1.5";
    static final String tableName = "plu";
    static final int MAX_CACHE_SIZE = 10;
    private static ArrayList primaryKeys;
    private static HashMap cache = new HashMap();
    private int uid;
    private static final Collection existedFieldList = DacBase.getExistedFieldList(getInsertUpdateTableNameStaticVersion());

    private static final DateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat HHmmss = new SimpleDateFormat("HH:mm:ss");

    static {
        primaryKeys = new ArrayList();
        primaryKeys.add("PLUNO");
    }

    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public PLU() throws InstantiationException {
    }

    public String getInsertUpdateTableName() {
        if (getUseClientSideTableName())
            return "plu";
        if (Server.serverExist())
            return "posdl_plu";
        else
            return "plu";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (Server.serverExist())
            return "posdl_plu";
        else
            return "plu";
    }

    public Collection getExistedFieldList() {
        return existedFieldList;
    }

    public String getPluNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("pluNumber");
        else
            return (String) getFieldValue("PLUNO");
    }

    public String getItemNumber() {
        return getInStoreCode();
    }

    public String getInStoreCode() {
        if (Server.serverExist())
            return (String) getFieldValue("itemNumber");
        else
            return (String) getFieldValue("ITEMNO");
    }

    public String getScreenName() {
        if (Server.serverExist())
            return (String) getFieldValue("pluName");
        else
            return (String) getFieldValue("PLUNAME");
    }

    public String getPrintName() {
        if (Server.serverExist())
            return (String) getFieldValue("pluEnglishName");
        else
            return (String) getFieldValue("PRINTNAME");
    }

    public String getShipNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("shipNumber");
        else
            return (String) getFieldValue("SHIPNO");
    }

    public String getCategoryNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("categoryNumber");
        else
            return (String) getFieldValue("CATNO");
    }

    public String getMidCategoryNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("midCategoryNumber");
        else
            return (String) getFieldValue("MIDCATNO");
    }

    public String getMicroCategoryNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("microCategoryNumber");
        else
            return (String) getFieldValue("MICROCATNO");
    }

    public String getThinCategoryNumber() {
        String value;
        if (Server.serverExist())
            value = (String) getFieldValue("thinCategoryNumber");
        else
            value = (String) getFieldValue("THINCATNO");
        return value != null ? value : "";
    }

    public BigDecimal getUnitPrice() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("storeUnitPrice");
        else
            return (BigDecimal) getFieldValue("PLUPRICE");
    }

    public BigDecimal getSpecialPrice() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("specialPrice");
        else
            return (BigDecimal) getFieldValue("PLUPMPRICE");
    }

    public BigDecimal getMemberPrice() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("memberPrice");
        else
            return (BigDecimal) getFieldValue("PLUMBPRICE");
    }

    public BigDecimal getDiscountPrice1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("couponPrice1");
        else
            return (BigDecimal) getFieldValue("PLUOPPRICE1");
    }

    public BigDecimal getDiscountPrice2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("couponPrice2");
        else
            return (BigDecimal) getFieldValue("PLUOPPRICE2");
    }

    public Date getDiscountStartDate() {
        if (Server.serverExist())
            return (Date) getFieldValue("specialBeginDate");
        else
            return (Date) getFieldValue("PLUPMDATES");
    }

    public Date getDiscountEndDate() {
        if (Server.serverExist())
            return (Date) getFieldValue("specialEndDate");
        else
            return (Date) getFieldValue("PLUPMDATEE");
    }

    public Time getDiscountStartTime() {
        if (Server.serverExist())
            return (Time) getFieldValue("specialBeginTime");
        else
            return (Time) getFieldValue("PLUPMTIMES");
    }

    public Time getDiscountEndTime() {
        if (Server.serverExist())
            return (Time) getFieldValue("specialEndTime");
        else
            return (Time) getFieldValue("PLUPMTIMEE");
    }

    //public Integer getSI

    public Integer getSIGroup() {
        try {
            Object o = null;
            if (Server.serverExist())
                o = getFieldValue("siGroup");
            else
                o = getFieldValue("PLUSIGN1");
            //CreamToolkit.logMessage(getItemNumber() + "| PLU.getSIGroup()>" +o);
            if (o instanceof Integer)
                return (Integer)o;
            else
                return Integer.valueOf((String) o);
        } catch (Exception e) {
            //e.printStackTrace(CreamToolkit.getLogger());
            return new Integer(0);
        }
    }

    public Date getDiscountBeginDate() {
        if (Server.serverExist())
            return (Date) getFieldValue("discountBeginDate");
        else
            return (Date) getFieldValue("DISCBEGDT");
    }

    public Date getDiscountEnd2() {
        if (Server.serverExist())
            return (Date) getFieldValue("discountEndDate");
        else
            return (Date) getFieldValue("DISCENDDT");
    }

    public Integer getAttribute1() {
        if (Server.serverExist())
            return (Integer) getFieldValue("PLUSIGN2");
        else
            return (Integer) getFieldValue("PLUSIGN2");
    }

    public Integer getAttribute2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("PLUSIGN3");
        else
            return (Integer) getFieldValue("PLUSIGN3");
    }

    public String getTaxType() {
        if (Server.serverExist())
            return (String) getFieldValue("taxID");
        else
            return (String) getFieldValue("PLUTAX");
    }

    /*public String getPriceLimit() {
        return (String)getFieldValue("PLULIMAMT");
    }*/
    
    public String getDepID() {
        if (Server.serverExist())
            return (String) getFieldValue("depID");
        else
            return (String) getFieldValue("depID");
    }

    public String getSpecification() {
        String spec = (String) getFieldValue("specification");
        return spec != null ? spec : "";
    }

    public String getNameAndSpecification() {
        return getScreenName() + getSpecification();
    }

    public String getSmallUnit() {
        String value = (String) getFieldValue("smallUnit");
        return value != null ? value : "";
    }

    public String getInvCycleNo() {
        String value = (String) getFieldValue("invCycleNo");
        return value != null ? value : "";
    }

    public String getSizeCategory() {
        String value = (String) getFieldValue("sizeCategory");
        return value != null ? value : "";
    }

    public String getItemBrand() {
        String value = (String) getFieldValue("itemBrand");
        return value != null ? value : "";
    }

    public String getSize() {
        String value = (String) getFieldValue("size");
        return value != null ? value : "";
    }

    public String getColor() {
        String value = (String) getFieldValue("color");
        return value != null ? value : "";
    }

    public String getStyle() {
        String value = (String) getFieldValue("style");
        return value != null ? value : "";
    }

    public String getSeason() {
        String value = (String) getFieldValue("season");
        return value != null ? value : "";
    }

    public String getMixAndMatchNumber() {
        String m = "";
        if (Server.serverExist())
            m = (String) getFieldValue("mixAndMatchNumber");
        else
            m = (String) getFieldValue("MAMNO");
        m = m.trim();
        if (m == null)
            m = "0000";
        for (; m.length() < 4; m = m + "0")
            ;
        return m;
    }

    public String getMixAndMatchID() {
        if (getMixAndMatchNumber() == null || getMixAndMatchNumber().equals(""))
            return "";
        else
            return getMixAndMatchNumber().substring(0, 3);
    }

    public String getMixAndMatchGroupID() {
        if (getMixAndMatchNumber() == null || getMixAndMatchNumber().equals(""))
            return "";
        else
            return getMixAndMatchNumber().charAt(3) + "";
    }

    /**
     * Query PLU by a PLU number.
     */
    public static PLU queryByPluNumber(String pluNumber) {
        /**
         * Meyer/2003-02-28/去点PLU的CACHE
         */
         /*
        if (cache.containsKey(pluNumber))
           return (PLU)cache.get(pluNumber);
          */
        PLU plu;
        if (Server.serverExist()) {
            String query = "SELECT storeid FROM store";
            String storeID = null;
            Map value = DacBase.getValueOfStatement(query);
            if (value != null && value.containsKey("storeid"))
                storeID = value.get("storeid").toString();
            String sel = "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE " + (storeID != null ? " storeID='" + storeID + "' AND " : "") + " pluNumber='" + pluNumber + "'";
            plu = (PLU) DacBase.getSingleObject(hyi.cream.dac.PLU.class, sel);
        } else {
            plu = (PLU) DacBase.getSingleObject(hyi.cream.dac.PLU.class, "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE PLUNO='" + pluNumber + "'");
        }
        /*
        if (cache.size() < MAX_CACHE_SIZE)
           cache.put(pluNumber, plu);
        else {
           cache.clear();
           cache.put(pluNumber, plu);
        }

        */
        return plu;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof PLU)) {
            return false;
        } else {
            PLU objCp = (PLU) obj;
            return getPluNumber().equals(objCp.getPluNumber());
        }
    }

    public static PLU queryByItemNumber(String itemNumber) {
        return queryByInStoreCode(itemNumber);
    }

    public static PLU queryByInStoreCode(String inStoreCode) {
        /*int len = barCode.length();
        for (int i = 0; i < 13 - len; i++) {
            barCode = "0" + barCode;
        }*/
        if (Server.serverExist())
            return (PLU) DacBase.getSingleObject(hyi.cream.dac.PLU.class, "select * from plu where itemNumber='" + inStoreCode + "'");
        else
            return (PLU) DacBase.getSingleObject(hyi.cream.dac.PLU.class, "select * from plu where ITEMNO='" + inStoreCode + "'");
    }

    public static PLU queryBarCode(String barCode) {
        if (Server.serverExist())
            return (PLU) DacBase.getSingleObject(hyi.cream.dac.PLU.class, "select * from plu where pluNumber='" + barCode + "'");
        else
            return (PLU) DacBase.getSingleObject(hyi.cream.dac.PLU.class, "select * from plu where PLUNO='" + barCode + "'");
    }

    public void setUID(int uid) {
        this.uid = uid;
    }

    public int getUID() {
        return uid;
    }

    /**
     * Get all data for downloading to POS. This method is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        Collection dacs = DacBase.getMultipleObjects(hyi.cream.dac.PLU.class,
                "SELECT * FROM posdl_plu", getScToPosFieldNameMap());
        if (dacs == null)
            return null;

        String kindType;
        String signType;
        String openPrice;
        Iterator iter = dacs.iterator();
        while (iter.hasNext()) {
            PLU plu = (PLU)iter.next();

            // 1. Determine values of PLUSIGN2, PLUSIGN3 from __KINDTYPE, __SIGNTYPE,
            //    and __OPENPRICE, then add them into fieldMap.
            kindType = (String)plu.getFieldValue("__KINDTYPE");
            signType = (String)plu.getFieldValue("__SIGNTYPE");
            openPrice = (String)plu.getFieldValue("__OPENPRICE");

            //if (kindType != null && signType != null && openPrice != null) {
            Integer[] attr = getPluSpecialValue(kindType, signType, openPrice);
            plu.setFieldValue("PLUSIGN2", attr[0]);
            plu.setFieldValue("PLUSIGN3", attr[1]);

            // 2. Remove __KINDTYPE, __SIGNTYPE, and __OPENPRICE.
            plu.fieldMap.remove("__KINDTYPE");
            plu.fieldMap.remove("__SIGNTYPE");
            plu.fieldMap.remove("__OPENPRICE");
        }
        return dacs;
    }

    static Integer[] getPluSpecialValue(String kindType, String signType, String openPrice) {
        BigInteger attribute1 = new BigInteger("0");

        if (kindType == null && openPrice == null) {

        } else if (openPrice == null) {
            if (kindType.equals("3"))
                attribute1 = attribute1.setBit(0);   // (SC)訂貨專用 -> (POS)营业外商品
        } else if (kindType == null){
            if (openPrice.equals("3"))
                attribute1 =  attribute1.setBit(6);  // (SC)磅秤,價格自磅秤讀入 -> (POS)磅秤
        } else {
            //cream.plu.attribute1
            //   bit0: set to 1 if cake.plu.kindType = '3'
            //   bit6: set to 1 if cake.plu.openprice = '3'
            if (kindType.equals("3"))
                attribute1 =  attribute1.setBit(0);  // (SC)訂貨專用 -> (POS)营业外商品
            if (openPrice.equals("3"))
                attribute1 =  attribute1.setBit(6);  // (SC)磅秤,價格自磅秤讀入 -> (POS)磅秤
        }

        //cream.plu.attribute2
        //   bit0: set to 1 if cake.plu.kindType = '1'     特別訂貨
        //   bit2: set to 1 if cake.plu.kindType = '2'     銷售專用
        //   bit3: set to 0
        //   bit4: set to 1 if cake.plu.signType[0] = 'Y'  认证
        //   bit6: set to 1 if cake.plu.signType[3] = 'Y'  //Bruce/20030324 收银机变价 override amount
        //   bit7: set to 1 if cake.plu.openprice = '2'    零秤,手輸價格

        BigInteger attribute2 = new BigInteger("0");

        if (kindType == null && signType == null && openPrice == null) {
            ;
        } else {
            if (kindType == null)
                kindType = "";
            if (signType == null)
                signType = "";
            if (openPrice == null)
                openPrice = "";

            if (kindType.equals("1"))
                attribute2 =  attribute2.setBit(0);     // (SC)特別訂貨 -> (POS)代收  ??
            else if (kindType.equals("2"))
                attribute2 =  attribute2.setBit(2);     // (SC)銷售專用 -> (POS)代付  ??

            if (signType.length() > 0) {
                if (signType.charAt(0) == 'Y' || signType.charAt(0) == 'y' || signType.charAt(0) == '1')
                    attribute2 = attribute2.setBit(4);  // (SC)認證屬性 -> (POS)认证
            }
            //Bruce/2003-06-09
            if (signType.length() >= 10) {
                if (signType.charAt(9) == 'Y' || signType.charAt(9) == 'y' || signType.charAt(9) == '1')
                    attribute2 = attribute2.setBit(5);  // (SC)限量促销商品 -> (POS)限量促销  
            }
            //Bruce/2003-06-09
            if (signType.length() >= 9) {
                if (signType.charAt(8) == 'Y' || signType.charAt(8) == 'y' || signType.charAt(8) == '1')
                    attribute2 = attribute2.setBit(6);  // (SC)可手动变价Override Amount -> (POS)可手动变价
            }

            if (openPrice.equals("2"))
                attribute2 =  attribute2.setBit(7);     // (SC)零秤,手輸價格 -> (POS)开放价格
        }
        Integer [] attr = new Integer[2];
        attr[0] = new Integer(attribute1.intValue());
        attr[1] = new Integer(attribute2.intValue());
        return attr;
    }

    public static boolean importSomePLUs(Object plus[]) {
        boolean somethingWrong = false;
        if (!Server.serverExist()) {
            for (int i = 0; i < plus.length; i++) {
                PLU plu = (PLU) plus[i];

                //Meyer/2003-02-19
                ((DacBase) (plu)).fieldMap.remove("pluPriceChangeID");

                //Bruce/2005-10-14 否则下面的update不work
                plu.setAllFieldsDirty(true);
                
                if (!plu.update() && !plu.insert())
                    somethingWrong = true;
            }

        }
        return !somethingWrong;
    }

    /**
     * 如果在特卖期间，则返回特卖价，否则返回null。
     * 
     * @return 特卖价 or null.
     */
    public BigDecimal getSpecialPriceIfWithinTimeLimit() {
        BigDecimal specialPrice = getSpecialPrice();
        if (specialPrice != null && specialPrice.compareTo(new BigDecimal("0")) > 0) {
            
            //Bruce/20080625/ Rewrite the logic for special price retrieval

            java.util.Date now = new java.util.Date();
            String dateNow = yyyyMMdd.format(now);
            String timeNow = HHmmss.format(now);

            String discStartDate = yyyyMMdd.format(getDiscountStartDate());
            String discEndDate = yyyyMMdd.format(getDiscountEndDate());

            String discStartTime = HHmmss.format(getDiscountStartTime());
            String discEndTime = HHmmss.format(getDiscountEndTime());
            
            if (discStartDate.compareTo(dateNow) <= 0 && dateNow.compareTo(discEndDate) <= 0 &&
                discStartTime.compareTo(timeNow) <= 0 && timeNow.compareTo(discEndTime) <= 0)
                return specialPrice;
            
//            java.util.Date now = new java.util.Date();
//            Date curDate = Date.valueOf((new SimpleDateFormat("yyyy-MM-dd")).format(now));
//            Time curTime = Time.valueOf((new SimpleDateFormat("HH:mm:ss")).format(now));
//            Date discStartDate = getDiscountStartDate();
//            Date discEndDate = getDiscountEndDate();
//            if (discStartDate != null && discEndDate != null && curDate.compareTo(discStartDate) >= 0 && curDate.compareTo(discEndDate) <= 0) {
//                Time discStartTime = getDiscountStartTime();
//                Time discEndTime = getDiscountEndTime();
//                if (discStartTime == null || discEndTime == null || curTime.compareTo(discStartTime) >= 0 && curTime.compareTo(discEndTime) <= 0)
//                    return specialPrice;
//            }
        }
        return null;
    }

    /**
     * 取得单品售价。
     * 
     * <P><PRE>Decision table for determining saling price:
     *
     *    会员否   OnlyMemberCanUseSpecialPrice 特卖期间   使用售价
     *    -------  ---------------------------- -------   -------
     *      no              no                  no        unitPrice
     *      no              yes                 no        unitPrice
     *      yes             no                  no        memberPrice
     *      yes             yes                 no        memberPrice
     *      no              yes                 yes       unitPrice
     *      no              no                  yes       specialPrice
     *      yes             no                  yes       specialPrice
     *      yes             yes                 yes       specialPrice
     * </PRE>
     * 
     * @return Return a Object array with two elements, first is the saling price by a BigDecimal
     * object, the second is a String to specify what price it is used: "UNIT_PRICE": 正常单价，
     * "MEMBER_PRICE": 会员价，"SPECIAL_PRICE": 特卖价.
     */
    public Object[] getSalingPriceDetail() {
        String onlyMemberCanUseSpecialPrice = CreamProperties.getInstance().getProperty("OnlyMemberCanUseSpecialPrice", "no");
        String memberID = POSTerminalApplication.getInstance().getCurrentTransaction().getMemberID();
        boolean isMember = memberID != null && memberID.length() > 0;
        Object salingPriceDetail[] = new Object[2];
        BigDecimal unitPrice = getUnitPrice();
        BigDecimal memberPrice = getMemberPrice();
        BigDecimal salingPrice;
        if (isMember) {
            salingPrice = memberPrice;
            salingPriceDetail[1] = "MEMBER_PRICE";
        } else {
            salingPrice = unitPrice;
            salingPriceDetail[1] = "UNIT_PRICE";
        }
        if (onlyMemberCanUseSpecialPrice.equalsIgnoreCase("no") || onlyMemberCanUseSpecialPrice.equalsIgnoreCase("yes") && isMember) {
            BigDecimal sp = getSpecialPriceIfWithinTimeLimit();
            if (sp != null) {
                salingPrice = sp;
                salingPriceDetail[1] = "SPECIAL_PRICE";
            }
        }
        salingPriceDetail[0] = salingPrice;
        if (salingPriceDetail[0] == null)
            salingPriceDetail[0] = new BigDecimal("0.00");
        return salingPriceDetail;
    }

    /**
     * get pluPriceChangeID
     */
    public String getPluPriceChangeID() {
        return (String) getFieldValue("pluPriceChangeID");
    }

    /**
     * Meyer/2003-02-20
     * return fieldName map of PosToSc as Map 
     */
    public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("pluNumber", "PLUNO");
        fieldNameMap.put("pluName", "PLUNAME");
        fieldNameMap.put("pluEnglishName", "PRINTNAME");
        fieldNameMap.put("itemNumber", "ITEMNO");
        fieldNameMap.put("shipNumber", "SHIPNO");
        fieldNameMap.put("taxID", "PLUTAX");
        fieldNameMap.put("storeUnitPrice", "PLUPRICE");
        fieldNameMap.put("openPrice", "__OPENPRICE");
        fieldNameMap.put("specialPrice", "PLUPMPRICE");
        fieldNameMap.put("specialBeginDate", "PLUPMDATES");
        fieldNameMap.put("specialEndDate", "PLUPMDATEE");
        fieldNameMap.put("specialBeginTime", "PLUPMTIMES");
        fieldNameMap.put("specialEndTime", "PLUPMTIMEE");
        fieldNameMap.put("memberPrice", "PLUMBPRICE");
        fieldNameMap.put("couponPrice1", "PLUOPPRICE1");
        fieldNameMap.put("couponPrice2", "PLUOPPRICE2");
        fieldNameMap.put("discountBeginDate", "DISCBEGDT");
        fieldNameMap.put("discountEndDate", "DISCENDDT");
        fieldNameMap.put("siGroup", "PLUSIGN1");
        fieldNameMap.put("categoryNumber", "CATNO");
        fieldNameMap.put("midCategoryNumber", "MIDCATNO");
        fieldNameMap.put("microCategoryNumber", "MICROCATNO");
        fieldNameMap.put("thinCategoryNumber", "THINCATNO");
        fieldNameMap.put("kindType", "__KINDTYPE");
        fieldNameMap.put("signType", "__SIGNTYPE");
        fieldNameMap.put("mixAndMatchNumber", "MAMNO");
        fieldNameMap.put("depID", "depID");
        fieldNameMap.put("specification", "specification");
        fieldNameMap.put("smallUnit", "smallUnit");
        fieldNameMap.put("invCycleNo", "invCycleNo");
        fieldNameMap.put("sizeCategory", "sizeCategory");
        fieldNameMap.put("itemBrand", "itemBrand");
        fieldNameMap.put("size", "size");
        fieldNameMap.put("color", "color");
        fieldNameMap.put("style", "style");
        fieldNameMap.put("season", "season");
        return fieldNameMap;
    }

    public Iterator getSequentialFieldList() {
        String[] f = {
                "PLUNO",
                "ITEMNO",
                "SHIPNO",
                "PLUNAME",
                "PRINTNAME",
                "CATNO",
                "MIDCATNO",
                "MICROCATNO",
                "PLUPRICE",
                "PLUPMPRICE",
                "PLUPMDATES",
                "PLUPMDATEE",
                "PLUPMTIMES",
                "PLUPMTIMEE",
                "PLUMBPRICE",
                "PLUOPPRICE1",
                "PLUOPPRICE2",
                "PLUSIGN1",
                "PLUSIGN2",
                "DISCBEGDT",
                "DISCENDDT",
                "PLUSIGN3",
                "PLUTAX",
                "MAMNO",
                "depID",
            };
            ArrayList list2 = new ArrayList(Arrays.asList(f));
            
            if (isFieldExist("thinCategoryNumber")) {
                list2.add(list2.indexOf("MICROCATNO") + 1, "THINCATNO");
            }
            
            //Bruce/20030413
            if (isFieldExist("invCycleNo"))
                list2.add("invCycleNo");
            if (isFieldExist("sizeCategory"))
                list2.add("sizeCategory");
            if (isFieldExist("itemBrand"))
                list2.add("itemBrand");
            if (isFieldExist("size"))
                list2.add("size");
            if (isFieldExist("color"))
                list2.add("color");
            if (isFieldExist("style"))
                list2.add("style");
            if (isFieldExist("season"))
                list2.add("season");

            return list2.iterator();
        }

    /**
     * 是否为open price?
     */
    public boolean isOpenPrice() {
        BigInteger attrib = new BigInteger(String.valueOf(getAttribute2()));
        return attrib.testBit(7);
    }

    /**
     * 是否为限量促销商品？
     */
    public boolean isQuantityLimited() {
        BigInteger attrib = new BigInteger(String.valueOf(getAttribute2()));
        return attrib.testBit(5);
    }

    /**
     * 是否可以override amount?
     */
    public boolean canOverrideAmount() {
        if (Server.serverExist()) {
            return false;
        } else {
            BigInteger attrib = new BigInteger(String.valueOf(getAttribute2()));
            return attrib.testBit(6);
        }
    }

    /**
     * 是否为秤重商品？
     */
    public boolean isWeightedPlu() {
        if (!Server.serverExist()) {
            BigInteger attrib = new BigInteger(String.valueOf(getAttribute1()));
            return attrib.testBit(6);
        } else {
            return false;
        }
    }

    /**
     * 是否为资金商品？
     */
    public boolean isCashFlowPlu() {
        if (!Server.serverExist()) {
            BigInteger attrib = new BigInteger(String.valueOf(getAttribute1()));
            return attrib.testBit(5);
        } else {
            return false;
        }
    }

//  public static void main(String[] args) {
//  Date curDate = new Date();      
//    java.sql.Time curTime = java.sql.Time.valueOf(
//      new java.text.SimpleDateFormat("HH:mm:ss").format(curDate));
//      
//  java.sql.Date date1 = java.sql.Date.valueOf(args[0]);
//  java.sql.Time time1 = java.sql.Time.valueOf(args[1]);   
//  
//  System.out.println(curDate);
//  System.out.println(curDate.after(date1));
//  System.out.println(date1);
//
//  System.out.println(curTime);
//  System.out.println(curTime.after(time1));
//  System.out.println(time1);
//}
    
    
}

/*
 * DECOMPILATION REPORT
 * 
 * Decompiled from: D:\workspace\CStorePOS\classes\cream.jar Total time: 171 ms
 * Jad reported messages/errors: Exit status: 0 Caught exceptions:
 */