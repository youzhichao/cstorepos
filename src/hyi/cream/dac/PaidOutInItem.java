// Copyright (c) 2000 HYI
package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.io.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PaidOutInItem extends DacBase implements Serializable {

	static final String tableName = "paidoutinitem";
	private static ArrayList primaryKeys = new ArrayList();

	static {
		primaryKeys.add("ID");
	}

    /**
     * Constructor
     */
    public PaidOutInItem() throws InstantiationException {
    }

	public List getPrimaryKeyList() {
		return  primaryKeys;
	}

	public static PaidOutInItem queryByID (String ID) {
		return (PaidOutInItem)getSingleObject(PaidOutInItem.class,
			"SELECT * FROM " + tableName + " WHERE ID='" + ID + "'");
	}

	public static ArrayList queryPaidType(int paidType) {
        ArrayList paidItem = new ArrayList();
        if (paidType == 0) {
            for (int i = 0; i < 5; i++) {
                paidItem.add(queryByID(String.valueOf(i)));
            }
        } else {
            paidItem.add(queryByID("A")); 
            paidItem.add(queryByID("B"));
            paidItem.add(queryByID("C"));
            paidItem.add(queryByID("D"));
            paidItem.add(queryByID("E"));
        }               
        return paidItem;
	}

    public static Iterator queryAll() {
        return getMultipleObjects(PaidOutInItem.class,
            "SELECT * FROM " + tableName);
    }

	public String getID() {
		return (String)getFieldValue("ID");
	}

	public String getPluNumber() {
		return (String)getFieldValue("PLUNO");
	}

	public String getInsertUpdateTableName() {
		return tableName;
	}
}

 
