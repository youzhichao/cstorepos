package hyi.cream.dac;

import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Payment type class. This is a read only cache object.
 *
 * @author Slackware, Bruce
 * @version 1.5
 */
public class Payment extends DacBase implements Serializable {

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public static Collection getAllObjectsForPOS()
     */
    transient public static final String VERSION = "1.5";

    static final String tableName = "payment";
    transient static private Set cache;

    private static ArrayList primaryKeys = new ArrayList();

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    static {
        primaryKeys.add("PAYID");
        createCache();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Payment))
            return false;
        return getPaymentID().equals(((Payment)obj).getPaymentID());
    }

    public Payment () throws InstantiationException {
    }

    public static void createCache() {
        cache = new HashSet();
        Iterator itr = getMultipleObjects(Payment.class, "SELECT * FROM " + tableName);
        if (itr != null) {
            while (itr.hasNext()) {
                cache.add(itr.next());
            }
        }
    }

    // query from cache
    public static Payment queryByPaymentID(String paymentID) {
        if (cache == null)
            return null;
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            Payment p = (Payment)itr.next();
            if (p.getPaymentID().equals(paymentID)){
            return p;
            }
        }
        return null;
    }

    public static Iterator getAllPayment() {
        /*if (cache == null) {
            return null;
        }
        return cache.iterator();*/

        if (cache == null)
            return null;
        Iterator itr = cache.iterator();
        ArrayList al = new ArrayList();
        while (itr.hasNext()) {
            Payment r = (Payment)itr.next();
            if (al.size() == 0) {
                al.add(r);
            } else {
                boolean state = false;
                Payment temp = null;
                for (int i = 0; i < al.size(); i++) {
                    temp = (Payment)al.get(i);
                    if (Integer.parseInt(r.getPaymentID()) < Integer.parseInt(temp.getPaymentID())) {
                        al.set(i, r);
                        Payment temp2 = null;
                        for (int j = i + 1; j < al.size(); j++) {
                            temp2 = (Payment)al.get(j);
                            al.set(j, temp);
                            temp = temp2;
                        }
                        al.add(temp);
                        state = true;
                        break;
                    }
                }
                if (!state) {
                    al.add(r);
                }
            }
        }
        if (al.size() == 0) {
            return null;
        } else {
            return al.iterator();
        }
    }

    //Get properties value:
    public String getPaymentID () {
        return (String) getFieldValue("PAYID");//PaymentID	PAYID	CHAR(2)	N
    }

    public String getPrintName () {
        return (String) getFieldValue("PAYNAME");//PrintName PAYNAME VARCHAR(10) N
    }

    public String getScreenName() {
        return (String) getFieldValue("PAYCNAME");//ScreenName PAYCNAME VARCHAR(14)	N
    }

    public String getPaymentType() {
        String p = (String) getFieldValue("PAYTYPE");
        p = p.trim();
        while (p.length() < 8) {
            p = "0" + p;
        }
        return p;//PaymentType	PAYTYPE	CHAR(8)	N
    }

    public String getPaymentRate () {
       return (String) getFieldValue("PAYRATE");//PaymentRate	PAYRATE	CHAR(2)	Y
    }

    public BigDecimal getDenomination() {
       return (BigDecimal)getFieldValue("DENOMINATION");//DENOMINATION	Decimal N
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    //set properties value:
    public void setPaymentID (String aPaymentID) {
        setFieldValue("PAYID",aPaymentID);//PaymentID	PAYID	CHAR(2)	N
    }

    public void setPrintName (String aPrintName) {
        setFieldValue("PAYNAME", aPrintName);//PrintName PAYNAME VARCHAR(10) N
    }

    public void setScreenName(String aScreenName) {
        setFieldValue("PAYCNAME", aScreenName);//ScreenName PAYCNAME VARCHAR(14)	N
    }

    public void setPaymentType(String aPayType) {
        setFieldValue("PAYTYPE", aPayType);//PaymentType	PAYTYPE	CHAR(8)	N
    }

    public void setPaymentRate (String aPaymentRate) {
       setFieldValue("PAYRATE", aPaymentRate);//PaymentRate	PAYRATE	CHAR(2)	Y
    }

    public void setDenomination(BigDecimal aDenomination) {
       setFieldValue("DENOMINATION", aDenomination);//DENOMINATION	Decimal N
    }


	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("payID", "PAYID");
        fieldNameMap.put("payPrintName", "PAYNAME");
        fieldNameMap.put("payScreenName", "PAYCNAME");
        fieldNameMap.put("payType", "PAYTYPE");
        fieldNameMap.put("currencyID", "PAYRATE");
        fieldNameMap.put("denomination", "DENOMINATION");
        return fieldNameMap;	
	}
	
    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {

        return DacBase.getMultipleObjects(hyi.cream.dac.Payment.class,
            "SELECT * FROM posdl_payment", getScToPosFieldNameMap());
    }
    
    /**
     * 
     */
    public static void insertCashIfNotExist() {
    	if (DacBase.getSingleObject(hyi.cream.dac.Payment.class,
    			"SELECT * FROM payment WHERE payID='00'") == null) {
			try {				
				Payment payment = new Payment();
				payment.setPaymentID("00");
				payment.setPrintName("现金");
				payment.setScreenName("现金");
				payment.setPaymentType("00001010");
				payment.setPaymentRate("");
				payment.setDenomination(new BigDecimal("1"));
				payment.insert();		
			} catch (InstantiationException e) {
				
			}
    	}
    	    	
    	
    }
}
