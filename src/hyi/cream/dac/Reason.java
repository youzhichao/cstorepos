package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.io.*;

/**
 * Reason definition class.
 *
 * @author Dai, Bruce
 * @version 1.5
 */
public class Reason extends DacBase implements Serializable {

    /**
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public static Collection getAllObjectsForPOS()
     */
    transient public static final String VERSION = "1.5";

    transient static final String tableName = "reason";
    private static ArrayList primaryKeys = new ArrayList();
    transient private static Set cache;

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    static {
        primaryKeys.add("reasonCategory");
        primaryKeys.add("reasonNumber");
        createCache();
    }

    public static void createCache() {
        cache = new HashSet();
        Iterator itr = getMultipleObjects(Reason.class, "SELECT * FROM " + tableName);
        if (itr != null) {
            while (itr.hasNext()) {
                cache.add(itr.next());
            }
        }
    }

    public boolean equals(Object obj) {
        if ( !(obj instanceof Reason))
            return false;
        if (getreasonNumber().equals(((Reason)obj).getreasonNumber())) {
            if (getreasonCategory().equals(((Reason)obj).getreasonCategory())) {
                return true;
            }
        }
        return false;
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static Iterator queryByreasonNumber (String id) {
        if (cache == null)
            return null;
        Iterator itr = cache.iterator();
        ArrayList al = new ArrayList();
        while (itr.hasNext()) {
            Reason r = (Reason)itr.next();
            if (r.getreasonNumber().equals(id)){
                al.add(r);
            }
        }
        if (al.size() == 0) {
            return null;
        } else {
            return al.iterator();
        }
    }

    public static Iterator queryByreasonCategory (String kind) {
        if (cache == null)
            return null;
        Iterator itr = cache.iterator();
        ArrayList al = new ArrayList();
        while (itr.hasNext()) {
            Reason r = (Reason)itr.next();
            if (r.getreasonNumber().equals("00")) {
                continue;
            }
            if (r.getreasonCategory().equalsIgnoreCase(kind)){
                if (al.size() == 0) {
                    al.add(r);
                } else {
                    boolean state = false;
                    Reason temp = null;
                    for (int i = 0; i < al.size(); i++) {
                        temp = (Reason)al.get(i);
                        if (Integer.parseInt(r.getreasonNumber()) < Integer.parseInt(temp.getreasonNumber())) {
                            al.set(i, r);
                            Reason temp2 = null;
                            for (int j = i + 1; j < al.size(); j++) {
                                temp2 = (Reason)al.get(j);
                                al.set(j, temp);
                                temp = temp2;
                            }
                            al.add(temp);
                            state = true;
                            break;
                        }
                    }
                    if (!state) {
                        al.add(r);
                    }
                }
            }
        }
        if (al.size() == 0) {
            return null;
        } else {
            return al.iterator();
        }
    }

    public static Reason queryByreasonNumberAndreasonCategory(String id, String kind) {
        if (cache == null) {
            return null;
        }
        Iterator iter = cache.iterator();
        //ArrayList al = new ArrayList();
        while (iter.hasNext()) {
            Reason r = (Reason)iter.next();
            if (r.getreasonCategory().equals(kind) && r.getreasonNumber().equals(id)) {
                return r;
            }
        }
        return null;
    }

    public Reason() throws InstantiationException {
    }

    public String getreasonNumber() {
        return (String)getFieldValue("reasonNumber");
    }

    public String getreasonCategory() {
        return (String)getFieldValue("reasonCategory");
    }

    public String getreasonName() {
        return (String)getFieldValue("reasonName");
    }

	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("reasonCategory", "reasonCategory");
        fieldNameMap.put("reasonNumber", "reasonNumber");
        fieldNameMap.put("reasonName", "reasonName");
        return fieldNameMap;	
	}
	
	/**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {

        return DacBase.getMultipleObjects(hyi.cream.dac.Reason.class,
            "SELECT * FROM posdl_reason", getScToPosFieldNameMap());
    }
}
