// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   Rebate.java

package hyi.cream.dac;

import hyi.cream.inline.Server;
import hyi.cream.util.CreamProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

// Referenced classes of package hyi.cream.dac:
//            DacBase

public class Rebate extends DacBase
    implements Serializable
{

    private static ArrayList primaryKeys;
    private static String storeNumber = CreamProperties.getInstance().getProperty("StoreNumber");

    public Rebate()
        throws InstantiationException
    {
    }

    public List getPrimaryKeyList()
    {
        return primaryKeys;
    }

    public String getInsertUpdateTableName()
    {
        if(getUseClientSideTableName())
            return "rebate";
        if(Server.serverExist())
            return "posdl_rebate";
        else
            return "rebate";
    }

    public static String getInsertUpdateTableNameStaticVersion()
    {
        if(Server.serverExist())
            return "posdl_rebate";
        else
            return "rebate";
    }

    public static Collection getAllObjectsForPOS()
    {
        String sql = "SELECT * FROM posdl_rebate";
        Collection dacs = DacBase.getMultipleObjects(hyi.cream.dac.Rebate.class, sql, null);
        return dacs;
    }

    public static BigDecimal getRebateRate(String itemNumber)
    {
        BigDecimal rate = new BigDecimal(0.0D);
        Date dt = new Date();
        String today = (new SimpleDateFormat("yyyy-MM-dd")).format(dt);
        String table = getInsertUpdateTableNameStaticVersion();
        String sql = "SELECT * FROM " + table + " " + "WHERE itemNumber = '" + itemNumber + "' " + "AND beginDate <= '" + today + "' " + "AND endDate >= '" + today + "' " + "ORDER BY beginDate DESC LIMIT 1";
        DacBase dacBase = DacBase.getSingleObject(hyi.cream.dac.Rebate.class, sql);
        if(dacBase != null)
        {
            Rebate rebate = (Rebate)dacBase;
            Date discBDate = rebate.getDiscountBeginDate();
            Date discEDate = rebate.getDiscountEndDate();
            dt = java.sql.Date.valueOf(today);
            if(dt.compareTo(discBDate) >= 0 && dt.compareTo(discEDate) <= 0)
                rate = rebate.getDiscountRebateRate();
            else
                rate = rebate.getRebateRate();
        }
        return rate;
    }

    public String getStoreID()
    {
        return (String)getFieldValue("storeID");
    }

    public String getItemNumber()
    {
        return (String)getFieldValue("itemNumber");
    }

    public java.sql.Date getBeginDate()
    {
        return (java.sql.Date)getFieldValue("beginDate");
    }

    public java.sql.Date getEndDate()
    {
        return (java.sql.Date)getFieldValue("endDate");
    }

    public BigDecimal getRebateRate()
    {
        return (BigDecimal)getFieldValue("rebateRate");
    }

    public java.sql.Date getDiscountBeginDate()
    {
        return (java.sql.Date)getFieldValue("discountBeginDate");
    }

    public java.sql.Date getDiscountEndDate()
    {
        return (java.sql.Date)getFieldValue("discountEndDate");
    }

    public BigDecimal getDiscountRebateRate()
    {
        return (BigDecimal)getFieldValue("discountRebateRate");
    }

    static 
    {
        primaryKeys = new ArrayList();
        primaryKeys.add("storeID");
        primaryKeys.add("itemNumber");
        primaryKeys.add("beginDate");
        primaryKeys.add("discountBeginDate");
    }
}
