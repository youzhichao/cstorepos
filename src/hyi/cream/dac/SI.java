package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.io.*;
import hyi.cream.util.*;

public class SI extends DacBase implements Serializable {

	private static ArrayList primaryKeys = new ArrayList();

	static {
        if (hyi.cream.inline.Server.serverExist()) {
            primaryKeys.add("storeID");
            primaryKeys.add("siID");
        } else
            primaryKeys.add("SIID");
	}

	public List getPrimaryKeyList() {
		return primaryKeys;
	}

	public String getInsertUpdateTableName() {
		if (getUseClientSideTableName())
			return "si";
		else if (hyi.cream.inline.Server.serverExist())
			return "posdl_si";
		else
			return "si";
	}

	public static String getInsertUpdateTableNameStaticVersion() {
		if (hyi.cream.inline.Server.serverExist())
			return "posdl_si";
		else
			return "si";
	}

	public static SI queryBySIID(String SIID) {
		if (hyi.cream.inline.Server.serverExist()) {
			return (SI) getSingleObject(
				SI.class,
				"SELECT * FROM "
					+ getInsertUpdateTableNameStaticVersion()
					+ " WHERE SIID='" + SIID + "'" 
                    + " AND storeID = '"
					+ CreamProperties.getInstance().getProperty("StoreNumber")
					+ "'");
		} else
			return (SI) getSingleObject(
				SI.class,
				"SELECT * FROM " + getInsertUpdateTableNameStaticVersion() + " WHERE SIID='" + SIID + "'");
	}

	public SI() throws InstantiationException {
	}

	/**
	 * 判断商品 plu 是否包含在 SI 中。
	 * 判断方法：
	 *     如果getCATNO()（大类）为空就判断 SIGroup 
	 *     大类不为空就看是否和商品所属大类相同，不相同给isContains 赋值 false 否则
	 *        如果getMIDCATNO()（中类）为空给isContains 赋值 true 否则判断中类是否相符；
	 *          如果中类不符就赋值给isContains 赋值 false
	 *          如果小类为空就给isContains 赋值 true 否则判断小类是否相符
	 *              相符就给isContains 赋值true 否则给isContains 赋值 false;
	 * 　　然后，如果isContains值为true,就判断周循环中，当日是否有效，
	 *         如果无效则给isContains 赋值 false
	 *     返回　isContains  
	 * @param plu
	 * @return boolean
	 */
	public boolean contains(PLU plu) {
		if (plu == null)
			return false;
		boolean isContains = false;

		if (getCATNO() != null) {
			if (getCATNO().equals(plu.getCategoryNumber())) {
				if (this.getMIDCATNO() != null) {
					if (getMIDCATNO().equals(plu.getMidCategoryNumber())) {
						if (getMICROCATNO() != null) {
							isContains = (getMICROCATNO().equals(plu.getMicroCategoryNumber()));
						} else {
							isContains = true; //大类中类均相符但小类为空
						}
					} else {
						isContains = false; //大类相符但中类不符
					}
				} else {
					isContains = true; // 大类相符 中类为空
				}
			} else {
				isContains = false; // 大类不为空但中类不符
			}
		} else {
			if (plu.getSIGroup() != null) {
				BigInteger pluSI = new BigInteger(plu.getSIGroup().toString()); //
				String siDiscount = getDiscountSI();
				for (int i = 1; i <= siDiscount.length(); i++) {
					if (siDiscount.charAt(i - 1) == '1' && pluSI.testBit(siDiscount.length() - i)) {
						Date now = new Date();
						if (now.after(plu.getDiscountEnd2()) || now.before(plu.getDiscountBeginDate()))
							isContains = false;
						else
							isContains = true;

						break;
					}
				}
			} // end of "if (plu.getSIGroup() != null)"
		}

		if (!isContains)
			return isContains;

		//  check day of week
		int dayOfWeek = (new Date()).getDay();
		String weekCycle = getCycle();
		if (weekCycle == null) {
			weekCycle = "1111111";
		} else if (weekCycle.length() < 7) {
			weekCycle = weekCycle + "11111111";
		}

		if (weekCycle.charAt(dayOfWeek) == '0') {
			isContains = false;
		}

		return isContains;
	}

	public boolean contains(LineItem lineItem) {
		if (lineItem.getDetailCode().equals("D"))
			return false;
		PLU plu = PLU.queryByPluNumber(lineItem.getPluNumber());
		return contains(plu);
	}

	public boolean isAfterSI() {
		/*	if (this.getAttribute().charAt(7) == 'A') 
		        return true;
		    return false;
		*/
		return true;
	}

	/**
	 * @return boolean  (if the SI is valid then return true, else return false) 
	 */
	public boolean isValid() {
		Date now = new Date();
		java.sql.Date curDate = java.sql.Date.valueOf(new java.text.SimpleDateFormat("yyyy-MM-dd").format(now));
		java.sql.Time curTime = java.sql.Time.valueOf(new java.text.SimpleDateFormat("HH:mm:ss").format(now));

		java.sql.Date startDate = getBDate();
		java.sql.Date endDate = getEDate();

		if (startDate != null
			&& endDate != null
			&& curDate.compareTo(startDate) >= 0
			&& curDate.compareTo(endDate) <= 0) {

			java.sql.Time startTime = getBTime();
			java.sql.Time endTime = getETime();
			if (startTime == null
				|| endTime == null
				|| (curTime.compareTo(startTime) >= 0 && curTime.compareTo(endTime) <= 0)) {
				return true;
			}
		}
		return false;
	}

	//public BigDecimal computeDiscntValue(BigDecimal base, BigDecimal itemCount) {
	public BigDecimal computeDiscntValue(BigDecimal base) {
		BigDecimal discountAmount = null;

		////////////////////////////
		// CHAR[0]: 四舍五入flag
		//    ‘R’ - 四舍五入
		//    ‘C’ - 进位
		//    ‘D’ - 舍去
		// CHAR[1]:小数位数
		// CHAR[2]:SI相容
		// CHAR[3]:浮动%
		// CHAR[4]:认证
		// CHAR[5]:M&M相容
		// CHAR[6]:
		// CHAR[7]
		////////////////////////////

		int round;
		String attr = getAttribute();
		if (attr.charAt(2) == '0')
			return new BigDecimal(0); //SI不相容		   
		if (attr.charAt(0) == 'R')
			round = BigDecimal.ROUND_HALF_UP;
		else if (attr.charAt(0) == 'C')
			round = BigDecimal.ROUND_UP;
		else
			round = BigDecimal.ROUND_DOWN; //四舍五入flag

		if (getType().equals("0") || getType().equals("4")) {
			//折扣  ||  开放折扣
			discountAmount = base.multiply(getValue()).negate();
		} else if (getType().equals("1")) {
			// 折让
			//discountAmount = getValue().negate().multiply(itemCount);
			discountAmount = getValue().negate();
		} else if (getType().equals("2")) {
			//加成
			discountAmount = base.multiply(getValue());
		} else if (getType().equals("3")) {
			//全折
			discountAmount = base.negate();
		}

		return discountAmount.setScale(attr.charAt(1) - '0', round);

	}

	/**
	 * 从SC 中取出数据为下载到POS系统做准备
	 * 
	 * @return Collection
	 */
	public static Collection getAllObjectsForPOS() {
		String sql = "SELECT * FROM posdl_si";
		Collection dacs = DacBase.getMultipleObjects(SI.class, sql, getScToPosFieldNameMap());
		return dacs;
	}

	public static Map getScToPosFieldNameMap() {
		Map fieldNameMap = new HashMap();
		fieldNameMap.put("siID", "SIID");
		fieldNameMap.put("siScreenName", "SICNAMES");
		fieldNameMap.put("siPrintName", "SINAMEP");
		fieldNameMap.put("bDate", "BDATE");
		fieldNameMap.put("eDate", "EDATE");
		fieldNameMap.put("bTime", "BTIME");
		fieldNameMap.put("eTime", "ETIME");
		fieldNameMap.put("cycle", "CYCLE");
		fieldNameMap.put("siGroupNumber", "SINO");
		fieldNameMap.put("catNo", "CATNO");
		fieldNameMap.put("midCatNo", "MIDCATNO");
		fieldNameMap.put("microCatNo", "MICROCATNO");
		fieldNameMap.put("siType", "SITYPE");
		fieldNameMap.put("discountValue", "DVALUE");
		fieldNameMap.put("discountBase", "DBASE");
		fieldNameMap.put("discountAttribute", "ATTR");

		return fieldNameMap;
	}

	//Get properties value:
	public String getSIID() {
		if (hyi.cream.inline.Server.serverExist())
			return (String) getFieldValue("siID");
		else
			return (String) getFieldValue("SIID");
	}

	public String getScreenName() {
		if (hyi.cream.inline.Server.serverExist())
			return (String) getFieldValue("siScreenName");
		else
			return (String) getFieldValue("SICNAMES");
	}

	public String getPrintName() {
		if (hyi.cream.inline.Server.serverExist())
			return (String) getFieldValue("siPrintName");
		else
			return (String) getFieldValue("SINAMEP");
	}

	public java.sql.Date getBDate() {
		if (hyi.cream.inline.Server.serverExist())
			return (java.sql.Date) getFieldValue("bDate");
		else
			return (java.sql.Date) getFieldValue("BDATE");
	}

	public java.sql.Date getEDate() {
		if (hyi.cream.inline.Server.serverExist())
			return (java.sql.Date) getFieldValue("eDate");
		else
			return (java.sql.Date) getFieldValue("EDATE");
	}

	public java.sql.Time getBTime() {
		if (hyi.cream.inline.Server.serverExist())
			return (java.sql.Time) getFieldValue("bTime");
		else
			return (java.sql.Time) getFieldValue("BTIME");
	}

	public java.sql.Time getETime() {
		if (hyi.cream.inline.Server.serverExist())
			return (java.sql.Time) getFieldValue("eTime");
		else
			return (java.sql.Time) getFieldValue("ETIME");
	}

	public String getCycle() {
		if (hyi.cream.inline.Server.serverExist())
			return (String) getFieldValue("cycle");
		else
			return (String) getFieldValue("CYCLE");
	}

	public String getDiscountSI() {
		if (hyi.cream.inline.Server.serverExist())
			return (String) getFieldValue("siGroupNumber");
		else
			return (String) getFieldValue("SINO");
	}

	public String getCATNO() {
		if (hyi.cream.inline.Server.serverExist())
			return (String) getFieldValue("catNo");
		else
			return (String) getFieldValue("CATNO");
	}

	public String getMIDCATNO() {
		if (hyi.cream.inline.Server.serverExist())
			return (String) getFieldValue("midCatNo");
		else
			return (String) getFieldValue("MIDCATNO");
	}

	public String getMICROCATNO() {
		if (hyi.cream.inline.Server.serverExist())
			return (String) getFieldValue("microCatNo");
		else
			return (String) getFieldValue("MICROCATNO");
	}

	/**
	 * @return String
	 * 折扣形态 
	 *           ‘0’:折扣（百分比）
	 *           ‘1’：折让 
	 *           ‘2’加成（百分比）
	 *           ‘3’：全折 
	 *           ‘4’:开放折扣数输入 
	 */
	public String getType() {
		if (hyi.cream.inline.Server.serverExist())
			return (String) getFieldValue("siType");
		else
			return (String) getFieldValue("SITYPE");
	}

	public BigDecimal getValue() {
		if (hyi.cream.inline.Server.serverExist())
			return (BigDecimal) getFieldValue("discountValue");
		else
			return (BigDecimal) getFieldValue("DVALUE");
	}

	/*
	 * 开放折扣需要输入折扣比率
	 */
	public void setValue(BigDecimal value) {
		if (hyi.cream.inline.Server.serverExist())
			setFieldValue("discountValue", value);
		else
			setFieldValue("DVALUE", value);
	}

	/**
	 * @return String
	 * 折扣基准   
	 *           ‘R’：依折扣前单价计算 
	 *           ‘N’：依折扣后单价计算
	 */
	public String getBase() {
		if (hyi.cream.inline.Server.serverExist())
			return (String) getFieldValue("discountBase");
		else
			return (String) getFieldValue("DBASE");
	}

	/**
	 * @return String
	 *           CHAR[0]: 四舍五入flag(‘R’:四舍五入 ‘C’:进位   ‘D’:舍去)
	 *           CHAR[1]:小数位数
	 *           CHAR[2]:SI相容（‘0’不相容）
	 *           CHAR[3]:No use
	 *           CHAR[4]:认证
	 *           CHAR[5]:M&M相容（‘0’不相容）
	 *           CHAR[6]:No use
	 *           CHAR[7]:No use
	 */
	public String getAttribute() {
		if (hyi.cream.inline.Server.serverExist())
			return (String) getFieldValue("discountAttribute");
		else
			return (String) getFieldValue("ATTR");
	}

}

