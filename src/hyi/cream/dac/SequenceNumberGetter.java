package hyi.cream.dac;

import java.util.*;
import java.math.*;
import hyi.cream.util.*;
import hyi.cream.inline.Server;

/**
 * Providing query method for sequence number from inline client.
 * Including transaction number, and Z number.
 *
 * <P>POS换机后第一次使用，必须连线，以便取得上次的最大交易序号和Z号。
 *
 * @author Bruce
 */
public class SequenceNumberGetter extends DacBase {
    private static boolean isServerSideOracle = Server.serverExist() && "oracle".equalsIgnoreCase(CreamProperties.getInstance().getProperty("DBType"));

    public SequenceNumberGetter() throws InstantiationException {
    }

    public List getPrimaryKeyList() {
        return null;
    }

    public String getInsertUpdateTableName() {
        return null;
    }

    /**
     * Query object method. This method will be invoked by ServerThread when
     * receiving "getObject hyi.cream.dac.SequenceNumberGetter [{pos_no},{tran|z}]" command.
     * 
     * @param data 
     * @return Collection Return a Collection contains a SequenceNumberGetter object for sequence number.
     */
    public static Collection queryObject(String data) {
        try {
            StringTokenizer tk = new StringTokenizer(data, ",");
            String posNumber = tk.nextToken();
            String type = tk.nextToken();
            List retList = new ArrayList();
            SequenceNumberGetter obj;
            if (type.equals("tran")) {
                if (isServerSideOracle)
                {
                    obj =
                            (SequenceNumberGetter)DacBase.getSingleObject(SequenceNumberGetter.class,
                                    "select tranno from (select transactionNumber tranno from posul_tranhead" +
                                            " where posNumber=" + posNumber + " order by systemDate desc) where rownum = 1");
                } else {

                    obj =
                            (SequenceNumberGetter)DacBase.getSingleObject(SequenceNumberGetter.class,
                                    "select transactionNumber tranno from posul_tranhead" +
                                            " where posNumber=" + posNumber + " order by systemDate desc limit 1");
                }

                retList.add(obj);
            } else if (type.equals("z")) {
                obj =
                    (SequenceNumberGetter)DacBase.getSingleObject(SequenceNumberGetter.class,
                    "select max(zSequenceNumber) zno from posul_z" +
                    " where posNumber=" + posNumber);
//                CreamToolkit.logMessage("****｜SequenceNumberGetter | max(zSequenceNumber) : " + obj.fieldMap);
                retList.add(obj);
            }
            return retList;

        } catch (NoSuchElementException e) {
            return null;
        }
    }
    
    public Integer getMaxTransactionNumber() {
        Object obj = getFieldValue("tranno") == null ? getFieldValue("TRANNO") : getFieldValue("tranno") ;
        
        if (obj == null)
            return null;
        else {
        	try {
        		return new Integer(obj.toString());
        	} catch (Exception e) {
        		return new Integer((new BigDecimal(obj.toString())).intValue());
        	}
        }
    }

    public Integer getMaxZNumber() {
        Object obj = getFieldValue("zno") == null ? getFieldValue("ZNO") : getFieldValue("zno");
        if (obj == null)
            return null;
        else {
        	try {
        		return new Integer(obj.toString());
        	} catch (Exception e) {
        		return new Integer((new BigDecimal(obj.toString())).intValue());
        	}
        }
    }
}
