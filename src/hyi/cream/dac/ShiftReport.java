// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   ShiftReport.java

package hyi.cream.dac;

import hyi.cream.inline.Server;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Referenced classes of package hyi.cream.dac:
//            DacBase, ZReport, Transaction

public class ShiftReport extends DacBase implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final transient String VERSION = "1.5";
    static ShiftReport currentShift = null;
    private static ArrayList primaryKeys;
    private BigDecimal taxAmount;

    static {
        primaryKeys = new ArrayList();
        if (Server.serverExist()) {
            primaryKeys.add("StoreID");
            primaryKeys.add("PosNumber");
            primaryKeys.add("ZSequenceNumber");
            primaryKeys.add("ShiftSequenceNumber");
        } else {
            primaryKeys.add("STORENO");
            primaryKeys.add("POSNO");
            primaryKeys.add("EODCNT");
            primaryKeys.add("SHIFTCNT");
        }
    }

    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public ShiftReport() throws InstantiationException {
        taxAmount = new BigDecimal(0.0D);
        setAccountingDate(CreamToolkit.getInitialDate());
        setStoreNumber(CreamProperties.getInstance().getProperty("StoreNumber"));
        String posNo = CreamProperties.getInstance().getProperty(
                "TerminalNumber");
        if (posNo != null)
            setTerminalNumber(Integer.decode(posNo));
        setCashierNumber(CreamProperties.getInstance().getProperty(
                "CashierNumber"));
        setMachineNumber(CreamProperties.getInstance().getProperty(
                "TerminalPhysicalNumber"));
        setUploadState("0");
    }

    public ShiftReport(int i) throws InstantiationException {
        taxAmount = new BigDecimal(0.0D);
    }

    public static Iterator queryByDateTime(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return DacBase
                .getMultipleObjects(hyi.cream.dac.ShiftReport.class,
                        "SELECT * FROM "
                                + getInsertUpdateTableNameStaticVersion()
                                + " WHERE ACCDATE='"
                                + df.format(date).toString() + "'");
    }

    public static Iterator queryByZNumber(Integer zNumber) {
        return DacBase.getMultipleObjects(hyi.cream.dac.ShiftReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                        + " WHERE EODCNT='" + zNumber + "'");
    }

    public static ShiftReport queryByZNumberAndShiftNumber(Integer zNumber,
            Integer shiftNumber) {
        return (ShiftReport) DacBase.getSingleObject(
                hyi.cream.dac.ShiftReport.class, "SELECT * FROM "
                        + getInsertUpdateTableNameStaticVersion()
                        + " WHERE EODCNT='" + zNumber + "' AND SHIFTCNT='"
                        + shiftNumber + "'");
    }

    public static Iterator queryRecentReports() {
        return DacBase.getMultipleObjects(hyi.cream.dac.ShiftReport.class,
                "SELECT SGNONDTTM, SGNOFDTTM, SHIFTCNT, EODCNT FROM "
                        + getInsertUpdateTableNameStaticVersion()
                        + " ORDER BY SGNONDTTM DESC LIMIT 20");
    }

    public static Iterator getUploadFailedList() {
        String failFlag = "2";
        return DacBase.getMultipleObjects(hyi.cream.dac.ShiftReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                        + " WHERE TCPFLG='" + failFlag + "'");
    }

    public static Iterator getNotUploadedList() {
        String notUploadFlag = "0";
        return DacBase.getMultipleObjects(hyi.cream.dac.ShiftReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                        + " WHERE TCPFLG='" + notUploadFlag + "'");
    }

    public static Iterator queryNotUploadReport() {
        return DacBase.getMultipleObjects(hyi.cream.dac.ShiftReport.class,
                "SELECT EODCNT,SHIFTCNT FROM "
                        + getInsertUpdateTableNameStaticVersion()
                        + " WHERE TCPFLG<>'1' AND ACCDATE<>'1970-01-01'");
    }

    public static ShiftReport getCurrentShift() {
        if (currentShift == null) {
            //Iterator iter = queryByDateTime(CreamToolkit.getInitialDate());
            //if (iter != null && iter.hasNext())
            //    currentShift = (ShiftReport) iter.next();

            ZReport currentZ = ZReport.getCurrentZReport();
            if (currentZ == null) // 如果没有current Z，就不可能有current Shift
                return null;

            // 看同一个Z序號中最大的那筆是否為197X年，如果是則返回他作為當前Shift
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            currentShift = (ShiftReport) DacBase.getSingleObject(hyi.cream.dac.ShiftReport.class,
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                    + " WHERE eodcnt=" + currentZ.getSequenceNumber() + " ORDER BY shiftcnt DESC LIMIT 1");
            if (currentShift != null && df.format(currentShift.getAccountingDate()).startsWith("197"))
                return currentShift;
            else
                currentShift = null;
        }
        return currentShift;
    }

    public static int getNextShiftNumber() {
        int z = 0;
        if (ZReport.queryByDateTime(CreamToolkit.getInitialDate()) == null)
            return 0;
        z = ZReport.getCurrentZNumber();
        String select = "SELECT MAX(SHIFTCNT) AS SHIFTNUMBER FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE EODCNT = "
                + z;
        Map value = DacBase.getValueOfStatement(select);
        Integer i = (Integer) value.get("SHIFTNUMBER");
        if (i == null)
            return 0;
        else
            return i.intValue() + 1;
    }

    public static synchronized ShiftReport createShiftReport() {
        try {
            Iterator iter = queryByDateTime(CreamToolkit.getInitialDate());
            if (iter != null && iter.hasNext()) {
                CreamToolkit
                        .logMessage("ShiftReport.createShiftReport(): A current shift report exists! Why?");
                currentShift = (ShiftReport) iter.next();
                return currentShift;
            }
            currentShift = new ShiftReport();
            int z = 0;
            if (ZReport.getCurrentZNumber() > 0)
                z = ZReport.getCurrentZNumber();
            if (ZReport.getCurrentZNumber() >= 0
                    && ZReport.queryByDateTime(CreamToolkit.getInitialDate()) == null)
                z++;
            currentShift.setSequenceNumber(new Integer(getNextShiftNumber()));
            currentShift.setZSequenceNumber(new Integer(z));
            currentShift.setSignOnSystemDateTime(new Date());
            currentShift
                    .setSignOffSystemDateTime(CreamToolkit.getInitialDate());
            currentShift.setBeginTransactionNumber(new Integer(Transaction
                    .getNextTransactionNumber()), true);
            currentShift.setEndTransactionNumber(new Integer(Transaction
                    .getNextTransactionNumber()), true);
            currentShift.setBeginInvoiceNumber("0", true);
            currentShift.setEndInvoiceNumber("0", true);
            currentShift.setMachineNumber(CreamProperties.getInstance()
                    .getProperty("TerminalPhysicalNumber"));
            String initialCashInAmount = CreamProperties.getInstance()
                    .getProperty("InitialCashInAmount");
            if (initialCashInAmount != null) {
                currentShift
                        .setCashInAmount(new BigDecimal(initialCashInAmount));
                currentShift.setCashInCount(new Integer(1));
            }
        } catch (InstantiationException ie) {
            CreamToolkit.logMessage(ie.toString());
        }
        if (!currentShift.insert()) {
            currentShift = (ShiftReport) currentShift.queryByPrimaryKey();
            CreamToolkit.logMessage("Create shift failed! (z="
                    + currentShift.getZSequenceNumber() + ", shift="
                    + currentShift.getSequenceNumber() + ")");
        }
        return currentShift;
    }

    public BigDecimal getGrossSales() {
        BigDecimal total = new BigDecimal(0.0D);
        for (int i = 0; i < 5; i++) {
            BigDecimal sub = (BigDecimal) getFieldValue("GROSALTX" + i + "AM");
            if (sub != null)
                total = total.add(sub);
        }

        return total;
    }

    public BigDecimal getMixAndMatchTotalAmount() {
        BigDecimal total = new BigDecimal(0.0D);
        for (int i = 0; i < 5; i++) {
            BigDecimal sub = (BigDecimal) getFieldValue("MNMAMT" + i);
            if (sub != null)
                total = total.add(sub);
        }

        return total;
    }

    public int getMixAndMatchTotalCount() {
        int total = 0;
        for (int i = 0; i < 5; i++) {
            Integer sub = (Integer) getFieldValue("MNMCNT" + i);
            if (sub != null)
                total += sub.intValue();
        }

        return total;
    }

    public BigDecimal getNetSalesTotalAmount() {
        BigDecimal total = new BigDecimal(0.0D);
        for (int i = 0; i < 5; i++) {
            BigDecimal sub = (BigDecimal) getFieldValue("NETSALAMT" + i);
            if (sub != null)
                total = total.add(sub);
        }

        return total;
    }

    public BigDecimal getTotalAmount() {
        return getNetSalesTotalAmount().add(getDaiShouAmount()).add(
                getDaiShouAmount2()).add(getDaiFuAmount().negate()).add(
                getSpillAmount().add(
                        getPaidInAmount().add(getPaidOutAmount().negate())));
    }

    public BigDecimal getTotalPayAmount() {
        BigDecimal total = new BigDecimal(0.0D);
        for (int i = 0; i < 20; i++) {
            String j = i + "";
            if (i < 10)
                j = "0" + i;
            BigDecimal sub = (BigDecimal) getFieldValue("PAY" + j + "AMT");
            if (sub != null)
                total = total.add(sub);
        }

        return total;
    }

    public String getStoreNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("StoreID");
        else
            return (String) getFieldValue("STORENO");
    }

    public void setStoreNumber(String storeNumber) {
        if (Server.serverExist())
            setFieldValue("StoreID", storeNumber);
        else
            setFieldValue("STORENO", storeNumber);
    }

    public Integer getTerminalNumber() {
        if (Server.serverExist())
            return (Integer) getFieldValue("PosNumber");
        else
            return (Integer) getFieldValue("POSNO");
    }

    public void setTerminalNumber(Integer posno) {
        if (Server.serverExist())
            setFieldValue("PosNumber", posno);
        else
            setFieldValue("POSNO", posno);
    }

    public String getMachineNumber() {
        return (String) getFieldValue("MACHINENO");
    }

    public void setMachineNumber(String machineno) {
        if (Server.serverExist())
            setFieldValue("MachineNumber", machineno);
        else
            setFieldValue("MACHINENO", machineno);
    }

    public Integer getSequenceNumber() {
        if (Server.serverExist())
            return (Integer) getFieldValue("ShiftSequenceNumber");
        else
            return (Integer) getFieldValue("SHIFTCNT");
    }

    public void setSequenceNumber(Integer SQNumber) {
        setFieldValue("SHIFTCNT", SQNumber);
    }

    public Integer getZSequenceNumber() {
        if (Server.serverExist())
            return (Integer) getFieldValue("ZSequenceNumber");
        else
            return (Integer) getFieldValue("EODCNT");
    }

    public void setZSequenceNumber(Integer ZNO) {
        setFieldValue("EODCNT", ZNO);
    }

    public Date getAccountingDate() {
        if (Server.serverExist())
            return (Date) getFieldValue("AccountDate");
        else
            return (Date) getFieldValue("ACCDATE");
    }

    public void setAccountingDate(Date accdate) {
        if (Server.serverExist())
            setFieldValue("AccountDate", accdate);
        else
            setFieldValue("ACCDATE", accdate);
    }

    public Date getSignOnSystemDateTime() {
        if (Server.serverExist())
            return (Date) getFieldValue("SignOnSystemDateTime");
        else
            return (Date) getFieldValue("SGNONDTTM");
    }

    public void setSignOnSystemDateTime(Date sgnondttm) {
        if (Server.serverExist())
            setFieldValue("SignOnSystemDateTime", sgnondttm);
        else
            setFieldValue("SGNONDTTM", sgnondttm);
    }

    public Date getSignOffSystemDateTime() {
        if (Server.serverExist())
            return (Date) getFieldValue("SignOffSystemDateTime");
        else
            return (Date) getFieldValue("SGNOFDTTM");
    }

    public void setSignOffSystemDateTime(Date sgnoffdttm) {
        if (Server.serverExist())
            setFieldValue("SignOffSystemDateTime", sgnoffdttm);
        else
            setFieldValue("SGNOFDTTM", sgnoffdttm);
    }

    public Integer getBeginTransactionNumber() {
        return (Integer) getFieldValue("SGNONSEQ");
    }

    public void setBeginTransactionNumber(Integer BNO, boolean isUpdProperty) {
        setFieldValue("SGNONSEQ", BNO);
        if (isUpdProperty)
            CreamProperties.getInstance().setProperty(
                "BeginTransactionNumberOfCashier", BNO.toString());
        CreamProperties.getInstance().deposit();
    }

    public Integer getEndTransactionNumber() {
        return (Integer) getFieldValue("SGNOFFSEQ");
    }

    public void setEndTransactionNumber(Integer ENO, boolean isUpdProperty) {
        setFieldValue("SGNOFFSEQ", ENO);
        if (isUpdProperty)
            CreamProperties.getInstance().setProperty(
                "EndTransactionNumberOfCashier", ENO.toString());
        CreamProperties.getInstance().deposit();
    }

    public String getBeginInvoiceNumber() {
        return (String) getFieldValue("SGNONINV");
    }

    public void setBeginInvoiceNumber(String BNO, boolean isUpdProperty) {
        setFieldValue("SGNONINV", BNO);
        if (isUpdProperty)
            CreamProperties.getInstance().setProperty(
                "BeginInvoiceNumberOfCashier", BNO);
        CreamProperties.getInstance().deposit();
    }

    public String getEndInvoiceNumber() {
        return (String) getFieldValue("SGNOFFINV");
    }

    public void setEndInvoiceNumber(String ENO, boolean isUpdProperty) {
        setFieldValue("SGNOFFINV", ENO);
        if (isUpdProperty)
            CreamProperties.getInstance().setProperty("EndInvoiceNumberOfCashier",
                ENO);
        CreamProperties.getInstance().deposit();
    }

    public String getUploadState() {
        return (String) getFieldValue("TCPFLG");
    }

    public void setUploadState(String TcpFlag) {
        if (Server.serverExist())
            setFieldValue("UploadState", TcpFlag);
        else
            setFieldValue("TCPFLG", TcpFlag);
    }

    public String getCashierNumber() {
        return (String) getFieldValue("CASHIER");
    }

    public void setCashierNumber(String cashierNo) {
        if (Server.serverExist())
            setFieldValue("CashierID", cashierNo);
        else
            setFieldValue("CASHIER", cashierNo);
    }

    public BigDecimal getTaxType0GrossSales() {
        return (BigDecimal) getFieldValue("GROSALTX0AM");
    }

    public void setTaxType0GrossSales(BigDecimal TaxType0GrossSales) {
        setFieldValue("GROSALTX0AM", TaxType0GrossSales);
    }

    public BigDecimal getTaxType1GrossSales() {
        return (BigDecimal) getFieldValue("GROSALTX1AM");
    }

    public void setTaxType1GrossSales(BigDecimal TaxType1GrossSales) {
        setFieldValue("GROSALTX1AM", TaxType1GrossSales);
    }

    public BigDecimal getTaxType2GrossSales() {
        return (BigDecimal) getFieldValue("GROSALTX2AM");
    }

    public void setTaxType2GrossSales(BigDecimal TaxType2GrossSales) {
        setFieldValue("GROSALTX2AM", TaxType2GrossSales);
    }

    public BigDecimal getTaxType3GrossSales() {
        return (BigDecimal) getFieldValue("GROSALTX3AM");
    }

    public void setTaxType3GrossSales(BigDecimal TaxType3GrossSales) {
        setFieldValue("GROSALTX3AM", TaxType3GrossSales);
    }

    public BigDecimal getTaxType4GrossSales() {
        return (BigDecimal) getFieldValue("GROSALTX4AM");
    }

    public void setTaxType4GrossSales(BigDecimal TaxType4GrossSales) {
        setFieldValue("GROSALTX4AM", TaxType4GrossSales);
    }

    public BigDecimal getTaxAmount0() {
        return (BigDecimal) getFieldValue("TAXAMT0");
    }

    public void setTaxAmount0(BigDecimal taxamt0) {
        setFieldValue("TAXAMT0", taxamt0);
    }

    public BigDecimal getTaxAmount1() {
        return (BigDecimal) getFieldValue("TAXAMT1");
    }

    public void setTaxAmount1(BigDecimal taxamt1) {
        setFieldValue("TAXAMT1", taxamt1);
    }

    public BigDecimal getTaxAmount2() {
        return (BigDecimal) getFieldValue("TAXAMT2");
    }

    public void setTaxAmount2(BigDecimal taxamt2) {
        setFieldValue("TAXAMT2", taxamt2);
    }

    public BigDecimal getTaxAmount3() {
        return (BigDecimal) getFieldValue("TAXAMT3");
    }

    public void setTaxAmount3(BigDecimal taxamt3) {
        setFieldValue("TAXAMT3", taxamt3);
    }

    public BigDecimal getTaxAmount4() {
        return (BigDecimal) getFieldValue("TAXAMT4");
    }

    public void setTaxAmount4(BigDecimal taxamt4) {
        setFieldValue("TAXAMT4", taxamt4);
    }

    public BigDecimal getTotalTaxAmount() {
        return getTaxAmount0().add(getTaxAmount1()).add(getTaxAmount2()).add(
                getTaxAmount3()).add(getTaxAmount4());
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxamt) {
        taxAmount = taxamt;
    }

    public BigDecimal getItemCount() {
        return (BigDecimal) getFieldValue("ITEMSALEQTY");
    }

    public void setItemCount(BigDecimal saleCount) {
        setFieldValue("ITEMSALEQTY", saleCount);
    }

    public Integer getTransactionCount() {
        Object o = getFieldValue("TRANCNT");
        int n = -1;
        if (o instanceof Integer)
            n = ((Integer) o).intValue();
        else if (o instanceof Long)
            n = ((Long) o).intValue();
        return new Integer(n);
    }

    public void setTransactionCount(Integer tranCount) {
        setFieldValue("TRANCNT", tranCount);
    }

    public Integer getCustomerCount() {
        return (Integer) getFieldValue("CUSTCNT");
    }

    public void setCustomerCount(Integer custcnt) {
        setFieldValue("CUSTCNT", custcnt);
    }

    public BigDecimal getSIPercentPlusTotalAmount() {
        BigDecimal total = getSIPercentPlusAmount0();
        total = total.add(getSIPercentPlusAmount1());
        total = total.add(getSIPercentPlusAmount2());
        total = total.add(getSIPercentPlusAmount3());
        total = total.add(getSIPercentPlusAmount4());
        return total;
    }

    public Integer getSIPercentPlusTotalCount() {
        int total = getSIPercentPlusCount0().intValue();
        total += getSIPercentPlusCount1().intValue();
        total += getSIPercentPlusCount2().intValue();
        total += getSIPercentPlusCount3().intValue();
        total += getSIPercentPlusCount4().intValue();
        return new Integer(total);
    }

    public BigDecimal getSIPercentOffTotalAmount() {
        BigDecimal total = getSIPercentOffAmount0();
        total = total.add(getSIPercentOffAmount1());
        total = total.add(getSIPercentOffAmount2());
        total = total.add(getSIPercentOffAmount3());
        total = total.add(getSIPercentOffAmount4());
        return total;
    }

    public Integer getSIPercentOffTotalCount() {
        int total = getSIPercentOffCount0().intValue();
        total += getSIPercentOffCount1().intValue();
        total += getSIPercentOffCount2().intValue();
        total += getSIPercentOffCount3().intValue();
        total += getSIPercentOffCount4().intValue();
        return new Integer(total);
    }

    public BigDecimal getSIDiscountTotalAmount() {
        BigDecimal total = getSIDiscountAmount0();
        total = total.add(getSIDiscountAmount1());
        total = total.add(getSIDiscountAmount2());
        total = total.add(getSIDiscountAmount3());
        total = total.add(getSIDiscountAmount4());
        return total;
    }

    public Integer getSIDiscountTotalCount() {
        int total = getSIDiscountCount0().intValue();
        total += getSIDiscountCount1().intValue();
        total += getSIDiscountCount2().intValue();
        total += getSIDiscountCount3().intValue();
        total += getSIDiscountCount4().intValue();
        return new Integer(total);
    }

    public BigDecimal getNotIncludedTotalSales() {
        BigDecimal total = getNotIncludedSales0();
        total = total.add(getNotIncludedSales1());
        total = total.add(getNotIncludedSales2());
        total = total.add(getNotIncludedSales3());
        total = total.add(getNotIncludedSales4());
        return total;
    }

    public Integer getTotalPayCount() {
        int total = 0;
        for (int i = 0; i < 20; i++) {
            String j = String.valueOf(i);
            if (i < 10)
                j = "0" + i;
            if (getFieldValue("PAY" + j + "CNT") != null) {
                Integer t = (Integer) getFieldValue("PAY" + j + "CNT");
                total += t.intValue();
            }
        }

        return new Integer(total);
    }

    public BigDecimal getSIPercentPlusAmount0() {
        return (BigDecimal) getFieldValue("SIPLUSAMT0");
    }

    public void setSIPercentPlusAmount0(BigDecimal SIPercentPlusAmount0) {
        setFieldValue("SIPLUSAMT0", SIPercentPlusAmount0);
    }

    public Integer getSIPercentPlusCount0() {
        return (Integer) getFieldValue("SIPLUSCNT0");
    }

    public void setSIPercentPlusCount0(Integer SIPercentPlusCount0) {
        setFieldValue("SIPLUSCNT0", SIPercentPlusCount0);
    }

    public BigDecimal getSIPercentOffAmount0() {
        return (BigDecimal) getFieldValue("SIPAMT0");
    }

    public void setSIPercentOffAmount0(BigDecimal SIPercentOffAmount0) {
        setFieldValue("SIPAMT0", SIPercentOffAmount0);
    }

    public Integer getSIPercentOffCount0() {
        return (Integer) getFieldValue("SIPCNT0");
    }

    public void setSIPercentOffCount0(Integer SIPercentOffCount0) {
        setFieldValue("SIPCNT0", SIPercentOffCount0);
    }

    public BigDecimal getSIDiscountAmount0() {
        return (BigDecimal) getFieldValue("SIMAMT0");
    }

    public void setSIDiscountAmount0(BigDecimal SIDiscountAmount0) {
        setFieldValue("SIMAMT0", SIDiscountAmount0);
    }

    public Integer getSIDiscountCount0() {
        return (Integer) getFieldValue("SIMCNT0");
    }

    public void setSIDiscountCount0(Integer SIDiscountCount0) {
        setFieldValue("SIMCNT0", SIDiscountCount0);
    }

    public BigDecimal getMixAndMatchAmount0() {
        return (BigDecimal) getFieldValue("MNMAMT0");
    }

    public void setMixAndMatchAmount0(BigDecimal mnmamt0) {
        setFieldValue("MNMAMT0", mnmamt0);
    }

    public Integer getMixAndMatchCount0() {
        return (Integer) getFieldValue("MNMCNT0");
    }

    public void setMixAndMatchCount0(Integer MixAndMatchCount0) {
        setFieldValue("MNMCNT0", MixAndMatchCount0);
    }

    public BigDecimal getNotIncludedSales0() {
        return (BigDecimal) getFieldValue("RCPGIFAMT0");
    }

    public void setNotIncludedSales0(BigDecimal rcpgifamt0) {
        setFieldValue("RCPGIFAMT0", rcpgifamt0);
    }

    public BigDecimal getNetSalesAmount0() {
        return (BigDecimal) getFieldValue("NETSALAMT0");
    }

    public void setNetSalesAmount0(BigDecimal netsalamt0) {
        setFieldValue("NETSALAMT0", netsalamt0);
    }

    public BigDecimal getSIPercentPlusAmount1() {
        return (BigDecimal) getFieldValue("SIPLUSAMT1");
    }

    public void setSIPercentPlusAmount1(BigDecimal SIPercentPlusAmount1) {
        setFieldValue("SIPLUSAMT1", SIPercentPlusAmount1);
    }

    public Integer getSIPercentPlusCount1() {
        return (Integer) getFieldValue("SIPLUSCNT1");
    }

    public void setSIPercentPlusCount1(Integer SIPercentPlusCount1) {
        setFieldValue("SIPLUSCNT1", SIPercentPlusCount1);
    }

    public BigDecimal getSIPercentOffAmount1() {
        return (BigDecimal) getFieldValue("SIPAMT1");
    }

    public void setSIPercentOffAmount1(BigDecimal SIPercentOffAmount1) {
        setFieldValue("SIPAMT1", SIPercentOffAmount1);
    }

    public Integer getSIPercentOffCount1() {
        return (Integer) getFieldValue("SIPCNT1");
    }

    public void setSIPercentOffCount1(Integer SIPercentOffCount1) {
        setFieldValue("SIPCNT1", SIPercentOffCount1);
    }

    public BigDecimal getSIDiscountAmount1() {
        return (BigDecimal) getFieldValue("SIMAMT1");
    }

    public void setSIDiscountAmount1(BigDecimal SIDiscountAmount1) {
        setFieldValue("SIMAMT1", SIDiscountAmount1);
    }

    public Integer getSIDiscountCount1() {
        return (Integer) getFieldValue("SIMCNT1");
    }

    public void setSIDiscountCount1(Integer SIDiscountCount1) {
        setFieldValue("SIMCNT1", SIDiscountCount1);
    }

    public BigDecimal getMixAndMatchAmount1() {
        return (BigDecimal) getFieldValue("MNMAMT1");
    }

    public void setMixAndMatchAmount1(BigDecimal mnmamt1) {
        setFieldValue("MNMAMT1", mnmamt1);
    }

    public Integer getMixAndMatchCount1() {
        return (Integer) getFieldValue("MNMCNT1");
    }

    public void setMixAndMatchCount1(Integer MixAndMatchCount1) {
        setFieldValue("MNMCNT1", MixAndMatchCount1);
    }

    public BigDecimal getNotIncludedSales1() {
        return (BigDecimal) getFieldValue("RCPGIFAMT1");
    }

    public void setNotIncludedSales1(BigDecimal rcpgifamt1) {
        setFieldValue("RCPGIFAMT1", rcpgifamt1);
    }

    public BigDecimal getNetSalesAmount1() {
        return (BigDecimal) getFieldValue("NETSALAMT1");
    }

    public void setNetSalesAmount1(BigDecimal netsalamt1) {
        setFieldValue("NETSALAMT1", netsalamt1);
    }

    public BigDecimal getSIPercentPlusAmount2() {
        return (BigDecimal) getFieldValue("SIPLUSAMT2");
    }

    public void setSIPercentPlusAmount2(BigDecimal SIPercentPlusAmount2) {
        setFieldValue("SIPLUSAMT2", SIPercentPlusAmount2);
    }

    public Integer getSIPercentPlusCount2() {
        return (Integer) getFieldValue("SIPLUSCNT2");
    }

    public void setSIPercentPlusCount2(Integer SIPercentPlusCount2) {
        setFieldValue("SIPLUSCNT2", SIPercentPlusCount2);
    }

    public BigDecimal getSIPercentOffAmount2() {
        return (BigDecimal) getFieldValue("SIPAMT2");
    }

    public void setSIPercentOffAmount2(BigDecimal SIPercentOffAmount2) {
        setFieldValue("SIPAMT2", SIPercentOffAmount2);
    }

    public Integer getSIPercentOffCount2() {
        return (Integer) getFieldValue("SIPCNT2");
    }

    public void setSIPercentOffCount2(Integer SIPercentOffCount2) {
        setFieldValue("SIPCNT2", SIPercentOffCount2);
    }

    public BigDecimal getSIDiscountAmount2() {
        return (BigDecimal) getFieldValue("SIMAMT2");
    }

    public void setSIDiscountAmount2(BigDecimal SIDiscountAmount2) {
        setFieldValue("SIMAMT2", SIDiscountAmount2);
    }

    public Integer getSIDiscountCount2() {
        return (Integer) getFieldValue("SIMCNT2");
    }

    public void setSIDiscountCount2(Integer SIDiscountCount2) {
        setFieldValue("SIMCNT2", SIDiscountCount2);
    }

    public BigDecimal getMixAndMatchAmount2() {
        return (BigDecimal) getFieldValue("MNMAMT2");
    }

    public void setMixAndMatchAmount2(BigDecimal mnmamt2) {
        setFieldValue("MNMAMT2", mnmamt2);
    }

    public Integer getMixAndMatchCount2() {
        return (Integer) getFieldValue("MNMCNT2");
    }

    public void setMixAndMatchCount2(Integer MixAndMatchCount2) {
        setFieldValue("MNMCNT2", MixAndMatchCount2);
    }

    public BigDecimal getNotIncludedSales2() {
        return (BigDecimal) getFieldValue("RCPGIFAMT2");
    }

    public void setNotIncludedSales2(BigDecimal rcpgifamt2) {
        setFieldValue("RCPGIFAMT2", rcpgifamt2);
    }

    public BigDecimal getNetSalesAmount2() {
        return (BigDecimal) getFieldValue("NETSALAMT2");
    }

    public void setNetSalesAmount2(BigDecimal netsalamt2) {
        setFieldValue("NETSALAMT2", netsalamt2);
    }

    public BigDecimal getSIPercentPlusAmount3() {
        return (BigDecimal) getFieldValue("SIPLUSAMT3");
    }

    public void setSIPercentPlusAmount3(BigDecimal SIPercentPlusAmount3) {
        setFieldValue("SIPLUSAMT3", SIPercentPlusAmount3);
    }

    public Integer getSIPercentPlusCount3() {
        return (Integer) getFieldValue("SIPLUSCNT3");
    }

    public void setSIPercentPlusCount3(Integer SIPercentPlusCount3) {
        setFieldValue("SIPLUSCNT3", SIPercentPlusCount3);
    }

    public BigDecimal getSIPercentOffAmount3() {
        return (BigDecimal) getFieldValue("SIPAMT3");
    }

    public void setSIPercentOffAmount3(BigDecimal SIPercentOffAmount3) {
        setFieldValue("SIPAMT3", SIPercentOffAmount3);
    }

    public Integer getSIPercentOffCount3() {
        return (Integer) getFieldValue("SIPCNT3");
    }

    public void setSIPercentOffCount3(Integer SIPercentOffCount3) {
        setFieldValue("SIPCNT3", SIPercentOffCount3);
    }

    public BigDecimal getSIDiscountAmount3() {
        return (BigDecimal) getFieldValue("SIMAMT3");
    }

    public void setSIDiscountAmount3(BigDecimal SIDiscountAmount3) {
        setFieldValue("SIMAMT3", SIDiscountAmount3);
    }

    public Integer getSIDiscountCount3() {
        return (Integer) getFieldValue("SIMCNT3");
    }

    public void setSIDiscountCount3(Integer SIDiscountCount3) {
        setFieldValue("SIMCNT3", SIDiscountCount3);
    }

    public BigDecimal getMixAndMatchAmount3() {
        return (BigDecimal) getFieldValue("MNMAMT3");
    }

    public void setMixAndMatchAmount3(BigDecimal mnmamt3) {
        setFieldValue("MNMAMT3", mnmamt3);
    }

    public Integer getMixAndMatchCount3() {
        return (Integer) getFieldValue("MNMCNT3");
    }

    public void setMixAndMatchCount3(Integer MixAndMatchCount3) {
        setFieldValue("MNMCNT3", MixAndMatchCount3);
    }

    public BigDecimal getNotIncludedSales3() {
        return (BigDecimal) getFieldValue("RCPGIFAMT3");
    }

    public void setNotIncludedSales3(BigDecimal rcpgifamt3) {
        setFieldValue("RCPGIFAMT3", rcpgifamt3);
    }

    public BigDecimal getNetSalesAmount3() {
        return (BigDecimal) getFieldValue("NETSALAMT3");
    }

    public void setNetSalesAmount3(BigDecimal netsalamt3) {
        setFieldValue("NETSALAMT3", netsalamt3);
    }

    public BigDecimal getSIPercentPlusAmount4() {
        return (BigDecimal) getFieldValue("SIPLUSAMT4");
    }

    public void setSIPercentPlusAmount4(BigDecimal SIPercentPlusAmount4) {
        setFieldValue("SIPLUSAMT4", SIPercentPlusAmount4);
    }

    public Integer getSIPercentPlusCount4() {
        return (Integer) getFieldValue("SIPLUSCNT4");
    }

    public void setSIPercentPlusCount4(Integer SIPercentPlusCount4) {
        setFieldValue("SIPLUSCNT4", SIPercentPlusCount4);
    }

    public BigDecimal getSIPercentOffAmount4() {
        return (BigDecimal) getFieldValue("SIPAMT4");
    }

    public void setSIPercentOffAmount4(BigDecimal SIPercentOffAmount4) {
        setFieldValue("SIPAMT4", SIPercentOffAmount4);
    }

    public Integer getSIPercentOffCount4() {
        return (Integer) getFieldValue("SIPCNT4");
    }

    public void setSIPercentOffCount4(Integer SIPercentOffCount4) {
        setFieldValue("SIPCNT4", SIPercentOffCount4);
    }

    public BigDecimal getSIDiscountAmount4() {
        return (BigDecimal) getFieldValue("SIMAMT4");
    }

    public void setSIDiscountAmount4(BigDecimal SIDiscountAmount4) {
        setFieldValue("SIMAMT4", SIDiscountAmount4);
    }

    public Integer getSIDiscountCount4() {
        return (Integer) getFieldValue("SIMCNT4");
    }

    public void setSIDiscountCount4(Integer SIDiscountCount4) {
        setFieldValue("SIMCNT4", SIDiscountCount4);
    }

    public BigDecimal getMixAndMatchAmount4() {
        return (BigDecimal) getFieldValue("MNMAMT4");
    }

    public void setMixAndMatchAmount4(BigDecimal mnmamt4) {
        setFieldValue("MNMAMT4", mnmamt4);
    }

    public Integer getMixAndMatchCount4() {
        return (Integer) getFieldValue("MNMCNT4");
    }

    public void setMixAndMatchCount4(Integer MixAndMatchCount4) {
        setFieldValue("MNMCNT4", MixAndMatchCount4);
    }

    public BigDecimal getNotIncludedSales4() {
        return (BigDecimal) getFieldValue("RCPGIFAMT4");
    }

    public void setNotIncludedSales4(BigDecimal rcpgifamt4) {
        setFieldValue("RCPGIFAMT4", rcpgifamt4);
    }

    public BigDecimal getNetSalesAmount4() {
        return (BigDecimal) getFieldValue("NETSALAMT4");
    }

    public void setNetSalesAmount4(BigDecimal netsalamt4) {
        setFieldValue("NETSALAMT4", netsalamt4);
    }

    public BigDecimal getVoucherAmount() {
        return (BigDecimal) getFieldValue("VALPAMT");
    }

    public void setVoucherAmount(BigDecimal VoucherAmount) {
        setFieldValue("VALPAMT", VoucherAmount);
    }

    public Integer getVoucherCount() {
        return (Integer) getFieldValue("VALPCNT");
    }

    public void setVoucherCount(Integer VoucherCount) {
        setFieldValue("VALPCNT", VoucherCount);
    }

    public BigDecimal getPreRcvAmount() {
        return (BigDecimal) getFieldValue("PRCVAMT");
    }

    public void setPreRcvAmount(BigDecimal amt) {
        setFieldValue("PRCVAMT", amt);
    }

    public Integer getPreRcvCount() {
        return (Integer) getFieldValue("PRCVCNT");
    }

    public void setPreRcvCount(Integer cnt) {
        setFieldValue("PRCVCNT", cnt);
    }

    public BigDecimal getPreRcvDetailAmount1() {
        return (BigDecimal) getFieldValue("PRCVPLUAMT1");
    }

    public void setPreRcvDetailAmount1(BigDecimal amt) {
        setFieldValue("PRCVPLUAMT1", amt);
    }

    public Integer getPreRcvDetailCount1() {
        return (Integer) getFieldValue("PRCVPLUCNT1");
    }

    public void setPreRcvDetailCount1(Integer cnt) {
        setFieldValue("PRCVPLUCNT1", cnt);
    }

    public BigDecimal getPreRcvDetailAmount2() {
        return (BigDecimal) getFieldValue("PRCVPLUAMT2");
    }

    public void setPreRcvDetailAmount2(BigDecimal amt) {
        setFieldValue("PRCVPLUAMT2", amt);
    }

    public Integer getPreRcvDetailCount2() {
        return (Integer) getFieldValue("PRCVPLUCNT2");
    }

    public void setPreRcvDetailCount2(Integer cnt) {
        setFieldValue("PRCVPLUCNT2", cnt);
    }

    public BigDecimal getPreRcvDetailAmount3() {
        return (BigDecimal) getFieldValue("PRCVPLUAMT3");
    }

    public void setPreRcvDetailAmount3(BigDecimal amt) {
        setFieldValue("PRCVPLUAMT3", amt);
    }

    public Integer getPreRcvDetailCount3() {
        return (Integer) getFieldValue("PRCVPLUCNT3");
    }

    public void setPreRcvDetailCount3(Integer cnt) {
        setFieldValue("PRCVPLUCNT3", cnt);
    }

    public BigDecimal getPreRcvDetailAmount4() {
        return (BigDecimal) getFieldValue("PRCVPLUAMT4");
    }

    public void setPreRcvDetailAmount4(BigDecimal amt) {
        setFieldValue("PRCVPLUAMT4", amt);
    }

    public Integer getPreRcvDetailCount4() {
        return (Integer) getFieldValue("PRCVPLUCNT4");
    }

    public void setPreRcvDetailCount4(Integer cnt) {
        setFieldValue("PRCVPLUCNT4", cnt);
    }

    public BigDecimal getPreRcvDetailAmount5() {
        return (BigDecimal) getFieldValue("PRCVPLUAMT5");
    }

    public void setPreRcvDetailAmount5(BigDecimal amt) {
        setFieldValue("PRCVPLUAMT5", amt);
    }

    public Integer getPreRcvDetailCount5() {
        return (Integer) getFieldValue("PRCVPLUCNT5");
    }

    public void setPreRcvDetailCount5(Integer cnt) {
        setFieldValue("PRCVPLUCNT5", cnt);
    }

    public BigDecimal getDaiShouAmount() {
        return (BigDecimal) getFieldValue("DAISHOUAMT");
    }

    public void setDaiShouAmount(BigDecimal daishouamt) {
        setFieldValue("DAISHOUAMT", daishouamt);
    }

    public Integer getDaiShouCount() {
        return (Integer) getFieldValue("DAISHOUCNT");
    }

    public void setDaiShouCount(Integer daishoucnt) {
        setFieldValue("DAISHOUCNT", daishoucnt);
    }

    public BigDecimal getDaiShouDetailAmount1() {
        return (BigDecimal) getFieldValue("DAISHOUPLUAMT1");
    }

    public void setDaiShouDetailAmount1(BigDecimal daishoupluamt) {
        setFieldValue("DAISHOUPLUAMT1", daishoupluamt);
    }

    public Integer getDaiShouDetailCount1() {
        return (Integer) getFieldValue("DAISHOUPLUCNT1");
    }

    public void setDaiShouDetailCount1(Integer daishouplucnt) {
        setFieldValue("DAISHOUPLUCNT1", daishouplucnt);
    }

    public BigDecimal getDaiShouDetailAmount2() {
        return (BigDecimal) getFieldValue("DAISHOUPLUAMT2");
    }

    public void setDaiShouDetailAmount2(BigDecimal daishoupluamt) {
        setFieldValue("DAISHOUPLUAMT2", daishoupluamt);
    }

    public Integer getDaiShouDetailCount2() {
        return (Integer) getFieldValue("DAISHOUPLUCNT2");
    }

    public void setDaiShouDetailCount2(Integer daishouplucnt) {
        setFieldValue("DAISHOUPLUCNT2", daishouplucnt);
    }

    public BigDecimal getDaiShouDetailAmount3() {
        return (BigDecimal) getFieldValue("DAISHOUPLUAMT3");
    }

    public void setDaiShouDetailAmount3(BigDecimal daishoupluamt) {
        setFieldValue("DAISHOUPLUAMT3", daishoupluamt);
    }

    public Integer getDaiShouDetailCount3() {
        return (Integer) getFieldValue("DAISHOUPLUCNT3");
    }

    public void setDaiShouDetailCount3(Integer daishouplucnt) {
        setFieldValue("DAISHOUPLUCNT3", daishouplucnt);
    }

    public BigDecimal getDaiShouDetailAmount4() {
        return (BigDecimal) getFieldValue("DAISHOUPLUAMT4");
    }

    public void setDaiShouDetailAmount4(BigDecimal daishoupluamt) {
        setFieldValue("DAISHOUPLUAMT4", daishoupluamt);
    }

    public Integer getDaiShouDetailCount4() {
        return (Integer) getFieldValue("DAISHOUPLUCNT4");
    }

    public void setDaiShouDetailCount4(Integer daishouplucnt) {
        setFieldValue("DAISHOUPLUCNT4", daishouplucnt);
    }

    public BigDecimal getDaiShouDetailAmount5() {
        return (BigDecimal) getFieldValue("DAISHOUPLUAMT5");
    }

    public void setDaiShouDetailAmount5(BigDecimal daishoupluamt) {
        setFieldValue("DAISHOUPLUAMT5", daishoupluamt);
    }

    public Integer getDaiShouDetailCount5() {
        return (Integer) getFieldValue("DAISHOUPLUCNT5");
    }

    public void setDaiShouDetailCount5(Integer daishouplucnt) {
        setFieldValue("DAISHOUPLUCNT5", daishouplucnt);
    }

    public BigDecimal getDaiShouAmount2() {
        return (BigDecimal) getFieldValue("DAISHOUAMT2");
    }

    public void setDaiShouAmount2(BigDecimal daishouamt) {
        setFieldValue("DAISHOUAMT2", daishouamt);
    }

    public Integer getDaiShouCount2() {
        return (Integer) getFieldValue("DAISHOUCNT2");
    }

    public void setDaiShouCount2(Integer daishoucnt) {
        setFieldValue("DAISHOUCNT2", daishoucnt);
    }

    public BigDecimal getDaiFuAmount() {
        return (BigDecimal) getFieldValue("DAIFUAMT");
    }

    public void setDaiFuAmount(BigDecimal daifuamt) {
        setFieldValue("DAIFUAMT", daifuamt);
    }

    public Integer getDaiFuCount() {
        return (Integer) getFieldValue("DAIFUCNT");
    }

    public void setDaiFuCount(Integer daifucnt) {
        setFieldValue("DAIFUCNT", daifucnt);
    }

    public BigDecimal getDaiFuDetailAmount1() {
        return (BigDecimal) getFieldValue("DAIFUPLUAMT1");
    }

    public void setDaiFuDetailAmount1(BigDecimal daifupluamt) {
        setFieldValue("DAIFUPLUAMT1", daifupluamt);
    }

    public Integer getDaiFuDetailCount1() {
        return (Integer) getFieldValue("DAIFUPLUCNT1");
    }

    public void setDaiFuDetailCount1(Integer daifuplucnt) {
        setFieldValue("DAIFUPLUCNT1", daifuplucnt);
    }

    public BigDecimal getDaiFuDetailAmount2() {
        return (BigDecimal) getFieldValue("DAIFUPLUAMT2");
    }

    public void setDaiFuDetailAmount2(BigDecimal daifupluamt) {
        setFieldValue("DAIFUPLUAMT2", daifupluamt);
    }

    public Integer getDaiFuDetailCount2() {
        return (Integer) getFieldValue("DAIFUPLUCNT2");
    }

    public void setDaiFuDetailCount2(Integer daifuplucnt) {
        setFieldValue("DAIFUPLUCNT2", daifuplucnt);
    }

    public BigDecimal getDaiFuDetailAmount3() {
        return (BigDecimal) getFieldValue("DAIFUPLUAMT3");
    }

    public void setDaiFuDetailAmount3(BigDecimal daifupluamt) {
        setFieldValue("DAIFUPLUAMT3", daifupluamt);
    }

    public Integer getDaiFuDetailCount3() {
        return (Integer) getFieldValue("DAIFUPLUCNT3");
    }

    public void setDaiFuDetailCount3(Integer daifuplucnt) {
        setFieldValue("DAIFUPLUCNT3", daifuplucnt);
    }

    public BigDecimal getDaiFuDetailAmount4() {
        return (BigDecimal) getFieldValue("DAIFUPLUAMT4");
    }

    public void setDaiFuDetailAmount4(BigDecimal daifupluamt) {
        setFieldValue("DAIFUPLUAMT4", daifupluamt);
    }

    public Integer getDaiFuDetailCount4() {
        return (Integer) getFieldValue("DAIFUPLUCNT4");
    }

    public void setDaiFuDetailCount4(Integer daifuplucnt) {
        setFieldValue("DAIFUPLUCNT4", daifuplucnt);
    }

    public BigDecimal getDaiFuDetailAmount5() {
        return (BigDecimal) getFieldValue("DAIFUPLUAMT5");
    }

    public void setDaiFuDetailAmount5(BigDecimal daifupluamt) {
        setFieldValue("DAIFUPLUAMT5", daifupluamt);
    }

    public Integer getDaiFuDetailCount5() {
        return (Integer) getFieldValue("DAIFUPLUCNT5");
    }

    public void setDaiFuDetailCount5(Integer daifuplucnt) {
        setFieldValue("DAIFUPLUCNT5", daifuplucnt);
    }

    public BigDecimal getPaidInAmount() {
        return (BigDecimal) getFieldValue("PINAMT");
    }

    public void setPaidInAmount(BigDecimal pinamt) {
        setFieldValue("PINAMT", pinamt);
    }

    public Integer getPaidInCount() {
        return (Integer) getFieldValue("PINCNT");
    }

    public void setPaidInCount(Integer pincnt) {
        setFieldValue("PINCNT", pincnt);
    }

    public BigDecimal getPaidInDetailAmount1() {
        return (BigDecimal) getFieldValue("PINPLUAMT1");
    }

    public void setPaidInDetailAmount1(BigDecimal pinpluamt1) {
        setFieldValue("PINPLUAMT1", pinpluamt1);
    }

    public Integer getPaidInDetailCount1() {
        return (Integer) getFieldValue("PINPLUCNT1");
    }

    public void setPaidInDetailCount1(Integer pinplucnt1) {
        setFieldValue("PINPLUCNT1", pinplucnt1);
    }

    public BigDecimal getPaidInDetailAmount2() {
        return (BigDecimal) getFieldValue("PINPLUAMT2");
    }

    public void setPaidInDetailAmount2(BigDecimal pinpluamt2) {
        setFieldValue("PINPLUAMT2", pinpluamt2);
    }

    public Integer getPaidInDetailCount2() {
        return (Integer) getFieldValue("PINPLUCNT2");
    }

    public void setPaidInDetailCount2(Integer pinplucnt2) {
        setFieldValue("PINPLUCNT2", pinplucnt2);
    }

    public BigDecimal getPaidInDetailAmount3() {
        return (BigDecimal) getFieldValue("PINPLUAMT3");
    }

    public void setPaidInDetailAmount3(BigDecimal pinpluamt3) {
        setFieldValue("PINPLUAMT3", pinpluamt3);
    }

    public Integer getPaidInDetailCount3() {
        return (Integer) getFieldValue("PINPLUCNT3");
    }

    public void setPaidInDetailCount3(Integer pinplucnt3) {
        setFieldValue("PINPLUCNT3", pinplucnt3);
    }

    public BigDecimal getPaidInDetailAmount4() {
        return (BigDecimal) getFieldValue("PINPLUAMT4");
    }

    public void setPaidInDetailAmount4(BigDecimal pinpluamt4) {
        setFieldValue("PINPLUAMT4", pinpluamt4);
    }

    public Integer getPaidInDetailCount4() {
        return (Integer) getFieldValue("PINPLUCNT4");
    }

    public void setPaidInDetailCount4(Integer pinplucnt4) {
        setFieldValue("PINPLUCNT4", pinplucnt4);
    }

    public BigDecimal getPaidInDetailAmount5() {
        return (BigDecimal) getFieldValue("PINPLUAMT5");
    }

    public void setPaidInDetailAmount5(BigDecimal pinpluamt5) {
        setFieldValue("PINPLUAMT5", pinpluamt5);
    }

    public Integer getPaidInDetailCount5() {
        return (Integer) getFieldValue("PINPLUCNT5");
    }

    public void setPaidInDetailCount5(Integer pinplucnt5) {
        setFieldValue("PINPLUCNT5", pinplucnt5);
    }

    public BigDecimal getPaidOutAmount() {
        return (BigDecimal) getFieldValue("POUTAMT");
    }

    public void setPaidOutAmount(BigDecimal poutamt) {
        setFieldValue("POUTAMT", poutamt);
    }

    public Integer getPaidOutCount() {
        return (Integer) getFieldValue("POUTCNT");
    }

    public void setPaidOutCount(Integer poutcnt) {
        setFieldValue("POUTCNT", poutcnt);
    }

    public BigDecimal getPaidOutDetailAmount1() {
        return (BigDecimal) getFieldValue("POUTPLUAMT1");
    }

    public void setPaidOutDetailAmount1(BigDecimal poutpluamt1) {
        setFieldValue("POUTPLUAMT1", poutpluamt1);
    }

    public Integer getPaidOutDetailCount1() {
        return (Integer) getFieldValue("POUTPLUCNT1");
    }

    public void setPaidOutDetailCount1(Integer poutplucnt1) {
        setFieldValue("POUTPLUCNT1", poutplucnt1);
    }

    public BigDecimal getPaidOutDetailAmount2() {
        return (BigDecimal) getFieldValue("POUTPLUAMT2");
    }

    public void setPaidOutDetailAmount2(BigDecimal poutpluamt2) {
        setFieldValue("POUTPLUAMT2", poutpluamt2);
    }

    public Integer getPaidOutDetailCount2() {
        return (Integer) getFieldValue("POUTPLUCNT2");
    }

    public void setPaidOutDetailCount2(Integer poutplucnt2) {
        setFieldValue("POUTPLUCNT2", poutplucnt2);
    }

    public BigDecimal getPaidOutDetailAmount3() {
        return (BigDecimal) getFieldValue("POUTPLUAMT3");
    }

    public void setPaidOutDetailAmount3(BigDecimal poutpluamt3) {
        setFieldValue("POUTPLUAMT3", poutpluamt3);
    }

    public Integer getPaidOutDetailCount3() {
        return (Integer) getFieldValue("POUTPLUCNT3");
    }

    public void setPaidOutDetailCount3(Integer poutplucnt3) {
        setFieldValue("POUTPLUCNT3", poutplucnt3);
    }

    public BigDecimal getPaidOutDetailAmount4() {
        return (BigDecimal) getFieldValue("POUTPLUAMT4");
    }

    public void setPaidOutDetailAmount4(BigDecimal poutpluamt4) {
        setFieldValue("POUTPLUAMT4", poutpluamt4);
    }

    public Integer getPaidOutDetailCount4() {
        return (Integer) getFieldValue("POUTPLUCNT4");
    }

    public void setPaidOutDetailCount4(Integer poutplucnt4) {
        setFieldValue("POUTPLUCNT4", poutplucnt4);
    }

    public BigDecimal getPaidOutDetailAmount5() {
        return (BigDecimal) getFieldValue("POUTPLUAMT5");
    }

    public void setPaidOutDetailAmount5(BigDecimal poutpluamt5) {
        setFieldValue("POUTPLUAMT5", poutpluamt5);
    }

    public Integer getPaidOutDetailCount5() {
        return (Integer) getFieldValue("POUTPLUCNT5");
    }

    public void setPaidOutDetailCount5(Integer poutplucnt5) {
        setFieldValue("POUTPLUCNT5", poutplucnt5);
    }

    public BigDecimal getExchangeAmount() {
        return (BigDecimal) getFieldValue("EXGAMT");
    }

    public void setExchangeAmount(BigDecimal exgamt) {
        setFieldValue("EXGAMT", exgamt);
    }

    public BigDecimal getExchangeItemQuantity() {
        return (BigDecimal) getFieldValue("EXGITEMQTY");
    }

    public void setExchangeItemQuantity(BigDecimal exgitemqty) {
        setFieldValue("EXGITEMQTY", exgitemqty);
    }

    public BigDecimal getReturnAmount() {
        return (BigDecimal) getFieldValue("RTNAMT");
    }

    public void setReturnAmount(BigDecimal returnAmount) {
        setFieldValue("RTNAMT", returnAmount);
    }

    public BigDecimal getReturnItemQuantity() {
        return (BigDecimal) getFieldValue("RTNITEMQTY");
    }

    public void setReturnItemQuantity(BigDecimal rtnitemqty) {
        setFieldValue("RTNITEMQTY", rtnitemqty);
    }

    public Integer getReturnTransactionCount() {
        return (Integer) getFieldValue("RTNTRANCNT");
    }

    public void setReturnTransactionCount(Integer returnCount) {
        setFieldValue("RTNTRANCNT", returnCount);
    }

    public BigDecimal getTransactionVoidAmount() {
        return (BigDecimal) getFieldValue("VOIDINVAMT");
    }

    public void setTransactionVoidAmount(BigDecimal VoidAmount) {
        setFieldValue("VOIDINVAMT", VoidAmount);
    }

    public Integer getTransactionVoidCount() {
        return (Integer) getFieldValue("VOIDINVCNT");
    }

    public void setTransactionVoidCount(Integer VoidCount) {
        setFieldValue("VOIDINVCNT", VoidCount);
    }

    public BigDecimal getTransactionCancelAmount() {
        return (BigDecimal) getFieldValue("CANCELAMT");
    }

    public void setTransactionCancelAmount(BigDecimal transactionCancelAmount) {
        setFieldValue("CANCELAMT", transactionCancelAmount);
    }

    public Integer getTransactionCancelCount() {
        return (Integer) getFieldValue("CANCELCNT");
    }

    public void setTransactionCancelCount(Integer transactionCancelCount) {
        setFieldValue("CANCELCNT", transactionCancelCount);
    }

    public BigDecimal getLineVoidAmount() {
        return (BigDecimal) getFieldValue("UPDAMT");
    }

    public void setLineVoidAmount(BigDecimal LineVoidAmount) {
        setFieldValue("UPDAMT", LineVoidAmount);
    }

    public Integer getLineVoidCount() {
        return (Integer) getFieldValue("UPDCNT");
    }

    public void setLineVoidCount(Integer LineVoidCount) {
        setFieldValue("UPDCNT", LineVoidCount);
    }

    public BigDecimal getVoidAmount() {
        return (BigDecimal) getFieldValue("NOWUPDAMT");
    }

    public void setVoidAmount(BigDecimal VoidAmount) {
        setFieldValue("NOWUPDAMT", VoidAmount);
    }

    public Integer getVoidCount() {
        return (Integer) getFieldValue("NOWUPDCNT");
    }

    public void setVoidCount(Integer VoidCount) {
        setFieldValue("NOWUPDCNT", VoidCount);
    }

    public BigDecimal getManualEntryAmount() {
        return (BigDecimal) getFieldValue("ENTRYAMT");
    }

    public void setManualEntryAmount(BigDecimal entryamt) {
        setFieldValue("ENTRYAMT", entryamt);
    }

    public Integer getManualEntryCount() {
        return (Integer) getFieldValue("ENTRYCNT");
    }

    public void setManualEntryCount(Integer entrycnt) {
        setFieldValue("ENTRYCNT", entrycnt);
    }

    public BigDecimal getPriceOpenEntryAmount() {
        return (BigDecimal) getFieldValue("POENTRYAMT");
    }

    public void setPriceOpenEntryAmount(BigDecimal poentryamt) {
        setFieldValue("POENTRYAMT", poentryamt);
    }

    public Integer getPriceOpenEntryCount() {
        return (Integer) getFieldValue("POENTRYCNT");
    }

    public void setPriceOpenEntryCount(Integer poentrycnt) {
        setFieldValue("POENTRYCNT", poentrycnt);
    }

    public BigDecimal getCashReturnAmount() {
        return (BigDecimal) getFieldValue("RTNCSHAMT");
    }

    public void setCashReturnAmount(BigDecimal rtncshamt) {
        setFieldValue("RTNCSHAMT", rtncshamt);
    }

    public Integer getCashReturnCount() {
        return (Integer) getFieldValue("RTNCSHCNT");
    }

    public void setCashReturnCount(Integer rtncshcnt) {
        setFieldValue("RTNCSHCNT", rtncshcnt);
    }

    public Integer getDrawerOpenCount() {
        return (Integer) getFieldValue("DROPENCNT");
    }

    public void setDrawerOpenCount(Integer DrawerOpenCount) {
        setFieldValue("DROPENCNT", DrawerOpenCount);
    }

    public BigDecimal getReprintAmount() {
        return (BigDecimal) getFieldValue("REPRTAMT");
    }

    public void setReprintAmount(BigDecimal reprtamt) {
        setFieldValue("REPRTAMT", reprtamt);
    }

    public Integer getReprintCount() {
        return (Integer) getFieldValue("REPRTCNT");
    }

    public void setReprintCount(Integer reprintCount) {
        setFieldValue("REPRTCNT", reprintCount);
    }

    public BigDecimal getCashInAmount() {
        return (BigDecimal) getFieldValue("CASHINAMT");
    }

    public void setCashInAmount(BigDecimal cashinamt) {
        setFieldValue("CASHINAMT", cashinamt);
    }

    public Integer getCashInCount() {
        return (Integer) getFieldValue("CASHINCNT");
    }

    public void setCashInCount(Integer cashincnt) {
        setFieldValue("CASHINCNT", cashincnt);
    }

    public BigDecimal getCashOutAmount() {
        return (BigDecimal) getFieldValue("CASHOUTAMT");
    }

    public void setCashOutAmount(BigDecimal cashoutamt) {
        setFieldValue("CASHOUTAMT", cashoutamt);
    }

    public Integer getCashOutCount() {
        return (Integer) getFieldValue("CASHOUTCNT");
    }

    public void setCashOutCount(Integer cashoutcnt) {
        setFieldValue("CASHOUTCNT", cashoutcnt);
    }

    public BigDecimal getSpillAmount() {
        return (BigDecimal) getFieldValue("OVERAMT");
    }

    public void setSpillAmount(BigDecimal SpillAmount) {
        setFieldValue("OVERAMT", SpillAmount);
    }

    public Integer getSpillCount() {
        return (Integer) getFieldValue("OVERCNT");
    }

    public void setSpillCount(Integer SpillCount) {
        setFieldValue("OVERCNT", SpillCount);
    }

    public BigDecimal getExchangeRate1() {
        return (BigDecimal) getFieldValue("RATE1");
    }

    public void setExchangeRate1(BigDecimal ExchangeRate1) {
        setFieldValue("RATE1", ExchangeRate1);
    }

    public BigDecimal getExchangeRate2() {
        return (BigDecimal) getFieldValue("RATE2");
    }

    public void setExchangeRate2(BigDecimal ExchangeRate2) {
        setFieldValue("RATE2", ExchangeRate2);
    }

    public BigDecimal getExchangeRate3() {
        return (BigDecimal) getFieldValue("RATE3");
    }

    public void setExchangeRate3(BigDecimal ExchangeRate3) {
        setFieldValue("RATE3", ExchangeRate3);
    }

    public BigDecimal getExchangeRate4() {
        return (BigDecimal) getFieldValue("RATE4");
    }

    public void setExchangeRate4(BigDecimal ExchangeRate4) {
        setFieldValue("RATE4", ExchangeRate4);
    }

    public BigDecimal getPay00Amount() {
        return (BigDecimal) getFieldValue("PAY00AMT");
    }

    public void setPay00Amount(BigDecimal Pay00Amount) {
        setFieldValue("PAY00AMT", Pay00Amount);
    }

    public Integer getPay00Count() {
        return (Integer) getFieldValue("PAY00CNT");
    }

    public void setPay00Count(Integer Pay00Count) {
        setFieldValue("PAY00CNT", Pay00Count);
    }

    public BigDecimal getPay01Amount() {
        return (BigDecimal) getFieldValue("PAY01AMT");
    }

    public void setPay01Amount(BigDecimal Pay01Amount) {
        setFieldValue("PAY01AMT", Pay01Amount);
    }

    public Integer getPay01Count() {
        return (Integer) getFieldValue("PAY01CNT");
    }

    public void setPay01Count(Integer Pay01Count) {
        setFieldValue("PAY01CNT", Pay01Count);
    }

    public BigDecimal getPay02Amount() {
        return (BigDecimal) getFieldValue("PAY02AMT");
    }

    public void setPay02Amount(BigDecimal Pay02Amount) {
        setFieldValue("PAY02AMT", Pay02Amount);
    }

    public Integer getPay02Count() {
        return (Integer) getFieldValue("PAY02CNT");
    }

    public void setPay02Count(Integer Pay02Count) {
        setFieldValue("PAY02CNT", Pay02Count);
    }

    public BigDecimal getPay03Amount() {
        return (BigDecimal) getFieldValue("PAY03AMT");
    }

    public void setPay03Amount(BigDecimal Pay03Amount) {
        setFieldValue("PAY03AMT", Pay03Amount);
    }

    public Integer getPay03Count() {
        return (Integer) getFieldValue("PAY03CNT");
    }

    public void setPay03Count(Integer Pay03Count) {
        setFieldValue("PAY03CNT", Pay03Count);
    }

    public BigDecimal getPay04Amount() {
        return (BigDecimal) getFieldValue("PAY04AMT");
    }

    public void setPay04Amount(BigDecimal Pay04Amount) {
        setFieldValue("PAY04AMT", Pay04Amount);
    }

    public Integer getPay04Count() {
        return (Integer) getFieldValue("PAY04CNT");
    }

    public void setPay04Count(Integer Pay04Count) {
        setFieldValue("PAY04CNT", Pay04Count);
    }

    public BigDecimal getPay05Amount() {
        return (BigDecimal) getFieldValue("PAY05AMT");
    }

    public void setPay05Amount(BigDecimal Pay05Amount) {
        setFieldValue("PAY05AMT", Pay05Amount);
    }

    public Integer getPay05Count() {
        return (Integer) getFieldValue("PAY05CNT");
    }

    public void setPay05Count(Integer Pay05Count) {
        setFieldValue("PAY05CNT", Pay05Count);
    }

    public BigDecimal getPay06Amount() {
        return (BigDecimal) getFieldValue("PAY06AMT");
    }

    public void setPay06Amount(BigDecimal Pay06Amount) {
        setFieldValue("PAY06AMT", Pay06Amount);
    }

    public Integer getPay06Count() {
        return (Integer) getFieldValue("PAY06CNT");
    }

    public void setPay06Count(Integer Pay06Count) {
        setFieldValue("PAY06CNT", Pay06Count);
    }

    public BigDecimal getPay07Amount() {
        return (BigDecimal) getFieldValue("PAY07AMT");
    }

    public void setPay07Amount(BigDecimal Pay07Amount) {
        setFieldValue("PAY07AMT", Pay07Amount);
    }

    public Integer getPay07Count() {
        return (Integer) getFieldValue("PAY07CNT");
    }

    public void setPay07Count(Integer Pay07Count) {
        setFieldValue("PAY07CNT", Pay07Count);
    }

    public BigDecimal getPay08Amount() {
        return (BigDecimal) getFieldValue("PAY08AMT");
    }

    public void setPay08Amount(BigDecimal Pay08Amount) {
        setFieldValue("PAY08AMT", Pay08Amount);
    }

    public Integer getPay08Count() {
        return (Integer) getFieldValue("PAY08CNT");
    }

    public void setPay08Count(Integer Pay08Count) {
        setFieldValue("PAY08CNT", Pay08Count);
    }

    public BigDecimal getPay09Amount() {
        return (BigDecimal) getFieldValue("PAY09AMT");
    }

    public void setPay09Amount(BigDecimal Pay09Amount) {
        setFieldValue("PAY09AMT", Pay09Amount);
    }

    public Integer getPay09Count() {
        return (Integer) getFieldValue("PAY09CNT");
    }

    public void setPay09Count(Integer Pay09Count) {
        setFieldValue("PAY09CNT", Pay09Count);
    }

    public BigDecimal getPay10Amount() {
        return (BigDecimal) getFieldValue("PAY10AMT");
    }

    public void setPay10Amount(BigDecimal Pay10Amount) {
        setFieldValue("PAY10AMT", Pay10Amount);
    }

    public Integer getPay10Count() {
        return (Integer) getFieldValue("PAY10CNT");
    }

    public void setPay10Count(Integer Pay10Count) {
        setFieldValue("PAY10CNT", Pay10Count);
    }

    public BigDecimal getPay11Amount() {
        return (BigDecimal) getFieldValue("PAY11AMT");
    }

    public void setPay11Amount(BigDecimal Pay11Amount) {
        setFieldValue("PAY11AMT", Pay11Amount);
    }

    public Integer getPay11Count() {
        return (Integer) getFieldValue("PAY11CNT");
    }

    public void setPay11Count(Integer Pay11Count) {
        setFieldValue("PAY11CNT", Pay11Count);
    }

    public BigDecimal getPay12Amount() {
        return (BigDecimal) getFieldValue("PAY12AMT");
    }

    public void setPay12Amount(BigDecimal Pay12Amount) {
        setFieldValue("PAY12AMT", Pay12Amount);
    }

    public Integer getPay12Count() {
        return (Integer) getFieldValue("PAY12CNT");
    }

    public void setPay12Count(Integer Pay12Count) {
        setFieldValue("PAY12CNT", Pay12Count);
    }

    public BigDecimal getPay13Amount() {
        return (BigDecimal) getFieldValue("PAY13AMT");
    }

    public void setPay13Amount(BigDecimal Pay13Amount) {
        setFieldValue("PAY13AMT", Pay13Amount);
    }

    public Integer getPay13Count() {
        return (Integer) getFieldValue("PAY13CNT");
    }

    public void setPay13Count(Integer Pay13Count) {
        setFieldValue("PAY13CNT", Pay13Count);
    }

    public BigDecimal getPay14Amount() {
        return (BigDecimal) getFieldValue("PAY14AMT");
    }

    public void setPay14Amount(BigDecimal Pay14Amount) {
        setFieldValue("PAY14AMT", Pay14Amount);
    }

    public Integer getPay14Count() {
        return (Integer) getFieldValue("PAY14CNT");
    }

    public void setPay14Count(Integer Pay14Count) {
        setFieldValue("PAY14CNT", Pay14Count);
    }

    public BigDecimal getPay15Amount() {
        return (BigDecimal) getFieldValue("PAY15AMT");
    }

    public void setPay15Amount(BigDecimal Pay15Amount) {
        setFieldValue("PAY15AMT", Pay15Amount);
    }

    public Integer getPay15Count() {
        return (Integer) getFieldValue("PAY15CNT");
    }

    public void setPay15Count(Integer Pay15Count) {
        setFieldValue("PAY15CNT", Pay15Count);
    }

    public BigDecimal getPay16Amount() {
        return (BigDecimal) getFieldValue("PAY16AMT");
    }

    public void setPay16Amount(BigDecimal Pay16Amount) {
        setFieldValue("PAY16AMT", Pay16Amount);
    }

    public Integer getPay16Count() {
        return (Integer) getFieldValue("PAY16CNT");
    }

    public void setPay16Count(Integer Pay16Count) {
        setFieldValue("PAY16CNT", Pay16Count);
    }

    public BigDecimal getPay17Amount() {
        return (BigDecimal) getFieldValue("PAY17AMT");
    }

    public void setPay17Amount(BigDecimal Pay17Amount) {
        setFieldValue("PAY17AMT", Pay17Amount);
    }

    public Integer getPay17Count() {
        return (Integer) getFieldValue("PAY17CNT");
    }

    public void setPay17Count(Integer Pay17Count) {
        setFieldValue("PAY17CNT", Pay17Count);
    }

    public BigDecimal getPay18Amount() {
        return (BigDecimal) getFieldValue("PAY18AMT");
    }

    public void setPay18Amount(BigDecimal Pay18Amount) {
        setFieldValue("PAY18AMT", Pay18Amount);
    }

    public Integer getPay18Count() {
        return (Integer) getFieldValue("PAY18CNT");
    }

    public void setPay18Count(Integer Pay18Count) {
        setFieldValue("PAY18CNT", Pay18Count);
    }

    public BigDecimal getPay19Amount() {
        return (BigDecimal) getFieldValue("PAY19AMT");
    }

    public void setPay19Amount(BigDecimal Pay19Amount) {
        setFieldValue("PAY19AMT", Pay19Amount);
    }

    public Integer getPay19Count() {
        return (Integer) getFieldValue("PAY19CNT");
    }

    public void setPay19Count(Integer Pay19Count) {
        setFieldValue("PAY19CNT", Pay19Count);
    }

    public BigDecimal getPay20Amount() {
        return (BigDecimal) getFieldValue("PAY20AMT");
    }

    public void setPay20Amount(BigDecimal Pay20Amount) {
        setFieldValue("PAY20AMT", Pay20Amount);
    }

    public Integer getPay20Count() {
        return (Integer) getFieldValue("PAY20CNT");
    }

    public void setPay20Count(Integer Pay20Count) {
        setFieldValue("PAY20CNT", Pay20Count);
    }

    public BigDecimal getPay21Amount() {
        return (BigDecimal) getFieldValue("PAY21AMT");
    }

    public void setPay21Amount(BigDecimal Pay21Amount) {
        setFieldValue("PAY21AMT", Pay21Amount);
    }

    public Integer getPay21Count() {
        return (Integer) getFieldValue("PAY21CNT");
    }

    public void setPay21Count(Integer Pay21Count) {
        setFieldValue("PAY21CNT", Pay21Count);
    }

    public BigDecimal getPay22Amount() {
        return (BigDecimal) getFieldValue("PAY22AMT");
    }

    public void setPay22Amount(BigDecimal Pay22Amount) {
        setFieldValue("PAY22AMT", Pay22Amount);
    }

    public Integer getPay22Count() {
        return (Integer) getFieldValue("PAY22CNT");
    }

    public void setPay22Count(Integer Pay22Count) {
        setFieldValue("PAY22CNT", Pay22Count);
    }

    public BigDecimal getPay23Amount() {
        return (BigDecimal) getFieldValue("PAY23AMT");
    }

    public void setPay23Amount(BigDecimal Pay23Amount) {
        setFieldValue("PAY23AMT", Pay23Amount);
    }

    public Integer getPay23Count() {
        return (Integer) getFieldValue("PAY23CNT");
    }

    public void setPay23Count(Integer Pay23Count) {
        setFieldValue("PAY23CNT", Pay23Count);
    }

    public BigDecimal getPay24Amount() {
        return (BigDecimal) getFieldValue("PAY24AMT");
    }

    public void setPay24Amount(BigDecimal Pay24Amount) {
        setFieldValue("PAY24AMT", Pay24Amount);
    }

    public Integer getPay24Count() {
        return (Integer) getFieldValue("PAY24CNT");
    }

    public void setPay24Count(Integer Pay24Count) {
        setFieldValue("PAY24CNT", Pay24Count);
    }

    public BigDecimal getPay25Amount() {
        return (BigDecimal) getFieldValue("PAY25AMT");
    }

    public void setPay25Amount(BigDecimal Pay25Amount) {
        setFieldValue("PAY25AMT", Pay25Amount);
    }

    public Integer getPay25Count() {
        return (Integer) getFieldValue("PAY25CNT");
    }

    public void setPay25Count(Integer Pay25Count) {
        setFieldValue("PAY25CNT", Pay25Count);
    }

    public BigDecimal getPay26Amount() {
        return (BigDecimal) getFieldValue("PAY26AMT");
    }

    public void setPay26Amount(BigDecimal Pay26Amount) {
        setFieldValue("PAY26AMT", Pay26Amount);
    }

    public Integer getPay26Count() {
        return (Integer) getFieldValue("PAY26CNT");
    }

    public void setPay26Count(Integer Pay26Count) {
        setFieldValue("PAY26CNT", Pay26Count);
    }

    public BigDecimal getPay27Amount() {
        return (BigDecimal) getFieldValue("PAY27AMT");
    }

    public void setPay27Amount(BigDecimal Pay27Amount) {
        setFieldValue("PAY27AMT", Pay27Amount);
    }

    public Integer getPay27Count() {
        return (Integer) getFieldValue("PAY27CNT");
    }

    public void setPay27Count(Integer Pay27Count) {
        setFieldValue("PAY27CNT", Pay27Count);
    }

    public BigDecimal getPay28Amount() {
        return (BigDecimal) getFieldValue("PAY28AMT");
    }

    public void setPay28Amount(BigDecimal Pay28Amount) {
        setFieldValue("PAY28AMT", Pay28Amount);
    }

    public Integer getPay28Count() {
        return (Integer) getFieldValue("PAY28CNT");
    }

    public void setPay28Count(Integer Pay28Count) {
        setFieldValue("PAY28CNT", Pay28Count);
    }

    public BigDecimal getPay29Amount() {
        return (BigDecimal) getFieldValue("PAY29AMT");
    }

    public void setPay29Amount(BigDecimal Pay29Amount) {
        setFieldValue("PAY29AMT", Pay29Amount);
    }

    public Integer getPay29Count() {
        return (Integer) getFieldValue("PAY29CNT");
    }

    public void setPay29Count(Integer Pay29Count) {
        setFieldValue("PAY29CNT", Pay29Count);
    }

    //==============gllg=======================
    public BigDecimal getItemDiscAmount0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount0");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT0");
    }

    public void setItemDiscAmount0(BigDecimal itemDiscAMT0) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount0", itemDiscAMT0);
        else
            setFieldValue("ITEMDISCAMT0", itemDiscAMT0);
    }
    
    public BigDecimal getItemDiscAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount1");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT1");
    }

    public void setItemDiscAmount1(BigDecimal itemDiscAMT0) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount1", itemDiscAMT0);
        else
            setFieldValue("ITEMDISCAMT1", itemDiscAMT0);
    }
    
    public BigDecimal getItemDiscAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount2");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT2");
    }

    public void setItemDiscAmount2(BigDecimal itemDiscAMT2) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount2", itemDiscAMT2);
        else
            setFieldValue("ITEMDISCAMT2", itemDiscAMT2);
    }
    
    public BigDecimal getItemDiscAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount3");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT3");
    }

    public void setItemDiscAmount3(BigDecimal itemDiscAMT3) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount3", itemDiscAMT3);
        else
            setFieldValue("ITEMDISCAMT3", itemDiscAMT3);
    }
    
    public BigDecimal getItemDiscAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount4");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT4");
    }

    public void setItemDiscAmount4(BigDecimal itemDiscAMT4) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount4", itemDiscAMT4);
        else
            setFieldValue("ITEMDISCAMT4", itemDiscAMT4);
    }

    public BigDecimal getItemDiscAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT");
    }

    public void setItemDiscAmount(BigDecimal itemDiscAMT) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount", itemDiscAMT);
        else
            setFieldValue("ITEMDISCAMT", itemDiscAMT);
    }
    
    public Integer getItemDiscCnt() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount");
        else
            return (Integer) getFieldValue("ITEMDISCCNT");
    }
    
    public void setItemDiscCnt(int itemDiscCnt) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT", new Integer(itemDiscCnt));
        else
            setFieldValue("itemDiscCount", new Integer(itemDiscCnt));
    }

    
    public Integer getItemDiscCnt0() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount0");
        else
            return (Integer) getFieldValue("ITEMDISCCNT0");
    }
    
    public void setItemDiscCnt0(int itemDiscCnt0) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT0", new Integer(itemDiscCnt0));
        else
            setFieldValue("itemDiscCount0", new Integer(itemDiscCnt0));
    }
    
    public Integer getItemDiscCnt1() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount1");
        else
            return (Integer) getFieldValue("ITEMDISCCNT1");
    }
    
    public void setItemDiscCnt1(int itemDiscCnt1) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT1", new Integer(itemDiscCnt1));
        else
            setFieldValue("itemDiscCount1", new Integer(itemDiscCnt1));
    }
    
    public Integer getItemDiscCnt2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount2");
        else
            return (Integer) getFieldValue("ITEMDISCCNT2");
    }
    
    public void setItemDiscCnt2(int itemDiscCnt2) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT2", new Integer(itemDiscCnt2));
        else
            setFieldValue("itemDiscCount2", new Integer(itemDiscCnt2));
    }
    
    public Integer getItemDiscCnt3() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount3");
        else
            return (Integer) getFieldValue("ITEMDISCCNT3");
    }
    
    public void setItemDiscCnt3(int itemDiscCnt3) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT3", new Integer(itemDiscCnt3));
        else
            setFieldValue("itemDiscCount3", new Integer(itemDiscCnt3));
    }
    
    public Integer getItemDiscCnt4() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount4");
        else
            return (Integer) getFieldValue("ITEMDISCCNT4");
    }
    
    public void setItemDiscCnt4(int itemDiscCnt4) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT4", new Integer(itemDiscCnt4));
        else
            setFieldValue("itemDiscCount4", new Integer(itemDiscCnt4));
    }

    //==============gllg=======================

    public BigDecimal getPayAmountByID(String id) {
        String innerID = null;
        if (id.length() == 1 && Integer.parseInt(id) < 10)
            innerID = "0" + id;
        else
            innerID = id;
        return (BigDecimal) getFieldValue("PAY" + innerID + "AMT");
    }

    public void setPayAmountByID(String id, BigDecimal payment) {
        String innerID = null;
        if (id.length() == 1 && Integer.parseInt(id) < 10)
            innerID = "0" + id;
        else
            innerID = id;
        setFieldValue("PAY" + innerID + "AMT", payment);
    }

    public Integer getPayCountByID(String id) {
        String innerID = null;
        if (id.length() == 1 && Integer.parseInt(id) < 10)
            innerID = "0" + id;
        else
            innerID = id;
        return (Integer) getFieldValue("PAY" + innerID + "CNT");
    }

    public void setPayCountByID(String id, Integer paycount) {
        String innerID = null;
        if (id.length() == 1 && Integer.parseInt(id) < 10)
            innerID = "0" + id;
        else
            innerID = id;
        setFieldValue("PAY" + innerID + "CNT", paycount);
    }

    public String getInsertUpdateTableName() {
        if (Server.serverExist())
            return "posul_shift";
        else
            return "shift";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (Server.serverExist())
            return "posul_shift";
        else
            return "shift";
    }

    private static String[][] posToScFieldNameArray;

    public static String[][] getPosToScFieldNameArray() {
        if (posToScFieldNameArray == null)
            posToScFieldNameArray = new String[][] {
                new String[] { "STORENO", "StoreID" },
                new String[] { "POSNO", "PosNumber" },
                new String[] { "EODCNT", "ZSequenceNumber" },
                new String[] { "SHIFTCNT", "ShiftSequenceNumber" },
                new String[] { "ACCDATE", "AccountDate" },
                new String[] { "SGNONSEQ", "SignOnTransactionNumber" },
                new String[] { "SGNOFFSEQ", "SignOffTransactionNumber" },
                new String[] { "SGNONINV", "SignOnInvoiceNumber" },
                new String[] { "SGNOFFINV", "SignOffInvoiceNumber" },
                new String[] { "SGNONDTTM", "SignOnSystemDateTime" },
                new String[] { "SGNOFDTTM", "SignOffSystemDateTime" },
                new String[] { "MACHINENO", "MachineNumber" },
                new String[] { "CASHIER", "CashierID" },
                new String[] { "TCPFLG", "UploadState" },
                new String[] { "TRANCNT", "TransactionCount" },
                new String[] { "CUSTCNT", "CustomerCount" },
                new String[] { "ITEMSALEQTY", "ItemSaleQuantity" },
                new String[] { "GrossSales", "GrossSaleTotalAmount" },
                new String[] { "GROSALTX0AM", "GrossSaleTax0Amount" },
                new String[] { "GROSALTX1AM", "GrossSaleTax1Amount" },
                new String[] { "GROSALTX2AM", "GrossSaleTax2Amount" },
                new String[] { "GROSALTX3AM", "GrossSaleTax3Amount" },
                new String[] { "GROSALTX4AM", "GrossSaleTax4Amount" },
                new String[] { "SIPercentPlusTotalAmount", "SIPlusTotalAmount" },
                new String[] { "SIPLUSAMT0", "SIPlusTax0Amount" },
                new String[] { "SIPLUSAMT1", "SIPlusTax1Amount" },
                new String[] { "SIPLUSAMT2", "SIPlusTax2Amount" },
                new String[] { "SIPLUSAMT3", "SIPlusTax3Amount" },
                new String[] { "SIPLUSAMT4", "SIPlusTax4Amount" },
                new String[] { "SIPercentPlusTotalCount", "SIPlusTotalCount" },
                new String[] { "SIPLUSCNT0", "SIPlusTax0Count" },
                new String[] { "SIPLUSCNT1", "SIPlusTax1Count" },
                new String[] { "SIPLUSCNT2", "SIPlusTax2Count" },
                new String[] { "SIPLUSCNT3", "SIPlusTax3Count" },
                new String[] { "SIPLUSCNT4", "SIPlusTax4Count" },
                new String[] { "SIPercentOffTotalAmount", "DiscountTotalAmount" },
                new String[] { "SIPAMT0", "DiscountTax0Amount" },
                new String[] { "SIPAMT1", "DiscountTax1Amount" },
                new String[] { "SIPAMT2", "DiscountTax2Amount" },
                new String[] { "SIPAMT3", "DiscountTax3Amount" },
                new String[] { "SIPAMT4", "DiscountTax4Amount" },
                new String[] { "SIPercentOffTotalCount", "DiscountTotalCount" },
                new String[] { "SIPCNT0", "DiscountTax0Count" },
                new String[] { "SIPCNT1", "DiscountTax1Count" },
                new String[] { "SIPCNT2", "DiscountTax2Count" },
                new String[] { "SIPCNT3", "DiscountTax3Count" },
                new String[] { "SIPCNT4", "DiscountTax4Count" },
                new String[] { "MixAndMatchTotalAmount",
                        "MixAndMatchTotalAmount" },
                new String[] { "MNMAMT0", "MixAndMatchTax0Amount" },
                new String[] { "MNMAMT1", "MixAndMatchTax1Amount" },
                new String[] { "MNMAMT2", "MixAndMatchTax2Amount" },
                new String[] { "MNMAMT3", "MixAndMatchTax3Amount" },
                new String[] { "MNMAMT4", "MixAndMatchTax4Amount" },
                new String[] { "MixAndMatchTotalCount", "MixAndMatchTotalCount" },
                new String[] { "MNMCNT0", "MixAndMatchTax0Count" },
                new String[] { "MNMCNT1", "MixAndMatchTax1Count" },
                new String[] { "MNMCNT2", "MixAndMatchTax2Count" },
                new String[] { "MNMCNT3", "MixAndMatchTax3Count" },
                new String[] { "MNMCNT4", "MixAndMatchTax4Count" },
                new String[] { "NotIncludedTotalSales", "NotIncludedTotalSale" },
                new String[] { "RCPGIFAMT0", "NotIncludedTax0Sale" },
                new String[] { "RCPGIFAMT1", "NotIncludedTax1Sale" },
                new String[] { "RCPGIFAMT2", "NotIncludedTax2Sale" },
                new String[] { "RCPGIFAMT3", "NotIncludedTax3Sale" },
                new String[] { "RCPGIFAMT4", "NotIncludedTax4Sale" },
                new String[] { "NetSalesTotalAmount", "NetSaleTotalAmount" },
                new String[] { "NETSALAMT0", "NetSaleTax0Amount" },
                new String[] { "NETSALAMT1", "NetSaleTax1Amount" },
                new String[] { "NETSALAMT2", "NetSaleTax2Amount" },
                new String[] { "NETSALAMT3", "NetSaleTax3Amount" },
                new String[] { "NETSALAMT4", "NetSaleTax4Amount" },
                new String[] { "TAXAMT0", "TaxAmount0" },
                new String[] { "TAXAMT1", "TaxAmount1" },
                new String[] { "TAXAMT2", "TaxAmount2" },
                new String[] { "TAXAMT3", "TaxAmount3" },
                new String[] { "TAXAMT4", "TaxAmount4" },
                new String[] { "TotalTaxAmount", "taxTotalAmount" },
                new String[] { "VALPAMT", "TotalVoucherAmount" },
                new String[] { "VALPCNT", "TotalVoucherCount" },
                new String[] { "PRCVAMT", "PreRcvAmount" },
                new String[] { "PRCVCNT", "PreRcvCount" },
                new String[] { "PRCVPLUAMT1", "PreRcvDetailAmount1" },
                new String[] { "PRCVPLUCNT1", "PreRcvDetailCount1" },
                new String[] { "PRCVPLUAMT2", "PreRcvDetailAmount2" },
                new String[] { "PRCVPLUCNT2", "PreRcvDetailCount2" },
                new String[] { "PRCVPLUAMT3", "PreRcvDetailAmount3" },
                new String[] { "PRCVPLUCNT3", "PreRcvDetailCount3" },
                new String[] { "PRCVPLUAMT4", "PreRcvDetailAmount4" },
                new String[] { "PRCVPLUCNT4", "PreRcvDetailCount4" },
                new String[] { "PRCVPLUAMT5", "PreRcvDetailAmount5" },
                new String[] { "PRCVPLUCNT5", "PreRcvDetailCount5" },
                new String[] { "DAISHOUAMT", "TotalDaiShouAmount" },
                new String[] { "DAISHOUCNT", "TotalDaiShouCount" },
                new String[] { "DAISHOUPLUAMT1", "DaiShouDetailAmount1" },
                new String[] { "DAISHOUPLUCNT1", "DaiShouDetailCount1" },
                new String[] { "DAISHOUPLUAMT2", "DaiShouDetailAmount2" },
                new String[] { "DAISHOUPLUCNT2", "DaiShouDetailCount2" },
                new String[] { "DAISHOUPLUAMT3", "DaiShouDetailAmount3" },
                new String[] { "DAISHOUPLUCNT3", "DaiShouDetailCount3" },
                new String[] { "DAISHOUPLUAMT4", "DaiShouDetailAmount4" },
                new String[] { "DAISHOUPLUCNT4", "DaiShouDetailCount4" },
                new String[] { "DAISHOUPLUAMT5", "DaiShouDetailAmount5" },
                new String[] { "DAISHOUPLUCNT5", "DaiShouDetailCount5" },
                new String[] { "DAISHOUAMT2", "TotalDaiShouAmount2" },
                new String[] { "DAISHOUCNT2", "TotalDaiShouCount2" },
                new String[] { "DAIFUAMT", "TotalDaiFuAmount" },
                new String[] { "DAIFUCNT", "TotalDaiFuCount" },
                new String[] { "DAIFUPLUAMT1", "DaiFuDetailAmount1" },
                new String[] { "DAIFUPLUCNT1", "DaiFuDetailCount1" },
                new String[] { "DAIFUPLUAMT2", "DaiFuDetailAmount2" },
                new String[] { "DAIFUPLUCNT2", "DaiFuDetailCount2" },
                new String[] { "DAIFUPLUAMT3", "DaiFuDetailAmount3" },
                new String[] { "DAIFUPLUCNT3", "DaiFuDetailCount3" },
                new String[] { "DAIFUPLUAMT4", "DaiFuDetailAmount4" },
                new String[] { "DAIFUPLUCNT4", "DaiFuDetailCount4" },
                new String[] { "DAIFUPLUAMT5", "DaiFuDetailAmount5" },
                new String[] { "DAIFUPLUCNT5", "DaiFuDetailCount5" },
                new String[] { "PINAMT", "TotalPaidinAmount" },
                new String[] { "PINCNT", "TotalPaidinCount" },
                new String[] { "PINPLUAMT1", "PaidInDetailAmount1" },
                new String[] { "PINPLUCNT1", "PaidInDetailCount1" },
                new String[] { "PINPLUAMT2", "PaidInDetailAmount2" },
                new String[] { "PINPLUCNT2", "PaidInDetailCount2" },
                new String[] { "PINPLUAMT3", "PaidInDetailAmount3" },
                new String[] { "PINPLUCNT3", "PaidInDetailCount3" },
                new String[] { "PINPLUAMT4", "PaidInDetailAmount4" },
                new String[] { "PINPLUCNT4", "PaidInDetailCount4" },
                new String[] { "PINPLUAMT5", "PaidInDetailAmount5" },
                new String[] { "PINPLUCNT5", "PaidInDetailCount5" },
                new String[] { "POUTAMT", "TotalPaidoutAmount" },
                new String[] { "POUTCNT", "TotalPaidoutCount" },
                new String[] { "POUTPLUAMT1", "PaidOutDetailAmount1" },
                new String[] { "POUTPLUCNT1", "PaidOutDetailCount1" },
                new String[] { "POUTPLUAMT2", "PaidOutDetailAmount2" },
                new String[] { "POUTPLUCNT2", "PaidOutDetailCount2" },
                new String[] { "POUTPLUAMT3", "PaidOutDetailAmount3" },
                new String[] { "POUTPLUCNT3", "PaidOutDetailCount3" },
                new String[] { "POUTPLUAMT4", "PaidOutDetailAmount4" },
                new String[] { "POUTPLUCNT4", "PaidOutDetailCount4" },
                new String[] { "POUTPLUAMT5", "PaidOutDetailAmount5" },
                new String[] { "POUTPLUCNT5", "PaidOutDetailCount5" },
                new String[] { "EXGAMT", "exchangeItemAmount" },
                new String[] { "EXGITEMQTY", "exchangeItemQuantity" },
                new String[] { "EXGTRANCNT", "exchangeTransactionCount" },
                new String[] { "RTNAMT", "returnItemAmount" },
                new String[] { "RTNITEMQTY", "returnItemQuantity" },
                new String[] { "RTNTRANCNT", "returnTransactionCount" },
                new String[] { "VOIDINVAMT", "TransactionVoidAmount" },
                new String[] { "VOIDINVCNT", "TransactionVoidCount" },
                new String[] { "CANCELAMT", "TransactionCancelAmount" },
                new String[] { "CANCELCNT", "TransactionCancelCount" },
                new String[] { "UPDAMT", "LineVoidAmount" },
                new String[] { "UPDCNT", "LineVoidCount" },
                new String[] { "NOWUPDAMT", "VoidAmount" },
                new String[] { "NOWUPDCNT", "VoidCount" },
                new String[] { "DROPENCNT", "DrawerOpenCount" },
                new String[] { "RTNCSHCNT", "ReprintCount" },
                new String[] { "RTNCSHAMT", "ReprintAmount" },
                new String[] { "ENTRYAMT", "ManualEntryAmount" },
                new String[] { "ENTRYCNT", "ManualEntryCount" },
                new String[] { "POENTRYAMT", "PriceOpenEntryAmount" },
                new String[] { "POENTRYCNT", "PriceOpenEntryCount" },
                new String[] { "REPRTAMT", "cashReturnAmount" },
                new String[] { "REPRTCNT", "cashReturnCount" },
                new String[] { "CASHINAMT", "CashInAmount" },
                new String[] { "CASHINCNT", "CashInCount" },
                new String[] { "CASHOUTAMT", "CashOutAmount" },
                new String[] { "CASHOUTCNT", "CashOutCount" },
                new String[] { "OVERAMT", "SpillAmount" },
                new String[] { "OVERCNT", "SpillCount" },
                new String[] { "RATE1", "ExchangeRate1" },
                new String[] { "RATE2", "ExchangeRate2" },
                new String[] { "RATE3", "ExchangeRate3" },
                new String[] { "RATE4", "ExchangeRate4" },
                new String[] { "PAY00AMT", "Pay00Amount" },
                new String[] { "PAY00CNT", "Pay00Count" },
                new String[] { "PAY01AMT", "Pay01Amount" },
                new String[] { "PAY01CNT", "Pay01Count" },
                new String[] { "PAY02AMT", "Pay02Amount" },
                new String[] { "PAY02CNT", "Pay02Count" },
                new String[] { "PAY03AMT", "Pay03Amount" },
                new String[] { "PAY03CNT", "Pay03Count" },
                new String[] { "PAY04AMT", "Pay04Amount" },
                new String[] { "PAY04CNT", "Pay04Count" },
                new String[] { "PAY05AMT", "Pay05Amount" },
                new String[] { "PAY05CNT", "Pay05Count" },
                new String[] { "PAY06AMT", "Pay06Amount" },
                new String[] { "PAY06CNT", "Pay06Count" },
                new String[] { "PAY07AMT", "Pay07Amount" },
                new String[] { "PAY07CNT", "Pay07Count" },
                new String[] { "PAY08AMT", "Pay08Amount" },
                new String[] { "PAY08CNT", "Pay08Count" },
                new String[] { "PAY09AMT", "Pay09Amount" },
                new String[] { "PAY09CNT", "Pay09Count" },
                new String[] { "PAY10AMT", "Pay10Amount" },
                new String[] { "PAY10CNT", "Pay10Count" },
                new String[] { "PAY11AMT", "Pay11Amount" },
                new String[] { "PAY11CNT", "Pay11Count" },
                new String[] { "PAY12AMT", "Pay12Amount" },
                new String[] { "PAY12CNT", "Pay12Count" },
                new String[] { "PAY13AMT", "Pay13Amount" },
                new String[] { "PAY13CNT", "Pay13Count" },
                new String[] { "PAY14AMT", "Pay14Amount" },
                new String[] { "PAY14CNT", "Pay14Count" },
                new String[] { "PAY15AMT", "Pay15Amount" },
                new String[] { "PAY15CNT", "Pay15Count" },
                new String[] { "PAY16AMT", "Pay16Amount" },
                new String[] { "PAY16CNT", "Pay16Count" },
                new String[] { "PAY17AMT", "Pay17Amount" },
                new String[] { "PAY17CNT", "Pay17Count" },
                new String[] { "PAY18AMT", "Pay18Amount" },
                new String[] { "PAY18CNT", "Pay18Count" },
                new String[] { "PAY19AMT", "Pay19Amount" },
                new String[] { "PAY19CNT", "Pay19Count" },
                new String[] { "PAY20AMT", "Pay20Amount" },
                new String[] { "PAY20CNT", "Pay20Count" },
                new String[] { "PAY21AMT", "Pay21Amount" },
                new String[] { "PAY21CNT", "Pay21Count" },
                new String[] { "PAY22AMT", "Pay22Amount" },
                new String[] { "PAY22CNT", "Pay22Count" },
                new String[] { "PAY23AMT", "Pay23Amount" },
                new String[] { "PAY23CNT", "Pay23Count" },
                new String[] { "PAY24AMT", "Pay24Amount" },
                new String[] { "PAY24CNT", "Pay24Count" },
                new String[] { "PAY25AMT", "Pay25Amount" },
                new String[] { "PAY25CNT", "Pay25Count" },
                new String[] { "PAY26AMT", "Pay26Amount" },
                new String[] { "PAY26CNT", "Pay26Count" },
                new String[] { "PAY27AMT", "Pay27Amount" },
                new String[] { "PAY27CNT", "Pay27Count" },
                new String[] { "PAY28AMT", "Pay28Amount" },
                new String[] { "PAY28CNT", "Pay28Count" },
                new String[] { "PAY29AMT", "Pay29Amount" },
                new String[] { "PAY29CNT", "Pay29Count" },
                new String[] { "CHANGAMT", "ExchangeDifference" },
                new String[] { "SI00AMT", "SI00Amount" },
                new String[] { "SI00CNT", "SI00Count" },
                new String[] { "SI01AMT", "SI01Amount" },
                new String[] { "SI01CNT", "SI01Count" },
                new String[] { "SI02AMT", "SI02Amount" },
                new String[] { "SI02CNT", "SI02Count" },
                new String[] { "SI03AMT", "SI03Amount" },
                new String[] { "SI03CNT", "SI03Count" },
                new String[] { "SI04AMT", "SI04Amount" },
                new String[] { "SI04CNT", "SI04Count" },
                new String[] { "SI05AMT", "SI05Amount" },
                new String[] { "SI05CNT", "SI05Count" },
                new String[] { "SI06AMT", "SI06Amount" },
                new String[] { "SI06CNT", "SI06Count" },
                new String[] { "SI07AMT", "SI07Amount" },
                new String[] { "SI07CNT", "SI07Count" },
                new String[] { "SI08AMT", "SI08Amount" },
                new String[] { "SI08CNT", "SI08Count" },
                new String[] { "SI09AMT", "SI09Amount" },
                new String[] { "SI09CNT", "SI09Count" },
                new String[] { "SI10AMT", "SI10Amount" },
                new String[] { "SI10CNT", "SI10Count" },
                new String[] { "SI11AMT", "SI11Amount" },
                new String[] { "SI11CNT", "SI11Count" },
                new String[] { "SI12AMT", "SI12Amount" },
                new String[] { "SI12CNT", "SI12Count" },
                new String[] { "SI13AMT", "SI13Amount" },
                new String[] { "SI13CNT", "SI13Count" },
                new String[] { "SI14AMT", "SI14Amount" },
                new String[] { "SI14CNT", "SI14Count" },
                new String[] { "SI15AMT", "SI15Amount" },
                new String[] { "SI15CNT", "SI15Count" },
                new String[] { "SI16AMT", "SI16Amount" },
                new String[] { "SI16CNT", "SI16Count" },
                new String[] { "SI17AMT", "SI17Amount" },
                new String[] { "SI17CNT", "SI17Count" },
                new String[] { "SI18AMT", "SI18Amount" },
                new String[] { "SI18CNT", "SI18Count" },
                new String[] { "SI19AMT", "SI19Amount" },
                new String[] { "SI19CNT", "SI19Count" },
                // gllg                
                new String[] { "ITEMDISCAMT", "itemDiscAmount" } ,
                new String[] { "ITEMDISCAMT0", "itemDiscAmount0" } ,
                new String[] { "ITEMDISCAMT1", "itemDiscAmount1" } ,
                new String[] { "ITEMDISCAMT2", "itemDiscAmount2" } ,
                new String[] { "ITEMDISCAMT3", "itemDiscAmount3" } ,
                new String[] { "ITEMDISCAMT4", "itemDiscAmount4" } ,
                new String[] { "ITEMDISCCNT", "itemDiscCount" } ,
                new String[] { "ITEMDISCCNT0", "itemDiscCount0" } ,
                new String[] { "ITEMDISCCNT1", "itemDiscCount1" } ,
                new String[] { "ITEMDISCCNT2", "itemDiscCount2" } ,
                new String[] { "ITEMDISCCNT3", "itemDiscCount3" } ,
                new String[] { "ITEMDISCCNT4", "itemDiscCount4" } 
        };
        return posToScFieldNameArray;
    }

    public ShiftReport cloneForSC() {
        String fieldNameMap[][] = getPosToScFieldNameArray();
        try {
            ShiftReport clonedShift = new ShiftReport(0);
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = getFieldValue(fieldNameMap[i][0]);
                if (value == null)
                    try {
                        value = getClass().getDeclaredMethod(
                                "get" + fieldNameMap[i][0], new Class[0])
                                .invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                clonedShift.setFieldValue(fieldNameMap[i][1], value);
            }

            return clonedShift;
        } catch (InstantiationException e) {
            return null;
        }
    }
}
