package hyi.cream.dac;

import java.util.*;

/**
 * Store DAC.
 * 
 * @author Bruce
 */
public class Store extends DacBase {
    transient public static final String VERSION = "1.0";
    transient static private Store store;
    private static ArrayList primaryKeys = new ArrayList();
    static {
        primaryKeys.add("storeID");
        createCache();
    }

    /**
     * Constructor
     */
    public Store() throws InstantiationException {
    }

    /**
     * @see hyi.cream.dac.DacBase#getPrimaryKeyList()
     */
    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public static void createCache() {
        store = (Store)
            getSingleObject(
                Store.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion());
    }

    public String getInsertUpdateTableName() {
        return "store";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        return "store";
    }

    public static String getStoreID() {
        if (store != null)
            return (String)store.getFieldValue("storeID");
        else
            return "";
    }

    public static String getStoreChineseName() {
        if (store != null)
            return (String)store.getFieldValue("storeChineseName");
        else
            return "";
    }

   public static String getStoreEnglishName() {
        if (store != null)
            return (String)store.getFieldValue("storeEnglishName");
        else
            return "";
    }

   public static String getAreaNumber() {
        if (store != null)
            return (String)store.getFieldValue("areaNumber");
        else
            return "";
    }
   public static String getTelephoneNumber() {
        if (store != null)
            return (String)store.getFieldValue("telephoneNumber");
        else
            return "";
    }

   public static String getFaxNumber() {
        if (store != null)
            return (String)store.getFieldValue("faxNumber");
        else
            return "";
    }
   public static String getZip() {
        if (store != null)
            return (String)store.getFieldValue("zip");
        else
            return "";
    }

   public static String getAddress() {
        if (store != null)
            return (String)store.getFieldValue("address");
        else
            return "";
    }
	
	/**
	 * Meyer/2003-02-20
	 * return fieldName map of PosToSc as Map 
	 */
	public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("storeID", "storeID");
        fieldNameMap.put("storeChineseName", "storeChineseName");
        fieldNameMap.put("storeEnglishName", "storeEnglishName");
        fieldNameMap.put("areaNumber", "areaNumber");
        fieldNameMap.put("telephoneNumber", "telephoneNumber");
        fieldNameMap.put("faxNumber", "faxNumber");
        fieldNameMap.put("zip", "zip");
        fieldNameMap.put("address", "address");
        return fieldNameMap;	
	}
	
    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        return DacBase.getMultipleObjects(hyi.cream.dac.Store.class,
            "SELECT * FROM store", getScToPosFieldNameMap());
    }
}
