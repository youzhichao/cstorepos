package hyi.cream.dac;

import java.math.*;
import java.util.*;
import java.io.*;

/**
 * TaxType definition class.
 *
 * @author Dai, Bruce
 * @version 1.6
 */
public class TaxType extends DacBase implements Serializable {//Tax type definition
                                      //Application-level read-only
                                      //Support cache mechanism
    /**
     * /Bruce/1.6/2002-05-08/
     *    修改后台小数位数字段名为 "numberCount". 解决POS端小数位数字段值不正确的问题.
     *
     * /Bruce/1.5/2002-03-10/
     *    Add/Modify some methods for preparing to use in new inline:
     *    Add: public static Collection getAllObjectsForPOS()
     */
    transient public static final String VERSION = "1.6";

    static final String tableName = "taxtype";
    private static ArrayList primaryKeys = new ArrayList();
    transient private static Set cache;

    public List getPrimaryKeyList() {
        return  primaryKeys;
    }

    static {
        primaryKeys.add("TAXID");
        createCache();
    }

    public static void createCache() {
        cache = new HashSet();
        Iterator itr = getMultipleObjects(TaxType.class, "SELECT * FROM " + tableName);
        if (itr != null) {
            while (itr.hasNext()) {
                cache.add(itr.next());
            }
        }
    }

    public boolean equals(Object obj) {
        if ( !(obj instanceof TaxType))
            return false;
        return getTaxID().equals(((TaxType)obj).getTaxID());
    }

    public static TaxType queryByTaxID (String taxID) {
        //System.out.println("query tax id = " + taxID);
        if (cache == null) {
            //System.out.println("cache = null");
            return null;
        }
        Iterator itr = cache.iterator();
        while (itr.hasNext()) {
            TaxType t = (TaxType)itr.next();
            //System.out.println("tax id = " + t.getTaxID());
            if (t.getTaxID().equals(taxID)){
                //System.out.println("return " + t);
                return t;
            }
        }
        //System.out.println("return null");
        return null;
        /*return (TaxType)getSingleObject(TaxType.class,
            "SELECT * FROM " + tableName + " WHERE TAXID='" + taxID + "'");//*/
    }

    public static boolean getTaxedType(String id) {
        TaxType t = TaxType.queryByTaxID(id);
        if (t != null) {
           if (!t.getType().equals("0"))
              return true;
        }
        return false;
    }

    public TaxType() throws InstantiationException {
    }

    //Get properties values:
    //TaxID	TAXID	CHAR(1)	N
    public String getTaxID() {
        return (String)getFieldValue("TAXID");
    }
    //Type	TYPE	CHAR(1)	N
    public String getType() {
        return (String)getFieldValue("TYPE");
    }

    //TAXNM
    public String getTaxName() {
         return (String)getFieldValue("TAXNM");
    }

    public void setTaxName(String name) {
         setFieldValue("TAXNM", name);
    }


    //Round	ROUND	CHAR(1)	N
    public String getRound() {
        return (String)getFieldValue("ROUND");
    }
    //Percent	PERCENT	DECIMAL(5,2)	N
    public BigDecimal getPercent() {
        return (BigDecimal)getFieldValue("PERCENT");
    }

    public Integer getDecimalDigit() {
        return (Integer)getFieldValue("DECIMALCNT");
    }

    public String getInsertUpdateTableName() {
        return tableName;
    }

    public static Iterator queryByTaxType(String taxType) {
        return getMultipleObjects(TaxType.class,
            "SELECT * FROM TAXTYPE WHERE TYPE='" + taxType + "'");
    }

    /**
     * Get all data for downloading to POS. This methid is used by inline
     * server.
     */
    public static Collection getAllObjectsForPOS() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("taxID", "TAXID");
        fieldNameMap.put("taxType", "TYPE");
        fieldNameMap.put("taxName", "TAXNM");
        fieldNameMap.put("roundType", "ROUND");
        fieldNameMap.put("numberCount", "DECIMALCNT");
        fieldNameMap.put("taxRate", "PERCENT");
        return DacBase.getMultipleObjects(hyi.cream.dac.TaxType.class,
            "SELECT * FROM posdl_taxtype", fieldNameMap);
    }
}
