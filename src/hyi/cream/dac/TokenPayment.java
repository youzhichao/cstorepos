package hyi.cream.dac;

import hyi.cream.inline.Server;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.*;

public class TokenPayment extends DacBase implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    static final String tableName = "token_payment";
    
    private static ArrayList primaryKeys;
    
    private static final Collection existedFieldList = DacBase.getExistedFieldList(getInsertUpdateTableNameStaticVersion());

    transient private static Set cache;

    static 
    {
        primaryKeys = new ArrayList();
        primaryKeys.add("storeID");
        primaryKeys.add("tokenNumber");
        primaryKeys.add("pluNumber");
    }
    
    public TokenPayment() throws InstantiationException {
        super();
    }

    /**
     * 
     */

    public String getInsertUpdateTableName() {
        if(getUseClientSideTableName())
            return "token_payment";
        if(Server.serverExist())
            return "posdl_token_payment";
        else
            return "token_payment";
    }

    public static String getInsertUpdateTableNameStaticVersion()
    {
        if(Server.serverExist())
            return "posdl_token_payment";
        else
            return "token_payment";
    }
    
    public Collection getExistedFieldList()
    {
        return existedFieldList;
    }
    
    public String getStoreID()
    {
        if(Server.serverExist())
            return (String)getFieldValue("STOREID");
        else
            return (String)getFieldValue("storeID");
    }
    
    public String getTokenNumber()
    {
        if(Server.serverExist())
            return (String)getFieldValue("TOKENNUMBER");
        else
            return (String)getFieldValue("tokenNumber");
    }
    
    public String getPluNumber()
    {
        if(Server.serverExist())
            return (String)getFieldValue("PLUNUMBER");
        else
            return (String)getFieldValue("pluNumber");
    }
    
    public Date getBeginDateTime()
    {
        if(Server.serverExist())
            return (Date)getFieldValue("BEGINDATETIME");
        else
            return (Date)getFieldValue("beginDateTime");
    }
    
    public Date getEndDateTime()
    {
        if(Server.serverExist())
            return (Date)getFieldValue("ENDDATETIME");
        else
            return (Date)getFieldValue("endDateTime");
    }
    
    public BigDecimal getMatchQuantity()
    {
        if(Server.serverExist())
            return (BigDecimal)getFieldValue("MATCHQUANTITY");
        else
            return (BigDecimal)getFieldValue("matchQuantity");
    }
    
    public BigDecimal getTokenAmount()
    {
        if(Server.serverExist())
            return (BigDecimal)getFieldValue("TOKENAMOUNT");
        else
            return (BigDecimal)getFieldValue("tokenAmount");
    }
    
    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public boolean equals(Object obj)
    {
        if(!(obj instanceof TokenPayment))
        {
            return false;
        } else
        {
            TokenPayment objCp = (TokenPayment)obj;
            return "".equals(objCp.getPluNumber());
        }
    }
    
    public static Iterator queryByInStoreIDTokenNo(String tokenNumber, String storeID)
    {
        if(Server.serverExist())
            return DacBase.getMultipleObjects(hyi.cream.dac.TokenPayment.class, "select * from posdl_token_payment where TOKENNUMBER='" + tokenNumber + "' and storeID = '"+storeID+"'");
        else {
            return DacBase.getMultipleObjects(hyi.cream.dac.TokenPayment.class, "select * from token_payment where TOKENNUMBER='"
                + tokenNumber + "' and storeID = '"+storeID+"'" + " and CURRENT_TIMESTAMP between beginDateTime and endDateTime");
        }
         
    }

    public static Map getScToPosFieldNameMap() {
        Map fieldNameMap = new HashMap();
        fieldNameMap.put("storeID", "storeID");
        fieldNameMap.put("tokenNumber", "tokenNumber");
        fieldNameMap.put("pluNumber", "pluNumber");
        fieldNameMap.put("beginDateTime", "beginDateTime");
        fieldNameMap.put("endDateTime", "endDateTime");
        fieldNameMap.put("matchQuantity", "matchQuantity");
        fieldNameMap.put("tokenAmount", "tokenAmount");
        return fieldNameMap;
	}

    public static Collection getAllObjectsForPOS() {
            return DacBase.getMultipleObjects(TokenPayment.class,
                "SELECT * FROM posdl_token_payment", getScToPosFieldNameMap());
        }
    
     public static void createCache() {
        cache = new HashSet();
        Iterator itr = getMultipleObjects(TokenPayment.class, "SELECT * FROM " + tableName);
        if (itr != null) {
            while (itr.hasNext()) {
                cache.add(itr.next());
            }
        }
    }

}
