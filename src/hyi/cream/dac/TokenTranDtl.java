package hyi.cream.dac;

import hyi.cream.inline.Server;
import hyi.cream.util.CreamProperties;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class TokenTranDtl extends DacBase implements  Serializable, Cloneable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    static final String tableName = "token_trandtl";
    
    private static ArrayList primaryKeys;
    
    private static final Collection existedFieldList = DacBase.getExistedFieldList(getInsertUpdateTableNameStaticVersion());
    
    static {
        primaryKeys = new ArrayList();
        primaryKeys.add("storeID");
        primaryKeys.add("posNumber");
        primaryKeys.add("transactionNumber");
        primaryKeys.add("systemDate");
        primaryKeys.add("itemSequence");
    }
    
    public TokenTranDtl() throws InstantiationException {
        super();
    }

    public String getInsertUpdateTableName() {
        if(Server.serverExist())
            return "posul_token_trandtl";
        else
            return "token_trandtl";
    }

    public static String getInsertUpdateTableNameStaticVersion()
    {
        if(Server.serverExist())
            return "posul_token_trandtl";
        else
            return "token_trandtl";
    }
    
    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public Collection getExistedFieldList()
    {
        return existedFieldList;
    }
    
    public boolean equals(Object obj)
    {
        if(!(obj instanceof TokenTranDtl))
        {
            return false;
        } else
        {
            TokenTranDtl objCp = (TokenTranDtl)obj;
            return "".equals(objCp.getPluNumber());
        }
    }
    
    public String getStoreID()
    {
        if(Server.serverExist())
            return (String)getFieldValue("STOREID");
        else
            return (String)getFieldValue("storeID");
    }
    
    public void setStoreNumber(String storecode) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("STOREID", storecode);
        else
            setFieldValue("storeID", storecode);
    }
        
    public Integer getPosNumber()
    {
        if(Server.serverExist())
            return (Integer)getFieldValue("POSNUMBER");
        else
            return (Integer)getFieldValue("posNumber");
    }
    
    public void setPosNumber(int posNumber) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("POSNUMBER", new Integer(posNumber));
        else
            setFieldValue("posNumber", new Integer(posNumber));
    }
    
    public Integer getTransactionNumber()
    {
        if(Server.serverExist())
            return (Integer)getFieldValue("TRANSACTIONNUMBER");
        else
            return (Integer)getFieldValue("transactionNumber");
    }
    
    public void setTransactionNumber(int TranNo) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("TRANSACTIONNUMBER", new Integer(TranNo));
        else
            setFieldValue("transactionNumber", new Integer(TranNo));
    }
    
    public Date getSystemDate()
    {
        if(Server.serverExist())
            return (Date)getFieldValue("SYSTEMDATE");
        else
            return (Date)getFieldValue("systemDate");
    }
    
    public void setSystemDate(Date date) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("SYSTEMDATE", date);
        else
            setFieldValue("systemDate", date);
    }
    
    public Integer getItemSequence()
    {
        if(Server.serverExist())
            return (Integer)getFieldValue("ITEMSEQUENCE");
        else
            return (Integer)getFieldValue("itemSequence");
    }
    
    public void setItemSequence(int itemSeq) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("ITEMSEQUENCE", new Integer(itemSeq));
        else
            setFieldValue("itemSequence", new Integer(itemSeq));
    }
    
    public String getTokenNumber()
    {
        if(Server.serverExist())
            return (String)getFieldValue("TOKENNUMBER");
        else
            return (String)getFieldValue("tokenNumber");
    }
    
    public void setTokenNumber(String tokenNo) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("TOKENNUMBER", tokenNo);
        else
            setFieldValue("tokenNumber", tokenNo);
    }
    
    public String getPluNumber()
    {
        if(Server.serverExist())
            return (String)getFieldValue("PLUNUMBER");
        else
            return (String)getFieldValue("pluNumber");
    }
    
    public void setPluNumber(String pluNo) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("PLUNUMBER", pluNo);
        else
            setFieldValue("pluNumber", pluNo);
    }
    
    public BigDecimal getSaleQuantity()
    {
        if(Server.serverExist())
            return (BigDecimal)getFieldValue("SALEQUANTITY");
        else
            return (BigDecimal)getFieldValue("saleQuantity");
    }
    
    public void setSaleQuantity(BigDecimal saleQty) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("SALEQUANTITY", saleQty);
        else
            setFieldValue("saleQuantity", saleQty);
    }
    
    public BigDecimal getTokenQuantity()
    {
        if(Server.serverExist())
            return (BigDecimal)getFieldValue("TOKENQUANTITY");
        else
            return (BigDecimal)getFieldValue("tokenQuantity");
    }
    
    public void setTokenQuantity(BigDecimal tokenQty) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("TOKENQUANTITY", tokenQty);
        else
            setFieldValue("tokenQuantity", tokenQty);
    }
    
    public BigDecimal getTokenAmount()
    {
        if(Server.serverExist())
            return (BigDecimal)getFieldValue("TOKENAMOUNT");
        else
            return (BigDecimal)getFieldValue("tokenAmount");
    }
    
    public void setTokenAmount(BigDecimal tokenAmt) {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("TOKENAMOUNT", tokenAmt);
        else
            setFieldValue("tokenAmount", tokenAmt);
    }
    
    public String getStatus() 
    {
        if (Server.serverExist())
            return (String)getFieldValue("STATUS");
        else
            return (String)getFieldValue("status");
    }
    
    public void setStatus(String status)
    {
        if (hyi.cream.inline.Server.serverExist())
            setFieldValue("STATUS", status);
        else
            setFieldValue("status", status);
    }
    
    static public Iterator queryByTranNo(String tranNo) {
        Iterator it ;
        String storeId = CreamProperties.getInstance().getProperty("StoreNumber");
        if (hyi.cream.inline.Server.serverExist()) {
            String sel = "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
                " WHERE STOREID='" + storeId + "' and TRANSACTIONNUMBER = " +"'" +tranNo+"'";
            it = getMultipleObjects(TokenTranDtl.class,sel);
        } else {
            String sel = "SELECT * FROM " + getInsertUpdateTableNameStaticVersion() +
                " WHERE storeID='" + storeId + "' and transactionNumber = " + "'"+tranNo+"'";
            it = getMultipleObjects(TokenTranDtl.class,sel);
            ArrayList newArrayList = new ArrayList();
            if (it == null)
                return newArrayList.iterator();
            while (it.hasNext()) {
                TokenTranDtl tokenTrandtl = (TokenTranDtl)it.next();
                newArrayList.add(tokenTrandtl);
            }
            return newArrayList.iterator();
        }
        return it;
    }

    public static  String[][] getPosToScFieldNameArray() {
		return new String[][]{
	        {"storeID", "storeID"},
	        {"posNumber", "posNumber"},
	        {"transactionNumber", "transactionNumber"},
	        {"systemDate", "systemDate"},
	        {"itemSequence", "itemSequence"},
	        {"tokenNumber", "tokenNumber"},
	        {"pluNumber", "pluNumber"},
	        {"saleQuantity", "saleQuantity"},
	        {"tokenQuantity", "tokenQuantity"},
	        {"tokenAmount", "tokenAmount"},
            {"status", "status"},

		};
	}
    public Object clone() {
        return super.clone();
    }
    public TokenTranDtl cloneForSC() {    	
        String[][] fieldNameMap = getPosToScFieldNameArray();
        try {
            TokenTranDtl clonedTokenTranDtl = new TokenTranDtl();
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = this.getFieldValue(fieldNameMap[i][0]);
                if (value == null) {
                    try {
                        value = this.getClass().getDeclaredMethod(
                            "get" + fieldNameMap[i][0], new Class[0]).invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                clonedTokenTranDtl.setFieldValue(fieldNameMap[i][1], value);
            }
            return clonedTokenTranDtl;
        } catch (InstantiationException e) {
            return null;
        }
    }
}
