// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   Transaction.java

package hyi.cream.dac;

import hyi.cream.LineItemNotFoundException;
import hyi.cream.POSTerminalApplication;
import hyi.cream.TooManyLineItemsException;
import hyi.cream.event.TransactionEvent;
import hyi.cream.event.TransactionListener;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.inline.Server;
import hyi.cream.state.StateMachine;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.NumberConfuser;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

// Referenced classes of package hyi.cream.dac:
//            DacBase, LineItem, ZReport, ShiftReport, 
//            DepSales, DaishouSales, PLU, Reason, 
//            DaiShouSales2, TaxType, Trigger, Payment, 
//            SI, Store, Cashier

public class Transaction extends DacBase implements Serializable {

    private static final long serialVersionUID = 1L;

    //public static final String VERSION = "1.6";
    private static final BigDecimal ZERO = new BigDecimal(0);
    private static final BigDecimal ONE = new BigDecimal(1);
    private static final BigDecimal MINUS_ONE = new BigDecimal(-1);
    
    private static ArrayList primaryKeys;
    private ArrayList lineItemArray;
    private ArrayList lineItemArrayLast;
    private ArrayList listener;
    private ArrayList paymentArray;
    public ArrayList payMentList;  // 4 token gllg
    private LineItem currentLineItem;
    private TokenTranDtl curTokenTrandtl; //gllg
    private Payment lastestPayment;
    private BigDecimal itemDiscount;
    private Hashtable siAmtList;
    private Hashtable taxMMAmount;
    private Hashtable taxMMCount;
    private String lastSIID;
    static int i = 0;
    public static Transaction currentTransaction = null;
    private static int mmCount = 0;
    private static BigDecimal mmAmount = ZERO;
    private BigDecimal totalMMAmount;
    private BigDecimal taxAmount;
    private HashMap mmMap;
    private static DateFormat dateFormatter = new SimpleDateFormat(
            "yyyy/MM/dd HH:mm:ss ");
    public static final int maxLineItems = Integer.parseInt(CreamProperties
            .getInstance().getProperty("MaxLineItems"));
    private static final String mixAndMatchVersion = CreamProperties
            .getInstance().getProperty("MixAndMatchVersion");
    private static final Collection existedFieldList = DacBase
            .getExistedFieldList(getInsertUpdateTableNameStaticVersion());
    private List daiShouSalesCommitted;
    private boolean commit;
    private boolean storeComplete;
    private final BigDecimal bigDecimal0 = ZERO;
    private final Integer integer0 = new Integer(0);
    private static final String MIX_AND_MATCH_VERSION = CreamProperties
            .getInstance().getProperty("MixAndMatchVersion");
    private static final boolean excudeRebateInSum = CreamProperties
            .getInstance().getProperty("ExcludeRebateInSum", "no")
            .equalsIgnoreCase("yes");
    private static final String rebateID = CreamProperties.getInstance()
            .getProperty("RebatePaymentID", "");
    private BigDecimal itemRebateAmt;
    private BigDecimal specialRebateAmt;
    private BigDecimal beforeTotalRebate;
    private BigDecimal useRebateAmt;

    //Bruce/20080613/ 全部退货标记
    private boolean returnAll;

    public Transaction() throws InstantiationException {
        lineItemArray = new ArrayList();
        lineItemArrayLast = new ArrayList();
        listener = new ArrayList();
        paymentArray = new ArrayList();
        payMentList = new ArrayList();  // 4 token gllg
        currentLineItem = null;
        lastestPayment = null;
        itemDiscount = ZERO;
        siAmtList = new Hashtable();
        taxMMAmount = new Hashtable();
        taxMMCount = new Hashtable();
        lastSIID = "";
        totalMMAmount = null;
        taxAmount = ZERO;
        mmMap = new HashMap();
        storeComplete = true;
        itemRebateAmt = ZERO;
        specialRebateAmt = ZERO;
        beforeTotalRebate = ZERO;
        useRebateAmt = ZERO;
    }

    public Object deepClone() {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ObjectOutputStream oo = new ObjectOutputStream(bo);
            addLineItem(null); // make the currentLineItem add to lineItemArray 
            oo.writeObject(this);
            ByteArrayInputStream bi = new ByteArrayInputStream(bo.toByteArray());
            ObjectInputStream oi = new ObjectInputStream(bi);
            return oi.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Collection getExistedFieldList() {
        return existedFieldList;
    }

    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public String toString() {
        return "Transaction (pos=" + getTerminalNumber() + ", no="
                + getTransactionNumber() + ")";
    }

    public static Transaction queryByTransactionNumber(String number) {
        return queryByTransactionNumber(number, false);
    }
    
    public static ArrayList queryPrintHistoryTransactionNumber(int startNO, int endNO){
    	ArrayList al = new ArrayList();
    	if (Server.serverExist()) {
    		return al; // not support it
    	}  
    	 Connection connection = null;
         Statement statement = null;
         ResultSet resultSet = null;
         try {
             connection = CreamToolkit.getPooledConnection();
             statement = connection.createStatement();
             String sql = "SELECT tmtranseq FROM "
               + getInsertUpdateTableNameStaticVersion() 
               +" WHERE posno="
               + CreamProperties.getInstance().getProperty("TerminalNumber")
               + " AND tmtranseq between " + startNO + " AND " + endNO
               + " ORDER BY tmtranseq";
             resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
            	 al.add(resultSet.getObject(1));
             }
         } catch (SQLException e) {
             CreamToolkit.logMessage("SQLException: " + e.getMessage());
             CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
             CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
             CreamToolkit.logMessage(e);
         } finally {
             try {
                 if (resultSet != null)
                     resultSet.close();
                 if (statement != null)
                     statement.close();
             } catch (SQLException e) {
                 CreamToolkit.logMessage("SQLException: " + e.getMessage());
                 CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                 CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                 CreamToolkit.logMessage(e);
             }
             if (connection != null)
                 CreamToolkit.releaseConnection(connection);
         }
    	
    	
        return al;
    }
   
    
    public static Transaction queryByTransactionNumber(String number,
            boolean display) {
        Transaction t = (Transaction) DacBase.getSingleObject(
                hyi.cream.dac.Transaction.class, "SELECT * FROM "
                        + getInsertUpdateTableNameStaticVersion()
                        + " WHERE TMTRANSEQ='" + number + "'");
        if (t == null)
            return t;
        if (display)
            POSTerminalApplication.getInstance().getItemList()
                    .setTransaction(t);
        
        Iterator itr = LineItem.queryByTransactionNumber(number);
        if (itr != null)
            try {
                for (; itr.hasNext(); t.addLineItem((LineItem) itr.next()))
                    t.setLockEnable(true);

            } catch (TooManyLineItemsException te) {
                te.printStackTrace(CreamToolkit.getLogger());
            }
        //gllg
        itr = TokenTranDtl.queryByTranNo(number);
        if (itr != null)
            for (; itr.hasNext(); t.addTokenTrandtl((TokenTranDtl) itr.next()));
                t.setLockEnable(true);

        t.setLockEnable(true);
        return t;
    }

    public static Transaction queryByTransactionNumberNoMaxLineItemLimit(String number,
            boolean display) {
        Transaction t = (Transaction) DacBase.getSingleObject(
                hyi.cream.dac.Transaction.class, "SELECT * FROM "
                        + getInsertUpdateTableNameStaticVersion()
                        + " WHERE TMTRANSEQ='" + number + "'");
        if (t == null)
            return t;
        if (display)
            POSTerminalApplication.getInstance().getItemList()
                    .setTransaction(t);
        Iterator itr = LineItem.queryByTransactionNumber(number);
        if (itr != null)
            try {
                for (; itr.hasNext(); t.addLineItemNoMaxLineItemLimit((LineItem) itr.next()))
                    t.setLockEnable(true);

            } catch (TooManyLineItemsException te) {
                te.printStackTrace(CreamToolkit.getLogger());
            }
        //gllg
        itr = TokenTranDtl.queryByTranNo(number);
        if (itr != null)
            for (; itr.hasNext(); t.addTokenTrandtl((TokenTranDtl) itr.next()));
                t.setLockEnable(true);

        t.setLockEnable(true);
        return t;
    }

    public static Transaction queryByTransactionNumber(int posNumber,
            String number) {
        Transaction t = null;
        if (Server.serverExist())
            t = (Transaction) DacBase.getSingleObject(
                    hyi.cream.dac.Transaction.class, "SELECT * FROM "
                            + getInsertUpdateTableNameStaticVersion()
                            + " WHERE PosNumber=" + posNumber
                            + " AND TransactionNumber=" + number);
        else
            t = (Transaction) DacBase.getSingleObject(
                    hyi.cream.dac.Transaction.class, "SELECT * FROM "
                            + getInsertUpdateTableNameStaticVersion()
                            + " WHERE TMTRANSEQ='" + number + "'");
        if (t == null)
            return t;
        Iterator itr = LineItem.queryByTransactionNumber(posNumber, number);
        if (itr != null)
            for (; itr.hasNext(); t.addLineItemSimpleVersion((LineItem) itr
                    .next()))
                t.setLockEnable(true);

        t.setLockEnable(true);
        return t;
    }

    public static int getNextTransactionNumber() {
        int maxSeqInTranhead = 0;
        Connection connection = null;
        Statement statement = null;
        ResultSet rst = null;
        try {
            String sql = "SELECT tmtranseq FROM tranhead order by SYSDATE desc limit 1 ";
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            rst = statement.executeQuery(sql);
            if (rst.next())
                maxSeqInTranhead = rst.getInt("tmtranseq");
            rst.close();
            rst = null;
            statement.close();
            statement = null;
            CreamToolkit.releaseConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                if (rst != null) {
                    rst.close();
                    rst = null;
                }
                if (statement != null) {
                    statement.close();
                    statement = null;
                }
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (Exception exception) {
            }
        }
        int nextTransactionNumber = maxSeqInTranhead + 1;
        String nextTransactionNumberInPropertyStr = CreamProperties
                .getInstance().getProperty("NextTransactionNumber");
        int nextTransactionNumberInProperty;
        try {
            if (nextTransactionNumberInPropertyStr == null)
                nextTransactionNumberInProperty = 1;
            else
                nextTransactionNumberInProperty = Integer
                        .parseInt(nextTransactionNumberInPropertyStr);
        } catch (NumberFormatException e) {
            nextTransactionNumberInProperty = 1;
        }
        nextTransactionNumber = Math.max(nextTransactionNumber,
                nextTransactionNumberInProperty);
        if (nextTransactionNumber > getMaxTranNumberInProperty())
            nextTransactionNumber = 1;
        return nextTransactionNumber;
    }

    public static int getMaxTransactionNumber(int posNumber) {
        Map m;
        if (Server.serverExist())
            m = DacBase
                    .getValueOfStatement("SELECT MAX(transactionNumber) FROM "
                            + getInsertUpdateTableNameStaticVersion()
                            + " WHERE PosNumber='" + posNumber + "'");
        else
            m = DacBase.getValueOfStatement("SELECT MAX(tmtranseq) FROM "
                    + getInsertUpdateTableNameStaticVersion());
        if (m.isEmpty())
            return -1;
        Iterator itr = m.keySet().iterator();
        Integer n = (Integer) m.get(itr.next());
        if (n == null)
            return 0;
        else
            return n.intValue();
    }

    public static int getMaxTransactionNumber() {
        Map m = DacBase.getValueOfStatement("SELECT MAX(tmtranseq) FROM "
                + getInsertUpdateTableNameStaticVersion() + " WHERE posno="
                + CreamProperties.getInstance().getProperty("TerminalNumber"));
        if (m.isEmpty())
            return -1;
        Iterator itr = m.keySet().iterator();
        Integer n = (Integer) m.get(itr.next());
        if (n == null)
            return 0;
        else
            return n.intValue();
    }

    public static int getMaxTranNumberInProperty() {
        int d = 0x5f5e0ff;
        String maxTranNumber = CreamProperties.getInstance().getProperty(
                "MaxTransactionNumber");
        if (maxTranNumber != null)
            try {
                d = Integer.parseInt(maxTranNumber);
            } catch (NumberFormatException e) {
                d = 0x5f5e0ff;
            }
        return d;
    }

    public static int getTransactionReserved() {
        String tranReserved = CreamProperties.getInstance().getProperty(
                "TransactionReserved");
        int d;
        if (tranReserved == null || tranReserved.equals(""))
            d = 60;
        else
            try {
                d = Integer.parseInt(tranReserved);
            } catch (NumberFormatException e) {
                d = 60;
            }
        return d;
    }

    public static int[] getBeginAndEndTranNumber() {
        int a[] = new int[2];
        int tranReserved = getTransactionReserved();
        String today = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
        long l = java.sql.Date.valueOf(today).getTime()
                - (long) (tranReserved * 1000 * 3600) * 24L;
        String baseTime = (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"))
                .format(new java.sql.Date(l));
        String sql = "SELECT tmtranseq FROM tranhead WHERE sysdate > '"
                + baseTime + "' " + " AND posno="
                + CreamProperties.getInstance().getProperty("TerminalNumber")
                + " order by sysdate desc limit 2000";
        Connection connection = null;
        Statement statement = null;
        ResultSet rst = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            rst = statement.executeQuery(sql);
            for (int i = 0; rst.next(); i++) {
                if (i == 0)
                    a[1] = rst.getInt("tmtranseq");
                a[0] = rst.getInt("tmtranseq");
            }

        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            CreamToolkit.logMessage(e);
        } catch (Exception e) {
            CreamToolkit.logMessage(e.getMessage());
        } finally {
            try {
                if (rst != null)
                    rst.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
            return a;
        }
    }

    public static int[] getTransactionLost(int posNumber, int maxTranNumber,
            int beginTranNumber, int endTranNumber) {
        String tranNumberCondition = "";
        boolean b = beginTranNumber <= endTranNumber;
        String selectStatement;
        if (Server.serverExist()) {
            if (b)
                tranNumberCondition = " AND transactionNumber BETWEEN "
                        + beginTranNumber + " AND " + endTranNumber;
            else
                tranNumberCondition = " AND ((transactionNumber BETWEEN "
                        + beginTranNumber + " AND " + maxTranNumber + ") "
                        + "OR (transactionNumber BETWEEN 1 " + "AND "
                        + endTranNumber + ")) ";
            selectStatement = "SELECT transactionNumber FROM "
                    + getInsertUpdateTableNameStaticVersion()
                    + " WHERE PosNumber=" + posNumber + tranNumberCondition
                    + " ORDER BY systemDate";
        } else {
            if (b)
                tranNumberCondition = " AND tmtranseq BETWEEN "
                        + beginTranNumber + " AND " + endTranNumber;
            else
                tranNumberCondition = " AND ((tmtranseq BETWEEN "
                        + beginTranNumber + " AND " + maxTranNumber + ") "
                        + "OR (tmtranseq BETWEEN 1 " + "AND " + endTranNumber
                        + ")) ";
            selectStatement = "SELECT tmtranseq FROM "
                    + getInsertUpdateTableNameStaticVersion()
                    + " WHERE POSNO = " + posNumber + tranNumberCondition
                    + " ORDER BY SYSDATE";
        }
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList collection = new ArrayList();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            List tranNoInSC = new ArrayList();
            for (; resultSet.next(); tranNoInSC.add(new Integer(resultSet
                    .getInt(1))))
                ;
            Object tmpArrays[] = tranNoInSC.toArray();
            Arrays.sort(tmpArrays);
            for (int i = beginTranNumber; i <= (b ? endTranNumber
                    : maxTranNumber + endTranNumber); i++) {
                int k = i > maxTranNumber ? i - maxTranNumber : i;
                if (tmpArrays.length == 0
                        || Arrays.binarySearch(tmpArrays, new Integer(k)) < 0)
                    collection.add(new Integer(k));
            }

        } catch (SQLException e) {
            CreamToolkit.logMessage("SQLException: " + e.getMessage());
            CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
            CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
            CreamToolkit.logMessage(e);
        } catch (Exception e) {
            CreamToolkit.logMessage(e);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                CreamToolkit.logMessage("SQLException: " + e.getMessage());
                CreamToolkit.logMessage("SQLState:     " + e.getSQLState());
                CreamToolkit.logMessage("VendorError:  " + e.getErrorCode());
                CreamToolkit.logMessage(e);
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
        if (collection != null) {
            int ret[] = new int[collection.size()];
            for (i = 0; i < ret.length; i++)
                ret[i] = ((Integer) collection.get(i)).intValue();

            return ret;
        } else {
            return null;
        }
    }

    public static Iterator getUploadFailedShift() {
        String failFlag = "2";
        String notUploadFlag = "0";
        return DacBase.getMultipleObjects(hyi.cream.dac.ShiftReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                        + " WHERE TCPFLG='" + failFlag + "' OR TCPFLG='"
                        + notUploadFlag + "'");
    }

    public void setLastSIID(String id) {
        lastSIID = id;
    }

    public static Transaction createCurrentTransaction() {
        if (currentTransaction == null) {
            try {
                currentTransaction = new Transaction();
            } catch (InstantiationException e) {
                e.printStackTrace(CreamToolkit.getLogger());
                return null;
            }
            currentTransaction.init();
        }
        return currentTransaction;
    }

    public synchronized void init() {
        setCommit(false);
        CreamProperties prop = CreamProperties.getInstance();
        setDealType1("0");
        setDealType3("0");
        setDealType2("0");
        setTransactionNumber(getNextTransactionNumber());
        setTerminalPhysicalNumber(prop.getProperty("TerminalPhysicalNumber"));
        setTerminalNumber(Integer.parseInt(prop.getProperty("TerminalNumber")));
        setStoreNumber(prop.getProperty("StoreNumber"));
        setInvoiceID(prop.getProperty("InvoiceID"));
        setInvoiceNumber(prop.getProperty("invoiceNumber"));
        setInvoiceCount(0);
        setSignOnNumber(Integer.parseInt(prop.getProperty("ShiftNumber")));
        setZSequenceNumber(new Integer(ZReport.getCurrentZNumber()));
        setCashierNumber(prop.getProperty("CashierNumber"));
        setBuyerNumber("");
        setTransactionType("00");
        setSpillAmount(bigDecimal0);
        setDetailCount(0);
        setSalesAmount(bigDecimal0);
        setGrossSalesAmount(bigDecimal0);
        setDaiFuAmount(bigDecimal0);
        setDaiShouAmount(bigDecimal0);
        setDaiShouAmount2(bigDecimal0);
        setItemsRebateAmount(bigDecimal0);
        setSpecialRebateAmount(bigDecimal0);
        setRebateAmount(bigDecimal0);
        setRebateChangeAmount(bigDecimal0);
        setTotalRebateAmount(bigDecimal0);
        setUseRebateAmt(bigDecimal0);
        setBeforeTotalRebateAmt(bigDecimal0);
        setCustomerCount(1);
        setCustomerAgeLevel(1);
        setSalesman(POSTerminalApplication.getNewCashierID());
        POSTerminalApplication.getInstance().getSystemInfo().setSalesManNumber(
                POSTerminalApplication.getNewCashierID());
        setGrossSalesTax0Amount(bigDecimal0);
        setGrossSalesTax1Amount(bigDecimal0);
        setGrossSalesTax2Amount(bigDecimal0);
        setGrossSalesTax3Amount(bigDecimal0);
        setGrossSalesTax4Amount(bigDecimal0);
        setSIDiscountAmount0(bigDecimal0);
        setSIDiscountAmount1(bigDecimal0);
        setSIDiscountAmount2(bigDecimal0);
        setSIDiscountAmount3(bigDecimal0);
        setSIDiscountAmount4(bigDecimal0);
        setSIDiscountCount0(0);
        setSIDiscountCount1(0);
        setSIDiscountCount2(0);
        setSIDiscountCount3(0);
        setSIDiscountCount4(0);
        setSIPercentPlusAmount0(bigDecimal0);
        setSIPercentPlusAmount1(bigDecimal0);
        setSIPercentPlusAmount2(bigDecimal0);
        setSIPercentPlusAmount3(bigDecimal0);
        setSIPercentPlusAmount4(bigDecimal0);
        setSIPercentPlusCount0(0);
        setSIPercentPlusCount1(0);
        setSIPercentPlusCount2(0);
        setSIPercentPlusCount3(0);
        setSIPercentPlusCount4(0);
        setSIPercentOffAmount0(bigDecimal0);
        setSIPercentOffAmount1(bigDecimal0);
        setSIPercentOffAmount2(bigDecimal0);
        setSIPercentOffAmount3(bigDecimal0);
        setSIPercentOffAmount4(bigDecimal0);
        setSIPercentOffCount0(0);
        setSIPercentOffCount1(0);
        setSIPercentOffCount2(0);
        setSIPercentOffCount3(0);
        setSIPercentOffCount4(0);
        setNotIncludedSales0(bigDecimal0);
        setNotIncludedSales1(bigDecimal0);
        setNotIncludedSales2(bigDecimal0);
        setNotIncludedSales3(bigDecimal0);
        setNotIncludedSales4(bigDecimal0);
        setNetSalesAmount(bigDecimal0);
        setNetSalesAmount0(bigDecimal0);
        setNetSalesAmount1(bigDecimal0);
        setNetSalesAmount2(bigDecimal0);
        setNetSalesAmount3(bigDecimal0);
        setNetSalesAmount4(bigDecimal0);
        setMixAndMatchAmount0(bigDecimal0.setScale(2, 4));
        setMixAndMatchAmount1(bigDecimal0.setScale(2, 4));
        setMixAndMatchAmount2(bigDecimal0.setScale(2, 4));
        setMixAndMatchAmount3(bigDecimal0.setScale(2, 4));
        setMixAndMatchAmount4(bigDecimal0.setScale(2, 4));
        setMixAndMatchCount0(0);
        setMixAndMatchCount1(0);
        setMixAndMatchCount2(0);
        setMixAndMatchCount3(0);
        setMixAndMatchCount4(0);
        setCreditCardExpireDate(CreamToolkit.getInitialDate());
        //gllg
        setItemDiscAmount0(bigDecimal0);
        setItemDiscAmount1(bigDecimal0);
        setItemDiscAmount2(bigDecimal0);
        setItemDiscAmount3(bigDecimal0);
        setItemDiscAmount4(bigDecimal0);
        
        setItemDiscCnt0(0);
        setItemDiscCnt1(0);
        setItemDiscCnt2(0);
        setItemDiscCnt3(0);
        setItemDiscCnt4(0);
 
        currentLineItem = null;
        lineItemArray = new ArrayList();
        lineItemArrayLast = new ArrayList();
        setSIAmtList(null, null);
        paymentArray.clear();
        itemDiscount = bigDecimal0;
        mmAmount = bigDecimal0;
        mmCount = 0;
        clearMMDetail();
        totalMMAmount = null;
        setReturnAll(false);
        if (POSTerminalApplication.getInstance().getSystemInfo() != null)
            POSTerminalApplication.getInstance().getSystemInfo().setIsDaiFu(
                    false);
    }

    public void Clear(boolean fireEvent) {
        super.clear();
        init();
        if (fireEvent)
            fireEvent(new TransactionEvent(this, 4));
    }

    public void Clear() {
        Clear(true);
    }

    public void makeNegativeValue() {
        Iterator itr = getValues().keySet().iterator();
        Map values = getValues();
        while (itr.hasNext()) {
            Object key = itr.next();
            if (values.get(key) instanceof BigDecimal) {
                BigDecimal big = (BigDecimal) values.get(key);
                big = big.negate();
                setFieldValue((String) key, big);
            } else if (values.get(key) instanceof Integer) {
                String k = (String) key;
                Integer in = (Integer) values.get(k);
                if (k.toUpperCase().startsWith("MNMCNT")
                        || k.toUpperCase().startsWith("ITEMDISCCNT")
                        || k.toUpperCase().startsWith("SIPCNT")
                        || k.toUpperCase().startsWith("SIPLUSCNT")
                        || k.toUpperCase().startsWith("SIMCNT"))
                    setFieldValue((String) key, new Integer(0 - in.intValue()));
            }
        }
        if (getLineItemsArrayLast() != null) {
            LineItem li;
            for (Iterator iter = getLineItems(); iter.hasNext(); li
                    .makeNegativeValue())
                li = (LineItem) iter.next();

        }
        setTotalMMAmount(getTotalMMAmount().negate());
        Hashtable ht = (Hashtable) getSIAmtList().clone();
        if (ht.size() > 0) {
            String siID;
            for (Iterator iter = ht.keySet().iterator(); iter.hasNext(); setSIAmtList(
                    siID, ((BigDecimal) ht.get(siID)).negate()))
                siID = (String) iter.next();

        }
    }

    public void makeCopy(Transaction t) {
        super.fieldMap = t.getValues();
    }

    public Integer getZSequenceNumber() {
        if (Server.serverExist())
            return (Integer) getFieldValue("zSequenceNumber");
        else
            return (Integer) getFieldValue("EODCNT");
    }

    public void setZSequenceNumber(Integer number) {
        if (Server.serverExist())
            setFieldValue("zSequenceNumber", number);
        else
            setFieldValue("EODCNT", number);
    }

    public String getCorrespondingID(String payID) {
        if (getPayNumber1() != null && getPayNumber1().equals(payID))
            return "1";
        if (getPayNumber2() != null && getPayNumber2().equals(payID))
            return "2";
        if (getPayNumber3() != null && getPayNumber3().equals(payID))
            return "3";
        if (getPayNumber4() != null && getPayNumber4().equals(payID))
            return "4";
        else
            return "";
    }

    public void tran_Processing(boolean isRecalc) throws Exception {
        ShiftReport shift = null;
        ZReport z = null;
        boolean ischeck = false;
        if (POSTerminalApplication.ischeck) {
            shift = POSTerminalApplication.getInstance().getNewShift();
            z = POSTerminalApplication.getInstance().getNewZ();
            ischeck = true;
        } else {
            shift = ShiftReport.getCurrentShift();
            z = ZReport.getOrCreateCurrentZReport();
        }
        Integer in = integer0;
        if (getDealType2().compareTo("9") == 0) {
            shift.setSignOffSystemDateTime(new Date());
            if (isRecalc)
                shift.setEndTransactionNumber(getTransactionNumber(), false);
            else
                shift.setEndTransactionNumber(getTransactionNumber(), true);

            in = new Integer(shift.getTransactionCount().intValue() + 1);
            shift.setTransactionCount(in);
            if (z != null)
                z.setCashierNumber(shift.getCashierNumber());
            if (!ischeck) {
                shift.setAccountingDate(new Date());
                shift.update();
                CreamToolkit.logMessage("Shift report closed: accdate="
                        + new SimpleDateFormat("yyyy-MM-dd").format(shift.getAccountingDate())
                        + ",z="
                        + shift.getZSequenceNumber() + ",shift="
                        + shift.getSequenceNumber() + ",time="
                        + dateFormatter.format(shift.getSignOffSystemDateTime())
                        + ",amt=" + shift.getTotalAmount());
            } else {
                shift.setAccountingDate(POSTerminalApplication.getInstance()
                        .getNewDate());
            }
            return;
        }
        if (getDealType2().compareTo("D") == 0) {
            z.setEndSystemDateTime(new Date());
            if (isRecalc)
                z.setEndTransactionNumber(getTransactionNumber(), false);
            else
                z.setEndTransactionNumber(getTransactionNumber(), true);
            
            in = new Integer(z.getTransactionCount().intValue() + 1);
            z.setTransactionCount(in);
            z
                    .setTransactionCount(new Integer((z
                            .getEndTransactionNumber().intValue() - z
                            .getBeginTransactionNumber().intValue()) + 1));
            if (!ischeck) {
                z.setAccountingDate(new Date());
                z.update();
                Object d[] = DepSales.getCurrentDepSales();
                for (int i = 0; i < d.length; i++) {
                    DepSales dep = (DepSales) d[i];
                    dep.setAccountDate(new Date());
                    dep.update();
                }

                Iterator ite = DaishouSales
                        .getCurrentDaishouSales(getZSequenceNumber().intValue());
                if (ite != null) {
                    DaishouSales daishouSales;
                    for (; ite.hasNext(); daishouSales.update()) {
                        daishouSales = (DaishouSales) ite.next();
                        daishouSales.setAccountDate(new Date());
                    }

                }
                CreamToolkit.logMessage("Z report closed: accdate="
                        + new SimpleDateFormat("yyyy-MM-dd").format(z.getAccountingDate())
                        + ",z=" + z.getSequenceNumber() + ",time="
                        + dateFormatter.format(z.getEndSystemDateTime()) + ",amt="
                        + z.getTotalAmount());
            } else {
                z.setAccountingDate(POSTerminalApplication.getInstance()
                        .getNewDate());
            }
            return;
        }
        if (getDealType3().compareTo("1") == 0) {
            shift.setTransactionVoidAmount(shift.getTransactionVoidAmount()
                    .add(getSalesAmount().negate()));
            shift.setTransactionVoidCount(new Integer(shift
                    .getTransactionVoidCount().intValue() + 1));
            z.setTransactionVoidAmount(z.getTransactionVoidAmount().add(
                    getSalesAmount().negate()));
            z.setTransactionVoidCount(new Integer(z.getTransactionVoidCount()
                    .intValue() + 1));
        }
        if (getDealType3().compareTo("2") == 0) {
            shift.setReprintAmount(shift.getReprintAmount().add(
                    getSalesAmount().negate()));
            shift.setReprintCount(new Integer(shift.getReprintCount()
                    .intValue() + 1));
            z.setReprintAmount(z.getReprintAmount().add(
                    getSalesAmount().negate()));
            z.setReprintCount(new Integer(z.getReprintCount().intValue() + 1));
        } else if (getDealType3().compareTo("3") == 0) {
            shift.setTransactionCancelAmount(shift.getTransactionCancelAmount()
                    .subtract(getSalesAmount()).subtract(getPayAmount1())
                    .subtract(getPayAmount2()).subtract(getPayAmount3())
                    .subtract(getPayAmount4()));
            shift.setTransactionCancelCount(new Integer(shift
                    .getTransactionCancelCount().intValue() + 1));
            z.setTransactionCancelAmount(z.getTransactionCancelAmount()
                    .subtract(getSalesAmount()).subtract(getPayAmount1())
                    .subtract(getPayAmount2()).subtract(getPayAmount3())
                    .subtract(getPayAmount4()));
            z.setTransactionCancelCount(new Integer(z
                    .getTransactionCancelCount().intValue() + 1));
        } else if (getDealType3().compareTo("4") == 0) {
            shift.setReturnAmount(shift.getReturnAmount().subtract(
                    getNetSalesAmount()));
            z
                    .setReturnAmount(z.getReturnAmount().subtract(
                            getNetSalesAmount()));
            shift.setReturnItemQuantity(shift.getReturnItemQuantity().add(
                    POSTerminalApplication.getInstance().getReturnQty()));
            z.setReturnItemQuantity(z.getReturnItemQuantity().add(
                    POSTerminalApplication.getInstance().getReturnQty()));
            POSTerminalApplication.getInstance().setReturnQty(bigDecimal0);
            if (getDealType2().compareTo("3") == 0) {
                shift.setReturnTransactionCount(new Integer(shift
                        .getReturnTransactionCount().intValue() + 1));
                z.setReturnTransactionCount(new Integer(z
                        .getReturnTransactionCount().intValue() + 1));
            }
        }
        z.setGrandTotal(z.getGrandTotal().add(getGrossSalesAmount()));
        z.setTransactionCountGrandTotal(new Integer(z
                .getTransactionCountGrandTotal().intValue() + 1));

        if (isRecalc)
           shift.setEndTransactionNumber(getTransactionNumber(), false);
        else
           shift.setEndTransactionNumber(getTransactionNumber(), true);
        
        if (isRecalc)
            z.setEndTransactionNumber(getTransactionNumber(), false);
        else
            z.setEndTransactionNumber(getTransactionNumber(), true);

        shift.setTaxType0GrossSales(shift.getTaxType0GrossSales().add(
                getGrossSalesTax0Amount()));
        shift.setTaxType1GrossSales(shift.getTaxType1GrossSales().add(
                getGrossSalesTax1Amount()));
        shift.setTaxType2GrossSales(shift.getTaxType2GrossSales().add(
                getGrossSalesTax2Amount()));
        shift.setTaxType3GrossSales(shift.getTaxType3GrossSales().add(
                getGrossSalesTax3Amount()));
        shift.setTaxType4GrossSales(shift.getTaxType4GrossSales().add(
                getGrossSalesTax4Amount()));
        if (z.getTaxType0GrossSales() == null)
            z.setTaxType0GrossSales(bigDecimal0);
        z.setTaxType0GrossSales(z.getTaxType0GrossSales().add(
                getGrossSalesTax0Amount()));
        if (z.getTaxType1GrossSales() == null)
            z.setTaxType1GrossSales(bigDecimal0);
        z.setTaxType1GrossSales(z.getTaxType1GrossSales().add(
                getGrossSalesTax1Amount()));
        if (z.getTaxType2GrossSales() == null)
            z.setTaxType2GrossSales(bigDecimal0);
        z.setTaxType2GrossSales(z.getTaxType2GrossSales().add(
                getGrossSalesTax2Amount()));
        if (z.getTaxType3GrossSales() == null)
            z.setTaxType3GrossSales(bigDecimal0);
        z.setTaxType3GrossSales(z.getTaxType3GrossSales().add(
                getGrossSalesTax3Amount()));
        if (z.getTaxType4GrossSales() == null)
            z.setTaxType4GrossSales(bigDecimal0);
        z.setTaxType4GrossSales(z.getTaxType4GrossSales().add(
                getGrossSalesTax4Amount()));
        shift.setNetSalesAmount0(shift.getNetSalesAmount0().add(
                getNetSalesAmount0()));
        shift.setNetSalesAmount1(shift.getNetSalesAmount1().add(
                getNetSalesAmount1()));
        shift.setNetSalesAmount2(shift.getNetSalesAmount2().add(
                getNetSalesAmount2()));
        shift.setNetSalesAmount3(shift.getNetSalesAmount3().add(
                getNetSalesAmount3()));
        shift.setNetSalesAmount4(shift.getNetSalesAmount4().add(
                getNetSalesAmount4()));
        if (z.getNetSalesAmount0() == null)
            z.setNetSalesAmount0(bigDecimal0);
        z.setNetSalesAmount0(z.getNetSalesAmount0().add(getNetSalesAmount0()));
        if (z.getNetSalesAmount1() == null)
            z.setNetSalesAmount1(bigDecimal0);
        z.setNetSalesAmount1(z.getNetSalesAmount1().add(getNetSalesAmount1()));
        if (z.getNetSalesAmount2() == null)
            z.setNetSalesAmount2(bigDecimal0);
        z.setNetSalesAmount2(z.getNetSalesAmount2().add(getNetSalesAmount2()));
        if (z.getNetSalesAmount3() == null)
            z.setNetSalesAmount3(bigDecimal0);
        z.setNetSalesAmount3(z.getNetSalesAmount3().add(getNetSalesAmount3()));
        if (z.getNetSalesAmount4() == null)
            z.setNetSalesAmount4(bigDecimal0);
        z.setNetSalesAmount4(z.getNetSalesAmount4().add(getNetSalesAmount4()));
        if (getTaxAmount() == null)
            setTaxAmount(bigDecimal0);
        if (getTaxAmount0() == null)
            setTaxAmount0(bigDecimal0);
        if (getTaxAmount1() == null)
            setTaxAmount1(bigDecimal0);
        if (getTaxAmount2() == null)
            setTaxAmount2(bigDecimal0);
        if (getTaxAmount3() == null)
            setTaxAmount3(bigDecimal0);
        if (getTaxAmount4() == null)
            setTaxAmount4(bigDecimal0);
        if (shift.getTaxAmount() == null)
            shift.setTaxAmount(bigDecimal0);
        if (shift.getTaxAmount0() == null)
            shift.setTaxAmount0(bigDecimal0);
        if (shift.getTaxAmount1() == null)
            shift.setTaxAmount1(bigDecimal0);
        if (shift.getTaxAmount2() == null)
            shift.setTaxAmount2(bigDecimal0);
        if (shift.getTaxAmount3() == null)
            shift.setTaxAmount3(bigDecimal0);
        if (shift.getTaxAmount4() == null)
            shift.setTaxAmount4(bigDecimal0);
        if (z.getTaxAmount() == null)
            z.setTaxAmount(bigDecimal0);
        if (z.getTaxAmount0() == null)
            z.setTaxAmount0(bigDecimal0);
        if (z.getTaxAmount1() == null)
            z.setTaxAmount1(bigDecimal0);
        if (z.getTaxAmount2() == null)
            z.setTaxAmount2(bigDecimal0);
        if (z.getTaxAmount3() == null)
            z.setTaxAmount3(bigDecimal0);
        if (z.getTaxAmount4() == null)
            z.setTaxAmount4(bigDecimal0);
        shift.setTaxAmount(shift.getTaxAmount().add(getTaxAmount()));
        shift.setTaxAmount0(shift.getTaxAmount0().add(getTaxAmount0()));
        shift.setTaxAmount1(shift.getTaxAmount1().add(getTaxAmount1()));
        shift.setTaxAmount2(shift.getTaxAmount2().add(getTaxAmount2()));
        shift.setTaxAmount3(shift.getTaxAmount3().add(getTaxAmount3()));
        shift.setTaxAmount4(shift.getTaxAmount4().add(getTaxAmount4()));
        z.setTaxAmount(z.getTaxAmount().add(getTaxAmount()));
        z.setTaxAmount0(z.getTaxAmount0().add(getTaxAmount0()));
        z.setTaxAmount1(z.getTaxAmount1().add(getTaxAmount1()));
        z.setTaxAmount2(z.getTaxAmount2().add(getTaxAmount2()));
        z.setTaxAmount3(z.getTaxAmount3().add(getTaxAmount3()));
        z.setTaxAmount4(z.getTaxAmount4().add(getTaxAmount4()));
        shift.setMixAndMatchAmount0(shift.getMixAndMatchAmount0().add(
                getMixAndMatchAmount0()));
        shift.setMixAndMatchAmount1(shift.getMixAndMatchAmount1().add(
                getMixAndMatchAmount1()));
        shift.setMixAndMatchAmount2(shift.getMixAndMatchAmount2().add(
                getMixAndMatchAmount2()));
        shift.setMixAndMatchAmount3(shift.getMixAndMatchAmount3().add(
                getMixAndMatchAmount3()));
        shift.setMixAndMatchAmount4(shift.getMixAndMatchAmount4().add(
                getMixAndMatchAmount4()));
        z.setMixAndMatchAmount0(z.getMixAndMatchAmount0().add(
                getMixAndMatchAmount0()));
        z.setMixAndMatchAmount1(z.getMixAndMatchAmount1().add(
                getMixAndMatchAmount1()));
        z.setMixAndMatchAmount2(z.getMixAndMatchAmount2().add(
                getMixAndMatchAmount2()));
        z.setMixAndMatchAmount3(z.getMixAndMatchAmount3().add(
                getMixAndMatchAmount3()));
        z.setMixAndMatchAmount4(z.getMixAndMatchAmount4().add(
                getMixAndMatchAmount4()));
        shift.setMixAndMatchCount0(new Integer(shift.getMixAndMatchCount0()
                .intValue()
                + getMixAndMatchCount0().intValue()));
        shift.setMixAndMatchCount1(new Integer(shift.getMixAndMatchCount1()
                .intValue()
                + getMixAndMatchCount1().intValue()));
        shift.setMixAndMatchCount2(new Integer(shift.getMixAndMatchCount2()
                .intValue()
                + getMixAndMatchCount2().intValue()));
        shift.setMixAndMatchCount3(new Integer(shift.getMixAndMatchCount3()
                .intValue()
                + getMixAndMatchCount3().intValue()));
        shift.setMixAndMatchCount4(new Integer(shift.getMixAndMatchCount4()
                .intValue()
                + getMixAndMatchCount4().intValue()));
        z.setMixAndMatchCount0(new Integer(z.getMixAndMatchCount0().intValue()
                + getMixAndMatchCount0().intValue()));
        z.setMixAndMatchCount1(new Integer(z.getMixAndMatchCount1().intValue()
                + getMixAndMatchCount1().intValue()));
        z.setMixAndMatchCount2(new Integer(z.getMixAndMatchCount2().intValue()
                + getMixAndMatchCount2().intValue()));
        z.setMixAndMatchCount3(new Integer(z.getMixAndMatchCount3().intValue()
                + getMixAndMatchCount3().intValue()));
        z.setMixAndMatchCount4(new Integer(z.getMixAndMatchCount4().intValue()
                + getMixAndMatchCount4().intValue()));
        shift.setSIDiscountAmount0(shift.getSIDiscountAmount0().add(
                getSIDiscountAmount0()));
        shift.setSIDiscountAmount1(shift.getSIDiscountAmount1().add(
                getSIDiscountAmount1()));
        shift.setSIDiscountAmount2(shift.getSIDiscountAmount2().add(
                getSIDiscountAmount2()));
        shift.setSIDiscountAmount3(shift.getSIDiscountAmount3().add(
                getSIDiscountAmount3()));
        shift.setSIDiscountAmount4(shift.getSIDiscountAmount4().add(
                getSIDiscountAmount4()));
        z.setSIDiscountAmount0(z.getSIDiscountAmount0().add(
                getSIDiscountAmount0()));
        z.setSIDiscountAmount1(z.getSIDiscountAmount1().add(
                getSIDiscountAmount1()));
        z.setSIDiscountAmount2(z.getSIDiscountAmount2().add(
                getSIDiscountAmount2()));
        z.setSIDiscountAmount3(z.getSIDiscountAmount3().add(
                getSIDiscountAmount3()));
        z.setSIDiscountAmount4(z.getSIDiscountAmount4().add(
                getSIDiscountAmount4()));
        shift.setSIDiscountCount0(new Integer(shift.getSIDiscountCount0()
                .intValue()
                + getSIDiscountCount0().intValue()));
        shift.setSIDiscountCount1(new Integer(shift.getSIDiscountCount1()
                .intValue()
                + getSIDiscountCount1().intValue()));
        shift.setSIDiscountCount2(new Integer(shift.getSIDiscountCount2()
                .intValue()
                + getSIDiscountCount2().intValue()));
        shift.setSIDiscountCount3(new Integer(shift.getSIDiscountCount3()
                .intValue()
                + getSIDiscountCount3().intValue()));
        shift.setSIDiscountCount4(new Integer(shift.getSIDiscountCount4()
                .intValue()
                + getSIDiscountCount4().intValue()));
        z.setSIDiscountCount0(new Integer(z.getSIDiscountCount0().intValue()
                + getSIDiscountCount0().intValue()));
        z.setSIDiscountCount1(new Integer(z.getSIDiscountCount1().intValue()
                + getSIDiscountCount1().intValue()));
        z.setSIDiscountCount2(new Integer(z.getSIDiscountCount2().intValue()
                + getSIDiscountCount2().intValue()));
        z.setSIDiscountCount3(new Integer(z.getSIDiscountCount3().intValue()
                + getSIDiscountCount3().intValue()));
        z.setSIDiscountCount4(new Integer(z.getSIDiscountCount4().intValue()
                + getSIDiscountCount4().intValue()));
        shift.setSIPercentOffAmount0(shift.getSIPercentOffAmount0().add(
                getSIPercentOffAmount0()));
        shift.setSIPercentOffAmount1(shift.getSIPercentOffAmount1().add(
                getSIPercentOffAmount1()));
        shift.setSIPercentOffAmount2(shift.getSIPercentOffAmount2().add(
                getSIPercentOffAmount2()));
        shift.setSIPercentOffAmount3(shift.getSIPercentOffAmount3().add(
                getSIPercentOffAmount3()));
        shift.setSIPercentOffAmount4(shift.getSIPercentOffAmount4().add(
                getSIPercentOffAmount4()));
        z.setSIPercentOffAmount0(z.getSIPercentOffAmount0().add(
                getSIPercentOffAmount0()));
        z.setSIPercentOffAmount1(z.getSIPercentOffAmount1().add(
                getSIPercentOffAmount1()));
        z.setSIPercentOffAmount2(z.getSIPercentOffAmount2().add(
                getSIPercentOffAmount2()));
        z.setSIPercentOffAmount3(z.getSIPercentOffAmount3().add(
                getSIPercentOffAmount3()));
        z.setSIPercentOffAmount4(z.getSIPercentOffAmount4().add(
                getSIPercentOffAmount4()));
        shift.setSIPercentOffCount0(new Integer(shift.getSIPercentOffCount0()
                .intValue()
                + getSIPercentOffCount0().intValue()));
        shift.setSIPercentOffCount1(new Integer(shift.getSIPercentOffCount1()
                .intValue()
                + getSIPercentOffCount1().intValue()));
        shift.setSIPercentOffCount2(new Integer(shift.getSIPercentOffCount2()
                .intValue()
                + getSIPercentOffCount2().intValue()));
        shift.setSIPercentOffCount3(new Integer(shift.getSIPercentOffCount3()
                .intValue()
                + getSIPercentOffCount3().intValue()));
        shift.setSIPercentOffCount4(new Integer(shift.getSIPercentOffCount4()
                .intValue()
                + getSIPercentOffCount4().intValue()));
        z.setSIPercentOffCount0(new Integer(z.getSIPercentOffCount0()
                .intValue()
                + getSIPercentOffCount0().intValue()));
        z.setSIPercentOffCount1(new Integer(z.getSIPercentOffCount1()
                .intValue()
                + getSIPercentOffCount1().intValue()));
        z.setSIPercentOffCount2(new Integer(z.getSIPercentOffCount2()
                .intValue()
                + getSIPercentOffCount2().intValue()));
        z.setSIPercentOffCount3(new Integer(z.getSIPercentOffCount3()
                .intValue()
                + getSIPercentOffCount3().intValue()));
        z.setSIPercentOffCount4(new Integer(z.getSIPercentOffCount4()
                .intValue()
                + getSIPercentOffCount4().intValue()));
        shift.setSIPercentPlusAmount0(shift.getSIPercentPlusAmount0().add(
                getSIPercentPlusAmount0()));
        shift.setSIPercentPlusAmount1(shift.getSIPercentPlusAmount1().add(
                getSIPercentPlusAmount1()));
        shift.setSIPercentPlusAmount2(shift.getSIPercentPlusAmount2().add(
                getSIPercentPlusAmount2()));
        shift.setSIPercentPlusAmount3(shift.getSIPercentPlusAmount3().add(
                getSIPercentPlusAmount3()));
        shift.setSIPercentPlusAmount4(shift.getSIPercentPlusAmount4().add(
                getSIPercentPlusAmount4()));
        z.setSIPercentPlusAmount0(z.getSIPercentPlusAmount0().add(
                getSIPercentPlusAmount0()));
        z.setSIPercentPlusAmount1(z.getSIPercentPlusAmount1().add(
                getSIPercentPlusAmount1()));
        z.setSIPercentPlusAmount2(z.getSIPercentPlusAmount2().add(
                getSIPercentPlusAmount2()));
        z.setSIPercentPlusAmount3(z.getSIPercentPlusAmount3().add(
                getSIPercentPlusAmount3()));
        z.setSIPercentPlusAmount4(z.getSIPercentPlusAmount4().add(
                getSIPercentPlusAmount4()));
        shift.setSIPercentPlusCount0(new Integer(shift.getSIPercentPlusCount0()
                .intValue()
                + getSIPercentPlusCount0().intValue()));
        shift.setSIPercentPlusCount1(new Integer(shift.getSIPercentPlusCount1()
                .intValue()
                + getSIPercentPlusCount1().intValue()));
        shift.setSIPercentPlusCount2(new Integer(shift.getSIPercentPlusCount2()
                .intValue()
                + getSIPercentPlusCount2().intValue()));
        shift.setSIPercentPlusCount3(new Integer(shift.getSIPercentPlusCount3()
                .intValue()
                + getSIPercentPlusCount3().intValue()));
        shift.setSIPercentPlusCount4(new Integer(shift.getSIPercentPlusCount4()
                .intValue()
                + getSIPercentPlusCount4().intValue()));
        z.setSIPercentPlusCount0(new Integer(z.getSIPercentPlusCount0()
                .intValue()
                + getSIPercentPlusCount0().intValue()));
        z.setSIPercentPlusCount1(new Integer(z.getSIPercentPlusCount1()
                .intValue()
                + getSIPercentPlusCount1().intValue()));
        z.setSIPercentPlusCount2(new Integer(z.getSIPercentPlusCount2()
                .intValue()
                + getSIPercentPlusCount2().intValue()));
        z.setSIPercentPlusCount3(new Integer(z.getSIPercentPlusCount3()
                .intValue()
                + getSIPercentPlusCount3().intValue()));
        z.setSIPercentPlusCount4(new Integer(z.getSIPercentPlusCount4()
                .intValue()
                + getSIPercentPlusCount4().intValue()));

        
        shift.setItemDiscAmount(shift.getItemDiscAmount().add(this.getItemDiscAmount()));
        shift.setItemDiscAmount0(shift.getItemDiscAmount0().add(this.getItemDiscAmount0()));
        shift.setItemDiscAmount1(shift.getItemDiscAmount1().add(this.getItemDiscAmount1()));
        shift.setItemDiscAmount2(shift.getItemDiscAmount2().add(this.getItemDiscAmount2()));
        shift.setItemDiscAmount3(shift.getItemDiscAmount3().add(this.getItemDiscAmount3()));
        shift.setItemDiscAmount4(shift.getItemDiscAmount4().add(this.getItemDiscAmount4()));
        
        z.setItemDiscAmount(z.getItemDiscAmount().add(this.getItemDiscAmount()));
        z.setItemDiscAmount0(z.getItemDiscAmount0().add(this.getItemDiscAmount0()));
        z.setItemDiscAmount1(z.getItemDiscAmount1().add(this.getItemDiscAmount1()));
        z.setItemDiscAmount2(z.getItemDiscAmount2().add(this.getItemDiscAmount2()));
        z.setItemDiscAmount3(z.getItemDiscAmount3().add(this.getItemDiscAmount3()));
        z.setItemDiscAmount4(z.getItemDiscAmount4().add(this.getItemDiscAmount4()));
        
        //gllg
        shift.setItemDiscCnt(shift.getItemDiscCnt().intValue() + this.getItemDiscCnt().intValue());
        shift.setItemDiscCnt0(shift.getItemDiscCnt0().intValue() + this.getItemDiscCnt0().intValue());
        shift.setItemDiscCnt1(shift.getItemDiscCnt1().intValue() + this.getItemDiscCnt1().intValue());
        shift.setItemDiscCnt2(shift.getItemDiscCnt2().intValue() + this.getItemDiscCnt2().intValue());
        shift.setItemDiscCnt3(shift.getItemDiscCnt3().intValue() + this.getItemDiscCnt3().intValue());
        shift.setItemDiscCnt4(shift.getItemDiscCnt4().intValue() + this.getItemDiscCnt4().intValue());

        //gllg
        z.setItemDiscCnt(z.getItemDiscCnt().intValue() + this.getItemDiscCnt().intValue());
        z.setItemDiscCnt0(z.getItemDiscCnt0().intValue() + this.getItemDiscCnt0().intValue());
        z.setItemDiscCnt1(z.getItemDiscCnt1().intValue() + this.getItemDiscCnt1().intValue());
        z.setItemDiscCnt2(z.getItemDiscCnt2().intValue() + this.getItemDiscCnt2().intValue());
        z.setItemDiscCnt3(z.getItemDiscCnt3().intValue() + this.getItemDiscCnt3().intValue());
        z.setItemDiscCnt4(z.getItemDiscCnt4().intValue() + this.getItemDiscCnt4().intValue());
        
        
        if (shift.getCustomerCount() == null)
            shift.setCustomerCount(integer0);
        if (z.getCustomerCount() == null)
            z.setCustomerCount(integer0);
        in = new Integer(shift.getCustomerCount().intValue()
                + getCustomerCount().intValue());
        shift.setCustomerCount(in);
        in = new Integer(z.getCustomerCount().intValue()
                + getCustomerCount().intValue());
        z.setCustomerCount(in);
        in = new Integer(shift.getTransactionCount().intValue() + 1);
        shift.setTransactionCount(in);
        in = new Integer(z.getTransactionCount().intValue() + 1);
        z.setTransactionCount(in);

        //Bruce/20080603 目前无使用
        //if (getDealType2().equals("F")) {
        //    shift.setVoucherAmount(shift.getVoucherAmount().add(
        //            getSalesAmount()));
        //    shift.setVoucherCount(new Integer(shift.getVoucherCount()
        //            .intValue() + 1));
        //    z.setVoucherAmount(z.getVoucherAmount().add(getSalesAmount()));
        //    z.setVoucherCount(new Integer(z.getVoucherCount().intValue() + 1));
        //}

        getLineItemsArrayLast();
        Iterator lineItems = getLineItems();
        LineItem lineItem = null;
        Integer zNumber = getZSequenceNumber();
        while (lineItems.hasNext()) {
            lineItem = (LineItem) lineItems.next();
            DepSales dps = null;
            String pluno = lineItem.getPluNumber();
            if (pluno != null && !pluno.equals("")) {
                PLU plu = PLU.queryByPluNumber(pluno);
                if (plu != null) {
                    String depID = plu.getDepID();
                    if (depID != null && !depID.equals(""))
                        dps = DepSales.queryByDepID(zNumber, depID);
                }
            } else if (pluno != null && pluno.equals("")) {
                String depID = lineItem.getMicroCategoryNumber();
                if (depID != null && !depID.equals(""))
                    dps = DepSales.queryByDepID(zNumber, depID);
            }
            if (lineItem.getDetailCode().equals("V")) {
                if (lineItem.getQuantity() == null
                        || lineItem.getQuantity().compareTo(bigDecimal0) == 0)
                    if (getDealType1().equals("0")
                            && getDealType2().equals("0")
                            && !getDealType3().equals("4")
                            && !getDealType3().equals("0")
                            || getDealType1().equals("0")
                            && getDealType2().equals("3")
                            && getDealType3().equals("4"))
                        lineItem.setQuantity(ONE);
                    else
                        lineItem.setQuantity(MINUS_ONE);
                z.setLineVoidAmount(z.getLineVoidAmount().subtract(
                        lineItem.getAmount()));
                z.setLineVoidCount(new Integer(z.getLineVoidCount().intValue()
                        - lineItem.getQuantity().signum()));
                shift.setLineVoidAmount(shift.getLineVoidAmount().subtract(
                        lineItem.getAmount()));
                shift.setLineVoidCount(new Integer(shift.getLineVoidCount()
                        .intValue()
                        - lineItem.getQuantity().signum()));
            } else if (lineItem.getDetailCode().equals("E")) {
                if (lineItem.getQuantity() == null
                        || lineItem.getQuantity().compareTo(bigDecimal0) == 0)
                    if (getDealType1().equals("0")
                            && getDealType2().equals("0")
                            && !getDealType3().equals("4")
                            && !getDealType3().equals("0")
                            || getDealType1().equals("0")
                            && getDealType2().equals("3")
                            && getDealType3().equals("4"))
                        lineItem.setQuantity(ONE);
                    else
                        lineItem.setQuantity(MINUS_ONE);
                z.setVoidAmount(z.getVoidAmount()
                        .subtract(lineItem.getAmount()));
                z.setVoidCount(new Integer(z.getVoidCount().intValue()
                        - lineItem.getQuantity().signum()));
                shift.setVoidAmount(shift.getVoidAmount().subtract(
                        lineItem.getAmount()));
                shift.setVoidCount(new Integer(shift.getVoidCount().intValue()
                        - lineItem.getQuantity().signum()));
            }
            if (lineItem.getRemoved())
                continue;
            
//            //Bruce/20080603/ 统计变价金额和次数，暂放在VoucherAmount栏位
//            if ("O".equals(lineItem.getDiscountType())) {
//                // 加上：originalPrice * quantity - afterDiscountAmount
//                shift.setVoucherAmount(shift.getVoucherAmount().add(
//                    lineItem.getOriginalPrice().multiply(lineItem.getQuantity())
//                    .subtract(lineItem.getAfterDiscountAmount())
//                ));
//                if (shift.getVoucherAmount().compareTo(ZERO) > 0)
//                    shift.setVoucherCount(new Integer(shift.getVoucherCount().intValue() + 1));
//                else
//                    shift.setVoucherCount(new Integer(shift.getVoucherCount().intValue() - 1));
//
//                z.setVoucherAmount(z.getVoucherAmount().add(
//                    lineItem.getOriginalPrice().multiply(lineItem.getQuantity())
//                    .subtract(lineItem.getAfterDiscountAmount())
//                ));
//                if (z.getVoucherAmount().compareTo(ZERO) > 0)
//                    z.setVoucherCount(new Integer(z.getVoucherCount().intValue() + 1));
//                else
//                    z.setVoucherCount(new Integer(z.getVoucherCount().intValue() - 1));
//            }

            if (!lineItem.getDetailCode().equals("M")
                    && !lineItem.getDetailCode().equals("D")
                    && !lineItem.getDetailCode().equals("I")
                    && !lineItem.getDetailCode().equals("O")
                    && !lineItem.getDetailCode().equals("Q")
                    && !lineItem.getDetailCode().equals("N")
                    && !lineItem.getDetailCode().equals("T")) {
                z.setItemCountGrandTotal(z.getItemCountGrandTotal().add(
                        lineItem.getQuantity()));
                shift.setItemCount(shift.getItemCount().add(
                        lineItem.getQuantity()));
                z.setItemCount(z.getItemCount().add(lineItem.getQuantity()));
                if (dps != null) {
                    dps.setGrossSaleTotalAmount(dps.getGrossSaleTotalAmount()
                            .add(lineItem.getAmount()));
                    dps
                            .setMixAndMatchTotalAmount(dps
                                    .getMixAndMatchTotalAmount()
                                    .add(
                                            lineItem
                                                    .getAmount()
                                                    .subtract(
                                                            lineItem
                                                                    .getAfterDiscountAmount())));

                    if ("C".equals(lineItem.getDiscountType()))
                        dps.setDiscountTotalAmount(dps.getDiscountTotalAmount().add(lineItem.getItemDsctAmt()));
                        
                    dps.setNetSaleTotalAmount(dps.getNetSaleTotalAmount().add(
                            lineItem.getAfterDiscountAmount()));
                }
            }
            if (lineItem.getPluNumber() == null) {
                lineItem.setPluNumber("");
                lineItem.setItemNumber("");
            }
            if (lineItem.getPluNumber().equals("")
                    && (lineItem.getDetailCode().equals("S") || lineItem
                            .getDetailCode().equals("R"))) {
                shift.setManualEntryAmount(shift.getManualEntryAmount().add(
                        lineItem.getAmount()));
                z.setManualEntryAmount(z.getManualEntryAmount().add(
                        lineItem.getAmount()));
                if (getDealType1().equals("0") && getDealType2().equals("0")
                        && !getDealType3().equals("4")
                        && !getDealType3().equals("0")
                        || getDealType1().equals("0")
                        && getDealType2().equals("3")
                        && getDealType3().equals("4")) {
                    shift.setManualEntryCount(new Integer(shift
                            .getManualEntryCount().intValue() - 1));
                    z.setManualEntryCount(new Integer(z.getManualEntryCount()
                            .intValue() - 1));
                } else {
                    shift.setManualEntryCount(new Integer(shift
                            .getManualEntryCount().intValue() + 1));
                    z.setManualEntryCount(new Integer(z.getManualEntryCount()
                            .intValue() + 1));
                }
            }
            if (lineItem.isOpenPrice()) {
                z.setPriceOpenEntryAmount(z.getPriceOpenEntryAmount().add(
                        lineItem.getAmount()));
                shift.setPriceOpenEntryAmount(shift.getPriceOpenEntryAmount()
                        .add(lineItem.getAmount()));
                z.setPriceOpenEntryCount(new Integer(z.getPriceOpenEntryCount()
                        .intValue() + 1));
                shift.setPriceOpenEntryCount(new Integer(shift
                        .getPriceOpenEntryCount().intValue() + 1));
            }
            if (lineItem.getDetailCode().equals("I")) {
                String firstNumber = lineItem.getDiscountNumber();
                String secondNumber = "";
                String fileName = "daishou"
                        + Integer.valueOf(firstNumber).intValue() + ".conf";
                fileName = CreamToolkit.getConfigDir() + fileName;
                File propFile = new File(fileName);
                ArrayList menuArrayList = new ArrayList();
                try {
                    FileInputStream filein = new FileInputStream(propFile);
                    InputStreamReader inst = null;
                    if (CreamProperties.getInstance().getProperty("Locale")
                            .equals("zh_TW"))
                        inst = new InputStreamReader(filein, "BIG5");
                    else if (CreamProperties.getInstance()
                            .getProperty("Locale").equals("zh_CN"))
                        inst = new InputStreamReader(filein, "GBK");
                    BufferedReader inn = new BufferedReader(inst);
                    char ch = ' ';
                    do {
                        String line = inn.readLine();
                        if (line != null) {
                            for (; line.equals(""); line = inn.readLine())
                                ;
                            int i = 0;
                            do {
                                ch = line.charAt(i);
                                i++;
                            } while ((ch == ' ' || ch == '\t')
                                    && i < line.length());
                            if (ch == '#' || ch == ' ' || ch == '\t')
                                continue;
                        }
                        if (line == null)
                            break;
                        menuArrayList.add(line);
                    } while (true);
                } catch (FileNotFoundException e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                    continue;
                } catch (IOException e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                    continue;
                }
                for (int j = 0; j < menuArrayList.size(); j++) {
                    String line = (String) menuArrayList.get(j);
                    StringTokenizer t = new StringTokenizer(line, ",.");
                    String key2 = t.nextToken();
                    t.nextToken();
                    String plunumber = t.nextToken();
                    if (!plunumber.equalsIgnoreCase(lineItem.getPluNumber()))
                        continue;
                    secondNumber = key2;
                    break;
                }

                DaishouSales daishouSales = DaishouSales
                        .queryByFirstNumberAndSecondNumber(getZSequenceNumber()
                                .toString(), firstNumber, secondNumber);
                if (daishouSales != null) {
                    daishouSales.setPosAmount(daishouSales.getPosAmount().add(
                            lineItem.getAmount()));
                    daishouSales.setPosCount(daishouSales.getPosCount()
                            + lineItem.getQuantity().intValue());
                    daishouSales.update();
                }
                Iterator iter = Reason.queryByreasonCategory("08");
                String id = "";
                Reason r = null;
                int index = 1;
                BigDecimal amount = null;
                String fieldName1 = "DAISHOUPLUAMT";
                String fieldName2 = "DAISHOUPLUCNT";
                String fieldName3 = "DAISHOUAMT";
                String fieldName4 = "DAISHOUCNT";
                if (iter != null) {
                    if (shift.getFieldValue(fieldName3) == null)
                        shift.setFieldValue(fieldName3, bigDecimal0);
                    if (shift.getFieldValue(fieldName4) == null)
                        shift.setFieldValue(fieldName4, integer0);
                    if (z.getFieldValue(fieldName3) == null)
                        z.setFieldValue(fieldName3, bigDecimal0);
                    if (z.getFieldValue(fieldName4) == null)
                        z.setFieldValue(fieldName4, integer0);
                    for (; iter.hasNext(); index++) {
                        r = (Reason) iter.next();
                        id = r.getreasonNumber();
                        if (Integer.parseInt(lineItem.getDiscountNumber()) != Integer
                                .parseInt(id))
                            continue;
                        amount = lineItem.getAmount();
                        fieldName1 = fieldName1 + index;
                        fieldName2 = fieldName2 + index;
                        if (shift.getFieldValue(fieldName1) == null)
                            shift.setFieldValue(fieldName1, bigDecimal0);
                        if (shift.getFieldValue(fieldName2) == null)
                            shift.setFieldValue(fieldName2, integer0);
                        if (z.getFieldValue(fieldName1) == null)
                            z.setFieldValue(fieldName1, bigDecimal0);
                        if (z.getFieldValue(fieldName2) == null)
                            z.setFieldValue(fieldName2, integer0);
                        shift.setFieldValue(fieldName1, ((BigDecimal) shift
                                .getFieldValue(fieldName1)).add(amount));
                        shift.setFieldValue(fieldName3, ((BigDecimal) shift
                                .getFieldValue(fieldName3)).add(amount));
                        z.setFieldValue(fieldName1, ((BigDecimal) z
                                .getFieldValue(fieldName1)).add(amount));
                        z.setFieldValue(fieldName3, ((BigDecimal) z
                                .getFieldValue(fieldName3)).add(amount));
                        if (getDealType1().equals("0")
                                && getDealType2().equals("0")
                                && (getDealType3().equals("1")
                                        || getDealType3().equals("2") || getDealType3()
                                        .equals("3"))
                                || getDealType1().equals("0")
                                && getDealType2().equals("3")
                                && getDealType3().equals("4")) {
                            shift.setFieldValue(fieldName2, new Integer(
                                    ((Integer) shift.getFieldValue(fieldName2))
                                            .intValue() - 1));
                            shift.setFieldValue(fieldName4, new Integer(
                                    ((Integer) shift.getFieldValue(fieldName4))
                                            .intValue() - 1));
                            z.setFieldValue(fieldName2, new Integer(
                                    ((Integer) z.getFieldValue(fieldName2))
                                            .intValue() - 1));
                            z.setFieldValue(fieldName4, new Integer(
                                    ((Integer) z.getFieldValue(fieldName4))
                                            .intValue() - 1));
                        } else {
                            shift.setFieldValue(fieldName2, new Integer(
                                    ((Integer) shift.getFieldValue(fieldName2))
                                            .intValue() + 1));
                            shift.setFieldValue(fieldName4, new Integer(
                                    ((Integer) shift.getFieldValue(fieldName4))
                                            .intValue() + 1));
                            z.setFieldValue(fieldName2, new Integer(
                                    ((Integer) z.getFieldValue(fieldName2))
                                            .intValue() + 1));
                            z.setFieldValue(fieldName4, new Integer(
                                    ((Integer) z.getFieldValue(fieldName4))
                                            .intValue() + 1));
                        }
                        break;
                    }

                }
            } else if (lineItem.getDetailCode().equalsIgnoreCase("O")) {
                if (shift.getDaiShouAmount2() == null)
                    shift.setDaiShouAmount2(ZERO);
                if (shift.getDaiShouCount2() == null)
                    shift.setDaiShouCount2(new Integer(0));
                if (z.getDaiShouAmount2() == null)
                    z.setDaiShouAmount2(ZERO);
                if (z.getDaiShouCount2() == null)
                    z.setDaiShouCount2(new Integer(0));
                BigDecimal tmpAmount = lineItem.getAmount();
                shift.setDaiShouAmount2(shift.getDaiShouAmount2()
                        .add(tmpAmount));
                z.setDaiShouAmount2(z.getDaiShouAmount2().add(tmpAmount));
                tmpAmount = null;
                if (getDealType1().equals("0")
                        && getDealType2().equals("0")
                        && (getDealType3().equals("1")
                                || getDealType3().equals("2") || getDealType3()
                                .equals("3")) || getDealType1().equals("0")
                        && getDealType2().equals("3")
                        && getDealType3().equals("4")) {
                    shift.setDaiShouCount2(new Integer(shift.getDaiShouCount2()
                            .intValue() - 1));
                    z.setDaiShouCount2(new Integer(z.getDaiShouCount2()
                            .intValue() - 1));
                } else {
                    shift.setDaiShouCount2(new Integer(shift.getDaiShouCount2()
                            .intValue() + 1));
                    z.setDaiShouCount2(new Integer(z.getDaiShouCount2()
                            .intValue() + 1));
                }
            } else if (lineItem.getDetailCode().equalsIgnoreCase("Q")) {
                Iterator iter = Reason.queryByreasonCategory("09");
                Reason r = null;
                String daifuID = "";
                int daifuIndex = 1;
                BigDecimal daifuAmount = null;
                String fieldName1 = "DAIFUPLUAMT";
                String fieldName2 = "DAIFUPLUCNT";
                String fieldName3 = "DAIFUAMT";
                String fieldName4 = "DAIFUCNT";
                if (iter != null)
                    for (; iter.hasNext(); daifuIndex++) {
                        r = (Reason) iter.next();
                        daifuID = r.getreasonNumber();
                        if (!lineItem.getPluNumber().equals(daifuID))
                            continue;
                        daifuAmount = lineItem.getUnitPrice().negate();
                        fieldName1 = fieldName1 + daifuIndex;
                        fieldName2 = fieldName2 + daifuIndex;
                        shift.setFieldValue(fieldName1, ((BigDecimal) shift
                                .getFieldValue(fieldName1)).add(daifuAmount));
                        shift.setFieldValue(fieldName3, ((BigDecimal) shift
                                .getFieldValue(fieldName3)).add(daifuAmount));
                        z.setFieldValue(fieldName1, ((BigDecimal) z
                                .getFieldValue(fieldName1)).add(daifuAmount));
                        z.setFieldValue(fieldName3, ((BigDecimal) z
                                .getFieldValue(fieldName3)).add(daifuAmount));
                        if (getDealType1().equals("0")
                                && getDealType2().equals("0")
                                && (getDealType3().equals("1")
                                        || getDealType3().equals("2") || getDealType3()
                                        .equals("3"))) {
                            shift.setFieldValue(fieldName2, new Integer(
                                    ((Integer) shift.getFieldValue(fieldName2))
                                            .intValue() - 1));
                            shift.setFieldValue(fieldName4, new Integer(
                                    ((Integer) shift.getFieldValue(fieldName4))
                                            .intValue() - 1));
                            z.setFieldValue(fieldName2, new Integer(
                                    ((Integer) z.getFieldValue(fieldName2))
                                            .intValue() - 1));
                            z.setFieldValue(fieldName4, new Integer(
                                    ((Integer) z.getFieldValue(fieldName4))
                                            .intValue() - 1));
                        } else {
                            shift.setFieldValue(fieldName2, new Integer(
                                    ((Integer) shift.getFieldValue(fieldName2))
                                            .intValue() + 1));
                            shift.setFieldValue(fieldName4, new Integer(
                                    ((Integer) shift.getFieldValue(fieldName4))
                                            .intValue() + 1));
                            z.setFieldValue(fieldName2, new Integer(
                                    ((Integer) z.getFieldValue(fieldName2))
                                            .intValue() + 1));
                            z.setFieldValue(fieldName4, new Integer(
                                    ((Integer) z.getFieldValue(fieldName4))
                                            .intValue() + 1));
                        }
                        break;
                    }

            } else {
                if (getDealType2().equals("H")) {
                    shift.setCashInAmount(shift.getCashInAmount().add(
                            lineItem.getUnitPrice()));
                    shift.setCashInCount(new Integer(shift.getCashInCount()
                            .intValue() + 1));
                    z.setCashInAmount(z.getCashInAmount().add(
                            lineItem.getUnitPrice()));
                    z.setCashInCount(new Integer(
                            z.getCashInCount().intValue() + 1));
                    if (!ischeck) {
                        shift.update();
                        z.update();
                    }
                    return;
                }
                if (getDealType2().equals("G")) {
                    shift.setCashOutAmount(shift.getCashOutAmount().add(
                            lineItem.getUnitPrice()));
                    shift.setCashOutCount(new Integer(shift.getCashOutCount()
                            .intValue() + 1));
                    z.setCashOutAmount(z.getCashOutAmount().add(
                            lineItem.getUnitPrice()));
                    z.setCashOutCount(new Integer(z.getCashOutCount()
                            .intValue() + 1));
                    if (!ischeck) {
                        shift.update();
                        z.update();
                    }
                    return;
                }
                if (lineItem.getDetailCode().equalsIgnoreCase("T")) {
                    Iterator iter = Reason.queryByreasonCategory("11");
                    Reason r = null;
                    String id = "";
                    int index = 1;
                    BigDecimal amount = null;
                    String fieldName1 = "POUTPLUAMT";
                    String fieldName2 = "POUTPLUCNT";
                    String fieldName3 = "POUTAMT";
                    String fieldName4 = "POUTCNT";
                    if (iter != null) {
                        if (shift.getFieldValue(fieldName3) == null)
                            shift.setFieldValue(fieldName3, bigDecimal0);
                        if (shift.getFieldValue(fieldName4) == null)
                            shift.setFieldValue(fieldName4, integer0);
                        if (z.getFieldValue(fieldName3) == null)
                            z.setFieldValue(fieldName3, bigDecimal0);
                        if (z.getFieldValue(fieldName4) == null)
                            z.setFieldValue(fieldName4, integer0);
                        for (; iter.hasNext(); index++) {
                            r = (Reason) iter.next();
                            id = r.getreasonNumber();
                            if (!lineItem.getPluNumber().equals(id))
                                continue;
                            amount = lineItem.getUnitPrice().negate();
                            fieldName1 = fieldName1 + index;
                            fieldName2 = fieldName2 + index;
                            if (shift.getFieldValue(fieldName1) == null)
                                shift.setFieldValue(fieldName1, bigDecimal0);
                            if (shift.getFieldValue(fieldName2) == null)
                                shift.setFieldValue(fieldName2, integer0);
                            if (z.getFieldValue(fieldName1) == null)
                                z.setFieldValue(fieldName1, bigDecimal0);
                            if (z.getFieldValue(fieldName2) == null)
                                z.setFieldValue(fieldName2, integer0);
                            shift.setFieldValue(fieldName1, ((BigDecimal) shift
                                    .getFieldValue(fieldName1)).add(amount));
                            shift.setFieldValue(fieldName3, ((BigDecimal) shift
                                    .getFieldValue(fieldName3)).add(amount));
                            z.setFieldValue(fieldName1, ((BigDecimal) z
                                    .getFieldValue(fieldName1)).add(amount));
                            z.setFieldValue(fieldName3, ((BigDecimal) z
                                    .getFieldValue(fieldName3)).add(amount));
                            if (getDealType1().equals("0")
                                    && getDealType2().equals("0")
                                    && (getDealType3().equals("1")
                                            || getDealType3().equals("2") || getDealType3()
                                            .equals("3"))) {
                                shift.setFieldValue(fieldName2, new Integer(
                                        ((Integer) shift
                                                .getFieldValue(fieldName2))
                                                .intValue() - 1));
                                shift.setFieldValue(fieldName4, new Integer(
                                        ((Integer) shift
                                                .getFieldValue(fieldName4))
                                                .intValue() - 1));
                                z.setFieldValue(fieldName2, new Integer(
                                        ((Integer) z.getFieldValue(fieldName2))
                                                .intValue() - 1));
                                z.setFieldValue(fieldName4, new Integer(
                                        ((Integer) z.getFieldValue(fieldName4))
                                                .intValue() - 1));
                            } else {
                                shift.setFieldValue(fieldName2, new Integer(
                                        ((Integer) shift
                                                .getFieldValue(fieldName2))
                                                .intValue() + 1));
                                shift.setFieldValue(fieldName4, new Integer(
                                        ((Integer) shift
                                                .getFieldValue(fieldName4))
                                                .intValue() + 1));
                                z.setFieldValue(fieldName2, new Integer(
                                        ((Integer) z.getFieldValue(fieldName2))
                                                .intValue() + 1));
                                z.setFieldValue(fieldName4, new Integer(
                                        ((Integer) z.getFieldValue(fieldName4))
                                                .intValue() + 1));
                            }
                            break;
                        }

                    }
                } else if (lineItem.getDetailCode().equalsIgnoreCase("N")) {
                    Iterator iter = Reason.queryByreasonCategory("10");
                    Reason r = null;
                    String id = "";
                    int index = 1;
                    BigDecimal amount = null;
                    String fieldName1 = "PINPLUAMT";
                    String fieldName2 = "PINPLUCNT";
                    String fieldName3 = "PINAMT";
                    String fieldName4 = "PINCNT";
                    if (iter != null) {
                        if (shift.getFieldValue(fieldName3) == null)
                            shift.setFieldValue(fieldName3, bigDecimal0);
                        if (shift.getFieldValue(fieldName4) == null)
                            shift.setFieldValue(fieldName4, integer0);
                        if (z.getFieldValue(fieldName3) == null)
                            z.setFieldValue(fieldName3, bigDecimal0);
                        if (z.getFieldValue(fieldName4) == null)
                            z.setFieldValue(fieldName4, integer0);
                        for (; iter.hasNext(); index++) {
                            r = (Reason) iter.next();
                            id = r.getreasonNumber();
                            if (!lineItem.getPluNumber().equals(id))
                                continue;
                            amount = lineItem.getUnitPrice();
                            fieldName1 = fieldName1 + index;
                            fieldName2 = fieldName2 + index;
                            if (shift.getFieldValue(fieldName1) == null)
                                shift.setFieldValue(fieldName1, bigDecimal0);
                            if (shift.getFieldValue(fieldName2) == null)
                                shift.setFieldValue(fieldName2, integer0);
                            if (z.getFieldValue(fieldName1) == null)
                                z.setFieldValue(fieldName1, bigDecimal0);
                            if (z.getFieldValue(fieldName2) == null)
                                z.setFieldValue(fieldName2, integer0);
                            shift.setFieldValue(fieldName1, ((BigDecimal) shift
                                    .getFieldValue(fieldName1)).add(amount));
                            shift.setFieldValue(fieldName3, ((BigDecimal) shift
                                    .getFieldValue(fieldName3)).add(amount));
                            z.setFieldValue(fieldName1, ((BigDecimal) z
                                    .getFieldValue(fieldName1)).add(amount));
                            z.setFieldValue(fieldName3, ((BigDecimal) z
                                    .getFieldValue(fieldName3)).add(amount));
                            if (getDealType1().equals("0")
                                    && getDealType2().equals("0")
                                    && (getDealType3().equals("1")
                                            || getDealType3().equals("2") || getDealType3()
                                            .equals("3"))) {
                                shift.setFieldValue(fieldName2, new Integer(
                                        ((Integer) shift
                                                .getFieldValue(fieldName2))
                                                .intValue() - 1));
                                shift.setFieldValue(fieldName4, new Integer(
                                        ((Integer) shift
                                                .getFieldValue(fieldName4))
                                                .intValue() - 1));
                                z.setFieldValue(fieldName2, new Integer(
                                        ((Integer) z.getFieldValue(fieldName2))
                                                .intValue() - 1));
                                z.setFieldValue(fieldName4, new Integer(
                                        ((Integer) z.getFieldValue(fieldName4))
                                                .intValue() - 1));
                            } else {
                                shift.setFieldValue(fieldName2, new Integer(
                                        ((Integer) shift
                                                .getFieldValue(fieldName2))
                                                .intValue() + 1));
                                shift.setFieldValue(fieldName4, new Integer(
                                        ((Integer) shift
                                                .getFieldValue(fieldName4))
                                                .intValue() + 1));
                                z.setFieldValue(fieldName2, new Integer(
                                        ((Integer) z.getFieldValue(fieldName2))
                                                .intValue() + 1));
                                z.setFieldValue(fieldName4, new Integer(
                                        ((Integer) z.getFieldValue(fieldName4))
                                                .intValue() + 1));
                            }
                            break;
                        }

                    }
                }
            }
        }
        if (getSpillAmount().doubleValue() != 0.0D) {
            int integer = 0;
            shift.setSpillAmount(shift.getSpillAmount().add(getSpillAmount()));
            if (getSpillAmount().compareTo(bigDecimal0) == 1)
                integer = shift.getSpillCount().intValue() + 1;
            else
                integer = shift.getSpillCount().intValue() - 1;
            shift.setSpillCount(new Integer(integer));
            z.setSpillAmount(z.getSpillAmount().add(getSpillAmount()));
            if (getSpillAmount().compareTo(bigDecimal0) == 1)
                integer = z.getSpillCount().intValue() + 1;
            else
                integer = z.getSpillCount().intValue() - 1;
            z.setSpillCount(new Integer(integer));
        }
        BigDecimal currentAmount = getPayAmount1();
        if (currentAmount != null) {
            String payID = getPayNumber1();
            if (!payID.equals("")) {
                currentAmount = shift.getPayAmountByID(payID)
                        .add(currentAmount);
                shift.setPayAmountByID(payID, currentAmount);
                currentAmount = getPayAmount1();
                currentAmount = z.getPayAmountByID(payID).add(currentAmount);
                z.setPayAmountByID(payID, currentAmount);
                int currentPayCount = shift.getPayCountByID(payID).intValue();
                int currentPayCount2 = z.getPayCountByID(payID).intValue();
                currentAmount = getPayAmount1();
                if (currentAmount.compareTo(bigDecimal0) == 1
                        && getDaiFuAmount().compareTo(bigDecimal0) == 0
                        || currentAmount.compareTo(bigDecimal0) == -1
                        && getDaiFuAmount().compareTo(bigDecimal0) == -1) {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount + 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 + 1));
                } else {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount - 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 - 1));
                }
            }
        }
        currentAmount = getPayAmount2();
        if (currentAmount != null) {
            String payID = getPayNumber2();
            if (!payID.equals("")) {
                currentAmount = shift.getPayAmountByID(payID)
                        .add(currentAmount);
                shift.setPayAmountByID(payID, currentAmount);
                currentAmount = getPayAmount2();
                currentAmount = z.getPayAmountByID(payID).add(currentAmount);
                z.setPayAmountByID(payID, currentAmount);
                int currentPayCount = shift.getPayCountByID(payID).intValue();
                int currentPayCount2 = z.getPayCountByID(payID).intValue();
                currentAmount = getPayAmount1();
                if (currentAmount.compareTo(bigDecimal0) == 1
                        && getDaiFuAmount().compareTo(bigDecimal0) == 0
                        || currentAmount.compareTo(bigDecimal0) == -1
                        && getDaiFuAmount().compareTo(bigDecimal0) == -1) {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount + 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 + 1));
                } else {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount - 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 - 1));
                }
            }
        }
        currentAmount = getPayAmount3();
        if (currentAmount != null) {
            String payID = getPayNumber3();
            if (!payID.equals("")) {
                currentAmount = shift.getPayAmountByID(payID)
                        .add(currentAmount);
                shift.setPayAmountByID(payID, currentAmount);
                currentAmount = getPayAmount3();
                currentAmount = z.getPayAmountByID(payID).add(currentAmount);
                z.setPayAmountByID(payID, currentAmount);
                int currentPayCount = shift.getPayCountByID(payID).intValue();
                int currentPayCount2 = z.getPayCountByID(payID).intValue();
                currentAmount = getPayAmount1();
                if (currentAmount.compareTo(bigDecimal0) == 1
                        && getDaiFuAmount().compareTo(bigDecimal0) == 0
                        || currentAmount.compareTo(bigDecimal0) == -1
                        && getDaiFuAmount().compareTo(bigDecimal0) == -1) {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount + 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 + 1));
                } else {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount - 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 - 1));
                }
            }
        }
        currentAmount = getPayAmount4();
        if (currentAmount != null) {
            String payID = getPayNumber4();
            if (!payID.equals("")) {
                currentAmount = shift.getPayAmountByID(payID)
                        .add(currentAmount);
                shift.setPayAmountByID(payID, currentAmount);
                currentAmount = getPayAmount4();
                currentAmount = z.getPayAmountByID(payID).add(currentAmount);
                z.setPayAmountByID(payID, currentAmount);
                int currentPayCount = shift.getPayCountByID(payID).intValue();
                int currentPayCount2 = z.getPayCountByID(payID).intValue();
                currentAmount = getPayAmount1();
                if (currentAmount.compareTo(bigDecimal0) == 1
                        && getDaiFuAmount().compareTo(bigDecimal0) == 0
                        || currentAmount.compareTo(bigDecimal0) == -1
                        && getDaiFuAmount().compareTo(bigDecimal0) == -1) {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount + 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 + 1));
                } else {
                    shift.setPayCountByID(payID, new Integer(
                            currentPayCount - 1));
                    z.setPayCountByID(payID, new Integer(currentPayCount2 - 1));
                }
            }
        }
        if (shift.getPay00Amount() != null && z.getPay00Amount() != null
                && getChangeAmount() != null) {
            shift.setPay00Amount(shift.getPay00Amount().subtract(
                    getChangeAmount()));
            z.setPay00Amount(z.getPay00Amount().subtract(getChangeAmount()));
        }
        if (!ischeck) {
            shift.update();
            z.update();
        }
    }

    private void rollBack() {
        int len = getLineItemsArrayLast().length;
        for (int i = 0; i < len; i++) {
            LineItem li = (LineItem) lineItemArrayLast.get(i);
            if (li.isCommit())
                li.deleteByPrimaryKey();
        }

        DaiShouSales2 dss;
        for (Iterator iter = daiShouSalesCommitted.iterator(); iter.hasNext(); dss
                .deleteByPrimaryKey())
            dss = (DaiShouSales2) iter.next();

        if (isCommit())
            deleteByPrimaryKey();
        CreamToolkit.writeTransactionLog("# Failed, rollback");
        CreamProperties prop = CreamProperties.getInstance();
        int nextTransactionNumber = getTransactionNumber().intValue() + 1;
        String nextTranNoOrig = prop.getProperty("NextTransactionNumber", "1");
        if (Integer.parseInt(nextTranNoOrig) < nextTransactionNumber) {
            prop.setProperty("NextTransactionNumber", ""
                    + nextTransactionNumber);
            prop.deposit();
        }
        StateMachine.getInstance().setEventProcessEnabled(false);
        try {
            Thread.sleep(0x337f9800L);
        } catch (InterruptedException interruptedexception) {
        }
    }

    public synchronized void store() {
        daiShouSalesCommitted = new ArrayList();
        if (getTotalMMAmount().compareTo(bigDecimal0) != 0
                && !getDealType3().equals("3")) {
            Iterator mmKey = mmMap.keySet().iterator();
            String mmID = "";
            LineItem mmLineItem = null;
            while (mmKey.hasNext()) {
                mmID = (String) mmKey.next();
                mmLineItem = (LineItem) mmMap.get(mmID);
                try {
                    mmLineItem.setTransactionNumber(getTransactionNumber());
                    mmLineItem.setTerminalNumber(getTerminalNumber());
                    mmLineItem.setDetailCode("M");
                    mmLineItem.setDiscountType("M");
                    mmLineItem.setDiscountNumber(mmID);
                    mmLineItem.setRemoved(false);
                    mmLineItem.setUnitPrice(mmLineItem.getUnitPrice().negate());
                    mmLineItem.setOriginalPrice(mmLineItem.getUnitPrice()
                            .negate());
                    mmLineItem.setQuantity(mmLineItem.getQuantity());
                    mmLineItem.setAmount(mmLineItem.getAmount().negate());
                    setLockEnable(true);
                    addLineItem(mmLineItem);
                    getLineItemsArrayLast();
                } catch (TooManyLineItemsException e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }
            }
        }
        integrityCheck();
        BigDecimal taxAmount = null;
        setTaxAmount(bigDecimal0);
        setTaxAmount0(bigDecimal0);
        setTaxAmount1(bigDecimal0);
        setTaxAmount2(bigDecimal0);
        setTaxAmount3(bigDecimal0);
        setTaxAmount4(bigDecimal0);
        int len = getLineItemsArrayLast().length;
        for (int i = 0; i < len; i++) {
            LineItem li = (LineItem) lineItemArrayLast.get(i);
            li.setTransactionNumber(getTransactionNumber());
            if ((li.getDetailCode().equals("S")
                    || li.getDetailCode().equals("R")
                    || li.getDetailCode().equals("V") || li.getDetailCode()
                    .equals("E"))
                    && li.getDaishouNumber() == 0) {
                if ((li.getDiscountType() == null || li.getDiscountType()
                        .equals(""))
                        && (li.getAfterDiscountAmount() == null || li
                                .getUnitPrice().compareTo(
                                        li.getAfterDiscountAmount()) == 0))
                    li.setAfterDiscountAmount(li.getAmount());
                if (mixAndMatchVersion == null
                        || mixAndMatchVersion.trim().equals("")
                        || mixAndMatchVersion.trim().equals("1"))
                    li.updateMM();
                TaxType taxType = TaxType.queryByTaxID(li.getTaxType());
                if (taxType != null) {
                    String taxTp = taxType.getType();
                    int round = 0;
                    String taxRound = taxType.getRound();
                    if (taxRound.equalsIgnoreCase("R"))
                        round = 4;
                    else if (taxRound.equalsIgnoreCase("C"))
                        round = 0;
                    else if (taxRound.equalsIgnoreCase("D"))
                        round = 1;
                    else
                        round = 4;
                    BigDecimal taxPercent = taxType.getPercent();
                    int taxDigit = taxType.getDecimalDigit().intValue();
                    if (taxTp.equalsIgnoreCase("1")) {
                        BigDecimal unit = ONE;
//                        taxAmount = li.getAfterDiscountAmount().multiply(
//                                taxPercent).divide(unit.add(taxPercent), 2)
//                                .setScale(taxDigit, round);
                        //use ROUND_UP instead of ROUND_CEILING, there will be difference when doing normal sales and reprint.  gllg Oct 15, 2008
                        taxAmount = li.getAfterDiscountAmount().multiply(
                                taxPercent).divide(unit.add(taxPercent), BigDecimal.ROUND_UP)
                                .setScale(taxDigit, round);
                    } else if (taxTp.equalsIgnoreCase("2")) {
                        taxAmount = li.getAmount().multiply(taxPercent)
                                .setScale(taxDigit, round);
                    } else {
                        BigDecimal tax = ZERO;
                        taxAmount = tax.setScale(taxDigit, round);
                    }
                    li.setTaxAmount(taxAmount);
                    Class decimal[] = { java.math.BigDecimal.class };
                    Method getMethod = null;
                    Method setMethod = null;
                    Object ret = null;
                    String type = li.getTaxType();
                    try {
                        getMethod = hyi.cream.dac.Transaction.class
                                .getDeclaredMethod("getTaxAmount" + type,
                                        new Class[0]);
                        ret = getMethod.invoke(this, new Object[0]);
                        if (ret instanceof BigDecimal) {
                            BigDecimal amt = (BigDecimal) ret;
                            amt = amt.add(li.getTaxAmount());
                            setMethod = hyi.cream.dac.Transaction.class
                                    .getDeclaredMethod("setTaxAmount" + type,
                                            decimal);
                            Object obj[] = { amt };
                            setMethod.invoke(this, obj);
                        }
                        setTaxAmount(getTaxAmount().add(li.getTaxAmount()));
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                    } catch (InvocationTargetException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                    }
                }
            }
        }

        setSignOnNumber(Integer.parseInt(CreamProperties.getInstance()
                .getProperty("ShiftNumber")));
        setInvoiceNumber("0");
        setInOutStore("1");
        setDetailCount(getLineItemsArrayLast().length);
        setSystemDateTime(new Date());
        setCustomerCount(1);
        if (isPureDaiShou2())
            setCustomerCount(0);
        if (getDealType2().equals("4") || getDealType2().equals("8")
                || getDealType2().equals("9") || getDealType2().equals("A")
                || getDealType2().equals("B") || getDealType2().equals("C")
                || getDealType2().equals("D") || getDealType2().equals("G")
                || getDealType2().equals("H")
                || getDaiFuAmount().compareTo(bigDecimal0) == -1)
            setCustomerCount(0);
        if (getDealType1().equals("0")
                && getDealType2().equals("0")
                && (getDealType3().equals("1") || getDealType3().equals("2") || getDealType3()
                        .equals("3")))
            setCustomerCount(0 - getCustomerCount().intValue());
        else if (getDealType1().equals("0") && getDealType2().equals("3")
                && getDealType3().equals("4"))
            setCustomerCount(0 - getCustomerCount().intValue());
        writeTransactionLog();
        Integer tranNumber = getTransactionNumber();
        if (getAnnotatedType() != null && getAnnotatedType().equals("P")
                && !getDealType3().equals("4")) {
            CreamProperties.getInstance().setProperty("PeiDaNumber",
                    getAnnotatedId());
            CreamProperties.getInstance().deposit();
        }
        len = getLineItemsArrayLast().length;
        for (int i = 0; i < len; i++) {
            LineItem li = (LineItem) lineItemArrayLast.get(i);
            if (li.insert()) {
                li.setCommit(true);
                CreamToolkit.logMessage("LineItem(" + tranNumber + ") seq="
                        + li.getLineItemSequence() + ", plu="
                        + li.getPluNumber() + ", qty=" + li.getQuantity()
                        + ", amt=" + li.getAmount() + " is stored.");
                try {
                    DaiShouSales2 dss = writeDaiShouSales2(li, false);
                    if (dss != null)
                        daiShouSalesCommitted.add(dss);
                } catch (SQLException e1) {
                    rollBack();
                    return;
                }
            } else {
                POSTerminalApplication.getInstance().getWarningIndicator()
                        .setMessage(
                                CreamToolkit.GetResource().getString(
                                        "InsertFailed"));
                for (int j = 0; j < 10; j++)
                    java.awt.Toolkit.getDefaultToolkit().beep();

                CreamToolkit.logMessage("Err> LineItem(" + tranNumber
                        + ") seq=" + li.getLineItemSequence() + ", plu="
                        + li.getPluNumber() + ", qty=" + li.getQuantity()
                        + ", qmt=" + li.getAmount() + " insert failed.");
                rollBack();
                return;
            }
        }

        //gllg set item discount count
//        currentTransaction.setLineItemDsctCnt();
        
        if (insert(false)) {
            setCommit(true);
            CreamToolkit.logMessage("Transaction(" + tranNumber + ",z="
                    + getZSequenceNumber() + ") is stored.");
            CreamToolkit.logMessage("after stored getGrossSalesAmount() = "+getGrossSalesAmount()+"  getGrossSalesTax0Amount() ="+getGrossSalesTax0Amount()
            		+" getGrossSalesTax1Amount() ="+getGrossSalesTax1Amount()+" getGrossSalesTax2Amount() ="+getGrossSalesTax2Amount()+" getGrossSalesTax3Amount()="+
            		getGrossSalesTax3Amount()+" getGrossSalesTax4Amount()="+getGrossSalesTax4Amount());
            CreamToolkit.logMessage("after store getNetSalesAmount()="+getNetSalesAmount()+" getNetSalesAmount0()="+getNetSalesAmount0()+" getNetSalesAmount1()="+
            		getNetSalesAmount1()+" getNetSalesAmount2()="+getNetSalesAmount2()+" getNetSalesAmount3()="+getNetSalesAmount3()
            		+" getNetSalesAmount4()="+getNetSalesAmount4());
        } else {
            POSTerminalApplication.getInstance().getWarningIndicator()
                    .setMessage(
                            CreamToolkit.GetResource()
                                    .getString("InsertFailed"));
            rollBack();
            for (int j = 0; j < 10; j++)
                java.awt.Toolkit.getDefaultToolkit().beep();

            CreamToolkit.logMessage("Err> Transaction(" + tranNumber
                    + ") insert failed.");
            return;
        }
        CreamToolkit.logMessage("Updating Shift/Z(" + tranNumber + ")...");
        try {
            tran_Processing(false);
            CreamToolkit.logMessage("Updating Shift/Z(" + tranNumber + ") OK!");
            CreamToolkit.writeTransactionLog("# OK");
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
            rollBack();
            CreamToolkit.logMessage("Updating Shift/Z(" + tranNumber
                    + ") Failed!");
            return;
        }
        final int t = tranNumber.intValue();
        Runnable upload = new Runnable() {

            public void run() {
                try {
                    Client.getInstance().processCommand("putTransaction " + t);
                } catch (ClientCommandException e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }
            }

        };
        (new Thread(upload)).start();
        if (getDealType2().compareTo("9") == 0) {
            ShiftReport shift = ShiftReport.getCurrentShift();
            try {
                Client.getInstance().processCommand(
                        "putShift " + shift.getZSequenceNumber() + " "
                                + shift.getSequenceNumber());
                shift.setUploadState("1");
            } catch (ClientCommandException e) {
                e.printStackTrace(CreamToolkit.getLogger());
                shift.setUploadState("2");
            }
            if (!POSTerminalApplication.ischeck)
                shift.update();
            int currentShift = Integer.parseInt(CreamProperties.getInstance()
                    .getProperty("ShiftNumber"));
            CreamProperties.getInstance().setProperty("ShiftNumber",
                    String.valueOf(++currentShift));
            CreamProperties.getInstance().setProperty("CashierNumber", "");
            CreamProperties.getInstance().deposit();
            POSTerminalApplication.getInstance().setCurrentCashierNumber("");
        } else if (getDealType2().compareTo("D") == 0) {
            int currentZ = ZReport.getCurrentZNumber();
            CreamProperties.getInstance().setProperty("ZNumber",
                    String.valueOf(++currentZ));
            CreamProperties.getInstance().setProperty("ShiftNumber",
                    String.valueOf(0));
            CreamProperties.getInstance().deposit();
            boolean success = false;
            Iterator itrDepSales = DepSales
                    .queryBySequenceNumber(getZSequenceNumber());
            ZReport z = ZReport.getOrCreateCurrentZReport();
            CreamToolkit.logMessage("Upload Z report begin...");
            try {
                Client.getInstance().processCommand(
                        "putZ " + z.getSequenceNumber());
                success = true;
                z.setUploadState("1");
            } catch (ClientCommandException e) {
                success = false;
                CreamToolkit.logMessage("Upload Z failed!");
                e.printStackTrace(CreamToolkit.getLogger());
                z.setUploadState("2");
            }
            if (success) {
                Iterator itrDaiShou = DaishouSales
                        .getCurrentDaishouSales(getZSequenceNumber().intValue());
                if (itrDaiShou != null) {
                    try {
                        Client.getInstance().processCommand(
                                "putDaishouSales " + getZSequenceNumber());
                        success = true;
                    } catch (ClientCommandException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                        success = false;
                    }
                    while (itrDaiShou.hasNext()) {
                        DaishouSales daishouSales = (DaishouSales) itrDaiShou
                                .next();
                        if (success)
                            daishouSales.setTcpflg("1");
                        else
                            daishouSales.setTcpflg("2");
                        if (!POSTerminalApplication.ischeck)
                            daishouSales.update();
                    }
                }
                Iterator itrDaiShouSales2 = DaiShouSales2
                        .getCurrentDaishouSales(getZSequenceNumber().intValue());
                if (itrDaiShouSales2 != null)
                    try {
                        CreamToolkit
                                .logMessage("Upload DaiShouSales2 begin...");
                        Client.getInstance().processCommand(
                                "putDaiShouSales2 " + getZSequenceNumber());
                        CreamToolkit.logMessage("Upload DaiShouSales2 ok!");
                        success = true;
                    } catch (ClientCommandException e) {
                        success = false;
                        CreamToolkit.logMessage("Upload DaiShouSales2 failed!");
                        e.printStackTrace(CreamToolkit.getLogger());
                    }
                if (success) {
                    CreamToolkit.logMessage("Upload depsales");
                    try {
                        if (itrDepSales != null)
                            Client.getInstance().processCommand(
                                    "putDepSales "
                                            + getZSequenceNumber().intValue());
                        success = true;
                    } catch (ClientCommandException e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                        success = false;
                    }
                    while (itrDepSales != null && itrDepSales.hasNext()) {
                        DepSales d = (DepSales) itrDepSales.next();
                        if (success)
                            d.setUploadState("1");
                        else
                            d.setUploadState("2");
                    }
                } else {
                    DepSales d;
                    for (; itrDepSales != null && itrDepSales.hasNext(); d
                            .setUploadState("2"))
                        d = (DepSales) itrDepSales.next();

                    z.setUploadState("2");
                }
            }
            if (!POSTerminalApplication.ischeck) {
                DepSales d;
                for (; itrDepSales != null && itrDepSales.hasNext(); d.update())
                    d = (DepSales) itrDepSales.next();

                z.update();
            }
        }
        POSTerminalApplication.getInstance().setTransactionEnd(true);
    }

    private void writeTransactionLog() {
        int len = getLineItemsArrayLast().length;
        for (int i = 0; i < len; i++) {
            LineItem li = (LineItem) lineItemArrayLast.get(i);
            CreamToolkit.writeTransactionLog(li.getInsertSqlStatement(true));
            try {
                writeDaiShouSales2(li, true);
            } catch (SQLException sqlexception) {
            }
        }

        CreamToolkit.writeTransactionLog(getInsertSqlStatement(true));
    }

    private boolean integrityCheck() {
        boolean ret = true;
        int len = getLineItemsArrayLast().length;
        LineItem lastLineItem = null;
        for (int i = 0; i < len; i++) {
            LineItem li = (LineItem) lineItemArrayLast.get(i);
            if (li.getDetailCode().equals("E")
                    && lastLineItem != null
                    && (!li.getUnitPrice().equals(lastLineItem.getUnitPrice())
                            || !li.getQuantity().equals(
                                    lastLineItem.getQuantity().negate()) || !li
                            .getAmount().equals(
                                    lastLineItem.getAmount().negate()))) {
                lastLineItem.setUnitPrice(li.getUnitPrice());
                lastLineItem.setQuantity(li.getQuantity().negate());
                lastLineItem.setAmount(li.getAmount().negate());
                lastLineItem.setAfterDiscountAmount(li.getAfterDiscountAmount()
                        .negate());
                lastLineItem.setOriginalPrice(li.getOriginalPrice());
                CreamToolkit.logMessage("LineItem Adjust> "
                        + lastLineItem.toString());
                System.out.println("LineItem Adjust> "
                        + lastLineItem.toString());
                ret = false;
            }
            lastLineItem = li;
        }

        if (getPayAmount1() != null
                && (getPayAmount1().compareTo(new BigDecimal("999999999.99")) >= 0 || getPayAmount1()
                        .compareTo(new BigDecimal("-999999999.99")) <= 0)) {
            setPayAmount1(getNetSalesAmount());
            setChangeAmount(bigDecimal0);
            CreamToolkit.logMessage("Transaction Adjust> " + toString());
            System.out.println("Transaction Adjust> " + toString());
            ret = false;
        }
        return ret;
    }

    private boolean backUp() {
        boolean windows = System.getProperties().get("os.name").toString()
                .indexOf("Windows") != -1;
        if (windows)
            return windows;
        else
            return Trigger.getInstance().backUp();
    }

    private DaiShouSales2 writeDaiShouSales2(LineItem lineItem,
            boolean writeTranLog) throws SQLException {
        DaiShouSales2 dss = null;
        if (lineItem.getDetailCode().equals("O") && !lineItem.getRemoved())
            try {
                dss = new DaiShouSales2();
                boolean isNormalTran = getVoidTransactionNumber() == null;
                boolean isReturnTran = "0".equals(getDealType1())
                        && "3".equals(getDealType2())
                        && "4".equals(getDealType3());
                boolean isReturnNew = "0".equals(getDealType1())
                        && "0".equals(getDealType2())
                        && "4".equals(getDealType3());
                if (!isNormalTran && !isReturnTran) {
                    int voidTranNo = getVoidTransactionNumber().intValue();
                    int lineSeqNo = lineItem.getLineItemSequence();
                    dss = DaiShouSales2
                            .queryDaiShouSales(voidTranNo, lineSeqNo);
                    if (!writeTranLog)
                        dss.deleteByPrimaryKey();
                    else
                        CreamToolkit.writeTransactionLog(dss
                                .getDeleteSqlStatement(true));
                    return dss;
                }
                if (isReturnNew) {
                    int lastTranNo = getTransactionNumber().intValue() - 1;
                    String barcode = lineItem.getDaiShouBarcode();
                    dss = DaiShouSales2.queryDaiShouSales(lastTranNo, barcode);
                    if (dss != null)
                        if (!writeTranLog)
                            dss.deleteByPrimaryKey();
                        else
                            CreamToolkit.writeTransactionLog(dss
                                    .getDeleteSqlStatement(true));
                    Transaction lastTran = queryByTransactionNumber((lastTranNo + "")
                            .trim());
                    Transaction voidTran = queryByTransactionNumber(lastTran
                            .getVoidTransactionNumber().toString());
                    if (!voidTran.getZSequenceNumber().equals(
                            getZSequenceNumber()))
                        return dss;
                }
                if (isReturnTran) {
                    int voidTranNo = getVoidTransactionNumber().intValue();
                    int lineSeqNo = lineItem.getLineItemSequence();
                    DaiShouSales2 tmpD = DaiShouSales2.queryDaiShouSales(
                            voidTranNo, lineSeqNo);
                    if (tmpD != null
                            && tmpD.getZSequenceNumber() == getZSequenceNumber()
                                    .intValue())
                        if (!writeTranLog)
                            tmpD.deleteByPrimaryKey();
                        else
                            CreamToolkit.writeTransactionLog(tmpD
                                    .getDeleteSqlStatement(true));
                }
                dss.setStoreID(lineItem.getStoreID());
                dss.setPosNumber(lineItem.getTerminalNumber().intValue());
                dss.setTransactionNumber(lineItem.getTransactionNumber()
                        .intValue());
                dss.setLineItemSeq(lineItem.getLineItemSequence());
                dss.setID(lineItem.getDaiShouID());
                dss.setZSequenceNumber(getZSequenceNumber().intValue());
                dss.setAccountDate(new Date());
                dss.setPayTime(new Date());
                dss.setBarcode(lineItem.getDaiShouBarcode());
                dss.setPosAmount(lineItem.getAmount());
                dss.setScAmount(lineItem.getAmount());
                if (!writeTranLog) {
                    if (dss.insert()) {
                        CreamToolkit.logMessage("DaiShouSales2("
                                + lineItem.getTransactionNumber() + ") seq="
                                + lineItem.getLineItemSequence() + ", plu="
                                + lineItem.getPluNumber() + ", barcode="
                                + lineItem.getDaiShouBarcode() + ", amt="
                                + lineItem.getAmount() + " is stored.");
                    } else {
                        POSTerminalApplication.getInstance()
                                .getWarningIndicator().setMessage(
                                        CreamToolkit.GetResource().getString(
                                                "InsertFailed"));
                        for (int j = 0; j < 10; j++)
                            java.awt.Toolkit.getDefaultToolkit().beep();

                        CreamToolkit.logMessage("Err> DaiShouSales2("
                                + lineItem.getTransactionNumber() + ") seq="
                                + lineItem.getLineItemSequence() + ", plu="
                                + lineItem.getPluNumber() + ", barcode="
                                + lineItem.getDaiShouBarcode() + ", amt="
                                + lineItem.getAmount() + " insert failed.");
                        throw new SQLException("Insert DaiShouSales2 failed.");
                    }
                } else {
                    CreamToolkit.writeTransactionLog(dss
                            .getInsertSqlStatement(true));
                }
            } catch (InstantiationException instantiationexception) {
            }
        return dss;
    }

    public Iterator getPayments() {
        return paymentArray.iterator();
    }

    public synchronized int setPayments(Payment payment) {
        if (payment == null) {
            paymentArray.clear();
            return 0;
        }
        boolean had = false;
        for (int i = 0; i < paymentArray.size(); i++)
            if (((Payment) paymentArray.get(i)).equals(payment)) {
                had = true;
                return 1;
            }

        if (!had && paymentArray.size() < 4) {
            paymentArray.add(payment);
            return 2;
        } else {
            return 0;
        }
    }

    public void setLastestPayment(Payment lastestPayment) {
        this.lastestPayment = lastestPayment;
    }

    public Payment getLastestPayment() {
        return lastestPayment;
    }

    public BigDecimal getPayAmountByID(String id) {
        int inner = Integer.parseInt(id);
        switch (inner) {
        case 1: // '\001'
            return getPayAmount1();

        case 2: // '\002'
            return getPayAmount2();

        case 3: // '\003'
            return getPayAmount3();

        case 4: // '\004'
            return getPayAmount4();
        }
        return null;
    }

    public void setPayAmountByID(String id, BigDecimal newValue) {
        int inner = Integer.parseInt(id);
        switch (inner) {
        case 1: // '\001'
            setPayAmount1(newValue);
            break;

        case 2: // '\002'
            setPayAmount2(newValue);
            break;

        case 3: // '\003'
            setPayAmount3(newValue);
            break;

        case 4: // '\004'
            setPayAmount4(newValue);
            break;
        }
    }

    public void addItemDiscount(BigDecimal itemDiscount) {
        this.itemDiscount = this.itemDiscount.add(itemDiscount);
    }

    public BigDecimal getItemDiscount() {
        return itemDiscount;
    }

    public void addTransactionListener(TransactionListener l) {
        listener.add(l);
    }

    public Iterator getTransactionListener() {
        return listener.iterator();
    }

    public void fireEvent(TransactionEvent e) {
        TransactionListener l;
        for (Iterator iter = listener.iterator(); iter.hasNext(); l
                .transactionChanged(e))
            l = (TransactionListener) iter.next();

    }

    public void setLockEnable(boolean flag) {
    }

    public boolean getLockEnable() {
        return true;
    }

    public int decreaseLockCount() {
        return 0;
    }

    public void addLineItemSimpleVersion(LineItem lineItem) {
        lineItemArray.add(lineItem);
    }

    public void addTokenTrandtlSimpleVersion(TokenTranDtl tokenTranDtl){
        payMentList.add(tokenTranDtl);       
    }
    //gllg
    public void addTokenTrandtl(TokenTranDtl tokenTrandtl){
        setLockEnable(false);
        if (tokenTrandtl != null)
            payMentList.add(tokenTrandtl);
        curTokenTrandtl = tokenTrandtl;
        if (curTokenTrandtl != null) {
            fireEvent(new TransactionEvent(this, 0));
        }
    }

    public void addLineItem(LineItem lineItem) throws TooManyLineItemsException {
    	setLockEnable(false);
    	if(lineItem != null){
    		CreamToolkit.logMessage("addLineItem(LineItem lineItem):lineItem.getItemNumber():" + lineItem.getItemNumber());
    		CreamToolkit.logMessage("addLineItem(LineItem lineItem):lineItem.getDetailCode():" + lineItem.getDetailCode());
    	}
    	if (getDisplayedLineItemsArray().size() >= maxLineItems
    			&& lineItem != null
                && !lineItem.getDetailCode().equals("E")
                && !lineItem.getDetailCode().equals("V")
                && !lineItem.getDetailCode().equals("M")
                && !lineItem.getDetailCode().equals("D")) {
            setLockEnable(true);
            throw new TooManyLineItemsException();
        }
        if (currentLineItem != null)
            lineItemArray.add(currentLineItem);
        currentLineItem = lineItem;
        if (currentLineItem != null) {
            currentLineItem.setLineItemSequence(lineItemArray.size() + 1);
            fireEvent(new TransactionEvent(this, 0));
        }
    }

    public void addLineItemNoMaxLineItemLimit(LineItem lineItem) throws TooManyLineItemsException {
        setLockEnable(false);
        if (currentLineItem != null)
            lineItemArray.add(currentLineItem);
        currentLineItem = lineItem;
        if (currentLineItem != null) {
            currentLineItem.setLineItemSequence(lineItemArray.size() + 1);
            fireEvent(new TransactionEvent(this, 0));
        }
    }

    public void removeLineItem(LineItem lineItem)
            throws LineItemNotFoundException {
        setLockEnable(false);
        boolean b = lineItemArray.remove(lineItem);
        if (!b) {
            throw new LineItemNotFoundException();
        } else {
            fireEvent(new TransactionEvent(this, 1));
            return;
        }
    }

    public void removeSILineItem() throws LineItemNotFoundException {
        if (!getLockEnable())
            return;
        Iterator lineItems = ((ArrayList) lineItemArrayLast.clone()).iterator();
        LineItem lineItem = null;
        while (lineItems.hasNext()) {
            lineItem = (LineItem) lineItems.next();
            if (lineItem.getDetailCode().equals("D")) {
                SI si = SI.queryBySIID(lineItem.getPluNumber());
                if (si.isAfterSI()) {
                    lineItemArrayLast.remove(lineItem);
                    lineItemArray.remove(lineItem);
                    fireEvent(new TransactionEvent(this, 1));
                }
            }
        }
        if (currentLineItem != null
                && currentLineItem.getDetailCode().equals("D"))
            currentLineItem = null;
    }

    public void changeLineItem(int sequenceNumber, LineItem lineItem)
            throws LineItemNotFoundException {
        setLockEnable(false);
        try {
            if (sequenceNumber == -1)
                currentLineItem = lineItem;
            else
                lineItemArray.set(sequenceNumber, lineItem);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Index out of bounds ");
            CreamToolkit.logMessage("LineItem not found");
        }
        fireEvent(new TransactionEvent(this, 2));
    }

    public void clearLineItem() {
        Object objs[] = getLineItemsArray();
        for (int i = 0; i < objs.length; i++) {
            setLockEnable(true);
            try {
                removeLineItem((LineItem) objs[i]);
            } catch (LineItemNotFoundException lineitemnotfoundexception) {
            }
            setLockEnable(false);
        }

        currentLineItem = null;
        fireEvent(new TransactionEvent(this, 4));
    }

    public LineItem getLineItem(int no) {
        return (LineItem) getLineItemsArrayLast()[no - 1];
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public LineItem getCurrentLineItem() {
        return currentLineItem;
    }

    public Iterator getLineItems() {
        return lineItemArrayLast.iterator(); 
    }

    public Iterator getLineItemsSimpleVersion() {
        return lineItemArray.iterator();
    }

    public Object[] getLineItemsArray() {
        return lineItemArray.toArray();
    }

    public Object[] getNormalLineItemsArray() {
        Object pureArrayList[] = getLineItemsArrayLast();
        ArrayList purelineItemArrayList = new ArrayList();
        for (int i = 0; i < pureArrayList.length; i++) {
            LineItem lineItem = (LineItem) pureArrayList[i];
            if ((lineItem.getDetailCode().equals("S") || lineItem
                    .getDetailCode().equals("R"))
                    && !lineItem.getRemoved())
                purelineItemArrayList.add(lineItem);
        }

        return purelineItemArrayList.toArray();
    }

    synchronized public Object[] getLineItemsArrayLast() {
        lineItemArrayLast = new ArrayList();
        for (Iterator itr = lineItemArray.iterator(); itr.hasNext();
            lineItemArrayLast.add(itr.next()))
            ;
        if (currentLineItem != null)
            lineItemArrayLast.add(currentLineItem);
        return lineItemArrayLast.toArray();
    }

    public void addTaxMM(int count, BigDecimal amount) {
        mmCount += count;
        mmAmount = mmAmount.add(amount);
        totalMMAmount = mmAmount;
        fireEvent(new TransactionEvent(this, 3));
    }

    public BigDecimal getTaxMMAmount() {
        return mmAmount.setScale(2, 4);
    }

    public void setTaxMMAmount(BigDecimal amount) {
        mmAmount = amount;
        totalMMAmount = mmAmount;
    }

    public int getTaxMMCount() {
        return mmCount;
    }

    public void setTaxMMCount(int count) {
        mmCount = count;
    }

    public void updateTaxMM() {
        setMixAndMatchAmount0(bigDecimal0.setScale(2, 4));
        setMixAndMatchAmount1(bigDecimal0.setScale(2, 4));
        setMixAndMatchAmount2(bigDecimal0.setScale(2, 4));
        setMixAndMatchAmount3(bigDecimal0.setScale(2, 4));
        setMixAndMatchAmount4(bigDecimal0.setScale(2, 4));
        setMixAndMatchCount0(getTaxMMCount());
        Iterator iter = taxMMAmount.keySet().iterator();
        String key = "";
        String fieldName = "";
        BigDecimal fieldValue = null;
        for (; iter.hasNext(); setFieldValue(fieldName, fieldValue.setScale(2,
                4))) {
            key = (String) iter.next();
            fieldName = "MNMAMT" + key;
            fieldValue = (BigDecimal) taxMMAmount.get(key);
        }

    }

    public String getInsertUpdateTableName() {
        if (Server.serverExist())
            return "posul_tranhead";
        else
            return "tranhead";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (Server.serverExist())
            return "posul_tranhead";
        else
            return "tranhead";
    }

    public String getStoreNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("storeID");
        else
            return (String) getFieldValue("STORENO");
    }

    public String getStoreName() {
        return Store.getStoreChineseName();
    }

    public void setStoreNumber(String storecode) {
        if (Server.serverExist())
            setFieldValue("storeID", storecode);
        else
            setFieldValue("STORENO", storecode);
    }

    public Date getSystemDateTime() {
        if (Server.serverExist())
            return (Date) getFieldValue("systemDate");
        else
            return (Date) getFieldValue("SYSDATE");
    }

    public void setSystemDateTime(Date sysdate) {
        if (Server.serverExist())
            setFieldValue("systemDate", sysdate);
        else
            setFieldValue("SYSDATE", sysdate);
    }

    public Integer getTerminalNumber() {
        if (Server.serverExist())
            return (Integer) getFieldValue("posNumber");
        else
            return (Integer) getFieldValue("POSNO");
    }

    public void setTerminalNumber(int tmcode) {
        if (Server.serverExist())
            setFieldValue("posNumber", new Integer(tmcode));
        else
            setFieldValue("POSNO", new Integer(tmcode));
    }

    public Integer getTransactionNumber() {
        if (Server.serverExist())
            return (Integer) getFieldValue("transactionNumber");
        else
            return (Integer) getFieldValue("TMTRANSEQ");
    }

    public String getPrintTranNumber() {
        String needConfuseTranNumber = CreamProperties.getInstance()
                .getProperty("ConfuseTranNumber");
        if (needConfuseTranNumber != null
                && needConfuseTranNumber.equalsIgnoreCase("yes"))
            return NumberConfuser.confuse(getTransactionNumber().toString());
        else
            return getTransactionNumber().toString();
    }

    public void setTransactionNumber(int tmtranseq) {
        if (Server.serverExist())
            setFieldValue("transactionNumber", new Integer(tmtranseq));
        else
            setFieldValue("TMTRANSEQ", new Integer(tmtranseq));
    }

    public void setTransactionNumber(Integer tmtranseq) {
        if (Server.serverExist())
            setFieldValue("transactionNumber", tmtranseq);
        else
            setFieldValue("TMTRANSEQ", tmtranseq);
    }

    public Integer getSignOnNumber() {
        if (Server.serverExist())
            return (Integer) getFieldValue("shiftCount");
        else
            return (Integer) getFieldValue("SIGNONID");
    }

    public void setSignOnNumber(int signonid) {
        if (Server.serverExist())
            setFieldValue("shiftCount", new Integer(signonid));
        else
            setFieldValue("SIGNONID", new Integer(signonid));
    }

    public Date getAccountingDate() {
        if (Server.serverExist())
            return (Date) getFieldValue("accountDate");
        else
            return (Date) getFieldValue("ACCDATE");
    }

    public void setAccountingDate(Date accdate) {
        if (Server.serverExist())
            setFieldValue("accountDate", accdate);
        else
            setFieldValue("ACCDATE", accdate);
    }

    public String getTransactionType() {
        if (Server.serverExist())
            return (String) getFieldValue("transactionType");
        else
            return (String) getFieldValue("TRANTYPE");
    }

    public void setTransactionType(String trantype) {
        if (Server.serverExist())
            setFieldValue("transactionType", trantype);
        else
            setFieldValue("TRANTYPE", trantype);
    }

    public String getDealType1() {
        if (Server.serverExist())
            return (String) getFieldValue("dealType1");
        else
            return (String) getFieldValue("DEALTYPE1");
    }

    public void setDealType1(String dealtype1) {
        if (Server.serverExist())
            setFieldValue("dealType1", dealtype1);
        else
            setFieldValue("DEALTYPE1", dealtype1);
    }

    public String getDealType2() {
        if (Server.serverExist())
            return (String) getFieldValue("dealType2");
        else
            return (String) getFieldValue("DEALTYPE2");
    }

    public void setDealType2(String dealtype2) {
        if (Server.serverExist())
            setFieldValue("dealType2", dealtype2);
        else
            setFieldValue("DEALTYPE2", dealtype2);
        fireEvent(new TransactionEvent(this, 3));
    }

    public String getDealType3() {
        if (Server.serverExist())
            return (String) getFieldValue("dealType3");
        else
            return (String) getFieldValue("DEALTYPE3");
    }

    public void setDealType3(String dealtype3) {
        if (Server.serverExist())
            setFieldValue("dealType3", dealtype3);
        else
            setFieldValue("DEALTYPE3", dealtype3);
    }

    public Integer getVoidTransactionNumber() {
        if (Server.serverExist())
            return (Integer) getFieldValue("voidTransactionNumber");
        else
            return (Integer) getFieldValue("VOIDSEQ");
    }

    public void setVoidTransactionNumber(Integer voidTNumber) {
        if (Server.serverExist())
            setFieldValue("voidTransactionNumber", voidTNumber);
        else
            setFieldValue("VOIDSEQ", voidTNumber);
    }

    public String getTerminalPhysicalNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("machineNumber");
        else
            return (String) getFieldValue("TMCODEP");
    }

    public void setTerminalPhysicalNumber(String tmcodep) {
        if (Server.serverExist())
            setFieldValue("machineNumber", tmcodep);
        else
            setFieldValue("TMCODEP", tmcodep);
    }

    public String getCashierNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("cashier");
        else
            return (String) getFieldValue("CASHIER");
    }

    public String getCashierName() {
        Cashier cas = Cashier.queryByCashierID(getCashierNumber());
        return cas != null ? cas.getCashierName() : "";
    }

    public void setCashierNumber(String cashier) {
        if (Server.serverExist())
            setFieldValue("cashier", cashier);
        else
            setFieldValue("CASHIER", cashier);
        fireEvent(new TransactionEvent(this, 3));
    }

    public String getInvoiceID() {
        if (Server.serverExist())
            return (String) getFieldValue("invoiceHead");
        else
            return (String) getFieldValue("INVNOHEAD");
    }

    public void setInvoiceID(String invnohead) {
        if (Server.serverExist())
            setFieldValue("invoiceHead", invnohead);
        else
            setFieldValue("INVNOHEAD", invnohead);
        fireEvent(new TransactionEvent(this, 3));
    }

    public String getInvoiceNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("invoiceNumber");
        else
            return (String) getFieldValue("INVNO");
    }

    public void setInvoiceNumber(String invno) {
        if (Server.serverExist())
            setFieldValue("invoiceNumber", invno);
        else
            setFieldValue("INVNO", invno);
    }

    public Integer getInvoiceCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("invoiceCount");
        else
            return (Integer) getFieldValue("INVCNT");
    }

    public void setInvoiceCount(int invcnt) {
        if (Server.serverExist())
            setFieldValue("invoiceCount", new Integer(invcnt));
        else
            setFieldValue("INVCNT", new Integer(invcnt));
    }

    public String getBuyerNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("idNumber");
        else
            return (String) getFieldValue("IDNO");
    }

    public void setBuyerNumber(String idno) {
        if (Server.serverExist())
            setFieldValue("idNumber", idno);
        else
            setFieldValue("IDNO", idno);
        fireEvent(new TransactionEvent(this, 3));
    }

    public ArrayList getDisplayedLineItemsArray() {
        Object pureArrayList[] = getLineItemsArrayLast();
        ArrayList purelineItemArrayList = new ArrayList();
        for (int i = 0; i < pureArrayList.length; i++) {
            LineItem lineItem = (LineItem) pureArrayList[i];
            LineItem lineItem2 = null;
            if (i + 1 < pureArrayList.length)
                lineItem2 = (LineItem) pureArrayList[i + 1];
            if ((lineItem2 == null || !lineItem.getRemoved() || !lineItem2
                    .getDetailCode().equals("E"))
                    && (!lineItem.getRemoved() || !lineItem.getDetailCode()
                            .equals("E")
                            && !lineItem.getDetailCode().equals("V"))
                    && !lineItem.getDetailCode().equals("M")) {
                purelineItemArrayList.add(lineItem);
                lineItem.setDisplayedLineItemSequence(purelineItemArrayList
                        .size());
            }
        }

        return purelineItemArrayList;
    }

    public Integer getDetailCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("detailCount");
        else
            return (Integer) getFieldValue("DETAILCNT");
    }

    public void setDetailCount(int detailcnt) {
        if (Server.serverExist())
            setFieldValue("detailCount", new Integer(detailcnt));
        else
            setFieldValue("DETAILCNT", new Integer(detailcnt));
    }

    public Integer getCustomerCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("customerCount");
        else
            return (Integer) getFieldValue("CUSTCNT");
    }

    public void setCustomerCount(int custcnt) {
        if (Server.serverExist())
            setFieldValue("customerCount", new Integer(custcnt));
        else
            setFieldValue("CUSTCNT", new Integer(custcnt));
    }

    public String getInOutStore() {
        if (Server.serverExist())
            return (String) getFieldValue("inOurID");
        else
            return (String) getFieldValue("INOUT");
    }

    public void setInOutStore(Object inout) {
        if (Server.serverExist())
            setFieldValue("inOurID", inout);
        else
            setFieldValue("INOUT", inout);
    }

    public Integer getCustomerAgeLevel() {
        if (Server.serverExist())
            return (Integer) getFieldValue("customerCategory");
        else
            return (Integer) getFieldValue("CUSTID");
    }

    public void setCustomerAgeLevel(int custid) {
        if (Server.serverExist())
            setFieldValue("customerCategory", new Integer(custid));
        else
            setFieldValue("CUSTID", new Integer(custid));
    }

    public BigDecimal getPositiveSalesAmount() {
        BigDecimal discntAmt = bigDecimal0;
        discntAmt = getItemDiscountTotal();
        if (Server.serverExist())
            return ((BigDecimal) getFieldValue("grossSaleTotalAmount"))
                    .add(discntAmt.negate());
        else
            return ((BigDecimal) getFieldValue("GROSALAMT")).add(discntAmt
                    .negate());
    }

    public BigDecimal getItemDiscountTotal() {
        return getItemDiscount().negate();
    }

    public String getSalesman() {
        if (Server.serverExist())
            return (String) getFieldValue("saleMan");
        else
            return (String) getFieldValue("SALEMAN");
    }

    public void setSalesman(String salesman) {
        if (Server.serverExist())
            setFieldValue("saleMan", salesman);
        else
            setFieldValue("SALEMAN", salesman);
    }

    public BigDecimal getGrossSalesAmount() {
        BigDecimal b;
        if (Server.serverExist())
            b = (BigDecimal) getFieldValue("grossSaleTotalAmount");
        else
            b = (BigDecimal) getFieldValue("GROSALAMT");
        if (b != null)
            return b;
        else
            return bigDecimal0;
    }
    
    //gllg
    public BigDecimal getGrossSalesAmtWithDsct(){
        return getSalesAmtWithItemDsct(); 
    }

    public void setGrossSalesAmount(BigDecimal grosalamt) {
        if (Server.serverExist())
            setFieldValue("grossSaleTotalAmount", grosalamt);
        else
            setFieldValue("GROSALAMT", grosalamt);
    }

    public BigDecimal getGrossSalesTax0Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("grossSaleTax0Amount");
        else
            return (BigDecimal) getFieldValue("GROSALTX0AMT");
    }

    public void setGrossSalesTax0Amount(BigDecimal grosaltx0amt) {
        if (Server.serverExist())
            setFieldValue("grossSaleTax0Amount", grosaltx0amt);
        else
            setFieldValue("GROSALTX0AMT", grosaltx0amt);
    }

    public BigDecimal getGrossSalesTax1Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("grossSaleTax1Amount");
        else
            return (BigDecimal) getFieldValue("GROSALTX1AMT");
    }

    public void setGrossSalesTax1Amount(BigDecimal grosaltx1amt) {
        if (Server.serverExist())
            setFieldValue("grossSaleTax1Amount", grosaltx1amt);
        else
            setFieldValue("GROSALTX1AMT", grosaltx1amt);
    }

    public BigDecimal getGrossSalesTax2Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("grossSaleTax2Amount");
        else
            return (BigDecimal) getFieldValue("GROSALTX2AMT");
    }

    public void setGrossSalesTax2Amount(BigDecimal grosaltx2amt) {
        if (Server.serverExist())
            setFieldValue("grossSaleTax2Amount", grosaltx2amt);
        else
            setFieldValue("GROSALTX2AMT", grosaltx2amt);
    }

    public BigDecimal getGrossSalesTax3Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("grossSaleTax3Amount");
        else
            return (BigDecimal) getFieldValue("GROSALTX3AMT");
    }

    public void setGrossSalesTax3Amount(BigDecimal grosaltx3amt) {
        if (Server.serverExist())
            setFieldValue("grossSaleTax3Amount", grosaltx3amt);
        else
            setFieldValue("GROSALTX3AMT", grosaltx3amt);
    }

    public BigDecimal getGrossSalesTax4Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("grossSaleTax4Amount");
        else
            return (BigDecimal) getFieldValue("GROSALTX4AMT");
    }

    public void setGrossSalesTax4Amount(BigDecimal grosaltx4amt) {
        if (Server.serverExist())
            setFieldValue("grossSaleTax4Amount", grosaltx4amt);
        else
            setFieldValue("GROSALTX4AMT", grosaltx4amt);
    }

    public BigDecimal getSIPercentPlusAmount0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("siPlusTax0Amount");
        else
            return (BigDecimal) getFieldValue("SIPLUSAMT0");
    }

    public void setSIPercentPlusAmount0(BigDecimal siplusamt0) {
        if (Server.serverExist())
            setFieldValue("siPlusTax0Amount", siplusamt0);
        else
            setFieldValue("SIPLUSAMT0", siplusamt0);
    }

    public Integer getSIPercentPlusCount0() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT0");
    }

    public void setSIPercentPlusCount0(int sipluscnt0) {
        if (!Server.serverExist())
            setFieldValue("SIPLUSCNT0", new Integer(sipluscnt0));
    }

    public BigDecimal getSIPercentOffAmount0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("discountTax0Amount");
        else
            return (BigDecimal) getFieldValue("SIPAMT0");
    }

    public void setSIPercentOffAmount0(BigDecimal sipamt0) {
        if (Server.serverExist())
            setFieldValue("discountTax0Amount", sipamt0);
        else
            setFieldValue("SIPAMT0", sipamt0);
    }

    public Integer getSIPercentOffCount0() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT0");
    }

    public void setSIPercentOffCount0(int sipcnt0) {
        if (!Server.serverExist())
            setFieldValue("SIPCNT0", new Integer(sipcnt0));
    }

    public BigDecimal getSIDiscountAmount0() {
        if (Server.serverExist())
            return bigDecimal0;
        else
            return (BigDecimal) getFieldValue("SIMAMT0");
    }

    public void setSIDiscountAmount0(BigDecimal simamt0) {
        if (!Server.serverExist())
            setFieldValue("SIMAMT0", simamt0);
    }

    public Integer getSIDiscountCount0() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT0");
    }

    public void setSIDiscountCount0(int simcnt0) {
        if (!Server.serverExist())
            setFieldValue("SIMCNT0", new Integer(simcnt0));
    }

    public BigDecimal getMixAndMatchAmount0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("mixAndMatchTax0Amount");
        else
            return (BigDecimal) getFieldValue("MNMAMT0");
    }

    public void setTaxMMAmount(Hashtable taxMMAmount) {
        this.taxMMAmount = taxMMAmount;
    }

    public void setTaxMMCount(Hashtable taxMMCount) {
        this.taxMMCount = taxMMCount;
    }

    public void setMixAndMatchAmount0(BigDecimal mnmamt0) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax0Amount", mnmamt0);
        else
            setFieldValue("MNMAMT0", mnmamt0);
    }

    public Integer getMixAndMatchCount0() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT0");
    }

    public void setMixAndMatchCount0(Integer mnmcnt0) {
        setMixAndMatchCount0(mnmcnt0.intValue());
    }

    public void setMixAndMatchCount1(Integer mnmcnt1) {
        setMixAndMatchCount1(mnmcnt1.intValue());
    }

    public void setMixAndMatchCount2(Integer mnmcnt2) {
        setMixAndMatchCount2(mnmcnt2.intValue());
    }

    public void setMixAndMatchCount3(Integer mnmcnt3) {
        setMixAndMatchCount3(mnmcnt3.intValue());
    }

    public void setMixAndMatchCount4(Integer mnmcnt4) {
        setMixAndMatchCount4(mnmcnt4.intValue());
    }

    public void setMixAndMatchCount0(int mnmcnt0) {
        if (!Server.serverExist())
            setFieldValue("MNMCNT0", new Integer(mnmcnt0));
    }

    public BigDecimal getNotIncludedSales0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("notIncludedTax0Sale");
        else
            return (BigDecimal) getFieldValue("RCPGIFAMT0");
    }

    public void setNotIncludedSales0(BigDecimal rcpgifamt0) {
        if (Server.serverExist())
            setFieldValue("notIncludedTax0Sale", rcpgifamt0);
        else
            setFieldValue("RCPGIFAMT0", rcpgifamt0);
    }

    public BigDecimal getNetSalesAmount0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("netSaleTax0Amount");
        else
            return (BigDecimal) getFieldValue("NETSALAMT0");
    }

    public void setNetSalesAmount0(BigDecimal netsalamt0) {
        if (Server.serverExist())
            setFieldValue("netSaleTax0Amount", netsalamt0);
        else
            setFieldValue("NETSALAMT0", netsalamt0);
    }

    public BigDecimal getSIPercentPlusAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("siPlusTax1Amount");
        else
            return (BigDecimal) getFieldValue("SIPLUSAMT1");
    }

    public void setSIPercentPlusAmount1(BigDecimal siplusamt1) {
        if (Server.serverExist())
            setFieldValue("siPlusTax1Amount", siplusamt1);
        else
            setFieldValue("SIPLUSAMT1", siplusamt1);
    }

    public Integer getSIPercentPlusCount1() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT1");
    }

    public void setSIPercentPlusCount1(int sipluscnt1) {
        if (!Server.serverExist())
            setFieldValue("SIPLUSCNT1", new Integer(sipluscnt1));
    }

    public BigDecimal getSIPercentOffAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("discountTax1Amount");
        else
            return (BigDecimal) getFieldValue("SIPAMT1");
    }

    public void setSIPercentOffAmount1(BigDecimal sipamt1) {
        if (Server.serverExist())
            setFieldValue("discountTax1Amount", sipamt1);
        else
            setFieldValue("SIPAMT1", sipamt1);
    }

    public Integer getSIPercentOffCount1() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT1");
    }

    public void setSIPercentOffCount1(int sipcnt1) {
        if (!Server.serverExist())
            setFieldValue("SIPCNT1", new Integer(sipcnt1));
    }

    public BigDecimal getSIDiscountAmount1() {
        if (Server.serverExist())
            return bigDecimal0;
        else
            return (BigDecimal) getFieldValue("SIMAMT1");
    }

    public void setSIDiscountAmount1(BigDecimal simamt1) {
        if (!Server.serverExist())
            setFieldValue("SIMAMT1", simamt1);
    }

    public Integer getSIDiscountCount1() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT1");
    }

    public void setSIDiscountCount1(int simcnt1) {
        if (!Server.serverExist())
            setFieldValue("SIMCNT1", new Integer(simcnt1));
    }

    public BigDecimal getMixAndMatchAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("mixAndMatchTax1Amount");
        else
            return (BigDecimal) getFieldValue("MNMAMT1");
    }

    public void setMixAndMatchAmount1(BigDecimal mnmamt1) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax1Amount", mnmamt1);
        else
            setFieldValue("MNMAMT1", mnmamt1);
    }

    public Integer getMixAndMatchCount1() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT1");
    }

    public void setMixAndMatchCount1(int mnmcnt1) {
        if (!Server.serverExist())
            setFieldValue("MNMCNT1", new Integer(mnmcnt1));
    }

    public BigDecimal getNotIncludedSales1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("notIncludedTax1Sale");
        else
            return (BigDecimal) getFieldValue("RCPGIFAMT1");
    }

    public void setNotIncludedSales1(BigDecimal rcpgifamt1) {
        if (Server.serverExist())
            setFieldValue("notIncludedTax1Sale", rcpgifamt1);
        else
            setFieldValue("RCPGIFAMT1", rcpgifamt1);
    }

    public BigDecimal getNetSalesAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("netSaleTax1Amount");
        else
            return (BigDecimal) getFieldValue("NETSALAMT1");
    }

    public void setNetSalesAmount1(BigDecimal netsalamt1) {
        if (Server.serverExist())
            setFieldValue("netSaleTax1Amount", netsalamt1);
        else
            setFieldValue("NETSALAMT1", netsalamt1);
    }

    public BigDecimal getSIPercentPlusAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("siPlusTax2Amount");
        else
            return (BigDecimal) getFieldValue("SIPLUSAMT2");
    }

    public void setSIPercentPlusAmount2(BigDecimal siplusamt2) {
        if (Server.serverExist())
            setFieldValue("siPlusTax2Amount", siplusamt2);
        else
            setFieldValue("SIPLUSAMT2", siplusamt2);
    }

    public Integer getSIPercentPlusCount2() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT2");
    }

    public void setSIPercentPlusCount2(int sipluscnt2) {
        if (!Server.serverExist())
            setFieldValue("SIPLUSCNT2", new Integer(sipluscnt2));
    }

    public BigDecimal getSIPercentOffAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("discountTax2Amount");
        else
            return (BigDecimal) getFieldValue("SIPAMT2");
    }

    public void setSIPercentOffAmount2(BigDecimal sipamt2) {
        if (Server.serverExist())
            setFieldValue("discountTax2Amount", sipamt2);
        else
            setFieldValue("SIPAMT2", sipamt2);
    }

    public Integer getSIPercentOffCount2() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT2");
    }

    public void setSIPercentOffCount2(int sipcnt2) {
        if (!Server.serverExist())
            setFieldValue("SIPCNT2", new Integer(sipcnt2));
    }

    public BigDecimal getSIDiscountAmount2() {
        if (Server.serverExist())
            return bigDecimal0;
        else
            return (BigDecimal) getFieldValue("SIMAMT2");
    }

    public void setSIDiscountAmount2(BigDecimal simamt2) {
        if (!Server.serverExist())
            setFieldValue("SIMAMT2", simamt2);
    }

    public Integer getSIDiscountCount2() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT2");
    }

    public void setSIDiscountCount2(int simcnt2) {
        if (!Server.serverExist())
            setFieldValue("SIMCNT2", new Integer(simcnt2));
    }

    public BigDecimal getMixAndMatchAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("mixAndMatchTax2Amount");
        else
            return (BigDecimal) getFieldValue("MNMAMT2");
    }

    public void setMixAndMatchAmount2(BigDecimal mnmamt2) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax2Amount", mnmamt2);
        else
            setFieldValue("MNMAMT2", mnmamt2);
    }

    public Integer getMixAndMatchCount2() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT2");
    }

    public void setMixAndMatchCount2(int mnmcnt2) {
        if (!Server.serverExist())
            setFieldValue("MNMCNT2", new Integer(mnmcnt2));
    }

    public BigDecimal getNotIncludedSales2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("notIncludedTax2Sale");
        else
            return (BigDecimal) getFieldValue("RCPGIFAMT2");
    }

    public void setNotIncludedSales2(BigDecimal rcpgifamt2) {
        if (Server.serverExist())
            setFieldValue("notIncludedTax2Sale", rcpgifamt2);
        else
            setFieldValue("RCPGIFAMT2", rcpgifamt2);
    }

    public BigDecimal getNetSalesAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("netSaleTax2Amount");
        else
            return (BigDecimal) getFieldValue("NETSALAMT2");
    }

    public void setNetSalesAmount2(BigDecimal netsalamt2) {
        if (Server.serverExist())
            setFieldValue("netSaleTax2Amount", netsalamt2);
        else
            setFieldValue("NETSALAMT2", netsalamt2);
    }

    public BigDecimal getSIPercentPlusAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("siPlusTax3Amount");
        else
            return (BigDecimal) getFieldValue("SIPLUSAMT3");
    }

    public void setSIPercentPlusAmount3(BigDecimal siplusamt3) {
        if (Server.serverExist())
            setFieldValue("siPlusTax3Amount", siplusamt3);
        else
            setFieldValue("SIPLUSAMT3", siplusamt3);
    }

    public Integer getSIPercentPlusCount3() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT3");
    }

    public void setSIPercentPlusCount3(int sipluscnt3) {
        if (!Server.serverExist())
            setFieldValue("SIPLUSCNT3", new Integer(sipluscnt3));
    }

    public BigDecimal getSIPercentOffAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("discountTax3Amount");
        else
            return (BigDecimal) getFieldValue("SIPAMT3");
    }

    public void setSIPercentOffAmount3(BigDecimal sipamt3) {
        if (Server.serverExist())
            setFieldValue("discountTax3Amount", sipamt3);
        else
            setFieldValue("SIPAMT3", sipamt3);
    }

    public Integer getSIPercentOffCount3() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT3");
    }

    public void setSIPercentOffCount3(int sipcnt3) {
        if (!Server.serverExist())
            setFieldValue("SIPCNT3", new Integer(sipcnt3));
    }

    public BigDecimal getSIDiscountAmount3() {
        if (Server.serverExist())
            return bigDecimal0;
        else
            return (BigDecimal) getFieldValue("SIMAMT3");
    }

    public void setSIDiscountAmount3(BigDecimal simamt3) {
        if (!Server.serverExist())
            setFieldValue("SIMAMT3", simamt3);
    }

    public Integer getSIDiscountCount3() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT3");
    }

    public void setSIDiscountCount3(int simcnt3) {
        if (!Server.serverExist())
            setFieldValue("SIMCNT3", new Integer(simcnt3));
    }

    public BigDecimal getMixAndMatchAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("mixAndMatchTax3Amount");
        else
            return (BigDecimal) getFieldValue("MNMAMT3");
    }

    public void setMixAndMatchAmount3(BigDecimal mnmamt3) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax3Amount", mnmamt3);
        else
            setFieldValue("MNMAMT3", mnmamt3);
    }

    public Integer getMixAndMatchCount3() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT3");
    }

    public void setMixAndMatchCount3(int mnmcnt3) {
        if (!Server.serverExist())
            setFieldValue("MNMCNT3", new Integer(mnmcnt3));
    }

    public BigDecimal getNotIncludedSales3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("notIncludedTax3Sale");
        else
            return (BigDecimal) getFieldValue("RCPGIFAMT3");
    }

    public void setNotIncludedSales3(BigDecimal rcpgifamt3) {
        if (Server.serverExist())
            setFieldValue("notIncludedTax3Sale", rcpgifamt3);
        else
            setFieldValue("RCPGIFAMT3", rcpgifamt3);
    }

    public BigDecimal getNetSalesAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("netSaleTax3Amount");
        else
            return (BigDecimal) getFieldValue("NETSALAMT3");
    }

    public void setNetSalesAmount3(BigDecimal netsalamt3) {
        if (Server.serverExist())
            setFieldValue("netSaleTax3Amount", netsalamt3);
        else
            setFieldValue("NETSALAMT3", netsalamt3);
    }

    public BigDecimal getSIPercentPlusAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("siPlusTax4Amount");
        else
            return (BigDecimal) getFieldValue("SIPLUSAMT4");
    }

    public void setSIPercentPlusAmount4(BigDecimal siplusamt4) {
        if (Server.serverExist())
            setFieldValue("siPlusTax4Amount", siplusamt4);
        else
            setFieldValue("SIPLUSAMT4", siplusamt4);
    }

    public Integer getSIPercentPlusCount4() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPLUSCNT4");
    }

    public void setSIPercentPlusCount4(int sipluscnt4) {
        if (!Server.serverExist())
            setFieldValue("SIPLUSCNT4", new Integer(sipluscnt4));
    }

    public BigDecimal getSIPercentOffAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("discountTax4Amount");
        else
            return (BigDecimal) getFieldValue("SIPAMT4");
    }

    public void setSIPercentOffAmount4(BigDecimal sipamt4) {
        if (Server.serverExist())
            setFieldValue("discountTax4Amount", sipamt4);
        else
            setFieldValue("SIPAMT4", sipamt4);
    }

    public Integer getSIPercentOffCount4() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIPCNT4");
    }

    public void setSIPercentOffCount4(int sipcnt4) {
        if (!Server.serverExist())
            setFieldValue("SIPCNT4", new Integer(sipcnt4));
    }

    public BigDecimal getSIDiscountAmount4() {
        if (Server.serverExist())
            return bigDecimal0;
        else
            return (BigDecimal) getFieldValue("SIMAMT4");
    }

    public void setSIDiscountAmount4(BigDecimal simamt4) {
        if (!Server.serverExist())
            setFieldValue("SIMAMT4", simamt4);
    }

    public Integer getSIDiscountCount4() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("SIMCNT4");
    }

    public void setSIDiscountCount4(int simcnt4) {
        if (!Server.serverExist())
            setFieldValue("SIMCNT4", new Integer(simcnt4));
    }

    public BigDecimal getMixAndMatchAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("mixAndMatchTax4Amount");
        else
            return (BigDecimal) getFieldValue("MNMAMT4");
    }

    public void setMixAndMatchAmount4(BigDecimal mnmamt4) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax4Amount", mnmamt4);
        else
            setFieldValue("MNMAMT4", mnmamt4);
    }

    public Integer getMixAndMatchCount4() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("MNMCNT4");
    }

    public void setMixAndMatchCount4(int mnmcnt4) {
        if (!Server.serverExist())
            setFieldValue("MNMCNT4", new Integer(mnmcnt4));
    }

    public BigDecimal getNotIncludedSales4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("notIncludedTax4Sale");
        else
            return (BigDecimal) getFieldValue("RCPGIFAMT4");
    }

    public void setNotIncludedSales4(BigDecimal rcpgifamt4) {
        if (Server.serverExist())
            setFieldValue("notIncludedTax4Sale", rcpgifamt4);
        else
            setFieldValue("RCPGIFAMT4", rcpgifamt4);
    }

    public BigDecimal getNetSalesAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("netSaleTax4Amount");
        else
            return (BigDecimal) getFieldValue("NETSALAMT4");
    }

    public void setNetSalesAmount4(BigDecimal netsalamt4) {
        if (Server.serverExist())
            setFieldValue("netSaleTax4Amount", netsalamt4);
        else
            setFieldValue("NETSALAMT4", netsalamt4);
    }

    public BigDecimal getNetSalesAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("netSaleTax0Amount");
        else
            return (BigDecimal) getFieldValue("NETSALAMT");
    }

    public void setNetSalesAmount(BigDecimal netsalamt) {
        if (Server.serverExist())
            setFieldValue("netSaleTax0Amount", netsalamt);
        else
            setFieldValue("NETSALAMT", netsalamt);
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxamt) {
        taxAmount = taxamt;
    }

    public void initForAccum() {
        setSalesAmount(bigDecimal0);
        setGrossSalesAmount(bigDecimal0);
        setDaiFuAmount(bigDecimal0);
        setDaiShouAmount(bigDecimal0);
        setDaiShouAmount2(bigDecimal0);
        setUseRebateAmt(bigDecimal0);
        setGrossSalesTax0Amount(bigDecimal0);
        setGrossSalesTax1Amount(bigDecimal0);
        setGrossSalesTax2Amount(bigDecimal0);
        setGrossSalesTax3Amount(bigDecimal0);
        setGrossSalesTax4Amount(bigDecimal0);
        setNotIncludedSales0(bigDecimal0);
        setNotIncludedSales1(bigDecimal0);
        setNotIncludedSales2(bigDecimal0);
        setNotIncludedSales3(bigDecimal0);
        setNotIncludedSales4(bigDecimal0);
        setNetSalesAmount(bigDecimal0);
        setNetSalesAmount0(bigDecimal0);
        setNetSalesAmount1(bigDecimal0);
        setNetSalesAmount2(bigDecimal0);
        setNetSalesAmount3(bigDecimal0);
        setNetSalesAmount4(bigDecimal0);
        setTaxAmount0(bigDecimal0);
        setTaxAmount1(bigDecimal0);
        setTaxAmount2(bigDecimal0);
        setTaxAmount3(bigDecimal0);
        setTaxAmount4(bigDecimal0);
        setItemDiscAmount0(bigDecimal0);
        setItemDiscAmount1(bigDecimal0);
        setItemDiscAmount2(bigDecimal0);
        setItemDiscAmount3(bigDecimal0);
        setItemDiscAmount4(bigDecimal0);
        //gllg
        setItemDiscCnt0(0);
        setItemDiscCnt1(0);
        setItemDiscCnt2(0);
        setItemDiscCnt3(0);
        setItemDiscCnt4(0);
    }

    public void initSIInfo() {
        setSIDiscountAmount0(bigDecimal0);
        setSIDiscountAmount1(bigDecimal0);
        setSIDiscountAmount2(bigDecimal0);
        setSIDiscountAmount3(bigDecimal0);
        setSIDiscountAmount4(bigDecimal0);
        setSIDiscountCount0(0);
        setSIDiscountCount1(0);
        setSIDiscountCount2(0);
        setSIDiscountCount3(0);
        setSIDiscountCount4(0);
        setSIPercentPlusAmount0(bigDecimal0);
        setSIPercentPlusAmount1(bigDecimal0);
        setSIPercentPlusAmount2(bigDecimal0);
        setSIPercentPlusAmount3(bigDecimal0);
        setSIPercentPlusAmount4(bigDecimal0);
        setSIPercentPlusCount0(0);
        setSIPercentPlusCount1(0);
        setSIPercentPlusCount2(0);
        setSIPercentPlusCount3(0);
        setSIPercentPlusCount4(0);
        setSIPercentOffAmount0(bigDecimal0);
        setSIPercentOffAmount1(bigDecimal0);
        setSIPercentOffAmount2(bigDecimal0);
        setSIPercentOffAmount3(bigDecimal0);
        setSIPercentOffAmount4(bigDecimal0);
        setSIPercentOffCount0(0);
        setSIPercentOffCount1(0);
        setSIPercentOffCount2(0);
        setSIPercentOffCount3(0);
        setSIPercentOffCount4(0);
    }

    public void saveOriginalLineItemAfterDiscountAmount() {
        getLineItemsArrayLast();
        Iterator itr = getLineItems();
        if (itr == null)
            return;
        while (itr.hasNext()) {
            LineItem li = (LineItem) itr.next();
            if (!li.getRemoved() && li.getDetailCode().equals("S"))
                li.setOriginalAfterDiscountAmount(li.getAfterDiscountAmount());
        }
    }

    public void restoreOriginalLineItemAfterDiscountAmount() {
        getLineItemsArrayLast();
        Iterator itr = getLineItems();
        if (itr == null)
            return;
        while (itr.hasNext()) {
            LineItem li = (LineItem) itr.next();
            if (!li.getRemoved() && li.getDetailCode().equals("S"))
                  li.setAfterDiscountAmount(li.getOriginalAfterDiscountAmount());
        
            	
        }
    }

    public void accumulate() {
    	CreamToolkit.logMessage("in accumulate()");
        getLineItemsArrayLast();
        Iterator itr = getLineItems();
        if (itr == null) {
        	CreamToolkit.logMessage("itr == null no accumulation");
        	return;
        }
        BigDecimal initialValue = bigDecimal0;
        while (itr.hasNext()) {
            LineItem li = (LineItem) itr.next();
            CreamToolkit.logMessage("li.getItemNumber():" + li.getItemNumber());
            CreamToolkit.logMessage("li.getDetailCode():" + li.getDetailCode());
            if (!li.getRemoved())
                if (li.getDetailCode().equals("I"))
                    setDaiShouAmount(getDaiShouAmount().add(li.getAmount()));
                else if (li.getDetailCode().equals("O"))
                    setDaiShouAmount2(getDaiShouAmount2().add(li.getAmount()));
                else if (li.getDetailCode().equals("Q")) {
                    setDaiFuAmount(getDaiFuAmount().add(li.getUnitPrice()));
                    initialValue = initialValue.add(li.getAmount());
                } else if (!li.getDetailCode().equals("M"))
                    if (li.getDetailCode().equals("N"))
                        setDaiShouAmount(getDaiShouAmount().add(
                                li.getUnitPrice()));
                    else if (li.getDetailCode().equals("T"))
                        setDaiFuAmount(getDaiFuAmount().add(li.getUnitPrice()));
                    else {
                    	CreamToolkit.logMessage("before accumGrossNetAndItemDiscountAmountByTaxType");
                    	accumGrossNetAndItemDiscountAmountByTaxType(li.getTaxType(), li);
                    	CreamToolkit.logMessage("after accumGrossNetAndItemDiscountAmountByTaxType");
                    }
        }
        if (mixAndMatchVersion == null || mixAndMatchVersion.trim().equals("")
                || mixAndMatchVersion.trim().equals("1"))
            updateTaxMM();
        BigDecimal gross = bigDecimal0;
        gross = gross.add(getGrossSalesTax0Amount());
        gross = gross.add(getGrossSalesTax1Amount());
        gross = gross.add(getGrossSalesTax2Amount());
        gross = gross.add(getGrossSalesTax3Amount());
        gross = gross.add(getGrossSalesTax4Amount());
        setGrossSalesAmount(gross);
        CreamToolkit.logMessage("accumulate() setGrossSalesAmount(gross) = "+gross);
        if (getNetSalesAmount0() == null)
            setNetSalesAmount0(bigDecimal0.setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount1() == null)
            setNetSalesAmount1(bigDecimal0.setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount2() == null)
            setNetSalesAmount2(bigDecimal0.setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount3() == null)
            setNetSalesAmount3(bigDecimal0.setScale(2, BigDecimal.ROUND_HALF_UP));
        if (getNetSalesAmount4() == null)
            setNetSalesAmount4(bigDecimal0.setScale(2, BigDecimal.ROUND_HALF_UP));

        setNetSalesAmount0(getNetSalesAmount0()
                 .subtract(getMixAndMatchAmount0())
                 .subtract(getSIPercentOffAmount0())
                 .subtract(getSIPercentPlusAmount0())
                 .subtract(getSIDiscountAmount0())
                 .subtract(getItemDiscAmount0())
                 .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        CreamToolkit.logMessage("accumulate() setNetSalesAmount0() = "+getNetSalesAmount0()
                .subtract(getMixAndMatchAmount0())
                .subtract(getSIPercentOffAmount0())
                .subtract(getSIPercentPlusAmount0())
                .subtract(getSIDiscountAmount0())
                .subtract(getItemDiscAmount0())
                .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        
        setNetSalesAmount1(getNetSalesAmount1()
                 .subtract(getMixAndMatchAmount1())
                 .subtract(getSIPercentOffAmount1())
                 .subtract(getSIPercentPlusAmount1())
                 .subtract(getSIDiscountAmount1())
                 .subtract(getItemDiscAmount1())
                 .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        CreamToolkit.logMessage("accumulate() setNetSalesAmount1() = "+getNetSalesAmount1()
                .subtract(getMixAndMatchAmount1())
                .subtract(getSIPercentOffAmount1())
                .subtract(getSIPercentPlusAmount1())
                .subtract(getSIDiscountAmount1())
                .subtract(getItemDiscAmount1())
                .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        
        setNetSalesAmount2(getNetSalesAmount2()
                 .subtract(getMixAndMatchAmount2())
                 .subtract(getSIPercentOffAmount2())
                 .subtract(getSIPercentPlusAmount2())
                 .subtract(getSIDiscountAmount2())
                 .subtract(getItemDiscAmount2())
                 .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        CreamToolkit.logMessage("accumulate() setNetSalesAmount1() = "+getNetSalesAmount2()
                .subtract(getMixAndMatchAmount2())
                .subtract(getSIPercentOffAmount2())
                .subtract(getSIPercentPlusAmount2())
                .subtract(getSIDiscountAmount2())
                .subtract(getItemDiscAmount2())
                .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        
        setNetSalesAmount3(getNetSalesAmount3()
                 .subtract(getMixAndMatchAmount3())
                 .subtract(getSIPercentOffAmount3())
                 .subtract(getSIPercentPlusAmount3())
                 .subtract(getSIDiscountAmount3())
                 .subtract(getItemDiscAmount3())
                 .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        CreamToolkit.logMessage("accumulate() setNetSalesAmount1() = "+getNetSalesAmount3()
                .subtract(getMixAndMatchAmount3())
                .subtract(getSIPercentOffAmount3())
                .subtract(getSIPercentPlusAmount3())
                .subtract(getSIDiscountAmount3())
                .subtract(getItemDiscAmount3())
                .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        
        setNetSalesAmount4(getNetSalesAmount4()
                 .subtract(getMixAndMatchAmount4())
                 .subtract(getSIPercentOffAmount4())
                 .subtract(getSIPercentPlusAmount4())
                 .subtract(getSIDiscountAmount4())
                 .subtract(getItemDiscAmount4())
                 .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        CreamToolkit.logMessage("accumulate() setNetSalesAmount1() = "+getNetSalesAmount4()
                .subtract(getMixAndMatchAmount4())
                .subtract(getSIPercentOffAmount4())
                .subtract(getSIPercentPlusAmount4())
                .subtract(getSIDiscountAmount4())
                .subtract(getItemDiscAmount4())
                .setScale(2, BigDecimal.ROUND_HALF_DOWN));
        
        setNetSalesAmount(getNetSalesAmount0()
                .add(getNetSalesAmount1())
                .add(getNetSalesAmount2())
                .add(getNetSalesAmount3())
                .add(getNetSalesAmount4()));
        
        CreamToolkit.logMessage("accumulate() setNetSalesAmount() = "+getNetSalesAmount0()
                .add(getNetSalesAmount1())
                .add(getNetSalesAmount2())
                .add(getNetSalesAmount3())
                .add(getNetSalesAmount4()));
        updateSIAmt(initialValue);
    }

    public void updateSIAmt(BigDecimal b) {
        BigDecimal sales = bigDecimal0;
        Iterator itra = getSIAmtList().values().iterator();
        if (mixAndMatchVersion == null || mixAndMatchVersion.trim().equals("")
                || mixAndMatchVersion.trim().equals("1"))
            sales = getGrossSalesAmount().subtract(getTaxMMAmount());
        else if (mixAndMatchVersion.equals("2"))
            sales = getGrossSalesAmount().subtract(getMixAndMatchTotalAmount());
        while (itra.hasNext())
            sales = sales.add((BigDecimal) itra.next());
        if (getDealType1().equals("0")
                && getDealType2().equals("0")
                && (getDealType3().equals("3") || getDealType3().equals("2") || getDealType3()
                        .equals("1")))
            setSalesAmount(sales.add(b));
        else
            setSalesAmount(sales.add(b).add(getDaiShouAmount()).add(
                    getDaiShouAmount2()).add(getDaiFuAmount()));
    }

    /** 將單品金額累加到分稅別的銷售毛額、淨額、和單品折扣金額 */
    public void accumGrossNetAndItemDiscountAmountByTaxType(String taxType, LineItem lineItem) {
        if (taxType == null || taxType.trim().length() == 0) 
            return;

        if ("M".equals(lineItem.getDetailCode()))
            return;

        //Bruce/20080726/ 拿掉看不懂的代碼
        /*
        if (!lineItem.getDetailCode().equals("S")) {
            if (lineItem.getDetailCode().equals("M"))
                return;
            if (!lineItem.getDetailCode().equals("D"))
                lineItem.getDetailCode().equals("V");
        }
        */

        //Bruce/20080726/ 只有做單品折扣的LineItem才需要計算單品折扣金額和次數
        boolean isItemDiscount = "C".equals(lineItem.getDiscountType());

        BigDecimal amount = lineItem.getAmount();
        BigDecimal itemDsctAmt = lineItem.getItemDsctAmt();

        Integer itemDsctCnt;
        try {
            itemDsctCnt = new Integer(lineItem.getItemDsctCnt().intValue());
        } catch (Exception e) {
            itemDsctCnt = new Integer("0");
        }
        
        if ("0".equals(taxType)) {
            setGrossSalesTax0Amount(getGrossSalesTax0Amount().add(amount));
            CreamToolkit.logMessage("setGrossSalesTax0Amount(getGrossSalesTax0Amount().add(amount))= "+getGrossSalesTax0Amount().add(amount)+
            		"amount ="+amount);
            setNetSalesAmount0(getNetSalesAmount0().add(amount));
            CreamToolkit.logMessage("setNetSalesAmount0(getNetSalesAmount0().add(amount))= "+getNetSalesAmount0().add(amount)+
            		"amount ="+amount);
            if (isItemDiscount) {
                setItemDiscAmount0(getItemDiscAmount0().add(itemDsctAmt));
                setItemDiscCnt0(getItemDiscCnt0().intValue() + itemDsctCnt.intValue());
            }
        } else if ("1".equals(taxType)) {
            setGrossSalesTax1Amount(getGrossSalesTax1Amount().add(amount));
            CreamToolkit.logMessage("setGrossSalesTax1Amount(getGrossSalesTax1Amount().add(amount))= "+getGrossSalesTax1Amount().add(amount)+
            		"amount ="+amount);
            setNetSalesAmount1(getNetSalesAmount1().add(amount));
            CreamToolkit.logMessage("setNetSalesAmount1(getNetSalesAmount1().add(amount))= "+getNetSalesAmount1().add(amount)+
            		"amount ="+amount);
            if (isItemDiscount) {
                setItemDiscAmount1(getItemDiscAmount1().add(itemDsctAmt));
                setItemDiscCnt1(getItemDiscCnt1().intValue() + itemDsctCnt.intValue());
            }
        } else if ("2".equals(taxType)) {
            setGrossSalesTax2Amount(getGrossSalesTax2Amount().add(amount));
            CreamToolkit.logMessage("setNetSalesAmount2(getNetSalesAmount2().add(amount))= "+getNetSalesAmount2().add(amount)+
            		"amount ="+amount);
            setNetSalesAmount2(getNetSalesAmount2().add(amount));
            CreamToolkit.logMessage("setNetSalesAmount2(getNetSalesAmount2().add(amount))= "+getNetSalesAmount2().add(amount)+
            		"amount ="+amount);
            if (isItemDiscount) {
                setItemDiscAmount2(getItemDiscAmount2().add(itemDsctAmt));
                setItemDiscCnt2(getItemDiscCnt2().intValue() + itemDsctCnt.intValue());
            }
        } else if ("3".equals(taxType)) {
            setGrossSalesTax3Amount(getGrossSalesTax3Amount().add(amount));
            CreamToolkit.logMessage("setNetSalesAmount3(getNetSalesAmount3().add(amount))= "+getNetSalesAmount3().add(amount)+
            		"amount ="+amount);
            setNetSalesAmount3(getNetSalesAmount3().add(amount));
            CreamToolkit.logMessage("setNetSalesAmount3(getNetSalesAmount3().add(amount))= "+getNetSalesAmount3().add(amount)+
            		"amount ="+amount);
            if (isItemDiscount) {
                setItemDiscAmount3(getItemDiscAmount3().add(itemDsctAmt));
                setItemDiscCnt3(getItemDiscCnt3().intValue() + itemDsctCnt.intValue());
            }
        } else if ("4".equals(taxType)) {
            setGrossSalesTax4Amount(getGrossSalesTax4Amount().add(amount));
            CreamToolkit.logMessage("setNetSalesAmount4(getNetSalesAmount4().add(amount))= "+getNetSalesAmount4().add(amount)+
            		"amount ="+amount);
            setNetSalesAmount4(getNetSalesAmount4().add(amount));
            CreamToolkit.logMessage("setNetSalesAmount4(getNetSalesAmount4().add(amount))= "+getNetSalesAmount4().add(amount)+
            		"amount ="+amount);
            if (isItemDiscount) {
                setItemDiscAmount4(getItemDiscAmount4().add(itemDsctAmt));
                setItemDiscCnt4(getItemDiscCnt4().intValue() + itemDsctCnt.intValue());
            }
        }
    }

    public BigDecimal getYingFuAmount() {
        return getSalesAmount().add(getDaiFuAmount()).add(getDaiShouAmount());
    }

    public BigDecimal getSalesAmount() {
        if (Server.serverExist())
            return bigDecimal0;
        BigDecimal saleAmt = (BigDecimal) getFieldValue("SALEAMT");
        if (saleAmt == null)
            return bigDecimal0;
        else
            return saleAmt;
    }

    //gllg
    public String getLineItemDsctAmtTotal(){
        BigDecimal total = new BigDecimal("0.00");
        LineItem  currLineItem;
        for(int i = 0; i < currentTransaction.getLineItemsArrayLast().length; i++){
           currLineItem = ((LineItem)currentTransaction.getLineItemsArrayLast()[i]);
            //System.out.println("total:"+total);
            if (!isReturnAll() && getDealType3().equals("4")) { // 部分退货
				// 部分退货的单品折扣总额只累加被退单品的单品折扣金额
                if (currLineItem.getRemoved()) {
                    total = total.add(currLineItem.getDiscountAmount().negate());
                }

            } else { // 计算正常交易或全退交易的单品折扣总额
                if (currLineItem.getDiscountType() != null && currLineItem.getDiscountType().equalsIgnoreCase("C") ){
                    //System.out.println("removed:"+currLineItem.getRemoved() + ",discountAmount="+currLineItem.getDiscountAmount());
					// 但必须排除被更正的商品
                    if (!currLineItem.getRemoved()){
                        total = total.add(currLineItem.getDiscountAmount());
                    }
                        
                }
            }
        }
        return total.toString();
    }
    
    //gllg
    public String getLineItemDsctAmtTotal4Display(){
        BigDecimal total = new BigDecimal("0.00");
        LineItem  currLineItem;
        for(int i = 0; i < currentTransaction.getLineItemsArrayLast().length; i++){
            currLineItem = ((LineItem)currentTransaction.getLineItemsArrayLast()[i]);
            System.out.println("total:"+total);
            if(getDealType3().equals("4")){
                if (!currLineItem.getRemoved()) {
                    if ("C".equals(currLineItem.getDiscountType()))
                        total = total.add(currLineItem.getDiscountAmount().negate());
                }
            } else {
                if (currLineItem.getDiscountType() != null && currLineItem.getDiscountType().equalsIgnoreCase("C") ){
                    System.out.println("removed:"+currLineItem.getRemoved() + ",discountAmount="+currLineItem.getDiscountAmount());
                    if (!currLineItem.getRemoved())
                        total = total.add(currLineItem.getDiscountAmount());
                }
            }
        }
        return total.toString();
        
    }
    
    
    
    //gllg
    public BigDecimal getSalesAmtWithItemDsct(){
        String  lineItemDsctAmtTotal = "";
        try {
            lineItemDsctAmtTotal = getLineItemDsctAmtTotal();
        } catch (Exception e) {
            lineItemDsctAmtTotal = "0.00";
        }
        
        return getSalesAmount().add(new BigDecimal(lineItemDsctAmtTotal).negate()).setScale(2, BigDecimal.ROUND_HALF_UP);
    }
    
    public void setSalesAmount(BigDecimal saleamt) {
        if (!Server.serverExist())
            setFieldValue("SALEAMT", saleamt);
    }

    public BigDecimal getDaiShouAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiShouAmount");
        else
            return (BigDecimal) getFieldValue("DAISHOUAMT");
    }

    public void setDaiShouAmount(BigDecimal daishouamt) {
        if (Server.serverExist())
            setFieldValue("daiShouAmount", daishouamt);
        else
            setFieldValue("DAISHOUAMT", daishouamt);
    }

    public BigDecimal getDaiShouAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiShouAmount2");
        else
            return (BigDecimal) getFieldValue("DAISHOUAMT2");
    }

    public void setDaiShouAmount2(BigDecimal daishouamt) {
        if (Server.serverExist())
            setFieldValue("daiShouAmount2", daishouamt);
        else
            setFieldValue("DAISHOUAMT2", daishouamt);
    }

    public BigDecimal getDaiFuAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiFuAmount");
        else
            return (BigDecimal) getFieldValue("DAIFUAMT");
    }

    public void setDaiFuAmount(BigDecimal daifuamt) {
        if (Server.serverExist())
            setFieldValue("daiFuAmount", daifuamt);
        else
            setFieldValue("DAIFUAMT", daifuamt);
    }

    public String getPayName1() {
        Payment payment = Payment.queryByPaymentID(getPayNumber1());
        if (payment != null)
            return payment.getPrintName();
        else
            return null;
    }

    public String getPayName2() {
        Payment payment = Payment.queryByPaymentID(getPayNumber2());
        if (payment != null)
            return payment.getPrintName();
        else
            return null;
    }

    public String getPayName3() {
        Payment payment = Payment.queryByPaymentID(getPayNumber3());
        if (payment != null)
            return payment.getPrintName();
        else
            return null;
    }

    public String getPayName4() {
        Payment payment = Payment.queryByPaymentID(getPayNumber4());
        if (payment != null)
            return payment.getPrintName();
        else
            return null;
    }

    public String getPayNumber1() {
        if (Server.serverExist())
            return (String) getFieldValue("payID1");
        else
            return (String) getFieldValue("PAYNO1");
    }

    public void setPayNumber1(String payno1) {
        if (Server.serverExist())
            setFieldValue("payID1", payno1);
        else
            setFieldValue("PAYNO1", payno1);
    }

    public String getPayNumber2() {
        if (Server.serverExist())
            return (String) getFieldValue("payID2");
        else
            return (String) getFieldValue("PAYNO2");
    }

    public void setPayNumber2(String payno2) {
        if (Server.serverExist())
            setFieldValue("payID2", payno2);
        else
            setFieldValue("PAYNO2", payno2);
    }

    public String getPayNumber3() {
        if (Server.serverExist())
            return (String) getFieldValue("payID3");
        else
            return (String) getFieldValue("PAYNO3");
    }

    public void setPayNumber3(String payno3) {
        if (Server.serverExist())
            setFieldValue("payID3", payno3);
        else
            setFieldValue("PAYNO3", payno3);
    }

    public String getPayNumber4() {
        if (Server.serverExist())
            return (String) getFieldValue("payID4");
        else
            return (String) getFieldValue("PAYNO4");
    }

    public void setPayNumber4(String payno4) {
        if (Server.serverExist())
            setFieldValue("payID4", payno4);
        else
            setFieldValue("PAYNO4", payno4);
    }

    public BigDecimal getTotalPaymentAmount() {
        BigDecimal total = ZERO;
        if (getPayAmount1() != null)
            total = total.add(getPayAmount1());
        if (getPayAmount2() != null)
            total = total.add(getPayAmount2());
        if (getPayAmount3() != null)
            total = total.add(getPayAmount3());
        if (getPayAmount4() != null)
            total = total.add(getPayAmount4());
        return total;
    }

    public BigDecimal getActualReceivedPaymentAmount() {
        BigDecimal total = ZERO;
        if (getPayAmount1() != null && getPayName1() != null
                && getPayName1().indexOf("\u8D4A") == -1
                && (!getPayNumber1().equals(rebateID) || !excudeRebateInSum))
            total = total.add(getPayAmount1());
        if (getPayAmount2() != null && getPayName2() != null
                && getPayName2().indexOf("\u8D4A") == -1
                && (!getPayNumber2().equals(rebateID) || !excudeRebateInSum))
            total = total.add(getPayAmount2());
        if (getPayAmount3() != null && getPayName3() != null
                && getPayName3().indexOf("\u8D4A") == -1
                && (!getPayNumber3().equals(rebateID) || !excudeRebateInSum))
            total = total.add(getPayAmount3());
        if (getPayAmount4() != null && getPayName4() != null
                && getPayName4().indexOf("\u8D4A") == -1
                && (!getPayNumber4().equals(rebateID) || !excudeRebateInSum))
            total = total.add(getPayAmount4());
        return total;
    }

    public String getPaymentDescription() {
        StringBuffer desc = new StringBuffer();
        String payName;
        if ((payName = getPayName1()) != null
                && (!getPayNumber1().equals(rebateID) || !excudeRebateInSum)) {
            desc.append(payName);
            desc.append(':');
            desc.append(getPayAmount1());
            desc.append("  ");
        }
        if ((payName = getPayName2()) != null
                && (!getPayNumber2().equals(rebateID) || !excudeRebateInSum)) {
            desc.append(payName);
            desc.append(':');
            desc.append(getPayAmount2());
            desc.append("  ");
        }
        if ((payName = getPayName3()) != null
                && (!getPayNumber3().equals(rebateID) || !excudeRebateInSum)) {
            desc.append(payName);
            desc.append(':');
            desc.append(getPayAmount3());
            desc.append("  ");
        }
        if ((payName = getPayName4()) != null
                && (!getPayNumber4().equals(rebateID) || !excudeRebateInSum)) {
            desc.append(payName);
            desc.append(':');
            desc.append(getPayAmount4());
            desc.append("  ");
        }
        return desc.toString();
    }

    public BigDecimal getPrintNetSaleAmount() {
        BigDecimal netSaleAmt = getNetSalesAmount();
        if (excudeRebateInSum) {
            BigDecimal useRebateAmt = getUseRebateAmt();
            netSaleAmt = netSaleAmt.subtract(useRebateAmt);
        }
        return netSaleAmt;
    }

    public BigDecimal getPayAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("payAmount1");
        else
            return (BigDecimal) getFieldValue("PAYAMT1");
    }

    public void setPayAmount1(BigDecimal payamt1) {
        if (Server.serverExist())
            setFieldValue("payAmount1", payamt1);
        else
            setFieldValue("PAYAMT1", payamt1);
    }

    public BigDecimal getPayAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("payAmount2");
        else
            return (BigDecimal) getFieldValue("PAYAMT2");
    }

    public void setPayAmount2(BigDecimal payamt2) {
        if (Server.serverExist())
            setFieldValue("payAmount2", payamt2);
        else
            setFieldValue("PAYAMT2", payamt2);
    }

    public BigDecimal getPayAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("payAmount3");
        else
            return (BigDecimal) getFieldValue("PAYAMT3");
    }

    public void setPayAmount3(BigDecimal payamt3) {
        if (Server.serverExist())
            setFieldValue("payAmount3", payamt3);
        else
            setFieldValue("PAYAMT3", payamt3);
    }

    public BigDecimal getPayAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("payAmount4");
        else
            return (BigDecimal) getFieldValue("PAYAMT4");
    }

    public void setPayAmount4(BigDecimal payamt4) {
        if (Server.serverExist())
            setFieldValue("payAmount4", payamt4);
        else
            setFieldValue("PAYAMT4", payamt4);
    }

    public BigDecimal getPayCash() {
        String s = "";
        if (Server.serverExist()) {
            if (getPayNumber1() != null && getPayNumber1().equals("00"))
                s = "payAmount1";
            else if (getPayNumber2() != null && getPayNumber2().equals("00"))
                s = "payAmount2";
            else if (getPayNumber3() != null && getPayNumber3().equals("00"))
                s = "payAmount3";
            else if (getPayNumber4() != null && getPayNumber4().equals("00"))
                s = "payAmount4";
        } else if (getPayNumber1() != null && getPayNumber1().equals("00"))
            s = "PAYAMT1";
        else if (getPayNumber2() != null && getPayNumber2().equals("00"))
            s = "PAYAMT2";
        else if (getPayNumber3() != null && getPayNumber3().equals("00"))
            s = "PAYAMT3";
        else if (getPayNumber4() != null && getPayNumber4().equals("00"))
            s = "PAYAMT4";
        return (BigDecimal) getFieldValue(s);
    }

    public void setPayCash(BigDecimal payCash) {
        String s = "";
        if (Server.serverExist()) {
            if (getPayNumber1() != null && getPayNumber1().equals("00"))
                s = "payAmount1";
            else if (getPayNumber2() != null && getPayNumber2().equals("00"))
                s = "payAmount2";
            else if (getPayNumber3() != null && getPayNumber3().equals("00"))
                s = "payAmount3";
            else if (getPayNumber4() != null && getPayNumber4().equals("00"))
                s = "payAmount4";
        } else if (getPayNumber1() != null && getPayNumber1().equals("00"))
            s = "PAYAMT1";
        else if (getPayNumber2() != null && getPayNumber2().equals("00"))
            s = "PAYAMT2";
        else if (getPayNumber3() != null && getPayNumber3().equals("00"))
            s = "PAYAMT3";
        else if (getPayNumber4() != null && getPayNumber4().equals("00"))
            s = "PAYAMT4";
        setFieldValue(s, payCash);
    }

    public BigDecimal getTaxAmount0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("tax0Amount");
        else
            return (BigDecimal) getFieldValue("TAXAMT0");
    }

    public void setTaxAmount0(BigDecimal amt) {
        if (Server.serverExist())
            setFieldValue("tax0Amount", amt);
        else
            setFieldValue("TAXAMT0", amt);
    }

    public BigDecimal getTaxAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("tax1Amount");
        else
            return (BigDecimal) getFieldValue("TAXAMT1");
    }

    public void setTaxAmount1(BigDecimal amt) {
        if (Server.serverExist())
            setFieldValue("tax1Amount", amt);
        else
            setFieldValue("TAXAMT1", amt);
    }

    public BigDecimal getTaxAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("tax2Amount");
        else
            return (BigDecimal) getFieldValue("TAXAMT2");
    }

    public void setTaxAmount2(BigDecimal amt) {
        if (Server.serverExist())
            setFieldValue("tax2Amount", amt);
        else
            setFieldValue("TAXAMT2", amt);
    }

    public BigDecimal getTaxAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("tax3Amount");
        else
            return (BigDecimal) getFieldValue("TAXAMT3");
    }

    public void setTaxAmount3(BigDecimal amt) {
        if (Server.serverExist())
            setFieldValue("tax3Amount", amt);
        else
            setFieldValue("TAXAMT3", amt);
    }

    public BigDecimal getTaxAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("tax4Amount");
        else
            return (BigDecimal) getFieldValue("TAXAMT4");
    }

    public void setTaxAmount4(BigDecimal amt) {
        if (Server.serverExist())
            setFieldValue("tax4Amount", amt);
        else
            setFieldValue("TAXAMT4", amt);
    }

    public BigDecimal getChangeAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("changeAmount");
        else
            return (BigDecimal) getFieldValue("CHANGEAMT");
    }

    public void setChangeAmount(BigDecimal change) {
        if (Server.serverExist())
            setFieldValue("changeAmount", change);
        else
            setFieldValue("CHANGEAMT", change);
    }

    public BigDecimal getSpillAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("spillAmount");
        else
            return (BigDecimal) getFieldValue("OVERAMT");
    }

    public void setSpillAmount(BigDecimal spillAmount) {
        if (Server.serverExist())
            setFieldValue("spillAmount", spillAmount);
        else
            setFieldValue("OVERAMT", spillAmount);
    }

    public String getCreditCardType() {
        if (Server.serverExist())
            return (String) getFieldValue("creditCardType");
        else
            return (String) getFieldValue("CRDTYPE");
    }

    public void setCreditCardType(String crdtype) {
        if (Server.serverExist())
            setFieldValue("creditCardType", crdtype);
        else
            setFieldValue("CRDTYPE", crdtype);
    }

    public String getCreditCardNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("creditCardNumber");
        else
            return (String) getFieldValue("CRDNO");
    }

    public void setCreditCardNumber(String crdno) {
        if (Server.serverExist())
            setFieldValue("creditCardNumber", crdno);
        else
            setFieldValue("CRDNO", crdno);
    }

    public Date getCreditCardExpireDate() {
        if (Server.serverExist())
            return (Date) getFieldValue("creditCardExpireDate");
        else
            return (Date) getFieldValue("CRDEND");
    }

    public void setCreditCardExpireDate(Date crdend) {
        if (Server.serverExist())
            setFieldValue("creditCardExpireDate", crdend);
        else
            setFieldValue("CRDEND", crdend);
    }

    public String getEmployeeNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("employeeNumber");
        else
            return (String) getFieldValue("EMPNO");
    }

    public void setEmployeeNumber(String empno) {
        if (Server.serverExist())
            setFieldValue("employeeNumber", empno);
        else
            setFieldValue("EMPNO", empno);
    }

    public String getMemberID() {
        if (Server.serverExist())
            return (String) getFieldValue("memberID");
        else
            return (String) getFieldValue("MEMBERID");
    }

    public void setMemberID(String memberid) {
        if (Server.serverExist())
            setFieldValue("memberID", memberid);
        else
            setFieldValue("MEMBERID", memberid);
    }

    public BigDecimal getExchangeDifference() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("exchangeDifference");
        else
            return (BigDecimal) getFieldValue("CHANGAMT");
    }

    public void setExchangeDifference(BigDecimal changamt) {
        if (Server.serverExist())
            setFieldValue("exchangeDifference", changamt);
        else
            setFieldValue("CHANGAMT", changamt);
    }

    public String getAuthoringNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("authoringNumber");
        else
            return (String) getFieldValue("AUTHORNO");
    }

    public void setAuthoringNumber(String authorno) {
        if (Server.serverExist())
            setFieldValue("authoringNumber", authorno);
        else
            setFieldValue("AUTHORNO", authorno);
    }

    public BigDecimal getRebateChangeAmount() {
        return (BigDecimal) getFieldValue("rebateChangeAmount");
    }

    public void setRebateChangeAmount(BigDecimal rebateChangeAmt) {
        setFieldValue("rebateChangeAmount", rebateChangeAmt);
    }

    public void setItemsRebateAmount(BigDecimal itemsRebateAmt) {
        itemRebateAmt = itemsRebateAmt;
    }

    public BigDecimal getItemsRebateAmount() {
        return itemRebateAmt;
    }

    public void setSpecialRebateAmount(BigDecimal splRebateAmt) {
        specialRebateAmt = splRebateAmt;
    }

    public BigDecimal getSpecialRebateAmount() {
        return specialRebateAmt;
    }

    public BigDecimal getRebateAmount() {
        return (BigDecimal) getFieldValue("rebateAmount");
    }

    public void setRebateAmount(BigDecimal rebateAmt) {
        setFieldValue("rebateAmount", rebateAmt);
    }

    public BigDecimal getTotalRebateAmount() {
        return (BigDecimal) getFieldValue("totalRebateAmount");
    }

    public void setTotalRebateAmount(BigDecimal totalRebateAmt) {
        setFieldValue("totalRebateAmount", totalRebateAmt);
    }

    public void setBeforeTotalRebateAmt(BigDecimal amt) {
        beforeTotalRebate = amt;
    }

    public BigDecimal getBeforeTotalRebateAmt() {
        return beforeTotalRebate;
    }

    public void setUseRebateAmt(BigDecimal amt) {
        useRebateAmt = amt;
    }

    public BigDecimal getUseRebateAmt() {
        return useRebateAmt;
    }

    public String getNeedPrintRebate() {
        BigDecimal big0 = ZERO;
        if (getRebateAmount().compareTo(big0) != 0
                || getUseRebateAmt().compareTo(big0) != 0
                || getBeforeTotalRebateAmt().compareTo(big0) != 0)
            return "true";
        else
            return null;
    }

    public boolean isPureDaiShou2() {
        boolean isPureDaiShou2 = true;
        for (Iterator itr = getLineItems(); itr.hasNext();) {
            LineItem li = (LineItem) itr.next();
            if (!li.getRemoved()
                    && (li.getDetailCode().equals("S") || li.getDetailCode()
                            .equals("R"))) {
                isPureDaiShou2 = false;
                break;
            }
        }

        return isPureDaiShou2;
    }

    public Hashtable getSIAmtList() {
        return siAmtList;
    }

    public void setSIAmtList(String siID, BigDecimal amt) {
        if (amt == null || siID == null) {
            siAmtList.clear();
            lastSIID = "";
            return;
        } else {
            siAmtList.put(siID, amt);
            lastSIID = siID;
            return;
        }
    }

    public String getLastSIID() {
        return lastSIID;
    }

    public BigDecimal getBalance(String paymentID) {
        BigDecimal balance = ZERO;
        BigDecimal payAmount = ZERO;
        String payno = "";
        String payamt = "";
        for (int i = 1; i < 5; i++) {
            if (Server.serverExist()) {
                payno = "PayID" + i;
                payamt = "PayAmount" + i;
            } else {
                payno = "PAYNO" + i;
                payamt = "PAYAMT" + i;
            }
            if (getFieldValue(payno) == null)
                break;
            if (((String) getFieldValue(payno)).equals(paymentID)
                    && getFieldValue(payamt) != null)
                payAmount = payAmount.add((BigDecimal) getFieldValue(payamt));
        }

        balance = getSalesAmount().subtract(payAmount);
        return balance;
    }

    public BigDecimal getBalance() {
        BigDecimal balance = ZERO;
        BigDecimal payAmount = ZERO;
        String payno = "";
        String payamt = "";
        for (int i = 1; i < 5; i++) {
            if (Server.serverExist()) {
                payno = "PayID" + i;
                payamt = "PayAmount" + i;
            } else {
                payno = "PAYNO" + i;
                payamt = "PAYAMT" + i;
            }
            if (getFieldValue(payno) == null)
                break;
            if (getFieldValue(payamt) != null)
                payAmount = payAmount.add((BigDecimal) getFieldValue(payamt));
        }

        balance = getSalesAmount().subtract(payAmount);
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        if (!app.getReturnItemState() && balance.compareTo(bigDecimal0) < 0
                || app.getReturnItemState()
                && balance.compareTo(bigDecimal0) > 0)
            balance = bigDecimal0;
        return balance;
    }
    
    public BigDecimal getBalanceWithItemDsct() {
        BigDecimal balance = ZERO;
        BigDecimal payAmount = ZERO;
        String payno = "";
        String payamt = "";
        for (int i = 1; i < 5; i++) {
                payno = "PAYNO" + i;
                payamt = "PAYAMT" + i;
            if (getFieldValue(payno) == null)
                break;
            if (getFieldValue(payamt) != null)
                payAmount = payAmount.add((BigDecimal) getFieldValue(payamt));
        }

        balance = getSalesAmtWithItemDsct().subtract(payAmount);
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        if (!app.getReturnItemState() && balance.compareTo(bigDecimal0) < 0
                || app.getReturnItemState()
                && balance.compareTo(bigDecimal0) > 0)
            balance = bigDecimal0;
        return balance;
    }

    public BigDecimal getItemsAmount() {
        Object objs[] = getLineItemsArrayLast();
        int l = objs.length;
        if (l == 0)
            return bigDecimal0;
        LineItem item = null;
        String detailCode = "";
        BigDecimal amount = bigDecimal0;
        for (int i = 0; i < l; i++) {
            item = (LineItem) objs[i];
            if (!item.getRemoved()) {
                detailCode = item.getDetailCode();
                if (!detailCode.equalsIgnoreCase("E")
                        && !detailCode.equalsIgnoreCase("V")
                        && !detailCode.equalsIgnoreCase("O")
                        && !detailCode.equalsIgnoreCase("I"))
                    amount = amount.add(item.getAmount());
            }
        }

        return amount;
    }

    public String getSIDescription() {
        StringBuffer sb = new StringBuffer("");
        Hashtable siHt = getSIAmtList();
        for (Enumeration siKey = siHt.keys(); siKey.hasMoreElements(); sb
                .append("  ")) {
            String siID = (String) siKey.nextElement();
            String siName = SI.queryBySIID(siID).getPrintName();
            sb.append(siName);
            sb.append(":");
            sb.append(((BigDecimal) siHt.get(siID)).toString());
        }

        return new String(sb);
    }

    public BigDecimal getTotalSIAmount() {
        BigDecimal total = ZERO;
        Hashtable siHt = getSIAmtList();
        for (Enumeration siKey = siHt.keys(); siKey.hasMoreElements();) {
            String siID = (String) siKey.nextElement();
            total = total.add((BigDecimal) siHt.get(siID));
        }

        return total;
    }

    public BigDecimal getSIPercentPlusTotalAmount() {
        BigDecimal total = getSIPercentPlusAmount0();
        total = total.add(getSIPercentPlusAmount1());
        total = total.add(getSIPercentPlusAmount2());
        total = total.add(getSIPercentPlusAmount3());
        total = total.add(getSIPercentPlusAmount4());
        return total;
    }

    public BigDecimal getSIPercentOffTotalAmount() {
        BigDecimal total = getSIPercentOffAmount0();
        total = total.add(getSIPercentOffAmount1());
        total = total.add(getSIPercentOffAmount2());
        total = total.add(getSIPercentOffAmount3());
        total = total.add(getSIPercentOffAmount4());
        return total;
    }

    public BigDecimal getSIDiscountTotalAmount() {
        BigDecimal total = getSIDiscountAmount0();
        total = total.add(getSIDiscountAmount1());
        total = total.add(getSIDiscountAmount2());
        total = total.add(getSIDiscountAmount3());
        total = total.add(getSIDiscountAmount4());
        return total;
    }

    public BigDecimal getMixAndMatchTotalAmount() {
        BigDecimal total = getMixAndMatchAmount0();
        if (getMixAndMatchAmount1() != null)
            total = total.add(getMixAndMatchAmount1());
        if (getMixAndMatchAmount2() != null)
            total = total.add(getMixAndMatchAmount2());
        if (getMixAndMatchAmount3() != null)
            total = total.add(getMixAndMatchAmount3());
        if (getMixAndMatchAmount4() != null)
            total = total.add(getMixAndMatchAmount4());
        return total;
    }

    public BigDecimal getNotIncludedTotalSales() {
        BigDecimal total = getNotIncludedSales0();
        total = total.add(getNotIncludedSales1());
        total = total.add(getNotIncludedSales2());
        total = total.add(getNotIncludedSales3());
        total = total.add(getNotIncludedSales4());
        return total;
    }

    public BigDecimal getNetSalesTotalAmount() {
        BigDecimal total = getNetSalesAmount0();
        total = total.add(getNetSalesAmount1());
        total = total.add(getNetSalesAmount2());
        total = total.add(getNetSalesAmount3());
        total = total.add(getNetSalesAmount4());
        return total;
    }

    public BigDecimal getPaidInAmount() {
        if (getDealType2().equals("4"))
            return getDaiShouAmount();
        else
            return bigDecimal0;
    }

    public BigDecimal getPaidOutAmount() {
        if (getDealType2().equals("4"))
            return getDaiFuAmount();
        else
            return bigDecimal0;
    }

    public void setTotalMMAmount(BigDecimal amount) {
        totalMMAmount = amount;
    }

    public BigDecimal getTotalMMAmount() {
        BigDecimal amount = null;
        if (mixAndMatchVersion == null || mixAndMatchVersion.trim().equals("")
                || mixAndMatchVersion.trim().equals("1"))
            if (totalMMAmount == null)
            	return getTaxMMAmount();
            else
            	return totalMMAmount;
        if (mixAndMatchVersion.trim().equals("2")) {
            amount = getMixAndMatchTotalAmount();
            if (amount == null)
                amount = getTaxMMAmount();
        }
        return amount;
    }

    public void setMMDetail(String mmID, LineItem mmLineItem) {
        if (mmLineItem.getUnitPrice() != null
                && mmLineItem.getUnitPrice().compareTo(bigDecimal0) == 1)
            mmMap.put(mmID, mmLineItem);
    }

    public void clearMMDetail() {
        mmMap.clear();
        taxMMAmount.clear();
    }

    private static String[][] posToScFieldNameArray;

    public static String[][] getPosToScFieldNameArray() {
        if (posToScFieldNameArray == null)
            posToScFieldNameArray = new String[][] {
                new String[] { "STORENO", "storeID" },
                new String[] { "SYSDATE", "systemDate" },
                new String[] { "POSNO", "posNumber" },
                new String[] { "TMTRANSEQ", "transactionNumber" },
                new String[] { "SIGNONID", "shiftCount" },
                new String[] { "EODCNT", "zSequenceNumber" },
                new String[] { "ACCDATE", "accountDate" },
                new String[] { "TRANTYPE", "transactionType" },
                new String[] { "DEALTYPE1", "dealType1" },
                new String[] { "DEALTYPE2", "dealType2" },
                new String[] { "DEALTYPE3", "dealType3" },
                new String[] { "VOIDSEQ", "voidTransactionNumber" },
                new String[] { "TMCODEP", "machineNumber" },
                new String[] { "CASHIER", "cashier" },
                new String[] { "INVNOHEAD", "invoiceHead" },
                new String[] { "INVNO", "invoiceNumber" },
                new String[] { "INVCNT", "invoiceCount" },
                new String[] { "IDNO", "idNumber" },
                new String[] { "DETAILCNT", "detailCount" },
                new String[] { "CUSTCNT", "customerCount" },
                new String[] { "INOUT", "inOurID" },
                new String[] { "CUSTID", "customerCategory" },
                new String[] { "SALEMAN", "saleMan" },
                new String[] { "GROSALAMT", "grossSaleTotalAmount" },
                new String[] { "GROSALTX0AMT", "grossSaleTax0Amount" },
                new String[] { "GROSALTX1AMT", "grossSaleTax1Amount" },
                new String[] { "GROSALTX2AMT", "grossSaleTax2Amount" },
                new String[] { "GROSALTX3AMT", "grossSaleTax3Amount" },
                new String[] { "GROSALTX4AMT", "grossSaleTax4Amount" },
                new String[] { "SIPercentPlusTotalAmount", "siPlusTotalAmount" },
                new String[] { "SIPLUSAMT0", "siPlusTax0Amount" },
                new String[] { "SIPLUSAMT1", "siPlusTax1Amount" },
                new String[] { "SIPLUSAMT2", "siPlusTax2Amount" },
                new String[] { "SIPLUSAMT3", "siPlusTax3Amount" },
                new String[] { "SIPLUSAMT4", "siPlusTax4Amount" },
                new String[] { "SIPercentOffTotalAmount", "discountTotalAmount" },
                new String[] { "SIPAMT0", "discountTax0Amount" },
                new String[] { "SIPAMT1", "discountTax1Amount" },
                new String[] { "SIPAMT2", "discountTax2Amount" },
                new String[] { "SIPAMT3", "discountTax3Amount" },
                new String[] { "SIPAMT4", "discountTax4Amount" },
                new String[] { "MixAndMatchTotalAmount",
                        "mixAndMatchTotalAmount" },
                new String[] { "MNMAMT0", "mixAndMatchTax0Amount" },
                new String[] { "MNMAMT1", "mixAndMatchTax1Amount" },
                new String[] { "MNMAMT2", "mixAndMatchTax2Amount" },
                new String[] { "MNMAMT3", "mixAndMatchTax3Amount" },
                new String[] { "MNMAMT4", "mixAndMatchTax4Amount" },
                new String[] { "NotIncludedTotalSales", "notIncludedTotalSale" },
                new String[] { "RCPGIFAMT0", "notIncludedTax0Sale" },
                new String[] { "RCPGIFAMT1", "notIncludedTax1Sale" },
                new String[] { "RCPGIFAMT2", "notIncludedTax2Sale" },
                new String[] { "RCPGIFAMT3", "notIncludedTax3Sale" },
                new String[] { "RCPGIFAMT4", "notIncludedTax4Sale" },
                new String[] { "NetSalesTotalAmount", "netSaleTotalAmount" },
                new String[] { "NETSALAMT0", "netSaleTax0Amount" },
                new String[] { "NETSALAMT1", "netSaleTax1Amount" },
                new String[] { "NETSALAMT2", "netSaleTax2Amount" },
                new String[] { "NETSALAMT3", "netSaleTax3Amount" },
                new String[] { "NETSALAMT4", "netSaleTax4Amount" },
                new String[] { "DAISHOUAMT", "daiShouAmount" },
                new String[] { "DAISHOUAMT2", "daiShouAmount2" },
                new String[] { "DAIFUAMT", "daiFuAmount" },
                new String[] { "rebateChangeAmount", "rebateChangeAmount" },
                new String[] { "rebateAmount", "rebateAmount" },
                new String[] { "totalRebateAmount", "totalRebateAmount" },
                new String[] { "PAYNO1", "payID1" },
                new String[] { "PAYNO2", "payID2" },
                new String[] { "PAYNO3", "payID3" },
                new String[] { "PAYNO4", "payID4" },
                new String[] { "PAYAMT1", "payAmount1" },
                new String[] { "PAYAMT2", "payAmount2" },
                new String[] { "PAYAMT3", "payAmount3" },
                new String[] { "PAYAMT4", "payAmount4" },
                new String[] { "TAXAMT0", "tax0Amount" },
                new String[] { "TAXAMT1", "tax1Amount" },
                new String[] { "TAXAMT2", "tax2Amount" },
                new String[] { "TAXAMT3", "tax3Amount" },
                new String[] { "TAXAMT4", "tax4Amount" },
                new String[] { "CHANGEAMT", "changeAmount" },
                new String[] { "OVERAMT", "spillAmount" },
                new String[] { "CHANGAMT", "exchangeDifference" },
                new String[] { "CRDTYPE", "creditCardType" },
                new String[] { "CRDNO", "creditCardNumber" },
                new String[] { "CRDEND", "creditCardExpireDate" },
                new String[] { "EMPNO", "employeeNumber" },
                new String[] { "MEMBERID", "memberID" },
                new String[] { "AUTHORNO", "authoringNumber" },
                new String[] { "ANNOTATEDID", "annotatedID" },
                new String[] { "ANNOTATEDTYPE", "annotatedType" },
                //gllg
                new String[] { "ITEMDISCAMT0", "itemDiscAmount0" },
                new String[] { "ITEMDISCAMT1", "itemDiscAmount1" },
                new String[] { "ITEMDISCAMT2", "itemDiscAmount2" },
                new String[] { "ITEMDISCAMT3", "itemDiscAmount3" },
                new String[] { "ITEMDISCAMT4", "itemDiscAmount4" },
                new String[] { "ITEMDISCCNT0", "itemDiscCount0" },
                new String[] { "ITEMDISCCNT1", "itemDiscCount1" },
                new String[] { "ITEMDISCCNT2", "itemDiscCount2" },
                new String[] { "ITEMDISCCNT3", "itemDiscCount3" },
                new String[] { "ITEMDISCCNT4", "itemDiscCount4" }
        };
        return posToScFieldNameArray;
    }

    public Object[] cloneForSC() {
        String fieldNameMap[][] = getPosToScFieldNameArray();
        List dacReturned = new ArrayList();
        try {
            Transaction clonedTransaction = new Transaction();
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = getFieldValue(fieldNameMap[i][0]);
                if (value == null)
                    try {
                        value = getClass().getDeclaredMethod(
                                "get" + fieldNameMap[i][0], new Class[0])
                                .invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                clonedTransaction.setFieldValue(fieldNameMap[i][1], value);
            }

            dacReturned.add(clonedTransaction);

            Object lineItems[] = getLineItemsArrayLast();
            if (lineItems != null) {
                for (int i = 0; i < lineItems.length; i++) {
                    LineItem lineItem = ((LineItem) lineItems[i]).cloneForSC();
                    clonedTransaction.addLineItemSimpleVersion(lineItem);
                    dacReturned.add(lineItem);
                }
            }

            Object tokenTrandtl[] = payMentList.toArray();
            if(tokenTrandtl != null){
                for (int i = 0; i < tokenTrandtl.length; i++){
                    TokenTranDtl tokenTranDtl = ((TokenTranDtl)tokenTrandtl[i]).cloneForSC();
                    clonedTransaction.payMentList.add(tokenTranDtl);
                    dacReturned.add(tokenTranDtl);
                 }
            }

            return dacReturned.toArray();
        } catch (InstantiationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return null;
    }

    public static Iterator getOutdateTranNumbers() {
        ArrayList numbers = new ArrayList();
        int tranReserved = getTransactionReserved();
        String today = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
        long l = java.sql.Date.valueOf(today).getTime() - (long) tranReserved
                * 1000L * 3600L * 24L;
        String baseTime = (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"))
                .format(new java.sql.Date(l));
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            for (resultSet = statement
                    .executeQuery("SELECT TMTRANSEQ FROM tranhead WHERE SYSDATE  < '"
                            + baseTime + "'"); resultSet.next(); numbers
                    .add(resultSet.getString("TMTRANSEQ")))
                ;
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (resultSet != null)
                    resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
            return numbers.iterator();
        }
    }

    public static void deleteOutdatedData() {
        int tranReserved = getTransactionReserved();
        String today = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
        long l = java.sql.Date.valueOf(today).getTime() - (long) tranReserved
                * 1000L * 3600L * 24L;
        String baseTime = (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"))
                .format(new java.sql.Date(l));
        Connection connection = null;
        Statement statement = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM "
                    + getInsertUpdateTableNameStaticVersion() + " WHERE "
                    + (Server.serverExist() ? "systemDate" : "SYSDATE")
                    + " < '" + baseTime + "'");
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
    }

    public BigDecimal getMMTotalAmt() {
        BigDecimal mmTotalAmt = bigDecimal0;
        for (Iterator iter = lineItemArray.iterator(); iter.hasNext();) {
            LineItem lineItem = (LineItem) iter.next();
            mmTotalAmt.add(lineItem.getAmount()).subtract(
                    lineItem.getAfterDiscountAmount());
            if (currentLineItem != null)
                mmTotalAmt.add(currentLineItem.getAmount()).subtract(
                        currentLineItem.getAfterDiscountAmount());
        }

        return mmTotalAmt;
    }

    public String getPeiDaNumberPrinted() {
        if (getAnnotatedType() != null && getAnnotatedType().equals("P"))
            return "PO: " + getAnnotatedId();
        else
            return "";
    }

    public String getWeiXiuNumber() {
        if (getAnnotatedType() != null && getAnnotatedType().equals("W"))
            return getAnnotatedId();
        else
            return null;
    }

    public String getAnnotatedType() {
        if (Server.serverExist())
            return (String) getFieldValue("annotatedType");
        else
            return (String) getFieldValue("ANNOTATEDTYPE");
    }

    public void setAnnotatedType(String annotatedType) {
        if (Server.serverExist())
            setFieldValue("annotatedType", annotatedType);
        else
            setFieldValue("ANNOTATEDTYPE", annotatedType);
    }

    public String getAnnotatedId() {
        if (Server.serverExist())
            return (String) getFieldValue("annotatedId");
        else
            return (String) getFieldValue("ANNOTATEDID");
    }

    public void setAnnotatedId(String annotatedId) {
        if (Server.serverExist())
            setFieldValue("annotatedId", annotatedId);
        else
            setFieldValue("ANNOTATEDID", annotatedId);
        if (getAnnotatedType() != null
                && getAnnotatedType().equalsIgnoreCase("P"))
            fireEvent(new TransactionEvent(this, 3));
    }
    
    //==============gllg=======================
    public BigDecimal getItemDiscAmount() {
        return getItemDiscAmount0()
                .add(getItemDiscAmount1())
                .add(getItemDiscAmount2())
                .add(getItemDiscAmount3())
                .add(getItemDiscAmount4());
    }

    public BigDecimal getItemDiscAmount0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount0");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT0");
    }

    public void setItemDiscAmount0(BigDecimal itemDiscAMT0) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount0", itemDiscAMT0);
        else
            setFieldValue("ITEMDISCAMT0", itemDiscAMT0);
    }
    
    public BigDecimal getItemDiscAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount1");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT1");
    }

    public void setItemDiscAmount1(BigDecimal itemDiscAMT0) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount1", itemDiscAMT0);
        else
            setFieldValue("ITEMDISCAMT1", itemDiscAMT0);
    }
    
    public BigDecimal getItemDiscAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount2");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT2");
    }

    public void setItemDiscAmount2(BigDecimal itemDiscAMT2) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount2", itemDiscAMT2);
        else
            setFieldValue("ITEMDISCAMT2", itemDiscAMT2);
    }
    
    public BigDecimal getItemDiscAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount3");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT3");
    }

    public void setItemDiscAmount3(BigDecimal itemDiscAMT3) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount3", itemDiscAMT3);
        else
            setFieldValue("ITEMDISCAMT3", itemDiscAMT3);
    }
    
    public BigDecimal getItemDiscAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount4");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT4");
    }

    public void setItemDiscAmount4(BigDecimal itemDiscAMT4) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount4", itemDiscAMT4);
        else
            setFieldValue("ITEMDISCAMT4", itemDiscAMT4);
    }

    public Integer getItemDiscCnt() {
        return new Integer(
            getItemDiscCnt0().intValue()
            + getItemDiscCnt1().intValue()
            + getItemDiscCnt2().intValue()
            + getItemDiscCnt3().intValue()
            + getItemDiscCnt4().intValue());
    }

//    public void setItemDiscCnt(int itemDiscCnt) {
//        if (!Server.serverExist())
//            setFieldValue("ITEMDISCCNT", new Integer(itemDiscCnt));
//    }
    
    public Integer getItemDiscCnt0() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("ITEMDISCCNT0");
    }
    
    public void setItemDiscCnt0(int itemDiscCnt0) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT0", new Integer(itemDiscCnt0));
    }
    
    public Integer getItemDiscCnt1() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("ITEMDISCCNT1");
    }
    
    public void setItemDiscCnt1(int itemDiscCnt1) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT1", new Integer(itemDiscCnt1));
    }
    
    public Integer getItemDiscCnt2() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("ITEMDISCCNT2");
    }
    
    public void setItemDiscCnt2(int itemDiscCnt2) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT2", new Integer(itemDiscCnt2));
    }
    
    public Integer getItemDiscCnt3() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("ITEMDISCCNT3");
    }
    
    public void setItemDiscCnt3(int itemDiscCnt3) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT3", new Integer(itemDiscCnt3));
    }
    
    public Integer getItemDiscCnt4() {
        if (Server.serverExist())
            return integer0;
        else
            return (Integer) getFieldValue("ITEMDISCCNT4");
    }
    
    public void setItemDiscCnt4(int itemDiscCnt4) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT4", new Integer(itemDiscCnt4));
    }

    //==============gllg=======================

    public boolean insert(boolean needQueryAgain) {
        int nextTransactionNumber = getTransactionNumber().intValue() + 1;
        if (!Server.serverExist()) {
            CreamProperties prop = CreamProperties.getInstance();
            String nextTranNoOrig = prop.getProperty("NextTransactionNumber",
                    "1");
            if (Integer.parseInt(nextTranNoOrig) < nextTransactionNumber) {
                prop.setProperty("NextTransactionNumber", ""
                        + nextTransactionNumber);
                prop.deposit();
            }
        }
        return super.insert(needQueryAgain);
    }

    public boolean isCommit() {
        return commit;
    }

    public void setCommit(boolean commit) {
        this.commit = commit;
    }

    public boolean isStoreComplete() {
        return storeComplete;
    }

    public void setStoreComplete(boolean b) {
        storeComplete = b;
    }

    public void clearPaymentInfo() {
        setChangeAmount(ZERO);
        setPayCash(ZERO);
        setPayNumber1(null);
        setPayNumber2(null);
        setPayNumber3(null);
        setPayNumber4(null);
        setPayAmount1(null);
        setPayAmount2(null);
        setPayAmount3(null);
        setPayAmount4(null);
        setPayments(null);
        setSpillAmount(ZERO);
    }

    public void initPaymentInfo() {
        setChangeAmount(ZERO);
        setPayCash(ZERO);
        setPayNumber1(null);
        setPayNumber2(null);
        setPayNumber3(null);
        setPayNumber4(null);
        setPayAmount1(null);
        setPayAmount2(null);
        setPayAmount3(null);
        setPayAmount4(null);
        setPayments(null);
        setSpillAmount(ZERO);
        initSIInfo();
        setSIAmtList(null, null);
        Object lineItemArray[] = getLineItemsArrayLast();
        LineItem lineItem = null;
        for (int i = 0; i < lineItemArray.length; i++) {
            lineItem = (LineItem) lineItemArray[i];
            if (lineItem.getDetailCode().equals("S")) {
                lineItem.setAfterSIAmount(lineItem.getAmount());
                lineItem.setAfterDiscountAmount(lineItem.getAmount());
                if (lineItem.getDiscountType() != null
                        && lineItem.getDiscountType().equals("S")) {
                    lineItem.setDiscountType(null);
                    lineItem.setDiscountNumber(null);
                }
            }
        }

        try {
            removeSILineItem();
        } catch (LineItemNotFoundException le) {
            le.printStackTrace(CreamToolkit.getLogger());
        }
        if (MIX_AND_MATCH_VERSION == null
                || MIX_AND_MATCH_VERSION.trim().equals("")
                || MIX_AND_MATCH_VERSION.trim().equals("1"))
            setSalesAmount(getSalesAmount().add(getTotalMMAmount()));
        else
            MIX_AND_MATCH_VERSION.trim().equals("2");
    }

    static {
        primaryKeys = new ArrayList();
        if (Server.serverExist()) {
            primaryKeys.add("storeID");
            primaryKeys.add("systemDate");
            primaryKeys.add("posNumber");
            primaryKeys.add("transactionNumber");
        } else {
            primaryKeys.add("TMTRANSEQ");
        }
    }

    public void setReturnAll(boolean returnAll) {
        this.returnAll = returnAll;
    }

    public boolean isReturnAll() {
        return returnAll;
    }

    public static List getTransactionsByTime(String startTime, String endTime) {
		String sql = "SELECT tmtranseq FROM tranhead where sysdate between '"
				+ startTime + "' and '" + endTime + "' order by tmtranseq";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List ret = new ArrayList();
		try {
			connection = CreamToolkit.getPooledConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				ret.add(resultSet.getObject(1));
			}
		} catch (Exception e) {
			CreamToolkit.logMessage("SQLException: " + sql);
			e.printStackTrace(CreamToolkit.getLogger());
		}
		try {
			if (resultSet != null)
				resultSet.close();
			if (statement != null)
				statement.close();
		} catch (SQLException e) {
			e.printStackTrace(CreamToolkit.getLogger());
		}
		if (connection != null)
			CreamToolkit.releaseConnection(connection);
		return ret;
	}

	/**
	 * smallest at first line
	 * @param t
	 * @return
	 */
	static public Object[] getNormalLineItemsArraySortByAmount(Transaction t) {
		Object[] objs = t.getNormalLineItemsArray();
		List normalLineItemArrayList = Arrays.asList(objs);
		Collections.sort(normalLineItemArrayList, new Comparator(){
			public int compare(Object o1, Object o2) {
				BigDecimal seqno1 = new BigDecimal(0);
				try {
					seqno1 = ((LineItem) o1).getAmount();	
				} catch (Exception e) {}
				BigDecimal seqno2 =  new BigDecimal(0);	
				try {
					seqno2 = ((LineItem) o2).getAmount();	
				} catch (Exception e) {}
				
				return seqno1.compareTo(seqno2);
			}
			public boolean equals(Object obj) {
				return ((String) obj).equals(this);
			}
		});
		
		return normalLineItemArrayList.toArray();
	}
}
