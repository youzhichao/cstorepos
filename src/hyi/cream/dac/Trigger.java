package hyi.cream.dac;

import java.util.*;
import java.text.*;
import java.io.*;
import hyi.cream.util.*;
import hyi.cream.state.StateMachine;
import hyi.cream.*;
import hyi.cream.uibeans.Indicator;
import hyi.cream.inline.*;

/**
 * Thread for downloading masters, program files, shrinking log files, and
 * purging outdated POS transaction data.
 *
 * @author Bruce
 * @version 1.1
 */
public class Trigger extends Thread {
    /**
     * /Bruce/1.1/2002-03-26/
     *     Split "getAll" to "getPLU", "getCashier", ...
     *
     * /Bruce/1.1/2002-03-21/
     *     Add "shrinkfile" mechanism for log files.
     *
     * /Bruce/1.1/2002-03-10/
     *     Modify the method for downloading master.
     */
    transient public static final String VERSION = "1.1";

    static Trigger t = null;
    static private DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    private int posNo = Integer.parseInt(CreamProperties.getInstance().getProperty("TerminalNumber"));
    private String masterVersion;
    private ResourceBundle res = CreamToolkit.GetResource();

	/**
	 * get InlineServerProgramDirectory
	 */
	private static final String INLINE_SERVER_PROGRAM_DIRECTORY = 
		CreamProperties.getInstance().getProperty("InlineServerProgramDirectory","");

	private static String failedCountOneDay;		
	
    public static Trigger getInstance() {
         if (t == null)
             t = new Trigger();
         return t;
    }

    public Trigger () {
        getMasterVersion();
        start();
    }

    String getMasterVersion() {
        masterVersion = CreamProperties.getInstance().getProperty("MasterVersion");
        if (masterVersion == null) {
            masterVersion = dateFormatter.format(new Date());
            setMasterVersion(masterVersion);
        }
        return masterVersion;
    }

    void setMasterVersion(String masterVersion) {
        this.masterVersion = masterVersion;
        CreamProperties.getInstance().setProperty("MasterVersion", masterVersion);
        CreamProperties.getInstance().deposit();
    }

    private void runScriptFiles() {
        ArrayList scriptFiles = Client.getInstance().getScriptFiles();
        Iterator iter = scriptFiles.iterator();
        while (iter.hasNext()) {
            String script = (String)iter.next();
            try {
                //<<PLATFORM-DEPENDENT CODE>>
                Process p = Runtime.getRuntime().exec(new String[] {"/bin/sh", script});
                p.waitFor();
            } catch (IOException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            } catch (InterruptedException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        }
    }

    public void downloadProgramFiles() throws ClientCommandException {
    	String serverDirectory = "/root/posdl/" + posNo;
    	if (!INLINE_SERVER_PROGRAM_DIRECTORY.equals("")) {
    		serverDirectory = INLINE_SERVER_PROGRAM_DIRECTORY + posNo;
    	}
    	
        Client.getInstance().processCommand("moveFile " +
            serverDirectory + " " +  // server directory
            ".");                    // client directory (current directory)
        runScriptFiles();
    }

    public void shrinkLogFiles() {
        CreamToolkit.logMessage("Shrinking log files...");
        /*
        Process p = Runtime.getRuntime().exec("/usr/bin/shrinkfile -s 5m /root/cream.log");
        p.waitFor();
        p = Runtime.getRuntime().exec("/usr/bin/shrinkfile -s 5m /root/inline.log");
        p.waitFor();
        p = Runtime.getRuntime().exec("/usr/bin/shrinkfile -s 5m /root/debug.log");
        p.waitFor();
        p = Runtime.getRuntime().exec("/usr/bin/shrinkfile -s 5m /root/err.log");
        p.waitFor();
        */
        CreamToolkit.closeLog();
        String logFile = CreamProperties.getInstance().getProperty("LogFile");
        if (logFile != null)
            CreamToolkit.shrinkFile(new File(logFile), 5L * 1024 * 1024);
        CreamToolkit.openLogger();

        Client.closeLog();
        String inlineLogFile = CreamProperties.getInstance().getProperty("InlineClientLogFile");
        if (inlineLogFile != null)
            CreamToolkit.shrinkFile(new File(inlineLogFile), 5L * 1024 * 1024);
        Client.openLog();

        CreamToolkit.shrinkFile(new File("debug.log"), 5L * 1024 * 1024);
        CreamToolkit.shrinkFile(new File("err.log"), 5L * 1024 * 1024);

        // remove transaction log two months ago
        File tranLogDir = new File("tranlog");
        if (tranLogDir.exists() && tranLogDir.isDirectory()) {
            Calendar day = Calendar.getInstance();
            day.add(Calendar.MONTH, -2);
            String twoMonthsAgo = new SimpleDateFormat("yyyy-MM-dd").format(day.getTime());
            File[] files = tranLogDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].getName().compareTo(twoMonthsAgo) < 0) {
                    files[i].delete();
                    CreamToolkit.logMessage(files[i].getAbsolutePath() + " is deleted.");
                }
            }
        }
    }

    public void deleteOutdatedData() {
        LineItem.deleteOutdatedData();
        Transaction.deleteOutdatedData();
    }

    public void repairAndOptimizeTables() {
        try {
            // Find mysqlcheck...
            String mysqlCheckPath;
            if (new File("/usr/bin/mysqlcheck").exists())
                mysqlCheckPath = "/usr/bin/mysqlcheck";
            else if (new File("c:\\Program Files\\MySQL\\bin\\mysqlcheck.exe").exists())
                mysqlCheckPath = "c:\\Program Files\\MySQL\\bin\\mysqlcheck.exe";
            else if (new File("c:\\MySQL\\bin\\mysqlcheck.exe").exists())
                mysqlCheckPath = "c:\\MySQL\\bin\\mysqlcheck.exe";
            else
                mysqlCheckPath = "mysqlcheck";

            // Run mysqlcheck and write log...
            String[] cmdList = {
                " -v -r cream tranhead trandetail z shift depsales daishousales daishousales2 token_trandtl",       // repair
                " -v -o cream tranhead trandetail z shift depsales daishousales daishousales2 token_trandtl"};      // optimize
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss ");
            for (int i = 0; i < cmdList.length; i++) {
                Process p = Runtime.getRuntime().exec(mysqlCheckPath + cmdList[i]);
                InputStream stdout = p.getInputStream();
                String logFilePath = CreamProperties.getInstance().getProperty("LogFile");
                if (logFilePath != null) {
                    OutputStream logFile = new BufferedOutputStream(new FileOutputStream(logFilePath, true));
                    String dateString = dateFormatter.format(new java.util.Date());
                    logFile.write(dateString.getBytes(CreamToolkit.getEncoding()));
                    logFile.write(' ');
                    logFile.write((mysqlCheckPath + cmdList[i]).getBytes(CreamToolkit.getEncoding()));
                    logFile.write('\n');
                    int c;
                    while ((c = stdout.read()) != -1)
                        logFile.write(c);
                    logFile.close();
                    stdout.close();
                }
                p.waitFor();
            }            
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } catch (InterruptedException e) {
        }
    }

    /**
     * 将数据库备份到指定的机器上。
     * 
     * @return true if sucess.
     */
    public boolean backUp() {
        boolean isOK = false;

        // 从"MySQLDataDirectory"和"DatabaseURL"取得资料存放的目录
        String localDir = CreamProperties.getInstance().getProperty("MySQLDataDirectory");
        if (localDir == null)
            return true;
        if (!localDir.endsWith(File.separator))
            localDir += File.separator;
        String url = CreamProperties.getInstance().getProperty("DatabaseURL");
        localDir += url.substring(url.lastIndexOf('/') + 1);
        if (!localDir.endsWith(File.separator))
            localDir += File.separator;

        // 从"MySQLRemoteBackupDirectory"取得远端资料备份的目录(rsync server directory)
        String backDir = CreamProperties.getInstance().getProperty("MySQLRemoteBackupDirectory");
        if (backDir == null)
            return true;
        if (!backDir.endsWith(File.separator)) 
            backDir += File.separator;            

        String posNo = CreamProperties.getInstance().getProperty("TerminalNumber");
        try {
            String commandLine = "rsync -r " + localDir + " " + backDir + posNo + File.separator;
            Process p = Runtime.getRuntime().exec(commandLine);
            int exitCode = p.waitFor();
            isOK = (exitCode == 0);
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InterruptedException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return isOK;
    }
    
    /**
     * 从备份恢复数据库。 
     * 
     * @return true if sucess.
     */
    public boolean recoverFromBackup() {
        boolean isOK = false;

        // 从"MySQLDataDirectory"和"DatabaseURL"取得资料存放的目录
        String localDir = CreamProperties.getInstance().getProperty("MySQLDataDirectory");
        if (localDir == null)
            return false;
        if (!localDir.endsWith(File.separator))
            localDir += File.separator;
        String url = CreamProperties.getInstance().getProperty("DatabaseURL");
        localDir += url.substring(url.lastIndexOf('/') + 1);
        if (!localDir.endsWith(File.separator))
            localDir += File.separator;
                
        // 从"MySQLRemoteBackupDirectory"取得远端资料备份的目录(rsync server directory)
        String backDir = CreamProperties.getInstance().getProperty("MySQLRemoteBackupDirectory");
        if (backDir == null)
            return false;
        if (!backDir.endsWith(File.separator)) 
            backDir += File.separator;            
                
        String posNo = CreamProperties.getInstance().getProperty("TerminalNumber");
                
        try {
            String commandLine = "rsync -r " + backDir + posNo + "/" + " " + localDir;
            Process p = Runtime.getRuntime().exec(commandLine);
            int exitCode = p.waitFor();
            isOK = (exitCode == 0);
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InterruptedException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return isOK;
    }
    
    public void run() {
        //int time = 0;
        Calendar cal = null;
        while (true) {
            try {
                Thread.sleep(29990);
            } catch (Exception ue) {
                ue.printStackTrace(CreamToolkit.getLogger());
                continue;
            }
            System.gc();
            cal = Calendar.getInstance();
            String downloadMasterHour = CreamProperties.getInstance().getProperty("DownloadMasterHour");
            int hour = 0;
            if (downloadMasterHour != null) {
                try {
                    hour = Integer.parseInt(downloadMasterHour);
                } catch (NumberFormatException e) {
                }
            }
            if (cal.get(Calendar.HOUR_OF_DAY) < hour)
                continue;
            if (cal.get(Calendar.HOUR_OF_DAY) == hour && cal.get(Calendar.MINUTE) < posNo * 3)
                continue;

            String version = getMasterVersion();
            String versionShouldHave = dateFormatter.format(new Date());
            if (version.equals(versionShouldHave))
                continue;
            if (!Client.getInstance().isConnected())
                continue;

            Indicator indicator = POSTerminalApplication.getInstance().getMessageIndicator();

            if (!POSTerminalApplication.getInstance().getTransactionEnd()) {
                CreamToolkit.logMessage("Not on transaction end");
            }
            
            /**
             * Meyer/2003-03-10/主档更新每日失败3次后，不再自动更新
             * 
             */
			String today = new SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
			if (failedCountOneDay == null 
				|| failedCountOneDay.length() != 9 
				|| !failedCountOneDay.substring(0,8).equals(today)) {
				failedCountOneDay = today + "0";	
			}
			int failedCount = Integer.parseInt(failedCountOneDay.substring(8,9));		
            if (
            /*!DacTransfer.getInstance().isDateShiftRunning(DacTransfer.DATE_BIZDATE) && */
				failedCount < 3 
				&& POSTerminalApplication.getInstance().getTransactionEnd()) {
             
                indicator.setMessage(res.getString("PreparingToDownloadMaster"));
                StateMachine.getInstance().setEventProcessEnabled(false);
                try {
                    Thread.sleep(1000);
                    //DacTransfer.getInstance().pluUpdate();

					Client.getInstance().downloadMaster();

                    indicator.setMessage(res.getString("ProgramDownloading"));
                    
                    downloadProgramFiles();
                    shrinkLogFiles();
                    deleteOutdatedData();
                    repairAndOptimizeTables();

                    setMasterVersion(versionShouldHave);

                    POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                        CreamToolkit.GetResource().getString("UpdateCompleted"));

					//Meyer/2003-03-10/successful
					failedCount = 0;

                    //Bruce/20021028/
                    //DacTransfer.shutdown(10);
                    //Bruce/20030826
                    // Only reboot on Linux
                    if (System.getProperties().get("os.name").toString().indexOf("Windows") == -1)
                        DacTransfer.reboot();

                } catch (ClientCommandException e) {
                    // don't reboot, show error message
                    e.printStackTrace(CreamToolkit.getLogger());
                    indicator.setMessage(res.getString("MasterDownloadFailed"));
					failedCount++;
                } catch (InterruptedException ue) {
                    ue.printStackTrace(CreamToolkit.getLogger());
					failedCount++;
                } finally {					
                    StateMachine.getInstance().setEventProcessEnabled(true);
                }    
            }
			failedCountOneDay = today + String.valueOf(failedCount);
        }
    }
}
