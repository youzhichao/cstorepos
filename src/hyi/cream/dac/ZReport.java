// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   ZReport.java

package hyi.cream.dac;

import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.inline.Server;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Referenced classes of package hyi.cream.dac:
//            DacBase, SequenceNumberGetter, Transaction, DepSales, 
//            DaishouSales, TaxType

public class ZReport extends DacBase implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final transient String VERSION = "1.6";
    static transient ZReport currentZ = null;
    private static ArrayList primaryKeys;
    transient static int currentZNumber = -1;

    static {
        primaryKeys = new ArrayList();
        if (Server.serverExist()) {
            primaryKeys.add("storeID");
            primaryKeys.add("posNumber");
            primaryKeys.add("zSequenceNumber");
        } else {
            primaryKeys.add("POSNO");
            primaryKeys.add("EODCNT");
        }
    }

    public List getPrimaryKeyList() {
        return primaryKeys;
    }

    public ZReport() throws InstantiationException {
        setAccountingDate(CreamToolkit.getInitialDate());
        setStoreNumber(CreamProperties.getInstance().getProperty("StoreNumber"));
        String s = CreamProperties.getInstance().getProperty("TerminalNumber");
        if (s != null)
            setTerminalNumber(Integer.decode(s));
        setMachineNumber(CreamProperties.getInstance().getProperty(
                "TerminalPhysicalNumber"));
        setUploadState("0");
    }

    public ZReport(int i) throws InstantiationException {
    }

    public static Iterator queryByDateTime(Date date) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
        if (Server.serverExist()) {
            if (isServerSideOracle())
                return DacBase.getMultipleObjects(hyi.cream.dac.ZReport.class,
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                            + " WHERE accountDate=TO_DATE('"
                            + simpledateformat.format(date).toString() + "','YYYY-MM-DD HH24:MI:SS')");
            else
                return DacBase.getMultipleObjects(hyi.cream.dac.ZReport.class,
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                            + " WHERE accountDate='"
                            + simpledateformat.format(date).toString() + "'");

        }
        else
            return DacBase.getMultipleObjects(hyi.cream.dac.ZReport.class,
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                            + " WHERE ACCDATE='"
                            + simpledateformat.format(date).toString()
                            + "' ORDER BY EODCNT DESC");
    }

    public static ZReport queryBySequenceNumber(Integer integer) {
        return (ZReport) DacBase.getSingleObject(hyi.cream.dac.ZReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                        + " WHERE EODCNT = " + integer);
    }

    public static Iterator queryRecentReports() {
        return DacBase.getMultipleObjects(hyi.cream.dac.ZReport.class,
                "SELECT ZBEGDTTM, ZENDDTTM, EODCNT FROM "
                        + getInsertUpdateTableNameStaticVersion()
                        + " ORDER BY ZBEGDTTM DESC LIMIT 20");
    }

    public static Iterator getUploadFailedList() {
        String s = "2";
        return DacBase.getMultipleObjects(hyi.cream.dac.ZReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                        + " WHERE TCPFLG='" + s + "'");
    }

    public static Iterator getNotUploadedList() {
        String s = "0";
        return DacBase.getMultipleObjects(hyi.cream.dac.ZReport.class,
                "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                        + " WHERE TCPFLG='" + s + "'");
    }

    public static Iterator queryNotUploadReport() {
        return DacBase.getMultipleObjects(hyi.cream.dac.ZReport.class,
                "SELECT EODCNT FROM " + getInsertUpdateTableNameStaticVersion()
                        + " WHERE TCPFLG<>'1' AND ACCDATE<>'1970-01-01'");
    }

    /**
     * This method should be called:
     * "getMaxZNumberFromPOSOrFromSCIfPOSDoNotHaveAnyZ_Or_TheCachedZNumberOfNewlyCreatedZ()".
     */
    public static int getCurrentZNumber() {
        //Bruce/20080414/ Cache currentZNumber for improving performance
        if (currentZNumber == -1) {
            Map map = DacBase.getValueOfStatement("SELECT MAX(EODCNT) FROM z");
            if (map.isEmpty())
                return -1;
            Iterator iterator = map.keySet().iterator();
            Object obj = map.get(iterator.next());
            int zno = -1;
            if (obj instanceof Integer)
                zno = ((Integer) obj).intValue();
            else if (obj instanceof Long)
                zno = ((Long) obj).intValue();
            if (zno == -1) {
                Integer integer = Client.getInstance().getMaxZNoInSC();
                if (integer == null)
                    return -1;
                else {
                    currentZNumber = integer.intValue();
                }
            } else {
                currentZNumber = zno;
            }
        }
        return currentZNumber;
    }

    public boolean insert(boolean needQueryAgain)
    {
        boolean result = super.insert(needQueryAgain);
        currentZNumber = -1;
        if (!Server.serverExist())
            getCurrentZNumber(); // only do this at POS side
        return result;
    }

    private static ZReport getOrCreateLastZReport() {
        int i = getCurrentZNumber();
        if (i == -1) { // 前後端都拿不到Z number
            ZReport zreport = createZReport();
            CreamToolkit.logMessage("Z report creating detected! new Z is: "
                    + zreport.getValues());
            return zreport;
        }
        ZReport zreport1 = (ZReport) DacBase.getSingleObject(
                hyi.cream.dac.ZReport.class, "SELECT * FROM "
                        + getInsertUpdateTableNameStaticVersion()
                        + " WHERE EODCNT='" + i + "'");
        if (zreport1 == null) {
            zreport1 = createZReport();
            CreamToolkit.logMessage("Z report creating detected! new Z is: "
                    + zreport1.getValues());
        }
        return zreport1;
    }

    public static ZReport getCurrentZReport() {
        if (currentZ == null) {
            // 看序號最大的那筆是否為197X年，如果是則返回他作為當前Z
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            currentZ = (ZReport) DacBase.getSingleObject(hyi.cream.dac.ZReport.class,
                    "SELECT * FROM " + getInsertUpdateTableNameStaticVersion()
                    + " ORDER BY eodcnt DESC LIMIT 1");
            if (currentZ != null && df.format(currentZ.getAccountingDate()).startsWith("197"))
                return currentZ;
            else {
                currentZ = null;
                return null;
            }
        }
        return currentZ;
    }

    public static ZReport getOrCreateCurrentZReport() {
        if (currentZ == null) {
            currentZ = getCurrentZReport();
            if (currentZ == null)
                currentZ = getOrCreateLastZReport();
        }
        return currentZ;
    }

    public static ZReport createZReport() {
        Client client = Client.getInstance();
        int i;
        if (client.isConnected()) {
            int j = -1;
            try {
                client.processCommand("queryMaxZNumber");
                DacBase dacbase = client.getReturnObject();
                if (dacbase != null) {
                    SequenceNumberGetter sequencenumbergetter = (SequenceNumberGetter) dacbase;
                    Integer integer = sequencenumbergetter.getMaxZNumber();
                    if (integer != null)
                        j = integer.intValue();
                }
            } catch (ClientCommandException clientcommandexception) {
            }
            i = getCurrentZNumber();
            if (j > i)
                i = j;
        } else {
            i = getCurrentZNumber();
        }
        currentZ = newZReport();
        currentZ.setSequenceNumber(new Integer(i + 1));
        currentZ.setBeginSystemDateTime(new Date());
        currentZ.setEndSystemDateTime(CreamToolkit.getInitialDate());
        currentZ.setBeginTransactionNumber(new Integer(Transaction
                .getNextTransactionNumber()), true);
        currentZ.setEndTransactionNumber(new Integer(Transaction
                .getNextTransactionNumber()), true);
        currentZ.setBeginInvoiceNumber("0", true);
        currentZ.setEndInvoiceNumber("0", true);
        currentZ.setMachineNumber(CreamProperties.getInstance().getProperty(
                "TerminalPhysicalNumber"));
        boolean flag = currentZ.insert();
        if (!flag) {
            currentZ = (ZReport) currentZ.queryByPrimaryKey();
            CreamToolkit.logMessage("Create Z report failed.");
        } else {
            CreamToolkit.logMessage("Z report is created. (zno=" + currentZ.getSequenceNumber() + ")");
        }
        DepSales.createDepSales(currentZ.getSequenceNumber().intValue());
        DaishouSales.createDaishouSales(currentZ.getSequenceNumber().intValue());
        return currentZ;
    }

    private static ZReport newZReport() {
        try {
            currentZ = new ZReport();
            int lastZ = getCurrentZNumber();
            if (lastZ == -1 || lastZ == 0) {
                currentZ.setGrandTotal((new BigDecimal(0.0D)).setScale(2, 4));
                currentZ.setTransactionCountGrandTotal(new Integer(0));
                currentZ.setItemCountGrandTotal((new BigDecimal(0.0D))
                        .setScale(2, 4));
                return currentZ;
            }
            lastZ--;
            ZReport dataZ = (ZReport) DacBase.getSingleObject(
                    hyi.cream.dac.ZReport.class, "SELECT * FROM "
                            + getInsertUpdateTableNameStaticVersion()
                            + " WHERE EODCNT="
                            + lastZ
                            + " AND  POSNO="
                            + Integer.parseInt(CreamProperties.getInstance()
                                    .getProperty("TerminalNumber")));
            if (dataZ != null) {
                currentZ.setGrandTotal(dataZ.getGrandTotal());
                currentZ.setTransactionCountGrandTotal(dataZ
                        .getTransactionCountGrandTotal());
                currentZ.setItemCountGrandTotal(dataZ.getItemCountGrandTotal());
            } else {
                currentZ.setGrandTotal((new BigDecimal(0.0D)).setScale(2, 4));
                currentZ.setTransactionCountGrandTotal(new Integer(0));
                currentZ.setItemCountGrandTotal((new BigDecimal(0.0D))
                        .setScale(2, 4));
            }
        } catch (InstantiationException ie) {
            CreamToolkit.logMessage(ie.toString());
        }
        return currentZ;
    }

    public boolean update() {
        boolean flag = super.update();
        DepSales.updateAll(getAccountingDate());
        if (!flag)
            CreamToolkit
                    .logMessage("  ** DANGER ** DANGER ** DANGER ** DANGER **");
        //getCurrentZNumber();
        return flag;
    }

    public String getStoreNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("storeID");
        else
            return (String) getFieldValue("STORENO");
    }

    public void setStoreNumber(String s) {
        if (Server.serverExist())
            setFieldValue("storeID", s);
        else
            setFieldValue("STORENO", s);
    }

    public Integer getTerminalNumber() {
        if (Server.serverExist())
            return retrieveInteger(getFieldValue("posNumber"));
        else {
            if (getFieldValue("POSNO") instanceof Byte)
                return new Integer(((Byte) getFieldValue("POSNO")).intValue());
            else
                return (Integer) getFieldValue("POSNO");
        }
    }

    public void setTerminalNumber(Integer integer) {
        if (Server.serverExist())
            setFieldValue("posNumber", integer);
        else
            setFieldValue("POSNO", integer);
    }

    public void setTerminalNumber(Byte byte1) {
        if (Server.serverExist())
            setFieldValue("posNumber", byte1);
        else
            setFieldValue("POSNO", byte1);
    }

    public String getMachineNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("machineNumber");
        else
            return (String) getFieldValue("MACHINENO");
    }

    public void setMachineNumber(String s) {
        if (Server.serverExist())
            setFieldValue("machineNumber", s);
        else
            setFieldValue("MACHINENO", s);
    }

    public Integer getSequenceNumber() {
        if (Server.serverExist())
            return (Integer) getFieldValue("zSequenceNumber");
        else
            return (Integer) getFieldValue("EODCNT");
    }

    public void setSequenceNumber(Integer integer) {
        if (Server.serverExist())
            setFieldValue("zSequenceNumber", integer);
        else
            setFieldValue("EODCNT", integer);
    }

    public Date getAccountingDate() {
        if (Server.serverExist())
            return (Date) getFieldValue("accountDate");
        else
            return (Date) getFieldValue("ACCDATE");
    }

    public void setAccountingDate(Date date) {
        if (Server.serverExist())
            setFieldValue("accountDate", date);
        else
            setFieldValue("ACCDATE", date);
    }

    public Date getBeginSystemDateTime() {
        if (Server.serverExist())
            return (Date) getFieldValue("zBeginDateTime");
        else
            return (Date) getFieldValue("ZBEGDTTM");
    }

    public void setBeginSystemDateTime(Date date) {
        if (Server.serverExist())
            setFieldValue("zBeginDateTime", date);
        else
            setFieldValue("ZBEGDTTM", date);
    }

    public Date getEndSystemDateTime() {
        if (Server.serverExist())
            return (Date) getFieldValue("zEndDateTime");
        else
            return (Date) getFieldValue("ZENDDTTM");
    }

    public void setEndSystemDateTime(Date date) {
        if (Server.serverExist())
            setFieldValue("zEndDateTime", date);
        else
            setFieldValue("ZENDDTTM", date);
    }

    public Integer getBeginTransactionNumber() {
        if (Server.serverExist())
            return (Integer) getFieldValue("zBeginTransactionNumber");
        else
            return (Integer) getFieldValue("SGNONSEQ");
    }

    //Add isUpdProperty para to this method.  gllg Aug 20, 2008
    public void setBeginTransactionNumber(Integer integer, boolean isUpdProperty) {
        if (Server.serverExist()) {
            setFieldValue("zBeginTransactionNumber", integer);
        } else {
            setFieldValue("SGNONSEQ", integer);
            if (isUpdProperty)
                CreamProperties.getInstance().setProperty(
                    "BeginTransactionNumberOfZ", integer.toString());
            CreamProperties.getInstance().deposit();
        }
    }

    public Integer getEndTransactionNumber() {
        if (Server.serverExist())
            return (Integer) getFieldValue("zEndTransactionNumber");
        else
            return (Integer) getFieldValue("SGNOFFSEQ");
    }

    public void setEndTransactionNumber(Integer integer, boolean isUpdProperty) {
        if (Server.serverExist()) {
            setFieldValue("zEndTransactionNumber", integer);
        } else {
            setFieldValue("SGNOFFSEQ", integer);
            if (isUpdProperty)
                CreamProperties.getInstance().setProperty(
                    "EndTransactionNumberOfZ", integer.toString());
            CreamProperties.getInstance().deposit();
        }
    }

    public String getBeginInvoiceNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("zBeginInvoiceNumber");
        else
            return (String) getFieldValue("SGNONINV");
    }

    public void setBeginInvoiceNumber(String s, boolean isUpdProperty) {
        if (Server.serverExist()) {
            setFieldValue("zBeginInvoiceNumber", s);
        } else {
            setFieldValue("SGNONINV", s);
            if (isUpdProperty)
                CreamProperties.getInstance().setProperty("BeginInvoiceNumberOfZ",
                    s);
            CreamProperties.getInstance().deposit();
        }
    }

    public String getEndInvoiceNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("zEndInvoiceNumber");
        else
            return (String) getFieldValue("SGNOFFINV");
    }

    public void setEndInvoiceNumber(String s, boolean isUpdProperty) {
        if (Server.serverExist()) {
            setFieldValue("zEndInvoiceNumber", s);
        } else {
            setFieldValue("SGNOFFINV", s);
            if (isUpdProperty)
                CreamProperties.getInstance().setProperty("EndInvoiceNumberOfZ", s);
            CreamProperties.getInstance().deposit();
        }
    }

    public String getUploadState() {
        if (Server.serverExist())
            return (String) getFieldValue("uploadState");
        else
            return (String) getFieldValue("TCPFLG");
    }

    public void setUploadState(String s) {
        if (Server.serverExist())
            setFieldValue("uploadState", s);
        else
            setFieldValue("TCPFLG", s);
    }

    public String getCashierNumber() {
        if (Server.serverExist())
            return (String) getFieldValue("cashierID");
        else
            return (String) getFieldValue("CASHIER");
    }

    public void setCashierNumber(String s) {
        if (Server.serverExist())
            setFieldValue("cashierID", s);
        else
            setFieldValue("CASHIER", s);
    }

    public BigDecimal getGrandTotal() {
        if (Server.serverExist())
            return new BigDecimal(0.0D);
        else
            return (BigDecimal) getFieldValue("GT");
    }

    public void setGrandTotal(BigDecimal bigdecimal) {
        if (!Server.serverExist())
            setFieldValue("GT", bigdecimal);
    }

    public Integer getTransactionCountGrandTotal() {
        if (Server.serverExist())
            return new Integer(0);
        else
            return (Integer) getFieldValue("GTCUSCNT");
    }

    public void setTransactionCountGrandTotal(Integer integer) {
        if (!Server.serverExist())
            setFieldValue("GTCUSCNT", integer);
    }

    public BigDecimal getItemCountGrandTotal() {
        if (Server.serverExist())
            return new BigDecimal(0.0D);
        else
            return (BigDecimal) getFieldValue("GTSALECNT");
    }

    public void setItemCountGrandTotal(BigDecimal bigdecimal) {
        if (!Server.serverExist())
            setFieldValue("GTSALECNT", bigdecimal);
    }

    public BigDecimal getTaxType0GrossSales() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("grossSaleTax0Amount");
        else
            return (BigDecimal) getFieldValue("SALAMTTAX0");
    }

    public void setTaxType0GrossSales(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("grossSaleTax0Amount", bigdecimal);
        else
            setFieldValue("SALAMTTAX0", bigdecimal);
    }

    public BigDecimal getTaxType1GrossSales() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("grossSaleTax1Amount");
        else
            return (BigDecimal) getFieldValue("SALAMTTAX1");
    }

    public void setTaxType1GrossSales(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("grossSaleTax1Amount", bigdecimal);
        else
            setFieldValue("SALAMTTAX1", bigdecimal);
    }

    public BigDecimal getTaxType2GrossSales() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("grossSaleTax2Amount");
        else
            return (BigDecimal) getFieldValue("SALAMTTAX2");
    }

    public void setTaxType2GrossSales(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("grossSaleTax2Amount", bigdecimal);
        else
            setFieldValue("SALAMTTAX2", bigdecimal);
    }

    public BigDecimal getTaxType3GrossSales() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("grossSaleTax3Amount");
        else
            return (BigDecimal) getFieldValue("SALAMTTAX3");
    }

    public void setTaxType3GrossSales(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("grossSaleTax3Amount", bigdecimal);
        else
            setFieldValue("SALAMTTAX3", bigdecimal);
    }

    public BigDecimal getTaxType4GrossSales() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("grossSaleTax4Amount");
        else
            return (BigDecimal) getFieldValue("SALAMTTAX4");
    }

    public void setTaxType4GrossSales(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("grossSaleTax4Amount", bigdecimal);
        else
            setFieldValue("SALAMTTAX4", bigdecimal);
    }

    public BigDecimal getTaxAmount0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("taxAmount0");
        else
            return (BigDecimal) getFieldValue("TAXAMT0");
    }

    public void setTaxAmount0(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("taxAmount0", bigdecimal);
        else
            setFieldValue("TAXAMT0", bigdecimal);
    }

    public BigDecimal getTaxAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("TaxAmount1");
        else
            return (BigDecimal) getFieldValue("TAXAMT1");
    }

    public void setTaxAmount1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("taxAmount1", bigdecimal);
        else
            setFieldValue("TAXAMT1", bigdecimal);
    }

    public BigDecimal getTaxAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("taxAmount2");
        else
            return (BigDecimal) getFieldValue("TAXAMT2");
    }

    public void setTaxAmount2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("taxAmount2", bigdecimal);
        else
            setFieldValue("TAXAMT2", bigdecimal);
    }

    public BigDecimal getTaxAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("taxAmount3");
        else
            return (BigDecimal) getFieldValue("TAXAMT3");
    }

    public void setTaxAmount3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("taxAmount3", bigdecimal);
        else
            setFieldValue("TAXAMT3", bigdecimal);
    }

    public BigDecimal getTaxAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("taxAmount4");
        else
            return (BigDecimal) getFieldValue("TAXAMT4");
    }

    public void setTaxAmount4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("taxAmount4", bigdecimal);
        else
            setFieldValue("TAXAMT4", bigdecimal);
    }

    public BigDecimal getTotalTaxAmount() {
        return getTaxAmount0().add(getTaxAmount1()).add(getTaxAmount2()).add(
                getTaxAmount3()).add(getTaxAmount4());
    }

    public BigDecimal getTaxAmount() {
        BigDecimal bigdecimal = new BigDecimal(0.0D);
        if (getTaxAmount0() != null)
            bigdecimal.add(getTaxAmount0());
        if (getTaxAmount1() != null)
            bigdecimal.add(getTaxAmount1());
        if (getTaxAmount2() != null)
            bigdecimal.add(getTaxAmount2());
        if (getTaxAmount3() != null)
            bigdecimal.add(getTaxAmount3());
        if (getTaxAmount4() != null)
            bigdecimal.add(getTaxAmount4());
        return bigdecimal;
    }

    public void setTaxAmount(BigDecimal bigdecimal) {
    }

    public BigDecimal getItemCount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemSaleQuantity");
        else
            return (BigDecimal) getFieldValue("SALECNT");
    }

    public void setItemCount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("itemSaleQuantity", bigdecimal);
        else
            setFieldValue("SALECNT", bigdecimal);
    }

    public Integer getTransactionCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("transactionCount");
        else
            return (Integer) getFieldValue("TRANCNT");
    }

    public void setTransactionCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("transactionCount", integer);
        else
            setFieldValue("TRANCNT", integer);
    }

    public Integer getCustomerCount() {
        if (Server.serverExist())
            return retrieveInteger(getFieldValue("customerCount"));
        else
            return (Integer) getFieldValue("CUSTCNT");
    }

    public void setCustomerCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("customerCount", integer);
        else
            setFieldValue("CUSTCNT", integer);
    }

    public BigDecimal getTaxedAmount() {
        BigDecimal bigdecimal = new BigDecimal(0.0D);
        String s = "0";
        if (TaxType.getTaxedType(s))
            bigdecimal = bigdecimal.add(getNetSalesAmount0());
        s = "1";
        if (TaxType.getTaxedType(s))
            bigdecimal = bigdecimal.add(getNetSalesAmount1());
        s = "2";
        if (TaxType.getTaxedType(s))
            bigdecimal = bigdecimal.add(getNetSalesAmount2());
        s = "3";
        if (TaxType.getTaxedType(s))
            bigdecimal = bigdecimal.add(getNetSalesAmount3());
        s = "4";
        if (TaxType.getTaxedType(s))
            bigdecimal = bigdecimal.add(getNetSalesAmount4());
        return bigdecimal;
    }

    public BigDecimal getNoTaxedAmount() {
        BigDecimal bigdecimal = new BigDecimal(0.0D);
        String s = "0";
        if (!TaxType.getTaxedType(s))
            bigdecimal = bigdecimal.add(getNetSalesAmount0());
        s = "1";
        if (!TaxType.getTaxedType(s))
            bigdecimal = bigdecimal.add(getNetSalesAmount1());
        s = "2";
        if (!TaxType.getTaxedType(s))
            bigdecimal = bigdecimal.add(getNetSalesAmount2());
        s = "3";
        if (!TaxType.getTaxedType(s))
            bigdecimal = bigdecimal.add(getNetSalesAmount3());
        s = "4";
        if (!TaxType.getTaxedType(s))
            bigdecimal = bigdecimal.add(getNetSalesAmount4());
        return bigdecimal;
    }

    public BigDecimal getSIPercentPlusTotalAmount() {
        BigDecimal bigdecimal = getSIPercentPlusAmount0();
        bigdecimal = bigdecimal.add(getSIPercentPlusAmount1());
        bigdecimal = bigdecimal.add(getSIPercentPlusAmount2());
        bigdecimal = bigdecimal.add(getSIPercentPlusAmount3());
        bigdecimal = bigdecimal.add(getSIPercentPlusAmount4());
        return bigdecimal;
    }

    public Integer getSIPercentPlusTotalCount() {
        int i = getSIPercentPlusCount0().intValue();
        i += getSIPercentPlusCount1().intValue();
        i += getSIPercentPlusCount2().intValue();
        i += getSIPercentPlusCount3().intValue();
        i += getSIPercentPlusCount4().intValue();
        return new Integer(i);
    }

    public BigDecimal getSIPercentOffTotalAmount() {
        BigDecimal bigdecimal = getSIPercentOffAmount0();
        bigdecimal = bigdecimal.add(getSIPercentOffAmount1());
        bigdecimal = bigdecimal.add(getSIPercentOffAmount2());
        bigdecimal = bigdecimal.add(getSIPercentOffAmount3());
        bigdecimal = bigdecimal.add(getSIPercentOffAmount4());
        return bigdecimal;
    }

    public Integer getSIPercentOffTotalCount() {
        int i = getSIPercentOffCount0().intValue();
        i += getSIPercentOffCount1().intValue();
        i += getSIPercentOffCount2().intValue();
        i += getSIPercentOffCount3().intValue();
        i += getSIPercentOffCount4().intValue();
        return new Integer(i);
    }

    public BigDecimal getSIDiscountTotalAmount() {
        BigDecimal bigdecimal = getSIDiscountAmount0();
        bigdecimal = bigdecimal.add(getSIDiscountAmount1());
        bigdecimal = bigdecimal.add(getSIDiscountAmount2());
        bigdecimal = bigdecimal.add(getSIDiscountAmount3());
        bigdecimal = bigdecimal.add(getSIDiscountAmount4());
        return bigdecimal;
    }

    public Integer getSIDiscountTotalCount() {
        int i = getSIDiscountCount0().intValue();
        i += getSIDiscountCount1().intValue();
        i += getSIDiscountCount2().intValue();
        i += getSIDiscountCount3().intValue();
        i += getSIDiscountCount4().intValue();
        return new Integer(i);
    }

    public BigDecimal getMixAndMatchTotalAmount() {
        BigDecimal bigdecimal = getMixAndMatchAmount0();
        bigdecimal = bigdecimal.add(getMixAndMatchAmount1());
        bigdecimal = bigdecimal.add(getMixAndMatchAmount2());
        bigdecimal = bigdecimal.add(getMixAndMatchAmount3());
        bigdecimal = bigdecimal.add(getMixAndMatchAmount4());
        return bigdecimal;
    }

    public Integer getMixAndMatchTotalCount() {
        int i = getMixAndMatchCount0().intValue();
        i += getMixAndMatchCount1().intValue();
        i += getMixAndMatchCount2().intValue();
        i += getMixAndMatchCount3().intValue();
        i += getMixAndMatchCount4().intValue();
        return new Integer(i);
    }

    public BigDecimal getNotIncludedTotalSales() {
        BigDecimal bigdecimal = getNotIncludedSales0();
        bigdecimal = bigdecimal.add(getNotIncludedSales1());
        bigdecimal = bigdecimal.add(getNotIncludedSales2());
        bigdecimal = bigdecimal.add(getNotIncludedSales3());
        bigdecimal = bigdecimal.add(getNotIncludedSales4());
        return bigdecimal;
    }

    public BigDecimal getNetSalesTotalAmount() {
        BigDecimal bigdecimal = getNetSalesAmount0();
        bigdecimal = bigdecimal.add(getNetSalesAmount1());
        bigdecimal = bigdecimal.add(getNetSalesAmount2());
        bigdecimal = bigdecimal.add(getNetSalesAmount3());
        bigdecimal = bigdecimal.add(getNetSalesAmount4());
        return bigdecimal;
    }

    public Integer getTotalPayCount() {
        int i = 0;
        for (int j = 0; j < 20; j++) {
            String s = String.valueOf(j);
            if (j < 10)
                s = "0" + j;
            try {
                Integer integer = (Integer) getClass().getDeclaredMethod(
                        "getPay" + s + "Count", new Class[0]).invoke(this,
                        new Object[0]);
                if (integer != null)
                    i += integer.intValue();
            } catch (Exception exception) {
            }
        }

        return new Integer(i);
    }

    public BigDecimal getSIPercentPlusAmount0() {
        if (Server.serverExist())
            return getBigDecimalFieldValue("siPlusTax0Amount");
        else
            return getBigDecimalFieldValue("SIPLUSAMT0");
    }

    public void setSIPercentPlusAmount0(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("siPlusTax0Amount", bigdecimal);
        else
            setFieldValue("SIPLUSAMT0", bigdecimal);
    }

    public Integer getSIPercentPlusCount0() {
        if (Server.serverExist())
            return (Integer) getFieldValue("siPlusTax0Count");
        else
            return (Integer) getFieldValue("SIPLUSCNT0");
    }

    public void setSIPercentPlusCount0(Integer integer) {
        if (Server.serverExist())
            setFieldValue("siPlusTax0Count", integer);
        else
            setFieldValue("SIPLUSCNT0", integer);
    }

    public BigDecimal getSIPercentOffAmount0() {
        if (Server.serverExist())
            return getBigDecimalFieldValue("discountTax0Amount");
        else
            return getBigDecimalFieldValue("SIPAMT0");
    }

    public void setSIPercentOffAmount0(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("discountTax0Amount", bigdecimal);
        else
            setFieldValue("SIPAMT0", bigdecimal);
    }

    public Integer getSIPercentOffCount0() {
        if (Server.serverExist())
            return (Integer) getFieldValue("discountTax0Count");
        else
            return (Integer) getFieldValue("SIPCNT0");
    }

    public void setSIPercentOffCount0(Integer integer) {
        if (Server.serverExist())
            setFieldValue("discountTax0Count", integer);
        else
            setFieldValue("SIPCNT0", integer);
    }

    public BigDecimal getSIDiscountAmount0() {
        if (Server.serverExist())
            return new BigDecimal(0.0D);
        else
            return (BigDecimal) getFieldValue("SIMAMT0");
    }

    public void setSIDiscountAmount0(BigDecimal bigdecimal) {
        if (!Server.serverExist())
            setFieldValue("SIMAMT0", bigdecimal);
    }

    public Integer getSIDiscountCount0() {
        if (Server.serverExist())
            return new Integer(0);
        else
            return (Integer) getFieldValue("SIMCNT0");
    }

    public void setSIDiscountCount0(Integer integer) {
        if (!Server.serverExist())
            setFieldValue("SIMCNT0", integer);
    }

    public BigDecimal getMixAndMatchAmount0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("mixAndMatchTax0Amount");
        else
            return (BigDecimal) getFieldValue("MNMAMT0");
    }

    public void setMixAndMatchAmount0(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax0Count", bigdecimal);
        else
            setFieldValue("MNMAMT0", bigdecimal);
    }

    public Integer getMixAndMatchCount0() {
        if (Server.serverExist())
            return (Integer) getFieldValue("mixAndMatchTax0Count");
        else
            return (Integer) getFieldValue("MNMCNT0");
    }

    public void setMixAndMatchCount0(Integer integer) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax0Count", integer);
        else
            setFieldValue("MNMCNT0", integer);
    }

    public BigDecimal getNotIncludedSales0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("notIncludedTax0Sale");
        else
            return (BigDecimal) getFieldValue("RCPGIFAMT0");
    }

    public void setNotIncludedSales0(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("notIncludedTax0Sale", bigdecimal);
        else
            setFieldValue("RCPGIFAMT0", bigdecimal);
    }

    public BigDecimal getNetSalesAmount0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("netSaleTax0Amount");
        else
            return (BigDecimal) getFieldValue("NETSALAMT0");
    }

    public void setNetSalesAmount0(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("netSaleTax0Amount", bigdecimal);
        else
            setFieldValue("NETSALAMT0", bigdecimal);
    }

    public BigDecimal getSIPercentPlusAmount1() {
        if (Server.serverExist())
            return getBigDecimalFieldValue("siPlusTax1Amount");
        else
            return getBigDecimalFieldValue("SIPLUSAMT1");
    }

    public void setSIPercentPlusAmount1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("siPlusTax1Amount", bigdecimal);
        else
            setFieldValue("SIPLUSAMT1", bigdecimal);
    }

    public Integer getSIPercentPlusCount1() {
        if (Server.serverExist())
            return (Integer) getFieldValue("siPlusTax1Count");
        else
            return (Integer) getFieldValue("SIPLUSCNT1");
    }

    public void setSIPercentPlusCount1(Integer integer) {
        if (Server.serverExist())
            setFieldValue("siPlusTax1Count", integer);
        else
            setFieldValue("SIPLUSCNT1", integer);
    }

    public BigDecimal getSIPercentOffAmount1() {
        if (Server.serverExist())
            return getBigDecimalFieldValue("discountTax1Amount");
        else
            return getBigDecimalFieldValue("SIPAMT1");
    }

    public void setSIPercentOffAmount1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("discountTax1Amount", bigdecimal);
        else
            setFieldValue("SIPAMT1", bigdecimal);
    }

    public Integer getSIPercentOffCount1() {
        if (Server.serverExist())
            return (Integer) getFieldValue("discountTax1Count");
        else
            return (Integer) getFieldValue("SIPCNT1");
    }

    public void setSIPercentOffCount1(Integer integer) {
        if (Server.serverExist())
            setFieldValue("discountTax1Count", integer);
        else
            setFieldValue("SIPCNT1", integer);
    }

    public BigDecimal getSIDiscountAmount1() {
        if (Server.serverExist())
            return new BigDecimal(0.0D);
        else
            return (BigDecimal) getFieldValue("SIMAMT1");
    }

    public void setSIDiscountAmount1(BigDecimal bigdecimal) {
        if (!Server.serverExist())
            setFieldValue("SIMAMT1", bigdecimal);
    }

    public Integer getSIDiscountCount1() {
        if (Server.serverExist())
            return new Integer(0);
        else
            return (Integer) getFieldValue("SIMCNT1");
    }

    public void setSIDiscountCount1(Integer integer) {
        if (!Server.serverExist())
            setFieldValue("SIMCNT1", integer);
    }

    public BigDecimal getMixAndMatchAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("mixAndMatchTax1Amount");
        else
            return (BigDecimal) getFieldValue("MNMAMT1");
    }

    public void setMixAndMatchAmount1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax1Amount", bigdecimal);
        else
            setFieldValue("MNMAMT1", bigdecimal);
    }

    public Integer getMixAndMatchCount1() {
        if (Server.serverExist())
            return (Integer) getFieldValue("mixAndMatchTax1Count");
        else
            return (Integer) getFieldValue("MNMCNT1");
    }

    public void setMixAndMatchCount1(Integer integer) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax1Count", integer);
        else
            setFieldValue("MNMCNT1", integer);
    }

    public BigDecimal getNotIncludedSales1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("notIncludedTax1Sale");
        else
            return (BigDecimal) getFieldValue("RCPGIFAMT1");
    }

    public void setNotIncludedSales1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("notIncludedTax1Sale", bigdecimal);
        else
            setFieldValue("RCPGIFAMT1", bigdecimal);
    }

    public BigDecimal getNetSalesAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("netSaleTax1Amount");
        else
            return (BigDecimal) getFieldValue("NETSALAMT1");
    }

    public void setNetSalesAmount1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("netSaleTax1Amount", bigdecimal);
        else
            setFieldValue("NETSALAMT1", bigdecimal);
    }

    public BigDecimal getSIPercentPlusAmount2() {
        if (Server.serverExist())
            return getBigDecimalFieldValue("siPlusTax2Amount");
        else
            return getBigDecimalFieldValue("SIPLUSAMT2");
    }

    public void setSIPercentPlusAmount2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("siPlusTax2Amount", bigdecimal);
        else
            setFieldValue("SIPLUSAMT2", bigdecimal);
    }

    public Integer getSIPercentPlusCount2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("siPlusTax2Count");
        else
            return (Integer) getFieldValue("SIPLUSCNT2");
    }

    public void setSIPercentPlusCount2(Integer integer) {
        if (Server.serverExist())
            setFieldValue("siPlusTax2Count", integer);
        else
            setFieldValue("SIPLUSCNT2", integer);
    }

    public BigDecimal getSIPercentOffAmount2() {
        if (Server.serverExist())
            return getBigDecimalFieldValue("discountTax2Amount");
        else
            return getBigDecimalFieldValue("SIPAMT2");
    }

    public void setSIPercentOffAmount2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("discountTax2Amount", bigdecimal);
        else
            setFieldValue("SIPAMT2", bigdecimal);
    }

    public Integer getSIPercentOffCount2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("discountTax2Count");
        else
            return (Integer) getFieldValue("SIPCNT2");
    }

    public void setSIPercentOffCount2(Integer integer) {
        if (Server.serverExist())
            setFieldValue("discountTax2Count", integer);
        else
            setFieldValue("SIPCNT2", integer);
    }

    public BigDecimal getSIDiscountAmount2() {
        if (Server.serverExist())
            return new BigDecimal(0.0D);
        else
            return (BigDecimal) getFieldValue("SIMAMT2");
    }

    public void setSIDiscountAmount2(BigDecimal bigdecimal) {
        if (!Server.serverExist())
            setFieldValue("SIMAMT2", bigdecimal);
    }

    public Integer getSIDiscountCount2() {
        if (Server.serverExist())
            return new Integer(0);
        else
            return (Integer) getFieldValue("SIMCNT2");
    }

    public void setSIDiscountCount2(Integer integer) {
        if (!Server.serverExist())
            setFieldValue("SIMCNT2", integer);
    }

    public BigDecimal getMixAndMatchAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("mixAndMatchTax2Amount");
        else
            return (BigDecimal) getFieldValue("MNMAMT2");
    }

    public void setMixAndMatchAmount2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax2Amount", bigdecimal);
        else
            setFieldValue("MNMAMT2", bigdecimal);
    }

    public Integer getMixAndMatchCount2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("mixAndMatchTax2Count");
        else
            return (Integer) getFieldValue("MNMCNT2");
    }

    public void setMixAndMatchCount2(Integer integer) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax2Count", integer);
        else
            setFieldValue("MNMCNT2", integer);
    }

    public BigDecimal getNotIncludedSales2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("notIncludedTax2Sale");
        else
            return (BigDecimal) getFieldValue("RCPGIFAMT2");
    }

    public void setNotIncludedSales2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("notIncludedTax2Sale", bigdecimal);
        else
            setFieldValue("RCPGIFAMT2", bigdecimal);
    }

    public BigDecimal getNetSalesAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("netSaleTax2Amount");
        else
            return (BigDecimal) getFieldValue("NETSALAMT2");
    }

    public void setNetSalesAmount2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("netSaleTax2Amount", bigdecimal);
        else
            setFieldValue("NETSALAMT2", bigdecimal);
    }

    public BigDecimal getSIPercentPlusAmount3() {
        if (Server.serverExist())
            return getBigDecimalFieldValue("siPlusTax3Amount");
        else
            return getBigDecimalFieldValue("SIPLUSAMT3");
    }

    public void setSIPercentPlusAmount3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("siPlusTax3Amount", bigdecimal);
        else
            setFieldValue("SIPLUSAMT3", bigdecimal);
    }

    public Integer getSIPercentPlusCount3() {
        if (Server.serverExist())
            return (Integer) getFieldValue("siPlusTax3Count");
        else
            return (Integer) getFieldValue("SIPLUSCNT3");
    }

    public void setSIPercentPlusCount3(Integer integer) {
        if (Server.serverExist())
            setFieldValue("siPlusTax3Count", integer);
        else
            setFieldValue("SIPLUSCNT3", integer);
    }

    public BigDecimal getSIPercentOffAmount3() {
        if (Server.serverExist())
            return getBigDecimalFieldValue("discountTax3Amount");
        else
            return getBigDecimalFieldValue("SIPAMT3");
    }

    public void setSIPercentOffAmount3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("discountTax3Amount", bigdecimal);
        else
            setFieldValue("SIPAMT3", bigdecimal);
    }

    public Integer getSIPercentOffCount3() {
        if (Server.serverExist())
            return (Integer) getFieldValue("discountTax3Count");
        else
            return (Integer) getFieldValue("SIPCNT3");
    }

    public void setSIPercentOffCount3(Integer integer) {
        if (Server.serverExist())
            setFieldValue("discountTax3Count", integer);
        else
            setFieldValue("SIPCNT3", integer);
    }

    public BigDecimal getSIDiscountAmount3() {
        if (Server.serverExist())
            return new BigDecimal(0.0D);
        else
            return (BigDecimal) getFieldValue("SIMAMT3");
    }

    public void setSIDiscountAmount3(BigDecimal bigdecimal) {
        if (!Server.serverExist())
            setFieldValue("SIMAMT3", bigdecimal);
    }

    public Integer getSIDiscountCount3() {
        if (Server.serverExist())
            return new Integer(0);
        else
            return (Integer) getFieldValue("SIMCNT3");
    }

    public void setSIDiscountCount3(Integer integer) {
        if (!Server.serverExist())
            setFieldValue("SIMCNT3", integer);
    }

    public BigDecimal getMixAndMatchAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("mixAndMatchTax3Amount");
        else
            return (BigDecimal) getFieldValue("MNMAMT3");
    }

    public void setMixAndMatchAmount3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax3Amount", bigdecimal);
        else
            setFieldValue("MNMAMT3", bigdecimal);
    }

    public Integer getMixAndMatchCount3() {
        if (Server.serverExist())
            return (Integer) getFieldValue("mixAndMatchTax3Count");
        else
            return (Integer) getFieldValue("MNMCNT3");
    }

    public void setMixAndMatchCount3(Integer integer) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax3Count", integer);
        else
            setFieldValue("MNMCNT3", integer);
    }

    public BigDecimal getNotIncludedSales3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("notIncludedTax3Sale");
        else
            return (BigDecimal) getFieldValue("RCPGIFAMT3");
    }

    public void setNotIncludedSales3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("notIncludedTax3Sale", bigdecimal);
        else
            setFieldValue("RCPGIFAMT3", bigdecimal);
    }

    public BigDecimal getNetSalesAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("netSaleTax3Amount");
        else
            return (BigDecimal) getFieldValue("NETSALAMT3");
    }

    public void setNetSalesAmount3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("netSaleTax3Amount", bigdecimal);
        else
            setFieldValue("NETSALAMT3", bigdecimal);
    }

    public BigDecimal getSIPercentPlusAmount4() {
        if (Server.serverExist())
            return getBigDecimalFieldValue("siPlusTax4Amount");
        else
            return getBigDecimalFieldValue("SIPLUSAMT4");
    }

    public void setSIPercentPlusAmount4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("siPlusTax4Amount", bigdecimal);
        else
            setFieldValue("SIPLUSAMT4", bigdecimal);
    }

    public Integer getSIPercentPlusCount4() {
        if (Server.serverExist())
            return (Integer) getFieldValue("siPlusTax4Count");
        else
            return (Integer) getFieldValue("SIPLUSCNT4");
    }

    public void setSIPercentPlusCount4(Integer integer) {
        if (Server.serverExist())
            setFieldValue("siPlusTax4Count", integer);
        else
            setFieldValue("SIPLUSCNT4", integer);
    }

    public BigDecimal getSIPercentOffAmount4() {
        if (Server.serverExist())
            return getBigDecimalFieldValue("discountTax4Amount");
        else
            return getBigDecimalFieldValue("SIPAMT4");
    }

    public void setSIPercentOffAmount4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("discountTax4Amount", bigdecimal);
        else
            setFieldValue("SIPAMT4", bigdecimal);
    }

    public Integer getSIPercentOffCount4() {
        if (Server.serverExist())
            return (Integer) getFieldValue("discountTax4Count");
        else
            return (Integer) getFieldValue("SIPCNT4");
    }

    public void setSIPercentOffCount4(Integer integer) {
        if (Server.serverExist())
            setFieldValue("discountTax4Count", integer);
        else
            setFieldValue("SIPCNT4", integer);
    }

    public BigDecimal getSIDiscountAmount4() {
        if (Server.serverExist())
            return new BigDecimal(0.0D);
        else
            return (BigDecimal) getFieldValue("SIMAMT4");
    }

    public void setSIDiscountAmount4(BigDecimal bigdecimal) {
        if (!Server.serverExist())
            setFieldValue("SIMAMT4", bigdecimal);
    }

    public Integer getSIDiscountCount4() {
        if (Server.serverExist())
            return new Integer(0);
        else
            return (Integer) getFieldValue("SIMCNT4");
    }

    public void setSIDiscountCount4(Integer integer) {
        if (!Server.serverExist())
            setFieldValue("SIMCNT4", integer);
    }

    public BigDecimal getMixAndMatchAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("mixAndMatchTax4Amount");
        else
            return (BigDecimal) getFieldValue("MNMAMT4");
    }

    public void setMixAndMatchAmount4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax4Amount", bigdecimal);
        else
            setFieldValue("MNMAMT4", bigdecimal);
    }

    public Integer getMixAndMatchCount4() {
        if (Server.serverExist())
            return (Integer) getFieldValue("mixAndMatchTax4Count");
        else
            return (Integer) getFieldValue("MNMCNT4");
    }

    public void setMixAndMatchCount4(Integer integer) {
        if (Server.serverExist())
            setFieldValue("mixAndMatchTax4Count", integer);
        else
            setFieldValue("MNMCNT4", integer);
    }

    public BigDecimal getNotIncludedSales4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("notIncludedTax4Sale");
        else
            return (BigDecimal) getFieldValue("RCPGIFAMT4");
    }

    public void setNotIncludedSales4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("notIncludedTax4Sale", bigdecimal);
        else
            setFieldValue("RCPGIFAMT4", bigdecimal);
    }

    public BigDecimal getNetSalesAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("netSaleTax4Amount");
        else
            return (BigDecimal) getFieldValue("NETSALAMT4");
    }

    public void setNetSalesAmount4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("netSaleTax4Amount", bigdecimal);
        else
            setFieldValue("NETSALAMT4", bigdecimal);
    }

    public BigDecimal getVoucherAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("totalVoucherAmount");
        else
            return (BigDecimal) getFieldValue("VALPAMT");
    }

    public void setVoucherAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("totalVoucherAmount", bigdecimal);
        else
            setFieldValue("VALPAMT", bigdecimal);
    }

    public Integer getVoucherCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("totalVoucherCount");
        else
            return (Integer) getFieldValue("VALPCNT");
    }

    public void setVoucherCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("totalVoucherCount", integer);
        else
            setFieldValue("VALPCNT", integer);
    }

    public BigDecimal getPreRcvAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("preRcvAmount");
        else
            return (BigDecimal) getFieldValue("PRCVAMT");
    }

    public void setPreRcvAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("preRcvAmount", bigdecimal);
        else
            setFieldValue("PRCVAMT", bigdecimal);
    }

    public Integer getPreRcvCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("preRcvCount");
        else
            return (Integer) getFieldValue("PRCVCNT");
    }

    public void setPreRcvCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("preRcvCount", integer);
        else
            setFieldValue("PRCVCNT", integer);
    }

    public BigDecimal getPreRcvDetailAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("preRcvDetailAmount1");
        else
            return (BigDecimal) getFieldValue("PRCVPLUAMT1");
    }

    public void setPreRcvDetailAmount1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("preRcvDetailAmount1", bigdecimal);
        else
            setFieldValue("PRCVPLUAMT1", bigdecimal);
    }

    public Integer getPreRcvDetailCount1() {
        if (Server.serverExist())
            return (Integer) getFieldValue("preRcvDetailCount1");
        else
            return (Integer) getFieldValue("PRCVPLUCNT1");
    }

    public void setPreRcvDetailCount1(Integer integer) {
        if (Server.serverExist())
            setFieldValue("preRcvDetailCount1", integer);
        else
            setFieldValue("PRCVPLUCNT1", integer);
    }

    public BigDecimal getPreRcvDetailAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("preRcvDetailAmount2");
        else
            return (BigDecimal) getFieldValue("PRCVPLUAMT2");
    }

    public void setPreRcvDetailAmount2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("preRcvDetailAmount2", bigdecimal);
        else
            setFieldValue("PRCVPLUAMT2", bigdecimal);
    }

    public Integer getPreRcvDetailCount2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("preRcvDetailCount2");
        else
            return (Integer) getFieldValue("PRCVPLUCNT2");
    }

    public void setPreRcvDetailCount2(Integer integer) {
        if (Server.serverExist())
            setFieldValue("preRcvDetailCount2", integer);
        else
            setFieldValue("PRCVPLUCNT2", integer);
    }

    public BigDecimal getPreRcvDetailAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("preRcvDetailAmount3");
        else
            return (BigDecimal) getFieldValue("PRCVPLUAMT3");
    }

    public void setPreRcvDetailAmount3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("preRcvDetailAmount3", bigdecimal);
        else
            setFieldValue("PRCVPLUAMT3", bigdecimal);
    }

    public Integer getPreRcvDetailCount3() {
        if (Server.serverExist())
            return (Integer) getFieldValue("preRcvDetailCount3");
        else
            return (Integer) getFieldValue("PRCVPLUCNT3");
    }

    public void setPreRcvDetailCount3(Integer integer) {
        if (Server.serverExist())
            setFieldValue("preRcvDetailCount3", integer);
        else
            setFieldValue("PRCVPLUCNT3", integer);
    }

    public BigDecimal getPreRcvDetailAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("preRcvDetailAmount4");
        else
            return (BigDecimal) getFieldValue("PRCVPLUAMT4");
    }

    public void setPreRcvDetailAmount4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("preRcvDetailAmount4", bigdecimal);
        else
            setFieldValue("PRCVPLUAMT4", bigdecimal);
    }

    public Integer getPreRcvDetailCount4() {
        if (Server.serverExist())
            return (Integer) getFieldValue("preRcvDetailCount4");
        else
            return (Integer) getFieldValue("PRCVPLUCNT4");
    }

    public void setPreRcvDetailCount4(Integer integer) {
        if (Server.serverExist())
            setFieldValue("preRcvDetailCount4", integer);
        else
            setFieldValue("PRCVPLUCNT4", integer);
    }

    public BigDecimal getPreRcvDetailAmount5() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("preRcvDetailAmount5");
        else
            return (BigDecimal) getFieldValue("PRCVPLUAMT5");
    }

    public void setPreRcvDetailAmount5(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("preRcvDetailAmount5", bigdecimal);
        else
            setFieldValue("PRCVPLUAMT5", bigdecimal);
    }

    public Integer getPreRcvDetailCount5() {
        if (Server.serverExist())
            return (Integer) getFieldValue("preRcvDetailCount5");
        else
            return (Integer) getFieldValue("PRCVPLUCNT5");
    }

    public void setPreRcvDetailCount5(Integer integer) {
        if (Server.serverExist())
            setFieldValue("preRcvDetailCount5", integer);
        else
            setFieldValue("PRCVPLUCNT5", integer);
    }

    public BigDecimal getDaiShouAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("totalDaiShouAmount");
        else
            return (BigDecimal) getFieldValue("DAISHOUAMT");
    }

    public void setDaiShouAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("totalDaiShouAmount", bigdecimal);
        else
            setFieldValue("DAISHOUAMT", bigdecimal);
    }

    public Integer getDaiShouCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("totalDaiShouCount");
        else
            return (Integer) getFieldValue("DAISHOUCNT");
    }

    public void setDaiShouCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("totalDaiShouCount", integer);
        else
            setFieldValue("DAISHOUCNT", integer);
    }

    public BigDecimal getDaiShouDetailAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiShouDetailAmount1");
        else
            return (BigDecimal) getFieldValue("DAISHOUPLUAMT1");
    }

    public void setDaiShouDetailAmount1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("daiShouDetailAmount1", bigdecimal);
        else
            setFieldValue("DAISHOUPLUAMT1", bigdecimal);
    }

    public Integer getDaiShouDetailCount1() {
        if (Server.serverExist())
            return (Integer) getFieldValue("daiShouDetailCount1");
        else
            return (Integer) getFieldValue("DAISHOUPLUCNT1");
    }

    public void setDaiShouDetailCount1(Integer integer) {
        if (Server.serverExist())
            setFieldValue("daiShouDetailCount1", integer);
        else
            setFieldValue("DAISHOUPLUCNT1", integer);
    }

    public BigDecimal getDaiShouDetailAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiShouDetailAmount2");
        else
            return (BigDecimal) getFieldValue("DAISHOUPLUAMT2");
    }

    public void setDaiShouDetailAmount2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("daiShouDetailAmount2", bigdecimal);
        else
            setFieldValue("DAISHOUPLUAMT2", bigdecimal);
    }

    public Integer getDaiShouDetailCount2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("daiShouDetailCount2");
        else
            return (Integer) getFieldValue("DAISHOUPLUCNT2");
    }

    public void setDaiShouDetailCount2(Integer integer) {
        if (Server.serverExist())
            setFieldValue("daiShouDetailCount2", integer);
        else
            setFieldValue("DAISHOUPLUCNT2", integer);
    }

    public BigDecimal getDaiShouDetailAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiShouDetailAmount3");
        else
            return (BigDecimal) getFieldValue("DAISHOUPLUAMT3");
    }

    public void setDaiShouDetailAmount3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("daiShouDetailAmount3", bigdecimal);
        else
            setFieldValue("DAISHOUPLUAMT3", bigdecimal);
    }

    public Integer getDaiShouDetailCount3() {
        if (Server.serverExist())
            return (Integer) getFieldValue("daiShouDetailCount3");
        else
            return (Integer) getFieldValue("DAISHOUPLUCNT3");
    }

    public void setDaiShouDetailCount3(Integer integer) {
        if (Server.serverExist())
            setFieldValue("daiShouDetailCount3", integer);
        else
            setFieldValue("DAISHOUPLUCNT3", integer);
    }

    public BigDecimal getDaiShouDetailAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiShouDetailAmount4");
        else
            return (BigDecimal) getFieldValue("DAISHOUPLUAMT4");
    }

    public void setDaiShouDetailAmount4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("daiShouDetailAmount4", bigdecimal);
        else
            setFieldValue("DAISHOUPLUAMT4", bigdecimal);
    }

    public Integer getDaiShouDetailCount4() {
        if (Server.serverExist())
            return (Integer) getFieldValue("daiShouDetailCount4");
        else
            return (Integer) getFieldValue("DAISHOUPLUCNT4");
    }

    public void setDaiShouDetailCount4(Integer integer) {
        if (Server.serverExist())
            setFieldValue("daiShouDetailCount4", integer);
        else
            setFieldValue("DAISHOUPLUCNT4", integer);
    }

    public BigDecimal getDaiShouDetailAmount5() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiShouDetailAmount5");
        else
            return (BigDecimal) getFieldValue("DAISHOUPLUAMT5");
    }

    public void setDaiShouDetailAmount5(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("daiShouDetailAmount5", bigdecimal);
        else
            setFieldValue("DAISHOUPLUAMT5", bigdecimal);
    }

    public Integer getDaiShouDetailCount5() {
        if (Server.serverExist())
            return (Integer) getFieldValue("daiShouDetailCount5");
        else
            return (Integer) getFieldValue("DAISHOUPLUCNT5");
    }

    public void setDaiShouDetailCount5(Integer integer) {
        if (Server.serverExist())
            setFieldValue("daiShouDetailCount5", integer);
        else
            setFieldValue("DAISHOUPLUCNT5", integer);
    }

    public BigDecimal getDaiShouAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("totalDaiShouAmount2");
        else
            return (BigDecimal) getFieldValue("DAISHOUAMT2");
    }

    public void setDaiShouAmount2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("totalDaiShouAmount2", bigdecimal);
        else
            setFieldValue("DAISHOUAMT2", bigdecimal);
    }

    public Integer getDaiShouCount2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("totalDaiShouCount2");
        else
            return (Integer) getFieldValue("DAISHOUCNT2");
    }

    public void setDaiShouCount2(Integer integer) {
        if (Server.serverExist())
            setFieldValue("totalDaiShouCount2", integer);
        else
            setFieldValue("DAISHOUCNT2", integer);
    }

    public BigDecimal getDaiFuAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("totalDaiFuAmount");
        else
            return (BigDecimal) getFieldValue("DAIFUAMT");
    }

    public void setDaiFuAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("totalDaiFuAmount", bigdecimal);
        else
            setFieldValue("DAIFUAMT", bigdecimal);
    }

    public Integer getDaiFuCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("totalDaiFuCount");
        else
            return (Integer) getFieldValue("DAIFUCNT");
    }

    public void setDaiFuCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("totalDaiFuCount", integer);
        else
            setFieldValue("DAIFUCNT", integer);
    }

    public BigDecimal getDaiFuDetailAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiFuDetailAmount1");
        else
            return (BigDecimal) getFieldValue("DAIFUPLUAMT1");
    }

    public void setDaiFuDetailAmount1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("daiFuDetailAmount1", bigdecimal);
        else
            setFieldValue("DAIFUPLUAMT1", bigdecimal);
    }

    public Integer getDaiFuDetailCount1() {
        if (Server.serverExist())
            return (Integer) getFieldValue("daiFuDetailCount1");
        else
            return (Integer) getFieldValue("DAIFUPLUCNT1");
    }

    public void setDaiFuDetailCount1(Integer integer) {
        if (Server.serverExist())
            setFieldValue("daiFuDetailCount1", integer);
        else
            setFieldValue("DAIFUPLUCNT1", integer);
    }

    public BigDecimal getDaiFuDetailAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiFuDetailAmount2");
        else
            return (BigDecimal) getFieldValue("DAIFUPLUAMT2");
    }

    public void setDaiFuDetailAmount2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("daiFuDetailAmount2", bigdecimal);
        else
            setFieldValue("DAIFUPLUAMT2", bigdecimal);
    }

    public Integer getDaiFuDetailCount2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("daiFuDetailCount2");
        else
            return (Integer) getFieldValue("DAIFUPLUCNT2");
    }

    public void setDaiFuDetailCount2(Integer integer) {
        if (Server.serverExist())
            setFieldValue("daiFuDetailCount2", integer);
        else
            setFieldValue("DAIFUPLUCNT2", integer);
    }

    public BigDecimal getDaiFuDetailAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiFuDetailAmount3");
        else
            return (BigDecimal) getFieldValue("DAIFUPLUAMT3");
    }

    public void setDaiFuDetailAmount3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("daiFuDetailAmount3", bigdecimal);
        else
            setFieldValue("DAIFUPLUAMT3", bigdecimal);
    }

    public Integer getDaiFuDetailCount3() {
        if (Server.serverExist())
            return (Integer) getFieldValue("daiFuDetailCount3");
        else
            return (Integer) getFieldValue("DAIFUPLUCNT3");
    }

    public void setDaiFuDetailCount3(Integer integer) {
        if (Server.serverExist())
            setFieldValue("daiFuDetailCount3", integer);
        else
            setFieldValue("DAIFUPLUCNT3", integer);
    }

    public BigDecimal getDaiFuDetailAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiFuDetailAmount4");
        else
            return (BigDecimal) getFieldValue("DAIFUPLUAMT4");
    }

    public void setDaiFuDetailAmount4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("daiFuDetailAmount4", bigdecimal);
        else
            setFieldValue("DAIFUPLUAMT4", bigdecimal);
    }

    public Integer getDaiFuDetailCount4() {
        if (Server.serverExist())
            return (Integer) getFieldValue("daiFuDetailCount4");
        else
            return (Integer) getFieldValue("DAIFUPLUCNT4");
    }

    public void setDaiFuDetailCount4(Integer integer) {
        if (Server.serverExist())
            setFieldValue("daiFuDetailCount4", integer);
        else
            setFieldValue("DAIFUPLUCNT4", integer);
    }

    public BigDecimal getDaiFuDetailAmount5() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("daiFuDetailAmount5");
        else
            return (BigDecimal) getFieldValue("DAIFUPLUAMT5");
    }

    public void setDaiFuDetailAmount5(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("daiFuDetailAmount5", bigdecimal);
        else
            setFieldValue("DAIFUPLUAMT5", bigdecimal);
    }

    public Integer getDaiFuDetailCount5() {
        if (Server.serverExist())
            return (Integer) getFieldValue("daiFuDetailCount5");
        else
            return (Integer) getFieldValue("DAIFUPLUCNT5");
    }

    public void setDaiFuDetailCount5(Integer integer) {
        if (Server.serverExist())
            setFieldValue("daiFuDetailCount5", integer);
        else
            setFieldValue("DAIFUPLUCNT5", integer);
    }

    public BigDecimal getPaidInAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("totalPaidinAmount");
        else
            return (BigDecimal) getFieldValue("PINAMT");
    }

    public void setPaidInAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("totalPaidinAmount", bigdecimal);
        else
            setFieldValue("PINAMT", bigdecimal);
    }

    public Integer getPaidInCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("totalPaidinCount");
        else
            return (Integer) getFieldValue("PINCNT");
    }

    public void setPaidInCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("totalPaidinCount", integer);
        else
            setFieldValue("PINCNT", integer);
    }

    public BigDecimal getPaidInDetailAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("paidInDetailAmount1");
        else
            return (BigDecimal) getFieldValue("PINPLUAMT1");
    }

    public void setPaidInDetailAmount1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("paidInDetailAmount1", bigdecimal);
        else
            setFieldValue("PINPLUAMT1", bigdecimal);
    }

    public Integer getPaidInDetailCount1() {
        if (Server.serverExist())
            return (Integer) getFieldValue("paidInDetailCount1");
        else
            return (Integer) getFieldValue("PINPLUCNT1");
    }

    public void setPaidInDetailCount1(Integer integer) {
        if (Server.serverExist())
            setFieldValue("paidInDetailCount1", integer);
        else
            setFieldValue("PINPLUCNT1", integer);
    }

    public BigDecimal getPaidInDetailAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("paidInDetailAmount2");
        else
            return (BigDecimal) getFieldValue("PINPLUAMT2");
    }

    public void setPaidInDetailAmount2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("paidInDetailAmount2", bigdecimal);
        else
            setFieldValue("PINPLUAMT2", bigdecimal);
    }

    public Integer getPaidInDetailCount2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("paidInDetailCount2");
        else
            return (Integer) getFieldValue("PINPLUCNT2");
    }

    public void setPaidInDetailCount2(Integer integer) {
        if (Server.serverExist())
            setFieldValue("paidInDetailCount2", integer);
        else
            setFieldValue("PINPLUCNT2", integer);
    }

    public BigDecimal getPaidInDetailAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("paidInDetailAmount3");
        else
            return (BigDecimal) getFieldValue("PINPLUAMT3");
    }

    public void setPaidInDetailAmount3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("paidInDetailAmount3", bigdecimal);
        else
            setFieldValue("PINPLUAMT3", bigdecimal);
    }

    public Integer getPaidInDetailCount3() {
        if (Server.serverExist())
            return (Integer) getFieldValue("paidInDetailCount3");
        else
            return (Integer) getFieldValue("PINPLUCNT3");
    }

    public void setPaidInDetailCount3(Integer integer) {
        if (Server.serverExist())
            setFieldValue("paidInDetailCount3", integer);
        else
            setFieldValue("PINPLUCNT3", integer);
    }

    public BigDecimal getPaidInDetailAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("paidInDetailAmount4");
        else
            return (BigDecimal) getFieldValue("PINPLUAMT4");
    }

    public void setPaidInDetailAmount4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("paidInDetailAmount4", bigdecimal);
        else
            setFieldValue("PINPLUAMT4", bigdecimal);
    }

    public Integer getPaidInDetailCount4() {
        if (Server.serverExist())
            return (Integer) getFieldValue("paidInDetailCount4");
        else
            return (Integer) getFieldValue("PINPLUCNT4");
    }

    public void setPaidInDetailCount4(Integer integer) {
        if (Server.serverExist())
            setFieldValue("paidInDetailCount4", integer);
        else
            setFieldValue("PINPLUCNT4", integer);
    }

    public BigDecimal getPaidInDetailAmount5() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("paidInDetailAmount5");
        else
            return (BigDecimal) getFieldValue("PINPLUAMT5");
    }

    public void setPaidInDetailAmount5(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("paidInDetailAmount5", bigdecimal);
        else
            setFieldValue("PINPLUAMT5", bigdecimal);
    }

    public Integer getPaidInDetailCount5() {
        if (Server.serverExist())
            return (Integer) getFieldValue("paidInDetailCount5");
        else
            return (Integer) getFieldValue("PINPLUCNT5");
    }

    public void setPaidInDetailCount5(Integer integer) {
        if (Server.serverExist())
            setFieldValue("paidInDetailCount5", integer);
        else
            setFieldValue("PINPLUCNT5", integer);
    }

    public BigDecimal getPaidOutAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("totalPaidoutAmount");
        else
            return (BigDecimal) getFieldValue("POUTAMT");
    }

    public void setPaidOutAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("totalPaidoutAmount", bigdecimal);
        else
            setFieldValue("POUTAMT", bigdecimal);
    }

    public Integer getPaidOutCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("totalPaidoutCount");
        else
            return (Integer) getFieldValue("POUTCNT");
    }

    public void setPaidOutCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("totalPaidoutCount", integer);
        else
            setFieldValue("POUTCNT", integer);
    }

    public BigDecimal getPaidOutDetailAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("paidOutDetailAmount1");
        else
            return (BigDecimal) getFieldValue("POUTPLUAMT1");
    }

    public void setPaidOutDetailAmount1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("paidOutDetailAmount1", bigdecimal);
        else
            setFieldValue("POUTPLUAMT1", bigdecimal);
    }

    public Integer getPaidOutDetailCount1() {
        if (Server.serverExist())
            return (Integer) getFieldValue("paidOutDetailCount1");
        else
            return (Integer) getFieldValue("POUTPLUCNT1");
    }

    public void setPaidOutDetailCount1(Integer integer) {
        if (Server.serverExist())
            setFieldValue("paidOutDetailCount1", integer);
        else
            setFieldValue("POUTPLUCNT1", integer);
    }

    public BigDecimal getPaidOutDetailAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("paidOutDetailAmount2");
        else
            return (BigDecimal) getFieldValue("POUTPLUAMT2");
    }

    public void setPaidOutDetailAmount2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("paidOutDetailAmount2", bigdecimal);
        else
            setFieldValue("POUTPLUAMT2", bigdecimal);
    }

    public Integer getPaidOutDetailCount2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("paidOutDetailCount2");
        else
            return (Integer) getFieldValue("POUTPLUCNT2");
    }

    public void setPaidOutDetailCount2(Integer integer) {
        if (Server.serverExist())
            setFieldValue("paidOutDetailCount2", integer);
        else
            setFieldValue("POUTPLUCNT2", integer);
    }

    public BigDecimal getPaidOutDetailAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("paidOutDetailAmount3");
        else
            return (BigDecimal) getFieldValue("POUTPLUAMT3");
    }

    public void setPaidOutDetailAmount3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("paidOutDetailAmount3", bigdecimal);
        else
            setFieldValue("POUTPLUAMT3", bigdecimal);
    }

    public Integer getPaidOutDetailCount3() {
        if (Server.serverExist())
            return (Integer) getFieldValue("paidOutDetailCount3");
        else
            return (Integer) getFieldValue("POUTPLUCNT3");
    }

    public void setPaidOutDetailCount3(Integer integer) {
        if (Server.serverExist())
            setFieldValue("paidOutDetailCount3", integer);
        else
            setFieldValue("POUTPLUCNT3", integer);
    }

    public BigDecimal getPaidOutDetailAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("paidOutDetailAmount4");
        else
            return (BigDecimal) getFieldValue("POUTPLUAMT4");
    }

    public void setPaidOutDetailAmount4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("paidOutDetailAmount4", bigdecimal);
        else
            setFieldValue("POUTPLUAMT4", bigdecimal);
    }

    public Integer getPaidOutDetailCount4() {
        if (Server.serverExist())
            return (Integer) getFieldValue("paidOutDetailCount4");
        else
            return (Integer) getFieldValue("POUTPLUCNT4");
    }

    public void setPaidOutDetailCount4(Integer integer) {
        if (Server.serverExist())
            setFieldValue("paidOutDetailCount4", integer);
        else
            setFieldValue("POUTPLUCNT4", integer);
    }

    public BigDecimal getPaidOutDetailAmount5() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("paidOutDetailAmount5");
        else
            return (BigDecimal) getFieldValue("POUTPLUAMT5");
    }

    public void setPaidOutDetailAmount5(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("paidOutDetailAmount5", bigdecimal);
        else
            setFieldValue("POUTPLUAMT5", bigdecimal);
    }

    public Integer getPaidOutDetailCount5() {
        if (Server.serverExist())
            return (Integer) getFieldValue("paidOutDetailCount5");
        else
            return (Integer) getFieldValue("POUTPLUCNT5");
    }

    public void setPaidOutDetailCount5(Integer integer) {
        if (Server.serverExist())
            setFieldValue("paidOutDetailCount5", integer);
        else
            setFieldValue("POUTPLUCNT5", integer);
    }

    public BigDecimal getExchangeAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("exchangeItemAmount");
        else
            return (BigDecimal) getFieldValue("EXGAMT");
    }

    public void setExchangeAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("exchangeItemAmount", bigdecimal);
        else
            setFieldValue("EXGAMT", bigdecimal);
    }

    public BigDecimal getExchangeItemQuantity() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("exchangeItemQuantity");
        else
            return (BigDecimal) getFieldValue("EXGITEMQTY");
    }

    public void setExchangeItemQuantity(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("exchangeItemQuantity", bigdecimal);
        else
            setFieldValue("EXGITEMQTY", bigdecimal);
    }

    public BigDecimal getReturnAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("returnItemAmount");
        else
            return (BigDecimal) getFieldValue("RTNAMT");
    }

    public void setReturnAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("returnItemAmount", bigdecimal);
        else
            setFieldValue("RTNAMT", bigdecimal);
    }

    public BigDecimal getReturnItemQuantity() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("returnItemQuantity");
        else
            return (BigDecimal) getFieldValue("RTNITEMQTY");
    }

    public void setReturnItemQuantity(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("returnItemQuantity", bigdecimal);
        else
            setFieldValue("RTNITEMQTY", bigdecimal);
    }

    public Integer getReturnTransactionCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("returnTransactionCount");
        else
            return (Integer) getFieldValue("RTNTRANCNT");
    }

    public void setReturnTransactionCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("returnTransactionCount", integer);
        else
            setFieldValue("RTNTRANCNT", integer);
    }

    public BigDecimal getTransactionVoidAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("transactionVoidAmount");
        else
            return (BigDecimal) getFieldValue("VOIDINVAMT");
    }

    public void setTransactionVoidAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("transactionVoidAmount", bigdecimal);
        else
            setFieldValue("VOIDINVAMT", bigdecimal);
    }

    public Integer getTransactionVoidCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("transactionVoidCount");
        else
            return (Integer) getFieldValue("VOIDINVCNT");
    }

    public void setTransactionVoidCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("transactionVoidCount", integer);
        else
            setFieldValue("VOIDINVCNT", integer);
    }

    public BigDecimal getTransactionCancelAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("transactionCancelAmount");
        else
            return (BigDecimal) getFieldValue("CANCELAMT");
    }

    public void setTransactionCancelAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("transactionCancelAmount", bigdecimal);
        else
            setFieldValue("CANCELAMT", bigdecimal);
    }

    public Integer getTransactionCancelCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("transactionCancelCount");
        else
            return (Integer) getFieldValue("CANCELCNT");
    }

    public void setTransactionCancelCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("transactionCancelCount", integer);
        else
            setFieldValue("CANCELCNT", integer);
    }

    public BigDecimal getLineVoidAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("lineVoidAmount");
        else
            return (BigDecimal) getFieldValue("UPDAMT");
    }

    public void setLineVoidAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("lineVoidAmount", bigdecimal);
        else
            setFieldValue("UPDAMT", bigdecimal);
    }

    public Integer getLineVoidCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("lineVoidCount");
        else
            return (Integer) getFieldValue("UPDCNT");
    }

    public void setLineVoidCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("lineVoidCount", integer);
        else
            setFieldValue("UPDCNT", integer);
    }

    public BigDecimal getVoidAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("voidAmount");
        else
            return (BigDecimal) getFieldValue("NOWUPDAMT");
    }

    public void setVoidAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("voidAmount", bigdecimal);
        else
            setFieldValue("NOWUPDAMT", bigdecimal);
    }

    public Integer getVoidCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("voidCount");
        else
            return (Integer) getFieldValue("NOWUPDCNT");
    }

    public void setVoidCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("voidCount", integer);
        else
            setFieldValue("NOWUPDCNT", integer);
    }

    public BigDecimal getManualEntryAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("manualEntryAmount");
        else
            return (BigDecimal) getFieldValue("ENTRYAMT");
    }

    public void setManualEntryAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("manualEntryAmount", bigdecimal);
        else
            setFieldValue("ENTRYAMT", bigdecimal);
    }

    public Integer getManualEntryCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("manualEntryCount");
        else
            return (Integer) getFieldValue("ENTRYCNT");
    }

    public void setManualEntryCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("manualEntryCount", integer);
        else
            setFieldValue("ENTRYCNT", integer);
    }

    public BigDecimal getPriceOpenEntryAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("priceOpenEntryAmount");
        else
            return (BigDecimal) getFieldValue("POENTRYAMT");
    }

    public void setPriceOpenEntryAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("priceOpenEntryAmount", bigdecimal);
        else
            setFieldValue("POENTRYAMT", bigdecimal);
    }

    public Integer getPriceOpenEntryCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("priceOpenEntryCount");
        else
            return (Integer) getFieldValue("POENTRYCNT");
    }

    public void setPriceOpenEntryCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("priceOpenEntryCount", integer);
        else
            setFieldValue("POENTRYCNT", integer);
    }

    public BigDecimal getCashReturnAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("cashReturnAmount");
        else
            return (BigDecimal) getFieldValue("RTNCSHAMT");
    }

    public void setCashReturnAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("cashReturnAmount", bigdecimal);
        else
            setFieldValue("RTNCSHAMT", bigdecimal);
    }

    public Integer getCashReturnCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("cashReturnCount");
        else
            return (Integer) getFieldValue("RTNCSHCNT");
    }

    public void setCashReturnCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("cashReturnCount", integer);
        else
            setFieldValue("RTNCSHCNT", integer);
    }

    public Integer getDrawerOpenCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("drawerOpenCount");
        else
            return (Integer) getFieldValue("DROPENCNT");
    }

    public void setDrawerOpenCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("drawerOpenCount", integer);
        else
            setFieldValue("DROPENCNT", integer);
    }

    public BigDecimal getReprintAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("reprintAmount");
        else
            return (BigDecimal) getFieldValue("REPRTAMT");
    }

    public void setReprintAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("reprintAmount", bigdecimal);
        else
            setFieldValue("REPRTAMT", bigdecimal);
    }

    public Integer getReprintCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("reprintCount");
        else
            return (Integer) getFieldValue("REPRTCNT");
    }

    public void setReprintCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("reprintCount", integer);
        else
            setFieldValue("REPRTCNT", integer);
    }

    public BigDecimal getCashInAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("cashInAmount");
        else
            return (BigDecimal) getFieldValue("CASHINAMT");
    }

    public void setCashInAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("cashInAmount", bigdecimal);
        else
            setFieldValue("CASHINAMT", bigdecimal);
    }

    public Integer getCashInCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("cashInCount");
        else
            return (Integer) getFieldValue("CASHINCNT");
    }

    public void setCashInCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("cashInCount", integer);
        else
            setFieldValue("CASHINCNT", integer);
    }

    public BigDecimal getCashOutAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("cashOutAmount");
        else
            return (BigDecimal) getFieldValue("CASHOUTAMT");
    }

    public void setCashOutAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("cashOutAmount", bigdecimal);
        else
            setFieldValue("CASHOUTAMT", bigdecimal);
    }

    public Integer getCashOutCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("cashOutCount");
        else
            return (Integer) getFieldValue("CASHOUTCNT");
    }

    public void setCashOutCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("cashOutCount", integer);
        else
            setFieldValue("CASHOUTCNT", integer);
    }

    public BigDecimal getSpillAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("spillAmount");
        else
            return (BigDecimal) getFieldValue("OVERAMT");
    }

    public void setSpillAmount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("spillAmount", bigdecimal);
        else
            setFieldValue("OVERAMT", bigdecimal);
    }

    public Integer getSpillCount() {
        if (Server.serverExist())
            return (Integer) getFieldValue("spillCount");
        else
            return (Integer) getFieldValue("OVERCNT");
    }

    public void setSpillCount(Integer integer) {
        if (Server.serverExist())
            setFieldValue("spillCount", integer);
        else
            setFieldValue("OVERCNT", integer);
    }

    public BigDecimal getExchangeRate1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("exchangeRate1");
        else
            return (BigDecimal) getFieldValue("RATE1");
    }

    public void setExchangeRate1(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("exchangeRate1", bigdecimal);
        else
            setFieldValue("RATE1", bigdecimal);
    }

    public BigDecimal getExchangeRate2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("exchangeRate2");
        else
            return (BigDecimal) getFieldValue("RATE2");
    }

    public void setExchangeRate2(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("exchangeRate2", bigdecimal);
        else
            setFieldValue("RATE2", bigdecimal);
    }

    public BigDecimal getExchangeRate3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("exchangeRate3");
        else
            return (BigDecimal) getFieldValue("RATE3");
    }

    public void setExchangeRate3(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("exchangeRate3", bigdecimal);
        else
            setFieldValue("RATE3", bigdecimal);
    }

    public BigDecimal getExchangeRate4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("exchangeRate4");
        else
            return (BigDecimal) getFieldValue("RATE4");
    }

    public void setExchangeRate4(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("exchangeRate4", bigdecimal);
        else
            setFieldValue("RATE4", bigdecimal);
    }

    public BigDecimal getPay00Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay00Amount");
        else
            return (BigDecimal) getFieldValue("PAY00AMT");
    }

    public void setPay00Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay00Amount", bigdecimal);
        else
            setFieldValue("PAY00AMT", bigdecimal);
    }

    public Integer getPay00Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay00Count");
        else
            return (Integer) getFieldValue("PAY00CNT");
    }

    public void setPay00Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay00Count", integer);
        else
            setFieldValue("PAY00CNT", integer);
    }

    public BigDecimal getPay01Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay01Amount");
        else
            return (BigDecimal) getFieldValue("PAY01AMT");
    }

    public void setPay01Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay01Amount", bigdecimal);
        else
            setFieldValue("PAY01AMT", bigdecimal);
    }

    public Integer getPay01Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay01Count");
        else
            return (Integer) getFieldValue("PAY01CNT");
    }

    public void setPay01Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay01Count", integer);
        else
            setFieldValue("PAY01CNT", integer);
    }

    public BigDecimal getPay02Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay02Amount");
        else
            return (BigDecimal) getFieldValue("PAY02AMT");
    }

    public void setPay02Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay02Amount", bigdecimal);
        else
            setFieldValue("PAY02AMT", bigdecimal);
    }

    public Integer getPay02Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay02Count");
        else
            return (Integer) getFieldValue("PAY02CNT");
    }

    public void setPay02Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay02Count", integer);
        else
            setFieldValue("PAY02CNT", integer);
    }

    public BigDecimal getPay03Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay03Amount");
        else
            return (BigDecimal) getFieldValue("PAY03AMT");
    }

    public void setPay03Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay03Amount", bigdecimal);
        else
            setFieldValue("PAY03AMT", bigdecimal);
    }

    public Integer getPay03Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay03Count");
        else
            return (Integer) getFieldValue("PAY03CNT");
    }

    public void setPay03Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay03Count", integer);
        else
            setFieldValue("PAY03CNT", integer);
    }

    public BigDecimal getPay04Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay04Amount");
        else
            return (BigDecimal) getFieldValue("PAY04AMT");
    }

    public void setPay04Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay04Amount", bigdecimal);
        else
            setFieldValue("PAY04AMT", bigdecimal);
    }

    public Integer getPay04Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay04Count");
        else
            return (Integer) getFieldValue("PAY04CNT");
    }

    public void setPay04Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay04Count", integer);
        else
            setFieldValue("PAY04CNT", integer);
    }

    public BigDecimal getPay05Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay05Amount");
        else
            return (BigDecimal) getFieldValue("PAY05AMT");
    }

    public void setPay05Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay05Amount", bigdecimal);
        else
            setFieldValue("PAY05AMT", bigdecimal);
    }

    public Integer getPay05Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay05Count");
        else
            return (Integer) getFieldValue("PAY05CNT");
    }

    public void setPay05Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay05Count", integer);
        else
            setFieldValue("PAY05CNT", integer);
    }

    public BigDecimal getPay06Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay06Amount");
        else
            return (BigDecimal) getFieldValue("PAY06AMT");
    }

    public void setPay06Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay06Amount", bigdecimal);
        else
            setFieldValue("PAY06AMT", bigdecimal);
    }

    public Integer getPay06Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay06Count");
        else
            return (Integer) getFieldValue("PAY06CNT");
    }

    public void setPay06Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay06Count", integer);
        else
            setFieldValue("PAY06CNT", integer);
    }

    public BigDecimal getPay07Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay07Amount");
        else
            return (BigDecimal) getFieldValue("PAY07AMT");
    }

    public void setPay07Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay07Amount", bigdecimal);
        else
            setFieldValue("PAY07AMT", bigdecimal);
    }

    public Integer getPay07Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay07Count");
        else
            return (Integer) getFieldValue("PAY07CNT");
    }

    public void setPay07Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay07Count", integer);
        else
            setFieldValue("PAY07CNT", integer);
    }

    public BigDecimal getPay08Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay08Amount");
        else
            return (BigDecimal) getFieldValue("PAY08AMT");
    }

    public void setPay08Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay08Amount", bigdecimal);
        else
            setFieldValue("PAY08AMT", bigdecimal);
    }

    public Integer getPay08Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay08Count");
        else
            return (Integer) getFieldValue("PAY08CNT");
    }

    public void setPay08Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay08Count", integer);
        else
            setFieldValue("PAY08CNT", integer);
    }

    public BigDecimal getPay09Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay09Amount");
        else
            return (BigDecimal) getFieldValue("PAY09AMT");
    }

    public void setPay09Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay09Amount", bigdecimal);
        else
            setFieldValue("PAY09AMT", bigdecimal);
    }

    public Integer getPay09Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay09Count");
        else
            return (Integer) getFieldValue("PAY09CNT");
    }

    public void setPay09Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay09Count", integer);
        else
            setFieldValue("PAY09CNT", integer);
    }

    public BigDecimal getPay10Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay10Amount");
        else
            return (BigDecimal) getFieldValue("PAY10AMT");
    }

    public void setPay10Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay10Amount", bigdecimal);
        else
            setFieldValue("PAY10AMT", bigdecimal);
    }

    public Integer getPay10Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay10Count");
        else
            return (Integer) getFieldValue("PAY10CNT");
    }

    public void setPay10Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay10Count", integer);
        else
            setFieldValue("PAY10CNT", integer);
    }

    public BigDecimal getPay11Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay11Amount");
        else
            return (BigDecimal) getFieldValue("PAY11AMT");
    }

    public void setPay11Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay11Amount", bigdecimal);
        else
            setFieldValue("PAY11AMT", bigdecimal);
    }

    public Integer getPay11Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay11Count");
        else
            return (Integer) getFieldValue("PAY11CNT");
    }

    public void setPay11Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay11Count", integer);
        else
            setFieldValue("PAY11CNT", integer);
    }

    public BigDecimal getPay12Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay12Amount");
        else
            return (BigDecimal) getFieldValue("PAY12AMT");
    }

    public void setPay12Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay12Amount", bigdecimal);
        else
            setFieldValue("PAY12AMT", bigdecimal);
    }

    public Integer getPay12Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay12Count");
        else
            return (Integer) getFieldValue("PAY12CNT");
    }

    public void setPay12Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay12Count", integer);
        else
            setFieldValue("PAY12CNT", integer);
    }

    public BigDecimal getPay13Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay13Amount");
        else
            return (BigDecimal) getFieldValue("PAY13AMT");
    }

    public void setPay13Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay13Amount", bigdecimal);
        else
            setFieldValue("PAY13AMT", bigdecimal);
    }

    public Integer getPay13Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay13Count");
        else
            return (Integer) getFieldValue("PAY13CNT");
    }

    public void setPay13Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay13Count", integer);
        else
            setFieldValue("PAY13CNT", integer);
    }

    public BigDecimal getPay14Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay14Amount");
        else
            return (BigDecimal) getFieldValue("PAY14AMT");
    }

    public void setPay14Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay14Amount", bigdecimal);
        else
            setFieldValue("PAY14AMT", bigdecimal);
    }

    public Integer getPay14Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay14Count");
        else
            return (Integer) getFieldValue("PAY14CNT");
    }

    public void setPay14Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay14Count", integer);
        else
            setFieldValue("PAY14CNT", integer);
    }

    public BigDecimal getPay15Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay15Amount");
        else
            return (BigDecimal) getFieldValue("PAY15AMT");
    }

    public void setPay15Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay15Amount", bigdecimal);
        else
            setFieldValue("PAY15AMT", bigdecimal);
    }

    public Integer getPay15Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay15Count");
        else
            return (Integer) getFieldValue("PAY15CNT");
    }

    public void setPay15Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay15Count", integer);
        else
            setFieldValue("PAY15CNT", integer);
    }

    public BigDecimal getPay16Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay16Amount");
        else
            return (BigDecimal) getFieldValue("PAY16AMT");
    }

    public void setPay16Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay16Amount", bigdecimal);
        else
            setFieldValue("PAY16AMT", bigdecimal);
    }

    public Integer getPay16Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay16Count");
        else
            return (Integer) getFieldValue("PAY16CNT");
    }

    public void setPay16Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay16Count", integer);
        else
            setFieldValue("PAY16CNT", integer);
    }

    public BigDecimal getPay17Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay17Amount");
        else
            return (BigDecimal) getFieldValue("PAY17AMT");
    }

    public void setPay17Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay17Amount", bigdecimal);
        else
            setFieldValue("PAY17AMT", bigdecimal);
    }

    public Integer getPay17Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay17Count");
        else
            return (Integer) getFieldValue("PAY17CNT");
    }

    public void setPay17Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay17Count", integer);
        else
            setFieldValue("PAY17CNT", integer);
    }

    public BigDecimal getPay18Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay18Amount");
        else
            return (BigDecimal) getFieldValue("PAY18AMT");
    }

    public void setPay18Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay18Amount", bigdecimal);
        else
            setFieldValue("PAY18AMT", bigdecimal);
    }

    public Integer getPay18Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay18Count");
        else
            return (Integer) getFieldValue("PAY18CNT");
    }

    public void setPay18Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay18Count", integer);
        else
            setFieldValue("PAY18CNT", integer);
    }

    public BigDecimal getPay19Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay19Amount");
        else
            return (BigDecimal) getFieldValue("PAY19AMT");
    }

    public void setPay19Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay19Amount", bigdecimal);
        else
            setFieldValue("PAY19AMT", bigdecimal);
    }

    public Integer getPay19Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay19Count");
        else
            return (Integer) getFieldValue("PAY19CNT");
    }

    public void setPay19Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay19Count", integer);
        else
            setFieldValue("PAY19CNT", integer);
    }

    public BigDecimal getPay20Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay20Amount");
        else
            return (BigDecimal) getFieldValue("PAY20AMT");
    }

    public void setPay20Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay20Amount", bigdecimal);
        else
            setFieldValue("PAY20AMT", bigdecimal);
    }

    public Integer getPay20Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay20Count");
        else
            return (Integer) getFieldValue("PAY20CNT");
    }

    public void setPay20Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay20Count", integer);
        else
            setFieldValue("PAY20CNT", integer);
    }

    public BigDecimal getPay21Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay21Amount");
        else
            return (BigDecimal) getFieldValue("PAY21AMT");
    }

    public void setPay21Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay21Amount", bigdecimal);
        else
            setFieldValue("PAY21AMT", bigdecimal);
    }

    public Integer getPay21Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay21Count");
        else
            return (Integer) getFieldValue("PAY21CNT");
    }

    public void setPay21Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay21Count", integer);
        else
            setFieldValue("PAY21CNT", integer);
    }

    public BigDecimal getPay22Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay22Amount");
        else
            return (BigDecimal) getFieldValue("PAY22AMT");
    }

    public void setPay22Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay22Amount", bigdecimal);
        else
            setFieldValue("PAY22AMT", bigdecimal);
    }

    public Integer getPay22Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay22Count");
        else
            return (Integer) getFieldValue("PAY22CNT");
    }

    public void setPay22Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay22Count", integer);
        else
            setFieldValue("PAY22CNT", integer);
    }

    public BigDecimal getPay23Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay23Amount");
        else
            return (BigDecimal) getFieldValue("PAY23AMT");
    }

    public void setPay23Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay23Amount", bigdecimal);
        else
            setFieldValue("PAY23AMT", bigdecimal);
    }

    public Integer getPay23Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay23Count");
        else
            return (Integer) getFieldValue("PAY23CNT");
    }

    public void setPay23Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay23Count", integer);
        else
            setFieldValue("PAY23CNT", integer);
    }

    public BigDecimal getPay24Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay24Amount");
        else
            return (BigDecimal) getFieldValue("PAY24AMT");
    }

    public void setPay24Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay24Amount", bigdecimal);
        else
            setFieldValue("PAY24AMT", bigdecimal);
    }

    public Integer getPay24Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay24Count");
        else
            return (Integer) getFieldValue("PAY24CNT");
    }

    public void setPay24Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay24Count", integer);
        else
            setFieldValue("PAY24CNT", integer);
    }

    public BigDecimal getPay25Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay25Amount");
        else
            return (BigDecimal) getFieldValue("PAY25AMT");
    }

    public void setPay25Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay25Amount", bigdecimal);
        else
            setFieldValue("PAY25AMT", bigdecimal);
    }

    public Integer getPay25Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay25Count");
        else
            return (Integer) getFieldValue("PAY25CNT");
    }

    public void setPay25Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay25Count", integer);
        else
            setFieldValue("PAY25CNT", integer);
    }

    public BigDecimal getPay26Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay26Amount");
        else
            return (BigDecimal) getFieldValue("PAY26AMT");
    }

    public void setPay26Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay26Amount", bigdecimal);
        else
            setFieldValue("PAY26AMT", bigdecimal);
    }

    public Integer getPay26Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay26Count");
        else
            return (Integer) getFieldValue("PAY26CNT");
    }

    public void setPay26Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay26Count", integer);
        else
            setFieldValue("PAY26CNT", integer);
    }

    public BigDecimal getPay27Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay27Amount");
        else
            return (BigDecimal) getFieldValue("PAY27AMT");
    }

    public void setPay27Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay27Amount", bigdecimal);
        else
            setFieldValue("PAY27AMT", bigdecimal);
    }

    public Integer getPay27Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay27Count");
        else
            return (Integer) getFieldValue("PAY27CNT");
    }

    public void setPay27Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay27Count", integer);
        else
            setFieldValue("PAY27CNT", integer);
    }

    public BigDecimal getPay28Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay28Amount");
        else
            return (BigDecimal) getFieldValue("PAY28AMT");
    }

    public void setPay28Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay28Amount", bigdecimal);
        else
            setFieldValue("PAY28AMT", bigdecimal);
    }

    public Integer getPay28Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay28Count");
        else
            return (Integer) getFieldValue("PAY28CNT");
    }

    public void setPay28Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay28Count", integer);
        else
            setFieldValue("PAY28CNT", integer);
    }

    public BigDecimal getPay29Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay29Amount");
        else
            return (BigDecimal) getFieldValue("PAY29AMT");
    }

    public void setPay29Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay29Amount", bigdecimal);
        else
            setFieldValue("PAY29AMT", bigdecimal);
    }

    public Integer getPay29Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay29Count");
        else
            return (Integer) getFieldValue("PAY29CNT");
    }

    public void setPay29Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay29Count", integer);
        else
            setFieldValue("PAY29CNT", integer);
    }

    public BigDecimal getPay30Amount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("pay30Amount");
        else
            return (BigDecimal) getFieldValue("PAY30AMT");
    }

    public void setPay30Amount(BigDecimal bigdecimal) {
        if (Server.serverExist())
            setFieldValue("pay30Amount", bigdecimal);
        else
            setFieldValue("PAY30AMT", bigdecimal);
    }

    public Integer getPay30Count() {
        if (Server.serverExist())
            return (Integer) getFieldValue("pay30Count");
        else
            return (Integer) getFieldValue("PAY30CNT");
    }

    public void setPay30Count(Integer integer) {
        if (Server.serverExist())
            setFieldValue("pay30Count", integer);
        else
            setFieldValue("PAY30CNT", integer);
    }
    //==============gllg=======================
    public BigDecimal getItemDiscAmount() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT");
    }

    public void setItemDiscAmount(BigDecimal itemDiscAMT) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount", itemDiscAMT);
        else
            setFieldValue("ITEMDISCAMT", itemDiscAMT);
    }

    public BigDecimal getItemDiscAmount0() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount0");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT0");
    }

    public void setItemDiscAmount0(BigDecimal itemDiscAMT0) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount0", itemDiscAMT0);
        else
            setFieldValue("ITEMDISCAMT0", itemDiscAMT0);
    }
    
    public BigDecimal getItemDiscAmount1() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount1");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT1");
    }

    public void setItemDiscAmount1(BigDecimal itemDiscAMT1) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount1", itemDiscAMT1);
        else
            setFieldValue("ITEMDISCAMT1", itemDiscAMT1);
    }
    
    public BigDecimal getItemDiscAmount2() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount2");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT2");
    }

    public void setItemDiscAmount2(BigDecimal itemDiscAMT2) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount2", itemDiscAMT2);
        else
            setFieldValue("ITEMDISCAMT2", itemDiscAMT2);
    }
    
    public BigDecimal getItemDiscAmount3() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount3");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT3");
    }

    public void setItemDiscAmount3(BigDecimal itemDiscAMT3) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount3", itemDiscAMT3);
        else
            setFieldValue("ITEMDISCAMT3", itemDiscAMT3);
    }
    
    public BigDecimal getItemDiscAmount4() {
        if (Server.serverExist())
            return (BigDecimal) getFieldValue("itemDiscAmount4");
        else
            return (BigDecimal) getFieldValue("ITEMDISCAMT4");
    }

    public void setItemDiscAmount4(BigDecimal itemDiscAMT4) {
        if (Server.serverExist())
            setFieldValue("itemDiscAmount4", itemDiscAMT4);
        else
            setFieldValue("ITEMDISCAMT4", itemDiscAMT4);
    }
    
    public Integer getItemDiscCnt() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount");
        else
            return (Integer) getFieldValue("ITEMDISCCNT");
    }
    
    public void setItemDiscCnt(int itemDiscCnt) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT", new Integer(itemDiscCnt));
        else
            setFieldValue("itemDiscCount", new Integer(itemDiscCnt));
    }

    public Integer getItemDiscCnt0() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount0");
        else
            return (Integer) getFieldValue("ITEMDISCCNT0");
    }
    
    public void setItemDiscCnt0(int itemDiscCnt0) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT0", new Integer(itemDiscCnt0));
        else
            setFieldValue("itemDiscCount0", new Integer(itemDiscCnt0));
    }
    
    public Integer getItemDiscCnt1() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount1");
        else
            return (Integer) getFieldValue("ITEMDISCCNT1");
    }
    
    public void setItemDiscCnt1(int itemDiscCnt1) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT1", new Integer(itemDiscCnt1));
        else
            setFieldValue("itemDiscCount1", new Integer(itemDiscCnt1));
    }
    
    public Integer getItemDiscCnt2() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount2");
        else
            return (Integer) getFieldValue("ITEMDISCCNT2");
    }
    
    public void setItemDiscCnt2(int itemDiscCnt2) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT2", new Integer(itemDiscCnt2));
        else
            setFieldValue("itemDiscCount2", new Integer(itemDiscCnt2));
    }
    
    public Integer getItemDiscCnt3() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount3");
        else
            return (Integer) getFieldValue("ITEMDISCCNT3");
    }
    
    public void setItemDiscCnt3(int itemDiscCnt3) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT3", new Integer(itemDiscCnt3));
        else
            setFieldValue("itemDiscCount3", new Integer(itemDiscCnt3));
    }
    
    public Integer getItemDiscCnt4() {
        if (Server.serverExist())
            return (Integer) getFieldValue("itemDiscCount4");
        else
            return (Integer) getFieldValue("ITEMDISCCNT4");
    }
    
    public void setItemDiscCnt4(int itemDiscCnt4) {
        if (!Server.serverExist())
            setFieldValue("ITEMDISCCNT4", new Integer(itemDiscCnt4));
        else
            setFieldValue("itemDiscCount4", new Integer(itemDiscCnt4));
    }

    //==============gllg=======================

    public BigDecimal getPayAmountByID(String id) {
        String innerID = null;
        if (id.length() == 1 && Integer.parseInt(id) < 10)
            innerID = "0" + id;
        else
            innerID = id;
        // return (BigDecimal)getFieldValue("PAY" + innerID + "AMT");
        try {
            return (BigDecimal) getClass().getDeclaredMethod(
                    "getPay" + innerID + "Amount", new Class[0]).invoke(this,
                    new Object[0]);
        } catch (Exception e) {
            return new BigDecimal(99999999D);
        }
    }

    public void setPayAmountByID(String s, BigDecimal bigdecimal) {
        String s1 = null;
        if (s.length() == 1 && Integer.parseInt(s) < 10)
            s1 = "0" + s;
        else
            s1 = s;
        try {
            getClass().getDeclaredMethod("setPay" + s1 + "Amount",
                    new Class[] { java.math.BigDecimal.class }).invoke(this,
                    new Object[] { bigdecimal });
        } catch (Exception exception) {
        }
    }

    public Integer getPayCountByID(String id) {
        String innerID = null;
        if (id.length() == 1 && Integer.parseInt(id) < 10)
            innerID = "0" + id;
        else
            innerID = id;
        // return (Integer)getFieldValue("PAY" + innerID + "CNT");
        try {
            return (Integer) getClass().getDeclaredMethod(
                    "getPay" + innerID + "Count", new Class[0]).invoke(this,
                    new Object[0]);
        } catch (Exception e) {
            return new Integer(0);
        }
    }

    public void setPayCountByID(String s, Integer integer) {
        String s1 = null;
        if (s.length() == 1 && Integer.parseInt(s) < 10)
            s1 = "0" + s;
        else
            s1 = s;
        try {
            getClass().getDeclaredMethod("setPay" + s1 + "Count",
                    new Class[] { java.lang.Integer.class }).invoke(this,
                    new Object[] { integer });
        } catch (Exception exception) {
        }
    }

    public BigDecimal getTotalAmount() {
        return getNetSalesTotalAmount().add(getDaiShouAmount()).add(
                getDaiShouAmount2()).add(getDaiFuAmount().negate()).add(
                getSpillAmount().add(
                        getPaidInAmount().add(getPaidOutAmount().negate())));
    }

    public BigDecimal getTotalPayAmount() {
        BigDecimal bigdecimal = new BigDecimal(0.0D);
        for (int i = 0; i < 20; i++) {
            String s = i + "";
            if (i < 10)
                s = "0" + i;
            try {
                BigDecimal bigdecimal1 = (BigDecimal) getClass()
                        .getDeclaredMethod("getPay" + s + "Amount",
                                new Class[0]).invoke(this, new Object[0]);
                if (bigdecimal1 != null)
                    bigdecimal = bigdecimal.add(bigdecimal1);
            } catch (Exception exception) {
            }
        }

        return bigdecimal;
    }

    public BigDecimal getGrossSales() {
        BigDecimal bigdecimal = new BigDecimal(0.0D);
        for (int i = 0; i < 5; i++)
            try {
                BigDecimal bigdecimal1 = (BigDecimal) getClass()
                        .getDeclaredMethod("getTaxType" + i + "GrossSales",
                                new Class[0]).invoke(this, new Object[0]);
                if (bigdecimal1 != null)
                    bigdecimal = bigdecimal.add(bigdecimal1);
            } catch (Exception exception) {
            }

        return bigdecimal;
    }

    public Integer getPeiDaCount() {
        Map map = DacBase
                .getValueOfStatement("SELECT COUNT(*) FROM tranhead WHERE EODCNT="
                        + getSequenceNumber()
                        + " AND VOIDSEQ IS NULL AND DEALTYPE1<>'*' AND ANNOTATEDTYPE='P'");
        if (map.isEmpty())
            return new Integer(0);
        Iterator iterator = map.keySet().iterator();
        Object obj = map.get(iterator.next());
        int i = 0;
        if (obj instanceof Integer)
            i = ((Integer) obj).intValue();
        else if (obj instanceof Long)
            i = ((Long) obj).intValue();
        else if (obj instanceof Short)
            i = ((Short) obj).intValue();
        return new Integer(i);
    }

    public String getInsertUpdateTableName() {
        if (Server.serverExist())
            return "posul_z";
        else
            return "z";
    }

    public static String getInsertUpdateTableNameStaticVersion() {
        if (Server.serverExist())
            return "posul_z";
        else
            return "z";
    }

    private static String[][] posToScFieldNameArray;

    public static String[][] getPosToScFieldNameArray() {
        if (posToScFieldNameArray == null)
            posToScFieldNameArray = new String[][] {
                new String[] { "STORENO", "storeID" },
                new String[] { "POSNO", "posNumber" },
                new String[] { "EODCNT", "zSequenceNumber" },
                new String[] { "ACCDATE", "accountDate" },
                new String[] { "ZBEGDTTM", "zBeginDateTime" },
                new String[] { "ZENDDTTM", "zEndDateTime" },
                new String[] { "SGNONSEQ", "zBeginTransactionNumber" },
                new String[] { "SGNOFFSEQ", "zEndTransactionNumber" },
                new String[] { "SGNONINV", "zBeginInvoiceNumber" },
                new String[] { "SGNOFFINV", "zEndInvoiceNumber" },
                new String[] { "MACHINENO", "machineNumber" },
                new String[] { "CASHIER", "cashierID" },
                new String[] { "TCPFLG", "uploadState" },
                new String[] { "TRANCNT", "transactionCount" },
                new String[] { "CUSTCNT", "customerCount" },
                new String[] { "SALECNT", "itemSaleQuantity" },
                new String[] { "GrossSales", "grossSaleTotalAmount" },
                new String[] { "SALAMTTAX0", "grossSaleTax0Amount" },
                new String[] { "SALAMTTAX1", "grossSaleTax1Amount" },
                new String[] { "SALAMTTAX2", "grossSaleTax2Amount" },
                new String[] { "SALAMTTAX3", "grossSaleTax3Amount" },
                new String[] { "SALAMTTAX4", "grossSaleTax4Amount" },
                new String[] { "SIPercentPlusTotalAmount", "siPlusTotalAmount" },
                new String[] { "SIPLUSAMT0", "siPlusTax0Amount" },
                new String[] { "SIPLUSAMT1", "siPlusTax1Amount" },
                new String[] { "SIPLUSAMT2", "siPlusTax2Amount" },
                new String[] { "SIPLUSAMT3", "siPlusTax3Amount" },
                new String[] { "SIPLUSAMT4", "siPlusTax4Amount" },
                new String[] { "SIPercentPlusTotalCount", "siPlusTotalCount" },
                new String[] { "SIPLUSCNT0", "siPlusTax0Count" },
                new String[] { "SIPLUSCNT1", "siPlusTax1Count" },
                new String[] { "SIPLUSCNT2", "siPlusTax2Count" },
                new String[] { "SIPLUSCNT3", "siPlusTax3Count" },
                new String[] { "SIPLUSCNT4", "siPlusTax4Count" },
                new String[] { "SIPercentOffTotalAmount", "discountTotalAmount" },
                new String[] { "SIPAMT0", "discountTax0Amount" },
                new String[] { "SIPAMT1", "discountTax1Amount" },
                new String[] { "SIPAMT2", "discountTax2Amount" },
                new String[] { "SIPAMT3", "discountTax3Amount" },
                new String[] { "SIPAMT4", "discountTax4Amount" },
                new String[] { "SIPercentOffTotalCount", "discountTotalCount" },
                new String[] { "SIPCNT0", "discountTax0Count" },
                new String[] { "SIPCNT1", "discountTax1Count" },
                new String[] { "SIPCNT2", "discountTax2Count" },
                new String[] { "SIPCNT3", "discountTax3Count" },
                new String[] { "SIPCNT4", "discountTax4Count" },
                new String[] { "MixAndMatchTotalAmount",
                        "mixAndMatchTotalAmount" },
                new String[] { "MNMAMT0", "mixAndMatchTax0Amount" },
                new String[] { "MNMAMT1", "mixAndMatchTax1Amount" },
                new String[] { "MNMAMT2", "mixAndMatchTax2Amount" },
                new String[] { "MNMAMT3", "mixAndMatchTax3Amount" },
                new String[] { "MNMAMT4", "mixAndMatchTax4Amount" },
                new String[] { "MixAndMatchTotalCount", "mixAndMatchTotalCount" },
                new String[] { "MNMCNT0", "mixAndMatchTax0Count" },
                new String[] { "MNMCNT1", "mixAndMatchTax1Count" },
                new String[] { "MNMCNT2", "mixAndMatchTax2Count" },
                new String[] { "MNMCNT3", "mixAndMatchTax3Count" },
                new String[] { "MNMCNT4", "mixAndMatchTax4Count" },
                new String[] { "NotIncludedTotalSales", "notIncludedTotalSale" },
                new String[] { "RCPGIFAMT0", "notIncludedTax0Sale" },
                new String[] { "RCPGIFAMT1", "notIncludedTax1Sale" },
                new String[] { "RCPGIFAMT2", "notIncludedTax2Sale" },
                new String[] { "RCPGIFAMT3", "notIncludedTax3Sale" },
                new String[] { "RCPGIFAMT4", "notIncludedTax4Sale" },
                new String[] { "NetSalesTotalAmount", "netSaleTotalAmount" },
                new String[] { "NETSALAMT0", "netSaleTax0Amount" },
                new String[] { "NETSALAMT1", "netSaleTax1Amount" },
                new String[] { "NETSALAMT2", "netSaleTax2Amount" },
                new String[] { "NETSALAMT3", "netSaleTax3Amount" },
                new String[] { "NETSALAMT4", "netSaleTax4Amount" },
                new String[] { "TAXAMT0", "taxAmount0" },
                new String[] { "TAXAMT1", "taxAmount1" },
                new String[] { "TAXAMT2", "taxAmount2" },
                new String[] { "TAXAMT3", "taxAmount3" },
                new String[] { "TAXAMT4", "taxAmount4" },
                new String[] { "TotalTaxAmount", "taxTotalAmount" },
                new String[] { "VALPAMT", "totalVoucherAmount" },
                new String[] { "VALPCNT", "totalVoucherCount" },
                new String[] { "PRCVAMT", "preRcvAmount" },
                new String[] { "PRCVCNT", "preRcvCount" },
                new String[] { "PRCVPLUAMT1", "preRcvDetailAmount1" },
                new String[] { "PRCVPLUCNT1", "preRcvDetailCount1" },
                new String[] { "PRCVPLUAMT2", "preRcvDetailAmount2" },
                new String[] { "PRCVPLUCNT2", "preRcvDetailCount2" },
                new String[] { "PRCVPLUAMT3", "preRcvDetailAmount3" },
                new String[] { "PRCVPLUCNT3", "preRcvDetailCount3" },
                new String[] { "PRCVPLUAMT4", "preRcvDetailAmount4" },
                new String[] { "PRCVPLUCNT4", "preRcvDetailCount4" },
                new String[] { "PRCVPLUAMT5", "preRcvDetailAmount5" },
                new String[] { "PRCVPLUCNT5", "preRcvDetailCount5" },
                new String[] { "PRCVPLUAMT6", "preRcvDetailAmount6" },
                new String[] { "PRCVPLUCNT6", "preRcvDetailCount6" },
                new String[] { "PRCVPLUAMT7", "preRcvDetailAmount7" },
                new String[] { "PRCVPLUCNT7", "preRcvDetailCount7" },
                new String[] { "PRCVPLUAMT8", "preRcvDetailAmount8" },
                new String[] { "PRCVPLUCNT8", "preRcvDetailCount8" },
                new String[] { "PRCVPLUAMT9", "preRcvDetailAmount9" },
                new String[] { "PRCVPLUCNT9", "preRcvDetailCount9" },
                new String[] { "PRCVPLUAMT10", "preRcvDetailAmount10" },
                new String[] { "PRCVPLUCNT10", "preRcvDetailCount10" },
                new String[] { "DAISHOUAMT", "totalDaiShouAmount" },
                new String[] { "DAISHOUCNT", "totalDaiShouCount" },
                new String[] { "DAISHOUPLUAMT1", "daiShouDetailAmount1" },
                new String[] { "DAISHOUPLUCNT1", "daiShouDetailCount1" },
                new String[] { "DAISHOUPLUAMT2", "daiShouDetailAmount2" },
                new String[] { "DAISHOUPLUCNT2", "daiShouDetailCount2" },
                new String[] { "DAISHOUPLUAMT3", "daiShouDetailAmount3" },
                new String[] { "DAISHOUPLUCNT3", "daiShouDetailCount3" },
                new String[] { "DAISHOUPLUAMT4", "daiShouDetailAmount4" },
                new String[] { "DAISHOUPLUCNT4", "daiShouDetailCount4" },
                new String[] { "DAISHOUPLUAMT5", "daiShouDetailAmount5" },
                new String[] { "DAISHOUPLUCNT5", "daiShouDetailCount5" },
                new String[] { "DAISHOUPLUAMT6", "daiShouDetailAmount6" },
                new String[] { "DAISHOUPLUCNT6", "daiShouDetailCount6" },
                new String[] { "DAISHOUPLUAMT7", "daiShouDetailAmount7" },
                new String[] { "DAISHOUPLUCNT7", "daiShouDetailCount7" },
                new String[] { "DAISHOUPLUAMT8", "daiShouDetailAmount8" },
                new String[] { "DAISHOUPLUCNT8", "daiShouDetailCount8" },
                new String[] { "DAISHOUPLUAMT9", "daiShouDetailAmount9" },
                new String[] { "DAISHOUPLUCNT9", "daiShouDetailCount9" },
                new String[] { "DAISHOUPLUAMT10", "daiShouDetailAmount10" },
                new String[] { "DAISHOUPLUCNT10", "daiShouDetailCount10" },
                new String[] { "DAISHOUAMT2", "TotalDaiShouAmount2" },
                new String[] { "DAISHOUCNT2", "TotalDaiShouCount2" },
                new String[] { "DAIFUAMT", "totalDaiFuAmount" },
                new String[] { "DAIFUCNT", "totalDaiFuCount" },
                new String[] { "DAIFUPLUAMT1", "daiFuDetailAmount1" },
                new String[] { "DAIFUPLUCNT1", "daiFuDetailCount1" },
                new String[] { "DAIFUPLUAMT2", "daiFuDetailAmount2" },
                new String[] { "DAIFUPLUCNT2", "daiFuDetailCount2" },
                new String[] { "DAIFUPLUAMT3", "daiFuDetailAmount3" },
                new String[] { "DAIFUPLUCNT3", "daiFuDetailCount3" },
                new String[] { "DAIFUPLUAMT4", "daiFuDetailAmount4" },
                new String[] { "DAIFUPLUCNT4", "daiFuDetailCount4" },
                new String[] { "DAIFUPLUAMT5", "daiFuDetailAmount5" },
                new String[] { "DAIFUPLUCNT5", "daiFuDetailCount5" },
                new String[] { "DAIFUPLUAMT6", "daiFuDetailAmount6" },
                new String[] { "DAIFUPLUCNT6", "daiFuDetailCount6" },
                new String[] { "DAIFUPLUAMT7", "daiFuDetailAmount7" },
                new String[] { "DAIFUPLUCNT7", "daiFuDetailCount7" },
                new String[] { "DAIFUPLUAMT8", "daiFuDetailAmount8" },
                new String[] { "DAIFUPLUCNT8", "daiFuDetailCount8" },
                new String[] { "DAIFUPLUAMT9", "daiFuDetailAmount9" },
                new String[] { "DAIFUPLUCNT9", "daiFuDetailCount9" },
                new String[] { "DAIFUPLUAMT10", "daiFuDetailAmount10" },
                new String[] { "DAIFUPLUCNT10", "daiFuDetailCount10" },
                new String[] { "PINAMT", "totalPaidinAmount" },
                new String[] { "PINCNT", "totalPaidinCount" },
                new String[] { "PINPLUAMT1", "paidInDetailAmount1" },
                new String[] { "PINPLUCNT1", "paidInDetailCount1" },
                new String[] { "PINPLUAMT2", "paidInDetailAmount2" },
                new String[] { "PINPLUCNT2", "paidInDetailCount2" },
                new String[] { "PINPLUAMT3", "paidInDetailAmount3" },
                new String[] { "PINPLUCNT3", "paidInDetailCount3" },
                new String[] { "PINPLUAMT4", "paidInDetailAmount4" },
                new String[] { "PINPLUCNT4", "paidInDetailCount4" },
                new String[] { "PINPLUAMT5", "paidInDetailAmount5" },
                new String[] { "PINPLUCNT5", "paidInDetailCount5" },
                new String[] { "PINPLUAMT6", "paidInDetailAmount6" },
                new String[] { "PINPLUCNT6", "paidInDetailCount6" },
                new String[] { "PINPLUAMT7", "paidInDetailAmount7" },
                new String[] { "PINPLUCNT7", "paidInDetailCount7" },
                new String[] { "PINPLUAMT8", "paidInDetailAmount8" },
                new String[] { "PINPLUCNT8", "paidInDetailCount8" },
                new String[] { "PINPLUAMT9", "paidInDetailAmount9" },
                new String[] { "PINPLUCNT9", "paidInDetailCount9" },
                new String[] { "PINPLUAMT10", "paidInDetailAmount10" },
                new String[] { "PINPLUCNT10", "paidInDetailCount10" },
                new String[] { "POUTAMT", "totalPaidoutAmount" },
                new String[] { "POUTCNT", "totalPaidoutCount" },
                new String[] { "POUTPLUAMT1", "paidOutDetailAmount1" },
                new String[] { "POUTPLUCNT1", "paidOutDetailCount1" },
                new String[] { "POUTPLUAMT2", "paidOutDetailAmount2" },
                new String[] { "POUTPLUCNT2", "paidOutDetailCount2" },
                new String[] { "POUTPLUAMT3", "paidOutDetailAmount3" },
                new String[] { "POUTPLUCNT3", "paidOutDetailCount3" },
                new String[] { "POUTPLUAMT4", "paidOutDetailAmount4" },
                new String[] { "POUTPLUCNT4", "paidOutDetailCount4" },
                new String[] { "POUTPLUAMT5", "paidOutDetailAmount5" },
                new String[] { "POUTPLUCNT5", "paidOutDetailCount5" },
                new String[] { "POUTPLUAMT6", "paidOutDetailAmount6" },
                new String[] { "POUTPLUCNT6", "paidOutDetailCount6" },
                new String[] { "POUTPLUAMT7", "paidOutDetailAmount7" },
                new String[] { "POUTPLUCNT7", "paidOutDetailCount7" },
                new String[] { "POUTPLUAMT8", "paidOutDetailAmount8" },
                new String[] { "POUTPLUCNT8", "paidOutDetailCount8" },
                new String[] { "POUTPLUAMT9", "paidOutDetailAmount9" },
                new String[] { "POUTPLUCNT9", "paidOutDetailCount9" },
                new String[] { "POUTPLUAMT10", "paidOutDetailAmount10" },
                new String[] { "POUTPLUCNT10", "paidOutDetailCount10" },
                new String[] { "EXGAMT", "exchangeItemAmount" },
                new String[] { "EXGITEMQTY", "exchangeItemQuantity" },
                new String[] { "EXGTRANCNT", "exchangeTransactionCount" },
                new String[] { "RTNAMT", "returnItemAmount" },
                new String[] { "RTNITEMQTY", "returnItemQuantity" },
                new String[] { "RTNTRANCNT", "returnTransactionCount" },
                new String[] { "VOIDINVAMT", "transactionVoidAmount" },
                new String[] { "VOIDINVCNT", "transactionVoidCount" },
                new String[] { "CANCELAMT", "transactionCancelAmount" },
                new String[] { "CANCELCNT", "transactionCancelCount" },
                new String[] { "UPDAMT", "lineVoidAmount" },
                new String[] { "UPDCNT", "lineVoidCount" },
                new String[] { "NOWUPDAMT", "voidAmount" },
                new String[] { "NOWUPDCNT", "voidCount" },
                new String[] { "DROPENCNT", "drawerOpenCount" },
                new String[] { "RTNCSHCNT", "cashReturnCount" },
                new String[] { "RTNCSHAMT", "cashReturnAmount" },
                new String[] { "ENTRYAMT", "manualEntryAmount" },
                new String[] { "ENTRYCNT", "manualEntryCount" },
                new String[] { "POENTRYAMT", "priceOpenEntryAmount" },
                new String[] { "POENTRYCNT", "priceOpenEntryCount" },
                new String[] { "REPRTAMT", "reprintAmount" },
                new String[] { "REPRTCNT", "reprintCount" },
                new String[] { "CASHINAMT", "cashInAmount" },
                new String[] { "CASHINCNT", "cashInCount" },
                new String[] { "CASHOUTAMT", "cashOutAmount" },
                new String[] { "CASHOUTCNT", "cashOutCount" },
                new String[] { "OVERAMT", "spillAmount" },
                new String[] { "OVERCNT", "spillCount" },
                new String[] { "RATE1", "exchangeRate1" },
                new String[] { "RATE2", "exchangeRate2" },
                new String[] { "RATE3", "exchangeRate3" },
                new String[] { "RATE4", "exchangeRate4" },
                new String[] { "PAY00AMT", "pay00Amount" },
                new String[] { "PAY00CNT", "pay00Count" },
                new String[] { "PAY01AMT", "pay01Amount" },
                new String[] { "PAY01CNT", "pay01Count" },
                new String[] { "PAY02AMT", "pay02Amount" },
                new String[] { "PAY02CNT", "pay02Count" },
                new String[] { "PAY03AMT", "pay03Amount" },
                new String[] { "PAY03CNT", "pay03Count" },
                new String[] { "PAY04AMT", "pay04Amount" },
                new String[] { "PAY04CNT", "pay04Count" },
                new String[] { "PAY05AMT", "pay05Amount" },
                new String[] { "PAY05CNT", "pay05Count" },
                new String[] { "PAY06AMT", "pay06Amount" },
                new String[] { "PAY06CNT", "pay06Count" },
                new String[] { "PAY07AMT", "pay07Amount" },
                new String[] { "PAY07CNT", "pay07Count" },
                new String[] { "PAY08AMT", "pay08Amount" },
                new String[] { "PAY08CNT", "pay08Count" },
                new String[] { "PAY09AMT", "pay09Amount" },
                new String[] { "PAY09CNT", "pay09Count" },
                new String[] { "PAY10AMT", "pay10Amount" },
                new String[] { "PAY10CNT", "pay10Count" },
                new String[] { "PAY11AMT", "pay11Amount" },
                new String[] { "PAY11CNT", "pay11Count" },
                new String[] { "PAY12AMT", "pay12Amount" },
                new String[] { "PAY12CNT", "pay12Count" },
                new String[] { "PAY13AMT", "pay13Amount" },
                new String[] { "PAY13CNT", "pay13Count" },
                new String[] { "PAY14AMT", "pay14Amount" },
                new String[] { "PAY14CNT", "pay14Count" },
                new String[] { "PAY15AMT", "pay15Amount" },
                new String[] { "PAY15CNT", "pay15Count" },
                new String[] { "PAY16AMT", "pay16Amount" },
                new String[] { "PAY16CNT", "pay16Count" },
                new String[] { "PAY17AMT", "pay17Amount" },
                new String[] { "PAY17CNT", "pay17Count" },
                new String[] { "PAY18AMT", "pay18Amount" },
                new String[] { "PAY18CNT", "pay18Count" },
                new String[] { "PAY19AMT", "pay19Amount" },
                new String[] { "PAY19CNT", "pay19Count" },
                new String[] { "PAY20AMT", "pay20Amount" },
                new String[] { "PAY20CNT", "pay20Count" },
                new String[] { "PAY21AMT", "pay21Amount" },
                new String[] { "PAY21CNT", "pay21Count" },
                new String[] { "PAY22AMT", "pay22Amount" },
                new String[] { "PAY22CNT", "pay22Count" },
                new String[] { "PAY23AMT", "pay23Amount" },
                new String[] { "PAY23CNT", "pay23Count" },
                new String[] { "PAY24AMT", "pay24Amount" },
                new String[] { "PAY24CNT", "pay24Count" },
                new String[] { "PAY25AMT", "pay25Amount" },
                new String[] { "PAY25CNT", "pay25Count" },
                new String[] { "PAY26AMT", "pay26Amount" },
                new String[] { "PAY26CNT", "pay26Count" },
                new String[] { "PAY27AMT", "pay27Amount" },
                new String[] { "PAY27CNT", "pay27Count" },
                new String[] { "PAY28AMT", "pay28Amount" },
                new String[] { "PAY28CNT", "pay28Count" },
                new String[] { "PAY29AMT", "pay29Amount" },
                new String[] { "PAY29CNT", "pay29Count" },
                new String[] { "CHANGAMT", "exchangeDifference" },
                new String[] { "SI00AMT", "si00Amount" },
                new String[] { "SI00CNT", "si00Count" },
                new String[] { "SI01AMT", "si01Amount" },
                new String[] { "SI01CNT", "si01Count" },
                new String[] { "SI02AMT", "si02Amount" },
                new String[] { "SI02CNT", "si02Count" },
                new String[] { "SI03AMT", "si03Amount" },
                new String[] { "SI03CNT", "si03Count" },
                new String[] { "SI04AMT", "si04Amount" },
                new String[] { "SI04CNT", "si04Count" },
                new String[] { "SI05AMT", "si05Amount" },
                new String[] { "SI05CNT", "si05Count" },
                new String[] { "SI06AMT", "si06Amount" },
                new String[] { "SI06CNT", "si06Count" },
                new String[] { "SI07AMT", "si07Amount" },
                new String[] { "SI07CNT", "si07Count" },
                new String[] { "SI08AMT", "si08Amount" },
                new String[] { "SI08CNT", "si08Count" },
                new String[] { "SI09AMT", "si09Amount" },
                new String[] { "SI09CNT", "si09Count" },
                new String[] { "SI10AMT", "si10Amount" },
                new String[] { "SI10CNT", "si10Count" },
                new String[] { "SI11AMT", "si11Amount" },
                new String[] { "SI11CNT", "si11Count" },
                new String[] { "SI12AMT", "si12Amount" },
                new String[] { "SI12CNT", "si12Count" },
                new String[] { "SI13AMT", "si13Amount" },
                new String[] { "SI13CNT", "si13Count" },
                new String[] { "SI14AMT", "si14Amount" },
                new String[] { "SI14CNT", "si14Count" },
                new String[] { "SI15AMT", "si15Amount" },
                new String[] { "SI15CNT", "si15Count" },
                new String[] { "SI16AMT", "si16Amount" },
                new String[] { "SI16CNT", "si16Count" },
                new String[] { "SI17AMT", "si17Amount" },
                new String[] { "SI17CNT", "si17Count" },
                new String[] { "SI18AMT", "si18Amount" },
                new String[] { "SI18CNT", "si18Count" },
                new String[] { "SI19AMT", "si19Amount" },
                new String[] { "SI19CNT", "si19Count" } ,
                // gllg                
                new String[] { "ITEMDISCAMT", "itemDiscAmount" } ,
                new String[] { "ITEMDISCAMT0", "itemDiscAmount0" } ,
                new String[] { "ITEMDISCAMT1", "itemDiscAmount1" } ,
                new String[] { "ITEMDISCAMT2", "itemDiscAmount2" } ,
                new String[] { "ITEMDISCAMT3", "itemDiscAmount3" } ,
                new String[] { "ITEMDISCAMT4", "itemDiscAmount4" } ,
                new String[] { "ITEMDISCCNT", "itemDiscCount" } ,
                new String[] { "ITEMDISCCNT0", "itemDiscCount0" } ,
                new String[] { "ITEMDISCCNT1", "itemDiscCount1" } ,
                new String[] { "ITEMDISCCNT2", "itemDiscCount2" } ,
                new String[] { "ITEMDISCCNT3", "itemDiscCount3" } ,
                new String[] { "ITEMDISCCNT4", "itemDiscCount4" } 
        };
        return posToScFieldNameArray; 
    }

    /**
     * Clone a ZReport object for SC with converted field names.
     * 
     * This method is only used at POS side.
     */
    public ZReport cloneForSC() {
        String fieldNameMap[][] = getPosToScFieldNameArray();
        try {
            ZReport clonedZ = new ZReport(0);
            for (int i = 0; i < fieldNameMap.length; i++) {
                Object value = getFieldValue(fieldNameMap[i][0]);
                if (value == null) {
                    try {
                        value = getClass().getDeclaredMethod(
                                "get" + fieldNameMap[i][0], new Class[0])
                                .invoke(this, new Object[0]);
                    } catch (Exception e) {
                        value = null;
                    }
                }
                // 因为SC没有区分SI折扣与折让金额，所以将其相加
                if (fieldNameMap[i][0].startsWith("SIPAMT"))
                    ((BigDecimal) value)
                            .add((BigDecimal) getFieldValue("SIMAMT"
                                    + fieldNameMap[i][0]
                                            .substring(fieldNameMap[i][0]
                                                    .length() - 1)));
                if (fieldNameMap[i][0].startsWith("SIPCNT"))
                    // ((Integer)value).add((Integer)this.getFieldValue(
                    // "SIMCNT" +
                    // fieldNameMap[i][0].substring(fieldNameMap[i][0].length()
                    // - 1)));
                    value = new Integer(((Integer) value).intValue()
                            + ((Integer) getFieldValue("SIMCNT"
                                    + fieldNameMap[i][0]
                                            .substring(fieldNameMap[i][0]
                                                    .length() - 1))).intValue());
                clonedZ.setFieldValue(fieldNameMap[i][1], value);
            }

            return clonedZ;
        } catch (InstantiationException e) {
            return null;
        }
    }
}
