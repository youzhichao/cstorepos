package hyi.cream.event;


public class SystemInfoEvent extends java.util.EventObject {

    /**
     * Constructor.
     * @param source the object that originated the event
     */
    public SystemInfoEvent(Object source) {
        super(source);
    }
}
