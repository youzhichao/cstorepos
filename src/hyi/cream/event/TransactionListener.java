package hyi.cream.event;


public interface TransactionListener extends java.util.EventListener {

    /**
     * Invoked when transaction has been changed.
     * @param e an event object represents the changes.
     */
    public void transactionChanged(TransactionEvent e);
}

 