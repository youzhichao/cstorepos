package hyi.cream.inline;

import java.text.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.math.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;

/**
 * PostProcessor for C-Store.
 *
 * @author Bruce
 * @version 1.7
 */
class CStorePostProcessor extends PostProcessorAdapter {

    /**
     * /Bruce/1.8/2003-05-29/
     *    增加同步处理机制。以避免同时上传数据造成问题。
     *
     * /Bruce/1.7/2002-04-27/
     *    增加上传代收明细帐.
     *
     * /Bruce/1.6/2002-04-10/
     *    1. 修改现金日报中的来客数为Z帐中的来客数而非交易数.
     *    2. 增加 beforeDownloadingMaster()：在下传主档之前，判断是否现在正在进
     *       行营业换日，若是，或者换日失败，则return false，表示现在禁止做主档
     *       下传。
     *
     * /Bruce/1.5/2002-04-01/
     *    Make error handling more robust.
     */
    transient public static final String VERSION = "1.8";

    static final int DATE_ACCDATE         = 200;
    static final int DATE_BIZDATE         = DATE_ACCDATE + 1;
    static final int DATE_INFDATE         = DATE_ACCDATE + 2;
    static private DateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd");
    static private DateFormat yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
    static final String POS_AMOUNT = "posamount";
    static final String STORE_AMOUNT = "storeamount";
    static final Integer integer0 = new Integer(0);
    static Object lockObject = new Object();
    
//    public static void main(String[] args) {
//        Server.setFakeExist();
//        //generateAccdayrptRecords(new Date());
//        try {
//            System.out.println(
//                getAccountingDate(new Date()));
//                    //new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
//                    //    .parse("2003-07-06 17:00:00")).toString());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    static private Map zSeqToAccountingDateMap = new HashMap();

    /**
     * 根据交易时间决定营业日期。
     * 
     * @param date 交易时间
     * @return 返回营业日期
     */
     private Date getBusinessDate(Date date) {
        String changeTime = "22:59:59";
        Map value = query("SELECT changetime FROM calendar WHERE dateType = '0'");
        if (value != null && value.containsKey("changetime")) {
            changeTime = (String)value.get("changetime").toString();
            if (changeTime.trim().length() == 0 || changeTime.equals("00:00:00")) {
                changeTime = "22:59:59";        
            }
        }
        String realTime = new SimpleDateFormat("HH:mm:ss").format(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (realTime.compareTo(changeTime) > 0) {
            cal.add(Calendar.DATE, 1);
        }
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * 由Z帐序号决定会计日期。
     * 
     * @param posNumber POS机号
     * @param zSeq Z帐序号
     * @param transactionDate 交易时间
     * @return 返回会计日期
     */
    private Date getAccountingDate(int posNumber, int zSeq) {
        Integer key = new Integer(posNumber * 1000000 + zSeq);
        Date accountingDate = (Date)zSeqToAccountingDateMap.get(key);
        if (accountingDate != null)
            return accountingDate;

        Map value = query("SELECT accountdate FROM posul_z WHERE posNumber=" + posNumber 
            + " AND zSequenceNumber=" + zSeq);
        if (value != null && value.containsKey("accountdate")) {
            accountingDate = (Date)value.get("accountdate");
            zSeqToAccountingDateMap.put(key, accountingDate);
            return accountingDate;
        }
        return getAccountingDate(new Date());
    }

    /**
     * 由Z帐序号或交易时间决定会计日期。
     * 
     * @param posNumber POS机号
     * @param zSeq Z帐序号
     * @param transactionDate 交易时间
     * @return 返回会计日期
     */
    private Date getAccountingDate(int posNumber, int zSeq, Date transactionDate) {
        Integer key = new Integer(posNumber * 1000000 + zSeq);
        Date accountingDate = (Date)zSeqToAccountingDateMap.get(key);
        if (accountingDate != null)
            return accountingDate;

        Map value = query("SELECT accountdate FROM posul_z WHERE posNumber=" + posNumber 
            + " AND zSequenceNumber=" + zSeq);
        if (value != null && value.containsKey("accountdate")) {
            accountingDate = (Date)value.get("accountdate");
            zSeqToAccountingDateMap.put(key, accountingDate);
            return accountingDate;
        }
        return getAccountingDate(transactionDate);
    }

    private Date getAccountingDate(Date date) {
        String changeTime = "23:59:59";
        Map value = query("SELECT changetime FROM calendar WHERE dateType = '1'");
        if (value != null && value.containsKey("changetime")) {
            changeTime = (String)value.get("changetime").toString();
            if (changeTime.trim().length() == 0 || changeTime.equals("00:00:00")) {
                changeTime = "23:59:59";        
            }
        }
        String realTime = new SimpleDateFormat("HH:mm:ss").format(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        //Bruce/2003-12-01
        //有两种决定会计日期的models:
        //如果AccountingDateShiftTime="afternoon"(default)，则超过换日时间后为(d+1)日；
        //否则(="morning")超过换日时间后仍为(d)日，在换日之前为(d-1)日。
        boolean afternoon = CreamProperties.getInstance().getProperty(
            "AccountingDateShiftTime","afternoon").equalsIgnoreCase("afternoon");
        if (afternoon) {
            if (realTime.compareTo(changeTime) > 0) {
                cal.add(Calendar.DATE, 1);
            }
        } else {
            if (realTime.compareTo(changeTime) <= 0) {
                cal.add(Calendar.DATE, -1);
            }
        }
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    private Date getAccountingDate() {
        String storeID = getStoreID();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String select = "SELECT * FROM calendar";
        String where = " WHERE storeID = '" + storeID + "' AND dateType = '1'";
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(select + where);
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace(Server.getLogger());
            }
            return new java.util.Date();
        }
        HashMap newMap = null;
        try {
            if (resultSet.next()) {
                newMap = new HashMap();
                ResultSetMetaData metaData = resultSet.getMetaData();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object obj = metaData.getColumnName(i);
                    newMap.put(obj, resultSet.getObject(i));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
        }
        java.util.Date accDate = new java.util.Date();
        if (newMap == null)
            return accDate;
        if (newMap.get("currentDate") instanceof java.util.Date)
            accDate = (java.util.Date)newMap.get("currentDate");
        return accDate;
    }

    public boolean isDateShiftRunning(int dateType) {
        String type = "";
        switch (dateType) {
           case DATE_ACCDATE:
               type = "1";
               break;
           case DATE_BIZDATE:
               type = "0";
               break;
           case DATE_INFDATE:
               type = "4";
               break;
           default:
               return false;
        }
        String storeID = getStoreID();//CreamToolkit.getConfiguration().getProperty("StoreID");
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String select = "SELECT * FROM calendar ";
        String where = " WHERE storeID = '" + storeID + "' AND dateType = '" + type + "'";
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(select + where);
            //System.out.println(select + where);
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            if (connection == null) {
                //BRUCE> THIS IS VERY SERIOUS PROBLEM, SHUT INLINE SERVER DOWN,
                // AND LET IT RESTART AGAIN!!!
                Server.log("Cannot get database connection, server is stopped.");
                System.exit(1);
            }
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace(Server.getLogger());
            }
            return false;
        }
        HashMap newMap = null;
        try {
            if (resultSet.next()) {
                newMap = new HashMap();
                ResultSetMetaData metaData = resultSet.getMetaData();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object obj = metaData.getColumnName(i);
                    newMap.put(obj, resultSet.getObject(i));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
        }
        String running  = "";
        if (newMap == null)
            return false;
        if (newMap.get("scEodResult") instanceof String)
            running = (String)newMap.get("scEodResult");
        if (running.compareTo("1") == 0)
           return true;
        return false;
    }

    public boolean isDateShiftRunningOrFailed(int dateType) {
        String type = "";
        switch (dateType) {
           case DATE_ACCDATE:
               type = "1";
               break;
           case DATE_BIZDATE:
               type = "0";
               break;
           case DATE_INFDATE:
               type = "4";
               break;
           default:
               return false;
        }
        String storeID = getStoreID();//CreamToolkit.getConfiguration().getProperty("StoreID");
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String select = "SELECT * FROM calendar ";
        String where = " WHERE storeID = '" + storeID + "' AND dateType = '" + type + "'";
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(select + where);
            //System.out.println(select + where);
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            if (connection == null) {
                //BRUCE> THIS IS VERY SERIOUS PROBLEM, SHUT INLINE SERVER DOWN,
                // AND LET IT RESTART AGAIN!!!
                Server.log("Cannot get database connection, server is stopped.");
                System.exit(1);
            }
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace(Server.getLogger());
            }
            return false;
        }
        HashMap newMap = null;
        try {
            if (resultSet.next()) {
                newMap = new HashMap();
                ResultSetMetaData metaData = resultSet.getMetaData();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object obj = metaData.getColumnName(i);
                    newMap.put(obj, resultSet.getObject(i));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
        }
        String running  = "";
        if (newMap == null)
            return false;
        if (newMap.get("scEodResult") instanceof String)
            running = (String)newMap.get("scEodResult");
        if (running.equals("1") || running.equals("2"))
           return true;
        return false;
    }

    public boolean afterReceivingShiftReport(ShiftReport shift) {
        // 根据shift的结束日期来决定会计日期
        Date accountingDate = getAccountingDate(shift.getSignOffSystemDateTime());
        shift.setAccountingDate(accountingDate);
        //shift.setAccountingDate(getAccountingDate());
        if (!shift.insert(false)) {
            Server.log("Shift exists (pos=" + shift.getTerminalNumber() +
                ",z=" +shift.getZSequenceNumber() +
                ",shift=" +shift.getSequenceNumber() + "), delete first.");
            shift.deleteByPrimaryKey();
            if (!shift.insert(false)) {
                Server.log("Shift inserted failed (pos=" + shift.getTerminalNumber() +
                    ",z=" +shift.getZSequenceNumber() +
                    ",shift=" +shift.getSequenceNumber() + ").");
                return false;
            }
        }
        Server.log("Shift inserted (date=" + dateFormatter.format(shift.getAccountingDate()) +
            ",pos=" + shift.getTerminalNumber() +
            ",z=" +shift.getZSequenceNumber() +
            ",shift=" +shift.getSequenceNumber() + ").");
        return true;
    }

    /**
     * Check if the given date's accdayrpt has no records, if yes, copy from
     * yesterday's record, or from commdl_accitem.
     * 
     * @author bruce
     */
    private boolean generateAccdayrptRecords(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        String yesterday = yyyy_MM_dd.format(cal.getTime());
        String today = yyyy_MM_dd.format(date);

        Map value = query("SELECT COUNT(*) AS cnt FROM accdayrpt " 
            + "WHERE accountDate='" + today + "'");
        if (value != null && value.containsKey("cnt")) {
            Integer todayCount = (Integer)value.get("cnt");
            if (todayCount.compareTo(integer0) > 0) {
                return true;  //表示今天的记录已经存在
            }
        }

        value = query("SELECT COUNT(*) AS cnt FROM accdayrpt " 
            + "WHERE accountDate='" + yesterday + "'");
        Integer yesterdayCount = integer0;
        if (value != null && value.containsKey("cnt"))
            yesterdayCount = (Integer)value.get("cnt");

        if (yesterdayCount.compareTo(integer0) > 0) { // 表示有昨天的记录
            update(
                "CREATE TEMPORARY TABLE tmp ("
                    + "storeID varchar(6) NOT NULL DEFAULT '' ,"
                    + "accountDate date NOT NULL DEFAULT '0000-00-00' ,"
                    + "accountNumber varchar(6) NOT NULL DEFAULT '' ,"
                    + "accountName varchar(30) ,"
                    + "posNumber tinyint(3) unsigned NOT NULL DEFAULT '0' ,"
                    + "displayType char(2) ,"
                    + "displaySequence varchar(6) ,"
                    + "posAmount decimal(12,2) ,"
                    + "storeAmount decimal(12,2) ,"
                    + "storeCount tinyint(3) unsigned ,"
                    + "updateType varchar(4) ,"
                    + "calType varchar(4) ,"
                    + "uploadType char(1) ,"
                    + "itemNumber varchar(10) ,"
                    + "payID char(2) ,"
                    + "dataSource varchar(50) ,"
                    + "updateUserID varchar(8) ,"
                    + "updateDate datetime"
                    + ")");
            update(
                "INSERT INTO tmp ("
                    + "storeID, accountDate, accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate) "
                    + "SELECT storeID, '" + today + "', accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate "
                    + "FROM accdayrpt "
                    + "WHERE accountDate='" + yesterday + "'");
            update("INSERT INTO accdayrpt SELECT * FROM tmp");
            update(
                "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                    + "WHERE accountDate='" + today + "'");
            update("DROP TABLE tmp");

        } else {  // accdayrpt中没有昨天的记录，从commdl_accitem取

            value = query("SELECT MAX(updatebegindate) dt FROM commdl_accitem");
            if (value == null || !value.containsKey("dt")) {
                Server.log("Cannot create accdayrpt records.");
                return false;
            }
            Date accitemDate = (Date)value.get("dt");
            String accitemDateString = yyyy_MM_dd.format(accitemDate);

            update(
                "INSERT INTO accdayrpt ("
                    + "storeID, accountDate, accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate) "
                    + "SELECT storeID, '" + today + "', accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate "
                    + "FROM commdl_accitem "
                    + "WHERE updateBeginDate='" + accitemDateString + "'");
            update(
                "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                    + "WHERE accountDate='" + today + "'");
        }
        return true;
    }

    public boolean afterReceivingZReport(ZReport z) {
        boolean returnValue = true;

        // 根据Z的结束日期来决定会计日期
        Date accountingDate = getAccountingDate(z.getEndSystemDateTime());
        z.setAccountingDate(accountingDate);

        if (!z.insert(false)) {
            Server.log("Z exists (pos=" + z.getTerminalNumber() +
                ",z=" + z.getSequenceNumber() + "), delete first.");
            z.deleteByPrimaryKey();
            if (!z.insert(false)) {
                Server.log("Z inserted failed (pos=" + z.getTerminalNumber() +
                    ",z=" + z.getSequenceNumber() + ").");
                return false;
            }
        }
        Server.log("Z inserted (date=" + dateFormatter.format(z.getAccountingDate()) +
            ",pos=" + z.getTerminalNumber() +
            ",z=" + z.getSequenceNumber() + ").");

        // 检查或生成该会计日期的accdayrpt记录
        generateAccdayrptRecords(accountingDate);

        // Update posaccountdate
        HashMap newRec = new HashMap();
        newRec.put("storeID", z.getStoreNumber().trim());
        newRec.put("accountDate", z.getAccountingDate());
        newRec.put("posNumber", z.getTerminalNumber());
        newRec.put("zSequenceNumber", z.getSequenceNumber());
        if (!insert(newRec, "posaccountdate"))
            Server.log("Maybe caused by re-send Z.");

        synchronized (lockObject) {
            Iterator iter = ZReport.queryByDateTime(accountingDate);
            if (iter == null)
                return returnValue;

            String accountingDateString = yyyy_MM_dd.format(accountingDate);
            // 先避免posAmount和storeAmount有空值
            update(
                "UPDATE accdayrpt SET storeAmount=0 "
                    + "WHERE accountDate='" + accountingDateString 
                    + "' AND storeAmount IS NULL");
            update(
                "UPDATE accdayrpt SET posAmount=0 "
                    + "WHERE accountDate='" + accountingDateString 
                    + "' AND posAmount IS NULL");
            // 把storeAmount先计成与原posAmount的差额(为了保留用户原来对storeAmount的修改)
            update(
                "UPDATE accdayrpt SET storeAmount=storeAmount-posAmount "
                    + "WHERE accountDate='" + accountingDateString + "'");
            // 然后把posAmount归零
            update(
                "UPDATE accdayrpt SET posAmount=0 "
                    + "WHERE accountDate='" + accountingDateString + "'");

            // 开始针对每一个该日的Z帐进行过转到accdayrpt的计算
            while (iter.hasNext()) {
                z = (ZReport)iter.next();

                // 更新现金日报表
                //Primary Key: storeID + accountDate + accountNumber + posNumber
                String storeID = z.getStoreNumber();
                int posNumber = z.getTerminalNumber().intValue();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String accDate = format.format(z.getAccountingDate()).toString();
                Connection connection = null;
                Statement statement = null;
                ResultSet resultSet = null;
                ArrayList table = null;
                String select = "SELECT * FROM accdayrpt ";
                String updateString = "UPDATE accdayrpt SET ";
                String where =" WHERE storeID = '" + storeID
                    + "' AND accountDate = '" + accDate + "'";//AND posNumber = '"    + posNumber + "'";
                //CreamToolkit.logMessage("" + select + where);
                try {
                    connection = CreamToolkit.getPooledConnection();
                    statement = connection.createStatement();
                    resultSet = statement.executeQuery(select + where);
                    table = constructFromResultSet(resultSet);
                    if (table.isEmpty()) {
                        Server.log("afterReceivingZReport(): Cannot find record in accdayrpt");
                        returnValue = false;
                    }
                } catch (SQLException e) {
                    Server.log("afterReceivingZReport(): Cannot find record in accdayrpt");
                    Server.log("SQLStatement: " + select + where);
                    e.printStackTrace(Server.getLogger());
                    returnValue = false;
                } finally {
                    if (!returnValue) {
                        try {
                            if (statement != null)
                                statement.close();
                        } catch (SQLException e) {
                            e.printStackTrace(Server.getLogger());
                        }
                        if (connection != null)
                            CreamToolkit.releaseConnection(connection);
                        return false;
                    }
                }
    
                Map zRecord = z.getValues();
                HashMap map = new HashMap();
        
                //Bruce/2002-04-10
                //map.put("001", z.getTransactionCount());
                map.put("001", z.getCustomerCount());
        
                map.put("002", z.getGrossSales());
                map.put("004", z.getMixAndMatchTotalAmount());
                map.put("006", z.getNetSalesTotalAmount());
                map.put("201", z.getVoucherAmount());
                map.put("300", z.getDaiShouAmount2()); //Bruce/20030819 公共事业费代收金额
                map.put("601", z.getSpillAmount());
                /*
                map.put("102A", z.getTaxAmount1());
                map.put("102B", z.getTaxAmount2());
                map.put("102C", z.getTaxAmount3());
                map.put("102D", z.getTaxAmount4());
                */
                //map.put("101A", z.getTaxedAmount());
                //map.put("101B", z.getNoTaxedAmount());
                //
                Iterator itr = table.iterator();
                while (itr.hasNext()) {
                    HashMap accdayrptRecord = (HashMap)itr.next();
                    String accountNumber = (String)accdayrptRecord.get("accountnumber");
                    //CreamToolkit.logMessage("" + "accountnumber=" + accountNumber);
                    String payID = (String)accdayrptRecord.get("payid");
                    if (payID == null || payID.length() == 0)
                       payID = "-1";
                    //CreamToolkit.logMessage("" + "payid=" + payID);
                    int cposNumber = Integer.parseInt(accdayrptRecord.get("posnumber").toString());
                    //CreamToolkit.logMessage("" + "posNumber=" + cposNumber);
                    if (cposNumber == posNumber) {
                        if (map.containsKey(accountNumber)) {
                            BigDecimal amt = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            BigDecimal mapAmt = new BigDecimal(0);
                            if (map.get(accountNumber) instanceof Integer) {
                                Integer in = (Integer)map.get(accountNumber);
                                mapAmt = new BigDecimal(in.intValue());
                            } else if (map.get(accountNumber) instanceof BigDecimal) {
                                mapAmt = (BigDecimal)map.get(accountNumber);
                            }
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt );
                            if (amt == null)
                               amt = new BigDecimal(0);
                            amt = amt.add(mapAmt);
                            //CreamToolkit.logMessage("" + accountNumber + ":Additional amount = " + mapAmt);
                            accdayrptRecord.put(POS_AMOUNT, amt);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(mapAmt));
                            }
                        }
                    } else if (cposNumber == 0) {
                        if (map.containsKey(accountNumber)) {
                            BigDecimal amt = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            BigDecimal mapAmt = new BigDecimal(0);
                            if (map.get(accountNumber) instanceof Integer) {
                                Integer in = (Integer)map.get(accountNumber);
                                mapAmt = new BigDecimal(in.intValue());
                            } else if (map.get(accountNumber) instanceof BigDecimal) {
                                mapAmt = (BigDecimal)map.get(accountNumber);
                            }
                            if (amt == null)
                               amt = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt );
                            //CreamToolkit.logMessage("" + accountNumber + ":Additional amount = " + mapAmt);
                            amt = amt.add(mapAmt);
                            accdayrptRecord.put(POS_AMOUNT, amt);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(mapAmt));
                            }
                        }  else if (accountNumber.startsWith("3")) {//代收
                            
                            BigDecimal daiShouTotalAmount = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (daiShouTotalAmount == null)
                               daiShouTotalAmount = new BigDecimal(0);

                            BigDecimal amt;
                            if (accountNumber.equals("300")) { //Bruce/20030819 公共事业费代收金额
                                amt = z.getDaiShouAmount2();
                            } else {
                                if (payID.startsWith("0") && payID.length() > 1)
                                   payID = payID.substring(1);
                                amt = (BigDecimal)zRecord.get("daiShouDetailAmount" + payID);
                            }

                            if (amt == null)
                               amt = new BigDecimal(0);
                            daiShouTotalAmount = daiShouTotalAmount.add(amt);
                            
                            accdayrptRecord.put(POS_AMOUNT, daiShouTotalAmount);
                            BigDecimal storeAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (storeAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, daiShouTotalAmount);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, storeAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("4")) {//代付
                            if (payID.startsWith("0") && payID.length() > 1)
                               payID = payID.substring(1);
                            BigDecimal amt = (BigDecimal)zRecord.get("daiFuDetailAmount" + payID);
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("5")) {//支付
                            if (payID.length() < 2)
                               payID += "0";
                            BigDecimal amt = (BigDecimal)zRecord.get("pay" + payID + "Amount");
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("7")) {//Paid-In
                            int id = Integer.parseInt(payID);
                            if (id < 0)
                                continue;
                            BigDecimal amt = (BigDecimal)zRecord.get("paidInDetailAmount" + id);
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("8")) {//Paid-Out
                            int id = Integer.parseInt(payID);
                            if (id < 0)
                                continue;
                            BigDecimal amt = (BigDecimal)zRecord.get("paidOutDetailAmount" + Integer.parseInt(payID));
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("9")) {//门市费用
                            int id = Integer.parseInt(payID);
                            if (id < 0)
                                continue;
                            BigDecimal amt = (BigDecimal)zRecord.get("paidOutDetailAmount" + Integer.parseInt(payID));
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            }
                        } else if (accountNumber.startsWith("101")) {
                            int id = Integer.parseInt(payID);
                            if (id < 0)
                                continue;
                            BigDecimal amt = (BigDecimal)zRecord.get("netSaleTax" + id + "Amount");
                            //CreamToolkit.logMessage("" + accountNumber + "zAmount = " + amt);
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("102")) {
                            int id = Integer.parseInt(payID);
                            if (id < 0)
                                continue;
                            BigDecimal amt = (BigDecimal)zRecord.get("taxAmount" + id);
                            //CreamToolkit.logMessage("" + accountNumber + "zAmount = " + amt);
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        }
                    }
                    accdayrptRecord.put("updateUserID", z.getCashierNumber());
                    accdayrptRecord.put("updateDate", new java.util.Date());
                    String uWhere =" WHERE storeID = '" + storeID
                       + "' AND accountDate = '" + accDate + "' AND posNumber = '" + cposNumber
                       + "' AND accountNumber = '" + accountNumber + "'";
                    String fieldName = "";
                    String nameParams = "";
                    Object fieldValue = new Object();
                    Iterator iterator = accdayrptRecord.keySet().iterator();
                    while (iterator.hasNext()) {
                        fieldName = (String)iterator.next();
                        if (accdayrptRecord.get(fieldName) == null) {
                            fieldValue = new Object();
                        } else {
                            fieldValue = accdayrptRecord.get(fieldName);
                            if (fieldValue instanceof java.util.Date) {
                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                java.util.Date d = (java.util.Date)fieldValue;
                                nameParams  += fieldName + "=" + "'" + df.format(d).toString() + "',";
                            } else if (fieldValue instanceof String) {
                                nameParams += fieldName + " = " + "'" + fieldValue + "',";
                            } else {
                                nameParams += fieldName + " = " +  fieldValue + ",";
                            }
                        }
                    }
                    String finalStr = updateString + nameParams.substring(0, nameParams.length()-1) + "  " + uWhere;//
                    try {
                        statement = connection.createStatement();
                        statement.execute(finalStr);
                        statement.close();
                    } catch (SQLException e) {
                        Server.log("SQL statement: " + finalStr);
                        e.printStackTrace(Server.getLogger());
                        returnValue = false;
                    } finally {
                        try {
                            if (statement != null)
                                statement.close();
                        } catch (SQLException e) {
                            e.printStackTrace(Server.getLogger());
                        }
                        if (connection != null)
                            CreamToolkit.releaseConnection(connection);
                    }
                }
            } // Iterate ZReport loop
        } // synchronization block
        return returnValue;
    }

    private void insertSalertn(Transaction t) {
        if (t == null)
            return;

        //Bruce/2003-11-10 在外层已经有锁lockObject
        //synchronized (CStorePostProcessor.class) {

        HashMap newMap = new HashMap();
        String storeID = t.getStoreNumber();
        String posNumber = t.getTerminalNumber().toString();
        Iterator lineItems = t.getLineItemsSimpleVersion();
        ArrayList last = new ArrayList();
        while (lineItems.hasNext()) {
            LineItem lineItem = (LineItem)lineItems.next();
            //if (!lineItem.getDetailCode().equals("R"))
                //continue;
            PLU plu = PLU.queryByPluNumber(lineItem.getPluNumber());
            if (plu == null) {
                Server.log("insertSalertn(): Cannot find " + lineItem.getPluNumber() +
                    "in posdl_plu");
                continue;
            }
            if (t.getDealType2().equals("0")) {
                last = getCollectionOfStatement(
                    "SELECT * FROM salertn WHERE transactionNumber = " +
                    (t.getTransactionNumber().intValue() - 1) +
                    " AND posNumber = '"  + posNumber +"'");
                Iterator itr = last.iterator();
                while (itr.hasNext()) {
                    Map next = (Map)itr.next();
                    BigDecimal amt = (BigDecimal)next.get("salertnquantity");
                    String number = (String)next.get("itemnumber");
                    if (amt.abs().compareTo(lineItem.getQuantity().abs()) == 0
                        && number.compareTo(plu.getInStoreCode()) == 0) {
                        //storeID + busiDate + posNumber + transactionNumber + lineItemSequence
                        String delete = "DELETE FROM salertn" +
                        " WHERE storeID = '" + next.get("storeid") + "' AND busiDate = '" + next.get("busidate") +
                        "' AND posNumber = '" + next.get("posnumber") + "' AND transactionNumber = " + next.get("transactionnumber") +
                        " AND lineItemSequence = " + next.get("lineitemsequence");
                        executeQuery(delete);

                        //Bruce> Add "break" to fix a bug:
                        //   Buy  (1) A x 1
                        //        (2) A x 1
                        //        (3) B x 2
                        //        (4) C x 3
                        //  if 部分退货 (1),(3),(4), then it'll delete all 'A' in salertn
                        break;
                    }
                }
                continue;
            }
            newMap.put("busiDate", getBusinessDate(t.getSystemDateTime()));
            newMap.put("storeID", storeID);
            newMap.put("posNumber", t.getTerminalNumber());
            newMap.put("transactionNumber", t.getTransactionNumber());
            newMap.put("lineItemSequence", new Integer(lineItem.getLineItemSequence()));
            newMap.put("itemNumber", plu.getInStoreCode());
            newMap.put("shipNumber", "0");
            newMap.put("updateUserID", t.getCashierNumber());
            newMap.put("updateDate", new java.util.Date());
            newMap.put("saleRtnQuantity", lineItem.getQuantity().abs());
            newMap.put("storeUnitPrice", lineItem.getUnitPrice());
            Map m = query("SELECT unitCost FROM plu WHERE itemNumber = '" + plu.getInStoreCode() + "'");
            BigDecimal unitCost = (BigDecimal)m.get("unitcost");
            newMap.put("unitCost", unitCost);
            insert(newMap, "salertn");
            //Server.log("insertSalertn(): Insert a record into salertn(no=" + plu.getInStoreCode() +
            //    ",qty=" + lineItem.getQuantity().abs() + ")");
        }
        //}
    }

    public void insertVoidInvo(Transaction t) {//插入作废发票明细表纪录
        //storeID + accountDate + posNumber + transactionNumber
        //CreamToolkit.logMessage("" + t.getVoidTransactionNumber()+ "????");
        if (t == null)
            return;
        if (t.getVoidTransactionNumber() == null)
            return;
        Transaction voidT = Transaction.queryByTransactionNumber(
            t.getTerminalNumber().intValue(),
            t.getVoidTransactionNumber().toString());
        if (voidT == null)
           return;
        /*if (!voidT.getDealType1().equals("*")) {
            voidT.setDealType1("*");
            voidT.update();
        }*/
        //update(voidT);
        HashMap newMap = new HashMap();
        String storeID = t.getStoreNumber();//CreamProperties.getInstance().getProperty("StoreNumber");
        String posNumber = t.getTerminalNumber().toString();//CreamProperties.getInstance().getProperty("TerminalNumber");
        newMap.put("accountDate", getAccountingDate());
        newMap.put("storeID", storeID);
        newMap.put("voidDateTime", t.getSystemDateTime());
        newMap.put("posNumber", posNumber);
        newMap.put("cashierID", t.getCashierNumber());
        newMap.put("transactionNumber", t.getVoidTransactionNumber());
        String dt1 = t.getDealType1();
        String dt2 = t.getDealType2();
        String dt3 = t.getDealType3();

        if (dt1.equals("0") && dt2.equals("0") && dt3.equals("1"))        {
            newMap.put("voidType", "1");
        } else if (dt1.equals("0") && dt2.equals("3") && dt3.equals("4")) {
            newMap.put("voidType", "2");
        } else if (dt1.equals("0") && dt2.equals("0") && dt3.equals("2")) {
            newMap.put("voidType", "3");
        } else if (dt1.equals("0") && dt2.equals("0") && dt3.equals("3")) {
            newMap.put("voidType", "4");
        } else {
 /*////        if (voidT.getNetSalesAmount().signum() < 0) {
                newMap.put("voidType", "6");
            } else if (voidT.getNetSalesAmont().signum() == 0)
                ap.put("voidType", "7");
            else
*/////        newMap.put("voidType", "5");
        }

/*voidInvoiceHead=INVNOHEAD
voidInvoiceNumber=INVNO
invoiceCount=INVCNT
netSaleTotalAmount=NETSALAMT
netSaleTax0Amount=NETSALAMT0
netSaleTax1Amount=NETSALAMT1
netSaleTax2Amount=NETSALAMT2
netSaleTax3Amount=NETSALAMT3
netSaleTax4Amount=NETSALAMT4
*/
        newMap.put("voidInvoiceHead", voidT.getInvoiceID());
        newMap.put("voidInvoiceNumber", voidT.getInvoiceNumber());
        newMap.put("invoiceCount", voidT.getInvoiceCount());
        newMap.put("netSaleTotalAmount", voidT.getNetSalesTotalAmount());
        newMap.put("netSaleTax0Amount", voidT.getNetSalesAmount0());
        newMap.put("netSaleTax1Amount", voidT.getNetSalesAmount1());
        newMap.put("netSaleTax2Amount", voidT.getNetSalesAmount2());
        newMap.put("netSaleTax3Amount", voidT.getNetSalesAmount3());
        newMap.put("netSaleTax4Amount", voidT.getNetSalesAmount4());
        insert(newMap, "voidinvo");
    }

    /**
     * Meyer/2003-03-10/Modified to deal with 包装商品 itempack
     * 
     * @param t
     * @param li
     */
    public void updateInv(Transaction t, LineItem li) {//更新库存表

        //Bruce/2003-11-10 在外层已经有锁lockObject
        //synchronized (CStorePostProcessor.class) {

        //storeID + busiDate + itemNumber + shipNumber
        //PLU plu = PLU.queryByPluNumber(li.getPluNumber());
        //if (plu == null) {
        //    if (li.getPluNumber() != null && li.getPluNumber().length() == 13)
        //        Server.log("Cannot update inv. Cannot find " + li.getPluNumber() +
        //            " in posdl_plu");
        //    return;
        //}

        /*
         * Meyer/2003-03-10/
         */
        String inStoreCode = li.getItemNumber(); //plu.getInStoreCode();
        String storeID = t.getStoreNumber();//CreamProperties.getInstance().getProperty("StoreNumber");
        BigDecimal origPluQty = li.getQuantity();

        Connection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;        
        HashMap pluQtyMap = new HashMap();        
        
        if (li.getDetailCode().equals("D") ||   //折扣
            li.getDetailCode().equals("I") ||   //代售
            li.getDetailCode().equals("M") ||   //组合促销 
            li.getDetailCode().equals("O") ||   //代收公共事业费
            li.getDetailCode().equals("Q"))     //代付
            return;
        
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            
            resultSet = statement.executeQuery(
                " SELECT * FROM itempack WHERE StoreID='" + storeID + 
                "' AND packNo='" + inStoreCode + "'");
            while (resultSet.next()) {
                pluQtyMap.put(resultSet.getString("itemNo"),
                    origPluQty.multiply(new BigDecimal(resultSet.getInt("packQty")))
                );
            }
            //Meyer/2003-03-11/get original itemNumber when no itempack record existed
            if (pluQtyMap.size() == 0) {
                pluQtyMap.put(inStoreCode, origPluQty);
            }    
        } catch (SQLException e) {
            //Meyer/2003-03-11/get original itemNumber when no itempack table existed
            pluQtyMap.put(inStoreCode, origPluQty);
            
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }    

        String shipNumber = "0";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String busiDate = format.format(getBusinessDate(t.getSystemDateTime())).toString();
        HashMap map = null;

        String itemNumber;
        BigDecimal itemQuantity;

        Iterator iter = pluQtyMap.keySet().iterator();
        while (iter.hasNext()) {
            itemNumber = (String)iter.next();
            itemQuantity = (BigDecimal)pluQtyMap.get(itemNumber); 
            
            String select = "SELECT * FROM inv ";
            String updateString = "UPDATE inv SET ";             
            String where = " WHERE storeID = '" + storeID + "' AND busiDate = '" +
                busiDate + "' AND itemNumber = '" + itemNumber +
                "' AND shipNumber = '" + shipNumber + "'";
            try {
                connection = CreamToolkit.getPooledConnection();
                statement = connection.createStatement();
                resultSet = statement.executeQuery(select + where);
                map = constructFromResultSet2(resultSet);
                if (map.isEmpty()) {
                    Server.log("Cannot find inv with number of: " + inStoreCode);
                    Server.log(select + where);
                    return;
                }
            } catch (SQLException e) {
                Server.log("Error SQL statement: " + select + where);
                e.printStackTrace(Server.getLogger());
                return;
            } finally {
                try {
                    if (statement != null)
                        statement.close();
                } catch (SQLException e) {
                    e.printStackTrace(Server.getLogger());
                }
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            }
            BigDecimal accSaleQuantity = (BigDecimal)map.get("accsalequantity");
            BigDecimal accSaleRtnQuantity = (BigDecimal)map.get("accsalertnquantity");
            String fieldName1 = "accsalequantity";
            String fieldName2 = "accsalertnquantity";
            String dt1 = t.getDealType1();
            String dt2 = t.getDealType2();
            String dt3 = t.getDealType3();
            
    Server.log("dt1=" + dt1 + ",dt2=" + dt2 + ",dt3=" + dt3);
    String invStr = "Sale qty of " + itemNumber + ":" + accSaleQuantity;
    String rtnStr = "Return qty of " + itemNumber + ":" + accSaleRtnQuantity;
            
            if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("1")) {//前笔误打
                accSaleQuantity = accSaleQuantity.add(itemQuantity);
                updateString += fieldName1  + "=" + accSaleQuantity + "";
    invStr += "->" + accSaleQuantity;
    Server.log(invStr);
            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("3") && dt3.equals("4")) {//全部退货
                //accSaleQuantity = accSaleQuantity.add(itemQuantity);
                //updateString += fieldName1  + "=" + accSaleQuantity + ",";
                accSaleRtnQuantity = accSaleRtnQuantity.add(itemQuantity.negate());
                updateString += fieldName2  + "=" +    accSaleRtnQuantity + "";
    rtnStr += "->" + accSaleRtnQuantity;
    Server.log(rtnStr);
            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("4")) {//部分退货
                accSaleRtnQuantity = accSaleRtnQuantity.add(itemQuantity.negate());
                updateString += fieldName2  + "=" +    accSaleRtnQuantity + "";
                //accSaleQuantity = accSaleQuantity.add(itemQuantity);
                //updateString += fieldName1  + "=" + accSaleQuantity + "";
    rtnStr += "->" + accSaleRtnQuantity;
    Server.log(rtnStr);
            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("3")) {//交易取消
                accSaleQuantity = accSaleQuantity.add(itemQuantity);
                updateString += fieldName1  + "=" + accSaleQuantity + "";
    invStr += "->" + accSaleQuantity;
    Server.log(invStr);
            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("2")) {//卡纸重印
                accSaleQuantity = accSaleQuantity.add(itemQuantity);
                updateString += fieldName1  + "=" + accSaleQuantity + "";
    invStr += "->" + accSaleQuantity;
    Server.log(invStr);
            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("0")) {//正常交易
                accSaleQuantity = accSaleQuantity.add(itemQuantity);
                updateString += fieldName1  + "=" + accSaleQuantity + "";
    invStr += "->" + accSaleQuantity;
    Server.log(invStr);
            } else {
                //CreamToolkit.logMessage(dt1 + "|" + dt2 + "+" + dt3);
                //CreamToolkit.logMessage("No record updated!");
                return;
            }
    
            //CreamToolkit.logMessage("" + accSaleQuantity + "|" +  accSaleRtnQuantity + "|" +  itemQuantity + "!" + inStoreCode);
            updateString += where;
            Connection connection2  = null;
            Statement  statement2   = null;
            try {
                //CreamToolkit.logMessage("" + updateString);
                connection2 = CreamToolkit.getPooledConnection();
                statement2 = connection2.createStatement();//    resultSet =
                statement2.executeUpdate(updateString);
            
                Collection col = getFieldList("inv");
                if (col.contains("modifyDate".toLowerCase())) {
                    executeQuery("UPDATE inv SET modifyDate = '" 
                            + getBusinessDate(new Date()) + "' "
                            + where);
                }
                
                //2003-09-22 更新inv表中期初数量
                if (!getAccountingDate(new Date()).equals(getAccountingDate())) {
                    //参数列表： 店号，交易日，品号，趟次，调整数量
                    if (!updateBeginInvQty(storeID, busiDate, itemNumber, shipNumber, accSaleQuantity.subtract(accSaleRtnQuantity))) {
                        Server.log("Error occured when update begin inv quantity: " 
                                  + "storeID = " + storeID + ", "
                                  + "itemNumber = " + itemNumber + ", " 
                                  + "busiDate = " + busiDate + ", "
                                  + "shipNumber = " + shipNumber);
                    }
                }

            } catch (SQLException e) {
                Server.log("Error SQL statement: " + updateString);
                e.printStackTrace(Server.getLogger());
            } finally {
                try {
                    if (statement2 != null)
                        statement2.close();
                } catch (SQLException e) {
                    e.printStackTrace(Server.getLogger());
                }
                if (connection2 != null)
                    CreamToolkit.releaseConnection(connection2);
            }
        } //end of while
        //}
    }

    /*
     * 更新busiDate 以后各天的期初数量
     * 参数列表
     *      String storeID      : 店号
     *      String busiDate     : 业务日期
     *      String itemNo       : 品号
     *      String shipNo       : 趟次
     *      BigDecimal deltaQty : 调整数量（需要从期初数量中减去）  
     */    
    private boolean updateBeginInvQty(String storeID, String busiDate, String itemNo, String shipNo, BigDecimal deltaQty) {
        Connection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;        

        boolean returnValue = false;
        String updateString = "UPDATE inv SET befInvQuantity = befInvQuantity - " + deltaQty + " "
                   + "WHERE storeID = '" + storeID + "' AND " 
                   + "itemNumber = '" + itemNo + "' AND "
                   + "shipNumber = '" + shipNo + "' AND "
                   + "busiDate > '" + busiDate + "'";
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.executeUpdate(updateString);
            returnValue = true;

        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
                
            return returnValue;
        }
    }
    
    public boolean afterReceivingTransaction(Object[] trans) {
        //Bruce/20030730
        //if (isDateShiftRunning(DATE_BIZDATE))
        //    return false;

        Server.log("addTrans {");
        // Insert into databas first
        int i;
        Transaction tran = null;

        int lineItemShouldHave = 0;
        int lineItemActualHave = 0;
        int posNumber = 0;
        int transactionNumber = 0;        
        for (i = 0; i < trans.length; i++) {
            if (trans[i] instanceof Transaction) {
                tran = (Transaction)trans[i];
                lineItemShouldHave = tran.getDetailCount().intValue();
                posNumber = tran.getTerminalNumber().intValue();
                transactionNumber = tran.getTransactionNumber().intValue();
            } else if (trans[i] instanceof LineItem) {
                lineItemActualHave++;
                LineItem li = (LineItem)trans[i];
                if (tran != null) {

                    // Because there is no connection between Transaction and
                    // LineItem objects sent from POS
                    tran.addLineItemSimpleVersion(li);

                    li.setSystemDateTime(tran.getSystemDateTime());
                }
            }
        }

        // integrity check, if has problem, discard it.
        if (lineItemShouldHave != lineItemActualHave) {
            Server.log("Transaction (pos=" + posNumber + ", no=" + transactionNumber
                + ") is incomplete. Detail count should be " + lineItemShouldHave
                + ", but actual detail count is " + lineItemActualHave);
            return false;
        }

        //Bruce/2003-11-4
        ///*1. 不让不同台的交易同时进行插入动作*/
        //2. 如果中途插入失败，试图做rollback动作
        //3. 尽可能catch各式exception，然后rollback
        for (i = 0; i < trans.length; i++) {
            DacBase dac = (DacBase)trans[i];
            try {
                Server.log("insert " + i);
                if (!dac.insert(false)) {
                    //throw new Exception();
                    Server.log("Transaction (pos=" + posNumber + ", no=" + transactionNumber
                        + ") was inserted failed.");
                    // rollback
                    for (int j = i - 1; j >= 0; j--)
                        ((DacBase)trans[j]).deleteByPrimaryKey();
                    Server.log("Rollbacked.");
                    return false;
                }
            } catch (Exception e) {
                Server.log("Transaction (pos=" + posNumber + ", no=" + transactionNumber
                    + ") was inserted failed.");
                // rollback
                for (int j = i - 1; j >= 0; j--)
                    ((DacBase)trans[j]).deleteByPrimaryKey();
                Server.log("Rollbacked.");
                return false;
            }
        }

        //Bruce/2003-11-22
        //因为怀疑JRE可能造成停滞在lockObject，所以先拿掉，先舍弃更新库存可能不准的问题
        //synchronized (lockObject) {

        Server.log("} updateInv {");
        //Bruce/2003-11-10 如果需要更新库存再更新inv等
        if (Server.isUpdateInv()) {
            for (i = 0; i < trans.length; i++) {
                try {
                    if (trans[i] instanceof Transaction) {
                        tran = (Transaction)trans[i];
                        if (!tran.getDealType3().equals("0")) {
                            insertVoidInvo(tran);
                            if (tran.getDealType3().equals("4")) {
                                insertSalertn(tran);
                            }
                        }
                    } else if (trans[i] instanceof LineItem) {
                        LineItem li = (LineItem)trans[i];
                        updateInv(tran, li);
                    }
                } catch (Exception e) {
                    e.printStackTrace(Server.getLogger());
                }
            }
        }
        Server.log("}");
        //}
        return true;
    }

    public boolean afterReceivingDepSales(Object[] depSales) {
        boolean success = true;
        for (int i = 0; i < depSales.length; i++) {
            DepSales ds = (DepSales)depSales[i];
            ds.setAccountDate(getAccountingDate(ds.getPOSNumber(),
                new Integer(ds.getSequenceNumber()).intValue()));
            if (!ds.insert(false)) {
                Server.log("DepSales exists (pos=" + ds.getPOSNumber() +
                    ",z=" + ds.getSequenceNumber() +
                    ",id=" + ds.getDepID() +
                     "), delete first.");
                ds.deleteByPrimaryKey();
                if (!ds.insert(false)) {
                    Server.log("DepSales inserted failed (pos=" + ds.getPOSNumber() +
                        ",z=" + ds.getSequenceNumber() +
                        ",id=" + ds.getDepID() + ").");
                    success = false;
                } else {
                    Server.log("DepSales inserted (pos=" + ds.getPOSNumber() +
                        ",z=" + ds.getSequenceNumber() +
                        ",id=" + ds.getDepID() + ").");
                }
            }
        }
        return success;
    }

    public boolean afterReceivingDaishouSales(Object[] daishouSales) {
        boolean success = true;
        for (int i = 0; i < daishouSales.length; i++) {
            DaishouSales ds = (DaishouSales)daishouSales[i];
            ds.setAccountDate(getAccountingDate(ds.getPosNumber(),
                new Integer(ds.getZSequenceNumber()).intValue()));
            if (!ds.insert(false)) {
                Server.log("DaishouSales exists (pos=" + ds.getPosNumber() +
                    ",z=" + ds.getZSequenceNumber() +
                    ",id1=" + ds.getFirstNumber() +
                    ",id2=" + ds.getSecondNumber() +
                     "), delete first.");
                ds.deleteByPrimaryKey();
                if (!ds.insert(false)) {
                    Server.log("DaishouSales inserted failed (pos=" + ds.getPosNumber() +
                        ",z=" + ds.getZSequenceNumber() +
                        ",id1=" + ds.getFirstNumber() +
                        ",id2=" + ds.getSecondNumber() + ").");
                    success = false;
                } else {
                    Server.log("DaishouSales inserted (pos=" + ds.getPosNumber() +
                        ",z=" + ds.getZSequenceNumber() +
                        ",id1=" + ds.getFirstNumber() +
                        ",id2=" + ds.getSecondNumber() + ").");
                }
            }
        }
        return success;
    }

    /**
     * afterReceivingDaiShouSales2
     *
     * @return 如果代收资料存入SC数据库中成功则返回 true，否则返回 false.
     */
    public boolean afterReceivingDaiShouSales2(Object[] daiShouSales2) {
        boolean success = true;
        for (int i = 0; i < daiShouSales2.length; i++) {
            DaiShouSales2 ds = (DaiShouSales2)daiShouSales2[i];
            ds.setAccountDate(getAccountingDate(ds.getPosNumber(),
                new Integer(ds.getZSequenceNumber()).intValue(), ds.getPayTime()));
            if (!ds.insert(false)) {
                Server.log("DaiShouSales2 exists (StoreID = " + ds.getStoreID() 
                    + ", POS = " + ds.getPosNumber()
                    + ", Z = " + ds.getZSequenceNumber()
                    + ", ID = " + ds.getID() 
                    + "), delete first.");
                ds.deleteByPrimaryKey();
                if (!ds.insert(false)) {
                    Server.log("DaiShouSales2 inserted failed (StoreID = " + ds.getStoreID() 
                    + ", POS = " + ds.getPosNumber()
                    + ", Z = " + ds.getZSequenceNumber()
                    + ", ID = " + ds.getID() 
                    + ")");
                    success = false;
                } else {
                    Server.log("DaiShouSales2 inserted (StoreID = " + ds.getStoreID() 
                    + ", POS = " + ds.getPosNumber()
                    + ", Z = " + ds.getZSequenceNumber()
                    + ", ID = " + ds.getID() 
                    + ")");
                }
            }
        }
        return success;
    }

    /**
     * Inventory processor.
     *
     * @param inventory Inventory objects.
     * @return true if success, otherwise false.
     */
    public boolean afterReceivingInventory(Object[] inventory) {
        int insertCount = 0;
        int updateCount = 0;
        int failCount = 0;
        for (int i = 0; i < inventory.length; i++) {
            Inventory ds = (Inventory)inventory[i];
            if (!ds.insert(false)) {
                //Server.log("Inventory exists (" 
                //    + "pos=" + ds.getPOSNumber()
                //    + ", seq=" + ds.getItemSequence()
                //    + "), delete first.");
                ds.deleteByPrimaryKey();
                if (!ds.insert(false)) {
                    Server.log("Inventory inserted failed (" 
                        + "pos=" + ds.getPOSNumber()
                        + ", seq=" + ds.getItemSequence()
                        + ")");
                    failCount++;
                } else {
                    //Server.log("Inventory inserted (" 
                    //    + "pos=" + ds.getPOSNumber()
                    //    + ", seq=" + ds.getItemSequence()
                    //    + ")");
                    updateCount++;
                }
            } else {
                insertCount++;
            }
        }
        //Server.log("Inventory data uploaded (insert:" + insertCount + ", update:" 
        //    + updateCount + ", fail:" + failCount + ").");
        return (failCount == 0);
    }

    /**
     * Master download preprocessor.
     *
     * @return true if it allows master downloading now, false otherwise.
     */
    public boolean beforeDownloadingMaster() {
        return !isDateShiftRunningOrFailed(DATE_BIZDATE);
    }
    
    /**
     * Meyer / 2002-02-11
     * Check before synchronize transaction
     * 
     * @return true if it allows synchronize transaction, false otherwise.
     */
    public boolean canSyncTransaction() {
        return !isDateShiftRunning(DATE_BIZDATE);
    }
}
