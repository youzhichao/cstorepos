/*jadclipse*/// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) radix(10) lradix(10) 
// Source File Name:   Client.java
package hyi.cream.inline;

import hyi.cream.POSTerminalApplication;
import hyi.cream.SystemInfo;
import hyi.cream.dac.*;
import hyi.cream.event.SystemInfoEvent;
import hyi.cream.uibeans.Indicator;
import hyi.cream.util.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

import sun.security.action.GetPropertyAction;

// Referenced classes of package hyi.cream.inline:
//            ClientCommandException, DacObjectOutputStream, DacObjectInputStream

/**
 * Inline client.
 * 
 * @author Bruce
 * @version 1.7
 */
public class Client extends MutableThread {

    /**
     * /Bruce/1.7/2002-04-27/
     *    增加上传代收明细帐.
     *
     * /Bruce/1.6/2002-04-13/
     *    Modify socket timeout interval.
     *
     * /Bruce/1.5/2002-04-03/
     *    socket.setSoTimeout(TIMEOUT_INTERVAL) on socket creation.
     *
     * /Bruce/1.5/2002-04-01/
     *    1. Change isConnected() and setConnected() to non-synchronized.
     *    2. Log more detail.
     */
    private boolean connectOrig;
    private static boolean commandLineMode;
    public static final transient String VERSION = "1.7";
    private static Class dacForDownload[];
    private static final int SEND_RUOK_INTERVAL = 10000;
    private static final int DEFAULT_TIMEOUT_INTERVAL = 30000;
    private static final int RUOK_TIMEOUT_INTERVAL = 5000;
    private static final int ACK_TIMEOUT_INTERVAL = 60000;
    private static final int LONG1_TIMEOUT_INTERVAL = 120000;
    private Socket socket;
    private String server;
    private int port;
    private DataOutputStream out;
    private DataInputStream in;
    //private BufferedInputStream buffIn;
    private ObjectOutputStream objectOut;
    private ObjectInputStream objectIn;
    private DacObjectOutputStream dacOut;
    private DacObjectInputStream dacIn;
    private boolean connected; // connection state
    private StringBuffer buff;
    private Thread ruokThread;
    private int ruokSynctransSwitcher;
    private boolean firstTimeConnect;
    private OutputStream logFile;
    private ArrayList scriptFiles;
    private boolean autoSyncTransaction;
    private DacBase returnObject;
    private int numberOfFilesGotOrMoved;
    private boolean firstTimeConnectSucc;
    private static Client client;
    private static DateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss ");
    private static int posNumber;
    private static PrintWriter logger;
    /**
     * Meyer/2003-02-17
     *         Get MixAndMatchVersion 
     */
    private String mixAndMatchVersion;
    /**
     * ZhaoHong /2003-06-18
     *         Get DaiShouVersion
     */
    //private String daiShouVersion; 

    /*
     * 2003-02-10 / Added By meyer
     * 资源  
     */
    private static ResourceBundle res = CreamToolkit.GetResource();
    private Integer maxZNoInSC;

    // Things could be changed.
    // 2003-02-10 Modified by Meyer: add 4 table to download
    static {
        dacForDownload = (new Class[] { hyi.cream.dac.PLU.class, hyi.cream.dac.Cashier.class, hyi.cream.dac.Reason.class, hyi.cream.dac.MixAndMatch.class, hyi.cream.dac.Payment.class, hyi.cream.dac.TaxType.class, hyi.cream.dac.Dep.class, hyi.cream.dac.Category.class, hyi.cream.dac.Store.class, hyi.cream.dac.MMGroup.class, hyi.cream.dac.CombDiscountType.class, hyi.cream.dac.CombGiftDetail.class, hyi.cream.dac.CombPromotionHeader.class, hyi.cream.dac.CombSaleDetail.class, hyi.cream.dac.DaiShouDef.class, hyi.cream.dac.SI.class, hyi.cream.dac.Rebate.class, hyi.cream.dac.DiscountType.class, hyi.cream.dac.TokenPayment.class });
        openLog();
    }
    public Integer getMaxZNoInSC() {
        return maxZNoInSC;
    }

    public static void openLog() {
        try {
            String logFilePath = CreamProperties.getInstance().getProperty("InlineClientLogFile");
            if (logFilePath != null)
                logger = new PrintWriter(new OutputStreamWriter(new FileOutputStream(logFilePath, true), CreamToolkit.getEncoding()));
            else
                logger = new PrintWriter(new OutputStreamWriter(new FileOutputStream("/root/inline.log", true), CreamToolkit.getEncoding()));
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    public static void closeLog() {
        if (logger != null) {
            logger.close();
            logger = null;
        }
    }

    public Client() throws InstantiationException {
        buff = new StringBuffer();
        firstTimeConnect = true;
        logFile = System.out;
        scriptFiles = new ArrayList();
        autoSyncTransaction = true;
        firstTimeConnectSucc = true;
        if (client != null)
            throw new InstantiationException();
        try {
            posNumber = Integer.parseInt(CreamProperties.getInstance().getProperty("TerminalNumber"));
            String autoSync = CreamProperties.getInstance().getProperty("AutoSyncTransaction");
            if (autoSync != null && autoSync.compareToIgnoreCase("NO") == 0)
                autoSyncTransaction = false;
            mixAndMatchVersion = CreamProperties.getInstance().getProperty("MixAndMatchVersion");
            if (mixAndMatchVersion == null)
                mixAndMatchVersion = "";
            //daiShouVersion = CreamProperties.getInstance().getProperty("DaiShouVersion");
            //if (daiShouVersion == null)
            //    daiShouVersion = "";
                
        } catch (NumberFormatException e) {
            e.printStackTrace(getLogger());
        }
    }

    public Client(String server, int port) throws InstantiationException {
        this();
        this.server = server; // remember the server and port connected
        this.port = port;
        client = this;
        // connect(server, port);
        start();// start a ruok thread
    }

    public static Client getInstance() {
        try {
            if (client == null)
                client = new Client();
            return client;
        } catch (InstantiationException e) {
            return null;
        }
    }

    //synchronized public boolean isConnected() {
    public boolean isConnected() {
        return connected;
    }

    //synchronized public void setConnected(boolean c) {
    public void setConnected(boolean c) {
        connected = c;
        if (!commandLineMode && connectOrig != connected) {
            connectOrig = connected;
            try {
                POSTerminalApplication.getInstance().getSystemInfo().fireEvent(new SystemInfoEvent(this));
            } catch (NullPointerException nullpointerexception) {
            }
        }
    }

    public synchronized Socket getSocket() {
        return socket;
    }

    public synchronized void setSocket(Socket s) {
        socket = s;
    }

    private void prompt(String s) {
        if (commandLineMode)
            System.out.println(s);
        log(s);
    }

    public static synchronized void log(String message) {
        //message = dateFormatter.format(new java.util.Date()) + message;
        if (posNumber == 0)
            message = dateFormatter.format(new Date()) + "[ ] " + message;
        else
            message = dateFormatter.format(new Date()) + "[" + posNumber + "] " + message;
        //System.out.println(message);
        logger.println(message);
        logger.flush();
    }

    public static PrintWriter getLogger() {
        return logger;
    }
    /*private void log(String message) {
        if (posNumber == 0)
            message = dateFormatter.format(new java.util.Date()) + "[ ] " +
                message;
        else
            message = dateFormatter.format(new java.util.Date()) + "[" +
                posNumber + "] " + message;
    
        //System.out.println(message);
        try {
            logFile.write(message.getBytes());
            logFile.write('\n');
            logFile.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Thread for keep sending "ruok?" (Are you ok?) and "synctrans" message to
     * server.
     */
    public void run() {
        ruokThread = Thread.currentThread();
        try {
            for (; !Thread.currentThread().isInterrupted(); Thread.sleep(SEND_RUOK_INTERVAL)) {
                if (!isConnected()) {
                    if (getSocket() != null)
                        closeConnection();
                    // if connection is broken, try connecting again
                    //Thread.sleep(SEND_RUOK_INTERVAL);
                    connect(server, port);
                    sendRuok();
                }
                if (++ruokSynctransSwitcher % 20 != 0 || !autoSyncTransaction)
                    sendRuok(); // it'll set connected to false if got trouble
                else
                    try { // every 20 * SEND_RUOK_INTERVAL millisecond synctrans once
                        sendSyncTransaction();
                    } catch (ClientCommandException e) {
                        e.printStackTrace(getLogger());
                    }
                    
            }

        } catch (InterruptedException e) {
            prompt("ruok thread is terminated.");
        }
    }

    public boolean connect(String server, int port) {
        try {
            try {
                setConnected(false);
                //prompt("Try connect to " + server + ":" + port + "...");
                socket = new Socket(server, port);
                socket.setSoTimeout(DEFAULT_TIMEOUT_INTERVAL);
                this.server = server; // remember the server and port connected
                this.port = port;
                out = new DataOutputStream(socket.getOutputStream());
                in = new DataInputStream(socket.getInputStream());
                dacOut = new DacObjectOutputStream(socket.getOutputStream());
                dacIn = new DacObjectInputStream(socket.getInputStream());
                //buffIn = new BufferedInputStream(in);
                objectOut = new ObjectOutputStream(new BufferedOutputStream(out));
                prompt("POS is connected to inline server.");
                setConnected(true);
                // if connected first time, get max transaction number from SC
                if (firstTimeConnectSucc) {
                    if (!commandLineMode)
                        adjustNextTransactionNumber(); // don't do this on command line mode
                    firstTimeConnectSucc = false;
                }
                return true;
            } catch (UnknownHostException e1) { // if the IP address of the host could not be determined.
                if (isConnected() || firstTimeConnect) {
                    e1.printStackTrace(getLogger());
                    closeConnection();
                    prompt("POS cannot connect to inline server.(" + e1 + ")");
                }
                setConnected(false);
                return false;
            } catch (IOException e2) { // if an I/O error occurs when creating the socket.
                if (isConnected() || firstTimeConnect) {
                    e2.printStackTrace(getLogger());
                    closeConnection();
                    prompt("POS cannot connect to inline server.(" + e2 + ")");
                }
                setConnected(false);
                return false;
            } catch (SecurityException e3) { // a security manager exists and its checkConnect method doesn't allow the operation
                if (isConnected() || firstTimeConnect) {
                    e3.printStackTrace(getLogger());
                    closeConnection();
                    prompt("POS cannot connect to inline server.(" + e3 + ")");
                }
            }
            setConnected(false);
            return false;
        } finally {
            firstTimeConnect = false;
        }
    }

    private void adjustNextTransactionNumber() {
        try {
            processCommand("queryMaxTransactionNumber");
            SequenceNumberGetter m = (SequenceNumberGetter) getReturnObject();
            if (m != null) {
                Integer maxTranNo = m.getMaxTransactionNumber();
                if (maxTranNo != null) {
                    int nextTranNoInSC = maxTranNo.intValue() + 1;
                    String nextTranNo = CreamProperties.getInstance().getProperty("NextTransactionNumber", "1");
                    int nextTranNoInPOS = Integer.parseInt(nextTranNo);
                    if (nextTranNoInSC > nextTranNoInPOS) {
                        CreamProperties.getInstance().setProperty("NextTransactionNumber", "" + nextTranNoInSC);
                        CreamProperties.getInstance().deposit();
                    }
                }
            }
            processCommand("queryMaxZNumber");
            m = (SequenceNumberGetter) getReturnObject();
            if (m != null)
                maxZNoInSC = m.getMaxZNumber();
        } catch (ClientCommandException clientcommandexception) {
        } catch (NumberFormatException numberformatexception) {
        }
    }

    public synchronized void closeConnection() {
        try {
            if (out != null) {
                out.close();
                out = null;
            }
            if (in != null) {
                in.close();
                in = null;
            }
            if (socket != null) {
                socket.close();
                socket = null;
            }
            objectIn = null;
            objectOut = null;
            dacOut = null;
            dacIn = null;
        } catch (IOException e) {
            prompt(e.toString());
            e.printStackTrace(getLogger());
        }
    }

    public synchronized void sendRuok() {
        if (!isConnected())
            return;
        try {
            int oldSoTimeout = socket.getSoTimeout();
            socket.setSoTimeout(RUOK_TIMEOUT_INTERVAL);
            //System.out.println("Client> Send ruok.");
            sendMessage("ruok?" + Store.getStoreID());
            out.writeInt(posNumber);
            String response = getMessage();
            socket.setSoTimeout(oldSoTimeout);
            boolean returnYes = response.startsWith("yes");
            if (response.startsWith("no")) {
                closeConnection();  // close connection silently
                setConnected(false);
                return;
            } else if (returnYes && !response.equals("yes")) {
                String piggybackCmd = response.substring(3);
                String priceChangeVersion = CreamProperties.getInstance().getProperty("PriceChangeVersion");
                if (priceChangeVersion == null || !priceChangeVersion.equalsIgnoreCase(piggybackCmd))
                    try {
                        // sleep for a while in case SC has not yet completely
                        // insert record into posdl_deltaplu
                        Thread.sleep(5000L);
                        processCommand("getPLUPriceChange");
                        CreamProperties.getInstance().setProperty("PriceChangeVersion", piggybackCmd);
                        CreamProperties.getInstance().deposit();
                    } catch (InterruptedException interruptedexception) {
                    } catch (ClientCommandException clientcommandexception) {
                    }
            }
            if (isConnected() && !returnYes)
                prompt("POS cannot connect to inline server.");
            setConnected(returnYes);
        } catch (SocketException e) {
            e.printStackTrace(getLogger());
            if (isConnected()) {
                prompt("POS cannot connect to inline server.(" + e + ")");
                closeConnection();
            }
            setConnected(false);
        } catch (IOException e) {
            e.printStackTrace(getLogger());
            if (isConnected()) {
                prompt("POS cannot connect to inline server.(" + e + ")");
                closeConnection();
            }
            setConnected(false);
        } catch (Exception e) {
            e.printStackTrace(getLogger());
            if (isConnected()) {
                prompt("POS cannot connect to inline server.(" + e + ")");
                closeConnection();
            }
            setConnected(false);
        }
    }

    public void sendSyncTransaction() throws ClientCommandException {
        if (!isConnected())
            return;
        try {
            int maxTransactionNumber = Transaction.getMaxTransactionNumber();
            int beginAndEndTranNumber[] = Transaction.getBeginAndEndTranNumber();
            int numOfTransactionRequested;
            int transactionNumbers[];
            synchronized (this) {
                if (out == null)    // for safe
                    return;
                sendMessage("synctrans");
                out.writeInt(posNumber);
                out.writeInt(maxTransactionNumber);
                out.writeInt(beginAndEndTranNumber[0]);
                out.writeInt(beginAndEndTranNumber[1]);
                numOfTransactionRequested = in.readInt();
                transactionNumbers = new int[numOfTransactionRequested];
                for (int i = 0; i < numOfTransactionRequested; i++)
                    transactionNumbers[i] = in.readInt();

            }
            for (int i = 0; i < numOfTransactionRequested; i++)
                try {
                    processCommand("putTransaction " + transactionNumbers[i]);
                } catch (ClientCommandException e) {
                    e.printStackTrace(getLogger());
                }

                // 上传未上传的Z/Shift
            Iterator iter = ShiftReport.queryNotUploadReport();
            if (iter != null)
                while (iter.hasNext()) {
                    ShiftReport shift = (ShiftReport) iter.next();
                    try {
                        processCommand("putShift " + shift.getZSequenceNumber() + " " + shift.getSequenceNumber());
                    } catch (ClientCommandException e) {
                        e.printStackTrace(getLogger());
                    }
                }
            iter = ZReport.queryNotUploadReport();
            if (iter != null)
                while (iter.hasNext()) {
                    ZReport z = (ZReport) iter.next();
                    String zSeq = "" + z.getSequenceNumber();
                    boolean b = false;
                    try {
                        processCommand("putZ " + zSeq);
                        b = true;
                    } catch (ClientCommandException e) {
                        e.printStackTrace(getLogger());
                    }
                    if (b) {
                        try {
                            processCommand("putDaiShouSales2 " + zSeq);
                            processCommand("putDaiShouSales " + zSeq);
                            processCommand("putDepSales " + zSeq);
                        } catch (ClientCommandException e) {
                            b = false;
                            e.printStackTrace(getLogger());
                        }
                        //if failed to upload DaiShouSales2 or DaiShouSales or DepSales
                        // set the upload flag of z to "2"
                        if (!b) {
                            z.setUploadState("2");
                            z.update();
                        }
                    }
                }
        } catch (IOException e) {
            e.printStackTrace(getLogger());
            throw new ClientCommandException(e.getMessage());
        }
    }

    public synchronized void sendMessage(String str) throws IOException {
        if (!isConnected()) {
            return;
        } else {
            out.write(str.getBytes(CreamToolkit.getEncoding()));
            out.write(10);
            out.flush();
            return;
        }
    }

    public synchronized String getMessage() throws IOException {
        if (!isConnected())
            return "";
        buff.setLength(0);
        do {
            int c = in.read();
            if (c == 10)
                return buff.toString();
            if (c == -1)
                return buff.toString();
            buff.append((char) c);
        } while (true);
    }

    public synchronized boolean sendObject(Object object) throws IOException {
        if (!isConnected())
            return false;
        sendMessage("putobject " + object.getClass().getName() + " 1");
        if (object instanceof DacBase) {
            dacOut.writeObject((DacBase) object);
            int oldSoTimeout = socket.getSoTimeout();
            socket.setSoTimeout(ACK_TIMEOUT_INTERVAL);
            boolean ret = getMessage().equals("OK");
            socket.setSoTimeout(oldSoTimeout);
            return ret;
        } else {
            objectOut.writeObject(object);
            objectOut.flush();
            //objectOut.reset();
            //Thread.sleep(3);    // for advoid a problem when doing a local test
            return true;
        }
    }

    public synchronized boolean sendObject(Object object[]) throws IOException {
        if (!isConnected())
            return false;
        if (object.length == 0)
            return true;
        sendMessage("putobject " + object[0].getClass().getName() + " " + object.length);
        boolean dacs = true;
        for (int i = 0; i < object.length; i++){
            if (object[i] instanceof DacBase) {
                dacOut.writeObject((DacBase) object[i]);
            } else {
                dacs = false;
                objectOut.writeObject(object[i]);
                objectOut.flush();
                //objectOut.reset();
            }
            //Thread.sleep(1);    // for advoid a problem when doing a local test
        }
        int oldSoTimeout = socket.getSoTimeout();
        socket.setSoTimeout(ACK_TIMEOUT_INTERVAL);
        boolean gotOK = getMessage().equals("OK");
        socket.setSoTimeout(oldSoTimeout);
        return !dacs || gotOK;
    }

    public synchronized int requestObjectsAndGetCount(String className) throws IOException {
        if (!isConnected())
            return 0;
        try {
            socket.setSoTimeout(DEFAULT_TIMEOUT_INTERVAL);
            sendMessage("getobject " + className);
            if (objectIn == null)
                objectIn = new ObjectInputStream(in);
            Integer numOfObject = (Integer) objectIn.readObject();
            if (numOfObject == null || numOfObject.intValue() == -1)
                return 0;
            else
                return numOfObject.intValue();
        } catch (ClassNotFoundException e) {
            e.printStackTrace(getLogger());
            return 0;
        } catch (Exception e) {
            //Bruce/20030505
            if (isConnected()) {
                prompt("POS cannot connect to inline server.(" + e + ")");
                closeConnection();
                e.printStackTrace(getLogger());
            }
            setConnected(false);
            if (e instanceof IOException)
                throw (IOException) e;
            else
                throw new IOException(e.toString());
        }
    }

    /**
    *
    * @ For Download Plu
    * @return
    */
    public synchronized int requestObjectsAndGetCount2(String className) throws IOException, ClientCommandException {
        if (!isConnected() || socket == null)
            return 0;
        try {
            sendMessage("getobject2 " + className);
            socket.setSoTimeout(LONG1_TIMEOUT_INTERVAL);
            int numOfObjects = in.readInt();
            log("requestObjectsAndGetCount2(): numofObjects=" + numOfObjects);
            socket.setSoTimeout(DEFAULT_TIMEOUT_INTERVAL);
            if (numOfObjects == -1) // get -1 if server failed
                throw new ClientCommandException("Get number of files failed");
            else
                return numOfObjects;
        } catch (IOException e) {
            e.printStackTrace(getLogger());
            throw e;
        }
    }

    public synchronized Object[] requestObjects(String className) {
        if (!isConnected())
            return null;
        try {
            sendMessage("getobject " + className);
            if (objectIn == null)
                objectIn = new ObjectInputStream(in);
            Integer numOfObject = (Integer) objectIn.readObject();
            if (numOfObject == null || numOfObject.intValue() == -1) // get -1 if server failed
                return null;
            Object object[] = new Object[numOfObject.intValue()];
            for (int i = 0; i < numOfObject.intValue(); i++)
                if (className.indexOf("hyi.cream.dac") != -1) {
                    object[i] = dacIn.readObject();
                    prompt("Get " + i + ":" + object[i]);
                } else {
                    object[i] = objectIn.readObject();
                }

            return object;
            // NOTE: caller has to sendMessage("OK") after finishing its work
        } catch (InterruptedIOException e1) {
            e1.printStackTrace(getLogger());
            return null;
        } catch (Exception e2) {
            e2.printStackTrace(getLogger());
        }
        return null;
    }

    public ArrayList getScriptFiles() {
        return scriptFiles;
    }

    public synchronized void requestFiles(String serverDir, String clientDir, boolean moveFiles) throws ClientCommandException {
        if (!isConnected())
            throw new ClientCommandException("Not connected with server");
        if (!clientDir.endsWith(File.separator))
            clientDir = clientDir + File.separator;
        scriptFiles.clear();
        try {
            if (moveFiles)
                sendMessage("movefile " + serverDir);
            else
                sendMessage("getfile " + serverDir);
            //Integer numOfFiles = (Integer)objectIn.readObject();
            numberOfFilesGotOrMoved = in.readInt();
            if (numberOfFilesGotOrMoved == -1)
                throw new ClientCommandException("Get number of files failed");
            for (int i = 0; i < numberOfFilesGotOrMoved; i++) {
                String fileName = getMessage();
                //Integer fileSize = (Integer)objectIn.readObject();
                int fileSize = in.readInt();
                prompt("Get " + clientDir + fileName + "  size=" + fileSize);
                if (fileName.endsWith(".sh"))
                    scriptFiles.add(clientDir + fileName);
                if (fileSize == -1)
                    throw new ClientCommandException("Get file size failed");
                byte fileContent[] = new byte[fileSize];
                //buffIn.read(fileContent);
                in.readFully(fileContent);
                OutputStream f = new BufferedOutputStream(new FileOutputStream(clientDir + fileName));
                f.write(fileContent);
                f.flush();
                f.close();
            }

        } catch (IOException e) {
            throw new ClientCommandException(e.getMessage());
        }
    }

    /**
     * Download master tables from SC, then return.
     * 
     * Caller has the resposibilites to send "OK"/"NG" back to server within synchronization
     * block of Client.getInstance().
     *
     * @param className Class name of a certain DAC.
     */
    public synchronized Object[] downloadDac0(String className) throws ClientCommandException {
        if (!isConnected())
            throw new ClientCommandException("Not connected with server now.");
        try {
            ArrayList dacs = new ArrayList();
            int numOfObjects = requestObjectsAndGetCount(className);
            if (numOfObjects == 0){
                //Meyer/2003-02-19/ sendMessage outside
                //sendMessage("OK");
                return null;
            }
            //DacBase dac = (DacBase)Class.forName(className).newInstance();
            for (int i = 0; i < numOfObjects; i++) {
                DacBase object;
                try {
                    object = dacIn.readObject();
                } catch (Exception e) {
                    e.printStackTrace(getLogger());
                    throw new ClientCommandException(className + " readObject() failed.");
                }
                dacs.add(object);
            }
            //sendMessage("OK");
            return dacs.toArray();
        } catch (IOException e) {
            e.printStackTrace(getLogger());
            throw new ClientCommandException(e.getMessage());
        }
    }

    /**
     * Query a DAC object from ServerThread. Given by a class name and a argument,
     * and it'll query by "getobject" protocol.
     * 
     * @param className DAC class name.
     * @param arg Query argument
     * @return DacBase Return null if nothing found, otherwise return a DAC object.
     */
    private synchronized DacBase querySingleObject(String className, String arg) {
        try {
            int numOfObjects = requestObjectsAndGetCount(className + " " + arg);
            if (numOfObjects == 0) {
                sendMessage("OK");
                return null;
            }
            DacBase object;
            try {
                object = dacIn.readObject();
                sendMessage("OK");
            } catch (Exception e) {
                return null;
            }
            return object;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Download master tables from SC, then insert into database.
     *
     * Policy: If something wrong, restore all original data backuped.
     *
     * @param className null if download all, otherwise the class name.
     */
    private synchronized void downloadDac(String className) throws ClientCommandException {
        boolean somethingWrong = false;
        String exceptionMessage = "";
        if (!isConnected())
            throw new ClientCommandException("Not connected with server now.");
        try {
            boolean backupFlag[] = new boolean[dacForDownload.length];
            for (int k = 0; k < dacForDownload.length; k++) {
                if (!"2".equals(mixAndMatchVersion) && (dacForDownload[k].equals(hyi.cream.dac.CombDiscountType.class) || dacForDownload[k].equals(hyi.cream.dac.CombGiftDetail.class) || dacForDownload[k].equals(hyi.cream.dac.CombPromotionHeader.class) || dacForDownload[k].equals(hyi.cream.dac.CombSaleDetail.class)) || className != null && !className.equals(dacForDownload[k].getName()))
                    continue;
                long timeStamp = System.currentTimeMillis();
                int numOfObjects = requestObjectsAndGetCount(dacForDownload[k].getName());
                if (numOfObjects == 0) {
                    prompt("It takes 0 second for download 0 record into " + dacForDownload[k].getName().substring(dacForDownload[k].getName().lastIndexOf('.') + 1));
                    sendMessage("OK");
                    continue;
                }
                DacBase dac = (DacBase) dacForDownload[k].newInstance();
                ////if (object == null) { // if no record it'll be a 0-size array, not a null
                ////    exceptionMessage = "request " + dacForDownload[k].getName() + " failed.";
                ////    somethingWrong = true;
                ////    sendMessage("OK");
                ////    break;
                ////}
                if (!dac.backupAll()) {
                    exceptionMessage = dacForDownload[k].getName() + ".backupAll() failed.";
                    somethingWrong = true;
                    sendMessage("OK");
                    break;
                }
                backupFlag[k] = true;
                if (!dac.deleteAll()) {
                    exceptionMessage = dacForDownload[k].getName() + ".deleteAll() failed.";
                    somethingWrong = true;
                    sendMessage("OK");
                    break;
                }
                //long t1,t2,t3,t4,tt;
                for (int i = 0; i < numOfObjects; i++) {
                    //t1 = System.currentTimeMillis();
                    DacBase object;
                    try {
                        object = dacIn.readObject();
                        //prompt("Get " + i + ":" + object);
                    } catch (Exception e) {
                        exceptionMessage = dacForDownload[k].getName() + " readObject() failed.";
                        somethingWrong = true;
                        break;
                    }
                    //t2 = System.currentTimeMillis();
                    //prompt("Insert...");
                    if (!object.insert(false)) {
                        exceptionMessage = dacForDownload[k].getName() + ".insert() failed.";
                        somethingWrong = true;
                        break;
                    }
                    //t3 = System.currentTimeMillis();
                    //t4 = t3;
                    if (numOfObjects % 20 == 0){
                        System.gc();
                        //t4 = System.currentTimeMillis();
                    }
                    //tt = t4 - t1;
                    ////prompt("net:" + (t2-t1) + "(" + ((t2-t1)*100.0/tt) + "%) " +
                    ////       "insert:" + (t3-t2) + "(" + ((t3-t2)*100.0/tt) + "%) " +
                    ////       "gc:" + (t4-t3) + "(" + ((t4-t3)*100.0/tt) + "%) ");

                    //prompt("insert " + object[i]);
                    ////if (!((DacBase)object[i]).insert(false)) {
                    ////    exceptionMessage = dacForDownload[k].getName() + ".insert() failed.";
                    ////    somethingWrong = true;
                    ////    break;
                    ////}
                }

                sendMessage("OK");
                timeStamp = System.currentTimeMillis() - timeStamp;
                if (somethingWrong)
                    break;
                prompt("It takes " + (double) timeStamp / 1000D + " seconds for download " + numOfObjects + " records into " + dac.getInsertUpdateTableName());
            }

            if (somethingWrong) {
                for (int k = 0; k < dacForDownload.length; k++)
                    if (backupFlag[k]) {
                        DacBase dac = (DacBase) dacForDownload[k].newInstance();
                        if (dac.restoreAll())
                            exceptionMessage = exceptionMessage + "\n" + dacForDownload[k].getName() + " restored.";
                        else
                            exceptionMessage = exceptionMessage + "\n" + dacForDownload[k].getName() + ".restoreAll() failed.";
                    }

                throw new ClientCommandException(exceptionMessage);
            }
        } catch (IOException e) {
            e.printStackTrace(getLogger());
            throw new ClientCommandException(e.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace(getLogger());
            throw new ClientCommandException(e.getMessage());
        } catch (InstantiationException e) {
            e.printStackTrace(getLogger());
            throw new ClientCommandException(e.getMessage());
        }
    }

    private boolean importIntoMySQL() {
        String line = "";
        Connection connection = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            Statement statement = connection.createStatement();
            BufferedReader fin = new BufferedReader(new InputStreamReader(new FileInputStream(CreamToolkit.getConfigDir() + "client.sql"), CreamToolkit.getEncoding()));
            while ((line = fin.readLine()) != null)
                statement.execute(line);
            fin.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return false;
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return false;
        } catch (SQLException e) {
            CreamToolkit.logMessage("Statement: " + line);
            e.printStackTrace(CreamToolkit.getLogger());
            return false;
        } finally {
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
        }
    }

    /**
     * Download DAC version 2.
     *
     * @return
     */
    private synchronized void downloadDac2(String className) throws ClientCommandException {
        boolean somethingWrong = false;
        String exceptionMessage = "";
        if (!isConnected())
            throw new ClientCommandException("Not connected with server now.");
        try {
            boolean backupFlag[] = new boolean[dacForDownload.length];
            for (int k = 0; k < dacForDownload.length; k++) {
                if (!"2".equals(mixAndMatchVersion) && (dacForDownload[k].equals(hyi.cream.dac.CombDiscountType.class) || dacForDownload[k].equals(hyi.cream.dac.CombGiftDetail.class) || dacForDownload[k].equals(hyi.cream.dac.CombPromotionHeader.class) || dacForDownload[k].equals(hyi.cream.dac.CombSaleDetail.class)) || className != null && !className.equals(dacForDownload[k].getName()))
                    continue;
                long timeStamp = System.currentTimeMillis();
                int numOfObjects = requestObjectsAndGetCount2(dacForDownload[k].getName());
                if (numOfObjects == 0) {
                    prompt("It takes 0 second for download 0 record into " + dacForDownload[k].getName().substring(dacForDownload[k].getName().lastIndexOf('.') + 1));
                    sendMessage("OK");
                    continue;
                }
                socket.setSoTimeout(LONG1_TIMEOUT_INTERVAL);
                int fileSize = in.readInt();
                log("downloadDac2(): fileSize=" + fileSize);
                if (fileSize < 0)
                    throw new ClientCommandException("Get file size failed");
                /*
                byte[] fileContent = new byte[fileSize];
                log("downloadDac2(): read file content...");
                in.readFully(fileContent);
                OutputStream f = new BufferedOutputStream(new FileOutputStream(
                    CreamToolkit.getConfigDir() + "client.sql"));
                log("downloadDac2(): write file content...");
                f.write(fileContent);
                */
                log("downloadDac2(): read/write file content...");
                int byteRead = 0;
                byte fileContent[] = new byte[4096];
                OutputStream f = new BufferedOutputStream(new FileOutputStream(CreamToolkit.getConfigDir() + "client.sql"));
                int bytes;
                for (; byteRead < fileSize; byteRead += bytes) {
                    bytes = in.read(fileContent);
                    f.write(fileContent, 0, bytes);
                }

                f.flush();
                f.close();
                socket.setSoTimeout(DEFAULT_TIMEOUT_INTERVAL);
                DacBase dac = (DacBase) dacForDownload[k].newInstance();
                ////if (object == null) { // if no record it'll be a 0-size array, not a null
                ////    exceptionMessage = "request " + dacForDownload[k].getName() + " failed.";
                ////    somethingWrong = true;
                ////    sendMessage("OK");
                ////    break;
                ////}
                log("downloadDac2(): backupAll...");
                if (!dac.backupAll()) {
                    exceptionMessage = dacForDownload[k].getName() + ".backupAll() failed.";
                    somethingWrong = true;
                    sendMessage("OK");
                    break;
                }
                backupFlag[k] = true;
                log("downloadDac2(): deleteAll...");
                if (!dac.deleteAll()) {
                    exceptionMessage = dacForDownload[k].getName() + ".deleteAll() failed.";
                    somethingWrong = true;
                    sendMessage("OK");
                    break;
                }
                // import into database...
                Runtime.getRuntime().freeMemory();
                log("downloadDac2(): import into MySQL...(FreeMem=" + Runtime.getRuntime().freeMemory() + ", TotalMem=" + Runtime.getRuntime().totalMemory() + ")");
                boolean exitCode = importIntoMySQL();
                if (!exitCode) {
                    somethingWrong = true;
                    sendMessage("OK");
                    break;
                }
                log("downloadDac2(): send ACK...");
                sendMessage("OK");
                timeStamp = System.currentTimeMillis() - timeStamp;
                if (somethingWrong)
                    break;
                prompt("It takes " + (double) timeStamp / 1000D + " seconds for download " + numOfObjects + " records into " + dac.getInsertUpdateTableName());
            }

            if (somethingWrong) {
                for (int k = 0; k < dacForDownload.length; k++)
                    if (backupFlag[k]) {
                        DacBase dac = (DacBase) dacForDownload[k].newInstance();
                        if (dac.restoreAll())
                            exceptionMessage = exceptionMessage + "\n" + dacForDownload[k].getName() + " restored.";
                        else
                            exceptionMessage = exceptionMessage + "\n" + dacForDownload[k].getName() + ".restoreAll() failed.";
                    }

                throw new ClientCommandException(exceptionMessage);
            }
        } catch (IOException e) {
            e.printStackTrace(getLogger());
            throw new ClientCommandException(e.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace(getLogger());
            throw new ClientCommandException(e.getMessage());
        } catch (InstantiationException e) {
            e.printStackTrace(getLogger());
            throw new ClientCommandException(e.getMessage());
        }
    }

    /**
     * Upload DAC object to SC.
     *
     * @return false if something wrong, true otherwise.
     */
    private synchronized boolean uploadDac(DacBase dac) {
        if (!isConnected())
            return false;
        try {
            if (dac instanceof ShiftReport) {
                ShiftReport shift = ((ShiftReport) dac).cloneForSC();
                return sendObject(shift);
            }
            if (dac instanceof ZReport) {
                ZReport z = ((ZReport) dac).cloneForSC();
                return sendObject(z);
            }
            if (dac instanceof CashForm) {
                CashForm cf = ((CashForm) dac).cloneForSC();
                return sendObject(cf);
            }
            if (dac instanceof Transaction) {
                //Transaction tran = ((Transaction)dac).cloneForSC();
                Object tran[] = ((Transaction) dac).cloneForSC();
                // dac contains Transaction and LineItems
                return sendObject(tran);
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace(getLogger());
        }
        return false;
    }

    /**
     * Upload DAC object to SC.
     *
     * @return false if something wrong, true otherwise.
     */
    private synchronized boolean uploadDac(Object dac[]) {
        if (!isConnected())
            return false;
        try {
            if (dac == null || dac.length == 0)
                return true;

            //if (dac[0] instanceof DepSales 
            //    || dac[0] instanceof DaishouSales
            //    || dac[0] instanceof DaiShouSales2) {
            return sendObject(dac);
        } catch (IOException e) {
            e.printStackTrace(getLogger());
        }
        return false;
    }

    /**
     * Upload DAC objects to SC. By "putobject2" protocol.
     *
     * @param dacs the format is:
     *      "[class_name]\t[first_field]\t[second_field]\t...\n" +
     *      "[class_name]\t[first_field]\t[second_field]\t...\n" + 
     *      ...
     *
     * @return false if something wrong, true otherwise.
     */
    private synchronized boolean uploadDac2(String dacs) {
        if (!isConnected())
            return false;
        try {
            sendMessage("putobject2");
            objectOut.writeObject(dacs);
            objectOut.flush();
            //if (sendObjectCallBack != null)
            //    sendObjectCallBack.callBack("数据传送完毕，等候后台回应，请稍侯...");
            return getMessage().equals("OK");
        } catch (IOException e) {
            prompt("uploadDac2: " + e.getMessage());
        }
        return false;
    }

    private String getHelp() {
        return "Inline Client Version 1.3 (Copyright 2001-2003, Hongyuan Software Co.)\n"
            + "Available commands:\n"
            + "    ? | help : 帮助\n"
            + "    quit : 退出\n"
            + "    getAll : 下传所有主表\n"
            + "    getPLU : 下传单品表\n"
//          + "    getPLUPriceChange : 下传单品即时变价表\n"
            + "    getCashier : 下传收银员表\n"
            + "    getReason : 下传原因代码和名称对照表\n"
            + "    getTokenPayment : 下传礼劵定义表\n"    
            + "    getPayment : 下传支付定义表\n"
            + "    getTaxType : 下传税别定义表\n"
            + "    getCategory : 下传分类表\n"
            + "    getDep : 下传税务分类表\n"
            + "    getStore : 下传门市基本资料表\n"
            + "    getDaiShouDef : 下传代收格式定义资料\n"
            + (mixAndMatchVersion.equals("2")
                ? "    getCombDiscountType : 下传组合促销折扣定义表\n"
                + "    getCombPromotionHeader : 下传组合促销定义头表\n"
                + "    getCombSaleDetail : 下传组合促销销售明细表\n"
                + "    getCombGiftDetail : 下传组合促销赠品定义表\n"
                : "    getMixAndMatch : 下传组合促销定义表\n" 
                + "    getMMGroup : 下传组合促销资料表\n")
            + "    getSI : 下传折扣定义表\n"
//          + "    getDiscountType : 下传单品折扣率定义表\n"
//          + "    getRebate: 下传还元金 \n"
//          + "    queryMember {member no} : 查询客户信息\n"
//          + "    queryLimitQtySold {Item Number} {1 or -1} [Member ID] : 查询与更新商品限量销售信息\n"
            + "    queryMaxTransactionNumber : 查询后台最大交易序号\n"
            + "    queryMaxZNumber : 查询后台最大Z帐序号\n"
            + "    putShift {z number} {shift number} : 上传收银员交班表\n"
//          + "    putCashForm {z number} {shift number} : 上传收银员现金清点单\n"
            + "    putZ {z number} : 上传日结表\n"
            + "    putDepSales {z number} : 上传税务分类帐表\n"
            + "    putDaishouSales {z number} : 上传代售明细表\n"
            + "    putDaiShouSales2 {z number} : 上传代收明细表\n"
            + "    putTransaction {transaction number} : 上传某交易明细\n"
//          + "    putInventory : 上传未上传盘点数据\n"
//          + "    putAllInventory : 上传所有盘点数据\n"
            + "    getFile {server directory} {client directory} : 下传档案\n"
            + "    moveFile {server directory} {client directory} : 下传档案并删除服务器端档案\n"
            + "    syncTransaction : 补传漏上传的交易";
    }

    private String[] getCommandAndArguments(String origLine) {
        List list = new ArrayList();
        for (StringTokenizer t = new StringTokenizer(origLine, " \t"); t.hasMoreTokens(); list.add(t.nextToken()))
            ;
        //String[] retStrings = new String[list.size() + 2]; // add another 2 null into it for checking with null value
        String retStrings[] = new String[list.size()];
        System.arraycopy(((Object) (list.toArray())), 0, retStrings, 0, list.size());
        return retStrings;
    }

    /**
     * Process a inline client command.
     * 
     * @param line Command string with arguments.
     * @throws ClientCommandException
     */
    public synchronized void processCommand(String line) throws ClientCommandException {
        log("TX:" + line);
        String args[] = getCommandAndArguments(line);
        if (args == null || args.length == 0 || args[0] == null)
            return;
        if (args[0].equals("?") || args[0].equalsIgnoreCase("help"))
            throw new ClientCommandException(getHelp());
        if (args[0].equalsIgnoreCase("quit")) {
            try {
                sendMessage("quit");
            } catch (IOException e) {
                e.printStackTrace(getLogger());
            }
            closeConnection();
            ruokThread.interrupt();
            return;
        }
        if (args[0].compareToIgnoreCase("putZ") == 0) {
            if (args.length != 2)
                throw new ClientCommandException("Please give a z number.");
            try {
                ZReport z = ZReport.queryBySequenceNumber(Integer.valueOf(args[1]));
                if (z != null) {
                    if (uploadDac(z)) {
                        z.setUploadState("1");
                        log("Z uploaded. no=" + z.getSequenceNumber());
                        z.update();
                    } else {
                        z.setUploadState("2");
                        log("Z uploaded failed. no=" + z.getSequenceNumber());
                        z.update();
                        throw new ClientCommandException("Send z failed.");
                    }
                } else {
                    throw new ClientCommandException("Cannot find the given z.");
                }
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Invalid z number.");
            }
        } else if (args[0].compareToIgnoreCase("putShift") == 0) {
            if (args.length != 3)
                throw new ClientCommandException("Please give a z and a shift number.");
            try {
                ShiftReport shift = ShiftReport.queryByZNumberAndShiftNumber(Integer.valueOf(args[1]), Integer.valueOf(args[2]));
                if (shift != null) {
                    if (uploadDac(shift)) {
                        shift.setUploadState("1");
                        log("Shift uploaded. z=" + shift.getZSequenceNumber() + ", shift=" + shift.getSequenceNumber());
                        shift.update();
                    } else {
                        shift.setUploadState("2");
                        log("Shift uploaded failed. z=" + shift.getZSequenceNumber() + ", shift=" + shift.getSequenceNumber());
                        shift.update();
                        throw new ClientCommandException("Send shift failed.");
                    }
                } else {
                    throw new ClientCommandException("Cannot find the given shift.");
                }
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Invalid shift number.");
            }
        } else if (args[0].compareToIgnoreCase("putCashForm") == 0) {
            if (args.length != 3)
                throw new ClientCommandException("Please give a z and a shift number.");
            try {
                CashForm cf = CashForm.queryByZNumberAndShiftNumber(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
                if (cf != null) {
                    if (uploadDac(cf)) {
                        cf.setUploadState("1");
                        log("CashForm uploaded. z=" + cf.getZSequenceNumber() + ", shift=" + cf.getSequenceNumber());
                        cf.update();
                    } else {
                        cf.setUploadState("2");
                        log("CashForm uploaded failed. z=" + cf.getZSequenceNumber() + ", shift=" + cf.getSequenceNumber());
                        cf.update();
                        throw new ClientCommandException("Send CashForm failed.");
                    }
                } else {
                    throw new ClientCommandException("Cannot find the given CashForm.");
                }
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Invalid CashForm number.");
            }
        } else if (args[0].compareToIgnoreCase("putTransaction") == 0) {
            if (args.length != 2)
                throw new ClientCommandException("Please give a transaction number.");
            Transaction tran = Transaction.queryByTransactionNumberNoMaxLineItemLimit(args[1], false);
            if (tran != null) {
                if (!uploadDac(tran))
                    throw new ClientCommandException("Send transaction failed.");
            } else {
                throw new ClientCommandException("Cannot find the given transaction.");
            }
        } else if (args[0].compareToIgnoreCase("putDepSales") == 0) {
            if (args.length != 2)
                throw new ClientCommandException("Please give a z number.");
            try {
                Iterator iter = DepSales.queryBySequenceNumber(Integer.valueOf(args[1]));
                if (iter == null)
                    throw new ClientCommandException("Cannot find the given DepSales.");
                Object depSales[] = DepSales.cloneForSC(iter);
                if (depSales != null) {
                    boolean success = uploadDac(depSales);
                    Iterator iter2 = DepSales.queryBySequenceNumber(Integer.valueOf(args[1]));
                    if (iter2 != null) {
                        DepSales ds;
                        for (; iter2.hasNext(); ds.update()) {
                            ds = (DepSales) iter2.next();
                            ds.setUploadState(success ? "1" : "2");
                        }

                    }
                    if (!success)
                        throw new ClientCommandException("Send DepSales failed.");
                } else {
                    throw new ClientCommandException("Cannot find the given DepSales.");
                }
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Invalid z number.");
            }
        } else if (args[0].compareToIgnoreCase("putDaishouSales") == 0) {
            if (args.length != 2)
                throw new ClientCommandException("Please give a z number.");
            try {
                Iterator iter = DaishouSales.getCurrentDaishouSales(Integer.parseInt(args[1]));
                if (iter == null)
                    throw new ClientCommandException("Cannot find the given DaishouSales.");
                Object ds[] = DaishouSales.cloneForSC(iter);
                if (ds != null) {
                    boolean success = uploadDac(ds);
                    Iterator iter2 = DaishouSales.getCurrentDaishouSales(Integer.parseInt(args[1]));
                    if (iter2 != null) {
                        DaishouSales d;
                        for (; iter2.hasNext(); d.update()) {
                            d = (DaishouSales) iter2.next();
                            d.setTcpflg(success ? "1" : "2");
                        }

                    }
                    if (!success)
                        throw new ClientCommandException("Send DaishouSales failed.");
                } else {
                    throw new ClientCommandException("Cannot find the given DaishouSales.");
                }
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Invalid z number.");
            }
        } else if (args[0].compareToIgnoreCase("putDaishouSales2") == 0) {
//          代收资料上传   
            if (args.length != 2)
                throw new ClientCommandException("Please give a z number.");
            try {
                Iterator iter = DaiShouSales2.getCurrentDaishouSales(Integer.parseInt(args[1]));
                if (iter == null){
                    return; //因为有可能没有资料，无资料视为正常
                    //throw new ClientCommandException("Cannot find the given DaishouSales.");
                }
                Object ds[] = DaiShouSales2.cloneForSC(iter);
                if (ds != null) {
                    boolean success = uploadDac(ds);
//                  Iterator iter2 = DaiShouSales2.getCurrentDaishouSales(Integer.parseInt(args[1]));
//                  if (iter2 != null) {
//                      while (iter2.hasNext()) {
//                          DaiShouSales2 d = (DaiShouSales2) iter2.next();
//                          d.setUploadState((success) ? "1" : "2");
//                          d.update();
//                      }
//                  }
                    if (!success)
                        throw new ClientCommandException("Send DaiShouSales2 failed.");
                } else {
                    throw new ClientCommandException("Cannot find the given DaiShouSales2.");
                }
            } catch (NumberFormatException e) {
                throw new ClientCommandException("Invalid z number.");
            }
        } else if (args[0].compareToIgnoreCase("putInventory") == 0) {
            String invData = Inventory.getUnsentTabSeperatedData();
            if (invData != null && invData.trim().length() != 0) {
                if (uploadDac2(invData)) {
                    Inventory.updateUploadState("1");
                } else {
                    Inventory.updateUploadState("2");
                    throw new ClientCommandException("Send inventory data failed.");
                }
            } else {
                throw new ClientCommandException("Cannot find inventory data.");
            }
        } else if (args[0].compareToIgnoreCase("putAllInventory") == 0) {
            String invData = Inventory.getTabSeperatedData();
            if (invData != null && invData.trim().length() != 0) {
                if (uploadDac2(invData)) {
                    Inventory.updateUploadState("1");
                } else {
                    Inventory.updateUploadState("2");
                    throw new ClientCommandException("Send inventory data failed.");
                }
            } else {
                throw new ClientCommandException("Cannot find inventory data.");
            }
        } else if (args[0].compareToIgnoreCase("getAll") == 0)
            downloadDac2(null);
        else if (args[0].compareToIgnoreCase("getPLU") == 0)
            downloadDac2("hyi.cream.dac.PLU");
        else if (args[0].compareToIgnoreCase("getPLUPriceChange") == 0) {
            Object plus[] = downloadDac0("hyi.cream.dac.PLUPriceChange");
            if (plus != null && !PLU.importSomePLUs(plus)) {
                //Meyer/2003-02-19/ send message
                try {
                    sendMessage("NG");
                } catch (IOException e) {
                    e.printStackTrace(getLogger());
                }
                throw new ClientCommandException("Some price change update failed.");
            }
            //Meyer/2003-02-19/ send message
            try {
                sendMessage("OK");
            } catch (IOException e) {
                e.printStackTrace(getLogger());
            }
        } else if (args[0].compareToIgnoreCase("getCashier") == 0)
            downloadDac2("hyi.cream.dac.Cashier");
        else if (args[0].compareToIgnoreCase("getDaiShouDef") == 0)
            downloadDac2("hyi.cream.dac.DaiShouDef");
        else if (args[0].compareToIgnoreCase("getReason") == 0)
            downloadDac2("hyi.cream.dac.Reason");
        else if (args[0].compareToIgnoreCase("getTokenPayment") == 0)
            downloadDac2("hyi.cream.dac.TokenPayment");
        else if (args[0].compareToIgnoreCase("getMixAndMatch") == 0)
            downloadDac2("hyi.cream.dac.MixAndMatch");
        else if (args[0].compareToIgnoreCase("getSI") == 0)
            downloadDac2("hyi.cream.dac.SI");
        else if (args[0].compareToIgnoreCase("getPayment") == 0) {
            downloadDac2("hyi.cream.dac.Payment");
            Payment.insertCashIfNotExist();
        } else if (args[0].compareToIgnoreCase("getTaxType") == 0)
            downloadDac2("hyi.cream.dac.TaxType");
        else if (args[0].compareToIgnoreCase("getCategory") == 0)
            downloadDac2("hyi.cream.dac.Category");
        else if (args[0].compareToIgnoreCase("getDep") == 0)
            downloadDac2("hyi.cream.dac.Dep");
        else if (args[0].compareToIgnoreCase("getStore") == 0)
            downloadDac2("hyi.cream.dac.Store");
        else if (args[0].compareToIgnoreCase("getMMGroup") == 0)
            downloadDac2("hyi.cream.dac.MMGroup");
        else if (args[0].compareToIgnoreCase("getDiscountType") == 0)
            downloadDac2("hyi.cream.dac.DiscountType");
        else if (args[0].compareToIgnoreCase("getRebate") == 0)
            downloadDac2("hyi.cream.dac.Rebate");
        else if (args[0].compareToIgnoreCase("queryMember") == 0) {
            setReturnObject(null);
            if (args.length != 2)
                throw new ClientCommandException("Please give a member id, telphone number or an identity card ID.");
            Member m = (Member) querySingleObject("hyi.cream.dac.Member", args[1]);
            if (m == null)
                prompt("Cannot find this member.");
            else if (commandLineMode) {
                prompt("Member ID: " + m.getID());
                prompt("Member Card ID: " + m.getMemberCardID());
                prompt("Member Name: " + m.getName());
                prompt("Telphone Number: " + m.getTelephoneNumber());
                prompt("Identity ID: " + m.getIdentityID());
                prompt("Action Rebate: " + m.getMemberActionBonus());
            } else {
                setReturnObject(m);
            }
            //zhaohong/2003-06-10 查询限量销售数据
        } else if (args[0].compareToIgnoreCase("queryLimitQtySold") == 0) {
            setReturnObject(null);
            String sArgs = "";
            if (args.length < 3 || args.length > 4)
                throw new ClientCommandException("Usage: queryLimitQtySold {Item Number} {1 or -1} [Member ID]");
            for (int i = 1; i < args.length; i++)
                sArgs = sArgs + args[i] + " ";

            LimitQtySold lqs = (LimitQtySold) querySingleObject("hyi.cream.dac.LimitQtySold", sArgs);
            if (commandLineMode) {
                if (lqs != null) {
                    prompt("LimitPrice: " + lqs.getLimitPrice());
                    prompt("LimitRebateRate: " + lqs.getLimitRebateRate());
                    prompt("QtySold: " + lqs.getQtySold());
                    prompt("Member Quantity Sold: " + lqs.getMemberQtySold());
                    prompt("IsSalable: " + lqs.isSaleable());
                }
            } else {
                setReturnObject(lqs);
            }
        } else if (args[0].compareToIgnoreCase("queryMaxTransactionNumber") == 0) {
            setReturnObject(null);
            SequenceNumberGetter m = (SequenceNumberGetter) querySingleObject("hyi.cream.dac.SequenceNumberGetter", posNumber + ",tran");
            if (m == null)
                prompt("Cannot find.");
            else if (commandLineMode)
                prompt("Max transaction number: " + m.getMaxTransactionNumber());
            else
                setReturnObject(m);
        } else if (args[0].compareToIgnoreCase("queryMaxZNumber") == 0) {
            setReturnObject(null);
            SequenceNumberGetter m = (SequenceNumberGetter) querySingleObject("hyi.cream.dac.SequenceNumberGetter", posNumber + ",z");
            if (m == null)
                prompt("Cannot find.");
            else if (commandLineMode)
                prompt("Max Z number: " + m.getMaxZNumber());
            else
                setReturnObject(m);
            //Meyer/2003-02-10
            //新增新版组合促销用表
        } else if (args[0].compareToIgnoreCase("getCombDiscountType") == 0)
            downloadDac2("hyi.cream.dac.CombDiscountType");
        else if (args[0].compareToIgnoreCase("getCombGiftDetail") == 0)
            downloadDac2("hyi.cream.dac.CombGiftDetail");
        else if (args[0].compareToIgnoreCase("getCombPromotionHeader") == 0)
            downloadDac2("hyi.cream.dac.CombPromotionHeader");
        else if (args[0].compareToIgnoreCase("getCombSaleDetail") == 0)
            downloadDac2("hyi.cream.dac.CombSaleDetail");
        else if (args[0].compareToIgnoreCase("getFile") == 0 || args[0].compareToIgnoreCase("moveFile") == 0) {
            if (args[1] == null || args[2] == null)
                throw new ClientCommandException("Please give a server and client directory name.");
            requestFiles(args[1], args[2], args[0].compareToIgnoreCase("moveFile") == 0);
        } else if (args[0].compareToIgnoreCase("syncTransaction") == 0)
            sendSyncTransaction();
        else
            throw new ClientCommandException("Invalid command.");
    }

    public void enterCommandLineMode() {
        commandLineMode = true;
        prompt("Inline Client Version 1.3 (Copyright 2001-2003, Hongyuan Software Co.)");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            do {
                System.out.print("> ");
                try {
                    String line = reader.readLine();
                    processCommand(line);
                    if (line.equalsIgnoreCase("quit")) {
                        closeConnection();
                        ruokThread.interrupt();
                        return;
                    }
                    if (!line.equals(""))
                        System.out.println("OK.");
                } catch (ClientCommandException e2) {
                    prompt(e2.getMessage());
                }
            } while (true);
        } catch (IOException e) {
            e.printStackTrace(getLogger());
        }
    }

    /*
     * Download Master table
     */
    public void downloadMaster() throws ClientCommandException {
        Indicator indicator = POSTerminalApplication.getInstance().getMessageIndicator();
        Client client = getInstance();
        //Client.getInstance().processCommand("getAll");
        indicator.setMessage(res.getString("MasterDownloading") + "(Cashier)");
        client.processCommand("getCashier");
        indicator.setMessage(res.getString("MasterDownloading") + "(Reason)");
        client.processCommand("getReason");
        //indicator.setMessage(res.getString("MasterDownloading") + "(DeltaPlu)");
        //client.processCommand("getPLUPriceChange");
        if ("2".equals(mixAndMatchVersion)) {
            indicator.setMessage(res.getString("MasterDownloading") + "(Combination_Discount_Type)");
            client.processCommand("getCombDiscountType");
            indicator.setMessage(res.getString("MasterDownloading") + "(Combination_Gift_Detail)");
            client.processCommand("getCombGiftDetail");
            indicator.setMessage(res.getString("MasterDownloading") + "(Combination_Promotion_Header)");
            client.processCommand("getCombPromotionHeader");
            indicator.setMessage(res.getString("MasterDownloading") + "(Combination_Sale_Detail)");
            client.processCommand("getCombSaleDetail");
        } else {
            indicator.setMessage(res.getString("MasterDownloading") + "(MixAndMatch)");
            client.processCommand("getMixAndMatch");
            indicator.setMessage(res.getString("MasterDownloading") + "(MMGroup)");
            client.processCommand("getMMGroup");
        }
        indicator.setMessage(res.getString("MasterDownloading") + "(Payment)");
        client.processCommand("getPayment");
        indicator.setMessage(res.getString("MasterDownloading") + "(TaxType)");
        client.processCommand("getTaxType");
        indicator.setMessage(res.getString("MasterDownloading") + "(Category)");
        client.processCommand("getCategory");
        indicator.setMessage(res.getString("MasterDownloading") + "(Dep)");
        client.processCommand("getDep");
        indicator.setMessage(res.getString("MasterDownloading") + "(Store)");
        client.processCommand("getStore");
        indicator.setMessage(res.getString("MasterDownloading") + "(PLU)");
        client.processCommand("getPLU");
        indicator.setMessage(res.getString("MasterDownloading") + "(DaiShouDef)");
        client.processCommand("getDaiShouDef");
        indicator.setMessage(res.getString("MasterDownloading") + "(SI)");
        client.processCommand("getSI");
        //indicator.setMessage(res.getString("MasterDownloading") + "(Rebate)");
        //client.processCommand("getRebate");
        //indicator.setMessage(res.getString("MasterDownloading") + "(DiscountType)");
        //client.processCommand("getDiscountType");
        indicator.setMessage(res.getString("MasterDownloading") + "(TokenPayment)");
        client.processCommand("getTokenPayment");
        // re-cache
        Store.createCache();
        Cashier.createCache();
        Dep.createCache();
        Payment.createCache();
        Reason.createCache();
        TokenPayment.createCache();
        TaxType.createCache();
    }

    /**
     * Command inline mode Inline Client.
     */
    public static void main(String args[]) throws InterruptedException {
        if (args.length != 2) {
            System.out.println("Please give me a server address and a port number.");
            System.exit(1);
        }
        try {
            Client.commandLineMode = true;
            Client c = new Client(args[0], Integer.parseInt(args[1]));
            //if (!c.isConnected())
            //    System.exit(2);
            c.enterCommandLineMode();
            System.exit(0);
        } catch (NumberFormatException e) {
            System.out.println("Please give me a legal port number.");
            System.exit(1);
        } catch (InstantiationException e) {
            System.out.println("Cannot create inline client.");
            System.exit(1);
        }
    }

    /**
     * Get a cached DAC object for caller.
     * 
     * @return The cached DAC object.
     */
    public DacBase getReturnObject() {
        return returnObject;
    }

    /**
     * Set a cached DAC object for caller.
     *
     * @param returnObject The cached DAC object.
     */
    public void setReturnObject(DacBase returnObject) {
        this.returnObject = returnObject;
    }

    /**
     * Return the number of files got or moved last time.
     * 
     * @return Return the number of files got or moved last time.
     */
    public int getNumberOfFilesGotOrMoved() {
        return numberOfFilesGotOrMoved;
    }

}

/*******************************************************************************
 * DECOMPILATION REPORT ***
 * 
 * DECOMPILED FROM: D:\workspace7\CStorePOS\classes\cream.jar
 * 
 * 
 * TOTAL TIME: 235 ms
 * 
 * 
 * JAD REPORTED MESSAGES/ERRORS:
 * 
 * 
 * EXIT STATUS: 0
 * 
 * 
 * CAUGHT EXCEPTIONS:
 * 
 ******************************************************************************/
