package hyi.cream.inline;

import java.io.*;
import java.util.*;
import hyi.cream.dac.DacBase;

public class DacObjectInputStream {

    private ObjectInputStream ois;
    private DataInputStream dis;

    public DacObjectInputStream(InputStream in)
        throws IOException, StreamCorruptedException {
        this.ois = new ObjectInputStream(in);
        this.dis = new DataInputStream(in);
    }

    public DacBase readObject() throws ClassNotFoundException, IOException {
        String dacClassName = (String)ois.readObject();
        //System.out.println("classname=" + dacClassName);

        ////DacBase dac = new hyi.cream.dac.PLU();
        DacBase dac;
        try {
            dac = (DacBase)Class.forName(dacClassName).newInstance();
        } catch (Exception e) {
            throw new ClassNotFoundException(e.toString());
        }

        Map fieldMap = dac.getFieldMap();
        fieldMap.clear();
        int fieldCount = dis.readInt();
//System.out.println("fieldCount=" + fieldCount);
        try {
            for (int i = 0; i < fieldCount; i++) {
                String fieldName = (String)ois.readObject();
//System.out.println(i + " fieldName=" + fieldName);
                String fieldClassName = (String)ois.readObject();
//System.out.println(i + " fieldClassName=" + fieldClassName);
                Object value;
                long time;
                String valueString;
                if (fieldClassName.indexOf("String") != -1) {
                    valueString = (String)ois.readObject();
                    if (valueString.equals("^null^"))
                        value = null;
                    else
                        value = valueString;
//System.out.println(i + " fieldValue=" + valueString);
                } else if (fieldClassName.indexOf("Integer") != -1) {
                    valueString = (String)ois.readObject();
                    value = new Integer(valueString);
                } else if (fieldClassName.indexOf("BigDecimal") != -1) {
                    valueString = (String)ois.readObject();
                    value = new java.math.BigDecimal(valueString);
                } else if (fieldClassName.indexOf("java.sql.Date") != -1) {
                    time = dis.readLong();
                    value = new java.sql.Date(time);
                } else if (fieldClassName.indexOf("Timestamp") != -1) {
                    time = dis.readLong();
                    value = new java.sql.Timestamp(time);
                } else if (fieldClassName.indexOf("Time") != -1) {
                    time = dis.readLong();
                    value = new java.sql.Time(time);
                } else if (fieldClassName.indexOf("java.util.Date") != -1) {
                    time = dis.readLong();
                    value = new java.util.Date(time);
                } else {
                    valueString = (String)ois.readObject();
                    //System.out.println(i + " fieldValue=" + valueString);
                    if (valueString.equals("^null^"))
                        value = null;
                    else
                        value = Class.forName(fieldClassName).getConstructor(
                            new Class[] {String.class}).newInstance(
                            new Object[] {valueString});
                }

/*
                if (fieldClassName.indexOf("Date") != -1 ||
                    fieldClassName.indexOf("Timestamp") != -1 ||
                    fieldClassName.indexOf("Time") != -1) {
                    long time = dis.readLong();
                    //System.out.println(i + " fieldValue=" + time);
                    value = Class.forName(fieldClassName).getConstructor(
                        new Class[] {long.class}).newInstance(
                        new Object[] {new Long(time)});
                } else {
                    String valueString = (String)ois.readObject();
                    //System.out.println(i + " fieldValue=" + valueString);
                    if (valueString.equals("^null^"))
                        value = null;
                    else
                        value = Class.forName(fieldClassName).getConstructor(
                            new Class[] {String.class}).newInstance(
                            new Object[] {valueString});
                }
*/
                fieldMap.put(fieldName, value);
            }
            return dac;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}