package hyi.cream.inline;

import java.text.*;
import java.sql.*;
import java.util.*;
import java.math.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;

/**
 * PostProcessor for Familymart.
 *
 * @author Meyer
 * @version 1.0
 */
public class FamilymartPostProcessor extends PostProcessorAdapter {

    /**
     * /Meyer/2003-02-10
     *		1、在CStorePostProcess基础上建立新类
     * 		2、增加处理"现金清点单"的处理方法	
     * 
     * 
     */
    transient public static final String VERSION = "1.0";

    static final int DATE_ACCDATE         = 200;
    static final int DATE_BIZDATE         = DATE_ACCDATE + 1;
    static final int DATE_INFDATE         = DATE_ACCDATE + 2;
    static private DateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd");

    private static java.util.Date getAccountingDate() {
        java.util.Date accountingDate = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(
            	"SELECT sp_value FROM system_parameters WHERE sp_id='0004'");
            if (resultSet.next()) {

        		accountingDate = resultSet.getDate("sp_value"); 		
				System.out.println("############ found" + accountingDate);
            }
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        }finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace(Server.getLogger());
            }
        }
		System.out.println("############ 1" + accountingDate);
        if (accountingDate == null) {
        	accountingDate = new java.util.Date();
        }
        System.out.println("############# 2" + accountingDate);
        return accountingDate;
    }


    public boolean afterReceivingShiftReport(ShiftReport shift) {
        shift.setAccountingDate(getAccountingDate());
        shift.deleteByPrimaryKey();
        
        if (!shift.insert(false)) {
            Server.log("Shift inserted failed (pos=" + shift.getTerminalNumber() +
                ",z=" +shift.getZSequenceNumber() +
                ",shift=" +shift.getSequenceNumber() + ").");
            return false;
        }
        Server.log("Shift inserted (date=" + dateFormatter.format(shift.getAccountingDate()) +
            ",pos=" + shift.getTerminalNumber() +
            ",z=" +shift.getZSequenceNumber() +
            ",shift=" +shift.getSequenceNumber() + ").");
        return true;
    }

    public boolean afterReceivingZReport(ZReport z) {
        boolean returnValue = true;

        z.setAccountingDate(getAccountingDate());
        z.deleteByPrimaryKey();

        if (!z.insert(false)) {
            Server.log("Z inserted failed (pos=" + z.getTerminalNumber() +
                    ",z=" + z.getSequenceNumber() + ").");
            return false;
            
        }
        Server.log("Z inserted (date=" + dateFormatter.format(z.getAccountingDate()) +
            ",pos=" + z.getTerminalNumber() +
            ",z=" + z.getSequenceNumber() + ").");

        return returnValue;
    }

    private java.util.Date getBussinessDate() {
        String storeID = getStoreID();//CreamToolkit.getConfiguration().getProperty("StoreID");
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String select = "SELECT * FROM calendar";
        String where = " WHERE storeID = '" + storeID + "' AND dateType = '" + "0'";
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(select + where);
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace(Server.getLogger());
            }
            return new java.util.Date();
        }
        HashMap newMap = null;
        try {
            if (resultSet.next()) {
                newMap = new HashMap();
                ResultSetMetaData metaData = resultSet.getMetaData();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object obj = metaData.getColumnName(i);
                    newMap.put(obj, resultSet.getObject(i));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
        }
        java.util.Date accDate = new java.util.Date();
        if (newMap == null)
            return accDate;
        if (newMap.get("currentDate") instanceof java.util.Date)
            accDate = (java.util.Date)newMap.get("currentDate");
        return accDate;
    }



    public boolean afterReceivingTransaction(Object[] trans) {
        int i;
        Transaction tran = null;
        for (i = 0; i < trans.length; i++) {
            if (trans[i] instanceof Transaction) {
                tran = (Transaction)trans[i];
            } else if (trans[i] instanceof LineItem) {
                LineItem li = (LineItem)trans[i];
                if (tran != null) {
                    tran.addLineItemSimpleVersion(li);
                    li.setSystemDateTime(tran.getSystemDateTime());
                }
            }
            DacBase dac = (DacBase)trans[i];
            if (!dac.insert(false)) {
                return false;
            }
        }

        return true;
    }

    public boolean afterReceivingDepSales(Object[] depSales) {
        boolean success = true;
        int zNumber = ((DepSales)depSales[0]).getSequenceNumber();
        int posNumber = ((DepSales)depSales[0]).getPOSNumber();
        
        //删除已经存在的
        DepSales.deleteBySequenceNumber(zNumber,posNumber);

        for (int i = 0; i < depSales.length; i++) {
            DepSales ds = (DepSales)depSales[i];
            ds.setAccountDate(getAccountingDate());
            if (!ds.insert(false)) {
                Server.log("DepSales inserted failed (pos=" + ds.getPOSNumber() +
                    ",z=" + ds.getSequenceNumber() +
                    ",id=" + ds.getDepID() + ").");
                success = false;
            } else {
                Server.log("DepSales inserted (pos=" + ds.getPOSNumber() +
                    ",z=" + ds.getSequenceNumber() +
                    ",id=" + ds.getDepID() + ").");
            }
        }
        return success;
    }

    public boolean afterReceivingDaishouSales(Object[] daishouSales) {
        boolean success = true;
        
        int zNumber = ((DaishouSales)daishouSales[0]).getZSequenceNumber();
        int posNumber = ((DaishouSales)daishouSales[0]).getPosNumber();
        DaishouSales.deleteBySequenceNumber(zNumber, posNumber);
        
        for (int i = 0; i < daishouSales.length; i++) {
            DaishouSales ds = (DaishouSales)daishouSales[i];
            ds.setAccountDate(getAccountingDate());
            if (!ds.insert(false)) {
                Server.log("DaishouSales inserted failed (pos=" + ds.getPosNumber() +
                    ",z=" + ds.getZSequenceNumber() +
                    ",id1=" + ds.getFirstNumber() +
                    ",id2=" + ds.getSecondNumber() + ").");
                success = false;
            } else {
                Server.log("DaishouSales inserted (pos=" + ds.getPosNumber() +
                    ",z=" + ds.getZSequenceNumber() +
                    ",id1=" + ds.getFirstNumber() +
                    ",id2=" + ds.getSecondNumber() + ").");
            }
        }
        return success;
    }
    
	/**
	 * afterReceivingDaiShouSales2
	 * 
	 * @return 如果代收资料存入SC数据库中成功则返回 true，否则返回 false.
	 */
  
	public boolean afterReceivingDaiShouSales2(Object[] daiShouSales2) {
		boolean success = true;
		
        int zNumber = ((DaiShouSales2)daiShouSales2[0]).getZSequenceNumber();
        int posNumber = ((DaiShouSales2)daiShouSales2[0]).getPosNumber();
        DaiShouSales2.deleteBySequenceNumber(zNumber, posNumber);

        for (int i = 0; i < daiShouSales2.length; i++) {
			DaiShouSales2 ds = (DaiShouSales2)daiShouSales2[i];
			ds.setAccountDate(getAccountingDate());
			if (!ds.insert(false)) {
				Server.log("DaiShouSales2 insert failed ( StoreID = " + ds.getStoreID() 
				+ ", POS = " + ds.getPosNumber()
				+ ", Z = " + ds.getZSequenceNumber()
				+ ", ID = " + ds.getID() 
				+ ")");
				success = false;
			} else {
				Server.log("DaiShouSales2 inserted ( StoreID = " + ds.getStoreID() 
				+ ", POS = " + ds.getPosNumber()
				+ ", Z = " + ds.getZSequenceNumber()
				+ ", ID = " + ds.getID() 
				+ ")");
			}
		}
		return success;
	}

    
    /*
     * 处理现金清点单
     * 
     */
    public boolean afterReceivingCashForm(Object[] CashForm) {
        boolean success = true;
        for (int i = 0; i < CashForm.length; i++) {
            CashForm cf = (CashForm)CashForm[i];

            cf.deleteByPrimaryKey();
            if (!cf.insert(false)) {
                Server.log("CashForm inserted failed (storeID=" + cf.getStoreID() + 
        		",pos=" + cf.getPOSNumber() +
                ",z=" + cf.getZSequenceNumber() + 
                ",shift=" + cf.getSequenceNumber() + ").");
                success = false;
            } else {
                Server.log("CashForm inserted (storeID=" + cf.getStoreID() + 
        		",pos=" + cf.getPOSNumber() +
                ",z=" + cf.getZSequenceNumber() + 
                ",shift=" + cf.getSequenceNumber() + ").");
            }
        }
        return success;
    }

	/**
	 * after done PluPriceChange, write flag into db
	 * 
	 */
    public void afterDonePluPriceChange(String ID, String posID, boolean success) {
        String status = success ? "0" : "1";
        Connection connection = null;
        Statement statement = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
			/*
			 * Meyer / 2003-02-21/just update, LogicChina has inserted the record 
			 */
            statement.executeUpdate(
            	" UPDATE pricechage_dl_status SET pds_status='" + status + "'" + 
            	" WHERE pds_serial_number='" + ID + "'" + 
            	" AND pds_pos_id='" + posID + "'");
        } catch(SQLException e) {
        	e.printStackTrace();        	
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace(Server.getLogger());  
            }
        }
    }

	public static void main(String[] args) {
		Server.setFakeExist();
		FamilymartPostProcessor.getAccountingDate();
	}
     
}
