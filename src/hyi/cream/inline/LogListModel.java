package hyi.cream.inline;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;

public class LogListModel extends AbstractListModel implements POSStatusListener {

    private ArrayList log = new ArrayList();

    public LogListModel() {
        log.add("start");
        try {
            Server server = Server.getInstance();
            if (server != null) {
                server.addPOSStatusListener(this);
            }
        } catch (InstantiationException e) {
            System.err.println("Cannot get Server.");
        }
    }

    synchronized public void posStatusChanged(POSStatusEvent e) {
        if (e.getStatus() != POSStatusEvent.STATUS_LOGGING)
            return;
        log.add(e.getMessage());
        fireIntervalAdded(this, log.size() -1, log.size() -1);
    }

    public int getSize() {
        return log.size();
    }

    public Object getElementAt(int index) {
        return log.get(index);
    }

}

