package hyi.cream.inline;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;

public class POSStatusTableModel extends AbstractTableModel implements POSStatusListener {

    //private ResourceBundle res = CaposinoToolkit.GetResource();
    //private CaposinoFormatter ft = new CaposinoFormatter();
    //private Statement stmt = CaposinoToolkit.getPooledStatement();
    //private SalesWeek salesWeek = new SalesWeek();

    private final static int MAX_POS_NUMBER = 5;
    private String[] headers = {
        "POS", "Status", "Send", "Receive"
    };

    private int preferedWidth[] = {6, 20, 220, 220};
    private Color[] headerColor = {
        SystemColor.windowText,
        SystemColor.windowText,
        Color.blue,
        Color.blue,
        Color.blue,
        Color.blue,
        Color.red,
        Color.red,
        Color.red,
        Color.red,
        SystemColor.windowText,
        SystemColor.windowText,
    };

    // connection status icons
    ImageIcon connectedIcon = new ImageIcon(POSStatusTableModel.class.getResource("connected.gif"));
    ImageIcon txrxIcon = new ImageIcon(POSStatusTableModel.class.getResource("txrx.gif"));
    ImageIcon rxIcon = new ImageIcon(POSStatusTableModel.class.getResource("rx.gif"));
    ImageIcon txIcon = new ImageIcon(POSStatusTableModel.class.getResource("tx.gif"));
    ImageIcon disconnectedIcon = new ImageIcon(POSStatusTableModel.class.getResource("disconnected.gif"));

    Object[][] data;
    ////final Object[][] data1 = {
    ////    {"1", "Disconnected", "", " ", ""},
    ////    {"2", "Disconnected", "", " ", ""},
    ////    {"3", "Disconnected", "", " ", ""},
    ////    {"4", "Disconnected", "", " ", ""},
    ////    {"5", "Disconnected", "", " ", ""},
    ////};
    final Object[][] data1 = {
        {" 1", disconnectedIcon, "", " "},
        {" 2", disconnectedIcon, "", " "},
        {" 3", disconnectedIcon, "", " "},
        {" 4", disconnectedIcon, "", " "},
        {" 5", disconnectedIcon, "", " "},
    };
    DisplayControlThread[] displayControlThread = new DisplayControlThread[MAX_POS_NUMBER];

    public POSStatusTableModel() {
        data = data1;

        try {
            Server server = Server.getInstance();
            if (server != null) {
                server.addPOSStatusListener(this);
            }
        } catch (InstantiationException e) {
            System.err.println("Cannot get Server.");
        }

        for (int i = 0; i < MAX_POS_NUMBER; i++) {
            displayControlThread[i] = new DisplayControlThread(i + 1, this);
            displayControlThread[i].start();
        }
    }

    public int getPreferedColumnWidth(int col) {
        return preferedWidth[col];
    }

    public Color getHeaderColor(int col) {
        return headerColor[col];
    }

    public int getPreferedHeaderHeight() {
        return 55;
    }

    public TableCellRenderer getTableColumnHeaderRenderer(int col) {
        return null; // return null means use our default CaposinoTableHeaderRenderer.
    }

    public void setupDefaultCellRenderer(JTable table) {
        //table.setDefaultRenderer(Percentage.class, new PercentageRenderer());
    }

    public int getColumnCount() {
        return headers.length;
    }

    public int getRowCount() {
        return data.length;
    }

    public Object getValueAt(int row, int col) {
        return data[row][col];
    }

    public String getColumnName(int column) {
        return headers[column];
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false; //getColumnClass(col) == String.class;
    }

    public void setValueAt(Object aValue, int row, int column) {
        data[row][column] = aValue;
    }

    public Object getExtraColumnData(int col) {
        return null;
    }

    synchronized public void posStatusChanged(POSStatusEvent e) {
        if (e.getStatus() != POSStatusEvent.STATUS_CONNECTED &&
            e.getStatus() != POSStatusEvent.STATUS_DISCONNECTED)
            return;

        if (e.getPosNumber() != 0) {
            int pos = e.getPosNumber() - 1;
            data[pos][0] = " " + e.getPosNumber() + "";

            if (e.getStatus() == POSStatusEvent.STATUS_DISCONNECTED) {
                data[pos][1] = disconnectedIcon;
                data[pos][2] = "";
                data[pos][3] = "";
                displayControlThread[pos].resetCountDown();
                fireTableDataChanged();
            } else if (e.getSending() && e.getReceiving()) {
                data[pos][1] = this.txrxIcon;
                data[pos][2] = e.getMessage();
                data[pos][3] = "";
                displayControlThread[pos].resetCountDown();
                fireTableDataChanged();
            } else if (e.getSending()) {
                if (data[pos][1] == this.txrxIcon || data[pos][1] == this.rxIcon)
                    // if previous status has "receiving", keep the receiving light on
                    data[pos][1] = this.txrxIcon;
                else
                    data[pos][1] = this.txIcon;
                data[pos][2] = e.getMessage();
                displayControlThread[pos].resetCountDown();
                fireTableDataChanged();
            } else if (e.getReceiving()) {
                if (data[pos][1] == this.txrxIcon || data[pos][1] == this.txIcon)
                    data[pos][1] = this.txrxIcon;
                else
                    data[pos][1] = this.rxIcon;
                data[pos][3] = e.getMessage();
                displayControlThread[pos].resetCountDown();
                fireTableDataChanged();
            } else {
                displayControlThread[pos].prepareSwithOff();
            }
        }
    }

    synchronized public void clearSendingReceiving(int posNumber) {
        data[posNumber - 1][1] = this.connectedIcon;
        data[posNumber - 1][2] = "";
        data[posNumber - 1][3] = "";
        fireTableDataChanged();
    }
}

