/*
 * Created on 2003-12-11
 *
 */
package hyi.cream.inline;

import java.io.*;
import java.util.*;
import java.text.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

import hyi.cream.dac.*;

/**
 * @author administrator
 *
 */
public class PostObjectData {
	private static String FILE_SEPARATOR = System.getProperty("file.separator");
	private String objectFileName; //文件名
	String tmpFileName ;
    private String postObjectDir = "TmpTrans/"; //上传上来的对象存放的目录
	private Document doc; //
    
    /**
	 * 
	 */
	public PostObjectData(int posNumber, int tranNumber, java.util.Date dt) {
	    String sdt = new SimpleDateFormat("yyyyMMdd").format(dt);
        tmpFileName = posNumber + "@" + tranNumber + "@"  + sdt + ".xml";
        objectFileName = postObjectDir + posNumber + "@" + tranNumber + "@"  + sdt + ".xml";

        try {
            File objectDir = new File(postObjectDir);
            //判断是否存在此目录，不存在就创建新目录
            if (!(objectDir.exists() && objectDir.isDirectory())) {
                objectDir.mkdir();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
  	}

	//该存放该交易的文件是否存在
	public boolean exists() {
		File objectFile = new File(objectFileName);
		return objectFile.exists();
	}

	public String getPostObjectDir() {
		return this.postObjectDir;
	}

	private boolean saveToFile() {
		try {
			XMLOutputter outputter = new XMLOutputter("", true);
            File tmpFile = new File(objectFileName + ".tmp");
            FileWriter fw = new FileWriter(tmpFile);
            outputter.output(doc, fw);
            fw.close();
            
            File newFile = new File(objectFileName);
            tmpFile.renameTo(newFile);
            
            return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}


	/**
	 * 结构如下
	 * <Transaction>
	 *      <TranHead>
	 *      </TranHead>
	 *      <TranDetail>
	 *      </TranDetail>
	 *      <TranDetail>
	 *      </TranDetail>
	 * </Transaction>     
	 * @param tran
	 */

	public void save(Transaction tran) {
        if (this.exists()) 
            return;		
        
        doc = new Document();
        Element root = new Element("Transaction");
		doc.setRootElement(root);
        
		Element emTranHead = new Element("TranHead");
		Iterator iPK = tran.getPrimaryKeyList().iterator();
		while (iPK.hasNext()) {
			String name = (String) iPK.next();
			Object value = tran.getFieldValue(name);
			Element el = new Element(name);
			el.setText(value.toString());
			emTranHead.addContent(el);
		}
        
        Element emDT1 = new Element("dealType1");
        emDT1.setText(tran.getDealType1());
        emTranHead.addContent(emDT1);
        
        Element emDT2 = new Element("dealType2");
        emDT2.setText(tran.getDealType2());
        emTranHead.addContent(emDT2);

        Element emDT3 = new Element("dealType3");
        emDT3.setText(tran.getDealType3());
        emTranHead.addContent(emDT3);

		Element emsql = new Element("sql");
		emsql.setText(tran.getInsertSqlStatement(false));
		emTranHead.addContent(emsql);

		root.addContent(emTranHead);

		Object[] items = tran.getLineItemsArrayLast();
		for (int i = 0; i < items.length; i++) {
			LineItem li = (LineItem) items[i];
			Iterator iLiPK = li.getPrimaryKeyList().iterator();
			Element emTranDetail = new Element("TranDetail");

            Element elSeq = new Element("lineItemSequence");
            elSeq.setText(li.getLineItemSequence() + "");
            emTranDetail.addContent(elSeq);
            
            Element elDetailCode = new Element("detailCode");
            elDetailCode.setText(li.getDetailCode());
            emTranDetail.addContent(elDetailCode);
            
            Element elItemNo = new Element("itemNumber");
            elItemNo.setText(li.getItemNumber());
            emTranDetail.addContent(elItemNo);

            Element elQuantity = new Element("quantity");
            elQuantity.setText(li.getQuantity().toString());
            emTranDetail.addContent(elQuantity);

            Element elWeight = new Element("weight");
            elWeight .setText(li.getQuantity().toString());
            emTranDetail.addContent(elWeight );

			Element elPrice = new Element("unitPrice");
            elPrice.setText(li.getUnitPrice().toString());
            emTranDetail.addContent(elPrice);
            
            Element emItemSql = new Element("sql");
			emItemSql.setText(li.getInsertSqlStatement(false));
			emTranDetail.addContent(emItemSql);

			root.addContent(emTranDetail);
		}
        //gllg
        if (tran.payMentList.size() > 0){
            Object[] tokenTrandtls = tran.payMentList.toArray();
            for (int i = 0; i < tokenTrandtls.length; i++) {
                TokenTranDtl ttd = (TokenTranDtl) tokenTrandtls[i];
                Element tokenTrandtl = new Element("TokenTrandtl");

                Element tokenTrandtlSql = new Element("sql");
                tokenTrandtlSql.setText(ttd.getInsertSqlStatement(false));
                tokenTrandtl.addContent(tokenTrandtlSql);
                root.addContent(tokenTrandtl);
            }
        }

        this.saveToFile();
	}

}
