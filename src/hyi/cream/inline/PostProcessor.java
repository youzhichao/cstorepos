package hyi.cream.inline;

import hyi.cream.dac.*;

/**
 * Data post-processor for inline server.
 *
 * @author Bruce
 * @version 1.7
 */
interface PostProcessor {
    /**
     * /Bruce/1.7/2002-04-27/
     *    增加上传代收明细帐.
     */

    /**
     * Shift report processor.
     *
     * @param shift Shift report object.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingShiftReport(ShiftReport shift);

    /**
     * Z report processor.
     *
     * @param z Z report object
     * @return true if success, otherwise false.
     */
    boolean afterReceivingZReport(ZReport z);

    /**
     * Transaction processor.
     *
     * @param trans Transaction and LineItem objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingTransaction(Object[] trans);

    /**
     * Department sales processor.
     *
     * @param depSales Department sales objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingDepSales(Object[] depSales);

    /**
     * Daishou sales processor.
     *
     * @param daishouSales Daishou sales objects.
     * @return true if success, otherwise false.
     */

    boolean afterReceivingDaishouSales(Object[] daishouSales);

	/**
	  * Daishou sales processor.
	  *
	  * @param DaiShouSales2 Daishou sales objects.
	  * @return true if success, otherwise false.
	  */

    boolean afterReceivingDaiShouSales2(Object[] daiShouSales2);
    
    /**
     * CashForm processor.
     *
     * @param cashForm CashForm objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingCashForm(Object[] cashForm);   

    /**
     * Inventory processor.
     *
     * @param inventory Inventory objects.
     * @return true if success, otherwise false.
     */
    boolean afterReceivingInventory(Object[] inventory);   

    /**
     * Master download preprocessor.
     *
     * @return true if it allows master downloading now, false otherwise.
     */
    boolean beforeDownloadingMaster();
    
    /**
     * 
     * 
     */
    boolean canSyncTransaction();
    
    /**
     * 变价
     */
    void afterDonePluPriceChange(String ID, String posID, boolean success);
}
