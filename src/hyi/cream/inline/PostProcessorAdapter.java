package hyi.cream.inline;

import java.util.*;
import java.sql.*;
import java.text.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;


public class PostProcessorAdapter implements PostProcessor {
    private static boolean isServerSideOracle = Server.serverExist() && "oracle".equalsIgnoreCase(CreamProperties.getInstance().getProperty("DBType"));

    public boolean afterReceivingShiftReport(ShiftReport shift) {return true;}
	public boolean afterReceivingZReport(ZReport z) {return true;}
    public boolean afterReceivingTransaction(Object[] trans) {return true;}
    public boolean afterReceivingDepSales(Object[] depSales)  {return true;}
    public boolean afterReceivingDaishouSales(Object[] daishouSales) {return true;}
    public boolean afterReceivingCashForm(Object[] cashForm) {return true;} 
    public boolean afterReceivingDaiShouSales2(Object[] daiShouSales2) {return true;}
    public boolean afterReceivingInventory(Object[] inventory) {return true;}   
    public boolean beforeDownloadingMaster() {return true;}
	public boolean canSyncTransaction() { return true;}
    public void afterDonePluPriceChange(String ID, String posID, boolean success) {};

    private static String stoID = getStoreID(); //缓存StoreID
    
    public static ArrayList constructFromResultSet(ResultSet resultSet) throws SQLException {
        ArrayList table = new ArrayList();
        HashMap newMap = null;
        while (resultSet.next()) {
            newMap = new HashMap();
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                Object obj = metaData.getColumnName(i);
                newMap.put(obj.toString().toLowerCase(), resultSet.getObject(i));
            }
            table.add(newMap);
        }
        return table;
    }
    
    public static HashMap constructFromResultSet2(ResultSet resultSet) throws SQLException {
        HashMap newMap = new HashMap();
        if (resultSet.next()) {
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                Object obj = metaData.getColumnName(i);
                newMap.put(obj.toString().toLowerCase(), resultSet.getObject(i));
            }
        }
        return newMap;
    }
    
    public static Map query(String selectStatement) {
        Connection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        HashMap map = new HashMap();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            if (resultSet.next()) {
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    map.put(metaData.getColumnName(i).toLowerCase(), resultSet.getObject(i));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
            return map;
        }
    }

    public static boolean update(String updateStatement) {
        Connection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.executeUpdate(updateStatement);
            return true;
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            return false;
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
        }
    }

    public static void executeQuery(String query) {
        //CreamToolkit.logMessage("" + selectStatement);
        Connection connection  = null;
        Statement  statement   = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            if (connection != null)
                CreamToolkit.releaseConnection(connection);
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
        }
    }

    public static ArrayList getCollectionOfStatement(String selectStatement) {
        Connection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        ArrayList collection   = new ArrayList();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            while (resultSet.next()) {
                HashMap map = new HashMap();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    map.put(metaData.getColumnName(i).toLowerCase(), resultSet.getObject(i));
                }
                collection.add(map);
            }
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            if (connection != null) {
                CreamToolkit.releaseConnection(connection);
            }
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();

            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
            return collection;
        }
    }
    
    public static boolean insert(HashMap fieldMap, String tableName) {
        Iterator iterator               = ((Set)fieldMap.keySet()).iterator();
        Object fieldValue               = null;
        String fieldName                = "";
        boolean result                  = true;
        Connection connection2  = null;
        Statement  statement   = null;
        String nameParams = "";
        String valueParams = "";
        while (iterator.hasNext()) {
            fieldName = (String)iterator.next();
            if (fieldMap.get(fieldName) == null) {
                fieldValue = new Object();
            } else {
                fieldValue = fieldMap.get(fieldName);
                if (fieldValue instanceof java.util.Date) {
                    SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    java.util.Date d = (java.util.Date)fieldValue;
                    nameParams  += fieldName + ",";
                    if (isServerSideOracle)
                        valueParams += "TO_DATE('" + DF.format(d).toString() + "','YYYY-MM-DD HH24:MI:SS'),";
                    else
                        valueParams += "'" + DF.format(d).toString() + "',";
                } else if (fieldValue instanceof String) {
                    nameParams  += fieldName + ",";
                    valueParams += "'" + fieldValue + "',";
                } else {
                   nameParams  += fieldName + ",";
                   valueParams += "'" + fieldValue + "',";
                }
            }
        }
        String insertString = "INSERT INTO " + tableName + "(" + nameParams.substring(0, nameParams.length()-1) + ")"
            + " VALUES (" + valueParams.substring(0,  valueParams.length()-1) + ")";

        try    {
            connection2 = CreamToolkit.getPooledConnection();
            statement = connection2.createStatement();
            statement.execute(insertString);
        } catch (SQLException e) {
            result = false;
            Server.log("Error SQL statement: " + insertString);
            e.printStackTrace(Server.getLogger());
        } finally {
            if (connection2 != null)
                CreamToolkit.releaseConnection(connection2);
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                result = false;
                Server.log("Error SQL statement: " + insertString);
                e.printStackTrace(Server.getLogger());
            }
            return result;
        }
    }   

    public static String getStoreID() {
        
        if (stoID != null && !stoID.equals("")) 
            return stoID;
        
        // Try to get storeID from CreamProperties first
        String storeID = CreamProperties.getInstance().getProperty("StoreID","");
        if (storeID.equals("")) {
            String query = "SELECT storeid FROM store";
            Map value = query(query);
            if (value != null) {
                if (value.containsKey("storeid"))
                   storeID = value.get("storeid").toString();
            }
        }
        stoID = storeID;
        return stoID;
    }
    
    /**
     * 取得指定表的字段名列表
     * @param tableName
     * @return Collection of fields
     */
    public static Collection getFieldList(String tableName) {
        Connection connection  = null;
        Statement  statement   = null;
        ResultSet  resultSet   = null;
        List list = new ArrayList();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM " + tableName + " WHERE 1=2");
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                list.add(metaData.getColumnName(i).toLowerCase());
            }
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
            return list;
        }
    }


}
