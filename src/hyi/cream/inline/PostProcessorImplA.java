package hyi.cream.inline;

import java.text.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.math.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;

class PostProcessorImplA extends PostProcessorAdapter {

    transient public static final String VERSION = "1.8";

    static final int DATE_ACCDATE         = 200;
    static final int DATE_BIZDATE         = DATE_ACCDATE + 1;
    static final int DATE_INFDATE         = DATE_ACCDATE + 2;
    static private DateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd");
    static private DateFormat yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
    static final String POS_AMOUNT = "posamount";
    static final String STORE_AMOUNT = "storeamount";
    static final Integer integer0 = new Integer(0);
    static Object lockObject = new Object();

    static private Map zSeqToAccountingDateMap = new HashMap();
    private boolean isServerSideOracle = Server.serverExist() && "oracle".equalsIgnoreCase(CreamProperties.getInstance().getProperty("DBType"));

//    /**
//     * 根据交易时间决定营业日期。
//     * 
//     * @param date 交易时间
//     * @return 返回营业日期
//     */
//    private Date getBusinessDate(Date date) {
//        String changeTime = "22:59:59";
//        Map value = query("SELECT changetime FROM calendar WHERE dateType = '0'");
//        if (value != null && value.containsKey("changetime")) {
//            changeTime = (String)value.get("changetime").toString();
//            if (changeTime.trim().length() == 0 || changeTime.equals("00:00:00")) {
//                changeTime = "22:59:59";        
//            }
//        }
//        String realTime = new SimpleDateFormat("HH:mm:ss").format(date);
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//        if (realTime.compareTo(changeTime) > 0) {
//            cal.add(Calendar.DATE, 1);
//        }
//        cal.set(Calendar.HOUR, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        return cal.getTime();
//    }

    /**
     * 由Z帐序号决定会计日期。
     * 
     * @param posNumber POS机号
     * @param zSeq Z帐序号
     * @return 返回会计日期
     */
    private Date getAccountingDate(int posNumber, int zSeq) {
        Integer key = new Integer(posNumber * 1000000 + zSeq);
        Date accountingDate = (Date)zSeqToAccountingDateMap.get(key);
        if (accountingDate != null)
            return accountingDate;
        Map value = query("SELECT accountdate FROM posul_z WHERE posNumber=" + posNumber 
            + " AND zSequenceNumber=" + zSeq);
        if (value != null && value.containsKey("accountdate")) {
            accountingDate = (Date)value.get("accountdate");
            zSeqToAccountingDateMap.put(key, accountingDate);
            return accountingDate;
        }
        return getAccountingDate(new Date());
    }

    /**
     * 由Z帐序号或交易时间决定会计日期。
     * 
     * @param posNumber POS机号
     * @param zSeq Z帐序号
     * @param transactionDate 交易时间
     * @return 返回会计日期
     */
    private Date getAccountingDate(int posNumber, int zSeq, Date transactionDate) {
        Integer key = new Integer(posNumber * 1000000 + zSeq);
        Date accountingDate = (Date)zSeqToAccountingDateMap.get(key);
        if (accountingDate != null)
            return accountingDate;
        Map value = query("SELECT accountdate FROM posul_z WHERE posNumber=" + posNumber 
            + " AND zSequenceNumber=" + zSeq);

        if (value != null && value.containsKey("accountdate")) {
            accountingDate = (Date)value.get("accountdate");
            zSeqToAccountingDateMap.put(key, accountingDate);
            return accountingDate;
        }
        return getAccountingDate(transactionDate);
    }

    private Date getAccountingDate(Date date) {
        // Karman said: (2008-06-02)
        // 当z帐里的结束时间 < z结束日期+AccountingDateShiftTime 时 z->accountdate=z结束日期
        // 当z帐里的结束时间 > z结束日期+AccountingDateShiftTime 时 z->accountdate=z结束日期+1

        String dateshiftTime = CreamProperties.getInstance().getProperty("AccountingDateShiftTime", "17:00:00");
        if (dateshiftTime.equalsIgnoreCase("afternoon") || dateshiftTime.equalsIgnoreCase("morning")) // for old conf in case
            dateshiftTime = "17:00:00";
        
        String timePart = new SimpleDateFormat("HH:mm:ss").format(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        if (timePart.compareTo(dateshiftTime) >= 0)
            cal.add(Calendar.DATE, 1);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();

//        String changeTime = "23:59:59";
//        Map value = query("SELECT changetime FROM calendar WHERE dateType = '1'");
//        if (value != null && value.containsKey("changetime")) {
//            changeTime = (String)value.get("changetime").toString();
//            if (changeTime.trim().length() == 0 || changeTime.equals("00:00:00")) {
//                changeTime = "23:59:59";        
//            }
//        }
//        String realTime = new SimpleDateFormat("HH:mm:ss").format(date);
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//
//        //Bruce/2003-12-01
//        //有两种决定会计日期的models:
//        //如果AccountingDateShiftTime="afternoon"(default)，则超过换日时间后为(d+1)日；
//        //否则(="morning")超过换日时间后仍为(d)日，在换日之前为(d-1)日。
//        boolean afternoon = CreamProperties.getInstance().getProperty(
//            "AccountingDateShiftTime","afternoon").equalsIgnoreCase("afternoon");
//        if (afternoon) {
//            if (realTime.compareTo(changeTime) > 0) {
//                cal.add(Calendar.DATE, 1);
//            }
//        } else {
//            if (realTime.compareTo(changeTime) <= 0) {
//                cal.add(Calendar.DATE, -1);
//            }
//        }
//        cal.set(Calendar.HOUR, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        return cal.getTime();
    }

//    private Date getAccountingDate() {
//        String storeID = getStoreID();
//        Connection connection = null;
//        Statement statement = null;
//        ResultSet resultSet = null;
//        String select = "SELECT * FROM calendar";
//        String where = " WHERE storeID = '" + storeID + "' AND dateType = '1'";
//        try {
//            connection = CreamToolkit.getPooledConnection();
//            statement = connection.createStatement();
//            resultSet = statement.executeQuery(select + where);
//        } catch (SQLException e) {
//            e.printStackTrace(Server.getLogger());
//            try {
//                if (statement != null)
//                    statement.close();
//                if (connection != null)
//                    CreamToolkit.releaseConnection(connection);
//            } catch (SQLException e2) {
//                e2.printStackTrace(Server.getLogger());
//            }
//            return new java.util.Date();
//        }
//        HashMap newMap = null;
//        try {
//            if (resultSet.next()) {
//                newMap = new HashMap();
//                ResultSetMetaData metaData = resultSet.getMetaData();
//                for (int i = 1; i <= metaData.getColumnCount(); i++) {
//                    Object obj = metaData.getColumnName(i);
//                    newMap.put(obj, resultSet.getObject(i));
//                }
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (statement != null)
//                    statement.close();
//                if (connection != null)
//                    CreamToolkit.releaseConnection(connection);
//            } catch (SQLException e) {
//                e.printStackTrace(Server.getLogger());
//            }
//        }
//        java.util.Date accDate = new java.util.Date();
//        if (newMap == null)
//            return accDate;
//        if (newMap.get("currentDate") instanceof java.util.Date)
//            accDate = (java.util.Date)newMap.get("currentDate");
//        return accDate;
//    }

    public boolean isDateShiftRunning(int dateType) {
        return false;
    /*
        String type = "";
        switch (dateType) {
           case DATE_ACCDATE:
               type = "1";
               break;
           case DATE_BIZDATE:
               type = "0";
               break;
           case DATE_INFDATE:
               type = "4";
               break;
           default:
               return false;
        }
        String storeID = getStoreID();//CreamToolkit.getConfiguration().getProperty("StoreID");
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String select = "SELECT * FROM calendar ";
        String where = " WHERE storeID = '" + storeID + "' AND dateType = '" + type + "'";
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(select + where);
            //System.out.println(select + where);
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            if (connection == null) {
                //BRUCE> THIS IS VERY SERIOUS PROBLEM, SHUT INLINE SERVER DOWN,
                // AND LET IT RESTART AGAIN!!!
                Server.log("Cannot get database connection, server is stopped.");
                System.exit(1);
            }
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace(Server.getLogger());
            }
            return false;
        }
        HashMap newMap = null;
        try {
            if (resultSet.next()) {
                newMap = new HashMap();
                ResultSetMetaData metaData = resultSet.getMetaData();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object obj = metaData.getColumnName(i);
                    newMap.put(obj, resultSet.getObject(i));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
        }
        String running  = "";
        if (newMap == null)
            return false;
        if (newMap.get("scEodResult") instanceof String)
            running = (String)newMap.get("scEodResult");
        if (running.compareTo("1") == 0)
           return true;
        return false;
    */           
    }

    public boolean isDateShiftRunningOrFailed(int dateType) {
        return false;
    /*
        String type = "";
        switch (dateType) {
           case DATE_ACCDATE:
               type = "1";
               break;
           case DATE_BIZDATE:
               type = "0";
               break;
           case DATE_INFDATE:
               type = "4";
               break;
           default:
               return false;
        }
        String storeID = getStoreID();//CreamToolkit.getConfiguration().getProperty("StoreID");
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String select = "SELECT * FROM calendar ";
        String where = " WHERE storeID = '" + storeID + "' AND dateType = '" + type + "'";
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(select + where);
            //System.out.println(select + where);
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            if (connection == null) {
                //BRUCE> THIS IS VERY SERIOUS PROBLEM, SHUT INLINE SERVER DOWN,
                // AND LET IT RESTART AGAIN!!!
                Server.log("Cannot get database connection, server is stopped.");
                System.exit(1);
            }
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e2) {
                e2.printStackTrace(Server.getLogger());
            }
            return false;
        }
        HashMap newMap = null;
        try {
            if (resultSet.next()) {
                newMap = new HashMap();
                ResultSetMetaData metaData = resultSet.getMetaData();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    Object obj = metaData.getColumnName(i);
                    newMap.put(obj, resultSet.getObject(i));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
        }
        String running  = "";
        if (newMap == null)
            return false;
        if (newMap.get("scEodResult") instanceof String)
            running = (String)newMap.get("scEodResult");
        if (running.equals("1") || running.equals("2"))
           return true;
        return false;
    */
    }

    public synchronized boolean afterReceivingShiftReport(ShiftReport shift) {
        // 根据shift的结束日期来决定会计日期
        Date accountingDate = getAccountingDate(shift.getSignOffSystemDateTime());
        shift.setAccountingDate(accountingDate);
        
        shift.deleteByPrimaryKey();
        
        if (!shift.insert(false)) {
            Server.log("Shift inserted failed (pos=" + shift.getTerminalNumber() +
                ",z=" +shift.getZSequenceNumber() +
                ",shift=" +shift.getSequenceNumber() + ").");
            return false;
        } else {
            Server.log("Shift inserted (date=" + dateFormatter.format(shift.getAccountingDate()) +
                ",pos=" + shift.getTerminalNumber() +
                ",z=" +shift.getZSequenceNumber() +
                ",shift=" +shift.getSequenceNumber() + ").");
        }
        return true;
    }

    private Integer retrieveInteger(Object number) {
        if (number == null)
            return new Integer(0);
        else if (number instanceof Integer)
            return (Integer)number;
        else if (number instanceof Long)
            return new Integer(((Long)number).intValue());
        else if (number instanceof Short)
            return new Integer(((Short)number).intValue());
        else if (number instanceof BigDecimal)
            return new Integer(((BigDecimal)number).intValue());
        else
            return new Integer(0);
    }

    /**
     * Check if the given date's accdayrpt has no records, if yes, copy from
     * yesterday's record, or from commdl_accitem.
     * 
     * @author bruce
     */
    private boolean generateAccdayrptRecords(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        String yesterday = yyyy_MM_dd.format(cal.getTime());
        String today = yyyy_MM_dd.format(date);

        Map value;
        if (isServerSideOracle)
            value = query("SELECT COUNT(*) AS cnt FROM accdayrpt " 
            + "WHERE accountDate=TO_DATE('" + today + "','YYYY-MM-DD HH24:MI:SS')");
        else
            value = query("SELECT COUNT(*) AS cnt FROM accdayrpt "
            + "WHERE accountDate='" + today + "'");

        if (value != null && value.containsKey("cnt")) {
            Integer todayCount = retrieveInteger(value.get("cnt"));
            if (todayCount.compareTo(integer0) > 0) {
                return true;  //表示今天的记录已经存在
            }
        }
        if (isServerSideOracle)
            value = query("SELECT COUNT(*) AS cnt FROM accdayrpt "
            + "WHERE accountDate=TO_DATE('" + yesterday + "','YYYY-MM-DD HH24:MI:SS')");
        else
            value = query("SELECT COUNT(*) AS cnt FROM accdayrpt "
            + "WHERE accountDate='" + yesterday + "'");
        Integer yesterdayCount = integer0;
        if (value != null && value.containsKey("cnt"))
            yesterdayCount = retrieveInteger(value.get("cnt"));

        if (yesterdayCount.compareTo(integer0) > 0) { // 表示有昨天的记录
            if (isServerSideOracle)
                update("TRUNCATE TABLE TMP");
            else
                update(
                "CREATE TEMPORARY TABLE tmp ("
                    + "storeID varchar(6) NOT NULL DEFAULT '' ,"
                    + "accountDate date NOT NULL DEFAULT '0000-00-00' ,"
                    + "accountNumber varchar(6) NOT NULL DEFAULT '' ,"
                    + "accountName varchar(30) ,"
                    + "posNumber tinyint(3) unsigned NOT NULL DEFAULT '0' ,"
                    + "displayType char(2) ,"
                    + "displaySequence varchar(6) ,"
                    + "posAmount decimal(12,2) ,"
                    + "storeAmount decimal(12,2) ,"
                    + "storeCount tinyint(3) unsigned ,"
                    + "updateType varchar(4) ,"
                    + "calType varchar(4) ,"
                    + "uploadType char(1) ,"
                    + "itemNumber varchar(10) ,"
                    + "payID char(2) ,"
                    + "dataSource varchar(50) ,"
                    + "updateUserID varchar(8) ,"
                    + "updateDate datetime"
                    + ")");
            if (isServerSideOracle)
                update(
                "INSERT INTO tmp ("
                    + "storeID, accountDate, accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate) "
                    + "SELECT storeID, TO_DATE('" + today + "','YYYY-MM-DD HH24:MI:SS'), accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate "
                    + "FROM accdayrpt "
                    + "WHERE accountDate=TO_DATE('" + yesterday + "','YYYY-MM-DD HH24:MI:SS')");
            else
                update(
                "INSERT INTO tmp ("
                    + "storeID, accountDate, accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate) "
                    + "SELECT storeID, '" + today + "', accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate "
                    + "FROM accdayrpt "
                    + "WHERE accountDate='" + yesterday + "'");

            update("INSERT INTO accdayrpt SELECT * FROM tmp");
            if (isServerSideOracle)
                update(
                "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                    + "WHERE accountDate=TO_DATE('" + today + "','YYYY-MM-DD HH24:MI:SS')");
            else
                update(
                "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                    + "WHERE accountDate='" + today + "'");

            if (isServerSideOracle)
                update("TRUNCATE TABLE TMP");
            else
                update("DROP TABLE tmp");

        } else {  // accdayrpt中没有昨天的记录，从commdl_accitem取

            value = query("SELECT MAX(updatebegindate) dt FROM commdl_accitem");
            if (value == null || !value.containsKey("dt")) {
                Server.log("Cannot create accdayrpt records.");
                return false;
            }
            Date accitemDate = (Date)value.get("dt");
            String accitemDateString = yyyy_MM_dd.format(accitemDate);
            if (isServerSideOracle) {
                update(
                "INSERT INTO accdayrpt ("
                    + "storeID, accountDate, accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate) "
                    + "SELECT storeID, TO_DATE('" + today + "','YYYY-MM-DD HH24:MI:SS'), accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate "
                    + "FROM commdl_accitem "
                    + "WHERE updateBeginDate=TO_DATE('"+accitemDateString+"','YYYY-MM-DD HH24:MI:SS')");
                update(
                    "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                    + "WHERE accountDate=TO_DATE('" + today + "','YYYY-MM-DD HH24:MI:SS')");
            } else {
                update(
                "INSERT INTO accdayrpt ("
                    + "storeID, accountDate, accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate) "
                    + "SELECT storeID, '" + today + "', accountNumber, accountName, posNumber,"
                    + "displayType, displaySequence, posAmount, storeAmount, storeCount,"
                    + "updateType, calType, uploadType, itemNumber, payID, dataSource,"
                    + "updateUserID, updateDate "
                    + "FROM commdl_accitem "
                    + "WHERE updateBeginDate='" + accitemDateString + "'");
                update(
                    "UPDATE accdayrpt SET posAmount=0,storeAmount=0,storeCount=0 "
                    + "WHERE accountDate='" + today + "'");

            }


        }
        return true;
    }

    public boolean afterReceivingZReport(ZReport z) {
        boolean returnValue = true;

        // 根据Z的结束日期来决定会计日期
        Date accountingDate = getAccountingDate(z.getEndSystemDateTime());
        z.setAccountingDate(accountingDate);

        z.deleteByPrimaryKey();
        
        if (!z.insert(false)) {
            Server.log("Z inserted failed (pos=" + z.getTerminalNumber() +
                ",z=" + z.getSequenceNumber() + ").");
            return false;
        } else {
            Server.log("Z inserted (date=" + dateFormatter.format(z.getAccountingDate()) +
                ",pos=" + z.getTerminalNumber() +
                ",z=" + z.getSequenceNumber() + ").");
        }

        // 检查或生成该会计日期的accdayrpt记录
        generateAccdayrptRecords(accountingDate);

        //Bruce/2008-09-27/ Get rid of posaccountdate, no need anymore
//      // Update posaccountdate
//      HashMap newRec = new HashMap();
//      newRec.put("storeID", z.getStoreNumber().trim());
//      newRec.put("accountDate", z.getAccountingDate());
//      newRec.put("posNumber", z.getTerminalNumber());
//      newRec.put("zSequenceNumber", z.getSequenceNumber());
//
//      if (!insert(newRec, "posaccountdate"))
//          Server.log("Maybe caused by re-send Z.");

        synchronized (lockObject) {
            Iterator iter = ZReport.queryByDateTime(accountingDate);
            if (iter == null)
                return returnValue;

            String accountingDateString = yyyy_MM_dd.format(accountingDate);
            if (isServerSideOracle) {

                 // 先避免posAmount和storeAmount有空值
                update(
                        "UPDATE accdayrpt SET storeAmount=0 "
                                + "WHERE accountDate=TO_DATE('"+accountingDateString
                                + "','YYYY-MM-DD HH24:MI:SS') AND storeAmount IS NULL");
                update(
                        "UPDATE accdayrpt SET posAmount=0 "
                                + "WHERE accountDate=TO_DATE('" + accountingDateString
                                + "','YYYY-MM-DD HH24:MI:SS') AND posAmount IS NULL");
                // 把storeAmount先计成与原posAmount的差额(为了保留用户原来对storeAmount的修改)
                update(
                        "UPDATE accdayrpt SET storeAmount=storeAmount-posAmount "
                                + "WHERE accountDate=TO_DATE('" + accountingDateString + "','YYYY-MM-DD HH24:MI:SS')");
                // 然后把posAmount归零
                update(
                        "UPDATE accdayrpt SET posAmount=0 "
                                + "WHERE accountDate=TO_DATE('" + accountingDateString + "','YYYY-MM-DD HH24:MI:SS')");
            } else {

                // 先避免posAmount和storeAmount有空值
                update(
                        "UPDATE accdayrpt SET storeAmount=0 "
                                + "WHERE accountDate='" + accountingDateString
                                + "' AND storeAmount IS NULL");
                update(
                        "UPDATE accdayrpt SET posAmount=0 "
                                + "WHERE accountDate='" + accountingDateString
                                + "' AND posAmount IS NULL");
                // 把storeAmount先计成与原posAmount的差额(为了保留用户原来对storeAmount的修改)
                update(
                        "UPDATE accdayrpt SET storeAmount=storeAmount-posAmount "
                                + "WHERE accountDate='" + accountingDateString + "'");
                // 然后把posAmount归零
                update(
                        "UPDATE accdayrpt SET posAmount=0 "
                                + "WHERE accountDate='" + accountingDateString + "'");
            }

            // 开始针对每一个该日的Z帐进行过转到accdayrpt的计算
            while (iter.hasNext()) {
                z = (ZReport)iter.next();

                // 更新现金日报表
                //Primary Key: storeID + accountDate + accountNumber + posNumber
                String storeID = z.getStoreNumber();
                int posNumber = z.getTerminalNumber().intValue();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String accDate = format.format(z.getAccountingDate()).toString();
                Connection connection = null;
                Statement statement = null;
                ResultSet resultSet = null;
                ArrayList table = null;
                String select = "SELECT * FROM accdayrpt ";
                String updateString = "UPDATE accdayrpt SET ";
                String where;
                if (isServerSideOracle)
                    where =" WHERE storeID = '" + storeID
                    + "' AND accountDate = TO_DATE('" + accDate + "','YYYY-MM-DD HH24:MI:SS')";//AND posNumber = '"    + posNumber + "'";
                else
                    where =" WHERE storeID = '" + storeID
                    + "' AND accountDate = '" + accDate + "'";//AND posNumber = '"    + posNumber + "'";
                //CreamToolkit.logMessage("" + select + where);
                try {
                    connection = CreamToolkit.getPooledConnection();
                    statement = connection.createStatement();
                    resultSet = statement.executeQuery(select + where);
                    table = constructFromResultSet(resultSet);
                    if (table.isEmpty()) {
                        Server.log("afterReceivingZReport(): Cannot find record in accdayrpt");
                        Server.log("SQLStatement: " + select + where);
                        returnValue = false;
                    }
                } catch (SQLException e) {
                    Server.log("afterReceivingZReport(): Cannot find record in accdayrpt, query failed.");
                    Server.log("SQLStatement: " + select + where);
                    e.printStackTrace(Server.getLogger());
                    returnValue = false;
                } finally {
                    try {
                        if (statement != null)
                            statement.close();
                    } catch (SQLException e) {
                        e.printStackTrace(Server.getLogger());
                    }
                    if (connection != null) {
                    	//Server.log("************* afterReceivingZReport : connection is close");
                        CreamToolkit.releaseConnection(connection);
                    }
                    if (!returnValue) {
                        return false;
                    }
                }
    
                Map zRecord = z.getValues();

                HashMap map = new HashMap();
        
                //Bruce/2002-04-10
                //map.put("001", z.getTransactionCount());
                map.put("001", z.getCustomerCount());
        
                map.put("002", z.getGrossSales());
                map.put("003", new BigDecimal(0.00)
                    .add(z.getSIPercentOffTotalAmount())
                    .subtract(z.getSIPercentPlusTotalAmount()));
                map.put("004", z.getMixAndMatchTotalAmount());
                map.put("005", z.getItemDiscAmount()); // Bruce/20080603 变价总额
                map.put("006", z.getNetSalesTotalAmount());
                map.put("201", z.getVoucherAmount());
                map.put("300", z.getDaiShouAmount2()); //Bruce/20030819 公共事业费代收金额
                map.put("601", z.getSpillAmount());
                /*
                map.put("102A", z.getTaxAmount1());
                map.put("102B", z.getTaxAmount2());
                map.put("102C", z.getTaxAmount3());
                map.put("102D", z.getTaxAmount4());
                */
                //map.put("101A", z.getTaxedAmount());
                //map.put("101B", z.getNoTaxedAmount());
                //
                Iterator itr = table.iterator();
                while (itr.hasNext()) {
                    HashMap accdayrptRecord = (HashMap)itr.next();
                    String accountNumber = (String)accdayrptRecord.get("accountnumber");
                    //CreamToolkit.logMessage("" + "accountnumber=" + accountNumber);
                    String payID = (String)accdayrptRecord.get("payid");
                    if (payID == null || payID.trim().length() == 0)
                       payID = "-1";
                    //CreamToolkit.logMessage("" + "payid=" + payID);
                    int cposNumber = Integer.parseInt(accdayrptRecord.get("posnumber").toString());
                    //CreamToolkit.logMessage("" + "posNumber=" + cposNumber);
                    if (cposNumber == posNumber) {
                        if (map.containsKey(accountNumber)) {
                            BigDecimal amt = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            BigDecimal mapAmt = new BigDecimal(0);
                            if (map.get(accountNumber) instanceof Integer || map.get(accountNumber) instanceof Long || map.get(accountNumber) instanceof Short) {
                                Integer in = retrieveInteger(map.get(accountNumber));
                                mapAmt = new BigDecimal(in.intValue());
                            } else if (map.get(accountNumber) instanceof BigDecimal) {
                                mapAmt = (BigDecimal)map.get(accountNumber);
                            }
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt );
                            if (amt == null)
                               amt = new BigDecimal(0);
                            amt = amt.add(mapAmt);
                            //CreamToolkit.logMessage("" + accountNumber + ":Additional amount = " + mapAmt);
                            accdayrptRecord.put(POS_AMOUNT, amt);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(mapAmt));
                            }
                        }
                    } else if (cposNumber == 0) {
                        if (map.containsKey(accountNumber)) {
                            BigDecimal amt = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            BigDecimal mapAmt = new BigDecimal(0);
                            if (map.get(accountNumber) instanceof Integer || map.get(accountNumber) instanceof Long || map.get(accountNumber) instanceof Short) {
                                Integer in = retrieveInteger(map.get(accountNumber));
                                mapAmt = new BigDecimal(in.intValue());
                            } else if (map.get(accountNumber) instanceof BigDecimal) {
                                mapAmt = (BigDecimal)map.get(accountNumber);
                            }
                            if (amt == null)
                               amt = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt );
                            //CreamToolkit.logMessage("" + accountNumber + ":Additional amount = " + mapAmt);
                            amt = amt.add(mapAmt);
                            accdayrptRecord.put(POS_AMOUNT, amt);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(mapAmt));
                            }
                        }  else if (accountNumber.startsWith("3")) {//代收
                            
                            BigDecimal daiShouTotalAmount = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (daiShouTotalAmount == null)
                               daiShouTotalAmount = new BigDecimal(0);

                            BigDecimal amt;
                            if (accountNumber.equals("300")) { //Bruce/20030819 公共事业费代收金额
                                amt = z.getDaiShouAmount2();
                            } else {
                                if (payID.startsWith("0") && payID.length() > 1)
                                   payID = payID.substring(1);
                                amt = (BigDecimal)zRecord.get("daiShouDetailAmount" + payID);
                            }

                            if (amt == null)
                               amt = new BigDecimal(0);
                            daiShouTotalAmount = daiShouTotalAmount.add(amt);
                            
                            accdayrptRecord.put(POS_AMOUNT, daiShouTotalAmount);
                            BigDecimal storeAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (storeAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, daiShouTotalAmount);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, storeAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("4")) {//代付
                            if (payID.startsWith("0") && payID.length() > 1)
                               payID = payID.substring(1);
                            BigDecimal amt = (BigDecimal)zRecord.get("daiFuDetailAmount" + payID);
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("5")) {//支付
                            if (payID.length() < 2)
                               payID += "0";
                            BigDecimal amt = (BigDecimal)zRecord.get("pay" + payID + "Amount");
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("7")) {//Paid-In
                            int id = Integer.parseInt(payID);
                            if (id < 0)
                                continue;
                            BigDecimal amt = (BigDecimal)zRecord.get("paidInDetailAmount" + id);
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("8")) {//Paid-Out
                            int id = Integer.parseInt(payID);
                            if (id < 0)
                                continue;
                            BigDecimal amt = (BigDecimal)zRecord.get("paidOutDetailAmount" + Integer.parseInt(payID));
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("9")) {//门市费用
                            int id = Integer.parseInt(payID);
                            if (id < 0)
                                continue;
                            BigDecimal amt = (BigDecimal)zRecord.get("paidOutDetailAmount" + Integer.parseInt(payID));
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            }
                        } else if (accountNumber.startsWith("101")) {
                            int id = Integer.parseInt(payID);
                            if (id < 0)
                                continue;
                            BigDecimal amt = (BigDecimal)zRecord.get("netSaleTax" + id + "Amount");
                            //CreamToolkit.logMessage("" + accountNumber + "zAmount = " + amt);
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        } else if (accountNumber.startsWith("102")) {
                            int id = Integer.parseInt(payID);
                            if (id < 0)
                                continue;
                            BigDecimal amt = (BigDecimal)zRecord.get("taxAmount" + id);
                            //CreamToolkit.logMessage("" + accountNumber + "zAmount = " + amt);
                            if (amt == null)
                               amt = new BigDecimal(0);
                            BigDecimal amt2 = (BigDecimal)accdayrptRecord.get(POS_AMOUNT);
                            if (amt2 == null)
                               amt2 = new BigDecimal(0);
                            //CreamToolkit.logMessage("" + accountNumber + ":Original amount = " + amt2);
                            //CreamToolkit.logMessage("" + accountNumber +  ":Additional amount = " + amt);
                            amt2 = amt2.add(amt);
                            accdayrptRecord.put(POS_AMOUNT, amt2);
                            BigDecimal sAmt = (BigDecimal)accdayrptRecord.get(STORE_AMOUNT);
                            if (sAmt == null) {
                                accdayrptRecord.put(STORE_AMOUNT, amt2);
                            } else {
                                accdayrptRecord.put(STORE_AMOUNT, sAmt.add(amt));
                            }
                        }
                    }
//                    accdayrptRecord.put("updateUserID", z.getCashierNumber());
//                    accdayrptRecord.put("updateDate", new java.util.Date());
                    accdayrptRecord.put("updateuserid", z.getCashierNumber());
                    accdayrptRecord.put("updatedate", new java.util.Date());

                    String uWhere;
                    if (isServerSideOracle)
                        uWhere =" WHERE storeID = '" + storeID
                       + "' AND accountDate = TO_DATE('" + accDate + "','YYYY-MM-DD HH24:MI:SS') AND posNumber = '" + cposNumber
                       + "' AND accountNumber = '" + accountNumber + "'";
                    else
                         uWhere =" WHERE storeID = '" + storeID
                       + "' AND accountDate = '" + accDate + "' AND posNumber = '" + cposNumber
                       + "' AND accountNumber = '" + accountNumber + "'";

                   
                    String fieldName = "";
                    String nameParams = "";
                    Object fieldValue = new Object();
                    Iterator iterator = accdayrptRecord.keySet().iterator();
                    while (iterator.hasNext()) {
                        fieldName = (String)iterator.next();
                        if (accdayrptRecord.get(fieldName) == null) {
                            fieldValue = new Object();
                        } else {
                            fieldValue = accdayrptRecord.get(fieldName);
                            if (fieldValue instanceof java.util.Date) {
                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                java.util.Date d = (java.util.Date)fieldValue;
                                if (isServerSideOracle)
                                    nameParams  += fieldName + "=" + "TO_DATE('" + df.format(d).toString() + "','YYYY-MM-DD HH24:MI:SS'),";
                                else
                                    nameParams  += fieldName + "=" + "'" + df.format(d).toString() + "',";
                            } else if (fieldValue instanceof String) {
                                nameParams += fieldName + " = " + "'" + fieldValue + "',";
                            } else {
                                nameParams += fieldName + " = " +  fieldValue + ",";
                            }
                        }
                    }
                    String finalStr = updateString + nameParams.substring(0, nameParams.length()-1) + "  " + uWhere;//
                    try {
                    	connection = CreamToolkit.getPooledConnection();
                        statement = connection.createStatement();
                        statement.execute(finalStr);
                        statement.close();
                    } catch (SQLException e) {
                        Server.log("SQL statement: " + finalStr);
                        e.printStackTrace(Server.getLogger());
                        returnValue = false;
                    } finally {
                        try {
                            if (statement != null)
                                statement.close();
                        } catch (SQLException e) {
                            e.printStackTrace(Server.getLogger());
                        }
                        if (connection != null)
                            CreamToolkit.releaseConnection(connection);
                    }
                }
            } // Iterate ZReport loop
        } // synchronization block
        return returnValue;
    }

    public synchronized boolean afterReceivingTransaction(Object[] trans) {

        int i;
        Transaction tran = null;

        int lineItemShouldHave = 0;
        int lineItemActualHave = 0;
        int posNumber = 0;
        int transactionNumber = 0;        
        for (i = 0; i < trans.length; i++) {
            if (trans[i] instanceof Transaction) {
                tran = (Transaction)trans[i];
                lineItemShouldHave = tran.getDetailCount().intValue();
                posNumber = tran.getTerminalNumber().intValue();
                transactionNumber = tran.getTransactionNumber().intValue();
            } else if (trans[i] instanceof LineItem) {
                lineItemActualHave++;
                LineItem li = (LineItem)trans[i];
                if (tran != null) {

                    // Because there is no connection between Transaction and
                    // LineItem objects sent from POS
                    tran.addLineItemSimpleVersion(li);
                    li.setSystemDateTime(tran.getSystemDateTime());
                }
            } else if (trans[i] instanceof TokenTranDtl) {
                TokenTranDtl ttd = (TokenTranDtl) trans[i];
                if (tran != null){
                     tran.addTokenTrandtlSimpleVersion(ttd);
                     ttd.setSystemDate(tran.getSystemDateTime());
                }

            }
        }

        Server.log("[" + posNumber + "] add transaction (no = " + transactionNumber + ") begin...");

        // integrity check, if has problem, discard it.
        if (lineItemShouldHave != lineItemActualHave) {
            Server.log("Transaction (pos=" + posNumber + ", no=" + transactionNumber
                + ") is incomplete. Detail count should be " + lineItemShouldHave
                + ", but actual detail count is " + lineItemActualHave);
            return false;
        }

        PostObjectData pod = new PostObjectData(posNumber,transactionNumber,tran.getSystemDateTime());
        pod.save(tran);
        
        Server.log("[" + posNumber + "] add transaction (no = " + transactionNumber + ") end");
        return true;
    }

    public synchronized boolean afterReceivingDepSales(Object[] depSales) {
        boolean success = true;
        int zNumber = ((DepSales)depSales[0]).getSequenceNumber();
        int posNumber = ((DepSales)depSales[0]).getPOSNumber();
        Date accDate = getAccountingDate(posNumber, zNumber);
        
        //删除已经存在的
        DepSales.deleteBySequenceNumber(zNumber,posNumber);
        
        for (int i = 0; i < depSales.length; i++) {
            DepSales ds = (DepSales)depSales[i];
            ds.setAccountDate(accDate);
            if (!ds.insert(false)) {
                Server.log("DepSales insert failed (pos=" + posNumber 
                         + ", z=" + zNumber 
                         + ", id=" + ds.getDepID() 
                         + ").");
                
            } else {            
                Server.log("DepSales inserted (pos=" + posNumber 
                         + ", zNumber=" + zNumber
                         + ", id=" + ds.getDepID() 
                         + ")."); 
            }
        }
        return success;
    }

    public synchronized boolean afterReceivingDaishouSales(Object[] daishouSales) {
        boolean success = true;
        int zNumber = ((DaishouSales)daishouSales[0]).getZSequenceNumber();
        int posNumber = ((DaishouSales)daishouSales[0]).getPosNumber();
        Date accDate = getAccountingDate(posNumber,zNumber);
        
        DaishouSales.deleteBySequenceNumber(zNumber, posNumber);
        
        for (int i = 0; i < daishouSales.length; i++) {
            DaishouSales ds = (DaishouSales)daishouSales[i];
            ds.setAccountDate(accDate);
            if (!ds.insert(false)) {
                Server.log("DaishouSales inserted failed (pos=" + posNumber +
                    ",z=" + zNumber +
                    ",id1=" + ds.getFirstNumber() +
                    ",id2=" + ds.getSecondNumber() + ").");
                success = false;
            } else {
                Server.log("DaishouSales inserted (pos=" + posNumber +
                    ",z=" + zNumber +
                    ",id1=" + ds.getFirstNumber() +
                    ",id2=" + ds.getSecondNumber() + ").");
            }
        }
        return success;
    }

    /**
     * afterReceivingDaiShouSales2
     *
     * @return 如果代收资料存入SC数据库中成功则返回 true，否则返回 false.
     */
    public synchronized boolean afterReceivingDaiShouSales2(Object[] daiShouSales2) {
        boolean success = true;
        if (daiShouSales2.length == 0) 
            return success;
       
        int zNumber = ((DaiShouSales2)daiShouSales2[0]).getZSequenceNumber();
        int posNumber = ((DaiShouSales2)daiShouSales2[0]).getPosNumber();
        
        DaiShouSales2.deleteBySequenceNumber(zNumber, posNumber);
        
        for (int i = 0; i < daiShouSales2.length; i++) {
            DaiShouSales2 ds = (DaiShouSales2)daiShouSales2[i];
            ds.setAccountDate(getAccountingDate(posNumber, zNumber, ds.getPayTime()));
            if (!ds.insert(false)) {
                Server.log("DaiShouSales2 insert failed (StoreID = " + ds.getStoreID() 
                + ", POS = " + posNumber
                + ", Z = " + zNumber
                + ", ID = " + ds.getID() 
                + ")");
                success = false;
            } else {
                Server.log("DaiShouSales2 inserted (StoreID = " + ds.getStoreID() 
                + ", POS = " + posNumber
                + ", Z = " + zNumber
                + ", ID = " + ds.getID() 
                + ")");
            }
        }
        return success;
    }

    /**
     * Inventory processor.
     *
     * @param inventory Inventory objects.
     * @return true if success, otherwise false.
     */
    public boolean afterReceivingInventory(Object[] inventory) {
        int insertCount = 0;
        int updateCount = 0;
        int failCount = 0;
        for (int i = 0; i < inventory.length; i++) {
            Inventory ds = (Inventory)inventory[i];
            if (!ds.insert(false)) {
                //Server.log("Inventory exists (" 
                //    + "pos=" + ds.getPOSNumber()
                //    + ", seq=" + ds.getItemSequence()
                //    + "), delete first.");
                ds.deleteByPrimaryKey();
                if (!ds.insert(false)) {
                    Server.log("Inventory inserted failed (" 
                        + "pos=" + ds.getPOSNumber()
                        + ", seq=" + ds.getItemSequence()
                        + ")");
                    failCount++;
                } else {
                    //Server.log("Inventory inserted (" 
                    //    + "pos=" + ds.getPOSNumber()
                    //    + ", seq=" + ds.getItemSequence()
                    //    + ")");
                    updateCount++;
                }
            } else {
                insertCount++;
            }
        }
        //Server.log("Inventory data uploaded (insert:" + insertCount + ", update:" 
        //    + updateCount + ", fail:" + failCount + ").");
        return (failCount == 0);
    }

    /**
     * Master download preprocessor.
     *
     * @return true if it allows master downloading now, false otherwise.
     */
    public boolean beforeDownloadingMaster() {
        return !isDateShiftRunningOrFailed(DATE_BIZDATE);
    }
    
    /**
     * Meyer / 2002-02-11
     * Check before synchronize transaction
     * 
     * @return true if it allows synchronize transaction, false otherwise.
     */
    public boolean canSyncTransaction() {
        return !isDateShiftRunning(DATE_BIZDATE);
    }
}
