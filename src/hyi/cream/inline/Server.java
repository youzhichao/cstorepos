package hyi.cream.inline;

import java.io.*;
import java.net.*;
import java.util.*;
import java.sql.*;
import java.awt.*;
import java.text.*;
import javax.swing.UIManager;
import hyi.cream.util.*;
import hyi.cream.dac.*;

/**
 * Inline server(singleton).
 *
 * @author Bruce
 */
public class Server implements POSStatusListener {

    /**
     * /Bruce/1.5/2002-04-03/
     *    Add starting up log message.
     */
    transient public static final String VERSION = "1.5";

    public static final int INLINE_SERVER_TCP_PORT = 3000;
    public static final String INLINE_SERVER_DOMAIN_NAME = "sc";

    private ServerSocket serverSocket;
    private static Server server;
    private ArrayList listener = new ArrayList();

    static private PrintWriter logger;
    static private DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
    public static boolean debugLog = false;
	static private boolean fakeExist = false;
    static private boolean updateInv = false;

    private static void init() {
        try {
            CreamProperties prop = CreamProperties.getInstance();
            CreamToolkit.getLogLevel();// init db pools
            String logFilePath = prop.getProperty("InlineServerLogFile");
            if (logFilePath != null) {
                //logger = new PrintWriter(new FileWriter(
                //    CreamProperties.getInstance().getProperty("InlineServerLogFile"),
                //    true));
                logger = new PrintWriter(
                    new OutputStreamWriter(
                        new FileOutputStream(prop.getProperty("InlineServerLogFile"), true), CreamToolkit.getEncoding()));
                    
            } else {
                //logger = new PrintWriter(new FileWriter("/root/inline.log", true));
                logger = new PrintWriter(
                    new OutputStreamWriter(
                        new FileOutputStream("/root/inline.log", true), CreamToolkit.getEncoding()));
            }

            String debug = prop.getProperty("DebugInlineServer");
            if (debug != null && debug.compareToIgnoreCase("yes") == 0) {
                debugLog = true;
            }

            String updateInvStr = prop.getProperty("UpdateInventory", "yes");
            updateInv = updateInvStr.equalsIgnoreCase("yes");
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    synchronized static public void log(String message) {
        if (logger != null) {
            message = dateFormatter.format(new java.util.Date()) + message;
            System.out.println(message);
            logger.println(message);
            logger.flush();
        }
    }

    static PrintWriter getLogger() {
        return logger;
    }

    public Server() throws InstantiationException {
        if (server != null)
            throw new InstantiationException();

        try {
            serverSocket = new ServerSocket(INLINE_SERVER_TCP_PORT);
            server = this;
            init();
        } catch (IOException e2) { // if an I/O error occurs when creating the socket.
            System.err.println(e2);
            throw new InstantiationException();
        } catch (SecurityException e3) { // a security manager exists and its checkConnect method doesn't allow the operation
            System.err.println(e3);
            throw new InstantiationException();
        }
    }



    /**
     * 为Server端准备主档文件所用
	 * @param b no matter true or false;
	 */
    Server(boolean b) {
        server = this;
        init();
	}
    
    public static void setFakeExist() {
		fakeExist = true;
	}

    public static boolean serverExist() {
        return (fakeExist) ? true : (server != null);
    }

    public static Server getInstance() throws InstantiationException {
        if (server == null)
            server = new Server();
        return server;
    }

    public void startup() {
        log("Inline server is starting up.");
        
        //the tpt thread is a mutable thread that is running to process transactions
        TranProcessorThread tpt = new TranProcessorThread("TranProcessorThread");
        tpt.start();

        //the dog will be watching on the threads 
        //if the thread is dead, then the dog will restart it. 
        ThreadWatchDog dog = ThreadWatchDog.getInstance();
        dog.setLogger(getLogger());
        dog.put(tpt);
        dog.start();
        
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                log("----------  A SOCKET SESSION STARTED!  ----------");
        		log(socket.getInetAddress().getHostAddress() + ":" + socket.getPort() + " is connected.");
                ServerThread t = new ServerThread(socket);
                t.start();
            } catch (IOException e1) {
                e1.printStackTrace(getLogger());
            } catch (InstantiationException e2) {
                e2.printStackTrace(getLogger());
            }
        }
    }

    public void closeServerSocket() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace(getLogger());
        }
    }

    public void addPOSStatusListener(POSStatusListener l) {
        listener.add(l);
    }

    private Iterator getPOSStatusListener() {
        return listener.iterator();
    }

    /**
     * Fire event to every POSStatusListener
     */
    private void fireEvent(POSStatusEvent e) {
        Iterator iter = getPOSStatusListener();
        while (iter.hasNext()) {
            POSStatusListener l = (POSStatusListener)iter.next();
            l.posStatusChanged(e);
        }
    }

    synchronized public void posStatusChanged(POSStatusEvent e) {
        fireEvent(e);
    }

    public void createServerGUI() {
        boolean packFrame = false;

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(Exception e) {
            e.printStackTrace(getLogger());
        }
        final ServerGUIFrame frame = new ServerGUIFrame();
        //Center the window
        //frame.setUndecorated(true);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = new Dimension(600, 400);
        frame.setSize(frameSize);
        if (frameSize.height > screenSize.height) {
            frameSize.height = screenSize.height;
        }
        if (frameSize.width > screenSize.width) {
            frameSize.width = screenSize.width;
        }
        frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);

        //Validate frames that have preset sizes
        //Pack frames that have useful preferred size info, e.g. from their layout

        if (packFrame) {
            frame.pack();
        }
        else {
            frame.validate();
        }
        frame.myInit();
        frame.setVisible(true);
    }

    /**
     * Server main().
     */
    public static void main(String[] args) {
        Server c;
        try {

            if (args.length != 0 && args[0].equalsIgnoreCase("-getMaster")) {
                c = new Server(true);
                c.prepareMasterFiles();
                return;  
            }
            
            c = Server.getInstance();
             
            shrinkLogFiles();
            if (args.length != 0 ) {
                if(args[0].equals("-gui")) 
                    c.createServerGUI();
                
            }
            new TransactionCheckerThread().start();
//            c.startup();
            new Thread() {
                public void run() {
                    try {
                        Server.getInstance().startup();
                    } catch (InstantiationException e) {
                        e.printStackTrace(getLogger());
                    }
                }
            }.start() ;
            
        } catch (Exception e) {
            e.printStackTrace(getLogger());
        }
    }

    private static void prepareMasterFiles() {
        String classNames[] = {
            "hyi.cream.dac.PLU",
            "hyi.cream.dac.Cashier",
            "hyi.cream.dac.Reason",
            "hyi.cream.dac.MixAndMatch",
            "hyi.cream.dac.Payment",
            "hyi.cream.dac.TaxType",
            "hyi.cream.dac.Dep",
            "hyi.cream.dac.Category",
            "hyi.cream.dac.Store",
            "hyi.cream.dac.MMGroup",
            "hyi.cream.dac.CombDiscountType",
            "hyi.cream.dac.CombGiftDetail",
            "hyi.cream.dac.CombPromotionHeader",
            "hyi.cream.dac.CombSaleDetail", 
            "hyi.cream.dac.DaiShouDef",
            "hyi.cream.dac.SI",
            "hyi.cream.dac.Rebate",           
            "hyi.cream.dac.DiscountType"
        }; 
        
        for (int i = 0; i < classNames.length; i++) {
            try {
                Server.log("Prepare download sql file for " + classNames[i] + " ... ");
                getDownloadSQLFile(classNames[i]);
            } catch (Exception e) {
                e.printStackTrace(getLogger());
            }
        }
    }
    
    private synchronized static void getDownloadSQLFile(String className) throws Exception{
        
        File sqlFilePath = new File(CreamToolkit.getConfigDir() + className); 
        if (sqlFilePath.exists()) 
            sqlFilePath.delete();

        File recCountFilePath = new File(sqlFilePath.getAbsolutePath() + ".cnt");
        if (recCountFilePath.exists())
            recCountFilePath.delete();
        
        //generate the download SQL file
        // Invoke the DAC's getAllObjectsForPOS()
        //System.out.println("Invoking hyi.cream.dac.PLU.getAllObjectsForPOS...");                
        Collection dacs =
            (Collection) Class.forName(className).getDeclaredMethod(
                "getAllObjectsForPOS",
                new Class[0]).invoke(
                null,
                new Object[0]);
        if (dacs == null || dacs.size() == 0)
            return;
            
        Object dacArray[] = dacs.toArray();
        
        //System.out.println("Creating download file: " + sqlFilePath + "...");
        OutputStream sqlFile = new BufferedOutputStream(new FileOutputStream(sqlFilePath));
        for (int i = 0; i < dacArray.length; i++) {
            DacBase dac = (DacBase) dacArray[i];
            try {
                String insertString = dac.getInsertSqlStatement(true);
                sqlFile.write(insertString.getBytes());
                sqlFile.write(';');
                sqlFile.write('\n');
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        sqlFile.close();

        // create record count file
        //System.out.println("Creating download file: " + recCountFilePath + "...");
        DataOutputStream recCountFile = new DataOutputStream(new FileOutputStream(recCountFilePath));
        recCountFile.write((dacArray.length + "").getBytes());
        recCountFile.close();

    }
    
    
    public static void shrinkLogFiles() {
        /*
        try {
            Runtime.getRuntime().exec("/usr/bin/shrinkfile -s 10m /root/cream.log /root/inline.log");
        } catch (IOException e) {
                e.printStackTrace(CreamToolkit.getLogger());
        }
        */
        logger.close();
        logger = null;
        CreamToolkit.closeLog();
        String f = CreamProperties.getInstance().getProperty("InlineServerLogFile");
        if (f != null)
            CreamToolkit.shrinkFile(new File(f), 10L * 1024 * 1024);
        f = CreamProperties.getInstance().getProperty("LogFile");
        if (f != null)
            CreamToolkit.shrinkFile(new File(f), 10L * 1024 * 1024);
        CreamToolkit.openLogger();
        init();
    }

    /**
     * 上传交易时是否更新库存表。
     * 
     * @return true if need update Inventory.
     */
    public static boolean isUpdateInv() {
        return updateInv;
    }
}
