package hyi.cream.inline;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;

public class ServerGUIFrame extends JFrame {
    JPanel contentPane;
    JToolBar jToolBar = new JToolBar();
    JButton AboutButton = new JButton();
    //ImageIcon image1;
    //ImageIcon image2;
    //ImageIcon image3;
    BorderLayout borderLayout1 = new BorderLayout();
    JPanel mainPanel = new JPanel();
    GridBagLayout gridBagLayout1 = new GridBagLayout();
    JScrollPane jTableScrollPane = new JScrollPane();
    JLabel logLabel = new JLabel();
    JScrollPane logScrollPane = new JScrollPane();
    POSStatusTableModel model = new POSStatusTableModel();
    JTable posStatusTable = new JTable(model);
    //ListModel logModel = new LogListModel();
    //JList logList = new JList(logModel);
    Border border1;
    Border border2;

    /**Construct the frame*/
    public ServerGUIFrame() {
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try {
            jbInit();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

    }

    public void myInit() {
        // Create default table cell renderer
        //TableCellRenderer renderer = new CaposinoTableHeaderRenderer(this);

        for (int i = 0; i < model.getColumnCount(); i++) {
            //TableCellRenderer custRenderer;

            // Make each column has distinct identifier
            posStatusTable.getColumn(model.getColumnName(i)).setIdentifier(new Integer(i));
            TableColumn tbcol = posStatusTable.getColumn(new Integer(i));

            // Set prefered header width
            int width = model.getPreferedColumnWidth(i);
            if (width >= 0) {
                tbcol.setPreferredWidth(width);
                //System.out.println("set width " + width);
            }

            //// Set column header renderer
            //if ((custRenderer = model.getTableColumnHeaderRenderer(i)) == null) {
            //    tbcol.setHeaderRenderer(renderer);
            //} else
            //    tbcol.setHeaderRenderer(custRenderer);
        }
        posStatusTable.setRowHeight(32);
        //model.setupDefaultCellRenderer(this);
    }

    /**Component initialization*/
    private void jbInit() throws Exception  {
        //image1 = new ImageIcon(hyi.cream.inline.ServerGUIFrame.class.getResource("openFile.gif"));
        //image2 = new ImageIcon(hyi.cream.inline.ServerGUIFrame.class.getResource("closeFile.gif"));
        //image3 = new ImageIcon(hyi.cream.inline.ServerGUIFrame.class.getResource("help.gif"));
        //setIconImage(Toolkit.getDefaultToolkit().createImage(ServerGUIFrame.class.getResource("[Your Icon]")));
        contentPane = (JPanel) this.getContentPane();
        border1 = BorderFactory.createMatteBorder(4,4,4,4,Color.gray);
        border2 = BorderFactory.createLineBorder(Color.darkGray,1);
        contentPane.setLayout(borderLayout1);
        this.setSize(new Dimension(400, 300));
        this.setTitle("Inline Server Monitor");
        //this.addWindowListener(new java.awt.event.WindowAdapter() {
        //    public void windowClosing(WindowEvent e) {
        //        this_windowClosing(e);
        //    }
        //});
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        AboutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AboutButton_actionPerformed(e);
            }
        });
        AboutButton.setBorder(border2);
        AboutButton.setMaximumSize(new Dimension(42, 27));
        AboutButton.setMinimumSize(new Dimension(42, 27));
        AboutButton.setToolTipText("About");
        AboutButton.setText("About");
        mainPanel.setLayout(gridBagLayout1);
        logLabel.setText("Log:");
        logScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        logScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        jToolBar.add(AboutButton);
        contentPane.add(mainPanel,  BorderLayout.CENTER);
        mainPanel.add(jTableScrollPane,        new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0
            ,GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        jTableScrollPane.getViewport().add(posStatusTable, null);
        mainPanel.add(logLabel,      new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        //mainPanel.add(logScrollPane,      new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0
        //    ,GridBagConstraints.SOUTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        //logScrollPane.getViewport().add(logList, null);
        contentPane.add(jToolBar,  BorderLayout.SOUTH);
    }

    /**File | Exit action performed*/
    public void jMenuFileExit_actionPerformed(ActionEvent e) {
        System.exit(0);
    }

    /**Help | About action performed*/
    public void jMenuHelpAbout_actionPerformed(ActionEvent e) {
    }

    /**Overridden so we can exit when window is closed*/
    protected void processWindowEvent(WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == WindowEvent.WINDOW_CLOSING) {
            if (0 == JOptionPane.showConfirmDialog(this,
                "关闭将会导致前台POS机离线，你确认关闭吗？", "！！关闭连线！！", JOptionPane.YES_NO_OPTION)) {
                //this_windowClosing(null);
                ServerThread.setPOSConnectionState(1, false);
                ServerThread.setPOSConnectionState(2, false);
                ServerThread.setPOSConnectionState(3, false);
                ServerThread.setPOSConnectionState(4, false);
                ServerThread.setPOSConnectionState(5, false);
                System.exit(0);
            }
        }
    }

    void this_windowClosing(WindowEvent e) {
        try {
            Server server = Server.getInstance();
            if (server != null)
                server.closeServerSocket();
        } catch (InstantiationException e2) {
        }
    }

    void AboutButton_actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this,
            "Inline Server Monitor Version 1.0 \nCopyright(c) 2002 HYI Co., Ltd. All Rights Reserved.",
            "About", JOptionPane.INFORMATION_MESSAGE);
    }
}