package hyi.cream.inline;

import java.net.*;
import java.io.*;
import java.util.*;
import java.util.zip.GZIPOutputStream;
import java.sql.*;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.*;
import hyi.cream.util.*;
import hyi.cream.dac.*;

/**
 * Inline server thread class.
 *
 * @author Bruce
 * @version 1.7
 */
public class ServerThread extends Thread {

	/**
	 * /Bruce/1.8/2003-05-27/
	 *    修改getobject protocol, 以增加查询会员资料功能。
	 * 
	 * /Bruce/1.7/2002-04-27/
	 *    增加上传代收明细帐.
	 *
	 * /Bruce/1.6/2002-04-10/
	 *    在下传主档之前，判断是否现在正在进行营业换日，若是，或者换日失败，
	 *    则禁止做主档下传。
	 *
	 * /Bruce/1.5/2002-04-01/
	 *    1. syncTransaction(): If date shift is running, don't send any request.
	 *    2. Keep log file size to at least 10 MB.
	 */
	transient public static final String VERSION = "1.8";
	transient public static final int DEFAULT_TIME_OUT = 70000;
	transient public static final int LONG_TIME_OUT = 130000;

	private Socket socket;
	private DataInputStream in;
	private DataOutputStream out;
	private ObjectInputStream objectIn;
	private ObjectOutputStream objectOut;
	private DacObjectOutputStream dacOut;
	private DacObjectInputStream dacIn;
	private StringBuffer buf = new StringBuffer();
	private int posNumber;
	private ArrayList listener = new ArrayList();
	private POSStatusEvent event = new POSStatusEvent(this, posNumber, 0, false, false, "");
	private boolean enableLog = true;
	private static String connectionStatusFile;
	private boolean hasBeenWrittenConnectionStatusFile;
	//private DateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss ");

	/**
	 * Meyer/2003-02-10
	 * 		Get PostProcessor
	 */
	private static PostProcessor pp = getPostProcessor();

	private static PostProcessor getPostProcessor() {
        PostProcessor p = null;
        boolean isValidPostProcessor = false;
        try {
            connectionStatusFile = CreamProperties.getInstance().getProperty("ConnectionStatusFile");
            String postProcessorClass = CreamProperties.getInstance().getProperty("PostProcessor");

            if (postProcessorClass != null)
            p = (PostProcessor) (Class.forName(postProcessorClass).newInstance());
 
            isValidPostProcessor = true;
        } catch (Exception e) {
            e.printStackTrace(Server.getLogger());
        }
        
        if (!isValidPostProcessor) {
            Server.log("Invlaid PostProcessor Property! Default PostProcessor Used");
            p = new CStorePostProcessor(); //Default PostProcessor
        }    
        return p;            
	}
    
    ServerThread(Socket socket) throws InstantiationException {
		if (Server.serverExist())
			addPOSStatusListener(Server.getInstance());

		this.socket = socket;
//		log(socket.getInetAddress().getHostName() + ":" +
		if (pp == null) 
            pp = getPostProcessor();
        
        try {
			socket.setSoTimeout(DEFAULT_TIME_OUT);
			out = new DataOutputStream(socket.getOutputStream());
			in = new DataInputStream(socket.getInputStream());
			dacOut = new DacObjectOutputStream(socket.getOutputStream());
			dacIn = new DacObjectInputStream(socket.getInputStream());
			//objectIn = new ObjectInputStream(new BufferedInputStream(in));
		} catch (IOException e) {
			log(e.toString());
			throw new InstantiationException();
		}
		log(socket.getInetAddress().getHostAddress() + ":" + socket.getPort() + " is connected.");
	}

	private void log(String message) {
		if (!enableLog)
			return;

		if (posNumber == 0)
			message = "[ ] " + message;
		else
			message = "[" + posNumber + "] " + message;

		fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_LOGGING, false, false, message));
		Server.log(message);
	}

	public int getPOSNumber() {
		return posNumber;
	}

	public void addPOSStatusListener(POSStatusListener l) {
		listener.add(l);
	}

	private Iterator getPOSStatusListener() {
		return listener.iterator();
	}

	/**
	 * Fire event to every POSStatusListener
	 */
	private void fireEvent(POSStatusEvent e) {
		Iterator iter = getPOSStatusListener();
		while (iter.hasNext()) {
			POSStatusListener l = (POSStatusListener) iter.next();
			l.posStatusChanged(e);
		}
	}

	private boolean putCompressedFile(String fileName) {

		//OutputStream buffOut = new BufferedOutputStream(out);
		File file = new File(fileName);
		InputStream read = null;

		if (file.isFile()) {
			try {
				GZIPOutputStream z = new GZIPOutputStream(socket.getOutputStream());
				DataOutputStream zipOut = new DataOutputStream(z);
				//out.writeInt(1);
				//putMessage(file.getName());         // file name
				log("Send " + file.getName());
				read = new BufferedInputStream(new FileInputStream(file));
				zipOut.writeInt((int) file.length()); // file size
				byte[] all = new byte[(int) file.length()];
				//int remainLength = read.available();
				read.read(all);

				// file content
				//buffOut.write(all);
				//buffOut.flush();
				zipOut.write(all);
				zipOut.flush();

				////for (int j = 0; j < (all.length - 1) / 4096 + 1; j++) {
				////    zipOut.write(all, j * 4096, Math.min(remainLength, 4096));
				////    zipOut.flush();
				////    //System.out.println("sent " + Math.min(remainLength, 4096) + " bytes");
				////    remainLength -= 4096;
				////}
				read.close();
				z.finish();
				//z.flush();
			} catch (IOException fe) {
				log(fe.toString());
				fe.printStackTrace();
				if (read != null) {
					try {
						read.close();
					} catch (IOException e) {
					}
				}
				return false;
			}
		}
		return true;
	}

	private static String[] getDownloadSQLFile(String className) throws Exception {

		String[] retFiles = new String[2];
        java.util.Date today = new java.util.Date();
		synchronized (ServerThread.class) {

			// Check if download cache file exists, if yes, return directly
            File sqlFilePath = new File(CreamToolkit.getConfigDir() + className); 
            File recCountFilePath = new File(sqlFilePath.getAbsolutePath() + ".cnt");

            boolean filesExist = sqlFilePath.exists() && recCountFilePath.exists();
//            boolean isFilesValid = false;   //最后修改时间距离当前时间8小时内有效
//            if(filesExist) 
//                isFilesValid = (today.getTime() - sqlFilePath.lastModified() < 1000L * 3600 * 8);
//            
            if (!filesExist) {
				//regenerate the download SQL file

				if (filesExist) {
                    sqlFilePath.delete();
                    recCountFilePath.delete();
                }
                
                // Invoke the DAC's getAllObjectsForPOS()
				//System.out.println("Invoking hyi.cream.dac.PLU.getAllObjectsForPOS...");                
				Collection dacs =
					(Collection) Class.forName(className).getDeclaredMethod(
						"getAllObjectsForPOS",
						new Class[0]).invoke(
						null,
						new Object[0]);
				if (dacs == null || dacs.size() == 0)
					return null;

				Object dacArray[] = dacs.toArray();

				OutputStream sqlFile = new BufferedOutputStream(new FileOutputStream(sqlFilePath));
				for (int i = 0; i < dacArray.length; i++) {
					DacBase dac = (DacBase) dacArray[i];
					try {
						String insertString = dac.getInsertSqlStatement(true);
						sqlFile.write(insertString.getBytes());
						sqlFile.write(';');
						sqlFile.write('\n');
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				sqlFile.close();

				// create record count file
				//System.out.println("Creating download file: " + recCountFilePath + "...");
				DataOutputStream recCountFile = new DataOutputStream(new FileOutputStream(recCountFilePath));
				recCountFile.write((dacArray.length + "").getBytes());
				recCountFile.close();
			}
			retFiles[0] = sqlFilePath.getAbsolutePath();
			retFiles[1] = recCountFilePath.getAbsolutePath();
			return retFiles;
		}
	}

	/**
	 * Inline protocol:
	 * 
	 *                              Client                                             Server
	 *                              ------                                             ------
	 *                                 |                                                  |
	 *                                 |                "quit\n"                          |
	 *                                 | -----------------------------------------------> |
	 * 
	 *                             
	 *                             sendRuok()
	 *                                 |                                                  |
	 *                                 |                "ruok?{Store Number}\n"           |
	 *                                 | -----------------------------------------------> |
	 *                                 |            {POS number(int)}                     |
	 *                                 | -----------------------------------------------> |
	 *                                 |         "yes[max id in posdl_deltaplu]\n"        |
     *                                 |         or "no\n"                                | if store number is not the same as SC
	 *                                 | <----------------------------------------------- |
	 *                                 |                                                  |
	 * 
	 * 
	 *  NOT YET IMPLEMENTED ->         |                                                  |
	 *                                 |                "ruok?\n"                         |
	 *                                 | -----------------------------------------------> |
	 *                                 |            {POS number(int)}                     |
	 *                                 | -----------------------------------------------> |
	 *                                 |                "getzlist\n"                      | piggyback command
	 *                                 | <----------------------------------------------- |
	 *                                 |                {number of z requested(int)}      |
	 *                                 | <----------------------------------------------- |
	 * Client retrieves at most number |     {number of z(int)}                           |
	 * of z requested                  | -----------------------------------------------> |
	 *                                 |     {first ZReport}                              |
	 *                                 | -----------------------------------------------> |
	 *                                 |     {second ZReport}                             |
	 *                                 | -----------------------------------------------> |
	 *                                 |       . . .                                      |
	 * 
	 * 
	 *                                 |                                                  |
	 *  NOT YET IMPLEMENTED ->         |                "ruok?\n"                         |
	 *                                 | -----------------------------------------------> |
	 *                                 |            {POS number(int)}                     |
	 *                                 | -----------------------------------------------> |
	 *                                 |                "getshiftlist\n"                  |
	 *                                 | <----------------------------------------------- |
	 *                                 |                {number of shift requested(int)}  |
	 *                                 | <----------------------------------------------- |
	 * Client retrieves at most number |     {number of shift(int)}                       |
	 * of shift requested              | -----------------------------------------------> |
	 *                                 |     {first ShiftReport}                          |
	 *                                 | -----------------------------------------------> |
	 *                                 |     {second ShiftReport}                         |
	 *                                 | -----------------------------------------------> |
	 *                                 |       . . .                                      |
	 * 
	 * 
	 *                             sendSyncTransaction()
	 *                                 |                                                  |
	 *                                 |    "synctrans\n"                                 |
	 *                                 | -----------------------------------------------> |
	 *                                 |    {POS number(int)}                             |
	 *                                 | -----------------------------------------------> |
	 *                                 |    {Max transaction number(int)}                 |
	 *                                 | -----------------------------------------------> |
     *                                 |    {Begin transaction number(int)}               |  
     *                                 | -----------------------------------------------> |
     *                                 |    {End transaction number(int)}                 |  
     *                                 | -----------------------------------------------> |
     *                                 |    {Number of transactions request(int)}         | 0 if nothing to request
	 *                                 | <----------------------------------------------- |
	 *                                 |    {First transaction number(int)}               |
	 *                                 | <----------------------------------------------- |
	 *                                 |    {Second transaction number(int)}              |
	 *                                 | <----------------------------------------------- |
	 *                                 |       . . .                                      |
	 * 
	 * 
	 *                             sendObject()                                       getObject()
	 *                                 |                                                  |
	 *                                 |   "putobject {class_name} [number_of_objects]"   |
	 *                                 | -----------------------------------------------> |
	 *                                 |         {serialized object stream}               | Send/recevie by DacObjectOutputStream/DacObjectInputStream
	 *                                 | -----------------------------------------------> |
	 *                                 |       . . .                                      |
	 *                                 |       . . .                                      |
	 *                                 |             "OK\n" or "Invalid command.\n"       |
	 *                                 | <----------------------------------------------- |
	 *
     * 
     *                             sendObject()                                       getObject()
     *                                 |                                                  |
     *                                 |   "putobject2\n"                                 |
     *                                 | -----------------------------------------------> |
     *                                 |   {A String object with the format:              |
     *                                 |  "classname\t                                    |
     *                                 |  "[class_name]\t[first_field]\t[second_field]\t...\n" +
     *                                 |  "[class_name]\t[first_field]\t[second_field]\t...\n" + 
     *                                 |  ...                                             |
     *                                 | -----------------------------------------------> |
     *                                 |             "OK\n" or "Invalid command.\n"       |
     *                                 | <----------------------------------------------- |
     *                              
	 *                             
	 *                             requestObjects()                                   putObject()
	 *                                 |                                                  |
	 *                                 |   "getobject {class_name} [arguments]\n"         | arguments is option
	 *                                 | -----------------------------------------------> |
	 *                                 |    {number of objects} (by an Integer object)    | -1 if server failed
	 *                                 | <----------------------------------------------- |
	 *                                 |                                                  | Retrieve objects by DAC's getAllObjectsForPOS(),
	 *                                 |                                                  | if arguments is not null, then will get by queryObject(argument).
	 *                                 |           {serialized object stream}             |
	 *                                 | <----------------------------------------------- | Send/recevie by DacObjectOutputStream/DacObjectInputStream.
	 *                                 |       . . .                                      | 
	 *                                 |       . . .                                      |
	 *                                 |                   "OK\n"                         |
	 *                                 | -----------------------------------------------> |
	 *                                 |                                                  |
	 * 
	 * 
	 *                             requestObjects2()                                  putObject()
	 *                                 |                                                  |
	 *                                 |   "getobject2 {class_name}\n"                    |
	 *                                 | -----------------------------------------------> |
	 *                                 |    {number of objects(int)}                      | Retrieve objects by DAC's getAllObjectsForPOS()
	 *                                 | <----------------------------------------------- | if it is 0, don't send INSERT stmt file
	 *                                 |   {INSERT statement file size(int)}              | 
	 *                                 | <----------------------------------------------- |
	 *                                 |   {INSERT statement file content}                |
	 *                                 | <----------------------------------------------- |
	 *                                 |                   "OK\n"                         |
	 *                                 | -----------------------------------------------> |
	 *                                 |                                                  |
	 *                             
	 * 
	 *                             requestFiles()                                     putFile()
	 *                                 |                                                  |
	 *                                 |  "getfile {server_dir_name}\n"                   |
	 *                                 | -----------------------------------------------> |
	 *                                 |               {number of files}                  | -1 if server failed
	 *                                 | <----------------------------------------------- |
	 *                                 |   {first file name}\n                            |
	 *                                 | <----------------------------------------------- |
	 *                                 |   {first file size(int)}                         |
	 *                                 | <----------------------------------------------- |
	 *                                 |            {first file content}                  |
	 *                                 | <----------------------------------------------- |
	 *                                 |   {second file name}\n                           |
	 *                                 | <----------------------------------------------- |
	 *                                 |   {second file size(int)}                        |
	 *                                 | <----------------------------------------------- |
	 *                                 |            {second file content}                 |
	 *                                 | <----------------------------------------------- |
	 *                                 |                   . . .                          |
	 *                                 | <----------------------------------------------- |
	 * 
	 * 
	 *                             requestFiles()                                     putFile()
	 *                                 |                                                  |
	 *                                 |  "movefile {server_dir_name} {client_dir_name}\n"|
	 *                                 | -----------------------------------------------> |
	 *                                 |               {number of files}                  | -1 if server failed
	 *                                 | <----------------------------------------------- |
	 *                                 |   {file name}\n                                  |
	 *                                 | <----------------------------------------------- |
	 *                                 |   {file size(int)}                               |
	 *                                 | <----------------------------------------------- |
	 *                                 |            {first file content}                  |
	 *                                 | <----------------------------------------------- |
	 *                                 |   {file name}\n                                  |
	 *                                 | <----------------------------------------------- |
	 *                                 |   {file size(int)}                               |
	 *                                 | <----------------------------------------------- |
	 *                                 |            {second file content}                 |
	 *                                 | <----------------------------------------------- |
	 *                                 |                   . . .                          |
	 *                                 | <----------------------------------------------- |
	 * 
	 * 
	 *         DacObjectOutputStream.writeObject(DacBase dac)              DacObjectInputStream.readObject()
	 *                                 |          {DAC classname(String)}                 |
	 *                                 | -----------------------------------------------> |
	 *                                 |          {number of fields(int)}                 |
	 *                                 | -----------------------------------------------> |
	 *                                 |    {first field name(String)}                    |
	 *                                 | -----------------------------------------------> |
	 *                                 |    {first field value classname(String)}         | 
	 *                                 | -----------------------------------------------> | 
	 *                                 |    {first field value(String)}                   | if it is a Date/Time/Timestamp, send its getTime() as value; otherwise send its toString()
	 *                                 | -----------------------------------------------> | if it is a null value, send String("^null^") 
	 *                                 |    {second field name(String)}                   |
	 *                                 | -----------------------------------------------> |
	 *                                 |    {second field value classname(String)}        |
	 *                                 | -----------------------------------------------> |
	 *                                 |    {second field value(String)}                  |
	 *                                 | -----------------------------------------------> |
	 *                                 |        . . .                                     |
	 *                                 | -----------------------------------------------> |
	 *                             
	 *
	 * 
	 * package: hyi.cream.inline
	 * 
	 * +--------------+                   +-----------------------+
	 * |<<singleton>> | 1            0..* |                       |
	 * |    Server    |-------------------|     ServerThread      |
	 * |--------------|                   |-----------------------|
	 * |--------------|                   |-----------------------|
	 * |+startup()    |                   |+run()                 |
	 * |+static boolean serverExist()     |-getMessage()          |
	 * |+static void log()                |-Object[] getObject()  |
	 * +--------------+                   |-putObject(Object)     |
	 *                                    |-putObject(Object[])   |
	 *                                    |+void addPOSStatusListener(POSStatusListener l)
	 *                                    |-Iterator getPOSStatusListener()
	 *                                    |-void fireEvent(POSStatusEvent e)
	 *                                    |                       |
	 *                                    +-----------------------+
	 * 
	 * +---------------------------------+
	 * |        Client                   |
	 * |---------------------------------|
	 * |---------------------------------|
	 * |+boolean isConnected()           |
	 * |+connect()                       |
	 * |+closeConnection()               |
	 * |+boolean isConnected()           |
	 * |+void setConnected(boolean c)    |
	 * |+void sendRuok()                 |
	 * |+sendMessage(String)             |
	 * |+String getMessage()             |
	 * |+sendObject(Object)              | 
	 * |+sendObject(Object[])            |
	 * |+void enterCommandLineMode()     |
	 * |+void processCommand(String line) throws ClientCommandException
	 * |+Object[] requestObjects(String) |
	 * |-uploadDac()                     |
	 * |-downloadDac()                   |
	 * |                                 |
	 * |                                 |
	 * +---------------------------------+
	 *  
	 * 
	 *  
	 * +------------------------+ 
	 * |         DacBase        |
	 * |------------------------|
	 * |------------------------|
	 * |+static Iterator getMultipleObjects(Class, String, Map) <-----+
	 * |+deleteAll()            |                                     |
	 * |+backupAll()            |                                     |
	 * |+restoreAll()           |                                     |
	 * |                        |                                     |
	 * +------------------------+                                     |
	 *                                                                |
	 *                                                                |
	 * *All DACs for download                                         |
	 * +------------------------+                                     |
	 * |          PLU           |                                     |
	 * |------------------------|                                     |
	 * |------------------------|                          <<invoke>> |
	 * |+static Collection getAllObjectsForPOS() ---------------------+
	 * |                        |
	 * |                        |
	 * +------------------------+
	 * 
	 * 
	 * +--------------------------+
	 * |       Transaction        |
	 * |--------------------------|
	 * |--------------------------|
	 * |                          |
	 * |+static int getMaxTransactionNumber()
	 * |+static int[] getTransactionLost(int posNumber, int maxNumberShouldHave)
	 * |+void addLineItemSimpleVersion(LineItem lineItem)
	 * |+Transaction cloneForSC() |
	 * |                          |
	 * |                          |
	 * +--------------------------+
	 * 
	 * 
	 * +--------------------------+
	 * |         ZReport          |
	 * |--------------------------|
	 * |--------------------------|
	 * |+ZReport cloneForSC()     |
	 * |                          |
	 * |                          |
	 * +--------------------------+
	 * 
	 * +--------------------------+
	 * |       ShiftReport        |
	 * |--------------------------|
	 * |--------------------------|
	 * |+ShiftReport cloneForSC() |
	 * |                          |
	 * |                          |
	 * +--------------------------+
	 * 
	 * +--------------------------+        +--------------------------+
	 * |  DacObjectOutputStream   |        |  DacObjectInputStream    |
	 * |--------------------------|        |--------------------------|
	 * |--------------------------|        |--------------------------|
	 * |+void writeObject(DacBase obj)     |+DacBase readObject()     |
	 * |                          |        |                          |
	 * |                          |        |                          |
	 * +--------------------------+        +--------------------------+
	 * 
	 * ServerUI:
	 * . hyi.cream.inline.ServerUI extends JFrame
	 * . contains a Swing table and a text pane
	 * //. contains a toolbar: getZList, getShiftList
	 * 
	 *  
	 *  +-----+------+------+-------+--------------------------------------------+
	 *  | POS |Status| Send |Receive|                 Message                    |
	 *  +-----+------+------+-------+------------------------------------------+-+
	 *  |  1  |  O   |  O   |   O   |                                          |^|
	 *  +-----+------+------+-------+------------------------------------------| |
	 *  |  2  |  O   |  O   |   O   |                                          | |
	 *  +-----+------+------+-------+------------------------------------------| |
	 *  |  3  |  O   |  O   |   O   |                                          | |
	 *  +-----+------+------+-------+------------------------------------------| |
	 *  |  4  |  O   |  O   |   O   |                                          | |
	 *  +-----+------+------+-------+------------------------------------------| |
	 *  |  5  |  O   |  O   |   O   |                                          |v|
	 *  +-----+------+------+-------+------------------------------------------+-+
	 *  Log:
	 *  +----------------------------------------------------------------------+-+
	 *  |YYYY/MM/DD HH:MM:SS XXXXXXXXXXXXXXXXXXXXXXXXXXX                       |^|
	 *  |YYYY/MM/DD HH:MM:SS XXXXXXXXXXXXXXXXXXXXXXXXXXX                       | |
	 *  |YYYY/MM/DD HH:MM:SS XXXXXXXXXXXXXXXXXXXXXXXXXXX                       | |
	 *  |YYYY/MM/DD HH:MM:SS XXXXXXXXXXXXXXXXXXXXXXXXXXX                       | |
	 *  |                                                                      | |
	 *  |                                                                      | |
	 *  |                                                                      | |
	 *  |                                                                      | |
	 *  |                                                                      | |
	 *  |                                                                      | |
	 *  |                                                                      |v|
	 *  +----------------------------------------------------------------------+-+
	 *  
	 * 
	 * 
	 * 
	 *                             +--------------------+
	 *                             |                    |
	 *                             |DisplayControlThread|
	 *                             +--------------------+            
	 *                                       |  
	 *                                       |           
	 *  +--------------+           +-------------------+ <<forward  +--------------+              +----------------+
	 *  |              |           |                   |   event>>  |              | 1       0..* |                |
	 *  |  JTable      |-----------|POSStatusTableModel|<-----------|    Server    |--------------|  ServerThread  |
	 *  +--------------+           +-------------------+            +--------------+              +----------------+
	 *                                      |                       /       |                             |
	 *                                      _                      /        _                             |<<instantiate>>
	 *                                      V                     /         V                             v
	 *                             +-------------------+         /  +-------------------+          +--------------+
	 *                             |                   |        /   |                   |          |              |
	 *                             | POSStatusListener |       /    | POSStatusListener |          |POSStatusEvent|
	 *                             +-------------------+      /     +-------------------+          +--------------+
	 *                                                       /                       
	 *  +--------------+           +-------------------+    /                                             
	 *  |              |           |                   |<- /
	 *  |  JList       |-----------|   LogListModel    |
	 *  +--------------+           +-------------------+
	 *                                      |           
	 *                                      _           
	 *                                      V           
	 *                             +-------------------+
	 *                             |                   |
	 *                             | POSStatusListener |
	 *                             +-------------------+
	 *                             
	 */
	public void run() {
		String command;
		String className;
		String serverDir;
		int numberOfObjects;
		StringTokenizer t;
		String[] commandList = { "ruok", "pubobject", "getobject", "getfile", "movefile", "synctrans" };

		while (true) {
			command = getMessage();
			if (Server.debugLog)
				log("got:" + command);

			if (command.equals("quit"))
				break;

			// Filter out the garbage in the front.
			// 因为有时候会发生这种状况，不知何因？
			// e.g.,
			// 2002/12/26 15:01:07 [1] RX:^@^@^@^A^@^@^CBputobject hyi.cream.dac.Transaction 1
			for (int i = 0; i < commandList.length; i++) {
				int idx;
				if ((idx = command.indexOf(commandList[i])) > 0) {
					command = command.substring(idx);
					break;
				}
			}

			// "ruok?"
			if (command.startsWith("ruok?")) {
				processRuok(command);

            // "putobject2\n {String_object}"
            } else if (command.startsWith("putobject2")) {
                // Now there are only two kinds of DAC sent by this protocol: Transaction(
                // which contains LineItem objects) and Inventory.
                log("RX: " + command);
                String objectsInString = getObject2();
                if (objectsInString != null) {
                    //if (objectsInString.startsWith("hyi.cream.dac.Transaction")) {
                    //    Object[] dacObjects = Transaction.constructFromTabSeperatedData(
                    //        objectsInString);
                    //    processObjectGot(dacObjects);
                    if (objectsInString.startsWith("hyi.cream.dac.Inventory")) {
                        Inventory.constructFromTabSeperatedData(this, objectsInString);
                    }
                    putMessage("OK");
                    log("TX: OK");
                }

			// "putobject {class_name} [number_of_objects]"
			} else if (command.startsWith("putobject")) {
				t = new java.util.StringTokenizer(command, " ");
				t.nextToken(); // skip "pubobject"

				// get {class_name}
				if (!t.hasMoreTokens()) { // must give a object's class name
					log("Command error: " + command);
					continue;
				}
				className = t.nextToken();

				// get [number_of_object]
				if (t.hasMoreTokens()) {
					try {
						numberOfObjects = Integer.parseInt(t.nextToken());
					} catch (NumberFormatException e) {
						log("Command error: " + command);
						continue;
					}
				} else
					numberOfObjects = 1; // default number of object is 1 if not given

				log("RX:" + command);
				Object[] object = getObject(className, numberOfObjects);
				if (object != null) {
					processObjectGot(object);
					putMessage("OK");
					log("TX:OK");
				}

			} else if (command.startsWith("getobject2")) {
				t = new java.util.StringTokenizer(command, " ");
				t.nextToken(); // skip "getobject"

				// get {class_name}
				if (!t.hasMoreTokens()) { // must give a object's class name
					putObject(new Integer(-1));
					log("Command error: " + command);
					continue;
				}
				className = t.nextToken();

				log("RX:" + command);
				try {
					if (!pp.beforeDownloadingMaster()) { // cannot download now
						log("Cannot download master now. Date shift process may be running now or failed.");
						out.writeInt(-1);
						continue;
					}

					//Bruce/20030404
					String cacheDF = CreamProperties.getInstance().getProperty("CacheDownloadFile", "no");
					if (cacheDF.equalsIgnoreCase("yes")) {

						String[] sqlFilePath = getDownloadSQLFile(className);
						if (sqlFilePath == null) {
							out.writeInt(0);
							if (!getMessage().equals("OK"))
								log("Didn't get an OK on getobjects");
						} else {
							// send record count
							InputStream recCountFile = new FileInputStream(sqlFilePath[1]);
							byte[] recCnt = new byte[(int) recCountFile.available()];
							recCountFile.read(recCnt);
							recCountFile.close();
							int recCount = Integer.parseInt(new String(recCnt));
							out.writeInt(recCount);
							log("Send record count: " + recCount);

							// then send compressed file content
							if (!putFile(sqlFilePath[0])) {
								log("Command error: " + command);
							} else {
								if (!getMessage().equals("OK"))
									log("Didn't get an OK on getobjects");
							}
						}

					} else {

						// invoke the DAC's getAllObjectsForPOS()
						Collection dacs =
							(Collection) Class.forName(className).getDeclaredMethod(
								"getAllObjectsForPOS",
								new Class[0]).invoke(
								null,
								new Object[0]);
						if (dacs == null || dacs.size() == 0) {
							out.writeInt(0);
							if (!getMessage().equals("OK"))
								log("Didn't get an OK on getobjects");
						} else {
							Object dacArray[] = dacs.toArray();
							out.writeInt(dacArray.length);

							String sqlFilePath = CreamToolkit.getConfigDir() + "insert" + getPOSNumber() + ".sql";
							OutputStream sqlFile = new BufferedOutputStream(new FileOutputStream(sqlFilePath));

							for (int i = 0; i < dacArray.length; i++) {
								DacBase dac = (DacBase) dacArray[i];

								String insertString = dac.getInsertSqlStatement(true);
								//System.out.println(insertString);
								try {
									sqlFile.write(insertString.getBytes(CreamToolkit.getEncoding()));
									sqlFile.write(';');
									sqlFile.write('\n');
									sqlFile.flush();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
							sqlFile.close();
							//log("Finished at make sqlFile.");
							//log("Begin transfer the sqlFiel.");
							//System.out.println("Send " + sqlFilePath);
							if (!putFile(sqlFilePath)) {
								log("Command error: " + command);
							} else {
								socket.setSoTimeout(LONG_TIME_OUT);
								if (!getMessage().equals("OK"))
									log("Didn't get an OK on getobjects");
								socket.setSoTimeout(DEFAULT_TIME_OUT);
							}
						}
					}

				} catch (Exception e) {
					log("Failed on: " + command);
					e.printStackTrace(Server.getLogger());
                    
                    //Bruce/20030717
                    //putObject(new Integer(-1));
                    try {
                        out.writeInt(-1);
                    } catch (IOException e1) {
                        e1.printStackTrace(Server.getLogger());
                    }
				}

				// "getobject {class_name}"
			} else if (command.startsWith("getobject")) {
				t = new java.util.StringTokenizer(command, " ");
				t.nextToken(); // skip "getobject"

				// get {class_name}
				if (!t.hasMoreTokens()) { // must give a object's class name
					putObject(new Integer(-1));
					log("Command error: " + command);
					continue;
				}
				className = t.nextToken();

				ArrayList args = new ArrayList();
				while (t.hasMoreTokens()) {
					String s = t.nextToken();
					if (!s.equals("null"))
						args.add(new String(s));
				}

				//System.out.println(args.size());

				Class argTypes[] = new Class[args.size()];
				for (int i = 0; i < args.size(); i++) {
					try {
						argTypes[i] = Class.forName("java.lang.String");
					} catch (ClassNotFoundException e) {
					}
				}
				// because there are too many log messages on "Message"...
				if (className.equals("hyi.cream.dac.Message"))
					enableLog = false;

				log("RX:" + command);

				// invoke the DAC's getAllObjectsForPOS() or queryObject()
				Collection dacs = null;
				boolean isPutSuccess = true;
				try {
					if (args.size() == 0) {
						dacs =
							(Collection) Class.forName(className).getDeclaredMethod(
								"getAllObjectsForPOS",
								new Class[0]).invoke(
								null,
								new Object[0]);
					} else {
						dacs =
							(Collection) Class.forName(className).getDeclaredMethod(
                                "queryObject", argTypes).invoke(
								null,
								args.toArray());
					}
					isPutSuccess = putObject(new Integer(dacs.size()));
					isPutSuccess = isPutSuccess && putObject(dacs.toArray());
				} catch (Exception e) {
					enableLog = true;
					putObject(new Integer(-1));
					log("Failed on: " + command);
					log(e.toString());
					isPutSuccess = false;
				}

				/*
				 * Meyer / 2003-02-19 / 
				 * 		if meeting getPLUPriceChange, invoke afterDonePluPriceChange()
				 * 
				 */
				if (className.equals("hyi.cream.dac.PLUPriceChange")) {
					String pluPriceChangeID = "";
					String maxID = "";
					Iterator iter = dacs.iterator();
					while (iter.hasNext()) {
						pluPriceChangeID = ((PLU) iter.next()).getPluPriceChangeID();
						if (pluPriceChangeID.compareToIgnoreCase(maxID) > 0) {
							maxID = pluPriceChangeID;
						}
					}
					if (dacs.size() > 0) {
						pp.afterDonePluPriceChange(maxID, String.valueOf(this.getPOSNumber()), isPutSuccess);
					}
				}

				enableLog = true;

				// "getfile {class_name}"
			} else if (command.startsWith("getfile") || command.startsWith("movefile")) {
				t = new java.util.StringTokenizer(command, " ");
				t.nextToken(); // skip "getobject"

				// get {class_name}
				if (!t.hasMoreTokens()) { // must give a object's class name
					putObject(new Integer(-1));
					log("Command error: " + command);
					continue;
				}
				serverDir = t.nextToken();
				log(command);
				if (!putFiles(serverDir, command.startsWith("movefile"))) {
					log("Command error: " + command);
					continue;
				}

				// "synctrans"
			} else if (command.startsWith("synctrans")) {
				syncTransaction();

			} else {
				log("RX:" + command);
				putMessage("Invalid command.");
			}
		}

		try {
			if (out != null)
				out.close();
			if (in != null)
				in.close();
			if (socket != null)
				socket.close();
		} catch (IOException ex) {
		}
		if (posNumber != 0) {
			setPOSConnectionState(posNumber, false);
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_DISCONNECTED, false, false, ""));
		}
		log("POS " + posNumber + " is disconnected.");
//		Server.shrinkLogFiles();
	}

	private void initFile() throws IOException {
	}

	/**
	 * Get a command string from client.
	 */
	private String getMessage() {
		try {
			int c;

			//System.out.println("Ready to read");

			// read one line
			buf.setLength(0);
			while ((c = in.read()) != '\n') {
				if (c == -1)
					return "quit";
				buf.append((char) c);
			}

			fireEvent(
				new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, true, buf.toString()));
			//System.out.println("Received:" + buf.toString());
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));

			return buf.toString();
		} catch (IOException e) {
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
			e.printStackTrace(Server.getLogger());
			return "quit";
		}
	}

	public void putMessage(String str) {
		try {
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, true, false, str));
			out.write(str.getBytes(CreamToolkit.getEncoding()));
			out.write('\n');
			out.flush();
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
		} catch (IOException e) {
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
			e.printStackTrace(Server.getLogger());
		}
	}

	/**
	 * Get objects from client.
	 */
	private Object[] getObject(String className, int numberOfObjects) {
		//log("putobject " + className + " " + numberOfObjects);

		try {
			Object[] object = new Object[numberOfObjects];

			if (numberOfObjects > 0)
				fireEvent(
					new POSStatusEvent(
						this,
						posNumber,
						POSStatusEvent.STATUS_CONNECTED,
						false,
						true,
						"RX:" + className));

			for (int i = 0; i < numberOfObjects; i++) {
				//log("prepare to receive a " + className);
				if (className.indexOf("hyi.cream.dac") != -1) {
					object[i] = dacIn.readObject();
				} else {
					if (objectIn == null)
						//objectIn = new ObjectInputStream(new BufferedInputStream(in)); don't use buffered!
						objectIn = new ObjectInputStream(in);

					object[i] = objectIn.readObject();
				}
				if (object[i] != null)
					log("RX:" + object[i].toString());
			}
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));

			return object;
			// NOTE: caller has to call the last putMessage("OK") after finishing its work for
		} catch (Exception e) {
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
			e.printStackTrace(Server.getLogger());
			return null;
		}
	}

    /**
     * Get objects from client by "putobject2" protocol in a String object.
     */
    private String getObject2() {
        try {
            if (objectIn == null)
                //objectIn = new ObjectInputStream(new BufferedInputStream(in)); don't use buffered!
                objectIn = new ObjectInputStream(in);
            String objects = (String)objectIn.readObject();

            if (objects != null && objects.length() != 0) {
                fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED,
                    false, true, "RX: " + objects.substring(0, 25) + "..."));
                log("TX: " + objects.substring(0, 25) + "...");
                fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED,
                    false, false, ""));
            }

            return objects;
            // NOTE: caller has to call the last putMessage("OK") after finishing its work for
        } catch (Exception e) {
            fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED,
                false, false, ""));
            e.printStackTrace(Server.getLogger());
            return null;
        }
    }


	private boolean putObject(Object object) {
		boolean success = true;
		try {
			if (objectOut == null)
				objectOut = new ObjectOutputStream(new BufferedOutputStream(out));

			fireEvent(
				new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, true, false, "TX:" + object));
			if (object instanceof DacBase) {
				dacOut.writeObject((DacBase) object);
				if (!getMessage().equals("OK"))
					throw new IOException("putObject(Object): Didn't get an OK on sending object");
			} else {
				objectOut.writeObject(object);
				objectOut.flush();
			}
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
			log("TX:" + object.toString());

		} catch (Exception e) {
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
			e.printStackTrace(Server.getLogger());
			log(e.toString());

			success = false;
		}

		return success;
	}

	private boolean putObject(Object[] object) {
		boolean success = true;
		try {
			if (objectOut == null)
				objectOut = new ObjectOutputStream(new BufferedOutputStream(out));

			boolean isDac = true;
			if (object.length > 0)
				fireEvent(
					new POSStatusEvent(
						this,
						posNumber,
						POSStatusEvent.STATUS_CONNECTED,
						true,
						false,
						"TX:" + object[0].getClass().getName()));
			for (int i = 0; i < object.length; i++) {
				if (object[i] instanceof DacBase) {
					//log("TX:" + i + ":" + object[i]);
					dacOut.writeObject((DacBase) object[i]);
				} else {
					isDac = false;
					objectOut.writeObject(object[i]);
					objectOut.flush();
				}
			}
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
			if (object.length > 0)
				log("TX:" + object.length + " " + object[0].getClass().getName() + " objects.");
			if (isDac && !getMessage().equals("OK")) {
				throw new IOException("putObject(Object[]): Didn't get an OK on sending object");
			}
			if (isDac)
				log("RX:OK");
		} catch (Exception e) {
			fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED, false, false, ""));
			e.printStackTrace(Server.getLogger());

			log(e.toString());
			success = false;
		}
		return success;
	}

	public void processObjectGot(Object[] object) {

		if (object[0] instanceof ShiftReport) {
			if (!pp.afterReceivingShiftReport((ShiftReport) object[0])) {
				// error handling...
			}
		} else if (object[0] instanceof ZReport) {
			if (!pp.afterReceivingZReport((ZReport) object[0])) {
				// error handling...
			}
		} else if (object[0] instanceof Transaction) {
			if (!pp.afterReceivingTransaction(object)) {
				// error handling...
			}
		} else if (object[0] instanceof DepSales) {
			if (!pp.afterReceivingDepSales(object)) {
				// error handling...
			}
		} else if (object[0] instanceof DaishouSales) {
			if (!pp.afterReceivingDaishouSales(object)) {
				// error handling...
			}
		} else if (object[0] instanceof DaiShouSales2) {
			if (!pp.afterReceivingDaiShouSales2(object)) {
			    // error handling...
			}
		} else if (object[0] instanceof CashForm) {
			if (!pp.afterReceivingCashForm(object)) {
				// error handling...
			}
        } else if (object[0] instanceof Inventory) {
            if (!pp.afterReceivingInventory(object)) {
                // error handling...
            }
        }

	}

	private boolean putFiles(String serverDir, boolean moveFiles) {
		try {
			File dir = new File(serverDir);
			if (!dir.exists() || !dir.isDirectory()) {
				//putObject(new Integer(-1));
				//out.writeInt(-1);

//                out.writeInt(-1);
//                return false;
                out.writeInt(0);
                return true;
            }
			//OutputStream buffOut = new BufferedOutputStream(out);
			File[] allFiles = dir.listFiles();
			out.writeInt(allFiles.length); // number of files
			//putObject(new Integer(allFiles.length));    // number of files
			for (int i = 0; i < allFiles.length; i++) {
				File file = allFiles[i];
				if (file.isFile()) {
					try {
						putMessage(file.getName()); // file name
						InputStream read = new BufferedInputStream(new FileInputStream(file));
						log("Send " + file.getName() + "(size=" + read.available() + ")");
						out.writeInt(read.available()); // file size
						//putObject(new Integer(read.available()));   // file size
						if (read.available() == 0)
							continue;
						byte[] all = new byte[read.available()];
						int remainLength = read.available();
						read.read(all);
						// file content
						//buffOut.write(all);
						//buffOut.flush();
						for (int j = 0; j < (all.length - 1) / 4096 + 1; j++) {
							out.write(all, j * 4096, Math.min(remainLength, 4096));
							out.flush();
							//System.out.println("sent " + Math.min(remainLength, 4096) + " bytes");
							remainLength -= 4096;
						}
						read.close();
						if (moveFiles)
							file.delete();
					} catch (IOException fe) {
						log(fe.toString());
						fe.printStackTrace();
						return false;
					}
				}
			}
		} catch (Exception e) {
			log(e.toString());
			return false;
		}
		return true;
	}

	private boolean putFile(String fileName) {
		//OutputStream buffOut = new BufferedOutputStream(out);
		File file = new File(fileName);

		if (file.isFile()) {
			try {
				//out.writeInt(1);
				//putMessage(file.getName());         // file name
				log("Send " + file.getName());
				InputStream read = new BufferedInputStream(new FileInputStream(file));
				out.writeInt(read.available()); // file size
				byte[] all = new byte[read.available()];
				int remainLength = read.available();
				read.read(all);
				// file content
				//buffOut.write(all);
				//buffOut.flush();
				for (int j = 0; j < (all.length - 1) / 4096 + 1; j++) {
					out.write(all, j * 4096, Math.min(remainLength, 4096));
					out.flush();
					//System.out.println("sent " + Math.min(remainLength, 4096) + " bytes");
					remainLength -= 4096;
				}
				read.close();
			} catch (IOException fe) {
				log(fe.toString());
				fe.printStackTrace();
				return false;
			}
		}
		return true;
	}

	private void syncTransaction() {
		try {

//			//Bruce/2002-04-01
//			// If date shift is running, don't send any request
//			if (!pp.canSyncTransaction()) {
//				out.writeInt(0);
//				if (Server.debugLog)
//					log("Cannot syncTransaction now, date shift is running.");
//				return;
//			}

			int posNumber = in.readInt();
			if (Server.debugLog)
				log("synctrans> posNumber=" + posNumber);

			int maxTransactionNumberInPOS = in.readInt();
			if (Server.debugLog)
				log("synctrans> maxTrans=" + maxTransactionNumberInPOS);

			int beginTransactionNumber = in.readInt();
            if (Server.debugLog)
                log("synctrans> beginTrans=" + beginTransactionNumber);
                
            int endTransactionNumber = in.readInt();
            if (Server.debugLog)
                log("synctrans> endTrans=" + endTransactionNumber);
          
                      //int maxTransactionNumberInSC = Transaction.getMaxTransactionNumber(posNumber);
			int[] lostNumbers = Transaction.getTransactionLost(posNumber, maxTransactionNumberInPOS, beginTransactionNumber, endTransactionNumber);
			if (lostNumbers == null) {
				out.writeInt(0);
				if (Server.debugLog)
					log("synctrans> No lostNumber. Over.");
				return;
			}
			out.writeInt(lostNumbers.length);
			for (int i = 0; i < lostNumbers.length; i++) {
				log("Request transaction " + lostNumbers[i]);
				out.writeInt(lostNumbers[i]);
			}
			if (Server.debugLog)
				log("synctrans> over.");
		} catch (IOException e) {
			log(e.toString());
		}
	}

	private void processRuok(String command) {
		try {
			posNumber = in.readInt();

			if (!hasBeenWrittenConnectionStatusFile) {
				setPOSConnectionState(posNumber, true);
				hasBeenWrittenConnectionStatusFile = true;
			}

			/*
			if (objectIn == null)
			    objectIn = new ObjectInputStream(in);
			posNumber = ((Integer)objectIn.readObject()).intValue();
			System.out.println(((String)objectIn.readObject()).toString());
			System.out.println(((java.util.Date)objectIn.readObject()).toString());
			//System.out.println(((java.math.BigDecimal)objectIn.readObject()).toString());
			*/

			//log("pos=" + posNumber);
			//putMessage("yes");

			// Retrieve max priceChgID from posdl_deltaplu, if got something,
			// send back it to POS.
			String maxPriceChangeID = PLUPriceChange.getMaxPriceChangeVersion();

			String storeNo = command.substring(5);
			if (storeNo != null && storeNo.length() > 0 && !storeNo.equals(Store.getStoreID()))
			    putMessage("no");
			else if (maxPriceChangeID == null)
				putMessage("yes");
			else
				putMessage("yes" + maxPriceChangeID);

		} catch (IOException e) {
			e.printStackTrace(Server.getLogger());
		}

		//if (posNumber != 0) {
		//    fireEvent(new POSStatusEvent(this, posNumber, POSStatusEvent.STATUS_CONNECTED,
		//        false, false, ""));
		//}
	}

	/**
	 * Write POS connection status file. File name is property "ConnectionStatusFile".
	 */
	static void setPOSConnectionState(int posNumber, boolean connect) {
		try {
			if (connectionStatusFile == null)
				return;

			File f0 = new File(connectionStatusFile);
			if (!f0.exists()) {
				FileWriter fw = new FileWriter(f0);
				fw.write("00000");
				fw.close();
			}

			RandomAccessFile f = new RandomAccessFile(connectionStatusFile, "rw");
			f.seek(posNumber - 1);
			f.writeBytes(connect ? "1" : "0");
			f.close();

		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}
}
