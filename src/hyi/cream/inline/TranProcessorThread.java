/*
 * Created on 2003-12-12
 *
 */
package hyi.cream.inline;

import java.util.*;
import java.util.Date;
import java.math.*;
import java.text.*;
import java.sql.*;
import java.io.*;
import org.jdom.*;
import org.jdom.input.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
/**
 * @author administrator
 */
public class TranProcessorThread extends MutableThread {
    private String postObjectDir = "TmpTrans/"; //上传上来的对象存放的目录     
    
    private boolean isDaishouPluUpdateInv = CreamProperties.getInstance().getProperty("IsDaishouPluUpdateInv", "no").equalsIgnoreCase("yes");
    //private boolean isBusinessDateByConf = CreamProperties.getInstance().getProperty("isBusinessDateByConf", "no").equalsIgnoreCase("yes");
    // 是否只针对对交易为当前营业日才更新inv
    private boolean isOnlyUpdateCurrentDateInv = CreamProperties.getInstance().getProperty("isOnlyUpdateCurrentDateInv", "no").equalsIgnoreCase("yes");
    // 如果更新了inv，那么是否要处理今天以后的inv
    private boolean isDealPostDateInv = CreamProperties.getInstance().getProperty("isDealPostDateInv", "yes").equalsIgnoreCase("yes");
    private boolean isInsertInv = CreamProperties.getInstance().getProperty("isInsertInv", "no").equalsIgnoreCase("yes");
    private boolean isServerSideOracle = Server.serverExist() && "oracle".equalsIgnoreCase(CreamProperties.getInstance().getProperty("DBType"));

    public TranProcessorThread(String s) {
        super(s);
        Server.log("isDaishouPluUpdateInv = " + isDaishouPluUpdateInv);
        //Server.log("isBusinessDateByConf = " + isBusinessDateByConf);
        Server.log("isOnlyUpdateCurrentDateInv = " + isOnlyUpdateCurrentDateInv);
        Server.log("isDealPostDateInv = " + isDealPostDateInv);
        Server.log("isInsertInv = " + isInsertInv);
    }
    
    //每隔一分钟运行一次
    public void run() {
            while (true) {
//              出现异常以后忽略，该线程继续运行
            try {
                Object[] tranNumbersInSC = getAllTrans(); 
                        File dir = new File(postObjectDir);
                if (dir.exists() && dir.isDirectory()) {
                    String[] fileNames = dir.list(new NameFilter());
                    for (int i=0; i < fileNames.length; i++) {
                        String tmp = fileNames[i].substring(0,fileNames[i].indexOf(".xml"));
                        if (Arrays.binarySearch(tranNumbersInSC, tmp) < 0){
                            processTransaction(postObjectDir + fileNames[i]);
                        }
                         new File(postObjectDir + fileNames[i]).delete();
                    }
                }
            } catch (Exception e) {
              e.printStackTrace(Server.getLogger());
            }
           
            try {
                                Thread.sleep(1L * 60L * 1000L);
                        } catch (Exception e) {
                                e.printStackTrace(Server.getLogger());
                        }
                }
        }

    private Object[] getAllTrans() {
                String sql = "SELECT transactionNumber, posNumber, systemDate FROM posul_tranhead";
        Object[] tmpArrays = new Object[]{};
        Connection con  = null ;
        Statement stmt = null;
        ResultSet rst = null; 
        try {
            con = CreamToolkit.getPooledConnection();
                stmt = con.createStatement();
                rst = stmt.executeQuery(sql);
            ArrayList list = new ArrayList();
            while (rst.next()) {
                String dt = new SimpleDateFormat("yyyyMMdd").format(rst.getDate(3));
                list.add(rst.getInt(2) + "@" + rst.getInt(1) + "@" + dt);
            }
            tmpArrays= list.toArray();
            Arrays.sort(tmpArrays);
            
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            try {
                if (rst != null)
                    rst.close();
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
                    
            if (con != null)
                CreamToolkit.releaseConnection(con);
        }
        return tmpArrays;        
    }

    private void processTransaction(String fileName) {
        try {
            SAXBuilder builder = new SAXBuilder(true);
            builder.setValidation(false);
            Document doc = builder.build (new File(fileName));
            ArrayList lstSql = new ArrayList();
            
            Element root = doc.getRootElement();
            Iterator itr = root.getChildren().iterator();
            while (itr.hasNext()) {
                Element el = (Element)itr.next();
                String s = el.getChildText("sql");
                if (s != null && s.length() > 0) {
                    lstSql.add(s); 
                }
            }
            
            if (executeQuery(lstSql.toArray())) {
                if (Server.isUpdateInv()) {
                    additionalProcess(root);    
                }
//                new File(fileName).delete();
            } else {
                String errString = "Error occured when insert transaction into database." +
                    "\n \t transaction file:[" + fileName + "]";
                Server.getLogger().write(errString);
            }
            
        } catch (JDOMException e) { 
            e.printStackTrace(Server.getLogger());
        } catch (Exception e) {
            e.printStackTrace(Server.getLogger());
        }
    }
        
    private boolean executeQuery(Object[] sqls) {
        boolean isOk = false;
        Connection con  = null ;
        Statement stmt = null;
        try {
            con = CreamToolkit.getPooledConnection();
            stmt = con.createStatement();
            for (int i=0; i < sqls.length; i++) {
                stmt.execute((String)sqls[i]);
            }
            isOk = true;
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
                        
            if (con != null)
                CreamToolkit.releaseConnection(con);
        }        
        return isOk;
    }
    
    /**
     * 额外的处理，包括插入作废发票、退货交易、更新库存等
     */
    private void additionalProcess(Element elTran) {

        Element elTranHead = elTran.getChild("TranHead"); 
        if (elTranHead == null) return;
        String dealType3 = elTranHead.getChildText("dealType3");
        String tmp = elTranHead.getChildText("sql");
        Map tranFieldMap = getFieldMap(tmp);     

        //gllg/20080618/ ignore voidinvo and salertn here
/*
        if (!(dealType3 != null && dealType3.equals("0"))) {
            insertVoidInvo(tranFieldMap);

            if (dealType3.equals("4"))
                insertSalertn(tranFieldMap, elTran); 
        }
*/
        // transacton.systemDate   VS   cream.conf + now()
        
        // 22:59:59 
        // now() -- yyyy-MM-dd 
        // yyyy-MM-dd 22:59:59
        // now() VS yyyy-MM-dd 22:59:59
        // now() VS ((yyyy-MM-dd) -1 ) 22:59:59
        if (tranFieldMap == null) 
            return;
        java.util.Date systemDate;
        if (isServerSideOracle)
            systemDate = Timestamp.valueOf((String)tranFieldMap.get("SYSTEMDATE"));
        else
            systemDate = Timestamp.valueOf((String)tranFieldMap.get("systemDate"));
        Date busiDate = getBusinessDate(systemDate);
        Date currentDate = getBusinessDate(new Date());
        DateFormat yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
        String busiDateStr = yyyy_MM_dd.format(busiDate);
        String currentDateStr = yyyy_MM_dd.format(currentDate);
        
        if (isOnlyUpdateCurrentDateInv && busiDate.compareTo(currentDate) != 0){
            return;
        }
        
        Iterator itr = elTran.getChildren("TranDetail").iterator();
        while (itr.hasNext()) {
            updateInv(elTranHead, (Element)itr.next(), busiDateStr, currentDateStr);
        }
      
    }
 

    private void insertVoidInvo(Map fieldMap) {
        Object o = fieldMap.get("voidTransactionNumber");
        if (o == null) return;
        
//        System.out.println("Insert Void Invoice");
        
        String voidTranNo = o.toString();
        int posNumber = Integer.parseInt((String)fieldMap.get("posNumber"));
        
        Transaction voidT = Transaction.queryByTransactionNumber(posNumber, voidTranNo);
        if (voidT == null)
           return;        
        
        HashMap newMap = new HashMap();
        newMap.put("accountDate", getAccountingDate(new java.util.Date()));
        newMap.put("storeID", (String)fieldMap.get("storeID"));
        java.util.Date d = Timestamp.valueOf((String)fieldMap.get("systemDate"));
        newMap.put("voidDateTime", d);
        newMap.put("posNumber", new Integer(posNumber));
        newMap.put("cashierID", (String)fieldMap.get("cashier"));
        newMap.put("transactionNumber", new Integer(voidTranNo));

        String dt1 = (String)fieldMap.get("dealType1");
        String dt2 = (String)fieldMap.get("dealType2");
        String dt3 = (String)fieldMap.get("dealType3");

        if (dt1.equals("0") && dt2.equals("0") && dt3.equals("1"))        {
            newMap.put("voidType", "1");
        } else if (dt1.equals("0") && dt2.equals("3") && dt3.equals("4")) {
            newMap.put("voidType", "2");
        } else if (dt1.equals("0") && dt2.equals("0") && dt3.equals("2")) {
            newMap.put("voidType", "3");
        } else if (dt1.equals("0") && dt2.equals("0") && dt3.equals("3")) {
            newMap.put("voidType", "4");
        } else {
        }
        
        newMap.put("voidInvoiceHead", voidT.getInvoiceID());
        newMap.put("voidInvoiceNumber", voidT.getInvoiceNumber());
        newMap.put("invoiceCount", voidT.getInvoiceCount());
        newMap.put("netSaleTotalAmount", voidT.getNetSalesTotalAmount());
        newMap.put("netSaleTax0Amount", voidT.getNetSalesAmount0());
        newMap.put("netSaleTax1Amount", voidT.getNetSalesAmount1());
        newMap.put("netSaleTax2Amount", voidT.getNetSalesAmount2());
        newMap.put("netSaleTax3Amount", voidT.getNetSalesAmount3());
        newMap.put("netSaleTax4Amount", voidT.getNetSalesAmount4());
        PostProcessorAdapter.insert(newMap, "voidinvo");        
    }
    
    private void insertSalertn(Map fieldMap, Element elTran) {
//        System.out.println("Insert Retrun Sale");
        
        String storeID = (String)fieldMap.get("storeID");
        String posNumber = (String)fieldMap.get("posNumber");
        String tranNumber = (String)fieldMap.get("transactionNumber"); 
        String cashier = (String)fieldMap.get("cashier");
        java.util.Date systemDate = Timestamp.valueOf((String)fieldMap.get("systemDate"));
        Date busiDate = getBusinessDate(systemDate);
        String dt1 = (String)fieldMap.get("dealType1");
        String dt2 = (String)fieldMap.get("dealType2");
        String dt3 = (String)fieldMap.get("dealType3");

        Iterator itrDetails = elTran.getChildren("TranDetail").iterator();
        while (itrDetails.hasNext()) {
            Element elDetail = (Element)itrDetails.next();
            String itemNumber = elDetail.getChildText("itemNumber");
            BigDecimal quantity = new BigDecimal(elDetail.getChildText("quantity"));
            BigDecimal unitPrice = new BigDecimal(elDetail.getChildText("unitPrice"));
            Integer lineItemSequence = new Integer(elDetail.getChildText("lineItemSequence"));
            
            PLU plu = PLU.queryByItemNumber(itemNumber);
            if (plu == null) {
                Server.log("insertSalertn(): Cannot find: " + 
                           "itemNumber[ " + itemNumber + "] in posdl_plu");
                continue;
            } 
            
            HashMap newMap = new HashMap();
            ArrayList last = new ArrayList();
            if (dt2.equals("0")) {
                last = PostProcessorAdapter.getCollectionOfStatement(
                    "SELECT * FROM salertn WHERE transactionNumber = " +
                    (Integer.parseInt(tranNumber) - 1) +
                    " AND posNumber = '"  + posNumber +"'");            
                
                Iterator itr = last.iterator();
                while (itr.hasNext()) {
                    Map next = (Map)itr.next();
                    BigDecimal amt = (BigDecimal)next.get("salertnquantity");
                    String number = (String)next.get("itemnumber");
                    if (amt.abs().compareTo(quantity.abs()) == 0
                        && number.compareTo(plu.getInStoreCode()) == 0) {
                        //storeID + busiDate + posNumber + transactionNumber + lineItemSequence
                        String delete ;
                        if (isServerSideOracle)
                            delete = "DELETE FROM salertn" +
                            " WHERE storeID = '" + next.get("storeid") + "' AND busiDate = TO_DATE('" + next.get("busidate") +
                            "','YYYY-MM-DD HH24:MI:SS') AND posNumber = '" + next.get("posnumber") + "' AND transactionNumber = " + next.get("transactionnumber") +
                            " AND lineItemSequence = " + next.get("lineitemsequence");
                        else
                            delete = "DELETE FROM salertn" +
                            " WHERE storeID = '" + next.get("storeid") + "' AND busiDate = '" + next.get("busidate") +
                            "' AND posNumber = '" + next.get("posnumber") + "' AND transactionNumber = " + next.get("transactionnumber") +
                            " AND lineItemSequence = " + next.get("lineitemsequence");
                        PostProcessorAdapter.executeQuery(delete);

                        //Bruce> Add "break" to fix a bug:
                        //   Buy  (1) A x 1
                        //        (2) A x 1
                        //        (3) B x 2
                        //        (4) C x 3
                        //  if 部分退货 (1),(3),(4), then it'll delete all 'A' in salertn
                        break;
                    }
                }
                continue;
            } // end if (dt2.equals("0"))
            
            newMap.put("busiDate", busiDate);
            newMap.put("storeID", storeID);
            newMap.put("posNumber", new Integer(posNumber));
            newMap.put("transactionNumber", new Integer(tranNumber));
            newMap.put("lineItemSequence", lineItemSequence);
            newMap.put("itemNumber", plu.getInStoreCode());
            newMap.put("shipNumber", "0");
            newMap.put("updateUserID", cashier);
            newMap.put("updateDate", new java.util.Date());
            newMap.put("saleRtnQuantity", quantity.abs());
            newMap.put("storeUnitPrice", unitPrice);
            Map m = PostProcessorAdapter.query("SELECT unitCost FROM plu WHERE itemNumber = '" + plu.getInStoreCode() + "'");
            BigDecimal unitCost = (BigDecimal)m.get("unitcost");
            newMap.put("unitCost", unitCost);
            PostProcessorAdapter.insert(newMap, "salertn");
        }
    }
    
    private boolean updateInv(Element elTranHead, Element elTranDetail,String busiDate, String currentDateStr) {
        String storeID = elTranHead.getChildText("storeID");
        String posNo = elTranHead.getChildText("posNumber");
        String tranNo = elTranHead.getChildText("transactionNumber");
        String dt1 = elTranHead.getChildText("dealType1");
        String dt2 = elTranHead.getChildText("dealType2");
        String dt3 = elTranHead.getChildText("dealType3");
        
        String itemNo = elTranDetail.getChildText("itemNumber");
        BigDecimal quantity = new BigDecimal(elTranDetail.getChildText("quantity"));
        String detailCode = elTranDetail.getChildText("detailCode");
        if (detailCode.equals("D") ||   //折扣
            detailCode.equals("M") ||   //组合促销 
            detailCode.equals("O") ||   //代收公共事业费
            detailCode.equals("Q"))     //代付
            return true;
        if (detailCode.equals("I") && !isDaishouPluUpdateInv)   //代售
            return true;
        HashMap pluQtyMap = new HashMap(); 
        Connection con = null;
        Statement stmt = null;
        ResultSet rst = null;
        try {
            con = CreamToolkit.getPooledConnection();
            stmt = con.createStatement();
            
            rst = stmt.executeQuery(
                " SELECT * FROM itempack WHERE StoreID='" + storeID + 
                "' AND packNo='" + itemNo + "'");
            while (rst.next()) {
                pluQtyMap.put(rst.getString("itemNo"),
                    quantity.multiply(new BigDecimal(rst.getInt("packQty")))
                );
            }
            //get original itemNumber when no itempack record existed
            if (pluQtyMap.size() == 0) {
                pluQtyMap.put(itemNo, quantity);
            }    
        } catch (SQLException e) {
            //get original itemNumber when no itempack table existed
            pluQtyMap.put(itemNo, quantity);
            
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
            if (con != null)
                CreamToolkit.releaseConnection(con);
        }    

        String shipNumber = "0";
        String itemNumber;
        BigDecimal itemQuantity;
        Iterator itr = pluQtyMap.keySet().iterator();
        
        String select = "SELECT * FROM inv ";
        String updateString = "UPDATE inv SET ";
        Collection invFieldList = PostProcessorAdapter.getFieldList("inv");
        
        String insertStringPrep = "INSERT INTO inv (storeID,itemNumber,shipNumber,busiDate," + "accsalequantity,accsalertnquantity)";
        String insertStringPost;
        if (isServerSideOracle)
            insertStringPost = " VALUES('"+storeID+"','"+itemNo+"','"+shipNumber+"',TO_DATE('"+busiDate+"','YYYY-MM-DD HH24:MI:SS'),";
        else
            insertStringPost = " VALUES('"+storeID+"','"+itemNo+"','"+shipNumber+"','"+busiDate+"',";
        String fieldName1 = "accsalequantity";
        String fieldName2 = "accsalertnquantity";
        while (itr.hasNext()) {
            itemNumber = (String)itr.next();
            itemQuantity = (BigDecimal)pluQtyMap.get(itemNumber); 
            String where;
            if (isServerSideOracle)
                where = " WHERE storeID = '" + storeID + "' AND busiDate = TO_DATE('" +
                busiDate + "','YYYY-MM-DD HH24:MI:SS') AND itemNumber = '" + itemNumber +
                "' AND shipNumber = '" + shipNumber + "'";
            else
                where = " WHERE storeID = '" + storeID + "' AND busiDate = '" +
                busiDate + "' AND itemNumber = '" + itemNumber +
                "' AND shipNumber = '" + shipNumber + "'";
            
            Map map = PostProcessorAdapter.query(select + where);
      
            BigDecimal accSaleQuantity = null;
            BigDecimal accSaleRtnQuantity = null;
            
            // if (map.isEmpty()) {
            if (map.isEmpty()) {
                Server.log("Cannot find inv with number of: " + itemNumber);
                Server.log(select + where);
                if (isInsertInv){
                    accSaleQuantity = new BigDecimal(0);
                    accSaleRtnQuantity = new BigDecimal(0);
                } else {
                   continue;
                }
            } else {
                accSaleQuantity = (BigDecimal)map.get("accsalequantity");
                accSaleRtnQuantity = (BigDecimal)map.get("accsalertnquantity");
            }

            Server.log("posNo=" + posNo + ",tranNo=" + tranNo + ",dt1=" + dt1 + ",dt2=" + dt2 + ",dt3=" + dt3);
            String invStr = "Sale qty of " + itemNumber + ":" + accSaleQuantity;
            String rtnStr = "Return qty of " + itemNumber + ":" + accSaleRtnQuantity;

            if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("1")) {//前笔误打
                accSaleQuantity = accSaleQuantity.add(itemQuantity);
            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("3") && dt3.equals("4")) {//全部退货
                accSaleRtnQuantity = accSaleRtnQuantity.add(itemQuantity.negate());
            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("4")) {//部分退货
                accSaleRtnQuantity = accSaleRtnQuantity.add(itemQuantity.negate());
            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("3")) {//交易取消
                accSaleQuantity = accSaleQuantity.add(itemQuantity);
            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("2")) {//卡纸重印
                accSaleQuantity = accSaleQuantity.add(itemQuantity);
            } else if ((dt1.equals("0") || dt1.equals("*")) && dt2.equals("0") && dt3.equals("0")) {//正常交易
                accSaleQuantity = accSaleQuantity.add(itemQuantity);
            } else {
                return true; //退出整笔交易,
            }
            Server.log(invStr + "->" + accSaleQuantity);
            Server.log(rtnStr + "->" + accSaleRtnQuantity);
            
            if (map.isEmpty()) {
                if (isInsertInv){
                    // insert 
                    String s = insertStringPrep + insertStringPost + accSaleQuantity + "," + accSaleRtnQuantity + ")";
                    System.out.println("PostProcessorAdapter.update(insertString);> " + s);
                    PostProcessorAdapter.update(s);
                }
            } else {
                updateString += fieldName1  + "=" + accSaleQuantity + ", " + fieldName2  + "=" +    accSaleRtnQuantity + "";
                if (invFieldList.contains("modifyDate".toLowerCase())) {
                    updateString += " modifyDate = '" + currentDateStr + "' ";
                }
                updateString += where;
                System.out.println("PostProcessorAdapter.update(updateString);> " + updateString);
                PostProcessorAdapter.update(updateString);
            }
                
            // 只有当天才 update inv, 对重传不处理
            // 对照日期(busiDate, accountDate 按照 cream.conf 配置 hh:mm:ss, 不要从calendar去
            // 希望将pos 与 sc 数据库关联明确 区分
            
            
            if (isDealPostDateInv) {
                //更新inv表中期初数量
                if (!currentDateStr.equals(busiDate)) {
                    //参数列表： 店号，交易日，品号，趟次，调整数量
                    if (!updateBeginInvQty(storeID, busiDate, itemNumber, shipNumber, quantity)) {
                        Server.log("Error occured when update begin inv quantity: " 
                                  + "storeID = " + storeID + ", "
                                  + "itemNumber = " + itemNumber + ", " 
                                  + "busiDate = " + busiDate + ", "
                                  + "shipNumber = " + shipNumber);
                    }
                }
            }

        } // end while
        
        return true;
    }

    /**
     * 更新busiDate 以后各天的期初数量
     * 参数列表
     *      String storeID      : 店号
     *      String busiDate     : 业务日期
     *      String itemNo       : 品号
     *      String shipNo       : 趟次
     *      BigDecimal deltaQty : 调整数量（需要从期初数量中减去）  
     */    
    private boolean updateBeginInvQty(String storeID, String busiDate, String itemNo, String shipNo, BigDecimal deltaQty) {
        String updateString;
        if (isServerSideOracle)
            updateString = "UPDATE inv SET befInvQuantity = befInvQuantity - " + deltaQty + " "
                   + "WHERE storeID = '" + storeID + "' AND " 
                   + "itemNumber = '" + itemNo + "' AND "
                   + "shipNumber = '" + shipNo + "' AND "
                   + "busiDate > TO_DATE('" + busiDate + "','YYYY-MM-DD HH24:MI:SS')";
        else
            updateString = "UPDATE inv SET befInvQuantity = befInvQuantity - " + deltaQty + " "
                   + "WHERE storeID = '" + storeID + "' AND "
                   + "itemNumber = '" + itemNo + "' AND "
                   + "shipNumber = '" + shipNo + "' AND "
                   + "busiDate > '" + busiDate + "'";
        
        return PostProcessorAdapter.update(updateString);
    }

    /**
     * sqlStatement 为完整的Insert语句
     */
    private Map getFieldMap (String sqlStatement) {
        Map newMap = new HashMap();
        String s1 = sqlStatement.substring(sqlStatement.indexOf("(") + 1, sqlStatement.indexOf(")"));
        String s2;
        if (isServerSideOracle)
            s2 = sqlStatement.substring(sqlStatement.indexOf("values (") + 8, sqlStatement.lastIndexOf(")"));
        else
            s2 =  sqlStatement.substring(sqlStatement.lastIndexOf("(") + 1, sqlStatement.lastIndexOf(")"));

        
        StringTokenizer stk1 = new StringTokenizer(s1,",");
        StringTokenizer stk2  = new StringTokenizer(s2, ",");
        
        while (stk1.hasMoreTokens()) {
            String fieldName = (String)stk1.nextToken();
            String tmp1 = (String)stk2.nextToken();
            String value = null;
            if (isServerSideOracle)
                if(tmp1.indexOf("TO_DATE") >= 0)   {
                    tmp1 =  tmp1.substring(tmp1.indexOf("\'") , tmp1.lastIndexOf("\'") + 1) ;
                } else if (tmp1.indexOf("YYYY-MM-DD HH24:MI:SS") >= 0) {
                    tmp1 = stk2.nextToken();
                }

            int i1 = tmp1.indexOf('\'');
            int i2 = tmp1.lastIndexOf('\'');
            if (!(i1 == i2 || i1 + 1 == i2))
                value = tmp1.substring(i1 + 1, i2);
            
            newMap.put(fieldName, value);    
        }
   
        return newMap;
         
    }

    /**
     * 根据交易时间决定会计日期。
     * 
     * @param date 交易时间
     * @return 返回会计日期
     */
    private java.util.Date getAccountingDate(java.util.Date date) {
        // Karman said: (2008-06-02)
        // 当z帐里的结束时间 < z结束日期+AccountingDateShiftTime 时 z->accountdate=z结束日期
        // 当z帐里的结束时间 > z结束日期+AccountingDateShiftTime 时 z->accountdate=z结束日期+1

        String dateshiftTime = CreamProperties.getInstance().getProperty("AccountingDateShiftTime", "17:00:00");
        if (dateshiftTime.equalsIgnoreCase("afternoon") || dateshiftTime.equalsIgnoreCase("morning")) // for old conf
            dateshiftTime = "17:00:00";
        
        String timePart = new SimpleDateFormat("HH:mm:ss").format(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        
        if (timePart.compareTo(dateshiftTime) >= 0)
            cal.add(Calendar.DATE, 1);

        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();

//        String changeTime = "23:59:59";
//        Map value = PostProcessorAdapter.query("SELECT changetime FROM calendar WHERE dateType = '1'");
//        if (value != null && value.containsKey("changetime")) {
//            changeTime = (String)value.get("changetime").toString();
//            if (changeTime.trim().length() == 0 || changeTime.equals("00:00:00")) {
//                changeTime = "23:59:59";        
//            }
//        }
//        String realTime = new SimpleDateFormat("HH:mm:ss").format(date);
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//
//        //有两种决定会计日期的models:
//        //如果AccountingDateShiftTime="afternoon"(default)，则超过换日时间后为(d+1)日；
//        //否则(="morning")超过换日时间后仍为(d)日，在换日之前为(d-1)日。
//        boolean afternoon = CreamProperties.getInstance().getProperty(
//            "AccountingDateShiftTime","afternoon").equalsIgnoreCase("afternoon");
//            //"BusinessDateShiftTime","afternoon").equalsIgnoreCase("afternoon");
//        if (afternoon) {
//            if (realTime.compareTo(changeTime) > 0) {
//                cal.add(Calendar.DATE, 1);
//            }
//        } else {
//            if (realTime.compareTo(changeTime) <= 0) {
//                cal.add(Calendar.DATE, -1);
//            }
//        }
//        cal.set(Calendar.HOUR, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        return cal.getTime();
    }

    /**
     * 根据交易时间决定营业日期。
     * 
     * @param date 交易时间
     * @return 返回营业日期
     */
     private java.util.Date getBusinessDate(java.util.Date date) {
        String changeTime = "22:59:59";
        //if (isBusinessDateByConf){
        changeTime = CreamProperties.getInstance().getProperty("changeTime", "22:59:59");
        //} else {
        //    Map value = PostProcessorAdapter.query("SELECT changetime FROM calendar WHERE dateType = '0'");
        //    if (value != null && value.containsKey("changetime")) {
        //        changeTime = (String)value.get("changetime").toString();
        //        if (changeTime.trim().length() == 0 || changeTime.equals("00:00:00")) {
        //            changeTime = "22:59:59";        
        //        }
        //    }
        //}
        String realTime = new SimpleDateFormat("HH:mm:ss").format(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        
        //Bruce/2008-06-03/ 现在cream.conf中AccountingDateShiftTime存的是会计换日时间17:00:00
        //boolean afternoon = CreamProperties.getInstance().getProperty(
        //        "AccountingDateShiftTime","afternoon").equalsIgnoreCase("afternoon");
        //        //"BusinessDateShiftTime","afternoon").equalsIgnoreCase("afternoon");
        //if (afternoon) {
        if (realTime.compareTo(changeTime) > 0) {
            cal.add(Calendar.DATE, 1);
        }
        //} else {
        //    if (realTime.compareTo(changeTime) <= 0) {
        //        cal.add(Calendar.DATE, -1);
        //    }
        //}
        return cal.getTime();
    }
     
    class NameFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            if (name.endsWith(".xml"))
                return true;
            else 
                return false;
        }
    }
    public static void main(String[] args) {
        ////test1();
        //test2();

        //TranProcessorThread.getFieldMap("insert into posul_tranhead(NETSALETAX4AMOUNT,NETSALETOTALAMOUNT,TAX0AMOUNT,GROSSSALETAX3AMOUNT,DAIFUAMOUNT,DISCOUNTTAX0AMOUNT,CREDITCARDNUMBER,TAX3AMOUNT,CREDITCARDEXPIREDATE,POSNUMBER,MIXANDMATCHTAX2AMOUNT,TRANSACTIONNUMBER,CHANGEAMOUNT,DAISHOUAMOUNT,MIXANDMATCHTOTALAMOUNT,CREDITCARDTYPE,NOTINCLUDEDTAX3SALE,ACCOUNTDATE,GROSSSALETOTALAMOUNT,MEMBERID,DISCOUNTTAX3AMOUNT,TAX1AMOUNT,MIXANDMATCHTAX1AMOUNT,IDNUMBER,TRANSACTIONTYPE,SIPLUSTAX4AMOUNT,MACHINENUMBER,DISCOUNTTAX2AMOUNT,NETSALETAX1AMOUNT,TAX4AMOUNT,MIXANDMATCHTAX0AMOUNT,SPILLAMOUNT,NETSALETAX0AMOUNT,STOREID,NETSALETAX2AMOUNT,CASHIER,NETSALETAX3AMOUNT,DISCOUNTTAX4AMOUNT,EMPLOYEENUMBER,PAYID4,PAYID3,SALEMAN,SIPLUSTOTALAMOUNT,PAYID1,PAYID2,GROSSSALETAX1AMOUNT,DETAILCOUNT,PAYAMOUNT1,ITEMDISCCOUNT2,NOTINCLUDEDTAX2SALE,ITEMDISCCOUNT1,PAYAMOUNT3,ITEMDISCCOUNT0,PAYAMOUNT2,PAYAMOUNT4,ITEMDISCCOUNT4,ITEMDISCCOUNT3,MIXANDMATCHTAX3AMOUNT,GROSSSALETAX4AMOUNT,CUSTOMERCOUNT,CUSTOMERCATEGORY,ITEMDISCAMOUNT4,DAISHOUAMOUNT2,ITEMDISCAMOUNT0,SIPLUSTAX0AMOUNT,ITEMDISCAMOUNT1,ITEMDISCAMOUNT2,ITEMDISCAMOUNT3,NOTINCLUDEDTAX4SALE,TAX2AMOUNT,SHIFTCOUNT,INOURID,MIXANDMATCHTAX4AMOUNT,DEALTYPE1,SIPLUSTAX1AMOUNT,DEALTYPE2,DEALTYPE3,NOTINCLUDEDTAX0SALE,ZSEQUENCENUMBER,INVOICECOUNT,EXCHANGEDIFFERENCE,INVOICEHEAD,INVOICENUMBER,DISCOUNTTOTALAMOUNT,GROSSSALETAX2AMOUNT,SIPLUSTAX3AMOUNT,DISCOUNTTAX1AMOUNT,GROSSSALETAX0AMOUNT,NOTINCLUDEDTAX1SALE,NOTINCLUDEDTOTALSALE,SYSTEMDATE,SIPLUSTAX2AMOUNT)values ('0.00','20.00','0.00','0.00','0.00','0.00','','0.00',TO_DATE('1970-01-01 00:00:00','YYYY-MM-DD HH24:MI:SS'),'2','0.00','924','0.00','0.00','0.00','','0.00',TO_DATE('1970-01-01 00:00:00','YYYY-MM-DD HH24:MI:SS'),'20.00','','0.00','2.90','0.00','','00','0.00','12345','0.00','20.00','0.00','0.00','0.00','0.00','111112','0.00','9999999','0.00','0.00','','','','','0.00','20','00','20.00','2','10.00','0','0.00','0','0.00','0','10.00','0.00','0','0','0.00','0.00','1','3','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0','1','0.00','0','0.00','0','0','0.00','1','0','0.00','AB','0','0.00','0.00','0.00','0.00','0.00','0.00','0.00',TO_DATE('2008-08-14 17:38:10','YYYY-MM-DD HH24:MI:SS'),'0.00')");
        
    }

    private static void test1() {
        String storeID = "1";
        String itemNo = "1";
        String shipNumber = "0";
        String accSaleQuantity = "3";
        String accSaleRtnQuantity = "4";
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date dates = cal.getTime();
        String insertStringPrep = "INSERT INTO inv (storeID,itemNumber,shipNumber,busiDate," + "accsalequantity, accsalertnquantity)";
        String insertStringPost = " VALUES('"+storeID+"','"+itemNo+"','"+shipNumber+"','"+new SimpleDateFormat("yyyy-MM-dd").format(dates)+"'";
        String s = insertStringPrep + insertStringPost + accSaleQuantity + "," + accSaleRtnQuantity + ")";
        System.out.println(s);
    }




}
