package hyi.cream.inline;

import java.text.*;
import java.sql.*;
import java.util.*;
import java.util.Date;

import hyi.cream.util.*;

/**
 * Transaction integrity checker thread.
 * 
 * @author Bruce
 */
public class TransactionCheckerThread extends Thread {
    
    private static String storeID;
    private boolean isServerSideOracle = Server.serverExist() && "oracle".equalsIgnoreCase(CreamProperties.getInstance().getProperty("DBType"));

    public void run() {
        while (true) {
            retrieveStoreID();
            transactionIntegrityCheck();
            
            // check every 5 minutes
            try {
                Thread.sleep(5L * 60 * 1000);
            } catch (InterruptedException e) {
            }
        }
    }

    private static Map query(String selectStatement) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        HashMap map = new HashMap();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectStatement);
            ResultSetMetaData metaData = resultSet.getMetaData();
            if (resultSet.next()) {
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    map.put(metaData.getColumnName(i).toLowerCase(), resultSet.getObject(i));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
            return map;
        }
    }

    public static void retrieveStoreID() {
        String query = "SELECT storeid FROM store";
        Map value = query(query);
        if (value != null) {
            if (value.containsKey("storeid"))
               storeID = value.get("storeid").toString();
        }
    }

    /**
     * 检查14天内的交易是否有单头单身不符的，如果有，删除它们。
     */
    private void transactionIntegrityCheck() {
//        Date date = new Date();
//        String end = df.format(date);
//        Date yesterday = new Date(date.getTime() - 14L * 24L * 60L * 60L * 1000L);
//        String begin = df.format(yesterday);  // 检查14天内的交易
//      sql1 = // 有头但单身笔数不符 
//          "select h.systemDate, h.posNumber, h.transactionNumber, " +
//          "   sum(h.detailCount)/count(*) Y, count(*) X, max(d.transactionNumber) Z " +
//          "from posul_tranhead h left join posul_trandtl d " +
//          "   on h.storeID=d.storeID and h.systemDate=d.systemDate and h.posNumber=d.posNumber and " +
//          "      h.transactionNumber=d.transactionNumber " +
//          "where h.storeID='" + storeID + "' and h.systemDate between '" + 
//          begin + "' and '" + end + "' " +
//          "group by h.systemDate, h.posNumber, h.transactionNumber " +
//          "having (Y<>X or " +  //头身笔数不符
//          "   (Y/X=X and Z is null))" + //有单头但无单身（单身笔数为1时）
//          "   and not (Y=0 and Z is null)"; //正常有头没身的不要显示
//      sql2 = // 有身没有头
//          "select d.systemDate, d.posNumber, d.transactionNumber, sum(h.detailCount) " +
//          "from posul_tranhead h right join posul_trandtl d " +
//          "    on h.systemDate=d.systemDate and h.posNumber=d.posNumber and " +
//          "       h.transactionNumber=d.transactionNumber " +
//          "where d.systemDate between '" + begin + "' and '" + end + "' " +
//          "group by d.systemDate, d.posNumber, d.transactionNumber " +
//          "having sum(h.detailCount)=0";

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sql1, sql2;
        if (isServerSideOracle) {
            sql1 = " select h.systemDate, h.posNumber, h.transactionNumber,                                  "
                + "    sum(h.detailCount)/count(*) Y, count(*) X, max(d.transactionNumber) Z                "
                + " from posul_tranhead h left join posul_trandtl d                                         "
                + "    on h.storeID=d.storeID and h.systemDate=d.systemDate and h.posNumber=d.posNumber and "
                + "       h.transactionNumber=d.transactionNumber                                           "
                + " where h.systemDate between sysdate - 14                                                 "
                + "    and sysdate                                                                          "
                + " group by h.systemDate, h.posNumber, h.transactionNumber                                 "
                + " having (sum(h.detailCount)/count(*)<>count(*) or                                        "
                + "    (sum(h.detailCount)/count(*)/count(*)=count(*) and max(d.transactionNumber) is null))"
                + "    and not (sum(h.detailCount)/count(*)=0 and max(d.transactionNumber) is null)         "
                + "                                                                                         ";

            sql2 =  " select d.systemDate, d.posNumber, d.transactionNumber, sum(h.detailCount)               "
                + " from posul_tranhead h right join posul_trandtl d                                        "
                + "     on h.systemDate=d.systemDate and h.posNumber=d.posNumber and                        "
                + "        h.transactionNumber=d.transactionNumber                                          "
                + " where d.systemDate between sysdate - 14                                                 "
                + "    and sysdate                                                                          "
                + " group by d.systemDate, d.posNumber, d.transactionNumber                                 "
                + " having sum(h.detailCount)=0 or sum(h.detailCount) is null                               ";

            
        } else {
            sql1 = // 有头但单身笔数不符
                    "select h.systemDate, h.posNumber, h.transactionNumber, " +
                            "   sum(h.detailCount)/count(*) Y, count(*) X, max(d.transactionNumber) Z " +
                            "from posul_tranhead h left join posul_trandtl d " +
                            "   on h.storeID=d.storeID and h.systemDate=d.systemDate and h.posNumber=d.posNumber and " +
                            "      h.transactionNumber=d.transactionNumber " +
                            "where h.systemDate between DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 14 DAY),'%Y-%m-%d %H:%i:%s') " +
                            "   and DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i:%s') " +
                            "group by h.systemDate, h.posNumber, h.transactionNumber " +
                            "having (Y<>X or " +  //头身笔数不符
                            "   (Y/X=X and Z is null))" + //有单头但无单身（单身笔数为1时）
                            "   and not (Y=0 and Z is null)"; //正常有头没身的不要显示

            sql2 = // 有身没有头
                    "select d.systemDate, d.posNumber, d.transactionNumber, sum(h.detailCount) " +
                            "from posul_tranhead h right join posul_trandtl d " +
                            "    on h.systemDate=d.systemDate and h.posNumber=d.posNumber and " +
                            "       h.transactionNumber=d.transactionNumber " +
                            "where d.systemDate between DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 14 DAY),'%Y-%m-%d %H:%i:%s') " +
                            "   and DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i:%s') " +
                            "group by d.systemDate, d.posNumber, d.transactionNumber " +
                            "having sum(h.detailCount)=0";

        }

        // first round
        List first1 = queryTransaction(sql1);
        List first2 = queryTransaction(sql2);
        first1.addAll(first2);      // merge two List;
        if (first1.size() == 0)
            return;

        try {
            Thread.sleep(5L * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // second round
        List second1 = queryTransaction(sql1);
        List second2 = queryTransaction(sql2);
        second1.addAll(second2);

        // 在两个轮回中皆找到的交易才做删除
        List inconsistentTrans = intersect(first1, second1);
        
        // delete inconsistent transaction
        Iterator iter = inconsistentTrans.iterator();
        while (iter.hasNext()) {
            Object[] obj = (Object[])iter.next();
            if (isServerSideOracle) {
                delete("delete from posul_trandtl where systemDate=TO_DATE('" +
                        df.format((Date)obj[0]) + "','YYYY-MM-DD HH24:MI:SS') and posNumber=" + ((Integer)obj[1]).intValue() +
                        " and transactionNumber=" + ((Integer)obj[2]).intValue());
                delete("delete from posul_tranhead where systemDate=TO_DATE('" +
                        df.format((Date)obj[0]) + "','YYYY-MM-DD HH24:MI:SS') and posNumber=" + ((Integer)obj[1]).intValue() +
                        " and transactionNumber=" + ((Integer)obj[2]).intValue());
            } else {
                delete("delete from posul_trandtl where systemDate='" +
                        df.format((Date)obj[0]) + "' and posNumber=" + ((Integer)obj[1]).intValue() +
                        " and transactionNumber=" + ((Integer)obj[2]).intValue());
                delete("delete from posul_tranhead where systemDate='" +
                        df.format((Date)obj[0]) + "' and posNumber=" + ((Integer)obj[1]).intValue() +
                        " and transactionNumber=" + ((Integer)obj[2]).intValue());

            }


            Server.log("Delete partial transaction, pos=" + obj[1] + ", no=" + obj[2]);
        }
    }

    static private List intersect(List first, List second) {
        List retList = new ArrayList();
        Iterator iter = first.iterator();
out:
        while (iter.hasNext()) {
            Object[] obj1 = (Object[])iter.next();
            for (int i = 0; i < second.size(); i++) {
                Object[] obj2 = (Object[])second.get(i);
                if (obj1[0].equals(obj2[0]) && obj1[1].equals(obj2[1]) &&
                    obj1[2].equals(obj2[2])) {
                    retList.add(obj1);
                    second.remove(i);
                    continue out;
                }
            }
        }
        return retList;
    }

    private boolean delete(String sql) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            statement.executeUpdate(sql);
            return true;
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
            return false;
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
        }
    }

    private List queryTransaction(String sql) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List resultList = new ArrayList();
        try {
            connection = CreamToolkit.getPooledConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            ResultSetMetaData metaData = resultSet.getMetaData();
            while (resultSet.next()) {
                Object[] valueArray = new Object[3];
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    String colName = metaData.getColumnName(i);
                    if (colName.equalsIgnoreCase("systemDate")) {
                        valueArray[0] = new Date(resultSet.getTimestamp(i).getTime());
                    } else if (colName.equalsIgnoreCase("posNumber")) {
                        valueArray[1] = new Integer(resultSet.getInt(i));
                    } else if (colName.equalsIgnoreCase("transactionNumber")) {
                        valueArray[2] = new Integer(resultSet.getInt(i));
                    }
                }
                resultList.add(valueArray);
            }
        } catch (SQLException e) {
            e.printStackTrace(Server.getLogger());
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
                if (connection != null)
                    CreamToolkit.releaseConnection(connection);
            } catch (SQLException e) {
                e.printStackTrace(Server.getLogger());
            }
            return resultList;
        }
    }
    
//    public static void main(String[] arg) {
//        // test intersect()
//        List list1 = new ArrayList();
//        Date now = new Date();
//        list1.add(new Object[] {now, new Integer(1), new Integer(2)});
//        list1.add(new Object[] {now, new Integer(11), new Integer(22)});
//        list1.add(new Object[] {now, new Integer(111), new Integer(222)});
//
//        List list2 = new ArrayList();
//        list2.add(new Object[] {now, new Integer(111), new Integer(222)});
//        now = new Date(now.getTime() + 1L);
//        list2.add(new Object[] {now, new Integer(11), new Integer(22)});
//        list2.add(new Object[] {now, new Integer(1), new Integer(2)});
//        List retList = intersect(list1, list2);
//        for (int i = 0; i < retList.size(); i++) {
//            Object[] obj = (Object[])retList.get(i);
//            System.out.println("" + obj[0] + ", " + obj[1] + ", " + obj[2]);
//        }
//    }
}
