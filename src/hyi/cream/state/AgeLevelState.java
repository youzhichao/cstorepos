
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
//import hyi.cream.state.*;
import hyi.cream.uibeans.*;

import java.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class AgeLevelState extends State {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private int custID;
    static AgeLevelState ageLevelState = null;

    public static AgeLevelState getInstance() {
        try {
            if (ageLevelState == null) {
                ageLevelState = new AgeLevelState();
            }
        } catch (InstantiationException ex) {
        }
        return ageLevelState;
    }

    /**
     * Constructor
     */
    public AgeLevelState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        System.out.println("agelevel state entry");
        custID = ((AgeLevelButton)event.getSource()).getAgeID();
    }

	public Class exit(EventObject event, State sinkState) {
        app.getCurrentTransaction().setCustomerAgeLevel(custID);
        return sinkState.getClass();
	}
}

 
