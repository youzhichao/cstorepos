package hyi.cream.state;

public interface AlphanumericProvider {
    /**
     * Get alphanumeric data inside. 
     *
     * @return the alphanumeric data.
     */
    public String getAlphanumericData();
}

