
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.util.*;
import java.util.*;
import hyi.cream.uibeans.*;
import jpos.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class BuyerState extends SomeAGReadyState {
    static BuyerState buyerState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    public static BuyerState getInstance() {
        try {
            if (buyerState == null) {
                buyerState = new BuyerState();
            }
        } catch (InstantiationException ex) {
        }
        return buyerState;
    }

    /**
	 * Constructor
     */
	public BuyerState() throws InstantiationException {
	}

	public String getPromptedMessage() {
		ResourceBundle res = CreamToolkit.GetResource();
		if (!app.getBuyerNumberPrinted())
			return res.getString("InputBuyerNumberHint");
		else {
			return res.getString("ErrorOfBuyerNo");
		}
	}

	public Class exit(EventObject event, State sinkState) {
		/*if (getAlphanumericData() == null || checkValidity()) {
			return getUltimateSinkState();
		} else {
			// Get warning message by getWarningMessage() and
			// display it on warningIndicator.
			app.getWarningIndicator().setMessage(getWarningMessage());
			return getInnerInitialState();
		}*/
		app.getWarningIndicator().setMessage("");
		
		if (event == null)
			return BuyerState.class;
		
		if (event.getSource() instanceof NumberButton) {
			if (app.getBuyerNumberPrinted()) {
				/*POSPeripheralHome posHome = POSPeripheralHome.getInstance();
					ToneIndicator tone = null;
					try {
						tone = posHome.getToneIndicator();er
					} catch (NoSuchPOSDeviceException ne) { CreamToolkit.logMessage(ne + "!"); }
					try {
						if (!tone.getDeviceEnabled())
							tone.setDeviceEnabled(true);
						if (!tone.getClaimed()) {
							 tone.claim(0);
						//tone.claim(0);
							 tone.setAsyncMode(true);
							 tone.sound(99999, 500);
						}
					} catch (JposException je) {System.out.println(je);}*/
				//super.exit(event, sinkState);
				//System.out.println(app.getBuyerNumberPrinted());
				return BuyerState.class;
			} else {
				//super.exit(event, sinkState);
				return 	hyi.cream.state.BuyerNumberingState.class;
			}
		}
		return BuyerState.class;	
	}

}

