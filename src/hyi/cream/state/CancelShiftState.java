
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.event.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class CancelShiftState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
	private static CancelShiftState zState = null;
	private ResourceBundle res = CreamToolkit.GetResource();


	public static CancelShiftState getInstance() {
        try {
            if (zState == null) {
				zState = new CancelShiftState();
            }
        } catch (InstantiationException ex) {
        }
        return zState;
    }

    /**
     * Constructor
     */
	public CancelShiftState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
		//System.out.println("ShiftState2 Enter");
		app.getMessageIndicator().setMessage(res.getString("ConfirmShiftCancel"));
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof EnterButton) {
			app.getWarningIndicator().setMessage(res.getString("shiftEnd") + ", " + res.getString("NoDataWriteToDisc"));
			app.getMessageIndicator().setMessage("");
		}
		return sinkState.getClass();		
	}
}

 
