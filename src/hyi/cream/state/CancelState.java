/**
 * State class
 * @since 2000
 * @author slackware
 */

package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;

public class CancelState extends State {
	private	ResourceBundle res = CreamToolkit.GetResource();
    private Class enterExitState = null;
    private Class clearExitState = null;
    private boolean printCancel = false;
    private boolean keyLock = false;
    private String warning = "";
    private String message = "";

    static CancelState cancelState = null;

    public static CancelState getInstance() {
        try {
            if (cancelState == null) {
                cancelState = new CancelState();
            }
        } catch (InstantiationException ex) {
        }
        return cancelState;
    }

    /**
     * Constructor
     */
    public CancelState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
		//System.out.println("This is CancelState's Entry!");

		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();

        message = posTerminal.getMessageIndicator().getMessage();
        warning = posTerminal.getWarningIndicator().getMessage();

		posTerminal.getMessageIndicator().setMessage(res.getString("CancelConfirm"));
        
        if (sourceState instanceof IdleState
        	|| sourceState instanceof CashierRightsCheckState) 
       	{
            if (!posTerminal.getTrainingMode()) {
                if (!CreamPrinter.getInstance().getHeaderPrinted() ) {
                    CreamPrinter.getInstance().printHeader();
                    CreamPrinter.getInstance().setHeaderPrinted(true);
                }
                if (POSTerminalApplication.getInstance().getCurrentTransaction().getCurrentLineItem() != null) {
                    CreamPrinter.getInstance().printLineItem(POSTerminalApplication.getInstance().getCurrentTransaction().getCurrentLineItem());
                }
            }
            enterExitState = InitialState.class;
            clearExitState = IdleState.class;
        } else if (sourceState instanceof DaiFuIdleState) {
            enterExitState = InitialState.class;
            clearExitState = DaiFuIdleState.class;
        } else if (sourceState instanceof PaidInIdleState) {
            enterExitState = InitialState.class;
            clearExitState = PaidInIdleState.class;
        } else if (sourceState instanceof PaidOutIdleState) {
            enterExitState = InitialState.class;
            clearExitState = PaidOutIdleState.class;
        } else {
            enterExitState = KeyLock1State.class;
            clearExitState = sourceState.getClass();
            keyLock = true;
        }

        //  check print
        if (sourceState instanceof CashInIdleState
            || sourceState instanceof CashInState
            || sourceState instanceof CashOutIdleState
            || sourceState instanceof CashOutState
            || sourceState instanceof DaiFuIdleState
            || sourceState instanceof PaidInIdleState
            || sourceState instanceof PaidOutIdleState) {
            printCancel = false;
        } else {
            printCancel = true;
        }

    }

	public Class exit(EventObject event, State sinkState) {
		//System.out.println("This is CancelState's Exit!");
		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        // init newCashierID
        posTerminal.setNewCashierID("");
		if (sinkState instanceof WarningState)
		    return WarningState.class;
		posTerminal.getMessageIndicator().setMessage("");
		if (event.getSource().getClass().getName().endsWith("EnterButton")) {
            if (printCancel) {
                posTerminal.getWarningIndicator().setMessage(res.getString("ReturnEndMessage"));
            }
            posTerminal.setChecked(false);
            if (posTerminal.getTrainingMode()) {
                return enterExitState;
            }
            
			if (posTerminal.getCurrentTransaction() == null) {
                keyLock = false;
				return enterExitState;
            }
			Object[] l = posTerminal.getCurrentTransaction().getLineItemsArrayLast();
			if (l.length > 0) {
                //  do for current transaction
				Transaction current = posTerminal.getCurrentTransaction();
				current.setDealType1("*");
				current.setDealType2("0");
				current.setDealType3("0");
                current.initForAccum();
                current.initSIInfo();
                current.accumulate();
                CreamToolkit.logMessage("store() in CancelState exit() L128 ");
                current.store();
                //  print
                if (printCancel) {
				    CreamPrinter.getInstance().printCancel("Canceled!");
				    CreamPrinter.getInstance().setHeaderPrinted(false);
				}
				//current.Clear();
                //  do for new transaction
                int nextTranNumber = current.getNextTransactionNumber();
				Transaction newTran = Transaction.queryByTransactionNumber(current.getTransactionNumber().toString());

                newTran.setTransactionNumber(nextTranNumber - 1);
				newTran.setDealType1("0");
				newTran.setDealType2("0");
				newTran.setDealType3("3");
                newTran.setVoidTransactionNumber(new Integer(nextTranNumber - 1));
                newTran.setStoreNumber(CreamProperties.getInstance().getProperty("StoreNumber"));
                newTran.setTerminalNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("TerminalNumber")));
				newTran.setTransactionNumber(Transaction.getNextTransactionNumber());//Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber")));
                newTran.setSignOnNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("ShiftNumber")));
                newTran.setTransactionType("00");
                newTran.setTerminalPhysicalNumber(CreamProperties.getInstance().getProperty("TerminalPhysicalNumber"));
                newTran.setCashierNumber(CreamProperties.getInstance().getProperty("CashierNumber"));
				newTran.setBuyerNumber("");

                long ll = System.currentTimeMillis();
                newTran.makeNegativeValue();
                ll = System.currentTimeMillis();

                //  for daishou 
                Iterator ite = current.getLineItems();
                Iterator ite2 = newTran.getLineItems();
                while (ite.hasNext()) {
                    LineItem li = (LineItem)ite.next();
                    LineItem li2 = (LineItem)ite2.next();
                    li2.setDaishouNumber(li.getDaishouNumber());
                }
                CreamToolkit.logMessage("store() in CancelStateexit() L166");
                newTran.store();

                posTerminal.getMessageIndicator().setMessage(res.getString("TransactionEnd"));

                newTran.Clear();
                
                if (keyLock) {                                   
                    posTerminal.getItemList().setTransaction(posTerminal.getCurrentTransaction());
                    posTerminal.getCurrentTransaction().clear();
                    posTerminal.getCurrentTransaction().clearLineItem();
                    posTerminal.getCurrentTransaction().setDealType1("0");
                    posTerminal.getCurrentTransaction().setDealType2("0"); 
                    posTerminal.getCurrentTransaction().setDealType3("0");
                    posTerminal.getCurrentTransaction().setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
                    posTerminal.getCurrentTransaction().setStoreNumber(CreamProperties.getInstance().getProperty("StoreNumber"));
                    posTerminal.getCurrentTransaction().setTerminalPhysicalNumber(CreamProperties.getInstance().getProperty("TerminalPhysicalNumber"));
                    keyLock = false;
                }

                return enterExitState;
            }
		} else if (event.getSource().getClass().getName().endsWith("ClearButton")) {
            keyLock = false;      
            posTerminal.getMessageIndicator().setMessage(message);
            posTerminal.getWarningIndicator().setMessage(warning);
			return clearExitState;
		}

        return sinkState.getClass();
    }
}
