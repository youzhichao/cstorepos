
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.event.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class CancelZState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
	private static CancelZState zState = null;
	private ResourceBundle res = CreamToolkit.GetResource();

	public static CancelZState getInstance() {
        try {
            if (zState == null) {
				zState = new CancelZState();
            }
        } catch (InstantiationException ex) {
        }
        return zState;
    }

    /**
     * Constructor
     */
	public CancelZState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
		//System.out.println("ShiftState2 Enter");
		app.getMessageIndicator().setMessage(res.getString("ConfirmZCancel"));
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof EnterButton) {
			app.getWarningIndicator().setMessage(res.getString("zEnd") + ", " + res.getString("NoDataWriteToDisc"));
			ZReport z = ZReport.getOrCreateCurrentZReport();
			String zPrint = CreamProperties.getInstance().getProperty("ZPrint");
			if (zPrint == null) {
			   zPrint = "no";
			}
			if (zPrint.equals("yes"))
				CreamPrinter.getInstance().printZReport(z, this);
            
            return ConfirmZState.getSourceState();
       } else 
		  return sinkState.getClass();		
	}
}

 
