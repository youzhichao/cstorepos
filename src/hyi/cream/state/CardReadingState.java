
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.state.*;
import hyi.cream.*;
import hyi.cream.util.*;

import java.util.*;
import jpos.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class CardReadingState extends State {

    private POSTerminalApplication app   = POSTerminalApplication.getInstance();
    private String cardNumber            = "";
    private String creditCardType        = "";
    private ResourceBundle res           = CreamToolkit.GetResource();
    static CardReadingState cardReadingState = null;

    public static CardReadingState getInstance() {
        try {
            if (cardReadingState == null) {
                cardReadingState = new CardReadingState();
            }
        } catch (InstantiationException ex) {
        }
        return cardReadingState;
    }

    /**
     * Constructor
     */
    public CardReadingState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState.getClass() == Paying2State.class) {
            app.getMessageIndicator().setMessage(res.getString("CardReadingWarning"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        Object eventSource = event.getSource();
        if (eventSource instanceof MSR) {
            try {
                switch (((MSR)eventSource).getTracksToRead()) {
                case 1:
                    cardNumber = new String(((MSR)eventSource).getTrack1Data());
                    break;
                case 2:
                    cardNumber = new String(((MSR)eventSource).getTrack2Data());
                    break;
                case 3:
                    cardNumber = new String(((MSR)eventSource).getTrack3Data());
                    break;
                }
            } catch (JposException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Jpos exception at MSR ");
            }
            System.out.println(" card number is " + cardNumber);
            int cardNo = Integer.parseInt(cardNumber.substring(0, 4));
            if (cardNo >= 0100 && cardNo <= 3199) {
                creditCardType = "01";
            } else if (cardNo == 4563 || cardNo == 4579) {
                creditCardType = "02";
            } else if (cardNo >= 4000 && cardNo <= 4999) {
                creditCardType = "03";
            } else if (cardNo >= 5000 && cardNo <= 5999) {
                creditCardType = "05";
            } else if ((cardNo >= 3400 && cardNo <= 3499)
                        || cardNo >= 3700 && cardNo <= 3799) {
                creditCardType = "06";
            } else if (cardNo == 1800 || cardNo == 2131
                       || (cardNo >= 3528 && cardNo <= 3589)) {
                creditCardType = "08";
            } else if (cardNo == 1040 || cardNo == 2030
                       || (cardNo >= 3000 && cardNo <= 3099)
                       || (cardNo >= 3600 && cardNo <= 3699)
                       || (cardNo >= 3800 && cardNo <= 3899)) {
                creditCardType = "07";
            } else if (cardNo >= 0 || cardNo <= 9) {
                creditCardType = "99";
            }
            app.getCurrentTransaction().setCreditCardType(creditCardType);
            app.getCurrentTransaction().setCreditCardNumber(cardNumber);
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("CardNumber") +
                    app.getCurrentTransaction().getCreditCardNumber());
        }
        return sinkState.getClass();
    }
}

