package hyi.cream.state;

import java.util.*;
import java.math.*;     
import java.awt.event.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class CashInIdleState extends State {
    static CashInIdleState cashInIdleState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Transaction trans = app.getCurrentTransaction();
    private ResourceBundle res = CreamToolkit.GetResource();

    public static CashInIdleState getInstance() {
        try {
            if (cashInIdleState == null) {
                cashInIdleState = new CashInIdleState();
            }
        } catch (InstantiationException ex) {
        }
        return cashInIdleState;
    }

    /**
     * Constructor
     */
    public CashInIdleState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("CashInIdleState entry");
        if (event == null) {
            return;
        }           
        
        // modify lxf //2003.03.03
//        if (sourceState instanceof KeyLock1State
//            && event.getSource() instanceof SelectButton) {

//        Collection fds = Cashier.getExistedFieldList("cashier");
//        
//        if ((!fds.contains("CASRIGHTS") || sourceState instanceof CashierRightsCheckState)
//            && event.getSource() instanceof EnterButton) {
        trans.setDealType2("H");

        app.getMessageIndicator().setMessage(res.getString("CashMessage"));
//        app.getWarningIndicator().setMessage(""); 
        app.getItemList().setVisible(false);
        app.getPayingPane().setTransaction(trans);
        //app.getPayingPane().setMode(1);
        app.getPayingPane().setVisible(true);
//        }

        app.getPayingPane().repaint();

        app.getMessageIndicator().setMessage(res.getString("CashMessage"));
    }

    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");

        // init newCashierID
        POSTerminalApplication.getInstance().setNewCashierID("");
        if (event.getSource() instanceof ClearButton) {
            app.getMessageIndicator().setMessage("");
            app.getWarningIndicator().setMessage("");
            //app.getPayingPane().setMode(0);
            app.getPayingPane().clear();
            return KeyLock1State.class;
        } else if (event.getSource() instanceof EnterButton) {
            if (trans.getLineItemsArrayLast().length == 0) {
                return CashInIdleState.class;
            }

            //  set to transaction
            trans.setGrossSalesAmount(new BigDecimal(0));
            trans.setNetSalesAmount(new BigDecimal(0));
            trans.setNetSalesAmount0(new BigDecimal(0));
            trans.setNetSalesAmount1(new BigDecimal(0));
            trans.setNetSalesAmount2(new BigDecimal(0));
            trans.setNetSalesAmount3(new BigDecimal(0));
            trans.setNetSalesAmount4(new BigDecimal(0));
            trans.setMixAndMatchAmount0(new BigDecimal(0));
            trans.setMixAndMatchAmount1(new BigDecimal(0));
            trans.setMixAndMatchAmount2(new BigDecimal(0));
            trans.setMixAndMatchAmount3(new BigDecimal(0));
            trans.setMixAndMatchAmount4(new BigDecimal(0));
            trans.setMixAndMatchCount0(0);
            trans.setMixAndMatchCount1(0);
            trans.setMixAndMatchCount2(0);
            trans.setMixAndMatchCount3(0);
            trans.setMixAndMatchCount4(0);   
            trans.clearMMDetail();

            app.getMessageIndicator().setMessage(res.getString("TransactionEnd"));
            app.getWarningIndicator().setMessage(""); 
            //app.getPayingPane().setMode(0);
            app.getPayingPane().clear();

            return DrawerOpenState.class;
        } else if (event.getSource() instanceof CancelButton) {
            return CancelState.class;
        } else if (event.getSource() instanceof NumberButton) {
            return CashInState.class;
        } else {
            return CashInIdleState.class;
        }
    }
}

