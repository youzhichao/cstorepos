package hyi.cream.state;

import java.util.*;
import java.math.*;
import java.awt.event.*;

import jpos.*;
import jpos.events.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class CashInState extends State {
    static CashInState cashInState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Transaction trans = app.getCurrentTransaction();
    private static String numberString = "";
    private boolean deviceClaimed = false;
    private ResourceBundle res = CreamToolkit.GetResource();

    public static CashInState getInstance() {
        try {
            if (cashInState == null) {
                cashInState = new CashInState();
            }
        } catch (InstantiationException ex) {
        }
        return cashInState;
    }

    /**
     * Constructor
     */
    public CashInState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("CashInState Entry");
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            numberString = numberString + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(numberString);
            app.getWarningIndicator().setMessage("");
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("CashInState exit");
        if (event.getSource() instanceof EnterButton) {
            if (!CreamToolkit.checkInput(numberString, new BigDecimal(0))) {
                numberString = "";
                app.getWarningIndicator().setMessage(res.getString("InputWrong"));
                return CashInIdleState.class;
            }
            
            String paymentID = "00";

            Payment curPayment = Payment.queryByPaymentID(paymentID);
            int state = trans.setPayments(curPayment);

            BigDecimal price = new BigDecimal(numberString);
            price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
            
            if (price.compareTo(new BigDecimal(0)) <= 0) {
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                numberString = "";
                return CashInIdleState.class;
            }
            
            if (price.compareTo(new BigDecimal(CreamProperties.getInstance().getProperty("MaxPrice"))) == 1) {
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyPrice"));
                numberString = "";
                return CashInIdleState.class;
            }

            if (state == 0) {
                app.getMessageIndicator().setMessage(res.getString("Wrong"));
                return CashInIdleState.class;
            }

            String payno = "";
            String payamt = "";
            trans.setLastestPayment(curPayment);
            //System.out.println(lastestPayment);
            //paymentAvailable = true;
            if (state == 1) {
                for (int i = 1; i < 5; i++) {
                    payno = "PAYNO" + i;
                    payamt = "PAYAMT" + i;
                    if (trans.getFieldValue(payno) == null) {
                        break;
                    }
                    if (((String)trans.getFieldValue(payno)).equals(paymentID)) {
                        trans.setFieldValue(payamt, ((BigDecimal)trans.getFieldValue(payamt)).add(price));
                    }
                }
            }
            if (state == 2) {        
                ArrayList paynoArray = new ArrayList();
                ArrayList payamtArray = new ArrayList();
                if (curPayment.getPaymentType().charAt(4) == '0') {
                    paynoArray.add(paymentID);
                    payamtArray.add(price);
                    for (int i = 1; i < 5; i++) {
                        payno = "PAYNO" + i;
                        payamt = "PAYAMT" + i;
                        if (trans.getFieldValue(payno) == null) {
                            break;
                        }
                        paynoArray.add(trans.getFieldValue(payno));
                        payamtArray.add(trans.getFieldValue(payamt));
                    }
                    for (int i = 0; i < paynoArray.size(); i++) {
                        payno = "PAYNO" + (i + 1);
                        payamt = "PAYAMT" + (i + 1);
                        trans.setFieldValue(payno, paynoArray.get(i));
                        trans.setFieldValue(payamt, payamtArray.get(i));
                    }
                } else {
                    for (int i = 1; i < 5; i++) {
                        payno = "PAYNO" + i;
                        payamt = "PAYAMT" + i;
                        if (trans.getFieldValue(payno) == null) {
                            trans.setFieldValue(payno, paymentID);
                            trans.setFieldValue(payamt, price);
                            break;
                        }
                    }
                }
            }

            try {
                LineItem cashInItem = new LineItem();
                cashInItem.setDetailCode("H");
                cashInItem.setPluNumber(paymentID);
                cashInItem.setTransactionNumber(new Integer(trans.getNextTransactionNumber()));
                cashInItem.setTerminalNumber(trans.getTerminalNumber());
                cashInItem.setRemoved(false);
                cashInItem.setDescription(res.getString("CashIn"));
                cashInItem.setUnitPrice(price);            
                cashInItem.setQuantity(new BigDecimal(0));
                cashInItem.setAmount(new BigDecimal(0));
                trans.addLineItem(cashInItem);
                numberString = "";
            } catch (InstantiationException e) {
            } catch (TooManyLineItemsException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Too many LineItem exception at " + this);
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyLineItems"));
            }
            return CashInIdleState.class;
        } else if (event.getSource() instanceof ClearButton) {
            if (numberString.equals("")) {
                return CashInIdleState.class;
            } else {
                numberString = "";
                return CashInIdleState.class;
            }
        }
        return sinkState.getClass();
    }
}

