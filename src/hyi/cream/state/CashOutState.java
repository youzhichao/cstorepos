package hyi.cream.state;

import java.util.*;
import java.math.*;
import java.awt.event.*;

import jpos.*;
import jpos.events.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class CashOutState extends State {
    static CashOutState cashOutState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Transaction trans = app.getCurrentTransaction();
    private String numberString = "";
    private boolean deviceClaimed = false;
    private ResourceBundle res = CreamToolkit.GetResource();

    public static CashOutState getInstance() {
        try {
            if (cashOutState == null) {
                cashOutState = new CashOutState();
            }
        } catch (InstantiationException ex) {
        }
        return cashOutState;
    }

    /**
     * Constructor
     */
    public CashOutState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("CashOutState entry");
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            numberString = numberString + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(numberString);
            app.getWarningIndicator().setMessage("");
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) { 
            if (!CreamToolkit.checkInput(numberString, new BigDecimal(0))) {
                numberString = "";
                app.getWarningIndicator().setMessage(res.getString("InputWrong"));
                return CashOutIdleState.class;
            }

            String paymentID = "00";

            Payment curPayment = Payment.queryByPaymentID(paymentID);
            int state = trans.setPayments(curPayment);
            //System.out.println(curPayment);  

            BigDecimal price = new BigDecimal(numberString);
            price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
            
            if (price.compareTo(new BigDecimal(0)) <= 0) {
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                numberString = "";
                return CashOutIdleState.class;
            }
            
            if (price.compareTo(new BigDecimal(CreamProperties.getInstance().getProperty("MaxPrice"))) == 1) {
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyPrice"));
                numberString = "";
                return CashOutIdleState.class;
            }

            if (state == 0) {
                app.getMessageIndicator().setMessage(res.getString("Wrong"));
                return CashOutIdleState.class;
            }

            String payno = "";
            String payamt = "";
            trans.setLastestPayment(curPayment);
            //System.out.println(lastestPayment);
            //paymentAvailable = true;
            if (state == 1) {
                for (int i = 1; i < 5; i++) {
                    payno = "PAYNO" + i;
                    payamt = "PAYAMT" + i;
                    if (trans.getFieldValue(payno) == null) {
                        break;
                    }
                    if (((String)trans.getFieldValue(payno)).equals(paymentID)) {
                        trans.setFieldValue(payamt, ((BigDecimal)trans.getFieldValue(payamt)).add(price));
                    }
                }
            }
            if (state == 2) {        
                ArrayList paynoArray = new ArrayList();
                ArrayList payamtArray = new ArrayList();
                if (curPayment.getPaymentType().charAt(4) == '0') {
                    paynoArray.add(paymentID);
                    payamtArray.add(price);
                    for (int i = 1; i < 5; i++) {
                        payno = "PAYNO" + i;
                        payamt = "PAYAMT" + i;
                        if (trans.getFieldValue(payno) == null) {
                            break;
                        }
                        paynoArray.add(trans.getFieldValue(payno));
                        payamtArray.add(trans.getFieldValue(payamt));
                    }
                    for (int i = 0; i < paynoArray.size(); i++) {
                        payno = "PAYNO" + (i + 1);
                        payamt = "PAYAMT" + (i + 1);
                        trans.setFieldValue(payno, paynoArray.get(i));
                        trans.setFieldValue(payamt, payamtArray.get(i));
                    }
                } else {
                    for (int i = 1; i < 5; i++) {
                        payno = "PAYNO" + i;
                        payamt = "PAYAMT" + i;
                        if (trans.getFieldValue(payno) == null) {
                            trans.setFieldValue(payno, paymentID);
                            trans.setFieldValue(payamt, price);
                            break;
                        }
                    }
                }
            }
            
            try {
                LineItem cashOutItem = new LineItem();
                cashOutItem.setDetailCode("G");
                cashOutItem.setPluNumber(paymentID);
                cashOutItem.setTransactionNumber(new Integer(trans.getNextTransactionNumber()));
                cashOutItem.setTerminalNumber(trans.getTerminalNumber());
                cashOutItem.setRemoved(false);
                cashOutItem.setDescription(res.getString("CashOut"));
                cashOutItem.setUnitPrice(price);
                cashOutItem.setQuantity(new BigDecimal(0));
                cashOutItem.setAmount(new BigDecimal(0));
                trans.addLineItem(cashOutItem);
                numberString = "";
            } catch (InstantiationException e) {
                System.out.println(e);
            } catch (TooManyLineItemsException e) {   
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Too many LineItem exception at " + this);
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyLineItems"));
            }
        } else if (event.getSource() instanceof ClearButton) {
            if (numberString.equals("")) {
                return CashOutIdleState.class;
            } else {
                numberString = "";
                return CashOutIdleState.class;
            }
        }
        return sinkState.getClass();
    }
}

