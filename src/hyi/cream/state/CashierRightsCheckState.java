package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;
import hyi.cream.util.*;
import jpos.*;
import java.util.*;

public class CashierRightsCheckState extends State {
    private String cashierName = "";
    private String oldCashierName = "";
    private String cashierPassword = "";
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private String display ="";
    private boolean flag = false;
    private boolean SrcLock =false;
	static CashierRightsCheckState cashierRightsCheckState;
	private static String targetState = "";
	private static String sourceState = "";
	private String exitState = "";
	private boolean checked = false;

    // 0. 初始状态  1. 需要转到目标位置　2. 已经到达过目标位置 3. 已经从目标位置返回
    private int turnKeyState = 0;
    
    private boolean needTurnKeyAtOverrideAmount = false;
    private boolean needTurnKey = false;
    private int beginKeyPosition = -1;     //随便赋一个值，等需要用到的地方再赋值
    private int objectiveKeyPosition = -1; //随便赋一个值，等需要用到的地方再赋值
	
    public static CashierRightsCheckState getInstance() {
		try {
            if (cashierRightsCheckState == null) {
                cashierRightsCheckState = new CashierRightsCheckState();
            }
        } catch (InstantiationException ex) {
        }
        return cashierRightsCheckState;
    }

    public CashierRightsCheckState() throws InstantiationException {
    	cashierName = "";
        String tmp = CreamProperties.getInstance().getProperty("TurnKeyAtOverrideAmount");
        needTurnKeyAtOverrideAmount  = tmp != null && tmp.equalsIgnoreCase("yes");
    }
    
    private void initstate(){
        this.initstate1();
        turnKeyState = 0;
    }

    private void initstate1() {
        cashierName = "";
        cashierPassword = "";
        String display = "";
        flag = false;    
    }
    
    
    public void entry(EventObject event, State sourceState) {
        exitState = "";
        needTurnKey = needTurnKeyAtOverrideAmount && targetState.equals("OverrideAmountState");     
        if (needTurnKey) {
            if (turnKeyState == 0) {
                turnKeyState = 1;
                beginKeyPosition = app.getKeyPosition();
                objectiveKeyPosition = beginKeyPosition + 1; //初始位置右边一位
                
                app.getMessageIndicator().setMessage(
                        CreamToolkit.GetResource().getString("TurnKeyTo"));
                return;                    
            }
        }
        
        if (event.getSource() instanceof Keylock) {
            Keylock k = (Keylock)event.getSource();
            try {
                int kp = k.getKeyPosition();
                if (kp == objectiveKeyPosition && turnKeyState == 1) {
                    app.setKeyPosition(kp);
                    app.getMessageIndicator().setMessage(
                        CreamToolkit.GetResource().getString("TurnKeyBack"));
                    turnKeyState = 2;
                    return;                                          
                } else if (kp == beginKeyPosition && turnKeyState == 2) {
                    app.setKeyPosition(kp);
                    turnKeyState = 3;
                    //还原
                    beginKeyPosition = -1;
                    objectiveKeyPosition = -1; 
                } else {
                    return; 
                }
            } catch (JposException e) {
               e.printStackTrace();
            }
        } else {
            if (needTurnKey && turnKeyState != 0 && turnKeyState != 3)
                return;
        }
        
        if(sourceState instanceof LockingState )
        	SrcLock = true;
       	oldCashierName = CreamProperties.getInstance().getProperty("CashierNumber");
       	if (!checked) {
       		if (checkRight()) {
	       		exitState = targetState;
	            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("Enter"));
	       		return ;
       		} else {
	       		initstate1();
       		}
       	}
       	if (!display.trim().equals(""))
       		return ;
       	if (flag) {
            app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("Password"));
            app.getMessageIndicator().setMessage("");
       	} else {
		    app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputCashierNumberAgain"));
            app.getWarningIndicator().setMessage("");
		}
    }
    
    public Class exit(EventObject event, State sinkState) {
        
        if (event.getSource() instanceof Keylock
           || (needTurnKey && turnKeyState != 0 && turnKeyState != 3)) //等待转Key
            return CashierRightsCheckState.class;

        if (exitState != null && !exitState.trim().equals("")) {
        	checked = false;
            initstate();
            return returnState(targetState);
        }
        
        if (event.getSource() instanceof NumberButton){
            app.getWarningIndicator().setMessage("");
            if (flag){
                NumberButton nb = (NumberButton)event.getSource();
                cashierPassword += nb.getNumberLabel().trim();
                display = "";
                for (int i=0; i < cashierPassword.length(); i++)
                    display += '*';
                app.getMessageIndicator().setMessage(display);
            } else {
                NumberButton nb = (NumberButton)event.getSource();
                cashierName += nb.getNumberLabel().trim();
                display = cashierName;
                app.getMessageIndicator().setMessage(cashierName);
            }
            return CashierRightsCheckState.class;
        } else if (event.getSource() instanceof ClearButton) {
            if (SrcLock){
                app.getMessageIndicator().setMessage("");
                app.getWarningIndicator().setMessage("");
                checked = false;
                initstate();
                return LockingState.class;
            } else {
                app.getMessageIndicator().setMessage("");
                app.getWarningIndicator().setMessage("");
                checked = false;
                initstate();
                return returnState(sourceState);
            }
        } else if (event.getSource() instanceof EnterButton) {
            display = "";
            if (cashierName != "") {
                flag = true;
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("Password"));
                app.getMessageIndicator().setMessage("");
                if (cashierPassword != "" ) {
                    app.getMessageIndicator().setMessage("查 询 中...");
                    if ( SrcLock ){
                        Cashier cashierDAC = Cashier.CheckValidCashier(cashierName, cashierPassword);
                        if (cashierDAC == null){
                            app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WrongPassORNoLevel"));
                            initstate();
                            return CashierRightsCheckState.class;
                        } else{
                            if (checkRight()) 
                                checked = false;
                            else  {
                                initstate();
                                return CashierRightsCheckState.class;
                            }
                            POSTerminalApplication.setNewCashierID(cashierName);
                            initstate();
                            app.getWarningIndicator().setMessage("");
                            app.getMessageIndicator().setMessage("");
                            flag = true;
                            return returnState(targetState);
                        }
                    }else{
                        String ReturnableLevel = CreamProperties.getInstance().getProperty("ReturnableLevel");
                        if ((ReturnableLevel == null) || (ReturnableLevel).equalsIgnoreCase(""))
                            ReturnableLevel = "3";
                        Cashier cashierDAC = Cashier.CheckValidCashier(cashierName, cashierPassword,ReturnableLevel);
                        if (cashierDAC == null){
                            app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WrongPassORNoLevel"));
                            initstate();
                            return CashierRightsCheckState.class;
                        } else{
                            if (checkRight()) 
                                checked = false;
                            else {
                                initstate();
                                return CashierRightsCheckState.class;
                            }
                            POSTerminalApplication.setNewCashierID(cashierName);
                            initstate();
                            app.getWarningIndicator().setMessage("");
                            app.getMessageIndicator().setMessage("");
                            flag = true;
                            return returnState(targetState);
                        }
                    }
                }
            }
        }                 
                
        return CashierRightsCheckState.class;
    }
		
	public static void setSourceState(String state) {
		sourceState = state;
	}
	 
	public static Class getSourceState() {
        try {
            return Class.forName("hyi.cream.state." + sourceState);
        } catch (Exception e) {
        }
        return null;
	}
    
    public static void setTargetState(String state) {
		targetState = state;
	}

    private boolean checkRight() {
    	boolean ret  = false;
    	try {
    		//CreamToolkit.logMessage("CashierRightsCheckState | checkRight | targetState : " + targetState);
    		checked = true;
    		String name = cashierName;
    		if (cashierName == null || cashierName.equals(""))
    			name = oldCashierName;
    		Cashier cashier = Cashier.queryByCashierID(name);
    		String cashierRights = "";
    		if (cashier != null) {
	    		if (cashier.getFieldValue("CASRIGHTS") == null) 
	    			return true;
	    		else
		    		cashierRights = (String) cashier.getFieldValue("CASRIGHTS");
    		} else
    			return false;
    		
            cashierRights = cashierRights + "00000000000000000000"; //右边补20位0，防止后面出现IndexOutOfBoundException
            
            //CreamToolkit.logMessage("CashierRightsCheckState | checkRight | cashierRights : " + cashierRights);
	    	if (targetState == null || targetState.trim().equals(""))
	    		ret = true;
    		else if (targetState.equalsIgnoreCase("VoidReadyState")) { //前笔误打
	    		if (cashierRights.charAt(0) == '1')
	    			ret = true;
	    	} else if (targetState.equalsIgnoreCase("CancelState")) { //交易取消
	    		if (cashierRights.charAt(1) == '1')
	    			ret = true;
	    	} else if (targetState.equalsIgnoreCase("ReturnNumberState")) { //退货 
	    		if (cashierRights.charAt(2) == '1')
	    			ret = true;
			} else if (targetState.equalsIgnoreCase("ReturnSaleState")) { //负项销售 
				if (cashierRights.charAt(2) == '1')
					ret = true;
	    	} else if (targetState.equalsIgnoreCase("DrawerOpenState2")) {//开屉
	    		if (cashierRights.charAt(3) == '1')
	    			ret = true;
	    	} else if (targetState.equalsIgnoreCase("CashInIdleState")) {//借零
	    		if (cashierRights.charAt(4) == '1')
	    			ret = true;
	    	} else if (targetState.equalsIgnoreCase("CashOutIdleState")) {//投库
	    		if (cashierRights.charAt(5) == '1')
	    			ret = true;
			} else if (targetState.equalsIgnoreCase("OverrideAmountState")) { //变价 
				if (cashierRights.charAt(6) == '1')
					ret = true;
	    	}
    	} catch (Exception e) {
    		e.printStackTrace(CreamToolkit.getLogger());
    	}
    	return ret;
    }
    
    private Class returnState(String stateName) {
    	try {
    		return Class.forName("hyi.cream.state." + stateName);
    	} catch (Exception e) {
    	}
		return null;
    }
}