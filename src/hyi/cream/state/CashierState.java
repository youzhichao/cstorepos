package hyi.cream.state;

import java.util.*;

import jpos.*;
import jpos.events.*;

import hyi.cream.*;
import hyi.cream.util.*;

public class CashierState extends SomeAGReadyState {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();

	static CashierState cashierState = null;

    public static CashierState getInstance() {
		try {
            if (cashierState == null) {
                cashierState = new CashierState();
            }
        } catch (InstantiationException ex) {
        }
        return cashierState;
    }

    /**
     * Constructor
     */
    public CashierState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("CashierState entry");
		app.getPopupMenuPane().setEnabled(true);
        if (app.getScanCashierNumber()
            && !app.getChecked()) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("CheckWarning"));
        } else {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputCashierNumber"));
        }       
        if (app.getBeginState()) {
            //Bruce/20030324
            String keylockMessage = CreamProperties.getInstance().getProperty("postype");
            if (keylockMessage != null) {
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("TurnKeyToOff(" + keylockMessage + ")"));
            } else {
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("TurnKeyToOff(ibm)"));
            }
        }
    }

	public Class exit(EventObject event, State sinkState) {
        //System.out.println("CashierState exit");

        if (event.getSource() instanceof Keylock) {  
            Keylock k = (Keylock)event.getSource();
            try {
                int kp = k.getKeyPosition();
                app.setKeyPosition(kp);
                if (CreamToolkit.getExitClass(kp) != null) {
                    return CreamToolkit.getExitClass(kp);
                } else {
                    return CashierState.class;
                }
            } catch (JposException e) {
                System.out.println(e);
            }
            /*Keylock k = (Keylock)event.getSource();
            try {
                int kp = k.getKeyPosition();
                if (kp == 3) {
                    app.setKeyPosition(kp);
                    return KeyLock2State.class;
                } else if (kp == 2) {
                    app.setKeyPosition(kp);
                    return InitialState.class;
				} else if (kp == 6) {
					return ConfigState.class;
				}
			} catch (JposException e) {
			}*/
		}

		//if (sinkState != null)
		return sinkState.getClass();
		//else
	
    }

    public String getPromptedMessage() {  
        if (app.getScanCashierNumber()
            && !app.getChecked()) {
            return CreamToolkit.GetResource().getString("CheckWarning");
        } else {
            return CreamToolkit.GetResource().getString("InputCashierNumber");
        }
    }
}
