
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.awt.Toolkit;
import java.util.*;

/**
 * A Class class.
 * <P>
 * @author slackware
 */
public class CheckCashierPasswordState extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
	static CheckCashierPasswordState getCashierNumberState = null;
	private String pwd = "";
	private String display ="";
	private int times = 1;

	private void init () {
		pwd = "";
		display ="";
        //times = 0;
	}

    public static CheckCashierPasswordState getInstance() {
        try {
			if (getCashierNumberState == null) {
                getCashierNumberState = new CheckCashierPasswordState();
            }
        } catch (InstantiationException ex) {
        }
        return getCashierNumberState;
	}

	/**
     * Constructor
     */
	public CheckCashierPasswordState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
		//System.out.println("ShiftState2 Enter");
		app.getMessageIndicator().setMessage(display);
		if (sourceState instanceof GetCashierNumberState) {
			app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("Password"));
			return;
		} else if (sourceState instanceof CheckCashierPasswordState) {
			if (event.getSource() instanceof ClearButton || event.getSource() instanceof EnterButton)
				app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WrongPassword"));
		}
    }

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof EnterButton) {
			app.getMessageIndicator().setMessage(res.getString("LookingAt"));
			Cashier cashierDAC = Cashier.CheckValidCashier(app.getCurrentCashierNumber(), pwd);
			init();
			if (cashierDAC == null) {
				Toolkit.getDefaultToolkit().beep();
				if (times >= 3) {
					times = 1;
					app.setCurrentCashierNumber("");
					return InitialState.class;
				} else {
					times ++;
					return CheckCashierPasswordState.class;
				}
			} else {
			    /*
			       Otherwise set CashierNumber in
			       CreamProperties, create/insert a new ShiftReport, meanwhile, check
			       to see if the current ZReport's SystemDataTime is '1970/01/01 00:00:00',
			       if it is not, create/insert a new ZReport and CategorySales. Finally,
			       clear messageIndicator and return IdleState.class.
			    */
                synchronized (POSTerminalApplication.waitForTransactionStored) {    // -> see DrawerOpenState.java
    				CreamProperties.getInstance().put("CashierNumber", app.getCurrentCashierNumber());
    				CreamProperties.getInstance().deposit();
    				Date initDate = CreamToolkit.getInitialDate();
    				//System.out.println(shift);
    				ZReport z = ZReport.getOrCreateCurrentZReport();
    				//Iterator itr = CategorySales.getCurrentCategorySales();
    				if (z.getAccountingDate().compareTo(initDate) != 0) {
    				   z = ZReport.createZReport();
    				}
    			 	ShiftReport shift = ShiftReport.createShiftReport();
    				app.getCurrentTransaction().clear();
    				app.getCurrentTransaction().init();
    				app.getCurrentTransaction().setDealType2("8");
    				CreamToolkit.logMessage("store() in CheckCashierpasswordState exit()");
    				app.getCurrentTransaction().store();
    				//DacTransfer.getInstance().uploadTransaction(app.getCurrentTransaction());
                    
                    //Bruce/20031121
    				//int nextInvoicNumber = Integer.parseInt(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
    				//int nextTranNumber = Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber"));
    				//CreamProperties.getInstance().setProperty("NextInvoiceNumber", Integer.toString(++nextInvoicNumber));
    				//CreamProperties.getInstance().setProperty("NextTransactionSequenceNumber", Integer.toString(++nextTranNumber));
    				//CreamProperties.getInstance().deposit();

    				String logMsg = CreamToolkit.GetResource().getString("CashierloginI") + " " + app.getCurrentCashierNumber() + " " + CreamToolkit.GetResource().getString("CashierloginII");
    				app.getMessageIndicator().setMessage(logMsg);
    				app.getWarningIndicator().setMessage("");
                }
				return IdleState.class;
			}
		} else if (event.getSource() instanceof NumberButton) {
			NumberButton nb = (NumberButton)event.getSource();
			pwd += nb.getNumberLabel().trim();
			for (int i=0; i < nb.getNumberLabel().length(); i++)
    			display += "*";
			return CheckCashierPasswordState.class;
		} else if (event.getSource() instanceof ClearButton) {
			 if (pwd.equals("")) {
				init();
               	times = 1;
               	app.setCurrentCashierNumber("");
				return InitialState.class;
			 } else {
				init();
				return CheckCashierPasswordState.class;
			 }
		} else
			return CheckCashierPasswordState.class;
	}
}

