package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;
import hyi.cream.util.*;
import java.util.*;
/**
 * Title: checkCashier State
 * Company:null
 * @author Wissly
 * @version 1.0
 */

public class CheckCashierState extends State{
    private String CashierName = "";
    private String CashierPassword = "";
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private String display ="";
    private boolean flag = false;
    private boolean SrcLock =false;
    public CheckCashierState() {

    }
    private void initstate(){
        CashierName = "";
        CashierPassword = "";
        String display = "";
        flag = false;
    }
    public void entry(EventObject event, State sourceState) {
   // System.out.println("Entering CheckCashier State ... ");
      if(sourceState instanceof LockingState )
      SrcLock = true;
        if (CashierName == "")
          app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputCashierNumber"));
         if (event.getSource() instanceof NumberButton){
               app.getWarningIndicator().setMessage("");
               if (flag){
                    NumberButton nb = (NumberButton)event.getSource();
                    CashierPassword += nb.getNumberLabel().trim();
                    display = "";
                    for (int i=0; i < CashierPassword.length(); i++)
                    display += '*';
                    app.getMessageIndicator().setMessage(display);
                }else{
                    NumberButton nb = (NumberButton)event.getSource();
                    CashierName += nb.getNumberLabel().trim();
                    app.getMessageIndicator().setMessage(CashierName);
                }
            }
    }
    public Class exit(EventObject event, State sinkState) {
    //System.out.println("Exit CheckCashierState ... ");
          if (event.getSource() instanceof ClearButton) {
                 if (SrcLock){
                    app.getMessageIndicator().setMessage("");
                    app.getWarningIndicator().setMessage("");
                    initstate();
                    return LockingState.class;
                 }else{
                    app.getMessageIndicator().setMessage("");
                    app.getWarningIndicator().setMessage("");
                    initstate();
                    return KeyLock1State.class;
                 }
           }

         if (event.getSource() instanceof EnterButton) {
              if (CashierName != "") {
                   app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("Password"));
                   app.getMessageIndicator().setMessage("");
                   flag = true;
                   if (CashierPassword != "" ) {
                        app.getMessageIndicator().setMessage("查 询 中...");
                        if ( SrcLock ){
                                Cashier cashierDAC = Cashier.CheckValidCashier(CashierName,CashierPassword);
                                if (cashierDAC == null){
                                    app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WrongPassORNoLevel"));
                                    initstate();
                                    return CheckCashierState.class;
                                } else{
                                    initstate();
                                    app.getWarningIndicator().setMessage("");
                                    app.getMessageIndicator().setMessage("");
                                    return IdleState.class;
                                }
                             }else{
                                   String ReturnableLevel = CreamProperties.getInstance().getProperty("ReturnableLevel");
                                   if ((ReturnableLevel == null) || (ReturnableLevel).equalsIgnoreCase(""))
                                      ReturnableLevel = "3";
                                    Cashier cashierDAC = Cashier.CheckValidCashier(CashierName,CashierPassword,ReturnableLevel);
                                    if (cashierDAC == null){
                                        app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WrongPassORNoLevel"));
                                        initstate();
                                        return CheckCashierState.class;
                                    } else{
                                        initstate();
                                        app.getWarningIndicator().setMessage("");
                                        app.getMessageIndicator().setMessage("");
                                        return ReturnNumberState.class;
                                    }
                               }

                   } else{
                        return CheckCashierState.class;
                    }
                }else{
                     return CheckCashierState.class;
                }
            }
                     return CheckCashierState.class;
         }
}

