// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   ConfigState.java

package hyi.cream.state;

import hyi.cream.POSButtonHome;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Trigger;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import java.awt.Component;
import java.awt.Dialog;
import java.io.*;
import java.util.*;
import jpos.JposException;
import jpos.Keylock;

// Referenced classes of package hyi.cream.state:
//            State

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class ConfigState extends State implements PopupMenuListener {

    private POSTerminalApplication app;
    private PopupMenuPane p;
    private static ArrayList menu;
    private static ArrayList states;
    private static ArrayList properties;
    private int selectItem;
    static ConfigState configState = null;

    public static ConfigState getInstance() {
        try {
            if (configState == null)
                configState = new ConfigState();
        } catch (InstantiationException instantiationexception) {
        }
        return configState;
    }

    /**
     * Constructor
     */
    public ConfigState() throws InstantiationException {
        app = POSTerminalApplication.getInstance();
        p = app.getPopupMenuPane();
        selectItem = 0;
    }

    public void entry(EventObject event, State sourceState) {
        app.setKeyPosition(6);
        p.setMenu(menu);
        p.setVisible(true);
        p.setVisible(true);
        p.addToList(this);
        p.clear();
        app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSelect"));
    }

    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");
        if (event.getSource() instanceof SelectButton) {
            String stateName = (String) states.get(selectItem);
            if (stateName.equalsIgnoreCase("halt")) {
                Runtime currentApp = Runtime.getRuntime();
                try {
                    Process c = currentApp.exec("halt");
                    //c.waitFor();
                    //result = c.exitValue();
                } catch (Exception e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                    System.out.println(e);
                }
            } else {
                if (stateName.equalsIgnoreCase("recoverDBFromBackUp"))
                    try {
                        boolean windows = System.getProperties().get("os.name").toString().indexOf("Windows") != -1;
                        if (!windows)
                            Trigger.getInstance().recoverFromBackup();
                        return hyi.cream.state.ConfigState.class;
                    } catch (Exception e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                        System.out.println(e);
                        return hyi.cream.state.ConfigState.class;
                    }
                try {
                    Class c = Class.forName("hyi.cream.state." + stateName);
                    return c;
                } catch (ClassNotFoundException e) {
                    System.out.println(e);
                }
                return hyi.cream.state.ConfigState.class;
            }
        }
        if (event.getSource() instanceof ClearButton)
            //app.getPopupMenuPane().setVisible(false);
            return hyi.cream.state.ConfigState.class;
        if (event.getSource() instanceof Keylock) {
            Keylock k = (Keylock) event.getSource();
            try {
                int kp = k.getKeyPosition();
                app.setKeyPosition(kp);
                Class exitState = CreamToolkit.getExitClass(kp);
                if (exitState != null) {
                    if (exitState.equals(hyi.cream.state.InitialState.class))
                        p.setVisible(false);
                    return exitState;
                } else {
                    return hyi.cream.state.ConfigState.class;
                }
            } catch (JposException e) {
                System.out.println(e);
            }
            /*Keylock k = (Keylock)event.getSource();
            try {
                int kp = k.getKeyPosition();
                if (kp == 2) {
                    app.setKeyPosition(kp);
                    app.getPopupMenuPane().setVisible(false);
                    return InitialState.class;
                } else if (kp == 6) {
                    app.setKeyPosition(kp);
                    return ConfigState.class;
                }
            } catch (JposException e) {
            }*/
            
        }
        return sinkState != null ? sinkState.getClass() : null;
    }

    public void doSomething() {
        if (p.getSelectedMode()) {
            selectItem = p.getSelectedNumber();
            POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        }
    }

    public String getProperty() {
        return (String) properties.get(selectItem);
    }

    static {
        menu = new ArrayList();
        states = new ArrayList();
        properties = new ArrayList();
        File propFile = CreamToolkit.getConfigurationFile(hyi.cream.state.ConfigState.class);
        try {
            FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = null;
            if (CreamProperties.getInstance().getProperty("Locale").equals("zh_TW"))
                inst = new InputStreamReader(filein, "BIG5");
            else if (CreamProperties.getInstance().getProperty("Locale").equals("zh_CN"))
                inst = new InputStreamReader(filein, "GBK");
            BufferedReader in = new BufferedReader(inst);
            String line = null;
            char ch;
            do {
                for (line = in.readLine(); line.equals(""); line = in.readLine())
                    ;
                int i = 0;
                do {
                    ch = line.charAt(i);
                    i++;
                } while ((ch == ' ' || ch == '\t') && i < line.length());
            } while (ch == '#' || ch == ' ' || ch == '\t');
            StringTokenizer t = null;
            for (; line != null; line = in.readLine()) {
                t = new StringTokenizer(line, ",");
                menu.add(t.nextToken());
                if (t.hasMoreElements())
                    states.add(t.nextToken());
                else
                    states.add("");
                if (t.hasMoreElements())
                    properties.add(t.nextToken());
                else
                    properties.add("");
            }

        } catch (FileNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("File is not found: " + propFile.toString());
        } catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("IO exception at " + hyi.cream.state.ConfigState.class);
        }
    }
}
