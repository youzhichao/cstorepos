// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.ZReport;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.util.CreamToolkit;

import java.util.Date;
import java.util.EventObject;

/**
 * 日結選擇菜單State.<P/>
 * Entering by specifying at keylock*.conf file..
 * 
 * @author dai, Bruce You
 */
public class ConfirmZState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
	private static ConfirmZState zState;
    //记录从哪个state进来的
    private static Class sourceState;
    private Class exitState;
    
    public ConfirmZState() throws InstantiationException {
    }

	public static ConfirmZState getInstance() {
        try {
            if (zState == null) {
				zState = new ConfirmZState();
            }
        } catch (InstantiationException ex) {
        }
        return zState;
    }

    public void entry(EventObject event, State sourceState) {
        ConfirmZState.sourceState = sourceState.getClass();
         
        Date initDate = CreamToolkit.getInitialDate();
		ZReport z = ZReport.getCurrentZReport();
		if (z == null || z.getAccountingDate().compareTo(initDate) != 0) {
			app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("ZReady"));
            exitState = ConfirmZState.sourceState; 
		} else {
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("ZConfirm"));
            exitState = ZState.class; 
		}
	}

	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof ClearButton) 
            return ConfirmZState.sourceState;
        else  {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("z"));
            return exitState;       
        }
	}

    public static Class getSourceState() {
        return sourceState;
    }

    public static void setSourceState(Class sourceState) {
        ConfirmZState.sourceState = sourceState;
    }
}
