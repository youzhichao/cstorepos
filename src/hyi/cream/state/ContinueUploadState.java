/**
 * State class
 * @since 2000
 * @author slackware
 */

package hyi.cream.state;

//for event processing
import java.util.*;
import java.io.*;
import hyi.cream.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.inline.*;

/**
 * ContinueUploadState state class.
 *
 * @author dai, Bruce
 * @version 1.7
 */
public class ContinueUploadState extends State {
    /**
     * /Bruce/1.7/2002-04-27/
     *    增加代收明细帐存盘.
     */
    transient public static final String VERSION = "1.7";
    
    //private static final String daiShouVersion =
    //    CreamProperties.getInstance().getProperty("DaiShouVersion") == null?
    //    "" : CreamProperties.getInstance().getProperty("DaiShouVersion"); 

    static ContinueUploadState paying2State = null;
    private POSTerminalApplication posTerminal      = POSTerminalApplication.getInstance();
    private ResourceBundle res                      = CreamToolkit.GetResource();
    boolean connected = false;

    public static ContinueUploadState getInstance() {
        try {
            if (paying2State == null) {
                paying2State = new ContinueUploadState();
            }
        } catch (InstantiationException ex) {
        }
        return paying2State;
    }

    /**
     * Constructor
     */
    public ContinueUploadState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        //System.out.println("This is ContinueUploadState's Entry!");
        //KeylockWarningState	Keylock
        //connected = DacTransfer.getInstance().isConnected(100);
        connected = Client.getInstance().isConnected();
        if (!connected) {
            boolean available = save();
            if (available) {
                if (StateMachine.getFloppySaveError())
                    posTerminal.getMessageIndicator().setMessage(
                        CreamToolkit.GetResource().getString("SaveFloppyErrorInsertAgain"));
                else
                    posTerminal.getMessageIndicator().setMessage(
                        CreamToolkit.GetResource().getString("SaveFloppyReady"));

            } else {
                posTerminal.getWarningIndicator().setMessage(res.getString("NoDataForUploading"));
                POSButtonHome.getInstance().buttonPressed(new POSButtonEvent(new ClearButton(0,0,0,"")));
            }
        } else
            posTerminal.getMessageIndicator().setMessage(res.getString("ContinueUpload"));

    }
	// modify by lxf 2003.02.08  (new flag FloppyContent)
    private boolean save() {
        boolean available = false;
        Iterator itr = ShiftReport.getUploadFailedList();
        //  only use when property "FloppyContent" is "full"
        boolean useSaveFull;
        String useSvaeStr = CreamProperties.getInstance().getProperty("FloppyContent");
        useSaveFull = useSvaeStr != null && useSvaeStr.equalsIgnoreCase("full");
        if (itr != null) {
            if (itr.hasNext()) {
                available = true;
                ShiftReport shift = (ShiftReport)itr.next();
                if (useSaveFull)
	                LineOffTransfer.getInstance().saveToDisk2(shift);
                else
	                LineOffTransfer.getInstance().saveToDisk(shift);
            }
        }
        itr = ZReport.getUploadFailedList();
        if (itr != null) {
            if (itr.hasNext()) {
                available = true;
                ZReport z = (ZReport)itr.next();
                if (useSaveFull)
	                LineOffTransfer.getInstance().saveToDisk2(z);
                else
	                LineOffTransfer.getInstance().saveToDisk(z);
            }
        }


		//lxf by 2003.02.18
        CreamToolkit.logMessage("Save depsales to floppy disk");
        ArrayList objArray = new ArrayList();
        try {
            Iterator it = DepSales.getUploadFailedList();
            if (it != null) {
    	        for (; it.hasNext();) {
    				objArray.add(it.next());            	
    	        }
            }
			if (!objArray.isEmpty()) {
	            if (useSaveFull)
		            LineOffTransfer.saveToDisk2(objArray.toArray());
	            else
		            LineOffTransfer.getInstance().saveToDisk(objArray.toArray());
			}
        } catch (Exception e) {
        	e.printStackTrace(CreamToolkit.getLogger());
        }

        CreamToolkit.logMessage("Save daishousales to floppy disk");
        objArray.clear();
        try {
            Iterator it = DaishouSales.getUploadFailedList();
            if (it != null) {
                for (; it.hasNext();) {
				    objArray.add(it.next());            	
	            }
            }
            
			if (!objArray.isEmpty()) {
	            if (useSaveFull)
		            LineOffTransfer.saveToDisk2(objArray.toArray());
	            else
		            LineOffTransfer.getInstance().saveToDisk(objArray.toArray());
			}
        } catch (Exception e) {
        	e.printStackTrace(CreamToolkit.getLogger());
        }

        CreamToolkit.logMessage("Save daishousales2 to floppy disk");
        objArray.clear();
        try {
            for (Iterator it = DaiShouSales2.getUploadFailedList(); it.hasNext();) {
                objArray.add(it.next());                
            }                
            
            if (!objArray.isEmpty()) {
                if (useSaveFull)
                    LineOffTransfer.saveToDisk2(objArray.toArray());
                else
                    LineOffTransfer.getInstance().saveToDisk(objArray.toArray());
            }
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
		
		/*
        //Bruce/2002-1-28  lxf/2003.02.08
        itr = DepSales.getUploadFailedList();
        if (itr != null && itr.hasNext()) {
            available = true;
            if (useSaveFull)
                DacTransfer.getInstance().saveToDisk2(DepSales.cloneForSC(itr));
            else
                DacTransfer.getInstance().saveToDisk(DepSales.cloneForSC(itr));
        }

        //Bruce/2002-04-27 lxf/2003.02.08
        itr = DaishouSales.getUploadFailedList();
        if (itr != null && itr.hasNext()) {
            available = true;
            if (useSaveFull)
                DacTransfer.getInstance().saveToDisk2(DaishouSales.cloneForSC(itr));
            else
                DacTransfer.getInstance().saveToDisk(DaishouSales.cloneForSC(itr));
        }
		*/
		
        return available;
    }

    private boolean update() {
        boolean available = false;
        Iterator itr = ShiftReport.getUploadFailedList();
        if (itr != null) {
            if (itr.hasNext()) {
                available = true;
                ShiftReport shift = (ShiftReport)itr.next();
                shift.setUploadState("1");
                shift.update();
            }
        }
        itr = ZReport.getUploadFailedList();
        if (itr != null) {
            if (itr.hasNext()) {
                available = true;
                ZReport z = (ZReport)itr.next();
                z.setUploadState("1");
                z.update();
            }
        }

        //Bruce/2002-1-28
        itr = DepSales.getUploadFailedList();
        if (itr != null) {
            while (itr.hasNext()) {
                available = true;
                DepSales d = (DepSales)itr.next();
                d.setUploadState("1");
                d.update();
            }
        }
        return available;
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("This is ContinueUploadState's Exit!");
        if (!(event.getSource() instanceof EnterButton))
            return ConfigState.class;
        //connected = DacTransfer.getInstance().isConnected(100);
        connected = Client.getInstance().isConnected();
        if (connected) {
            boolean ss = false;
            boolean zs = false;
            boolean zd = false;
            Iterator itr = ShiftReport.getUploadFailedList();
            if (itr != null) {
                while (itr.hasNext()) {
                    ShiftReport shift = (ShiftReport)itr.next();
                    posTerminal.getMessageIndicator().setMessage(
                        res.getString("UploadingShift") + shift.getSequenceNumber());
                    //boolean ok = DacTransfer.getInstance().uploadShiftReport(shift);
                    boolean ok = false;
                    try {
                        Client.getInstance().processCommand("putShift " +
                            shift.getZSequenceNumber() + " " +
                            shift.getSequenceNumber());
                        ok = true;
                    } catch (ClientCommandException e) {
                        ok = false;
                    }
                    if (ok) {
                        shift.setUploadState("1");
                        shift.update();
                        ss = true;
                    }
                }
            }
            itr = ZReport.getUploadFailedList();
            if (itr != null) {
                while (itr.hasNext()) {
                    ZReport z = (ZReport)itr.next();
                    posTerminal.getMessageIndicator().setMessage(res.getString("UploadingZ") + z.getSequenceNumber());
                    //boolean ok = DacTransfer.getInstance().uploadZReport(z);
                    boolean ok = false;
                    try {
                        Client.getInstance().processCommand("putZ " +
                            z.getSequenceNumber());
                        ok = true;
                    } catch (ClientCommandException e) {
                        ok = false;
                    }
                    if (ok) {
                        z.setUploadState("1");
                        z.update();
                        zs = true;
                    }
                }
            }

            //Bruce/2002-1-28
            itr = DepSales.getUploadFailedList();
            ArrayList a = new ArrayList();
            Set seqSet = new java.util.HashSet();
            if (itr != null) {
                while (itr.hasNext()) {
                    DepSales ds = (DepSales)itr.next();
                    seqSet.add("" + ds.getSequenceNumber());
                    a.add(ds);
                }
                //boolean ok = DacTransfer.getInstance().uploadDepSales(a.toArray());
                boolean ok = true;
                Iterator iter = seqSet.iterator();
                while (iter.hasNext()) {
                    String zSeq = (String)iter.next();
                    try {
                        Client.getInstance().processCommand("putDepSales " + zSeq);
                    } catch (ClientCommandException e) {
                        ok = false;
                    }
                }
                if (ok) {
                    itr = a.iterator();
                    while (itr.hasNext()) {
                        DepSales d = (DepSales)itr.next();
                        d.setUploadState("1");
                        d.update();
                        zd = true;
                    }
                }
            }

            if (zs || ss || zd)
                posTerminal.getWarningIndicator().setMessage(res.getString("Shift_Z_Uploaded"));
            return ConfigState.class;
        } else {
            //boolean saveToDisk = false;
            File serilizedDir = new File(DacTransfer.path);
            if (!serilizedDir.exists())
                serilizedDir.mkdir();
            File [] all = serilizedDir.listFiles();
            if (all.length == 0)
                save();
            boolean success = LineOffTransfer.saveToFloppyDisk(true);
            StateMachine.setFloppySaveError(!success);
            if (success) {
               update();
               return ConfigState.class;
            } else
                return ContinueUploadState.class;
        }

    }	//KeylockWarningState  	Keylock

}


