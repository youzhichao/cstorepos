package hyi.cream.state;

import java.util.*;

public class CustomState extends SomeAGReadyState {

	static CustomState customState = null;
	private State source;// = null;

    public static CustomState getInstance() {
        try {
			if (customState == null) {
                customState = new CustomState();
            }
		} catch (InstantiationException ex) {
		   
        }
        return customState;
    }

    /**
     * Constructor
     */
	public CustomState() throws InstantiationException {
		if (customState != null)
			throw new InstantiationException (this.toString());
        else {
			customState = this;
		}
    }

    public String getPromptedMessage() {
        return "Please input customs' amount:";
	}

	public void entry(EventObject event, State sourceState) {
		System.out.println(sourceState);
		CustomState.getInstance().source = sourceState;
		System.out.println(source);
		super .entry(event, sourceState);	
	}
	
	State getSourceState() {
        return source;
	}

}
