// Copyright (c) 2000 HYI
package hyi.cream.state;

import java.util.*;
import java.math.*;
import java.text.*;
import java.awt.*;
import jpos.*;

import hyi.cream.state.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.*;
import hyi.cream.event.*;
import hyi.cream.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class DaiFuIdleState extends State implements PopupMenuListener {
    private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private Transaction trans             = app.getCurrentTransaction();
	private ResourceBundle res            = CreamToolkit.GetResource(); 
    private PopupMenuPane p               = app.getPopupMenuPane();
    private ArrayList reasonArray         = new ArrayList();
    private String pluNo                  = "";
    private DaiFuButton daiFuButton   = null;
    
    static DaiFuIdleState daiFuIdleState      = null;

    public static DaiFuIdleState getInstance() {
        try {
            if (daiFuIdleState == null) {
                daiFuIdleState = new DaiFuIdleState();
            }
        } catch (InstantiationException ex) {
        }
        return daiFuIdleState;
    }

    /**
     * Constructor
     */
    public DaiFuIdleState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
        //System.out.println("DaiFuIdleState entry");

        if (event != null && event.getSource() instanceof DaiFuButton) {
            app.getSystemInfo().setIsDaiFu(true);
            daiFuButton = ((DaiFuButton)event.getSource());
            ArrayList menu = new ArrayList();
            Iterator daiFuItem = Reason.queryByreasonCategory("09");
            Reason r = null;
            String menuString = "";
            PLU plu = null;
            int i = 1;
            while (daiFuItem != null && daiFuItem.hasNext()) {
                r = (Reason)daiFuItem.next();
                reasonArray.add(r);
                menu.add(i + "." + r.getreasonName());
                i++;
            }

            p.setMenu(menu);
            p.setVisible(true);
            p.addToList(this);
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSelect"));
        } else {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("DaifuMessage"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("DaiFuIdleState exit");

        app.getMessageIndicator().setMessage("");
        app.getWarningIndicator().setMessage("");

        //  check enter button
        if (event.getSource() instanceof AgeLevelButton) {
            int agelevel = ((AgeLevelButton)event.getSource()).getAgeID();
            trans.setCustomerAgeLevel(agelevel);

            if ( !CreamPrinter.getInstance().getHeaderPrinted() ) {
                CreamPrinter.getInstance().printHeader();
                CreamPrinter.getInstance().setHeaderPrinted(true);
            }

            Object[] lineItemArray = trans.getLineItemsArrayLast();
            LineItem curLineItem = (LineItem)lineItemArray[lineItemArray.length-1];
            CreamPrinter.getInstance().printLineItem(curLineItem);
            
            return SummaryState.class;
        }

        //  check cancel button
        if (event.getSource() instanceof CancelButton) {
            return CancelState.class;
        }

        //  check select button
        if (event.getSource() instanceof SelectButton) {
            return DaiFuReadyState.class;
        }

        //  check clear button
        if (event.getSource() instanceof ClearButton) {
            if (trans.getCurrentLineItem() == null) {

                trans.setDealType2("0");
                return IdleState.class;
            } else {
                return DaiFuIdleState.class;
            }
        }                           

        //  check keylock
        if (event.getSource() instanceof Keylock) {  
            Keylock k = (Keylock)event.getSource();
            try {
                int kp = k.getKeyPosition();
                app.setKeyPosition(kp);
                if (CreamToolkit.getExitClass(kp) != null) {
                    return CreamToolkit.getExitClass(kp);
                } else {
                    return DaiFuIdleState.class;
                }
            } catch (JposException e) {
                System.out.println(e);
            }
            /*Keylock k = (Keylock)event.getSource();
            try {
                int kp = k.getKeyPosition();
                int ckp = app.getKeyPosition();
                if (kp == 3) {
                    app.setKeyPosition(kp);
                    return KeyLock2State.class;
                } else if (kp == 6) {
                    app.setKeyPosition(kp);
                    return ConfigState.class;
                } else if (kp == 2) {
                    app.setKeyPosition(kp);
                    return DaiFuIdleState.class;
                } else if (kp == 4) {
                    app.setKeyPosition(kp);
                    return KeyLock1State.class;
                }
            } catch (JposException e) {
            }*/
        }

        return sinkState.getClass();
    }

    public String getPluNo() {
        return pluNo;
    }

    public void doSomething() {
        if (p.getSelectedMode()) {
            int index = p.getSelectedNumber();
            pluNo = ((Reason)reasonArray.get(index)).getreasonNumber();
            POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        }
    }
}

