/**
 * State class
 * @since 2000
 * @author slackware
 */
 

package hyi.cream.state;

//for event processing
import java.util.*;
import hyi.cream.*;
import jpos.*;

//for transaction
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;

//for print
import hyi.cream.util.*;

//for maths calculation
import java.math.*;

public class DaiFuPrintState extends State {
    static DaiFuPrintState daiFuPrintState = null;

    public static DaiFuPrintState getInstance() {
        try {
            if (daiFuPrintState == null) {
                daiFuPrintState = new DaiFuPrintState();
            }
        } catch (InstantiationException ex) {
        }
        return daiFuPrintState;
    }

    /**
     * Constructor
     */
    public DaiFuPrintState() throws InstantiationException {
    }

	public void entry (EventObject event, State sourceState) {
        //System.out.println("DaiFuPrintState entry!");
        
		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        if (posTerminal.getCurrentTransaction().getDealType2().equals("4")) {
            return;
        }

		if (sourceState instanceof DaiFuOpenPriceState) {
			Transaction cTransaction = posTerminal.getCurrentTransaction();
			Object[] lineItemArrayLast = cTransaction.getLineItemsArrayLast();
			if (lineItemArrayLast.length > 1) {
				if (!CreamPrinter.getInstance().getHeaderPrinted() ) {
					CreamPrinter.getInstance().printHeader();
					CreamPrinter.getInstance().setHeaderPrinted(true);
			    }
				LineItem lineItem = (LineItem)lineItemArrayLast[lineItemArrayLast.length-1];
                if (lineItem.getDetailCode().equals("V"))
					CreamPrinter.getInstance().printLineItem(lineItem);
				lineItem = (LineItem)lineItemArrayLast[lineItemArrayLast.length-2];
				CreamPrinter.getInstance().printLineItem(lineItem);
			}
			if (!cTransaction.getBuyerNumber().equals("")
				&& !posTerminal.getBuyerNumberPrinted()) {
				posTerminal.setBuyerNumberPrinted(true);
			}
		}
        CreamToolkit.showText(posTerminal.getCurrentTransaction(), 0);
	}

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("DaiFuPrintState exit!");

        if (sinkState != null)
            return sinkState.getClass();
        else
            return null;
	}  
}

