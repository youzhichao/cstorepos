/*
 * Created on 2003-6-20
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.state;

import java.util.EventObject;

import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;

import java.math.BigDecimal;
import java.util.*;
import java.awt.event.*;
import jpos.*;

/**
 * @author Administrator
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DaiShou2CheckState extends State implements PopupMenuListener {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private static DaiShou2CheckState daiShou2CheckState = null;
	private PopupMenuPane p = app.getPopupMenuPane();
	private ResourceBundle res = CreamToolkit.GetResource();
	private ArrayList matchedDaiShouDefs = new ArrayList();
	private DaiShouDef selectedDaiShouDef;
	private String barcode = "";
	private int selectItem = 0;

	public DaiShou2CheckState() throws InstantiationException {

	}

	public static DaiShou2CheckState getInstance() {
		if (daiShou2CheckState == null) {
			try {
				daiShou2CheckState = new DaiShou2CheckState();
			} catch (InstantiationException e) {
			}
		}
		return daiShou2CheckState;
	}

	/* (non-Javadoc)
	 * @see hyi.cream.state.State#entry(java.util.EventObject, hyi.cream.state.State)
	 */
	public void entry(EventObject event, State sourceState) {
		Object eventSource = event.getSource();
		BigDecimal amount;
		if (sourceState instanceof DaiShou2State) {
			ArrayList menu = new ArrayList();
			matchedDaiShouDefs = ((DaiShou2State) sourceState).getMatchDefs();
			barcode = ((DaiShou2State) sourceState).getBarcode();
			Iterator itor = ((ArrayList)matchedDaiShouDefs.clone()).iterator();
			String defName;
			DaiShouDef dsf;
			int i = 1;
			while (itor.hasNext()) {
				dsf = (DaiShouDef) itor.next();
				amount = dsf.getAmount(barcode);
				if (amount.intValue() != -1) {
					defName = dsf.getName();
					menu.add(i++ +"." + defName + "  " + amount);
				} else {
					matchedDaiShouDefs.remove(dsf);
				}
			}

			p.setMenu(menu);
			p.setVisible(true);
			p.addToList(this);
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSelect"));

		}

	}

	/* (non-Javadoc)
	 * @see hyi.cream.state.State#exit(java.util.EventObject, hyi.cream.state.State)
	 */
	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof SelectButton) {
			selectedDaiShouDef = (DaiShouDef)matchedDaiShouDefs.get(selectItem);
			app.getMessageIndicator().setMessage("");
		//	p.setInputEnabled(false);
        }
        
		if (sinkState != null) {
			return sinkState.getClass();
		} else {
			return null;
		}
	}

	public void doSomething() {
		if (p.getSelectedMode()) {
			selectItem = p.getSelectedNumber();
			POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
			POSButtonHome.getInstance().buttonPressed(e);
		} else {
			POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
			POSButtonHome.getInstance().buttonPressed(e);
		
		}
	}
	
	public DaiShouDef getSelectedDaiShouDef() {
	    return selectedDaiShouDef;
	}

    public String getBarcode() {
        return barcode;
    }
}
