/*
 * Created on 2003-6-20
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.TooManyLineItemsException;
import hyi.cream.dac.DaiShouDef;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamToolkit;

import java.awt.Toolkit;
import java.math.BigDecimal;
import java.util.EventObject;
import java.util.ResourceBundle;

/**
 * @author Administrator
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DaiShou2ReadyState extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private Transaction curTran = app.getCurrentTransaction();
	private ResourceBundle res = CreamToolkit.GetResource();
	private LineItem lineItem = null;
	private static DaiShou2ReadyState daiShou2ReadyState = null;
	private DaiShouDef selectedDaiShoudef;
	private String barcode = "";

	public DaiShou2ReadyState() throws InstantiationException {

	}

	public DaiShou2ReadyState getInstance() {
		if (daiShou2ReadyState == null) {
			try {
				daiShou2ReadyState = new DaiShou2ReadyState();
			} catch (InstantiationException e) {
			}
		}
		return daiShou2ReadyState;
	}

	/* 
	 * @see hyi.cream.state.State#entry(java.util.EventObject, hyi.cream.state.State)
	 */
	public void entry(EventObject event, State sourceState) {
		
			if (sourceState instanceof DaiShou2State) {
				DaiShou2State dss = (DaiShou2State) sourceState;
				selectedDaiShoudef = (DaiShouDef) dss.getMatchDefs().get(0);
				barcode = dss.getBarcode();
			} else if (sourceState instanceof DaiShou2CheckState && event.getSource() instanceof SelectButton) {
				DaiShou2CheckState dscs = (DaiShou2CheckState) sourceState;
				selectedDaiShoudef = (DaiShouDef) dscs.getSelectedDaiShouDef();
				barcode = dscs.getBarcode();
			} else {
				return;
			}
			BigDecimal amount = selectedDaiShoudef.getAmount(barcode);
		
		try {	
			lineItem = new LineItem();
			lineItem.setDescription(selectedDaiShoudef.getName());
			lineItem.setDetailCode("O"); //代收公共事业费
//			lineItem.setPluNumber(
//				selectedDaiShoudef.getItemNumber() != null && selectedDaiShoudef.getItemNumber().length() > 0
//					? selectedDaiShoudef.getItemNumber()
//					: selectedDaiShoudef.getID());
            lineItem.setPluNumber(selectedDaiShoudef.getID());
//            lineItem.setDiscountNumber(selectedDaiShoudef.getID());
            lineItem.setDiscountNumber(barcode);
            lineItem.setDaiShouID(selectedDaiShoudef.getID());
			lineItem.setQuantity(new BigDecimal(1));

			//lineItem.setTaxType("0");
			lineItem.setTerminalNumber(curTran.getTerminalNumber());
			lineItem.setTransactionNumber(new Integer(Transaction.getNextTransactionNumber()));
			lineItem.setRemoved(false);
            lineItem.setUnitPrice(amount);
			lineItem.setOriginalPrice(amount);
			lineItem.setAmount(amount);
            lineItem.setAfterDiscountAmount(amount);
            lineItem.setDaiShouBarcode(barcode);
            lineItem.setPrinted(false);

			try {
				curTran.addLineItem(lineItem);
				POSTerminalApplication.getInstance().setTransactionEnd(false);
		
			} catch (TooManyLineItemsException e) {
				CreamToolkit.logMessage(e.toString());
				CreamToolkit.logMessage("Too many LineItem exception at " + this);
				POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyLineItems"));
			}

		} catch (InstantiationException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("Instantiation LineItem exception at " + this);
		}
	}

	/* 
	 * @see hyi.cream.state.State#exit(java.util.EventObject, hyi.cream.state.State)
	 */
	public Class exit(EventObject event, State sinkState) {
		Toolkit.getDefaultToolkit().beep();
        return PrintPluState.class;
	}

}
