/*
 * Created on 2003-6-20
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.state;

import java.util.*;
import jpos.*;
import jpos.Scanner;

import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;

/**
 * @author Administrator
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DaiShou2State extends State {

	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private static DaiShou2State daiShou2State = null;
	private String daiShouBarcode = "";
	private ArrayList daiShouDefs = new ArrayList();

	public DaiShou2State() throws InstantiationException {

	}

	public static DaiShou2State getInstance() {
		if (daiShou2State == null) {
			try {
				daiShou2State = new DaiShou2State();
			} catch (InstantiationException ex) {
			}
		}
		return daiShou2State;
	}

	/* (non-Javadoc)
	 * @see hyi.cream.state.State#entry(java.util.EventObject, hyi.cream.state.State)
	 */
	public void entry(EventObject event, State sourceState) {
		Object eventSource = event.getSource();
        if ((sourceState instanceof IdleState && eventSource instanceof DaiShouButton2)
            || (sourceState instanceof DaiShou2State && eventSource instanceof ClearButton)) {
			daiShouBarcode = "";
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("DaiShouNumber"));
		} else if (sourceState instanceof DaiShou2State && eventSource instanceof NumberButton) {
			String s = ((NumberButton) eventSource).getNumberLabel();
			daiShouBarcode += s;
			app.getMessageIndicator().setMessage(daiShouBarcode);
		} else if (sourceState instanceof DaiShou2State && eventSource instanceof Scanner) {
			try {
				((Scanner) eventSource).setDecodeData(true);
				daiShouBarcode = new String(((Scanner) eventSource).getScanDataLabel());
			} catch (JposException e) {
				CreamToolkit.logMessage(e.toString());
				CreamToolkit.logMessage("Jpos exception at " + this);
			}
		}

	}

	/* (non-Javadoc)
	 * @see hyi.cream.state.State#exit(java.util.EventObject, hyi.cream.state.State)
	 */
	public Class exit(EventObject event, State sinkState) {
		DaiShouDef dsf;
		Object eventSource = event.getSource();
		if (eventSource instanceof ClearButton) {
            app.getWarningIndicator().setMessage("");
            if (daiShouBarcode.length() > 0) 
                return DaiShou2State.class;
            else  
                return IdleState.class;
        } else if (eventSource instanceof EnterButton || eventSource instanceof Scanner) {
            if (eventSource instanceof Scanner) {
                try {
                   ((Scanner) eventSource).setDecodeData(true);
                   //((Scanner) eventSource).setDecodeData(false);
                    daiShouBarcode = new String(((Scanner) eventSource).getScanDataLabel());
                } catch (JposException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("Jpos exception at " + this);
                }
            }
            Transaction trans = POSTerminalApplication.getInstance().getCurrentTransaction();
            Object[] lineItemList = trans.getLineItemsArrayLast();
            for(int i = 0 ; i < lineItemList.length ; i++){
            	LineItem currItem  = (LineItem) lineItemList[i];
            	if(currItem!=null&&currItem.getDetailCode().equalsIgnoreCase("O")&&currItem.getDiscountNumber().equals(daiShouBarcode)){
            		app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputDoubleDaishou2Code"));
    				return DaiShou2State.class;
            	}
            }
            daiShouDefs.clear();
			Iterator itr = DaiShouDef.getAllDaiShouDefs().iterator();
			
			int maxPrefixLen = 0;
			while (itr.hasNext()) {
				dsf = (DaiShouDef) itr.next();
				if (dsf != null && dsf.getBarcodeLen().intValue() == daiShouBarcode.length()
//                  && dsf.containsPrefix(daiShouBarcode.substring(0, 2))
                    && dsf.containsPrefix(daiShouBarcode)
				    && dsf.isBarcodeValid(daiShouBarcode)
                    && dsf.getAmount(daiShouBarcode).intValue() > 0) {
					daiShouDefs.add(dsf);
					int prefixLen = dsf.getPrefixLength();
					maxPrefixLen = Math.max(maxPrefixLen, prefixLen);
				}
			}
			List toBeDeleted = new ArrayList();
			for (int i = 0; i < daiShouDefs.size(); i++) {
				dsf = ((DaiShouDef)daiShouDefs.get(i));
				int prefixLen = dsf.getPrefixLength();
				if (prefixLen != maxPrefixLen)
					toBeDeleted.add(dsf);
			}
			daiShouDefs.removeAll(toBeDeleted);

			if (daiShouDefs.size() > 1) {
				app.getMessageIndicator().setMessage("");
				return DaiShou2CheckState.class;
			} else if (daiShouDefs.size() == 1) {
				app.getMessageIndicator().setMessage("");
				return DaiShou2ReadyState.class;
			} else {
				app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("DaiShouNumberWrong"));
				return DaiShou2State.class;
			}
		}

		if (sinkState != null) {
			return sinkState.getClass();
		} else {
			return null;
		}
	}

	public String getBarcode() {
	  return daiShouBarcode;
    }
	
	public ArrayList getMatchDefs() {
		return daiShouDefs;
	}

}
