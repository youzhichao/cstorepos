/*
 * Created on 2003-7-2
 *
 */
package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

/**
 *
 *  开放折扣　输入折扣率
 * 
 */
public class DiscountInputState extends State {
	private DiscountInputState discountInputState = null;
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private String discountRate = "";

	public DiscountInputState() throws InstantiationException {

	}

	public DiscountInputState getInstance() {
		try {
			if (discountInputState == null) {
				discountInputState = new DiscountInputState();
			}
		} catch (InstantiationException ex) {
		}
		return discountInputState;
	}

	public void entry(EventObject event, State sourceState) {
		if (sourceState instanceof DiscountState) {
			discountRate = "";
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("DiscountRateInput"));

		} else if (sourceState instanceof DiscountInputState && event.getSource() instanceof NumberButton) {
			String s = ((NumberButton) event.getSource()).getNumberLabel();
			discountRate += s;
			app.getMessageIndicator().setMessage(discountRate);
		}
	}

	public Class exit(EventObject event, State sinkState) {
		if (sinkState != null)
			return sinkState.getClass();
		else
			return null;
	}

	/**
     * 开放折扣的折扣率 
     * 　　　例如：如果折扣为8折，输入的数据应该为80，而返回的折扣率为0.20　 
	 * @return BigDecimal
	 */
    public BigDecimal getDiscountRate() {
        if (this.discountRate.equals(""))
			return new BigDecimal(1);
		else {
            BigDecimal bd = new BigDecimal(this.discountRate).divide(new BigDecimal(100.0), 2, BigDecimal.ROUND_HALF_UP);
        	return  (new BigDecimal(1.0).subtract(bd));
		}
	}

}
