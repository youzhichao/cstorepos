/**
 * State class
 * @since 2000
 * @author slackware
 */

package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

public class DiscountState extends State {
	private static DiscountState discountState = null;
	private boolean validate = false;
	private SI siDac = null;
	private Class exitState = null;

	public static DiscountState getInstance() {
		try {
			if (discountState == null) {
				discountState = new DiscountState();
			}
		} catch (InstantiationException ex) {
		}
		return discountState;
	}

	public DiscountState() throws InstantiationException {

	}

	public void entry(EventObject event, State sourceState) {
		if (sourceState instanceof SummaryState && event.getSource() instanceof SIButton) {
			SIButton si = (SIButton) event.getSource();
			siDac = SI.queryBySIID(si.getSiID());
			if (siDac == null || !siDac.isAfterSI() || !siDac.isValid())
				return;
			if (siDac.getType().equals("4")) {
				exitState = DiscountInputState.class;
				return;
			}
			processDiscount(siDac);

		} else if (sourceState instanceof DiscountInputState) {
			BigDecimal rate = ((DiscountInputState) sourceState).getDiscountRate();
			siDac.setValue(rate);

			processDiscount(siDac);
			exitState = SummaryState.class;
		}
	}

	public Class exit(EventObject event, State sinkState) {
		//If this SI is需认证，return Slipping2State.class,
		if (exitState != null && !exitState.equals(SummaryState.class))
			return exitState;

		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
		posTerminal.getPayingPane().repaint();
		posTerminal.getPayingPane().setVisible(true);
		if (validate) {
			validate = false;
			return Slipping2State.class;
		}

		return SummaryState.class;

	} //KeylockWarningState Keylock

    void processDiscount(SI siDac) {
        processDiscount(siDac, null, false);
    }

    /**
     * @param siDac 折扣类型
     * @param cTransac 要做折扣的交易
     * @param checkLineItemDiscount　是不是从LineItem中检查折扣是否符合
     */
	void processDiscount(SI siDac, Transaction cTransac, boolean checkLineItemDiscount) {
        if (cTransac == null) {
            cTransac = POSTerminalApplication.getInstance().getCurrentTransaction();
        }
        
        LineItem lineItem = null;
        BigDecimal summary = new BigDecimal(0);
        BigDecimal afterSISummary = new BigDecimal(0);
        BigDecimal itemCount = new BigDecimal(0);
        boolean siCompatible = false;
        int decimalBits = siDac.getAttribute().charAt(1) - '0';
        BigDecimal totalSIAmt = new BigDecimal(0);
        boolean base = false;
        int round = 0;
        if (siDac.getBase().equals("R"))
        	base = true;

		////////////////////////////
		// CHAR[0]: 四舍五入flag
		//    ‘R’ - 四舍五入
		//    ‘C’ - 进位
		//    ‘D’ - 舍去
		// CHAR[1]:小数位数
		// CHAR[2]:SI相容
		// CHAR[3]:浮动%
		// CHAR[4]:认证
		// CHAR[5]:M&M相容
		// CHAR[6]:
		// CHAR[7]
		////////////////////////////

		//四舍五入flag
		if (siDac.getAttribute().charAt(0) == 'R')
			round = BigDecimal.ROUND_HALF_UP;
		else if (siDac.getAttribute().charAt(0) == 'C')
			round = BigDecimal.ROUND_UP;
		else
			round = BigDecimal.ROUND_DOWN;
		//SI相容
		if (siDac.getAttribute().charAt(2) == '1') {
			siCompatible = true;
		}
		//Validate needed or not 
		if (siDac.getAttribute().charAt(4) == '1')
			validate = true;
		else
			validate = false;

		Object[] lineItemArrayLast = cTransac.getLineItemsArrayLast();
		//preventing duplicate discount!
		for (int i = 0; i < lineItemArrayLast.length; i++) {
			lineItem = (LineItem) lineItemArrayLast[i];
			if (lineItem.getDetailCode().equals("D")) {
				if (!siCompatible) {
					validate = false;
					return;
				}
				if (lineItem.getPluNumber().equals(siDac.getSIID())) {
					validate = false;
					return;
				}
			}
		}

		//Object[] normalLineItemArray = cTransac.getNormalLineItemsArray();
		Object[] normalLineItemArray = Transaction.getNormalLineItemsArraySortByAmount(cTransac);
		
		//Accumulate the line item's amount in this SI group
		for (int i = 0; i < normalLineItemArray.length; i++) {
			lineItem = (LineItem) normalLineItemArray[i];
			if (lineItem.getAfterSIAmount() == null) {
				lineItem.setAfterSIAmount(lineItem.getAmount());
			}
            if (lineItem.getAfterDiscountAmount() == null) {
                lineItem.setAfterDiscountAmount(lineItem.getAmount());
            }
		
            if (checkLineItemDiscount? lineItem.isDiscountedBySI(siDac): siDac.contains(lineItem)) {
				// That means the item has got discounted 
				if (!checkLineItemDiscount
                    && lineItem.getDiscountNumber() != null 
                    && !siCompatible) {
					continue;
				}
				itemCount = itemCount.add(lineItem.getQuantity());
				//summary 用于计算折扣金额 
                if (base)
					summary = summary.add(lineItem.getAmount());
				else
					summary = summary.add(lineItem.getAfterSIAmount());
                // afterSISummary 用于计算折扣后实际总金额
                afterSISummary = afterSISummary.add(lineItem.getAfterSIAmount());   
			}
		}

		//totalSIAmt = siDac.computeDiscntValue(summary, itemCount); //Discount on all the line items
		totalSIAmt = siDac.computeDiscntValue(summary);
		//BigDecimal actualAmt = summary.add(totalSIAmt); //The amount custom should pay
        BigDecimal actualAmt = afterSISummary.add(totalSIAmt);
        BigDecimal accumulatedValue = new BigDecimal(0); //The amount that has been shared by the line items
		//Allocate the total SI amount to certain line item
		//          Object[] normalLineItems = cTransac.getNormalLineItemsArray();
        
		for (int i = 0; i < normalLineItemArray.length; i++) {
			lineItem = (LineItem) normalLineItemArray[i];
			BigDecimal percent = new BigDecimal(0);
            if (checkLineItemDiscount? lineItem.isDiscountedBySI(siDac): siDac.contains(lineItem)) {
                // That means the item has got discounted 
                if (!checkLineItemDiscount
                    && lineItem.getDiscountNumber() != null 
                    && !siCompatible) {
                    continue;
                }
                
				if (lineItem.getDiscountType() == null) {
					lineItem.setDiscountType("S"); // SI折扣
				}

                if (!checkLineItemDiscount) {
                    //DISCNO 字段形式　如：Mxxx,Sxx,Sxx 
                    if (lineItem.getDiscountNumber() != null){
    				    lineItem.setDiscountNumber("S" + siDac.getSIID() + "," + lineItem.getDiscountNumber());
                    } else {
                        lineItem.setDiscountNumber("S" + siDac.getSIID());
                    }
                }
                
                
				if (i == normalLineItemArray.length - 1) {
//					lineItem.setSIAmtList(
//						siDac.getSIID(),
//						lineItem.getAfterSIAmount().subtract(actualAmt.subtract(accumulatedValue)));
					if(siDac.getType().equals("0") || siDac.getType().equals("4")) {
//						折扣  ||  开放折扣
						this.accumulate(
								(actualAmt.subtract(accumulatedValue)).subtract(lineItem.getAfterSIAmount()),
								lineItem,
								cTransac,
								siDac);
					}else{
						this.accumulate(
								lineItem.getAfterSIAmount().subtract(actualAmt.subtract(accumulatedValue)),
								lineItem,
								cTransac,
								siDac);
					}
					lineItem.setAfterSIAmount(actualAmt.subtract(accumulatedValue).setScale(decimalBits,round));
                    lineItem.setAfterDiscountAmount(actualAmt.subtract(accumulatedValue).setScale(decimalBits,round));
                    break;
				}

				if (base)
					percent = lineItem.getAmount().divide(summary, decimalBits + 1, round);
				else
					percent = lineItem.getAfterSIAmount().divide(summary, decimalBits + 1, round);
                BigDecimal shareAmt = totalSIAmt.multiply(percent).setScale(decimalBits, round);
                
				//'0'：折扣（百分比）'1'：折让'2'：加成（百分比)
				this.accumulate(shareAmt, lineItem, cTransac, siDac);
				//lineItem.setSIAmtList(siDac.getSIID(), totalSIAmt.multiply(percent));
//                lineItem.setSIAmtList(siDac.getSIID(), shareAmt);
                BigDecimal afterAmt = lineItem.getAfterSIAmount().add(shareAmt);
				//	lineItem.getAfterSIAmount().add(totalSIAmt.multiply(percent)).setScale(decimalBits, round);
                //lineItem.getAfterSIAmount().add(shareAmt).setScale(decimalBits, round);
                
                lineItem.setAfterSIAmount(afterAmt);
                
                lineItem.setAfterDiscountAmount(afterAmt);
                
                accumulatedValue = accumulatedValue.add(afterAmt);
			}
		}

		LineItem siLineItem = null;
		try {
			siLineItem = new LineItem();
		} catch (InstantiationException ie) {
			CreamToolkit.logMessage(ie.toString());
			return;
		}

		siLineItem.setTransactionNumber(cTransac.getTransactionNumber());
		siLineItem.setTerminalNumber(cTransac.getTerminalNumber());
		siLineItem.setDetailCode("D");
		siLineItem.setPluNumber(siDac.getSIID());
		siLineItem.setItemNumber(siDac.getSIID());
		siLineItem.setRemoved(false);
		siLineItem.setDescription(siDac.getPrintName());
		siLineItem.setUnitPrice(siDac.getValue());
		siLineItem.setOriginalPrice(siDac.getValue());
		siLineItem.setQuantity(new BigDecimal(1));
		siLineItem.setAmount(totalSIAmt);
		try {
			cTransac.setSIAmtList(siDac.getSIID(), totalSIAmt);
			cTransac.addLineItem(siLineItem);

		} catch (TooManyLineItemsException te) {
			CreamToolkit.logMessage(te.toString());
		}

	}

	private void accumulate(BigDecimal shareAmt, LineItem lineItem, Transaction cTransac, SI siDac) {
		if (cTransac == null) {
            cTransac = POSTerminalApplication.getInstance().getCurrentTransaction();
        } 
        
		switch (siDac.getType().charAt(0)) {
			case '0' : //折扣（百分比）
			case '4' : //开放折扣

				if (lineItem.getTaxType().equals("0")) {
//					cTransac.setSIPercentOffAmount0(cTransac.getSIPercentOffAmount0().add(shareAmt.abs()));
//					cTransac.setSIPercentOffCount0(cTransac.getSIPercentOffCount0().intValue() + 1);
					if(shareAmt.doubleValue() < 0) {
						cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().add(shareAmt.abs()));
						cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);
					}else{
						cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().add(shareAmt.negate()));
						cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);	
					}
				} else if (lineItem.getTaxType().equals("1")) {
//					cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().add(shareAmt.abs()));
//					cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);
					if(shareAmt.doubleValue() < 0) {
						cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().add(shareAmt.abs()));
						cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);
					}else{
						cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().add(shareAmt.negate()));
						cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);	
					}
				} else if (lineItem.getTaxType().equals("2")) {
//					cTransac.setSIPercentOffAmount2(cTransac.getSIPercentOffAmount2().add(shareAmt.abs()));
//					cTransac.setSIPercentOffCount2(cTransac.getSIPercentOffCount2().intValue() + 1);
					if(shareAmt.doubleValue() < 0) {
						cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().add(shareAmt.abs()));
						cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);
					}else{
						cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().add(shareAmt.negate()));
						cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);	
					}
				} else if (lineItem.getTaxType().equals("3")) {
//					cTransac.setSIPercentOffAmount3(cTransac.getSIPercentOffAmount3().add(shareAmt.abs()));
//					cTransac.setSIPercentOffCount3(cTransac.getSIPercentOffCount3().intValue() + 1);
					if(shareAmt.doubleValue() < 0) {
						cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().add(shareAmt.abs()));
						cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);
					}else{
						cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().add(shareAmt.negate()));
						cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);	
					}
				} else if (lineItem.getTaxType().equals("4")) {
//					cTransac.setSIPercentOffAmount4(cTransac.getSIPercentOffAmount4().add(shareAmt.abs()));
//					cTransac.setSIPercentOffCount4(cTransac.getSIPercentOffCount4().intValue() + 1);
					if(shareAmt.doubleValue() < 0) {
						cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().add(shareAmt.abs()));
						cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);
					}else{
						cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().add(shareAmt.negate()));
						cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() + 1);	
					}
				}

				break;

			case '1' : //‘1’：折让
			case '3' : //全折
				if (lineItem.getTaxType().equals("0")) {
					cTransac.setSIDiscountAmount0(cTransac.getSIDiscountAmount0().add(shareAmt.abs()));
					cTransac.setSIDiscountCount0(cTransac.getSIDiscountCount0().intValue() + 1);
				} else if (lineItem.getTaxType().equals("1")) {
					cTransac.setSIDiscountAmount1(cTransac.getSIDiscountAmount1().add(shareAmt.abs()));
					cTransac.setSIDiscountCount1(cTransac.getSIDiscountCount1().intValue() + 1);
				} else if (lineItem.getTaxType().equals("2")) {
					cTransac.setSIDiscountAmount2(cTransac.getSIDiscountAmount2().add(shareAmt.abs()));
					cTransac.setSIDiscountCount2(cTransac.getSIDiscountCount2().intValue() + 1);
				} else if (lineItem.getTaxType().equals("3")) {
					cTransac.setSIDiscountAmount3(cTransac.getSIDiscountAmount3().add(shareAmt.abs()));
					cTransac.setSIDiscountCount3(cTransac.getSIDiscountCount3().intValue() + 1);
				} else if (lineItem.getTaxType().equals("4")) {
					cTransac.setSIDiscountAmount4(cTransac.getSIDiscountAmount4().add(shareAmt.abs()));
					cTransac.setSIDiscountCount4(cTransac.getSIDiscountCount4().intValue() + 1);
				}

				break;

			case '2' : //加成
				if (lineItem.getTaxType().equals("0")) {
					cTransac.setSIPercentPlusAmount0(cTransac.getSIPercentPlusAmount0().add(shareAmt.abs()));
					cTransac.setSIPercentPlusCount0(cTransac.getSIPercentPlusCount0().intValue() + 1);
				} else if (lineItem.getTaxType().equals("1")) {
					cTransac.setSIPercentPlusAmount1(cTransac.getSIPercentPlusAmount1().add(shareAmt.abs()));
					cTransac.setSIPercentPlusCount1(cTransac.getSIPercentPlusCount1().intValue() + 1);
				} else if (lineItem.getTaxType().equals("2")) {
					cTransac.setSIPercentPlusAmount2(cTransac.getSIPercentPlusAmount2().add(shareAmt.abs()));
					cTransac.setSIPercentPlusCount2(cTransac.getSIPercentPlusCount2().intValue() + 1);
				} else if (lineItem.getTaxType().equals("3")) {
					cTransac.setSIPercentPlusAmount3(cTransac.getSIPercentPlusAmount3().add(shareAmt.abs()));
					cTransac.setSIPercentPlusCount3(cTransac.getSIPercentPlusCount3().intValue() + 1);
				} else if (lineItem.getTaxType().equals("4")) {
					cTransac.setSIPercentPlusAmount4(cTransac.getSIPercentPlusAmount4().add(shareAmt.abs()));
					cTransac.setSIPercentPlusCount4(cTransac.getSIPercentPlusCount4().intValue() + 1);
				}

				break;

			default :
		}
	}

}
