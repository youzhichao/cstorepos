package hyi.cream.state;

import java.util.*;
import java.math.BigDecimal;

import jpos.*;
import jpos.events.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;

public class DrawerOpenState extends State {
    static DrawerOpenState drawerOpenState = null;

    //private State sourceState = null;
    private boolean isReturnTran = false;
    private boolean isCashTran = false;
    private boolean showWarning = true;
    private Class exitState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();

    //Meyer/2003-02-26
    // Get MixAndMatchVersion
    private static final String MIX_AND_MATCH_VERSION =
        CreamProperties.getInstance().getProperty("MixAndMatchVersion");

    public static DrawerOpenState getInstance() {
        try {
            if (drawerOpenState == null) {
                drawerOpenState = new DrawerOpenState();
            }
        } catch (InstantiationException ex) {
        }
        return drawerOpenState;
    }

    /**
     * Constructor
     */
    public DrawerOpenState() throws InstantiationException {

    }

    public void entry(EventObject event, State sourceState) {
        showWarning = false;
        isReturnTran = false;
        isCashTran = false;
        if (sourceState instanceof Paying3State) {
            if (app.getReturnItemState())
                exitState = CashierRightsCheckState.getSourceState();
            else
                exitState = InitialState.class;
        } else {
            exitState = KeyLock1State.class;
            isReturnTran = false;
            if (sourceState instanceof ReturnSummaryState) {
                isReturnTran = true;
                exitState = CashierRightsCheckState.getSourceState();
            } else if (sourceState instanceof MixAndMatchState) {
                isReturnTran = true;
            } else if (sourceState instanceof CashInIdleState || sourceState instanceof CashOutIdleState) {
                isCashTran = true;
                exitState = CashierRightsCheckState.getSourceState();    
            }
        }
        if (!isCashTran) {
            CreamToolkit.showText(app.getCurrentTransaction(), 1);
        }

        // Process training mode
        if (app.getTrainingMode()) {
            Runnable training = new Runnable() {
                public void run() {
                    POSPeripheralHome posHome = POSPeripheralHome.getInstance();
                    try {
                        CashDrawer drawer = posHome.getCashDrawer();
                        if (!posHome.getEventForwardEnabled())
                            posHome.setEventForwardEnabled(true);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }
                        posHome.statusUpdateOccurred(new StatusUpdateEvent(drawer, 0));
                    } catch (NoSuchPOSDeviceException ne) {
                        CreamToolkit.logMessage(ne.toString());
                        return;
                    }
                }
            };
            new Thread(training).start();
            return;
        }

        boolean checkDrawerClose = CreamProperties.getInstance().getProperty(
            "CheckDrawerClose", "no").equalsIgnoreCase("yes");

        // Open drawer
        final POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        try {
            CashDrawer drawer = posHome.getCashDrawer();
            if (!posHome.getEventForwardEnabled())
                posHome.setEventForwardEnabled(true);
            if (!drawer.getDeviceEnabled())
                drawer.setDeviceEnabled(true);
            if (checkDrawerClose)
                drawer.addStatusUpdateListener(posHome);
            drawer.openDrawer();
        } catch (Exception e) {
            // ignore any exception
        }

        // 保存交易
        app.getMessageIndicator().setMessage(res.getString("TransactionIsStoring"));
        if (!app.getTrainingMode())
            storeTransaction();
        app.getMessageIndicator().setMessage("");

        // Print payment
        if (/*!app.getCurrentTransaction().getDealType3().equals("4")
            && */ !app.getCurrentTransaction().getDealType2().equals("G")
            && !app.getCurrentTransaction().getDealType2().equals("H")
            && !app.getCurrentTransaction().getDealType2().equals("4")
            //Bruce/2003-12-24/ 部分退货交易不要在这印payment，因为已经在ReturnSummaryState印过了
            && !(app.getCurrentTransaction().getDealType3().equals("4") && !app.getReturnItemState())  
            && !app.getIsAllReturn()) {

        	CreamPrinter.getInstance().printPayment(app.getCurrentTransaction());
            //gllg
            CreamPrinter printer = CreamPrinter.getInstance();
            if (printer.getPrintEnabled() == false 
                    && app.getCurrentTransaction().getDaiShouAmount2().compareTo(new BigDecimal(0)) > 0) {
                boolean originalPrinterEnableState = printer.getPrintEnabled();
                printer.setPrintEnabled(true);
                printer.reprint(app.getCurrentTransaction());
                printer.setPrintEnabled(originalPrinterEnableState);
            }
        }

        if (checkDrawerClose) {
            // Generate a wait-for-drawer-closed thread
            new Thread(new Runnable() {
                public void run() {
                    try {
                        //Start a timer.
                        int wait =
                            Integer.parseInt(CreamProperties.getInstance().getProperty("DrawerCloseTime"));
                        showWarning = true;
                        startWarning(wait);
                        CashDrawer drawer = posHome.getCashDrawer();
                        if (drawer != null) {
                            drawer.waitForDrawerClose(wait * 1000, 0, 0, 300);
                            if (showWarningHint != null)
                                showWarningHint.interrupt();
                            drawer.removeStatusUpdateListener(posHome);
                        }
                        showWarning = false;
                    } catch (Exception e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                        return;
                    }
                }
            }).start();
        } else {
            // Generate a thread which creating fake CashDrawer close event
            new Thread(new Runnable() {
                public void run() {
                    try {
                        CashDrawer drawer = posHome.getCashDrawer();
                        posHome.statusUpdateOccurred(
                            new StatusUpdateEvent((drawer != null) ? drawer : new CashDrawer(), 0));
                    } catch (Exception e) {
                        e.printStackTrace(CreamToolkit.getLogger());
                        return;
                    }
                }
            }).start();
        }

//        //Chech drawer open or not
//        String str = CreamProperties.getInstance().getProperty("CheckDrawerClose");
//        if (str == null)
//            str = "yes";
//        str = str.trim();
//        if (str.equals("no")) {
//            Runnable t = new Runnable() {
//                public void run() {
//                    POSPeripheralHome deviceHome = POSPeripheralHome.getInstance();
//                    CashDrawer drawer = null;
//                    try {
//                        drawer = deviceHome.getCashDrawer();
//                    } catch (NoSuchPOSDeviceException e) {
//                    }
//                    if (!deviceHome.getEventForwardEnabled())
//                        deviceHome.setEventForwardEnabled(true);
//                    try {
//                        if (drawer != null) {
//                            drawer.setDeviceEnabled(true);
//                            drawer.openDrawer();
//                        }
//                        //Thread.sleep(1000);
//                        if (!app.getCurrentTransaction().getDealType3().equals("4")
//                            && !app.getCurrentTransaction().getDealType2().equals("G")
//                            && !app.getCurrentTransaction().getDealType2().equals("H")
//                            && !app.getCurrentTransaction().getDealType2().equals("4")
//                            && !app.getIsAllReturn()) {
//                            CreamPrinter.getInstance().printPayment(app.getCurrentTransaction());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace(CreamToolkit.getLogger());
//                    }
//                    deviceHome.statusUpdateOccurred(
//                        new StatusUpdateEvent((drawer != null) ? drawer : new CashDrawer(), 0));
//                }
//            };
//            new Thread(t).start();
//        } else {
//            Runnable t = new Runnable() {
//                public void run() {
//                    //set posHome enabled to prepare for event forwarding
//                    POSPeripheralHome posHome = POSPeripheralHome.getInstance();
//                    CashDrawer cash = null;
//                    try {
//                        cash = posHome.getCashDrawer();
//                    } catch (NoSuchPOSDeviceException ne) {
//                        CreamToolkit.logMessage(ne.toString());
//                        return;
//                    }
//                    if (!posHome.getEventForwardEnabled())
//                        posHome.setEventForwardEnabled(true);
//                    //drive the cash drawer
//                    try {
//                        if (!cash.getDeviceEnabled())
//                            cash.setDeviceEnabled(true);
//                        cash.addStatusUpdateListener(posHome);
//                        cash.openDrawer();
//                        if (!app.getCurrentTransaction().getDealType3().equals("4")
//                            && !app.getCurrentTransaction().getDealType2().equals("G")
//                            && !app.getCurrentTransaction().getDealType2().equals("H")
//                            && !app.getCurrentTransaction().getDealType2().equals("4")
//                            && !app.getIsAllReturn()) {
//                            CreamPrinter.getInstance().printPayment(app.getCurrentTransaction());
//                        }
//                        Thread.sleep(1000);
//                        //Start a timer.
//                        int wait =
//                            Integer.parseInt(CreamProperties.getInstance().getProperty("DrawerCloseTime"));
//                        showWarning = true;
//                        startWarning(wait);
//                        cash.waitForDrawerClose(wait * 1000, 0, 0, 300);
//                        showWarning = false;
//                        if (showWarningHint != null)
//                            showWarningHint.interrupt();
//                        cash.removeStatusUpdateListener(posHome);
//                        cash.setDeviceEnabled(false);
//                    } catch (Exception je) {
//                        je.printStackTrace(CreamToolkit.getLogger());
//                        return;
//                    }
//                }
//            };
//            (new Thread(t)).start();
//        }
    }

    static Thread showWarningHint = null;

    private void startWarning(int w) {
        final int wait = w;
        Runnable startWarning = new Runnable() {
            public void run() {
                try {
                    Thread.sleep(wait * 1000 + 1000);
                    if (showWarning)
                        app.getWarningIndicator().setMessage(res.getString("PleaseCloseDrawer"));
                } catch (Exception e) {
                    return;
                }
            }
        };
        showWarningHint = new Thread(startWarning);
        showWarningHint.start();
    }

    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");

        if (app.getTrainingMode()) {
            return exitState;
        }
        if (app.getBuyerNumberPrinted())
            app.setBuyerNumberPrinted(false);

        //如果当前交易保存还没有结束，等待
        //int cnt = 0;
        //Transaction currTran = app.getCurrentTransaction();
        //while (true) {
        //    if (currTran.isStoreComplete())
        //        break;
        //    try {
        //        Thread.sleep(500);
        //    } catch (InterruptedException e) {
        //    }
        //    if (++cnt > 20) {
        //        CreamToolkit.logMessage("Warn> Wait too long to store data!");
        //        break;
        //    }
        //} 

        int nextInvoicNumber =
            Integer.parseInt(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
        CreamProperties.getInstance().setProperty("NextInvoiceNumber", Integer.toString(++nextInvoicNumber));
        CreamProperties.getInstance().deposit();
        app.setIsAllReturn(false);
        app.setReturnItemState(false);
        return exitState;
    }

    private void storeTransaction() {
        Transaction currTran = app.getCurrentTransaction();
        currTran.setStoreComplete(false);
        
        synchronized (POSTerminalApplication.waitForTransactionStored) { // -> see CheckCashierPasswordState.java
                
            //Meyer/2003-02-26/
            // UpdateTaxMM only when meeting version 1
            if (MIX_AND_MATCH_VERSION == null
                || MIX_AND_MATCH_VERSION.trim().equals("")
                || MIX_AND_MATCH_VERSION.trim().equals("1")) {
                currTran.updateTaxMM();
            }

            //Object lineItems[] = cTransac.getLineItemsArrayLast();
            //BigDecimal amount = new BigDecimal(0);
            //BigDecimal lineAmount = new BigDecimal(0);
            if (!isReturnTran) {
            	CreamToolkit.logMessage("store() in DrawerOpernState storetransaction()");
                app.getCurrentTransaction().store();
                ScanTicketState.getInstance().saveTokenPayList();
            }

            //保存交易完成
            currTran.setStoreComplete(true);
        }
    }
    
}
