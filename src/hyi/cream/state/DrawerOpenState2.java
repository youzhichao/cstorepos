/**
 * State class
 * @since 2000
 * @author slackware
 */
 
package hyi.cream.state;

import java.util.*;
import java.math.*;
import java.io.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;

//for jpos devices
import jpos.*;
import jpos.events.*;

//for repaint
import java.awt.*;

public class DrawerOpenState2 extends State {

	static DrawerOpenState2 drawerOpenState = null;
	ZReport z = null;
	ShiftReport shift = null;
	boolean open = true;
	boolean showWarning = true; 
	
	public static DrawerOpenState2 getInstance() {
		try {
			if (drawerOpenState == null) {
				drawerOpenState = new DrawerOpenState2();
			}
        } catch (InstantiationException ex) {
		}
		return drawerOpenState;
	}

    /**
     * Constructor
     */
	public DrawerOpenState2() throws InstantiationException {
	}

    private void updateDrawerOpenCount() {
        z = ZReport.getOrCreateCurrentZReport();
        if (z != null) {
            z.setDrawerOpenCount(new Integer(z.getDrawerOpenCount().intValue() + 1));
            z.update();
        }

        shift = ShiftReport.getCurrentShift();
        if (shift != null) {
            shift.setDrawerOpenCount(new Integer(shift.getDrawerOpenCount().intValue() + 1));
            shift.update();
        }
    }

	public void entry (EventObject event, State sourceState) {
		System.out.println("This is DrawerOpenState2's Entry!");
   		showWarning = false;
		//Chech drawer open or not
		/*if (shift != null) {
			if (shift.getAccountingDate().compareTo(CreamToolkit.getInitialDate()) != 0)
				open = false;
			else
			    open = true;
		} else {
			open = false;
		}*/
		if (CreamProperties.getInstance().getProperty("CashierNumber").equals("")) {
			open = false;
		} else
		    open = true;
        if (!open) {
            Runnable t = new Runnable() {
                public void run() {
                    POSPeripheralHome posHome = POSPeripheralHome.getInstance();
                    try {
                        CashDrawer cashi = posHome.getCashDrawer();
                        if (!posHome.getEventForwardEnabled())
                            posHome.setEventForwardEnabled(true);
                        posHome.statusUpdateOccurred(new StatusUpdateEvent(cashi, 0));
                    } catch (NoSuchPOSDeviceException ne) {
                        CreamToolkit.logMessage(ne.toString());
                        return;
                    }
                }
            };
            new Thread(t).start();
            return;
        }
		String str = CreamProperties.getInstance().getProperty("CheckDrawerClose");
	  	if (str == null)
			str = "yes";
		if (str.equals("no")) {
			Runnable t = new Runnable() {
				public void run() {
					//POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
					POSPeripheralHome posHome = POSPeripheralHome.getInstance();
					try {
						CashDrawer cashi = posHome.getCashDrawer();
						if (!posHome.getEventForwardEnabled())
							posHome.setEventForwardEnabled(true);
						try {
						    cashi.setDeviceEnabled(true);
							cashi.openDrawer();
							updateDrawerOpenCount();
							Thread.sleep(1000);
						} catch (Exception e) {
						     CreamToolkit.logMessage(e.toString());
                        }
						posHome.statusUpdateOccurred(new StatusUpdateEvent(cashi, 0));
					} catch (NoSuchPOSDeviceException ne) { CreamToolkit.logMessage(ne.toString()); return; }				
				}
			};
			new Thread(t).start();
		} else {
			//sourceState = sourceState;
			Runnable t = new Runnable() {
				public void run() {
				//set posHome enabled to prepare for event forwarding
					POSPeripheralHome posHome = POSPeripheralHome.getInstance();
					CashDrawer cash = null;
					try {
						cash = posHome.getCashDrawer();
					} catch (NoSuchPOSDeviceException ne) {CreamToolkit.logMessage(ne.toString()); return; }
					if (!posHome.getEventForwardEnabled())
						posHome.setEventForwardEnabled(true);
				//drive the cash drawer
					try {
						if (!cash.getDeviceEnabled())
							cash.setDeviceEnabled(true);
						cash.addStatusUpdateListener(posHome);
						cash.openDrawer();
					   	POSTerminalApplication.getInstance().getMessageIndicator().setMessage("抽屉已开启");
						updateDrawerOpenCount();
						Thread.sleep(2000);
						int wait = Integer.parseInt(CreamProperties.getInstance().getProperty("DrawerCloseTime"));
						showWarning = true;
						startWarning(wait);
						cash.waitForDrawerClose(wait * 1000, 0, 0, 100);
						showWarning = false;
						if (showWarningHint != null)
						   showWarningHint.interrupt();
				 //Start a timer.
						cash.removeStatusUpdateListener(posHome);
						cash.setDeviceEnabled(false);
					} catch (Exception je) {
						CreamToolkit.logMessage(je.toString());
						return;
					}
				}
			};
			(new Thread(t)).start();
		}
		//System.out.println("DrawerOpenState2 entry finished");
	}

  	static Thread showWarningHint = null;
	private void startWarning(int w) {
		final int wait = w;
		Runnable startWarning = new Runnable() {
			public void run() {
				try {
					Thread.sleep(wait * 1000 + 1000);			
					if (showWarning)
					   POSTerminalApplication.getInstance().getWarningIndicator().setMessage("请关上抽屉");
					/*while (showWarning) {

					}*/
				} catch (Exception e) {
					//e.printStackTrace();
					return;
				}
			}
		};
		showWarningHint = new Thread(startWarning);
		showWarningHint.start();
	}

	public Class exit(EventObject event, State sinkState) {
		System.out.println("This is DrawerOpenState2's Exit!");
		if (!open)
			POSTerminalApplication.getInstance().getWarningIndicator().setMessage("请登录后再开抽屉");
		else {
			POSTerminalApplication.getInstance().getMessageIndicator().setMessage("");
			POSTerminalApplication.getInstance().getWarningIndicator().setMessage("");
	    }
        // init newCashierID
        POSTerminalApplication.setNewCashierID("");
		return CashierRightsCheckState.getSourceState();
//        return KeyLock1State.class;
	}
}
