/**
 * State class
 * @since 2000
 * @author slackware
 */

package hyi.cream.state;

import java.util.*;
import java.math.*;
import java.io.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.uibeans.SignOffForm;
import hyi.cream.util.*;

//for jpos devices
import jpos.*;
import jpos.events.*;

//for repaint
import java.awt.*;

public class DrawerOpenState3 extends State {

	static DrawerOpenState3 drawerOpenState = null;
	boolean showWarning = true;

	public static DrawerOpenState3 getInstance() {
		try {
			if (drawerOpenState == null) {
				drawerOpenState = new DrawerOpenState3();
			}
        } catch (InstantiationException ex) {
		}
		return drawerOpenState;
	}

    /**
     * Constructor
     */
	public DrawerOpenState3() throws InstantiationException {
	     drawerOpenState = this;
	}


	public void entry (EventObject event, State sourceState) {
        //System.out.println("DrawerOpenState3 entry");
        StateMachine.setFloppySaveError(false);
	   	showWarning = false;
		String str = CreamProperties.getInstance().getProperty("CheckDrawerClose");
		if (CreamProperties.getInstance().getProperty("ShiftPrint") == null) {
			CreamProperties.getInstance().setProperty("ShiftPrint", "no");
		}
		final String shiftPrint = CreamProperties.getInstance().getProperty("ShiftPrint");
		final ShiftReport shift = ShiftReport.getCurrentShift();

        // 如果使用现金清点单，就不用检查drawer close，这里也不用开抽屉（因为已经在ShiftState开过了）
        final String useCashFormStr = CreamProperties.getInstance().getProperty("UseCashForm");
        final boolean useCashForm = (useCashFormStr != null && useCashFormStr.equalsIgnoreCase("yes"));
		if (str == null || useCashForm)
			str = "no";  // Bruce> change default "CheckDrawerClose" to "no"

		if (str.equals("no")) {
			Runnable t = new Runnable() {
				public void run() {
					//POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
					POSPeripheralHome posHome = POSPeripheralHome.getInstance();
					try {
						CashDrawer cashi = posHome.getCashDrawer();
						if (!posHome.getEventForwardEnabled())
							posHome.setEventForwardEnabled(true);
						try {
						    cashi.setDeviceEnabled(true);
                            if (!useCashForm) //如果使用现金清点单，这里不用开抽屉（因为已经在ShiftState开过了）
    							cashi.openDrawer();
							if (shiftPrint.equals("yes"))
							   CreamPrinter.getInstance().printShiftReport(shift, drawerOpenState);

                            // Print CashForm
                            if (useCashForm) {
                                CashForm cf = CashForm.queryByZNumberAndShiftNumber(
                                    shift.getZSequenceNumber().intValue(),
                                    shift.getSequenceNumber().intValue());
                                CreamPrinter.getInstance().printCashForm(cf);
                            }
							Thread.sleep(500);
						} catch (Exception e) {
						     CreamToolkit.logMessage(e.toString());
                        }
						posHome.statusUpdateOccurred(new StatusUpdateEvent(cashi, 0));
					} catch (NoSuchPOSDeviceException ne) { CreamToolkit.logMessage(ne.toString()); return; }
				}
			};
			new Thread(t).start();
		} else {
			//sourceState = sourceState;
			Runnable t = new Runnable() {
				public void run() {
				//set posHome enabled to prepare for event forwarding
					POSPeripheralHome posHome = POSPeripheralHome.getInstance();
					CashDrawer cash = null;
					try {
						cash = posHome.getCashDrawer();
					} catch (NoSuchPOSDeviceException ne) {CreamToolkit.logMessage(ne.toString()); return; }
					if (!posHome.getEventForwardEnabled())
						posHome.setEventForwardEnabled(true);
				//drive the cash drawer
					try {
						if (!cash.getDeviceEnabled())
							cash.setDeviceEnabled(true);
						cash.addStatusUpdateListener(posHome);
						cash.openDrawer();
						//
						Thread.sleep(2000);
						if (shiftPrint.equals("yes"))
						   CreamPrinter.getInstance().printShiftReport(shift, drawerOpenState);
						 //Start a timer.
						int wait = Integer.parseInt(CreamProperties.getInstance().getProperty("DrawerCloseTime"));
						showWarning = true;
						startWarning(wait);
						cash.waitForDrawerClose(wait * 1000, 0, 0, 100);
						showWarning = false;
						if (showWarningHint != null)
						   showWarningHint.interrupt();
						 //Start a timer.
						cash.removeStatusUpdateListener(posHome);
						cash.setDeviceEnabled(false);
					} catch (Exception je) {
						CreamToolkit.logMessage(je.toString());
						return;
					}
				}
			};
			(new Thread(t)).start();
		}
	}

	static Thread showWarningHint = null;

	private void startWarning(int w) {
		final int wait = w;
		Runnable startWarning = new Runnable() {
			public void run() {
				try {
					Thread.sleep(wait * 1000 + 1000);
					if (showWarning)
					   POSTerminalApplication.getInstance().getWarningIndicator().setMessage("请关上抽屉");
					/*while (showWarning) {

					}*/
				} catch (Exception e) {
					//e.printStackTrace();
					return;
				}
			}
		};
		showWarningHint = new Thread(startWarning);
		showWarningHint.start();
	}

	public Class exit(EventObject event, State sinkState) {
		//System.out.println("This is DrawerOpenState3's Exit!");
	    POSTerminalApplication.getInstance().getWarningIndicator().setMessage("");
		if (DacTransfer.getInstance().getStoZPrompt()) {
			DacTransfer.getInstance().setStoZPrompt(false);
			return hyi.cream.state.ZState.class;
		} else
	    	return KeyLock1State.class;
	}
}
