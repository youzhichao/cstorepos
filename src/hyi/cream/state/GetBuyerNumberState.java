
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.util.*;

import java.util.*;
import jpos.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class GetBuyerNumberState extends GetSomeAGState {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static GetBuyerNumberState getBuyerNumberState = null;

    public static GetBuyerNumberState getInstance() {
        try {
            if (getBuyerNumberState == null) {
                getBuyerNumberState = new GetBuyerNumberState();
            }
        } catch (InstantiationException ex) {
        }
        return getBuyerNumberState;
    }

    /**
     * Constructor
     */
    public GetBuyerNumberState() throws InstantiationException {
    }

	public boolean checkValidity() {
		if (!app.getBuyerNumberPrinted()) {
			app.getCurrentTransaction().setBuyerNumber(getAlphanumericData());
			//app.getButtonPanel().showLayer(2);
			return true;
		}
		else
			return true;
    }

	public Class getUltimateSinkState() {
		if (!app.getBuyerNumberPrinted()
			&& !app.getCurrentTransaction().getBuyerNumber().equals("")) {
			ResourceBundle res = CreamToolkit.GetResource();
			app.getMessageIndicator().setMessage(res.getString("BuyerNumberUpdated"));
		} else {

		}
        return IdleState.class;
	}

	public Class getInnerInitialState() {
        return BuyerState.class;
	}


}

 
