
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;

import java.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class GetCashierNumberState extends GetSomeAGState {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static GetCashierNumberState getCashierNumberState = null;

    public static GetCashierNumberState getInstance() {
        try {
            if (getCashierNumberState == null) {
                getCashierNumberState = new GetCashierNumberState();
            }
        } catch (InstantiationException ex) {
        }
        return getCashierNumberState;
    }

	/**
     * Constructor
     */
    public GetCashierNumberState() throws InstantiationException {
    }

    public boolean checkValidity() {
        String cashierNumber = getAlphanumericData();
		Cashier cashierDAC = Cashier.queryByCashierID(cashierNumber);
        if (cashierDAC == null) {
            return false;
		} else {
            //Bruce/20030430/
            //Add a property "LevelsCanSignOn". If doesn't exist, don't check; otherwise
            //check if the signon cashier's level match any in "LevelsCanSignOn"
            String levels = CreamProperties.getInstance().getProperty("LevelsCanSignOn");
            if (levels != null) {
                String cashierLevel = cashierDAC.getCashierLevel();
                StringTokenizer tk = new StringTokenizer(levels, ", ");
                while (tk.hasMoreTokens()) {
                    String level = tk.nextToken();
                    if (cashierLevel.equals(level)) {
                        app.setCurrentCashierNumber(cashierNumber);
                        return true;
                    }
                }
                return false;       // cannot match any level in "LevelsCanSignOn"
            }
			app.setCurrentCashierNumber(cashierNumber);
		    return true;
		}
    }

    public Class getUltimateSinkState() {
        return CheckCashierPasswordState.class;
    }

    public Class getInnerInitialState() {
        return CashierState.class;
    }
}

 