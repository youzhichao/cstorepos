
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class GetInvoiceNumberState extends GetSomeAGState {
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static GetInvoiceNumberState getInvoiceNumberState = null;

    public static GetInvoiceNumberState getInstance() {
        try {
            if (getInvoiceNumberState == null) {
                getInvoiceNumberState = new GetInvoiceNumberState();
            }
        } catch (InstantiationException ex) {
        }
        return getInvoiceNumberState;
    }

    /**
     * Constructor
     */
    public GetInvoiceNumberState() throws InstantiationException {
    }

    public boolean checkValidity() {
        String invoiceNo = getAlphanumericData();
        app.getCurrentTransaction().setInvoiceID(invoiceNo);
        return true;
    }

    public Class getUltimateSinkState() {
        return IdleState.class;
    }

    public Class getInnerInitialState() {
        return InvoiceState.class;
    }
}

 