
// Copyright (c) 2000 HYI
package hyi.cream.state;

import java.util.EventObject;

import hyi.cream.*;
import hyi.cream.dac.*;

public class GetMemberNumberState extends GetSomeAGState {
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static GetMemberNumberState getMemberNumberState = null;
    static private Member member;

    public static GetMemberNumberState getInstance() {
        try {
            if (getMemberNumberState == null) {
                getMemberNumberState = new GetMemberNumberState();
            }
        } catch (InstantiationException ex) {
        }
        return getMemberNumberState;
    }

    public GetMemberNumberState() throws InstantiationException {
    }

    public boolean checkValidity() {
        String memberID = getAlphanumericData();
        member = Member.queryByMemberID(memberID);
        setMember(member);
        if (member == null) {
            return false;
        } else {
            app.getCurrentTransaction().setMemberID(member.getMemberCardID());
            return true;
        }
    }

    public Class getUltimateSinkState() {
        return ShowMemberInfoState.class;
    }

    public Class getInnerInitialState() {
        return MemberState.class;
    }

    public static Member getMember() {
        return member;
    }

    public static void setMember(Member member) {
        GetMemberNumberState.member = member;
    }
}

 