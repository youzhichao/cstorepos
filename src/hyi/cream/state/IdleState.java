/*jadclipse*/// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) radix(10) lradix(10) 
// Source File Name:   IdleState.java
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.SystemInfo;
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;
import hyi.cream.util.*;
import java.awt.Component;
import java.util.*;
import jpos.*;

// Referenced classes of package hyi.cream.state:
//            State, SummaryState, InitialState, CheckCashierPasswordState, 
//            CashierRightsCheckState

/**
 * State class
 * @since 2000
 * @author slackware
 */
public class IdleState extends State {

    private boolean indicatorCleared;
    private ResourceBundle res;
    static IdleState idleState = null;
    private POSTerminalApplication posTerminal;
    private Transaction trans;
    private static boolean start = false;
    //private static boolean started = false;
    static private  Map pluQtyMap = null;

    public static IdleState getInstance() {
        try {
            if (idleState == null)
                idleState = new IdleState();
        } catch (InstantiationException instantiationexception) {
        }
        return idleState;
    }

    /**
     * Constructor
     */
    public IdleState() throws InstantiationException {
        indicatorCleared = false;
        res = CreamToolkit.GetResource();
        posTerminal = POSTerminalApplication.getInstance();
        trans = posTerminal.getCurrentTransaction();
    }

    public void entry(EventObject event, State sourceState) {
        posTerminal.getMessageIndicator().setMessage(res.getString("SelectPlu"));
        posTerminal.getPopupMenuPane().setEnabled(true);
        posTerminal.getPopupMenuPane().clear();
        posTerminal.setEnabledPopupMenu(true);
        posTerminal.getItemList().repaint();
        if (posTerminal.getScanCashierNumber() && !posTerminal.getChecked()) {
            posTerminal.getMessageIndicator().setMessage(res.getString("CheckWarning"));
            posTerminal.getSystemInfo().setSalesManNumber("");
        } else {
            posTerminal.getMessageIndicator().setMessage(res.getString("SelectPlu"));
        }
        //Bruce/2003-12-24
        //解决MixAndMatchVersion=2时，若从结账画面返回至销售画面时，会因调用Transaciton.
        //initPaymentInfo()导致LineItem的AfterDiscountAmount的值变成与Amount相同
        if (sourceState instanceof SummaryState)
            trans.restoreOriginalLineItemAfterDiscountAmount();
        if (sourceState instanceof InitialState)
            start = true;
        if (sourceState instanceof CheckCashierPasswordState) {
            start = true;
            posTerminal.getCurrentTransaction().setDealType2("0");
            posTerminal.getCurrentTransaction().setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
            //posTerminal.getCurrentTransaction().setTransactionNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber")));
        }

        /*if (sourceState instanceof ReprintState) {
            start = false;
        }*/
        if (posTerminal.getBeginState()) {
            //Bruce/20030324
            String keylockMessage = CreamProperties.getInstance().getProperty("postype");
            if (keylockMessage != null)
                posTerminal.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("TurnKeyToOff(" + keylockMessage + ")"));
            else
                posTerminal.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("TurnKeyToOff(ibm)"));
        }

         if (sourceState instanceof PrintPluState) {
             calcPluQty();
        }
        
    }

    //gllg
    public void calcPluQty() {
        pluQtyMap = new HashMap();
        if (trans != null) {
            Object[] lineItemList;
            lineItemList = trans.getLineItemsArrayLast();
            int tranDetailCount = lineItemList.length;
            LineItem curLineItem;
            for (int i = 0; i < tranDetailCount; i++) {
                curLineItem = (LineItem) lineItemList[i];
                if (curLineItem.getItemNumber() == null)
                    continue;
                PLU plu = PLU.queryByItemNumber(curLineItem.getItemNumber());
                if (plu == null)
                    continue;
                String detailCode = curLineItem.getDetailCode();
                if (!curLineItem.getRemoved()
                        && ("R".equalsIgnoreCase(detailCode) || "S"
                                .equalsIgnoreCase(detailCode) || "I".equalsIgnoreCase(detailCode))) {
                    curLineItem.resetNoMatchedQty();
                    String curPluCode = curLineItem.getPluNumber();
                    int quantity = curLineItem.getQuantity().intValue();
                    if (pluQtyMap.containsKey(curPluCode)) {
                        quantity += ((Integer) pluQtyMap.get(curPluCode))
                                .intValue();
                    }
                    pluQtyMap.put(curPluCode, Integer.valueOf(String
                            .valueOf(quantity)));
                }
            }
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("IdleState exit!");
        //String str = String.valueOf(Transaction.getNextTransactionNumber());
        //POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        //check restart transaction
        posTerminal.setEnabledPopupMenu(false);
        posTerminal.getWarningIndicator().setMessage("");
        if (start) {
            //System.out.println("This is start :" + start);
            //Clear currentTransaction.
            if (!posTerminal.getMessageIndicator().getMessage().equals(""))
                posTerminal.getMessageIndicator().setMessage("");
            posTerminal.getItemList().setVisible(true);
            posTerminal.getPayingPane().setVisible(false);
            trans.Clear();

            //Bruce/2003-12-17
            if (posTerminal.getReturnItemState()) {
                trans.setDealType2("3");
                trans.setDealType3("4");
            } else {
                trans.setDealType2("0");
            }
            trans.setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
            //trans.setTransactionNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber")));
            trans.setSystemDateTime(new Date());
            //clear ItemList
            posTerminal.getItemList().selectFinished();
            posTerminal.getItemList().itemIndex = 0;
            posTerminal.getItemList().repaint();
            //Bruce/20021030/
            ////posTerminal.setTransactionEnd(false);
            start = false;
        }
        if (event.getSource() instanceof DailyReportButton)
            if (!posTerminal.getTransactionEnd())
                return hyi.cream.state.IdleState.class;
            else
                return hyi.cream.state.DailyReportDateState.class;

        if (event.getSource() instanceof MemberIDButton)
            //若已经在交易输入过程中，不允许输入会员编号，发出警讯
            //if (!posTerminal.getTransactionEnd()) {
            //Bruce/2003-06-23 若画面上看不到任何商品（例如可能被更正掉），仍允许输入会员编号。
            if (trans.getDisplayedLineItemsArray().size() != 0) {
                setWarningMessage(res.getString("CannotInputMemberID"));
                return hyi.cream.state.WarningState.class;
            } else {
                return hyi.cream.state.MemberState.class;
            }
        if (event.getSource() instanceof TransactionTypeButton) {
            String type = trans.getTransactionType();
            String first = type.substring(0, 1);
            String second = type.substring(1, 2);
            if ("0".equals(first)) {
                trans.setTransactionType("1" + second);
            } else {
                trans.setTransactionType("0" + second);
            }
            sinkState = IdleState.this;
            return IdleState.class;
        }
        if (event.getSource() instanceof ReprintButton) {
            if (posTerminal.getCurrentTransaction().getTransactionNumber().intValue() != Transaction.getNextTransactionNumber())
                return hyi.cream.state.ReprintState.class;
            if (posTerminal.getCurrentTransaction().getLineItemsArrayLast().length > 0) {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return hyi.cream.state.WarningState.class;
            } else {
                return hyi.cream.state.ReprintState.class;
            }
        }
        if (!indicatorCleared) {
            if (!posTerminal.getMessageIndicator().getMessage().equals(""))
                posTerminal.getMessageIndicator().setMessage("");
            indicatorCleared = true;
        }

        //check BuyerNoButton
        if (event.getSource() instanceof BuyerNoButton)
            if (!posTerminal.getBuyerNumberPrinted()) {
                return hyi.cream.state.BuyerState.class;
            } else {
                setWarningMessage(res.getString("ErrorOfBuyerNo1"));
                return hyi.cream.state.WarningState.class;
            }
        //check InvoiceNoButton
        if (event.getSource() instanceof InvoiceNoButton)
            if (trans.getCurrentLineItem() == null)
                return hyi.cream.state.InvoiceState.class;
            else
                return hyi.cream.state.WarningState.class;
        //check AgeLevelButton
        if (event.getSource() instanceof AgeLevelButton) {
            if (trans == null)
                return hyi.cream.state.IdleState.class;
            if (trans.getLineItemsArrayLast().length == 0)
                return hyi.cream.state.IdleState.class;
            Object li[] = trans.getLineItemsArrayLast();
            LineItem line = null;
            boolean available = true;
            for (int i = 0; i < li.length; i++) {
                line = (LineItem) li[i];
                if (!line.getRemoved()) {
                    available = true;
                    break;
                }
                available = false;
            }

            if (!available) {
                posTerminal.getWarningIndicator().setMessage(res.getString("NoAvailablePlu"));
                return hyi.cream.state.IdleState.class;
            }
            if (!CreamPrinter.getInstance().getHeaderPrinted()) {
                CreamPrinter.getInstance().printHeader();
                CreamPrinter.getInstance().setHeaderPrinted(true);
            }
            Object lineItemArray[] = trans.getLineItemsArrayLast();
            LineItem lineItem = (LineItem) lineItemArray[lineItemArray.length - 1];
            CreamPrinter.getInstance().printLineItem(lineItem);
            return hyi.cream.state.SummaryState.class;
        }
        //  check DaiFuButton
        if (event.getSource() instanceof DaiFuButton)
            if (trans.getCurrentLineItem() == null) {
                return hyi.cream.state.DaiFuIdleState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return hyi.cream.state.WarningState.class;
            }
        if (event.getSource() instanceof PaidOutButton) {
            java.util.Iterator paidOutItem = Reason.queryByreasonCategory("11");
            if (paidOutItem == null)
                return hyi.cream.state.IdleState.class;
            if (trans.getCurrentLineItem() == null) {
                return hyi.cream.state.PaidOutIdleState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return hyi.cream.state.WarningState.class;
            }
        }
        if (event.getSource() instanceof PaidInButton) {
            java.util.Iterator paidInItem = Reason.queryByreasonCategory("10");
            if (paidInItem == null)
                return hyi.cream.state.IdleState.class;
            if (trans.getCurrentLineItem() == null) {
                return hyi.cream.state.PaidInIdleState.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return hyi.cream.state.WarningState.class;
            }
        }
        //check TransactionVoidButton 前笔误打
        if (event.getSource() instanceof TransactionVoidButton) {
            CreamToolkit.logMessage("IdleState | you enter TransactionVoidButton");
            if (trans.getCurrentLineItem() == null) {
                Transaction oldTran = Transaction.queryByTransactionNumber(String.valueOf(Transaction.getNextTransactionNumber() - 1));
                //Bruce/20030324
                Collection fds = DacBase.getExistedFieldList("cashier");
                if (!oldTran.getDealType3().equals("1") && !oldTran.getDealType3().equals("2") && !oldTran.getDealType3().equals("3") && !oldTran.getDealType2().equals("3") && !oldTran.getDealType1().equals("*")) {
                    trans.setDealType3("1");
                    if (!fds.contains("CASRIGHTS")) {
                        return hyi.cream.state.VoidReadyState.class;
                    } else {
                        // modify by lxf 2003.02.13
                        CreamToolkit.logMessage("IdleState | VoidReadyState | start ...");
                        CashierRightsCheckState.setSourceState("IdleState");
                        CashierRightsCheckState.setTargetState("VoidReadyState");
                        return hyi.cream.state.CashierRightsCheckState.class;
//                      return VoidReadyState.class;
                    }
                } else {
                    setWarningMessage(res.getString("ReturnWarning"));
                    return hyi.cream.state.WarningState.class;
                }
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return hyi.cream.state.WarningState.class;
            }
        }
        //check CancelButton 交易取消
        if (event.getSource() instanceof CancelButton) {
            CreamToolkit.logMessage("IdleState | you enter CancelButton");
            //Bruce/20030324
            Collection fds = DacBase.getExistedFieldList("cashier"); //不存在CASRIGHTS就直接让他做
            if (trans.getCurrentLineItem() == null) {
                setWarningMessage(res.getString("InputWrong"));
                return hyi.cream.state.WarningState.class;
            }
            if (!fds.contains("CASRIGHTS")) {
                return hyi.cream.state.CancelState.class;
            } else {
                // modify by lxf 2003.02.13
                CreamToolkit.logMessage("IdleState | CancelState | start ...");
                CashierRightsCheckState.setSourceState("IdleState");
                CashierRightsCheckState.setTargetState("CancelState");
                return hyi.cream.state.CashierRightsCheckState.class;
                //return CancelState.class;
            }
        }
        if (event.getSource() instanceof CheckInOutButton)
            if (trans.getCurrentLineItem() == null && posTerminal.getTransactionEnd()) {
                //return InOutSelectState.class;
                return hyi.cream.state.KeyLock1State.class;
            } else {
                setWarningMessage(res.getString("TransactionNotEnd"));
                return hyi.cream.state.WarningState.class;
            }
        if (event.getSource() instanceof RemoveButton) {
            if (trans.getCurrentLineItem() == null || trans.getCurrentLineItem().getPrinted() || trans.getCurrentLineItem().getRemoved()) {
                setWarningMessage(res.getString("NoVoidPlu"));
                return hyi.cream.state.WarningState.class;
            }
            String type = posTerminal.getCurrentTransaction().getAnnotatedType();
            if (type != null && type.equalsIgnoreCase("P")) {
                setWarningMessage(res.getString("CannotRemovePluInPeiDa"));
                return hyi.cream.state.WarningState.class;
            } else {
                return hyi.cream.state.VoidState.class;
            }
        }
        if (event.getSource() instanceof ClearButton)
            if (posTerminal.getReturnItemState() && posTerminal.getTransactionEnd()) {
                posTerminal.setBuyerNumberPrinted(false);
                CreamPrinter.getInstance().setHeaderPrinted(false);
                posTerminal.getCurrentTransaction().clearLineItem();
                posTerminal.getCurrentTransaction().Clear();
                posTerminal.getPayingPane().setVisible(false);
                posTerminal.getItemList().itemIndex = 0;
                posTerminal.getItemList().repaint();
                posTerminal.getItemList().setVisible(true);
                posTerminal.setTransactionEnd(true);
                posTerminal.setReturnItemState(false);
                return CashierRightsCheckState.getSourceState();
            } else {
                return hyi.cream.state.IdleState.class;
            }
        //Bruce/20030324
        //check OverrideAmountButton
        if (event.getSource() instanceof OverrideAmountButton) {
            LineItem lineItem = trans.getCurrentLineItem();
            if (lineItem != null && !lineItem.getPrinted()) {
                PLU plu = PLU.queryByPluNumber(lineItem.getPluNumber());
                String property = CreamProperties.getInstance().getProperty("CanOverrideAmount");
                if (plu != null && plu.canOverrideAmount() || property != null && property.equalsIgnoreCase("yes")) {
                    // modify by lxf 2003.04.09
                    CreamToolkit.logMessage("IdleState | OverrideAmountState | start ...");
                    CashierRightsCheckState.setSourceState("IdleState");
                    CashierRightsCheckState.setTargetState("OverrideAmountState");
                    return hyi.cream.state.CashierRightsCheckState.class;
//                  return OverrideAmountState.class;
                    //} else if (ReturnCancelSelectState.getInsert()) {
                    //    return OverrideAmountState.class;
                    //} else if (posTerminal.getReturnSaleMode()) {
                    //    return OverrideAmountState.class;
                    //} else if (CreamProperties.getInstance().getProperty("CanOverrideAmount").equalsIgnoreCase("yes")) {
                    //    try{
                    //        Class cTemp = Class.forName("hyi.cream.state.OverrideAmountState");
                    //        Validate.setExitState((State)cTemp.newInstance());
                    //    }catch(InstantiationException e){
                    //    }catch(IllegalAccessException e){
                    //    }catch(ClassNotFoundException e){}
                    //    return Validate.class;
                } else {
                    return hyi.cream.state.WarningState.class;
                }
            } else {
                return hyi.cream.state.WarningState.class;
            }
        }
        
        //ItemDiscount2Button gllg
        if (event.getSource() instanceof ItemDiscount2Button) {
            LineItem lineItem = trans.getCurrentLineItem();
            if (lineItem != null && !lineItem.getPrinted()) {
                PLU plu = PLU.queryByPluNumber(lineItem.getPluNumber());
                String property = CreamProperties.getInstance().getProperty("CanOverrideAmount");
                if (plu != null && plu.canOverrideAmount() || property != null && property.equalsIgnoreCase("yes")) {
                    
                    CreamToolkit.logMessage("IdleState | ItemDiscount2State | start ...");
                    CashierRightsCheckState.setSourceState("IdleState");
                    CashierRightsCheckState.setTargetState("ItemDiscount2State");
                    return hyi.cream.state.CashierRightsCheckState.class;
                } else {
                    return hyi.cream.state.WarningState.class;
                }
            } else {
                return hyi.cream.state.WarningState.class;
            }
        }
       
        
        if (event.getSource() instanceof WeiXiuButton) {
            String type = posTerminal.getCurrentTransaction().getAnnotatedType();
            if (type != null && type.equalsIgnoreCase("P")) {
                setWarningMessage(res.getString("CannotUseWeiXiuInPeiDa"));
                return hyi.cream.state.WarningState.class;
            } else {
                return hyi.cream.state.WeiXiuState.class;
            }
        }
        if (event.getSource() instanceof PeiDaButton) {
            String type = posTerminal.getCurrentTransaction().getAnnotatedType();
            if (type != null && type.equalsIgnoreCase("W")) {
                setWarningMessage(res.getString("PeiDaMessage"));
                return hyi.cream.state.WarningState.class;
            } else {
                return hyi.cream.state.PeiDaState.class;
            }
        }
        if (event.getSource() instanceof Keylock) {
            Keylock k = (Keylock) event.getSource();
            try {
                int kp = k.getKeyPosition();
                posTerminal.setKeyPosition(kp);
                if (CreamToolkit.getExitClass(kp) != null) {
                    if (CreamToolkit.getExitClass(kp).equals(hyi.cream.state.InitialState.class))
                        return hyi.cream.state.IdleState.class;
                    else
                        return CreamToolkit.getExitClass(kp);
                } else {
                    return hyi.cream.state.IdleState.class;
                }
            } catch (JposException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
            /*Keylock k = (Keylock)event.getSource();
            try {
                int kp = k.getKeyPosition();
                int ckp = posTerminal.getKeyPosition();
                if (kp == 3) {
                    posTerminal.setKeyPosition(kp);
                    return KeyLock2State.class;
                } else if (kp == 6) {
                    posTerminal.setKeyPosition(kp);
                    return ConfigState.class;
                } else if (kp == 2) {
                    posTerminal.setKeyPosition(kp);
                    return IdleState.class;
                } else if (kp == 4) {
                    posTerminal.setKeyPosition(kp);
                    return KeyLock1State.class;
                }
            } catch (JposException e) {
                System.out.println(e);
            }*/
        }
        
        /*if (event.getSource() instanceof PageUpButton
            || event.getSource() instanceof PageDownButton) {
            return IdleState.class;
        }*/

        //null  MSR
        if (sinkState == null && event.getSource() instanceof MSR) {
//          Bruce/20030324
//          Because no use now.
//                      Object eventSource = event.getSource();
//                      String cardNumber = null;
//                      String creditCardType = null;
//                      try {
//                          switch (((MSR)eventSource).getTracksToRead()) {
//                          case 1:
//                              cardNumber = new String(((MSR)eventSource).getTrack1Data());
//                              break;
//                          case 2:
//                              cardNumber = new String(((MSR)eventSource).getTrack2Data());
//                              break;
//                          case 3:
//                              cardNumber = new String(((MSR)eventSource).getTrack3Data());
//                              break;
//                          }
//                      } catch (JposException e) {
//                          e.printStackTrace(CreamToolkit.getLogger());
//                      }
//                      int cardNo = Integer.parseInt(cardNumber.substring(0, 4));
//                      if (cardNo >= 0100 && cardNo <= 3199) {
//                          creditCardType = "01";
//                      } else if (cardNo == 4563 || cardNo == 4579) {
//                          creditCardType = "02";
//                      } else if (cardNo >= 4000 && cardNo <= 4999) {
//                          creditCardType = "03";
//                      } else if (cardNo >= 5000 && cardNo <= 5999) {
//                          creditCardType = "05";
//                      } else if ((cardNo >= 3400 && cardNo <= 3499)
//                                  || cardNo >= 3700 && cardNo <= 3799) {
//                          creditCardType = "06";
//                      } else if (cardNo == 1800 || cardNo == 2131
//                                 || (cardNo >= 3528 && cardNo <= 3589)) {
//                          creditCardType = "08";
//                      } else if (cardNo == 1040 || cardNo == 2030
//                                 || (cardNo >= 3000 && cardNo <= 3099)
//                                 || (cardNo >= 3600 && cardNo <= 3699)
//                                 || (cardNo >= 3800 && cardNo <= 3899)) {
//                          creditCardType = "07";
//                      } else if (cardNo >= 0 || cardNo <= 9) {
//                          creditCardType = "99";
//                      }
        }
        
        //KeylockWarningState   Keylock
        if (sinkState != null)
            return sinkState.getClass();
        else
            return null;
    }

        static public Map getPluQtyMap() {
        return pluQtyMap;
    }

    //gllg
    public static void setPluQtyMap(String pluCode, Integer qty) {
        pluQtyMap.put(pluCode, qty);
    }
}

/*******************************************************************************
 * DECOMPILATION REPORT ***
 * 
 * DECOMPILED FROM: D:\workspace7\CStorePOS\classes\cream.jar
 * 
 * 
 * TOTAL TIME: 94 ms
 * 
 * 
 * JAD REPORTED MESSAGES/ERRORS:
 * 
 * 
 * EXIT STATUS: 0
 * 
 * 
 * CAUGHT EXCEPTIONS:
 * 
 ******************************************************************************/
