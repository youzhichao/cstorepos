/**
 * State class
 * @since 2000
 * @author slackware
 */
 
package hyi.cream.state;

//for event processing
import java.util.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

public class InitialState extends State {
    static InitialState initialState = null;

    public static InitialState getInstance() {
        try {
            if (initialState == null) {
                initialState = new InitialState();
            }
        } catch (InstantiationException ex) {
        }
        return initialState;
    }

    /**
     * Constructor
     */
    public InitialState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
		//System.out.println("This is InitialState's Entry!");
		//POSPeripheralHome posHome = POSPeripheralHome.getInstance();

		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();

		posTerminal.setBuyerNumberPrinted(false);
        posTerminal.setTransactionEnd(true);
		CreamPrinter.getInstance().setHeaderPrinted(false);
		if (event == null && sourceState == null) {
			//PayingPane payingPane  = posTerminal.getPayingPane();
			//payingPane.setBounds(0, 0, 620, 342);
			//posTerminal.getButtonPanel().showLayer(2);
			return;
		}
        Object eventSource = null;
        if (event != null) {
            eventSource = event.getSource();
		}

        if (sourceState == null
            || sourceState instanceof DrawerOpenState) {
            posTerminal.setChecked(false);
        }
		/*
		if (sourceState instanceof DrawerOpenState
			|| sourceState instanceof ShiftState
			|| sourceState instanceof ZState
			|| eventSource instanceof EnterButton) {
			//posTerminal.getMessageIndicator().setMessage("");
			//posTerminal.getWarningIndicator().setMessage("");
			//posTerminal.showIndicator("message");
			//posTerminal.showIndicator("warning");
			//posTerminal.getButtonPanel().showLayer(2);
		}*/
		if (sourceState instanceof CancelState
			&& event.getSource() instanceof EnterButton) {
			//Clear currentTransaction.
			posTerminal.getCurrentTransaction().Clear();
			//posTerminal.getCurrentTransaction().setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
			//posTerminal.getCurrentTransaction().setTransactionNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber")));
			posTerminal.getPayingPane().setVisible(false);
			posTerminal.getItemList().itemIndex = 0;
			posTerminal.getItemList().repaint();
			posTerminal.getItemList().setVisible(true);
		}

        if (sourceState instanceof KeyLock2State) {       
			posTerminal.getItemList().setVisible(true);
			posTerminal.getPayingPane().setVisible(false);
			posTerminal.getItemList().itemIndex = 0;
			posTerminal.getItemList().repaint();
			posTerminal.getCurrentTransaction().Clear();
			//posTerminal.getCurrentTransaction().setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
			//posTerminal.getCurrentTransaction().setTransactionNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber")));
		}
		//hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.EnterButton
	}

	public Class exit(EventObject event, State sinkState) {
		//System.out.println("This is InitialState's Exit!");
		//If the CashierNumber in CreamProperties is an empty string,
		//return CashierNumberingState.class, otherwise return IdleState.class.

		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
		if (event == null && sinkState == null) {

			if (CreamProperties.getInstance().getProperty("CashierNumber").equals("")
			   || ShiftReport.getCurrentShift() == null) {
				if (!posTerminal.getTrainingMode()) {
					posTerminal.getCurrentTransaction().setDealType2("8");
					return CashierState.class;
				}
			} else
		  		return IdleState.class;
		}
        if (sinkState != null)
            return sinkState.getClass();
        else
            return null;
	}
}