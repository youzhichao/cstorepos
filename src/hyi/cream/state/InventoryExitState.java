package hyi.cream.state;

import java.util.*;
import java.text.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * 离开盘点State. 结束时打印盘点总零售金额小计.
 */
public class InventoryExitState extends State {
    private static InventoryExitState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Indicator messageIndicator = app.getMessageIndicator();
    private ResourceBundle res = CreamToolkit.GetResource();
    private String inputNumber = "";

    public static InventoryExitState getInstance() {
        try {
            if (instance == null) {
                instance = new InventoryExitState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public InventoryExitState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            inputNumber += pb.getNumberLabel();
            app.getMessageIndicator().setMessage(inputNumber.toString());
            return;
        }
        if (inputNumber.length() == 0)
            messageIndicator.setMessage(res.getString("InventoryExitConfirm"));
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            if (inputNumber.toString().equals("1")) {
                inputNumber = "";
                app.getMessageIndicator().setMessage("");

                // 打印盘点小计
                CreamPrinter.getInstance().printInventoryFooter();

                app.getDacViewer().clearDacList();
                app.getDacViewer().setVisible(false);
                app.getItemList().setVisible(true);

                return InventoryState.getOriginalState();
            } else {
                inputNumber = "";
                return InventoryExitState.class;
            }
        } else if (event.getSource() instanceof ClearButton) {
            inputNumber = "";
        }
        return sinkState.getClass();
    }
}
