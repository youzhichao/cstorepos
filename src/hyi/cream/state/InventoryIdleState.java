package hyi.cream.state;

import java.util.*;
import java.math.BigDecimal;
import java.text.*;

import jpos.JposException;
import jpos.Scanner;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * 输入盘点数据idle state.
 */
public class InventoryIdleState extends State {
    private BigDecimal weightAmount;
    private BigDecimal weightQuantity;
    private static InventoryIdleState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Indicator messageIndicator = app.getMessageIndicator();
    private ResourceBundle res = CreamToolkit.GetResource();
    private static StringBuffer itemNumber = new StringBuffer();
    private static Inventory currentInventory;

    public static InventoryIdleState getInstance() {
        try {
            if (instance == null) {
                instance = new InventoryIdleState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public InventoryIdleState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (itemNumber.length() == 0)
            messageIndicator.setMessage(res.getString("PleaseInputInventoryItem"));

    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            itemNumber.append(pb.getNumberLabel());
            messageIndicator.setMessage(itemNumber.toString());
            return sinkState.getClass();
        }

        if (event.getSource() instanceof ClearButton) {
            if (itemNumber.length() != 0) {
                itemNumber.setLength(0);
                return InventoryIdleState.class;
            } else {
                return InventoryExitState.class;
            }
        }

        if (event.getSource() instanceof RemoveButton) {
            List dacList = app.getDacViewer().getDacList();
            if (itemNumber.length() == 0 && dacList.size() > 0) { // 立即更正
                currentInventory = (Inventory)((Inventory)dacList.get(dacList.size() - 1)).clone();
                currentInventory.setItemSequence(Inventory.queryMaxItemSequence() + 1);
                currentInventory.setActStockQty(currentInventory.getActStockQty().negate());
                currentInventory.setAmount(currentInventory.getAmount().negate()); 
                app.getDacViewer().addDacToDacList(currentInventory);
                return InventoryStoreState.class;
            } else {    // 指定更正
                for (int i = dacList.size() - 1; i >= 0; i--) {
                    Inventory inv = (Inventory)dacList.get(i);
                    if (inv.getItemSequence().equals(new Integer(itemNumber.toString()))) {
                        currentInventory = (Inventory)inv.clone();
                        currentInventory.setItemSequence(Inventory.queryMaxItemSequence() + 1);
                        currentInventory.setActStockQty(currentInventory.getActStockQty().negate());
                        currentInventory.setAmount(currentInventory.getAmount().negate()); 
                        app.getDacViewer().addDacToDacList(currentInventory);
                        return InventoryStoreState.class;
                    }
                }
            }
            return InventoryIdleState.class;
        }

        PLU plu = null;
        boolean isWeighted = false;
        if (event.getSource() instanceof Scanner) {
            try {
                ((Scanner)event.getSource()).setDecodeData(true);
                String number = new String(((Scanner)event.getSource()).getScanDataLabel());
                plu = PLU.queryBarCode(number);
                if (plu == null)
                    plu = PLU.queryByInStoreCode(number);
                if (plu == null) {
                    plu = processScaleBarCode(number);
                    if (plu != null)
                        isWeighted = true;
                }
            } catch (JposException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        } else if (event.getSource() instanceof EnterButton
            || event.getSource() instanceof InStoreCodeButton
            || event.getSource() instanceof BarCodeButton) {
            plu = PLU.queryBarCode(itemNumber.toString());
            if (plu == null)
                plu = PLU.queryByInStoreCode(itemNumber.toString());
            if (plu == null) {
                plu = processScaleBarCode(itemNumber.toString());
                if (plu != null)
                    isWeighted = true;
            }
        }

        if (plu == null) {
            itemNumber.setLength(0);
            return InventoryIdleState.class;
        }
        try {
            currentInventory = new Inventory();
            currentInventory.setStoreID(app.getSystemInfo().getStoreNumber());
            currentInventory.setBusiDate(getBusinessDate());
            currentInventory.setPOSNumber(Integer.parseInt(app.getSystemInfo().getTerminalID()));
            currentInventory.setItemSequence(Inventory.queryMaxItemSequence() + 1);
            currentInventory.setStockNumber(InventoryStockState.getStockNumber());
            currentInventory.setItemNumber(plu.getItemNumber());
            currentInventory.setDescription(plu.getScreenName());
            currentInventory.setUnitPrice(plu.getUnitPrice());
            currentInventory.setUploadState("0");
            currentInventory.setUpdateUserID(InventoryState.getCashierNumber());
            currentInventory.setUpdateDateTime(new Date());
            if (isWeighted) {
                currentInventory.setActStockQty(this.weightQuantity);
                currentInventory.setAmount(this.weightAmount);
            } else {
                currentInventory.setActStockQty(new BigDecimal(0));
                currentInventory.setAmount(new BigDecimal(0));
            }

            app.getDacViewer().addDacToDacList(currentInventory);

            if (isWeighted)
                return InventoryStoreState.class;
            else if (plu.isOpenPrice())
                return InventoryOpenPriceState.class;
            else
                return InventoryQuantityState.class;
            
        } catch (InstantiationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
            return InventoryIdleState.class;
        }
    }

    public static void clearItemNumber() {
        itemNumber.setLength(0);
    }

    private Date getBusinessDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    public static Inventory getCurrentInventory() {
        return currentInventory;
    }

    public PLU processScaleBarCode(String number) {
        try {
            java.util.List formatList =  app.getScaleBarCodeFormat();
            if (formatList == null || formatList.isEmpty())
                return null;
            int totalLength = ((Integer) formatList.get(0)).intValue();
            if (number.length() != totalLength)
                return null;
            String pref = (String) formatList.get(1);
            if (!number.startsWith(pref)) {
                return null;
            }
            int iStart = ((Integer) formatList.get(2)).intValue();
            int iEnd   = ((Integer) formatList.get(3)).intValue();
            int wStart = ((Integer) formatList.get(4)).intValue();
            int wEnd = ((Integer) formatList.get(5)).intValue();
            int wComma = ((Integer) formatList.get(6)).intValue();
            int pStart = ((Integer) formatList.get(7)).intValue();
            int pEnd = ((Integer) formatList.get(8)).intValue();
            int pComma = ((Integer) formatList.get(9)).intValue();
            String itemNo = number.substring(iStart, iEnd);
            String weight = number.substring(wStart, wEnd);
            weight = weight.substring(0, wComma) + "." 
                + weight.substring(wComma, weight.length());
            String price = number.substring(pStart, pEnd);
            price = price.substring(0, pComma) + "." 
                + price.substring(pComma, price.length());
            BigDecimal wCount = new BigDecimal(weight);
            if (wCount.scale() > 4)
                wCount = wCount.setScale(4, BigDecimal.ROUND_HALF_UP);
            weightQuantity = wCount;
            BigDecimal pCount = new BigDecimal(price);
            weightAmount = pCount.setScale(2, BigDecimal.ROUND_HALF_UP);
            
            return PLU.queryByInStoreCode(itemNo);
        } catch (Exception e) {
            return null;
        }
    }
}
