package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;
import java.util.*;
import java.text.*;

/**
 * 盘点初始化State.
 * 
 * @author Bruce
 */
public class InventoryInitialState extends State {
    private static InventoryInitialState inventoryInitiState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private String inputNumber = "";
    private Class originalState = null;
   
    public static InventoryInitialState getInstance() {
        try {
            if (inventoryInitiState == null) {
                inventoryInitiState = new InventoryInitialState();
            }
        } catch (InstantiationException e) {
        }
        return inventoryInitiState;
    }

    public InventoryInitialState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            inputNumber = inputNumber + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(inputNumber);
            return;
        } else if (sourceState.getClass().getName().indexOf("KeyLock") != -1) {
            originalState = sourceState.getClass();
        }
        
        if (inputNumber.length() == 0)
            app.getMessageIndicator().setMessage(res.getString("InventoryInitConfirm"));
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            if (inputNumber.equals("1")) {
                try {
                    if (new Inventory().deleteAll()) {
                        //remember the last initializing date
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        String today = df.format(new java.util.Date());
                        CreamProperties.getInstance().setProperty("InventoryInitDate", today);
                        CreamProperties.getInstance().deposit();
                        CreamToolkit.logMessage("Inventory data has been initialized.");
                        app.getMessageIndicator().setMessage(res.getString("InventoryInitSuccess"));
                    } else {
                        app.getMessageIndicator().setMessage(res.getString("InventoryInitFailed"));
                    }
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                } catch (InstantiationException ex) {
                }
                inputNumber = "";
                return originalState;
            } else {
                inputNumber = "";
                return InventoryInitialState.class;
            }
        } else if (event.getSource() instanceof ClearButton) {
            inputNumber = "";
            return originalState;
        }
        
        return sinkState.getClass();
    }
}
