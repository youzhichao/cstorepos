package hyi.cream.state;

import java.util.*;
import java.math.BigDecimal;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

/**
 * 盘点数量输入State.
 */
public class InventoryQuantityState extends State {
    private static InventoryQuantityState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Indicator messageIndicator = app.getMessageIndicator();
    private Indicator warningIndicator = app.getWarningIndicator();
    private ResourceBundle res = CreamToolkit.GetResource();
    private static StringBuffer quantity = new StringBuffer();

    public static InventoryQuantityState getInstance() {
        try {
            if (instance == null) {
                instance = new InventoryQuantityState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public InventoryQuantityState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (quantity.length() == 0)
            messageIndicator.setMessage(res.getString("PleaseInputInventoryItemQuantity"));
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            quantity.append(pb.getNumberLabel());
            messageIndicator.setMessage(quantity.toString());
            warningIndicator.setMessage("");
            return sinkState.getClass();
        }
        if (event.getSource() instanceof ClearButton) {
            quantity.setLength(0);
            warningIndicator.setMessage("");
            return sinkState.getClass();
        }
        if (event.getSource() instanceof EnterButton
            || event.getSource() instanceof QuantityButton) {
            if (quantity.length() != 0) {
                try {
                    Inventory inv = InventoryIdleState.getCurrentInventory();
                    BigDecimal qty = new BigDecimal(quantity.toString());
                    if (qty.scale() > 4)
                        qty = qty.setScale(4, BigDecimal.ROUND_HALF_UP);
                    inv.setActStockQty(qty);
                    BigDecimal amt = qty.multiply(inv.getUnitPrice());
                    if (amt.scale() > 4)
                        amt = amt.setScale(4, BigDecimal.ROUND_HALF_UP);
                    inv.setAmount(amt);
                    app.getDacViewer().repaint();
                    warningIndicator.setMessage("");
                    return InventoryStoreState.class;
                } catch (NumberFormatException e) {
                    quantity.setLength(0);
                    warningIndicator.setMessage(res.getString("InputWrong"));
                }
            }
            return InventoryQuantityState.class;
        }
        return sinkState.getClass();
    }

    public static void clearQuantity() {
        quantity.setLength(0);
    }
}
