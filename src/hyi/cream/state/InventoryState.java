package hyi.cream.state;

import java.util.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

/**
 * 输入盘点数据进入点。此State负责搜集盘点人员编号.
 */
public class InventoryState extends State {
    private static InventoryState inventoryState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private static StringBuffer cashierNumber = new StringBuffer();
    private static Class originalState = null;
    
    public static InventoryState getInstance() {
        try {
            if (inventoryState == null) {
                inventoryState = new InventoryState();
            }
        } catch (InstantiationException e) {
        }
        return inventoryState;
    }

    public InventoryState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //Bruce/2003-12-01
        //修改因为没有归零，导致第二次盘点时数据无法显示
        app.getDacViewer().setCurrentIdx(0);

        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            cashierNumber.append(pb.getNumberLabel());
            app.getMessageIndicator().setMessage(cashierNumber.toString());
            return;
        } else if (sourceState.getClass().getName().indexOf("KeyLock") != -1) {
            setOriginalState(sourceState.getClass());
            clearCashierNumber();
        }
    
        if (cashierNumber.length() == 0) {
            app.getMessageIndicator().setMessage(res.getString("InputInventoryPerson"));
        } else {
            app.getWarningIndicator().setMessage("");
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            Cashier cashier = Cashier.queryByCashierID(cashierNumber.toString());
            if (cashier == null) {
                cashierNumber.setLength(0);
                app.getWarningIndicator().setMessage(res.getString("InventoryPersonNotFound"));
                return InventoryState.class;
            }
            return InventoryStockState.class;

        } else if (event.getSource() instanceof ClearButton) {
            if (cashierNumber.length() == 0) {
                return getOriginalState();
            } else {
                cashierNumber.setLength(0);
                return InventoryState.class;
            }
        }

        if (cashierNumber.length() == 0) {
            app.getWarningIndicator().setMessage("");
        }
        return sinkState.getClass();
    }

    public static String getCashierNumber() {
        return cashierNumber.toString();
    }

    public static void clearCashierNumber() {
        InventoryState.cashierNumber.setLength(0);
    }

    public static Class getOriginalState() {
        return originalState;
    }

    public static void setOriginalState(Class originalState) {
        InventoryState.originalState = originalState;
    }
}
