package hyi.cream.state;

import java.util.*;
import java.text.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * 输入盘点仓位编号State. 离开时打印盘点表头.
 */
public class InventoryStockState extends State {
    private static InventoryStockState inventoryState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private static StringBuffer stockNumber = new StringBuffer();

    public static InventoryStockState getInstance() {
        try {
            if (inventoryState == null) {
                inventoryState = new InventoryStockState();
            }
        } catch (InstantiationException e) {
        }
        return inventoryState;
    }

    public InventoryStockState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            stockNumber.append(pb.getNumberLabel());
            app.getMessageIndicator().setMessage(stockNumber.toString());
            return;
        } else {
            clearStockNumber();
        }
        if (stockNumber.length() == 0) {
            app.getMessageIndicator().setMessage(res.getString("InputInventoryStockNumber"));
        } else {
            app.getWarningIndicator().setMessage("");
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            if (stockNumber.length() > 3) {
                stockNumber.setLength(0);
                app.getWarningIndicator().setMessage(res.getString("InventoryStockNumberError"));
                return InventoryStockState.class;
            }

            // 打印盘点表头（包括：门市编号名称、盘点时间、仓位编号、盘点人员编号）
            CreamPrinter.getInstance().printInventoryHeader();

            DacViewer dv = app.getDacViewer();
            dv.setDispalyFieldList(new String[] {
                "ItemSequence", "ItemNumber", "Description", "UnitPrice", "ActStockQty", "Amount"});
            dv.setDispalyFieldNameList(new String[] {
                "项次", "货号", "商品名", "单价", "数量", "金额"});
            dv.setDispalyFieldWidthList(new int[] {
                8, 11, 43, 11, 13, 14});
            dv.setBackLabel(res.getString("BigInventoryLabel"));

            app.getItemList().setVisible(false);
            dv.setVisible(true);

            app.getWarningIndicator().setMessage("");
            app.getMessageIndicator().setMessage("");
            return InventoryIdleState.class;

        } else if (event.getSource() instanceof ClearButton) {
            if (stockNumber.length() == 0) {
                InventoryState.clearCashierNumber();
                return InventoryState.class;
            } else {
                stockNumber.setLength(0);
                return InventoryStockState.class;
            }
        }

        if (stockNumber.length() == 0) {
            app.getWarningIndicator().setMessage("");
        }
        return sinkState.getClass();
    }

    public static String getStockNumber() {
        return stockNumber.toString();
    }

    public static void clearStockNumber() {
        stockNumber.setLength(0);
    }
}
