package hyi.cream.state;

import java.util.*;
import java.text.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * 盘点记录储存和打印State.
 */
public class InventoryStoreState extends State {
    private static InventoryStoreState instance = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Indicator warningIndicator = app.getWarningIndicator();
    private ResourceBundle res = CreamToolkit.GetResource();

    public static InventoryStoreState getInstance() {
        try {
            if (instance == null) {
                instance = new InventoryStoreState();
            }
        } catch (InstantiationException e) {
        }
        return instance;
    }

    public InventoryStoreState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        Inventory inv = InventoryIdleState.getCurrentInventory();
        if (!inv.insert(false)) {
            warningIndicator.setMessage(res.getString("InventoryDataStoreFailed"));
            CreamToolkit.logMessage("Err> Store inventory record failed! " + inv.toString());
        }

        // 打印盘点记录
        CreamPrinter.getInstance().printInventoryItem(inv);

        //Bruce/2003-12-02
        //将显示会用到的DAC fields写到class fields，并将原DAC中的field map丢弃，
        //以节约memory overhead
        inv.discardFieldMap();
    }

    public Class exit(EventObject event, State sinkState) {
        InventoryIdleState.clearItemNumber();
        InventoryQuantityState.clearQuantity();
        InventoryOpenPriceState.clearPrice();
        return sinkState.getClass();
    }
}
