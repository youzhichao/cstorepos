package hyi.cream.state;

import java.util.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;
import hyi.cream.inline.*;


public class InventoryUploadState extends State { //implements SendObjectCallBack {
    private static InventoryUploadState InventoryUploadState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private String inputNumber = "";
    private Class originalState = null;

    public static InventoryUploadState getInstance() {
        try {
            if (InventoryUploadState == null) {
                InventoryUploadState = new InventoryUploadState();
            }
        } catch (InstantiationException ex) {
        }
        return InventoryUploadState;
    }

    public InventoryUploadState() throws InstantiationException {
    }

    //public void callBack(String message) {
    //    app.getMessageIndicator().setMessage("盘点数据发送中，请稍侯... 传送笔数：" + message);
    //}

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            inputNumber = inputNumber + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(inputNumber);
            return;
        } else if (sourceState.getClass().getName().indexOf("KeyLock") != -1) {
            originalState = sourceState.getClass();
        }

        if (inputNumber.length() == 0)
            app.getMessageIndicator().setMessage("选择(1):上传未上传盘点数据 (2):重新上传所有盘点数据");
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            if (inputNumber.equals("1") || inputNumber.equals("2")) {
                app.getMessageIndicator().setMessage("盘点数据发送中，请稍侯...");
                try {
                    //Client.getInstance().setSendObjectCallBack(this);
                    if (inputNumber.equals("1"))
                        Client.getInstance().processCommand("putInventory");
                    else
                        Client.getInstance().processCommand("putAllInventory");
                    app.getMessageIndicator().setMessage("盘点数据发送成功。");
                } catch (ClientCommandException e) {
                    app.getMessageIndicator().setMessage("盘点数据发送失败，请呼唤电脑人员处理。");
                }
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e2) {
                }
                //finally {
                //    Client.getInstance().setSendObjectCallBack(null);
                //}
                inputNumber = "";
                return originalState;
            } else {
                inputNumber = "";
                return InventoryUploadState.class;
            }
        }
        if (event.getSource() instanceof ClearButton) {
            return originalState;
        }
        return sinkState.getClass();
    }
}
