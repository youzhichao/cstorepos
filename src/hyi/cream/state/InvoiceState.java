
// Copyright (c) 2000 HYI
package hyi.cream.state;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class InvoiceState extends SomeAGReadyState {
    static InvoiceState invoiceState = null;

    public static InvoiceState getInstance() {
        try {
            if (invoiceState == null) {
                invoiceState = new InvoiceState();
            }
        } catch (InstantiationException ex) {
        }
        return invoiceState;
    }

    /**
     * Constructor
     */
    public InvoiceState() throws InstantiationException {
    }
    
    public String getPromptedMessage() {
        return "Please input invoice number :";
    }
}

 