package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;
import java.math.*;

/**
 * A Class class.
 * <P>
 * @author gllg
 */
public class ItemDiscount2State extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private int priceType;
    private String tempPrice = "";
    private BigDecimal realPrice;
    private Transaction trans = app.getCurrentTransaction();
	static ItemDiscount2State itemDiscount2State = null;
    private LineItem curLineItem = null;
    private boolean computeDiscountRate;
    private BigDecimal bigDecimal0 = new BigDecimal("0.00");

    public static ItemDiscount2State getInstance() {
        try {
            if (itemDiscount2State == null) {
                itemDiscount2State = new ItemDiscount2State();
            }
        } catch (InstantiationException ex) {
        }
        return itemDiscount2State;
    }
	
	public ItemDiscount2State() throws InstantiationException {
        computeDiscountRate = CreamProperties.getInstance().getProperty("ComputeDiscountRate", "no")
            .equalsIgnoreCase("yes");
    }
	
	public void entry(EventObject event, State sourceState) {
		curLineItem = trans.getCurrentLineItem();
        priceType = ItemDiscount2Button.getPriceType();
        if (sourceState instanceof CashierRightsCheckState)
			app.getMessageIndicator().setMessage("请输入价格，按[确认]键结束");
	}
	
	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof NumberButton) {
            //System.out.println("curLineItem.isMMPLU():"+curLineItem.isMMPLU() );
			NumberButton pb = (NumberButton)event.getSource();
                //System.out.println(" OverrideAmountState exit number" + pb.getNumberLabel());
			if (pb.getNumberLabel().equals("-") || (pb.getNumberLabel().equals(".") && tempPrice.length() == 0)) {
                //System.out.println(" OverrideAmountState exit number 1 " +tempPrice);
			} else { 
				tempPrice = tempPrice + pb.getNumberLabel();
                //System.out.println(" OverrideAmountState exit number 2" +tempPrice);
			}
			app.getMessageIndicator().setMessage(tempPrice);
			return ItemDiscount2State.class;
		}

        if (event.getSource() instanceof ClearButton) {
            if (tempPrice.length() == 0)
                return IdleState.class;
            tempPrice = "";
            app.getMessageIndicator().setMessage(tempPrice);
            return ItemDiscount2State.class;
        }

		if (event.getSource() instanceof EnterButton) {

            System.out.println("tempPrice:"+tempPrice);
            
            // Check number format  & upper limit & DaiShou   gllg
            try {
                if(curLineItem.getPrinted()){
                    app.getMessageIndicator().setMessage("商品已经打印不能变价，请按[取消]键结束");
                    tempPrice = "";
                    return ItemDiscount2State.class;
                }

                if("M".equals(curLineItem.getDiscountType())){
                    app.getMessageIndicator().setMessage("组合促销商品不能变价，请按[取消]键结束");
                    tempPrice = "";
                    return ItemDiscount2State.class;
                }
                
                
                realPrice = new BigDecimal(tempPrice);
                
                //Bruce/20030826/
                //检查是否为一个很大或很小的数
                if (realPrice.compareTo(new BigDecimal("999999.99")) > 0
                    || realPrice.compareTo(new BigDecimal("-999999.99")) < 0)
                    throw new NumberFormatException("");

                if (realPrice.compareTo(bigDecimal0) <= 0){
                    app.getMessageIndicator().setMessage("价格不能小于等于零，请重新输入价格");
                    tempPrice = "";
                    return ItemDiscount2State.class;
                }
                
                if (realPrice.compareTo(curLineItem.getOriginalPrice()) > 0) {
                    app.getMessageIndicator().setMessage("价格不能大于原始售价，请重新输入价格");
                    tempPrice = "";
                    return ItemDiscount2State.class;
                }
                
                //Need another state to check this out, put it here temporarily.
                //Check is DaiShou Item or not   gllg
                //System.out.println("DaiShou:" + curLineItem.getDaishouDetailNumber());
                if (curLineItem.getDaishouDetailNumber() != 0){
                    app.getMessageIndicator().setMessage("代售商品不能变价，请按[取消]键结束");
                    tempPrice = "";
                    return ItemDiscount2State.class;
                }
                
            } catch (NumberFormatException e) {
                app.getMessageIndicator().setMessage("请输入价格，按[确认]键结束");
                tempPrice = "";
                return ItemDiscount2State.class;
            }
            
            if (priceType == 0)
                curLineItem.setAfterItemDsctUnitPrice(realPrice);
            else
                curLineItem.setAfterItemDsctUnitPrice(curLineItem.getOriginalPrice().add(realPrice.negate()));
            
            System.out.println("PriceType:"+    ItemDiscount2Button.getPriceType()  );
            
            //Need to remain the original quantity which usr input just now   gllg
            curLineItem.setQuantity(new BigDecimal("1.00"));
			curLineItem.setAmount(curLineItem.getUnitPrice().multiply(curLineItem.getQuantity()));
            //curLineItem.setAfterDiscountAmount(curLineItem.getAmount());
            curLineItem.setAfterDiscountAmount(
                    curLineItem.getAfterItemDsctUnitPrice().multiply(curLineItem.getQuantity())
            );
            System.out.println("curLineItem.getAfterDiscountAmount():"+curLineItem.getAfterDiscountAmount());
            System.out.println("curLineItem.getUnitPrice():"+curLineItem.getUnitPrice());
            System.out.println("curLineItem.getUnitPrice1():"+curLineItem.getAfterItemDsctUnitPrice());
            System.out.println("curLineItem.getAmount():"+curLineItem.getAmount());
            System.out.println("curLineItem.getQuantity():"+curLineItem.getQuantity());
            System.out.println("curLineItem.getDetailCode():"+curLineItem.getDetailCode());
            curLineItem.setAddRebateAmount(curLineItem.getAfterDiscountAmount().multiply(curLineItem.getRebateRate()));
            
            
            
            TaxType taxType = TaxType.queryByTaxID(curLineItem.getTaxType());
            // calculate tax
            int round = 0;
            String taxRound = taxType.getRound();
            if (taxRound.equalsIgnoreCase("R")) {
                round = BigDecimal.ROUND_HALF_UP;
            } else if (taxRound.equalsIgnoreCase("C")) {
                round = BigDecimal.ROUND_UP;
            } else if (taxRound.equalsIgnoreCase("D")) {
                round = BigDecimal.ROUND_DOWN;
            }
            String taxTp = taxType.getType();
            BigDecimal taxPercent = taxType.getPercent();
            BigDecimal taxAmount = new BigDecimal(0);
            int taxDigit = taxType.getDecimalDigit().intValue();
            if (taxTp.equalsIgnoreCase("1")) {
                BigDecimal unit = new BigDecimal("1");
                //taxAmount = ((curLineItem.getAmount().multiply(taxPercent)).divide(
                //gllg
                taxAmount = ((curLineItem.getAfterDiscountAmount().multiply(taxPercent)).divide(
                            unit.add(taxPercent),2,round));
            } else if (taxTp.equalsIgnoreCase("2")) {
                //BigDecimal unit = new BigDecimal("1");
//                taxAmount = (curLineItem.getAmount().multiply(taxPercent)).setScale(taxDigit, round);
                taxAmount = (curLineItem.getAfterDiscountAmount().multiply(taxPercent)).setScale(taxDigit, round);
            } else {
                BigDecimal tax = new BigDecimal("0.00");
                taxAmount = tax.setScale(taxDigit, round);
            }
            //set tax
            curLineItem.setAfterSIAmount(curLineItem.getAmount());
            curLineItem.setTaxAmount(taxAmount);
            //curLineItem.setDiscountType("O");
            curLineItem.setDiscountType("C");
            //curLineItem.setDetailCode("C");
            
//            //Bruce/2003-08-08
//            if (computeDiscountRate) {
//                try {
//                    BigDecimal rate = curLineItem.getAfterItemDsctUnitPrice().divide(curLineItem.getOriginalPrice(),
//                        2, BigDecimal.ROUND_HALF_UP);
//                    curLineItem.setDiscountRate(rate);
//                    DiscountType dis = DiscountType.queryByDiscountRate(rate);
//                    if (dis != null) {
//                        curLineItem.setDiscountRateID(dis.getID());
//                    }
//                } catch (ArithmeticException e) {
//                //初始价格为0，不设置折扣信息 zhaohong 2003-10-30     
//                }
//            }
                
            try {
                app.getCurrentTransaction().changeLineItem(-1, curLineItem);
            } catch (LineItemNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("LineItem not found at " + this);
            }
            CreamToolkit.showText(app.getCurrentTransaction(), 0);
			tempPrice = "";
			return IdleState.class;
		}
	
		return ItemDiscount2State.class;
	}
}