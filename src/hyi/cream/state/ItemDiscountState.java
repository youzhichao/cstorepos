
package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class ItemDiscountState extends State {
    static ItemDiscountState itemDiscountState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Transaction trans = app.getCurrentTransaction();   
    private ResourceBundle res = CreamToolkit.GetResource();
    private String discountID = "";
    private BigDecimal discountUnitPrice = new BigDecimal(0);
    private Class exitState = null;
    private SI itemDiscount = null;
    private BigDecimal discountQuantity = new BigDecimal("0");
    private LineItem curLineItem = null;

    public static ItemDiscountState getInstance() {
        try {
            if (itemDiscountState == null) {
                itemDiscountState = new ItemDiscountState();
            }
        } catch (InstantiationException ex) {
        }
        return itemDiscountState;
    }

    /**
     * Constructor
     */
    public ItemDiscountState() throws InstantiationException {
    }                        

    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof IdleState) {
            curLineItem = trans.getCurrentLineItem();
            if (curLineItem == null &&
                !curLineItem.getDetailCode().equalsIgnoreCase("R")) {
                setWarningMessage(res.getString("ItemDiscount"));
                exitState = WarningState.class;
                return;
            }
            discountID = ((ItemDiscountButton)event.getSource()).getID();
            itemDiscount = SI.queryBySIID(discountID);
            discountQuantity = new BigDecimal("1.00");
        } else if (sourceState instanceof NumberingState) {
            curLineItem = trans.getCurrentLineItem();
            if (curLineItem == null &&
                !curLineItem.getDetailCode().equalsIgnoreCase("R")) {
                setWarningMessage(res.getString("ItemDiscount"));
                exitState = WarningState.class;
                return;
            }
            discountID = ((ItemDiscountButton)event.getSource()).getID();
            itemDiscount = SI.queryBySIID(discountID);
            discountQuantity = new BigDecimal(((NumberingState)sourceState).getNumberString());
        }
        try {
            LineItem discountLineItem = new LineItem();
            discountLineItem.setDescription(itemDiscount.getScreenName());
            discountLineItem.setDetailCode("T");
            discountLineItem.setQuantity(discountQuantity.negate());
            discountLineItem.setTerminalNumber(trans.getTerminalNumber());
            discountLineItem.setTransactionNumber(trans.getTransactionNumber());
            discountLineItem.setPrinted(false);
			discountLineItem.setRemoved(false);
			discountLineItem.setTaxType(curLineItem.getTaxType());
            discountLineItem.setPluNumber(discountID);
            discountLineItem.setLineItemSequence((trans.getLineItemsArrayLast()).length + 1);
            String discountType = itemDiscount.getType();
            BigDecimal discountValue = itemDiscount.getValue();
            //String discountBase = itemDiscount.getBase();
            String discountAttr = itemDiscount.getAttribute();
            int round = 0;
            String discountRound = discountAttr.charAt(0) + "";
            if (discountRound.equalsIgnoreCase("R")) {
                round = BigDecimal.ROUND_HALF_UP;
            } else if (discountRound.equalsIgnoreCase("C")) {
                round = BigDecimal.ROUND_UP;
            } else if (discountRound.equalsIgnoreCase("D")) {
                round = BigDecimal.ROUND_DOWN;
            }
            int scale = Integer.parseInt(discountAttr.charAt(1) + "");
            if (discountType.equals("0")) {
                discountUnitPrice = curLineItem.getUnitPrice().multiply(discountValue);
                discountUnitPrice = discountUnitPrice.setScale(scale, round);
            }
            discountLineItem.setUnitPrice(discountUnitPrice);
         
        
            BigDecimal discountAmount = discountUnitPrice.multiply(discountLineItem.getQuantity());
            discountLineItem.setAmount(discountAmount);
            discountLineItem.setDiscountNumber(discountID);
            discountLineItem.setDiscountType("S");
            trans.addLineItem(discountLineItem);
            trans.addItemDiscount(discountUnitPrice);
            String discountSlip = discountAttr.charAt(4) + "";
            if (discountSlip.equals("0")) {
                exitState = PrintPluState.class;
            } else if (discountSlip .equals("1")) {
                exitState = Slipping3State.class;
            }

            //  set to current LineItem properties
            curLineItem.setSINo(String.valueOf(discountLineItem.getLineItemSequence()));

            //  set to Transaction properties
            String taxID = curLineItem.getTaxType();
            String type = "";
            if (discountType.equals("0")) {
                type = "SIP";
            } else if (discountType.equals("1")) {
                type = "SIM";
            } else if (discountType.equals("2")) {
                type = "SIPLUS";
            }
            String fieldName = type + "AMT" + taxID;
            String fieldName2 = type + "CNT" + taxID;
            trans.setFieldValue(fieldName, discountAmount.add((BigDecimal)trans.getFieldValue(fieldName)));
            trans.setFieldValue(fieldName2, new Integer(((Integer)trans.getFieldValue(fieldName2)).intValue() + 1));
        } catch (InstantiationException e) {
        } catch (TooManyLineItemsException e) {
        }
    }

    public Class exit(EventObject event, State sinkState) {
        return exitState;
    }
}

