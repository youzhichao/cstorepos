package hyi.cream.state;

import java.util.*;
import java.io.*;

import jpos.*;
import jpos.events.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author
 */
public class KeyLock1State extends State implements PopupMenuListener {
    private static KeyLock1State keyLock1State = null;
    private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private Transaction trans             = app.getCurrentTransaction();
    private ResourceBundle res            = CreamToolkit.GetResource();
    private PopupMenuPane p               = app.getPopupMenuPane();
    private static ArrayList states       = new ArrayList();
    private int selectItem                = 0;
    private static ArrayList menu         = new ArrayList();
    private boolean buttonState = false;

    public static KeyLock1State getInstance() {
        try {
            if (keyLock1State == null) {
                keyLock1State = new KeyLock1State();
            }
        } catch (InstantiationException ex) {
        }
        return keyLock1State;
    }

    static {
        File propFile = CreamToolkit.getConfigurationFile(KeyLock1State.class);
        try {
            FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = null;
            if (CreamProperties.getInstance().getProperty("Locale").equals("zh_TW")) {
                inst = new InputStreamReader(filein, "BIG5");
            } else if (CreamProperties.getInstance().getProperty("Locale").equals("zh_CN")) {
                inst = new InputStreamReader(filein, "GBK");
            }
            BufferedReader in = new BufferedReader(inst);
            String line = null;

            StringTokenizer t = null;
            while ((line = in.readLine()) != null) {
                if (!line.trim().startsWith("#")) {
                    t = new StringTokenizer(line.trim(), ",");
                    menu.add(t.nextToken());
                    if (t.hasMoreElements()) {
                        states.add(t.nextToken());
                    } else {
                        states.add("");
                    }
                }
            }
        } catch (FileNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("File is not found: " + propFile.toString());
        } catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("IO exception at " + KeyLock1State.class);
        }
    }

    /**
     * Constructor
     */
    public KeyLock1State() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        //System.out.println("KeyLock1State Entry!");

        app.setKeyState(true);
        //Bruce/20021030/
        ////app.setTransactionEnd(true);
        app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSelect"));
        if (event != null && !(event.getSource() instanceof SelectButton)) {
            app.getWarningIndicator().setMessage("");
        }

        if (event != null && event.getSource() instanceof CheckInOutButton) {
            buttonState = true;
        }

        app.getPayingPane().setVisible(false);
        app.getItemList().setVisible(true);
        app.getPopupMenuPane().setInputEnabled(true);

        app.getCurrentTransaction().Clear();

        p.setMenu(menu);
        p.setVisible(true);
        p.addToList(this);
        p.clear();

        if (event != null
            && !(event.getSource() instanceof ClearButton)
            && !(event.getSource() instanceof SelectButton)
            && !(event.getSource() instanceof Keylock)) {
            app.setChecked(false);
        }

        if (app.getScanCashierNumber()
            && !app.getChecked()) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("CheckWarning"));
        }
    }

    public Class exit(EventObject event, State sinkState) {

        //Bruce/20021030
        ////app.setTransactionEnd(false);
        if (event.getSource() instanceof SelectButton) {
            String stateName = (String)states.get(selectItem);
			CreamToolkit.logMessage("KeyLock1State exit | stateName : " + stateName);
            
            if (stateName.equalsIgnoreCase("halt")) {
                Runtime currentApp = Runtime.getRuntime();
                try {
                    Process c = currentApp.exec("halt");
                } catch (Exception e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                    System.out.println(e);
                }

//              假如操作系统用的windows，上面的命令就不能执行，让它直接回到原状态
                return KeyLock1State.class;
                
            } else {
                boolean isSpecialState = 
                    stateName.equalsIgnoreCase("ReturnNumberState") ||
                    stateName.equalsIgnoreCase("ReturnSaleState")   ||
                    stateName.equalsIgnoreCase("CashInIdleState")   ||
                    stateName.equalsIgnoreCase("CashOutIdleState")  ||
                    stateName.equalsIgnoreCase("DrawerOpenState2")  ||
                    stateName.equalsIgnoreCase("CheckCashierState");  //Bruce/2003-12-24

                if (isSpecialState) {
                    if (app.getCurrentTransaction().getCashierNumber() == null
                        || app.getCurrentTransaction().getCashierNumber().equals("")) {
                        app.getWarningIndicator().setMessage(res.getString("NoLoginWarning"));
                        app.getMessageIndicator().setMessage("");
                        return KeyLock1State.class;
                    }
                    //Bruce/20030324
                    Collection fds = Cashier.getExistedFieldList("cashier");
        
                    //因为现在下面的几个操作结束时，都是返回
                    //到 CashierRightsCheckState 的SourceState，
                    //即从哪儿进入就返回到哪儿
                    CreamToolkit.logMessage("KeyLock1State | " + stateName + " | start ....");
                    CashierRightsCheckState.setSourceState("KeyLock1State");
                    //Bruce/2003-12-24
                    CashierRightsCheckState.setTargetState(
                        stateName.equalsIgnoreCase("CheckCashierState") ? "ReturnNumberState" : stateName);
              
                    //如果需要检查权限，就进入 CashierRightsCheckState
                    //否则就直接进入要操作的state    
                    if (fds.contains("CASRIGHTS")) {
                        if (stateName.equalsIgnoreCase("CheckCashierState"))
                            return CheckCashierState.class;
                        else
                            return CashierRightsCheckState.class;    
                    }
                }
     
                //Bruce/20021030/
                ////app.setTransactionEnd(false);
                //System.out.println("state name = " + stateName);
                try {
                    Class c = Class.forName("hyi.cream.state." + stateName);
                    return c;
                } catch (ClassNotFoundException e) {
                    System.out.println(e);
                }
            
            }
        } else if (event.getSource() instanceof Keylock) {
            Keylock k = (Keylock)event.getSource();
            try {
                int kp = k.getKeyPosition();
                app.setKeyPosition(kp);
                //p.setVisible(false);
                app.getCurrentTransaction().Clear(false);
                Class exitState = CreamToolkit.getExitClass(kp);
                if (exitState != null) {
                    if (exitState.equals(InitialState.class))  // 要回到销售画面的时候在把菜单消失
                        p.setVisible(false);
                    return exitState;
                } else {
                    return KeyLock1State.class;
                }
                /*if (kp == 5) {
                    app.setKeyPosition(kp);
                    p.setVisible(false);
                    return KeyLock3State.class;
                } else if (kp == 3) {
                    app.setKeyPosition(kp);
                    p.setVisible(false);
                    app.getCurrentTransaction().Clear();
                    return KeyLock2State.class;
                }*/
            } catch (JposException e) {
                System.out.println(e);
            }
        } else if (event.getSource() instanceof ClearButton) {
            //Bruce/20021030/
            ////app.setTransactionEnd(true);
            if (buttonState) {
                p.setVisible(false);
                buttonState = false;
                return InitialState.class;
            } else {
                return KeyLock1State.class;
            }
        }
        return sinkState.getClass();
    }

    public void doSomething() {
        if (p.getSelectedMode()) {
            selectItem = p.getSelectedNumber();
            POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        }
    }
}

