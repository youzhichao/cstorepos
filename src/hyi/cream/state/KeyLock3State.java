package hyi.cream.state;

import java.util.*;
import java.io.*;
import jpos.*;
import jpos.events.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;
import hyi.cream.inline.*;

/**
 * A Class class.
 * <P>
 * @author
 */
public class KeyLock3State extends State implements PopupMenuListener {
    private boolean showEmptyMenu;
    private static KeyLock3State keyLock3State = null;
    private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private Transaction trans             = app.getCurrentTransaction();
    private ResourceBundle res            = CreamToolkit.GetResource();
    private PopupMenuPane p               = app.getPopupMenuPane();
    private static ArrayList states       = new ArrayList();
    private int selectItem                = 0;
    private static ArrayList menu         = new ArrayList();
    private static ArrayList emptyMenu    = new ArrayList();

    public static KeyLock3State getInstance() {
        try {
            if (keyLock3State == null) {
                keyLock3State = new KeyLock3State();
            }
        } catch (InstantiationException ex) {
        }
        return keyLock3State;
    }

    static {
        File propFile = CreamToolkit.getConfigurationFile(KeyLock3State.class);
        try {
            FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = null;
            if (CreamProperties.getInstance().getProperty("Locale").equals("zh_TW")) {
                inst = new InputStreamReader(filein, "BIG5");
            } else if (CreamProperties.getInstance().getProperty("Locale").equals("zh_CN")) {
                inst = new InputStreamReader(filein, "GBK");
            }
            BufferedReader in = new BufferedReader(inst);
            String line = null;

            StringTokenizer t = null;
            while ((line = in.readLine()) != null) {
                if (!line.trim().startsWith("#")) {
                    t = new StringTokenizer(line.trim(), ",");
                    menu.add(t.nextToken());
                    if (t.hasMoreElements()) {
                        states.add(t.nextToken());
                    } else {
                        states.add("");
                    }
                }
            }
        } catch (FileNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("File is not found: " + propFile.toString());
        } catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("IO exception at " + KeyLock3State.class);
        }
    }

    /**
     * Constructor
     */
    public KeyLock3State() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        StateMachine.setFloppySaveError(false);
        if ((sourceState instanceof ZState || sourceState instanceof ZState2)
        	&& Client.getInstance().isConnected()) 
        {
            //DacTransfer.getInstance().getFiles();
            StateMachine.getInstance().setEventProcessEnabled(false);
            
            try {

                Trigger.getInstance().downloadProgramFiles();
//                Trigger.getInstance().shrinkLogFiles();

                if (Client.getInstance().getNumberOfFilesGotOrMoved() > 0) {
                    POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                        CreamToolkit.GetResource().getString("UpdateCompletedAndReboot"));
                    DacTransfer.shutdown(5);
                } else {
                    POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                        CreamToolkit.GetResource().getString("UpdateCompleted"));
                }

            } catch (ClientCommandException e) {
                POSTerminalApplication.getInstance().getMessageIndicator().setMessage(
                    res.getString("NoDataUpdated"));
            } finally {
                StateMachine.getInstance().setEventProcessEnabled(true);
            }
        }
        
        app.getItemList().setVisible(true);
//        app.getCurrentTransaction().Clear();
        
        app.setKeyState(true);
        //Bruce/20021030/
        ////app.setTransactionEnd(true);
        if (!(sourceState instanceof ZState) && !(sourceState instanceof ZState2)) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSelect"));
        }
        if (event != null && !(event.getSource() instanceof SelectButton)) {
            app.getWarningIndicator().setMessage("");
        }

        if (showEmptyMenu) {
            p.setMenu(emptyMenu);
            showEmptyMenu = false;
        } else {
            p.setMenu(menu);
        }
        p.setVisible(true);
        p.addToList(this);
        p.clear();

        if (event == null || event.getSource() instanceof Keylock) {
            trans.Clear(false);
        }

        if (event != null && !(event.getSource() instanceof ClearButton)
            && !(event.getSource() instanceof Keylock)) {
            app.setChecked(false);
        }

        if (app.getScanCashierNumber()
            && !app.getChecked()) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("CheckWarning"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("KeyLock3State Exit!");

        app.getPayingPane().setVisible(false);
        //Bruce/20021030/
        ////app.setTransactionEnd(false);

        if (event.getSource() instanceof SelectButton) {
            String stateName = (String)states.get(selectItem);

            boolean isSpecialState = 
                stateName.equalsIgnoreCase("ReturnNumberState") ||
                stateName.equalsIgnoreCase("ReturnSaleState")   ||
                stateName.equalsIgnoreCase("CashInIdleState")   ||
                stateName.equalsIgnoreCase("CashOutIdleState")  ||
                stateName.equalsIgnoreCase("DrawerOpenState2");

            if (isSpecialState) {
                if (app.getCurrentTransaction().getCashierNumber() == null
                    || app.getCurrentTransaction().getCashierNumber().equals("")) {
                    app.getWarningIndicator().setMessage(res.getString("NoLoginWarning"));
                    app.getMessageIndicator().setMessage("");
                    return KeyLock3State.class;
                }
                
                Collection fds = Cashier.getExistedFieldList("cashier");
        
                //因为现在下面的几个操作结束时，都是返回
                //到 CashierRightsCheckState 的SourceState，
                //即从哪儿进入就返回到哪儿
                CreamToolkit.logMessage("KeyLock3State | " + stateName + " | start ....");
                CashierRightsCheckState.setSourceState("KeyLock3State");
                CashierRightsCheckState.setTargetState(stateName);
              
                //如果需要检查权限，就进入 CashierRightsCheckState
                //否则就直接进入要操作的state    
                if (fds.contains("CASRIGHTS")) {
                    return CashierRightsCheckState.class;    
                }
            }

            try {
                Class c = Class.forName("hyi.cream.state." + stateName);
                return c;
            } catch (ClassNotFoundException e) {
                System.out.println(e);
            }
        }

        if (event.getSource() instanceof Keylock) {
            Keylock k = (Keylock)event.getSource();
            try {
                int kp = k.getKeyPosition();
                app.setKeyPosition(kp);
                //p.setVisible(false);
                app.getCurrentTransaction().Clear(false);
                Class exitState = CreamToolkit.getExitClass(kp);
                if (exitState != null) {
                    showEmptyMenu = false;
                    if (exitState.equals(InitialState.class))   // 要回到销售画面的时候在把菜单消失
                        p.setVisible(false);
                    return exitState;
                } else {
                    showEmptyMenu = true;   //Bruce/20030401 解决钥匙转到key3以外的要显示空菜单
                    return KeyLock3State.class;
                }
            } catch (JposException e) {
                System.out.println(e);
            }
            /*p.setVisible(false);
            app.getWarningIndicator().setMessage("");
            System.out.println("    **** keyLock state get keylock event ****");
            return KeyLock1State.class;*/
        }

        if (event.getSource() instanceof ClearButton) {
            //Bruce/20021030/
            ////app.setTransactionEnd(true);
            return KeyLock3State.class;
        }

        return sinkState.getClass();
    }

    public void doSomething() {
        if (p.getSelectedMode()) {
            selectItem = p.getSelectedNumber();
            POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        }
    }
}

