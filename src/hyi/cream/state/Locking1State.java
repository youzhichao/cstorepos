package hyi.cream.state;
import java.util.*;
import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
public class Locking1State extends State{

private POSTerminalApplication app = POSTerminalApplication.getInstance();
     public Locking1State(){

     }

     public void entry (EventObject event, State sourceState) {
         if (sourceState instanceof IdleState)
             app.getMessageIndicator().setMessage("(1) 锁定 (2) 取消 ");

     }

     public Class exit(EventObject event, State sinkState) {
         if (event.getSource() instanceof ClearButton) {
            return IdleState.class;
         }

         if (event.getSource() instanceof NumberButton) {
               NumberButton nb = (NumberButton)event.getSource();
               if(nb.getNumberLabel().equalsIgnoreCase("1") ){
                   app.getMessageIndicator().setMessage(nb.getNumberLabel());
               }
               if(nb.getNumberLabel().equalsIgnoreCase("2")){
                   app.getMessageIndicator().setMessage(nb.getNumberLabel());
               }
         }

          if (event.getSource() instanceof EnterButton ){
              if (app.getMessageIndicator().getMessage().trim().equalsIgnoreCase("1")){
                 return LockingState.class;
              }
              if (app.getMessageIndicator().getMessage().trim().equalsIgnoreCase("2")){
                 return IdleState.class;
              }
           }
            return IdleState.class;
         }

 }