package hyi.cream.state;
import java.util.*;
import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;

public class LockingState extends State {
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static LockingState lockingState = null;
    public static LockingState getInstance() {
        try {
             if (lockingState == null) {
                lockingState = new LockingState();
             }
         } catch (InstantiationException ex) {
         }
         return lockingState;
     }

    public LockingState() throws InstantiationException {

    }

    public void entry (EventObject event, State sourceState) {
        app.getMessageIndicator().setMessage("锁定状态");
    }

    public Class exit(EventObject event, State sinkState) {
       System.out.println("exit Locking State");
       return null;
    }

}