package hyi.cream.state;

/**
 * 输入会员卡的entry state.
 * 
 * @author Bruce
 */
public class MemberState extends SomeAGReadyState {
    static MemberState memberState;

    public static MemberState getInstance() {
        try {
            if (memberState == null)
                memberState = new MemberState();
        } catch (InstantiationException ex) {
        }
        return memberState;
    }

    public MemberState() throws InstantiationException {
    }

    public String getPromptedMessage() {
        return hyi.cream.util.CreamToolkit.GetResource().getString("PleaseInputMemberID");
    }
}
