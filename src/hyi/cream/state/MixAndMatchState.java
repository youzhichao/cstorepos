/*jadclipse*/// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) radix(10) lradix(10) 
// Source File Name:   MixAndMatchState.java
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.*;
import hyi.cream.util.*;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.*;

// Referenced classes of package hyi.cream.state:
//            State, ReturnSelect2State, SetQuantityState, ReturnSummaryState, 
//            VoidState, NormalCombPromotion

/**
 * A Class class.
 * <P>
 * 
 * @author Dai,Meyer
 * @version 1.1
 */
public class MixAndMatchState extends State {
    /**
     * 更新记录
     * 
     * Dai/1.0
     *      1. Create Class
     *      2. Add mixAndMatch1()
     * 
     * Meyer/1.1/2002-01-20
     *      2. Add mixAndMatch2()
     * 
     * 
     * 
     */
    static MixAndMatchState mixAndMatchState = null;
    private POSTerminalApplication app;
    private static Transaction trans = POSTerminalApplication.getInstance().getCurrentTransaction();
    private static Object totalLineItem[] = null;
    private ArrayList totalPLU;
    private ArrayList mixAndMatchIDArray;
    private String mixAndMatchID;
    private ArrayList curPLUArray;
    private ArrayList belongTo;
    private LineItem curLineItem;
    private MixAndMatch curMixAndMatch;
    private MMGroup mmgroup;
    private Calendar curCalendar;
    private Calendar calendar;
    private static ArrayList group1Array = new ArrayList();
    private static ArrayList group2Array = new ArrayList();
    private static ArrayList group3Array = new ArrayList();
    private static ArrayList group4Array = new ArrayList();
    private static ArrayList group5Array = new ArrayList();
    private static ArrayList group6Array = new ArrayList();
    private int mixAndMatchAccount;
    private ArrayList mixAndMatchedArray;
    private PLU curPLU;
    private BigDecimal mixAndMatchedAmount;
    private BigDecimal tempMixAndMatchedAmount;
    private Hashtable taxMMAmount;
    private Hashtable taxMMCount;
    private ArrayList mmLineItemArray;
    private Class exitState;
    private ArrayList selectArray;
    /**
     * Meyer/2003-01-20
     *      Get MixAndMatchVersion 
     */
    private static final String MIX_AND_MATCH_VERSION = CreamProperties.getInstance().getProperty("MixAndMatchVersion");
    public static MixAndMatchState getInstance() {
        try {
            if (mixAndMatchState == null)
                mixAndMatchState = new MixAndMatchState();
        } catch (InstantiationException instantiationexception) {
        }
        return mixAndMatchState;
    }

    public MixAndMatchState() throws InstantiationException {
        app = POSTerminalApplication.getInstance();
        totalPLU = new ArrayList();
        mixAndMatchIDArray = new ArrayList();
        mixAndMatchID = "";
        curPLUArray = new ArrayList();
        belongTo = new ArrayList();
        curLineItem = null;
        curMixAndMatch = null;
        mmgroup = null;
        curCalendar = Calendar.getInstance();
        calendar = null;
        mixAndMatchAccount = 0;
        mixAndMatchedArray = new ArrayList();
        curPLU = null;
        mixAndMatchedAmount = new BigDecimal(0.0D);
        tempMixAndMatchedAmount = new BigDecimal(0.0D);
        taxMMAmount = new Hashtable();
        taxMMCount = new Hashtable();
        mmLineItemArray = new ArrayList();
        exitState = null;
        selectArray = null;
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState instanceof ReturnSelect2State) {
            exitState = hyi.cream.state.ReturnOneState.class;
            selectArray = ((ReturnSelect2State) sourceState).getSelectArray();
            // 清除跟M&M相关的内容以便重新计算
            initLineItemsForMM();
        } else if (sourceState instanceof SetQuantityState)
            exitState = hyi.cream.state.IdleState.class;
        else if (sourceState instanceof ReturnSummaryState)
            exitState = hyi.cream.state.DrawerOpenState.class;
        else if (sourceState instanceof VoidState) {
            // 清除跟M&M相关的内容以便重新计算
            initLineItemsForMM();
            exitState = hyi.cream.state.PrintPluState.class;
        } else {
            exitState = hyi.cream.state.PrintPluState.class;
        }
        if (MIX_AND_MATCH_VERSION == null || MIX_AND_MATCH_VERSION.trim().equals("") || MIX_AND_MATCH_VERSION.trim().equals("1"))
            mixAndMatch1();
        else if (MIX_AND_MATCH_VERSION.trim().equals("2"))
            mixAndMatch2();
    }

//  By ZhaoHong 退货或者更正的时候，清除所有跟M&M相关的内容以便重新计算M&M
    private void initLineItemsForMM() {
        Object lines[] = trans.getLineItemsArrayLast();
        LineItem li = null;
        String discountNumber = null;
        for (int i = 0; i < lines.length; i++) {
            li = (LineItem) lines[i];
            if (li.getDiscountType() != null && (li.getDiscountType().equals("M") || li.getDiscountType().equals("S")) || li.getTempDiscountType() != null && (li.getTempDiscountType().equals("M") || li.getTempDiscountType().equals("S"))) {
                // 去掉discountNumbers 中跟M&M相关的部分
                discountNumber = clearMMNumbers(li.getDiscountNumber());
                li.setAfterDiscountAmount(li.getAmount());
                li.setTempAfterDiscountAmount(new BigDecimal(0.0D));
                li.setDiscountType(null);
                li.setTempDiscountType(null);
                li.setDiscountNumber(discountNumber);
                li.setTempDiscountNumber(null);
            }
        }

    }

    //By ZhaoHong 去掉discountNumbers 中跟M&M相关的部分
    private String clearMMNumbers(String discountNumbers) {
        if (discountNumbers == null || discountNumbers.length() == 0)
            return null;
        StringBuffer sb = new StringBuffer();
        for (StringTokenizer token = new StringTokenizer(discountNumbers, ","); token.hasMoreTokens();) {
            String tmpString = token.nextToken();
            if (!tmpString.startsWith("M")) {
                sb.append(tmpString);
                if (token.hasMoreTokens())
                    sb.append(",");
            }
        }

        return sb.toString();
    }

    /**
     * Dai/
     * MixAndMatch Version 1
     * 
     */
    private void mixAndMatch1() {
        taxMMAmount.clear();
        taxMMCount.clear();
        trans.setTaxMMAmount(new BigDecimal(0.0D));
        trans.setTaxMMCount(0);
        mmLineItemArray.clear();
        Date curDate = new Date();
        totalLineItem = trans.getLineItemsArrayLast();
        // get total PLU from total LineItem
        int length = totalLineItem.length;
        for (int i = 0; i < length; i++) {
            curLineItem = (LineItem) totalLineItem[i];
            if ((curLineItem.getDetailCode().equalsIgnoreCase("R") || curLineItem.getDetailCode().equalsIgnoreCase("S")) && !curLineItem.getRemoved()) {
                curLineItem.setTempAfterDiscountAmount(new BigDecimal(0.0D));
                this.curPLU = getPLU(curLineItem);
                if (this.curPLU != null) {
                    int qty = curLineItem.getQuantity().intValue();
                    for (int j = 0; j < qty; j++) {
                        this.curPLU = getPLU(curLineItem);
                        this.curPLU.setUID(i);
                        totalPLU.add(this.curPLU);
                    }

                }
            }
        }

        // get all PLU's MixAndMatchID in total PLU
        length = totalPLU.size();
        for (int i = 0; i < length; i++) {
            Iterator it = MMGroup.queryByITEMNO(((PLU) totalPLU.get(i)).getInStoreCode());
            if (it != null)
                while (it.hasNext()) {
                    MMGroup mmid = (MMGroup) it.next();
                    mixAndMatchID = mmid.getID();
                    if (mixAndMatchID != null && !mixAndMatchID.equalsIgnoreCase("000") && !mixAndMatchIDArray.contains(mixAndMatchID))
                        mixAndMatchIDArray.add(mixAndMatchID);
                }
        }

        trans.clearMMDetail();
        Collections.sort(mixAndMatchIDArray, new Comparator() {

            public int compare(Object o1, Object o2) {
                String s1 = (String) o1;
                String s2 = (String) o2;
                return -s1.compareTo(s2);
            }

            public boolean equals(Object obj) {
                return ((String) obj).equals(this);
            }

        });
        int mmLength = mixAndMatchIDArray.size();
        System.out.println("(MM) mixandmatch count = " + mmLength);
//      ***************************Begin*************************************************//

        for (int i = 0; i < mmLength; i++) {
            mixAndMatchID = (String) mixAndMatchIDArray.get(i);
            System.out.println("(MM) " + i + ". mixAndMatchID = " + mixAndMatchID);
            // get all PLU Contained this MixAndMatchID
            curPLUArray.clear();
            for (int j = 0; j < totalPLU.size(); j++) {
                this.curPLU = (PLU) totalPLU.get(j);
                if (compMixAndMatchID(mixAndMatchID, this.curPLU)) {
                    curPLUArray.add(this.curPLU);
                    System.out.println("(MM)     cur plu = " + this.curPLU.getScreenName());
                }
            }

            curMixAndMatch = MixAndMatch.queryByID(mixAndMatchID);
            if (curMixAndMatch != null) {
                System.out.println("(MM)     query MixAndMatch from plu's MixAndMatchID : OK");
//              Check DateTime Comes Here;
                Date mmbDate = curMixAndMatch.getBeginDate();
                Date mmbTime = curMixAndMatch.getBeginTime();
                Date mmeDate = curMixAndMatch.getEndDate();
                Date mmeTime = curMixAndMatch.getEndTime();
                // check begin datetime
                if (curDate.getYear() < mmbDate.getYear()) {
                    continue;
                } else if (curDate.getYear() == mmbDate.getYear()) {
                    if (curDate.getMonth() < mmbDate.getMonth()) {
                        continue;
                    } else if (curDate.getMonth() == mmbDate.getMonth()) {
                        if (curDate.getDate() < mmbDate.getDate()) {
                            continue;
                        } else if (curDate.getDate() == mmbDate.getDate()) {
                            if (curDate.getHours() < mmbTime.getHours()) {
                                continue;
                            } else if (curDate.getHours() == mmbTime.getHours()) {
                                if (curDate.getMinutes() < mmbTime.getMinutes()) {
                                    continue;
                                } else if (curDate.getMinutes() == mmbTime.getMinutes()) {
                                    if (curDate.getSeconds() < mmbTime.getSeconds()) {
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
                if (curDate.getHours() < mmbTime.getHours()) {
                    continue;
                } else if (curDate.getHours() == mmbTime.getHours()) {
                    if (curDate.getMinutes() < mmbTime.getMinutes()) {
                        continue;
                    } else if (curDate.getMinutes() == mmbTime.getMinutes()) {
                        if (curDate.getSeconds() < mmbTime.getSeconds()) {
                            continue;
                        }
                    }
                }
                // check end datetime
                if (curDate.getYear() > mmeDate.getYear()) {
                    continue;
                } else if (curDate.getYear() == mmeDate.getYear()) {
                    if (curDate.getMonth() > mmeDate.getMonth()) {
                        continue;
                    } else if (curDate.getMonth() == mmeDate.getMonth()) {
                        if (curDate.getDate() > mmeDate.getDate()) {
                            continue;
                        } else if (curDate.getDate() == mmeDate.getDate()) {
                            if (curDate.getHours() > mmeTime.getHours()) {
                                continue;
                            } else if (curDate.getHours() == mmeTime.getHours()) {
                                if (curDate.getMinutes() > mmeTime.getMinutes()) {
                                    continue;
                                } else if (curDate.getMinutes() == mmeTime.getMinutes()) {
                                    if (curDate.getSeconds() > mmeTime.getSeconds()) {
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
                if (curDate.getHours() > mmeTime.getHours()) {
                    continue;
                } else if (curDate.getHours() == mmeTime.getHours()) {
                    if (curDate.getMinutes() > mmeTime.getMinutes()) {
                        continue;
                    } else if (curDate.getMinutes() == mmeTime.getMinutes()) {
                        if (curDate.getSeconds() > mmeTime.getSeconds()) {
                            continue;
                        }
                    }
                }
                //  check day of week
                int dayOfWeek = curDate.getDay();
                String weekCycle = curMixAndMatch.getWeekCycle();
                if (weekCycle == null)
                    weekCycle = "1111111";
                else if (weekCycle.length() < 7) {
                    for (int m = weekCycle.length(); m < 8; m++)
                        weekCycle = weekCycle + "1";

                }
                if (weekCycle.charAt(dayOfWeek) != '0') {
                    System.out.println("(MM)     check DateTime is right");
//                  check MixAndMatch Type
//                  type = "1", means "一般组合促销"
                    System.out.println("(MM) MixAndMatch type = " + curMixAndMatch.getType());
                    int lLength = curPLUArray.size();
                    if (curMixAndMatch.getType().equals("1")) {
                        group1Array.clear();
                        group2Array.clear();
                        group3Array.clear();
                        group4Array.clear();
                        group5Array.clear();
                        group6Array.clear();
//                      check MixAndMatchGroupID
//                      put PLU into GroupArray
                        for (int j = 0; j < lLength; j++) {
                            this.curPLU = (PLU) curPLUArray.get(j);
                            String groupID = getMMGroupID(this.curPLU, mixAndMatchID);
                            if (groupID != null)
                                switch (Integer.parseInt(groupID)) {
                                case 1: // '\001'
                                    group1Array.add(this.curPLU);
                                    break;

                                case 2: // '\002'
                                    group2Array.add(this.curPLU);
                                    break;

                                case 3: // '\003'
                                    group3Array.add(this.curPLU);
                                    break;

                                case 4: // '\004'
                                    group4Array.add(this.curPLU);
                                    break;

                                case 5: // '\005'
                                    group5Array.add(this.curPLU);
                                    break;

                                case 6: // '\006'
                                    group6Array.add(this.curPLU);
                                    break;
                                }
                        }

                        //  get MixAndMatch Account
                        Integer aa = curMixAndMatch.getDiscountLimit();
                        if (aa == null)
                            mixAndMatchAccount = 0;
                        else
                            mixAndMatchAccount = aa.intValue();
                        System.out.println("BEFORE checkGrpQty : ");
                        if (checkGrpQty(curMixAndMatch)) {
                            System.out.println(" AFTER checkGrpQty : ");
                            // check MixAndMatch GroupQuantity and DiscountLimit
                            // get all MixAndMatched PLU
                            if (mixAndMatchAccount != 0) {
                                int mma = 0;
                                BigDecimal amt;
                                for (; checkGrpQty(curMixAndMatch) && mixAndMatchAccount != 0; mmLineItemArray.add(new NewArray(mixAndMatchID, amt))) {
                                    mixAndMatchAccount--;
                                    mma++;
                                    // get MixAndMatched PLUArray
                                    mixAndMatchedArray = getMMedPLU(mixAndMatchedArray, curMixAndMatch);
                                    amt = null;
                                    // PriceType = "1", means "售价"
                                    System.out.println("(MM)            MixAndMatch Price Type = " + curMixAndMatch.getPriceType());
                                    BigDecimal unitDiscountAmount = curMixAndMatch.getDiscountAmount();
                                    if (curMixAndMatch.getPriceType().equals("1")) {
                                        // get MixAndMatched Amount
                                        BigDecimal discountAmount = unitDiscountAmount.multiply(new BigDecimal(mma));
                                        amt = getMixAndMatchAmount(mixAndMatchedArray).subtract(discountAmount);
                                    } else
                                    // PriceType = "2", means "折让"
                                    if (curMixAndMatch.getPriceType().equals("2")) {
                                        BigDecimal discountAmount = unitDiscountAmount.multiply(new BigDecimal(mma));
                                        amt = discountAmount;
                                    } else if (curMixAndMatch.getPriceType().equals("3")) {
                                        // Bruce/20030401 折扣只算到小数位数1位
                                        BigDecimal pluAmount = getMixAndMatchAmount(mixAndMatchedArray);
                                        amt = pluAmount.multiply(unitDiscountAmount).setScale(1, 4);
                                    } else if (curMixAndMatch.getPriceType().equals("4")) {
                                        ArrayList newMixAndMatchedArray = new ArrayList();
                                        for (int j = 0; j < mixAndMatchedArray.size(); j++) {
                                            PLU curPLU = (PLU) mixAndMatchedArray.get(j);
                                            if (getMMGroupID(curPLU, mixAndMatchID).equals("1"))
                                                newMixAndMatchedArray.add(curPLU);
                                        }

                                        BigDecimal grp1Amount = new BigDecimal(0.0D);
                                        BigDecimal grp1OrigAmount = getMixAndMatchAmount(newMixAndMatchedArray);
                                        for (int j = 0; j < mixAndMatchedArray.size(); j++) {
                                            PLU curPLU = (PLU) mixAndMatchedArray.get(j);
                                            if (getMMGroupID(curPLU, mixAndMatchID).equals("1"))
                                                grp1Amount = grp1Amount.add(curMixAndMatch.getDiscountAmount());
                                        }

                                        amt = grp1OrigAmount.subtract(grp1Amount);
                                    }
                                    mixAndMatchedAmount = mixAndMatchedAmount.add(amt);
                                }

                                Integer cc = curMixAndMatch.getDiscountLimit();
                                if (cc == null)
                                    mixAndMatchAccount = 0 - mixAndMatchAccount;
                                else
                                    mixAndMatchAccount = cc.intValue() - mixAndMatchAccount;
                                System.out.println("(MM)            current count = " + mixAndMatchAccount);
                            } else {
                                mixAndMatchedArray.clear();
                                ArrayList unitMMArray = new ArrayList();
                                for (; checkGrpQty(curMixAndMatch); System.out.println("(MM)                current amount = " + mixAndMatchedAmount)) {
                                    mixAndMatchAccount++;
                                    unitMMArray.clear();
                                    unitMMArray = getMMedPLU(unitMMArray, curMixAndMatch);
                                    for (int m = 0; m < unitMMArray.size(); m++)
                                        mixAndMatchedArray.add(unitMMArray.get(m));

                                    BigDecimal amt = null;
                                    System.out.println("(MM)            current type = " + curMixAndMatch.getPriceType());
                                    BigDecimal unitDiscountAmount = curMixAndMatch.getDiscountAmount();
                                    if (curMixAndMatch.getPriceType().equals("1")) {
                                        BigDecimal discountAmount = unitDiscountAmount;
                                        System.out.println("(MM)                discount amount = " + discountAmount);
                                        amt = getMixAndMatchAmount(unitMMArray).subtract(discountAmount);
                                        System.out.println("(MM)                get amount = " + amt);
                                    } else if (curMixAndMatch.getPriceType().equals("2")) {
                                        BigDecimal discountAmount = unitDiscountAmount;
                                        System.out.println("(MM)                discount amount = " + discountAmount);
                                        amt = discountAmount;
                                    } else if (curMixAndMatch.getPriceType().equals("3")) {
                                        BigDecimal pluAmount = getMixAndMatchAmount(unitMMArray);
                                        amt = pluAmount.multiply(unitDiscountAmount);
                                        amt = amt.setScale(1, 0);
                                    } else if (curMixAndMatch.getPriceType().equals("4")) {
                                        ArrayList newMixAndMatchedArray = new ArrayList();
                                        for (int j = 0; j < unitMMArray.size(); j++) {
                                            PLU curPLU = (PLU) mixAndMatchedArray.get(j);
                                            if (getMMGroupID(curPLU, mixAndMatchID).equals("1"))
                                                newMixAndMatchedArray.add(curPLU);
                                        }

                                        BigDecimal grp1Amount = new BigDecimal(0.0D);
                                        BigDecimal grp1OrigAmount = getMixAndMatchAmount(newMixAndMatchedArray);
                                        for (int j = 0; j < unitMMArray.size(); j++) {
                                            PLU curPLU = (PLU) unitMMArray.get(j);
                                            if (getMMGroupID(curPLU, mixAndMatchID).equals("1"))
                                                grp1Amount = grp1Amount.add(curMixAndMatch.getDiscountAmount());
                                        }

                                        amt = grp1OrigAmount.subtract(grp1Amount);
                                    }
                                    //mixAndMatchedArray.clear();
                                    mixAndMatchedAmount = mixAndMatchedAmount.add(amt);
                                    mmLineItemArray.add(new NewArray(mixAndMatchID, amt));
                                }

                            }
                        }
                    }
                    //  type = "2", means "PACK组合促销"
                    if (curMixAndMatch.getType().equals("2")) {
                        String packAmountField = "";
                        BigDecimal mmAmount = new BigDecimal(0.0D);
                        //  get MixAndMatch Account
                        Integer aa = curMixAndMatch.getDiscountLimit();
                        if (aa == null)
                            mixAndMatchAccount = 0;
                        else
                            mixAndMatchAccount = aa.intValue();
                        int packSeq;
                        if (mixAndMatchAccount != 0) {
                            while ((packSeq = getPackQty(curMixAndMatch, curPLUArray)) != 0 && mixAndMatchAccount != 0) {
                                mixAndMatchAccount--;
                                packAmountField = "PACK" + packSeq + "AMT";
                                mmAmount = (BigDecimal) curMixAndMatch.getFieldValue(packAmountField);
                                NewArray arr = new NewArray(mixAndMatchID, mmAmount);
                                mmLineItemArray.add(arr);
                                mixAndMatchedAmount = mixAndMatchedAmount.add(mmAmount);
                            }
                            Integer bb = curMixAndMatch.getDiscountLimit();
                            if (bb == null)
                                mixAndMatchAccount = 0 - mixAndMatchAccount;
                            else
                                mixAndMatchAccount = bb.intValue() - mixAndMatchAccount;
                        } else {
                            while ((packSeq = getPackQty(curMixAndMatch, curPLUArray)) != 0) {
                                mixAndMatchAccount++;
                                packAmountField = "PACK" + packSeq + "AMT";
                                mmAmount = (BigDecimal) curMixAndMatch.getFieldValue(packAmountField);
                                NewArray arr = new NewArray(mixAndMatchID, mmAmount);
                                mmLineItemArray.add(arr);
                                mixAndMatchedAmount = mixAndMatchedAmount.add(mmAmount);
                            }
                        }
                    }
                    //  type = "3", means "小计金额组合促销"
                    curMixAndMatch.getType().equals("3");
                    if (curMixAndMatch.getType().equals("3")) {
                    }
                    //  type = "0", means "Linked 组合促销"
                    if (curMixAndMatch.getType().equals("0")) {
                        Integer aa = curMixAndMatch.getDiscountLimit();
                        if (aa == null)
                            mixAndMatchAccount = 0;
                        else
                            mixAndMatchAccount = aa.intValue();
                        ArrayList linkArray = new ArrayList();
                        String linkID = curMixAndMatch.getLinkedMixandMatch();
                        for (int j = 0; j < totalPLU.size(); j++) {
                            this.curPLU = (PLU) totalPLU.get(j);
                            if (getMMGroupID(this.curPLU, mixAndMatchID).equals(linkID))
                                linkArray.add(this.curPLU);
                        }

                        int grpqty = MixAndMatch.queryByID(linkID).getGroup1Quantity().intValue();
                        //  check MixAndMatch linkQuantity and DiscountLimit
                        //  get MixAndMatched PluArrayList
                        //int linkSeq;
                        int linkSize = linkArray.size();
                        if (mixAndMatchAccount != 0) {
                            while (curPLUArray.size() >= grpqty && linkSize > 0 && mixAndMatchAccount != 0) {
                                mixAndMatchAccount--;
                                linkSize--;
                                mixAndMatchedArray = getUnitMMedPLU(mixAndMatchedArray, curPLUArray, grpqty);
                            }
                            Integer dd = curMixAndMatch.getDiscountLimit();
                            if (dd == null)
                                mixAndMatchAccount = 0 - mixAndMatchAccount;
                            else
                                mixAndMatchAccount = dd.intValue() - mixAndMatchAccount;
                        } else {
                            while (curPLUArray.size() >= grpqty && linkSize > 0) {
                                mixAndMatchAccount++;
                                linkSize--;
                                mixAndMatchedArray = getUnitMMedPLU(mixAndMatchedArray, curPLUArray, grpqty);
                            }
                        }
                        //  get MixAndMatch Account
                        //mixAndMatchAccount = curMixAndMatch.getDiscountLimit().intValue() - mixAndMatchAccount;

                        //  get Linked Plu
                        linkArray = getUnitMMedPLU(new ArrayList(), linkArray, mixAndMatchAccount);

                        //  get MixAndMatchedAmount
                        mixAndMatchedAmount = mixAndMatchedAmount.add(getMixAndMatchAmount(linkArray));
                        for (int x = 0; x < linkArray.size(); x++)
                            mmLineItemArray.add(new NewArray(mixAndMatchID, ((PLU) linkArray.get(x)).getUnitPrice()));

                    }
                    MMTaxProportion(mixAndMatchID, mixAndMatchedArray, mixAndMatchedAmount, mixAndMatchAccount);
                    //MMCatProportion(mixAndMatchedArray, mixAndMatchedAmount);
                    curPLUArray.clear();
                    mixAndMatchedArray.clear();
                    tempMixAndMatchedAmount = tempMixAndMatchedAmount.add(mixAndMatchedAmount);
                    System.out.println("(MM) mixAndMatchedAmount = " + mixAndMatchedAmount.toString());
                    trans.addTaxMM(mixAndMatchAccount, mixAndMatchedAmount);
                    mixAndMatchedAmount = new BigDecimal(0.0D);
                    System.out.println("(MM) mixAndMatchedAmount = " + mixAndMatchedAmount.toString());
                    System.out.println("(MM) ********** MixAndMatch End **********");
                }
            }
        }

    }

    public Class exit(EventObject event, State sinkState) {
        totalLineItem = null;
        totalPLU.clear();
        belongTo.clear();
        mixAndMatchIDArray.clear();
        mixAndMatchedAmount = new BigDecimal(0.0D);
        if (exitState.equals(hyi.cream.state.DrawerOpenState.class)) {
            trans.initForAccum();
            trans.initSIInfo();
            trans.accumulate();
            CreamToolkit.showText(trans, 1);
            if (MIX_AND_MATCH_VERSION == null || MIX_AND_MATCH_VERSION.trim().equals("") || MIX_AND_MATCH_VERSION.trim().equals("1"))
                trans.updateTaxMM();
            CreamToolkit.logMessage("store() in MixAndMatchState exit()");
            trans.store();
            CreamPrinter.getInstance().printPayment(trans);
            trans = app.getCurrentTransaction();
        }
        return exitState;
    }

    public static void setTransaction(Transaction trans) {
        trans = trans;
    }

    public ArrayList getSelectArray() {
        return selectArray;
    }

    public void setTaxMM(String fieldName, BigDecimal mmAmount) {
        if (taxMMAmount.containsKey(fieldName))
            taxMMAmount.put(fieldName, mmAmount.add((BigDecimal) taxMMAmount.get(fieldName)));
        else
            taxMMAmount.put(fieldName, mmAmount);
        if (taxMMCount.containsKey(fieldName))
            taxMMCount.put(fieldName, new Integer(((Integer) taxMMCount.get(fieldName)).intValue() + 1));
        else
            taxMMCount.put(fieldName, new Integer(1));
        trans.setTaxMMAmount(taxMMAmount);
        trans.setTaxMMCount(taxMMCount);
    }

    public void MMTaxProportion(String mmID, ArrayList plusArray, BigDecimal amount, int qty) {
        try {
            LineItem mmLineItem = new LineItem();
            mmLineItem.setPluNumber(mmID);
            mmLineItem.setQuantity(new BigDecimal(qty));
            mmLineItem.setUnitPrice(amount);

            /*
             * Meyer/2003-02-21/
             */
            mmLineItem.setItemNumber(mmID);
            mmLineItem.setOriginalPrice(amount);
            mmLineItem.setAmount(amount);
            trans.setMMDetail(mmID, mmLineItem);
        } catch (InstantiationException instantiationexception) {
        }
        if (plusArray.size() == 0)
            return;
        BigDecimal unitPriceMax = new BigDecimal(-1D);
        PLU pluMax = null;
        for (int i = 0; i < plusArray.size(); i++)
            if (unitPriceMax.compareTo(((PLU) plusArray.get(i)).getUnitPrice()) == -1) {
                unitPriceMax = ((PLU) plusArray.get(i)).getUnitPrice();
                pluMax = (PLU) plusArray.get(i);
            }

        BigDecimal denominator = getMixAndMatchAmount(plusArray);
        BigDecimal numerator = null;
        BigDecimal modulus = null;
        BigDecimal proportion = null;
        String fieldName = "";
        PLU plu = null;
        BigDecimal tempAmount = new BigDecimal(0.0D);
        BigDecimal bigDecimal0 = new BigDecimal("0.00");
        boolean state = true;
        for (int i = 0; i < plusArray.size(); i++) {
            plu = (PLU) plusArray.get(i);
            if (plu.equals(pluMax) && plu.getUID() == pluMax.getUID() && state) {
                state = false;
            } else {
                numerator = getPluPrice(plu);
                //Bruce/20030328 advoid divide by zero
                if (denominator.equals(bigDecimal0))
                    modulus = bigDecimal0;
                else
                    modulus = numerator.divide(denominator, 2, 4);
                proportion = amount.multiply(modulus).setScale(2, 4);
                tempAmount = tempAmount.add(proportion);
                fieldName = plu.getTaxType();
                setTaxMM(fieldName, proportion);
                LineItem lineItem = (LineItem) totalLineItem[plu.getUID()];
                lineItem.setTempAfterDiscountAmount(lineItem.getTempAfterDiscountAmount().subtract(proportion));
                lineItem.setTempDiscountType("M");
                lineItem.setTempDiscountNumber(mixAndMatchID);
            }
        }

        proportion = amount.subtract(tempAmount);
        fieldName = pluMax.getTaxType();
        setTaxMM(fieldName, proportion);
        LineItem lineItemMax = (LineItem) totalLineItem[pluMax.getUID()];
        lineItemMax.setTempAfterDiscountAmount(lineItemMax.getTempAfterDiscountAmount().subtract(proportion));
        lineItemMax.setTempDiscountType("M");
        lineItemMax.setTempDiscountNumber(mixAndMatchID);
    }

    public ArrayList getLinkedArray(MixAndMatch curMM, ArrayList curArray) {
        //ArrayList mmedArray = new ArrayList();
        ArrayList linkedArray = new ArrayList();
        String linkedMMID = curMM.getLinkedMixandMatch();
        for (int i = 0; i < totalPLU.size(); i++) {
            curPLU = (PLU) totalPLU.get(i);
            if (getMMGroupID(curPLU, mixAndMatchID).equals(linkedMMID))
                linkedArray.add(curPLU);
        }

        if (linkedArray.size() <= curArray.size())
            return linkedArray;
        else
            return getUnitMMedPLU(new ArrayList(), linkedArray, curArray.size() - linkedArray.size());
    }

    public int getPackQty(MixAndMatch curMM, ArrayList curArray) {
        int i = curArray.size();
        int seq = getMaxPackSeq(curMM);
        //String qtyField = "";
        ArrayList mmedArray = new ArrayList();
        for (int j = seq; j > 0; j--) {
            //qtyField = "PACK" + j + "QTY";
            int qty = ((Integer) curMM.getFieldValue("PACK" + j + "QTY")).intValue();
            if (i >= qty) {
                mmedArray = getUnitMMedPLU(mmedArray, curArray, qty);
                mixAndMatchedArray = mmedArray;
                return j;
            }
        }

        return 0;
    }

    //  get max number of pack meet request
    public int getMaxPackSeq(MixAndMatch curMM) {
        int i = 0;
        String qtyField = "";
        do {
            i++;
            qtyField = "PACK" + i + "QTY";
        } while (curMM.getFieldValue(qtyField) != null && ((Integer) curMM.getFieldValue(qtyField)).intValue() != 0);
        return i - 1;
    }

    public BigDecimal getMixAndMatchAmount(ArrayList plusArray) {
        BigDecimal pluPrice = null;
        BigDecimal pluAmount = new BigDecimal(0.0D);
        for (int j = 0; j < plusArray.size(); j++) {
            curPLU = (PLU) plusArray.get(j);
            CreamToolkit.logMessage("MixAndMatchState getMixAndMatchAmount : curPLU = " + curPLU);
            pluPrice = curPLU.getUnitPrice();
            CreamToolkit.logMessage("MixAndMatchState getMixAndMatchAmount : pluPrice = " + pluPrice);
            BigDecimal pluPmPrice = curPLU.getSpecialPrice();
            //  check discount price
            Date currentDate = new Date();
            Time currentTime = new Time(currentDate.getTime());
            if (pluPmPrice != null && pluPmPrice.compareTo(new BigDecimal("0")) == 1) {
                java.sql.Date discountStartDate = curPLU.getDiscountStartDate();
                java.sql.Date discountEndDate = curPLU.getDiscountEndDate();
                if (discountStartDate != null && discountEndDate != null && currentDate.after(discountStartDate) && currentDate.before(discountEndDate)) {
                    Time discountStart = curPLU.getDiscountStartTime();
                    Time discountEnd = curPLU.getDiscountEndTime();
                    int discountStartHour = Integer.parseInt(discountStart.toString().substring(0, 2));
                    int discountStartMinute = Integer.parseInt(discountStart.toString().substring(3, 5));
                    int discountEndHour = Integer.parseInt(discountEnd.toString().substring(0, 2));
                    int discountEndMinute = Integer.parseInt(discountEnd.toString().substring(3, 5));
                    int discountEndSecond = Integer.parseInt(discountEnd.toString().substring(6));
                    int currentHour = currentTime.getHours();
                    int currentMinute = currentTime.getMinutes();
                    int currentSecond = currentTime.getSeconds();
                    if (currentHour < discountEndHour && currentHour > discountStartHour)
                        pluPrice = pluPmPrice;
                    else if (currentHour == discountEndHour || currentHour == discountStartHour)
                        if (currentMinute < discountEndMinute && currentMinute > discountStartMinute)
                            pluPrice = pluPmPrice;
                        else if (currentMinute == discountEndMinute && currentMinute == discountStartMinute && currentSecond <= discountEndSecond && currentSecond >= discountEndSecond)
                            pluPrice = pluPmPrice;
                }
            }
            //  check member price
            if (trans.getMemberID() != null && !trans.getMemberID().equals("")) {
                BigDecimal memberPrice = curPLU.getMemberPrice();
                if (memberPrice != null && memberPrice.compareTo(new BigDecimal(0.0D)) == 1 && pluPrice.compareTo(memberPrice) == 1)
                    pluPrice = memberPrice;
            }
            pluAmount = pluAmount.add(pluPrice);
        }

        return pluAmount;
    }

    public BigDecimal getPluPrice(PLU curPLU) {
        BigDecimal pluPrice = curPLU.getUnitPrice();
        BigDecimal pluPmPrice = curPLU.getSpecialPrice();
        //  check discount price
        Date currentDate = new Date();
        Time currentTime = new Time(currentDate.getTime());
        if (pluPmPrice != null && pluPmPrice.compareTo(new BigDecimal("0")) == 1) {
            java.sql.Date discountStartDate = curPLU.getDiscountStartDate();
            java.sql.Date discountEndDate = curPLU.getDiscountEndDate();
            if (discountStartDate != null && discountEndDate != null && currentDate.after(discountStartDate) && currentDate.before(discountEndDate)) {
                Time discountStart = curPLU.getDiscountStartTime();
                Time discountEnd = curPLU.getDiscountEndTime();
                int discountStartHour = Integer.parseInt(discountStart.toString().substring(0, 2));
                int discountStartMinute = Integer.parseInt(discountStart.toString().substring(3, 5));
                int discountStartSecond = Integer.parseInt(discountStart.toString().substring(6));
                int discountEndHour = Integer.parseInt(discountEnd.toString().substring(0, 2));
                int discountEndMinute = Integer.parseInt(discountEnd.toString().substring(3, 5));
                int discountEndSecond = Integer.parseInt(discountEnd.toString().substring(6));
                int currentHour = currentTime.getHours();
                int currentMinute = currentTime.getMinutes();
                int currentSecond = currentTime.getSeconds();
                if (currentHour < discountEndHour && currentHour > discountStartHour)
                    pluPrice = pluPmPrice;
                else if (currentHour == discountEndHour || currentHour == discountStartHour)
                    if (currentMinute < discountEndMinute && currentMinute > discountStartMinute)
                        pluPrice = pluPmPrice;
                    else if (currentMinute == discountEndMinute && currentMinute == discountStartMinute && currentSecond <= discountEndSecond && currentSecond >= discountEndSecond)
                        pluPrice = pluPmPrice;
            }
        }
        //  check member price
        if (trans.getMemberID() != null) {
            BigDecimal memberPrice = curPLU.getMemberPrice();
            if (memberPrice != null && pluPrice.compareTo(memberPrice) == 1)
                pluPrice = memberPrice;
        }
        return pluPrice;
    }

    public PLU getPLU(LineItem lineItem) {
        return PLU.queryBarCode(lineItem.getPluNumber());
    }

    public ArrayList getUnitMMedPLU(ArrayList mmedArray, ArrayList curArray, int quantity) {
        BigDecimal expensiveUnitPrice = new BigDecimal(0.0D);
        PLU expensivePLU = (PLU) curArray.get(0);
        PLU plu = null;
        for (int j = 0; j < quantity; j++) {
            for (int i = 0; i < curArray.size(); i++) {
                plu = (PLU) curArray.get(i);
                if (expensiveUnitPrice.compareTo(plu.getUnitPrice()) == -1) {
                    expensiveUnitPrice = plu.getUnitPrice();
                    expensivePLU = plu;
                }
            }

            mmedArray.add(expensivePLU);
            curArray.remove(expensivePLU);
            totalPLU.remove(expensivePLU);
            expensiveUnitPrice = new BigDecimal(0.0D);
            expensivePLU = null;
        }

        return mmedArray;
    }

    public ArrayList getMMedPLU(ArrayList mmedArray, MixAndMatch mm) {
        int grp1Qty = mm.getGroup1Quantity().intValue();
        mmedArray = getUnitMMedPLU(mmedArray, group1Array, grp1Qty);
        if (mm.getGroup2Quantity().intValue() == 0)
            return mmedArray;
        int grp2Qty = mm.getGroup2Quantity().intValue();
        mmedArray = getUnitMMedPLU(mmedArray, group2Array, grp2Qty);
        if (mm.getGroup3Quantity().intValue() == 0)
            return mmedArray;
        int grp3Qty = mm.getGroup3Quantity().intValue();
        mmedArray = getUnitMMedPLU(mmedArray, group3Array, grp3Qty);
        if (mm.getGroup4Quantity().intValue() == 0)
            return mmedArray;
        int grp4Qty = mm.getGroup4Quantity().intValue();
        mmedArray = getUnitMMedPLU(mmedArray, group4Array, grp4Qty);
        if (mm.getGroup5Quantity().intValue() == 0)
            return mmedArray;
        int grp5Qty = mm.getGroup5Quantity().intValue();
        mmedArray = getUnitMMedPLU(mmedArray, group5Array, grp5Qty);
        if (mm.getGroup6Quantity().intValue() == 0) {
            return mmedArray;
        } else {
            int grp6Qty = mm.getGroup6Quantity().intValue();
            mmedArray = getUnitMMedPLU(mmedArray, group6Array, grp6Qty);
            return mmedArray;
        }
    }

    public String getMixAndMatchID(PLU plu) {
        String mmid = "000";
        Iterator it = MMGroup.queryByITEMNO(plu.getInStoreCode());
        if (it == null)
            return null;
        while (it.hasNext()) {
            MMGroup tmp = (MMGroup) it.next();
            String tmpid = tmp.getID();
            if (tmpid.compareTo(mmid) > 0)
                mmid = tmpid;
        }
        return mmid;
    }

    private boolean compMixAndMatchID(String mixAndMatchID, PLU plu) {
        Iterator it = MMGroup.queryByITEMNO(plu.getInStoreCode());
        if (it != null)
            while (it.hasNext()) {
                MMGroup tmp = (MMGroup) it.next();
                if (mixAndMatchID.equals(tmp.getID()))
                    return true;
            }
        return false;
    }

    private String getMMGroupID(PLU plu, String mmID) {
        String Grpid = null;
        System.out.println("plu: " + plu.getInStoreCode() + " mmID : " + mmID);
        Iterator it = MMGroup.queryByITEMNO(plu.getInStoreCode());
        if (it == null)
            return Grpid;
        while (it.hasNext()) {
            MMGroup mm = (MMGroup) it.next();
            System.out.println("mm.getID(): " + mm.getID());
            if (mm.getID().equalsIgnoreCase(mmID))
                Grpid = mm.getGROUPNUM();
        }
        return Grpid;
    }

    public boolean checkGrpQty(MixAndMatch mm) {
        /*System.out.println("group1Array.size() :" + group1Array.size());
        System.out.println("group2Array.size() :" + group2Array.size());
        System.out.println("group3Array.size() :" + group3Array.size());
        System.out.println("group4Array.size() :" + group4Array.size());
        System.out.println("group5Array.size() :" + group5Array.size());
        System.out.println("group6Array.size() :" + group6Array.size());*/
        if (mm.getGroup1Quantity().intValue() == 0)
            return false;
        if (mm.getGroup1Quantity().intValue() > group1Array.size())
            return false;
        if (mm.getGroup2Quantity().intValue() == 0)
            return true;
        if (mm.getGroup2Quantity().intValue() > group2Array.size())
            return false;
        if (mm.getGroup3Quantity().intValue() == 0)
            return true;
        if (mm.getGroup3Quantity().intValue() > group3Array.size())
            return false;
        if (mm.getGroup4Quantity().intValue() == 0)
            return true;
        if (mm.getGroup4Quantity().intValue() > group4Array.size())
            return false;
        if (mm.getGroup5Quantity().intValue() == 0)
            return true;
        if (mm.getGroup5Quantity().intValue() > group5Array.size())
            return false;
        if (mm.getGroup6Quantity().intValue() == 0)
            return true;
        return mm.getGroup6Quantity().intValue() <= group6Array.size();
    }

    private void mixAndMatch2() {
        //Bruce> CStore is nothing to do with this.
        //(new NormalCombPromotion(trans)).match();
    }

    class NewArray {

        public String getOne() {
            return one;
        }

        public void setOne(String one) {
            this.one = one;
        }

        public int getTwo() {
            return two;
        }

        public void setTwo(int two) {
            this.two = two;
        }

        public BigDecimal getThree() {
            return three;
        }

        public void setThree(BigDecimal three) {
            this.three = three;
        }

        private String one;
        private int two;
        private BigDecimal three;

        public NewArray(String one, BigDecimal three) {
            this.one = "";
            two = 0;
            this.three = null;
            this.one = one;
            this.three = three;
        }
    }

    

}

/*******************************************************************************
 * DECOMPILATION REPORT ***
 * 
 * DECOMPILED FROM: D:\workspace7\CStorePOS\classes\cream.jar
 * 
 * 
 * TOTAL TIME: 359 ms
 * 
 * 
 * JAD REPORTED MESSAGES/ERRORS:
 * 
 * 
 * EXIT STATUS: 0
 * 
 * 
 * CAUGHT EXCEPTIONS:
 * 
 ******************************************************************************/
