// Copyright (c) 2000 HYI
package hyi.cream.state;

import java.util.*;
import java.math.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;


/**
 * A Class class.
 * <P>
 * @author dai
 */
public class Numbering2State extends State {

    private String printString          = "";
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static Numbering2State numbering2State = null;

    public static Numbering2State getInstance() {
        try {
            if (numbering2State == null) {
                numbering2State = new Numbering2State();
            }
        } catch (InstantiationException ex) {
        }
        return numbering2State;
    }

    /**
     * Constructor
     */
    public Numbering2State() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState.getClass() == SummaryState.class) {
            printString = "";
        }
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            printString = printString + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(printString);
            app.getWarningIndicator().setMessage("");
        }                         
        app.getWarningIndicator().setMessage("");
        StateMachine.getInstance().setPaymentMenuVisible(true);
    }

    public Class exit(EventObject event, State sinkState) {
        StateMachine.getInstance().setPaymentMenuVisible(false);
        if (event.getSource() instanceof ClearButton) {
            printString = "";
            app.getMessageIndicator().setMessage(printString);
        }
        if (sinkState == null){
            Transaction tran = app.getCurrentTransaction();
            BigDecimal totalAmount = tran.getSalesAmount();
            if (CreamToolkit.checkInput(printString, new BigDecimal(0))) {
                /*BigDecimal payCash = new BigDecimal(printString);
                payCash = payCash.setScale(2);
                if (payCash.compareTo(totalAmount) == -1) {
                    app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("CashNotEnough"));
                    return SummaryState.class;
                } else {
                    tran.setPayCash(payCash);
                    tran.setChangeAmount(payCash.subtract(totalAmount));
                    app.getPayingPane().repaint();*/
                    return Paying1State.class;
                //}
            } else {
                printString = "";
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                return SummaryState.class;
            }
        }
        return sinkState.getClass();
    }

    public String getNumberString() {
        return printString;
    }
}

 
