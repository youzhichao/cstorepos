// Copyright (c) 2000 HYI
package hyi.cream.state;

import java.util.*;
import java.math.*;
import java.awt.*;
import hyi.cream.state.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.*;


/**
 * A Class class.
 * <P>
 * @author dai
 */
public class Numbering3State extends State {

    private String printString;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    static Numbering3State numbering3State = null;

    public static Numbering3State getInstance() {
        try {
            if (numbering3State == null) {
                numbering3State = new Numbering3State();
            }
        } catch (InstantiationException ex) {
        }
        return numbering3State;
    }

    /**
     * Constructor
     */
    public Numbering3State() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (sourceState.getClass() == SummaryState.class) {
            printString = "";
        }
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            printString = printString + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(printString);
        }    
        app.getWarningIndicator().setMessage("");
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof ClearButton) {
            printString = "";
            app.getMessageIndicator().setMessage(printString);
        }
        if (sinkState == null){
            Transaction tran = app.getCurrentTransaction();
            BigDecimal totalAmount = (BigDecimal)tran.getFieldValue("INVAMT");
            if (CreamToolkit.checkInput(printString, new BigDecimal(0))) {
                BigDecimal payCash = new BigDecimal(printString);
                payCash = payCash.setScale(2, 4);
                if (payCash.compareTo(totalAmount) == -1) {
                    app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("CashNotEnough"));
                    return SummaryState.class;
                } else {
                    tran.setPayCash(payCash);
                    tran.setChangeAmount(payCash.subtract(totalAmount));
                    app.getPayingPane().repaint();
                    return Paying1State.class;
                }
            } else {
                printString = "";
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                return Numbering3State.class;
            }
        }
        return Numbering3State.class;
    }

    public String getNumberString() {
        return printString;
    }
}

