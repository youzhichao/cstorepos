/**
 * State class
 * @since 2000
 * @author slackware
 */
package hyi.cream.state;

import java.util.*;
import java.awt.event.*;

import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

public class Numbering4State extends State {

    private String numberString = "";
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    public static final int PLUNO = 0;
    public static final int BARCODE = 1;
    public static final int INSTORECODE = 2;
    private int numberType = PLUNO;

    static Numbering4State numbering4State = null;

    public static Numbering4State getInstance() {
        try {
            if (numbering4State == null) {
                numbering4State = new Numbering4State();
            }
        } catch (InstantiationException ex) {
        }
        return numbering4State;
    }

    /**
     * Constructor
     */
    public Numbering4State() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        Object eventSource = event.getSource();
        if (eventSource instanceof NumberButton) {
			String s = ((NumberButton)eventSource).getNumberLabel();
            if (sourceState instanceof PriceLookupState
                || sourceState instanceof PricingState) {
                numberType = PLUNO;
                numberString = s;
                app.getMessageIndicator().setMessage(numberString);
            } else if (sourceState instanceof Numbering4State) {
                numberString = numberString + s;
                app.getMessageIndicator().setMessage(numberString);
            }
        }
        if (eventSource instanceof BarCodeButton) {
            numberType = BARCODE;
            numberString = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputBarCode"));
        }
        if (eventSource instanceof InStoreCodeButton) {
            numberType = INSTORECODE;
            numberString = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputInStoreCode"));
        }
        app.getWarningIndicator().setMessage("");
    }

	public Class exit(EventObject event, State sinkState) {
        Object eventSource = event.getSource();
        if (eventSource instanceof ClearButton) {
            numberString = "";
            return PriceLookupState.class;
        }

        if (eventSource instanceof EnterButton) {
            if (numberString.equals("")) {
                return Numbering4State.class;
            } else {
                return PricingState.class;
            }
        }

	    if (sinkState != null) {
            return sinkState.getClass();
        } else {
			return null;
        }
	}

    public String getNumberString() {
        return numberString;
    }

    public int getNumberType() {
        return numberType;
    }
}


