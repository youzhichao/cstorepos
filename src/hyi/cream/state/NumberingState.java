// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.event.TransactionEvent;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import jpos.*;

import java.util.*;
import java.math.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class NumberingState extends State {

    private String numberString          = "";
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    static NumberingState numberingState = null;

    private static final int NULL_STATE = 1;
    private static final int DISCOUNT_RATE_ID_STATE = 2;
    private static final int DISCOUNT_RATE_STATE = 3;
    private int state = NULL_STATE;

    public static NumberingState getInstance() {
        try {
            if (numberingState == null) {
                numberingState = new NumberingState();
            }
        } catch (InstantiationException ex) {
        }
        return numberingState;
    }

    /**
     * Constructor
     */
    public NumberingState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("NumberingState entry");
        if (sourceState instanceof IdleState) {
            numberString = "";
            if (event.getSource() instanceof DiscountRateIDButton) {
                app.getMessageIndicator().setMessage(res.getString("PleaseInputDiscountRateID"));
                state = DISCOUNT_RATE_ID_STATE;
            } else if (event.getSource() instanceof DiscountRateButton) {
                app.getMessageIndicator().setMessage(res.getString("PleaseInputDiscountRate"));
                state = DISCOUNT_RATE_STATE;
            } else {
                state = NULL_STATE;
            }
        }
        if (event.getSource() instanceof NumberButton) {
            NumberButton pb = (NumberButton)event.getSource();
            numberString = numberString + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(numberString);
            app.setEnabledPopupMenu(true);
        }

        if (sourceState instanceof WarningState) {    
            app.getMessageIndicator().setMessage(numberString);
        }                
        app.getWarningIndicator().setMessage("");
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("NumberingState exit");

        Object eventSource = event.getSource();

        if (sinkState instanceof WarningState) {
            //numberString = "";
            setWarningMessage(CreamToolkit.GetResource().getString("Warning"));
        }

        //Bruce/20030401 若已经在交易输入过程中，不允许输入会员编号，发出警讯
        if (event.getSource() instanceof MemberIDButton) {
            //Bruce/2003-06-23 若画面上看不到任何商品（例如可能被更正掉），仍允许输入会员编号。
            //if (!app.getTransactionEnd()) {
            if (app.getCurrentTransaction().getDisplayedLineItemsArray().size() != 0) {
                setWarningMessage(res.getString("CannotInputMemberID"));
                numberString = "";
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            }
            Member member = Member.queryByMemberID(numberString);
            if (member == null) {
                setWarningMessage(CreamToolkit.GetResource().getString("WrongMemberID"));
                numberString = "";
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            } else {
                app.getCurrentTransaction().setMemberID(member.getMemberCardID());
                GetMemberNumberState.setMember(member); // will be used by ShowMemberInfoState
                return ShowMemberInfoState.class;
            }
        }

        //Bruce/20030402
        if (event.getSource() instanceof SalesmanButton) {
            Cashier cashierDAC = Cashier.queryByCashierID(numberString);
            if (cashierDAC == null) {
                setWarningMessage(CreamToolkit.GetResource().getString("WrongSalesmanNumber"));
                numberString = "";
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            } else {

                //Bruce/20030430/
                //Add a property "LevelsCanSaleThings". If doesn't exist, don't check; otherwise
                //check if the signon cashier's level match any in "LevelsCanSaleThings"
                String levels = CreamProperties.getInstance().getProperty("LevelsCanSaleThings");
                if (levels != null) {
                    String cashierLevel = cashierDAC.getCashierLevel();
                    StringTokenizer tk = new StringTokenizer(levels, ", ");
                    while (tk.hasMoreTokens()) {
                        String level = tk.nextToken();
                        if (cashierLevel.equals(level)) {
                            app.getSystemInfo().setSalesManNumber(numberString);
                            app.getCurrentTransaction().setSalesman(numberString);
                            app.getCurrentTransaction().fireEvent(new TransactionEvent(app.getCurrentTransaction(), TransactionEvent.TRANS_INFO_CHANGED));
                            return IdleState.class;
                        }
                    }
                    // Cannot match any level in "LevelsCanSignOn"
                    setWarningMessage(CreamToolkit.GetResource().getString("WrongSalesmanNumber"));
                    numberString = "";
                    WarningState.setExitState(IdleState.class);
                    return WarningState.class;
                }
                app.getSystemInfo().setSalesManNumber(numberString);
                app.getCurrentTransaction().setSalesman(numberString);
                app.getCurrentTransaction().fireEvent(new TransactionEvent(app.getCurrentTransaction(), TransactionEvent.TRANS_INFO_CHANGED));
                return IdleState.class;
            }
        }

        if (eventSource instanceof PluButton
            || eventSource instanceof jpos.Scanner) {
            String pluMaxQuantity = CreamProperties.getInstance().getProperty("PluMaxQuantity");
            //System.out.println(pluMaxQuantity);
            if (numberString.equals("")) {
                numberString = "0";
            }
            if (CreamToolkit.checkInput(numberString, 0)) {
                if (Integer.parseInt(numberString) <= 0) {
                    setWarningMessage(CreamToolkit.GetResource().getString("number1"));
                    WarningState.setExitState(IdleState.class);
                    return WarningState.class;
                } else if (Integer.parseInt(numberString) >= Integer.parseInt(pluMaxQuantity)) {
                    setWarningMessage(CreamToolkit.GetResource().getString("number2") + pluMaxQuantity);
                    WarningState.setExitState(IdleState.class);
                    return WarningState.class;
                } else {
                    return PluReadyState.class;
                }
            } else {                        
                numberString = "";
                app.getWarningIndicator().setMessage(res.getString("InputWrong"));
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            }
		}

        if (event.getSource() instanceof QuantityButton) {
            String pluMaxQuantity = CreamProperties.getInstance().getProperty("PluMaxQuantity");
            if (numberString.equals("")) {
                numberString = "1";
            }
            if (CreamToolkit.checkInput(numberString, 0)) {
                if (Integer.parseInt(numberString) <= 0) {
                    numberString = "";
                    setWarningMessage(CreamToolkit.GetResource().getString("number1"));
                    WarningState.setExitState(IdleState.class);
                    return WarningState.class;
                } else if (Integer.parseInt(numberString) >= Integer.parseInt(pluMaxQuantity)) { 
                    numberString = "";
                    setWarningMessage(CreamToolkit.GetResource().getString("number2") + pluMaxQuantity);
                    WarningState.setExitState(IdleState.class);
                    return WarningState.class;
                } else {
                    return SetQuantityState.class;
                }
            } else {
                numberString = "";
                app.getWarningIndicator().setMessage(res.getString("InputWrong"));
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            }
		}

        if (event.getSource() instanceof TaxedAmountButton) {
            if (!CreamToolkit.checkInput(numberString, new BigDecimal(0))) {
                numberString = "";
                //app.getMessageIndicator().setMessage(res.getString("InputNumber"));
                app.getWarningIndicator().setMessage(res.getString("InputWrong"));
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            }

            BigDecimal price = new BigDecimal(numberString);
            price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
            if (price.compareTo(new BigDecimal(CreamProperties.getInstance().getProperty("MaxPrice"))) == 1) {
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyPrice"));
                numberString = "";
                return IdleState.class;
            } else {
                return PluReadyState.class;
            }
        }

        if (event.getSource() instanceof RemoveButton) { 
            if (!CreamToolkit.checkInput(numberString, 0)) {
                numberString = "";
                //app.getMessageIndicator().setMessage(res.getString("InputNumber"));
                app.getWarningIndicator().setMessage(res.getString("InputWrong"));
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            }
            
            if (numberString.equals("")) {
                return NumberingState.class;
            } else {
                return VoidState.class;
            }
        }        

        if (event.getSource() instanceof InStoreCodeButton
            || event.getSource() instanceof BarCodeButton
            || (event.getSource() instanceof EnterButton && state == NULL_STATE)) {
            /*if (!CreamToolkit.checkInput(numberString, 0)) {
                numberString = "";
                //app.getMessageIndicator().setMessage(res.getString("InputNumber"));
                app.getWarningIndicator().setMessage(res.getString("InputWrong"));
                return IdleState.class;
            } else {*/
                return PluReadyState.class;
            //}
        }

        if (event.getSource() instanceof DiscountRateIDButton
            || (event.getSource() instanceof EnterButton && state == DISCOUNT_RATE_ID_STATE)) {
            LineItem curLineItem = app.getCurrentTransaction().getCurrentLineItem();
            if (curLineItem == null 
                || curLineItem.getPrinted() 
                || !curLineItem.getDetailCode().equals("S") 
                || curLineItem.getRemoved()) {
                numberString = "";
                setWarningMessage(CreamToolkit.GetResource().getString("NoDiscountItem"));
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            }

            DiscountType dis = DiscountType.queryByID(numberString);
            if (dis == null) {
                numberString = "";
                setWarningMessage(CreamToolkit.GetResource().getString("NoSuchDiscountID"));
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            }

            curLineItem.setDiscountRateID(dis.getID());
            curLineItem.setDiscountRate(dis.getUpLimit());
            curLineItem.setUnitPrice(curLineItem.getOriginalPrice().multiply(dis.getUpLimit()));
            curLineItem.setAmount(curLineItem.getUnitPrice().multiply(curLineItem.getQuantity()));
            curLineItem.setAfterDiscountAmount(curLineItem.getUnitPrice().multiply(curLineItem.getQuantity()));
            curLineItem.setDiscountType("D");

            try {
                app.getCurrentTransaction().changeLineItem(-1, curLineItem);
            } catch (LineItemNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("LineItem not found at " + this);
            }
            CreamToolkit.showText(app.getCurrentTransaction(), 0);
            return IdleState.class;
        }

        if (event.getSource() instanceof DiscountRateButton
            || (event.getSource() instanceof EnterButton && state == DISCOUNT_RATE_STATE)) {
            LineItem curLineItem = app.getCurrentTransaction().getCurrentLineItem();
            if (curLineItem == null 
                || curLineItem.getPrinted() 
                || !curLineItem.getDetailCode().equals("S") 
                || curLineItem.getRemoved()) {
                numberString = "";
                setWarningMessage(CreamToolkit.GetResource().getString("NoDiscountItem"));
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            }

            DiscountType dis = null;
            BigDecimal rate = null;
            try {
                rate = new BigDecimal(numberString);
                if (rate.compareTo(new BigDecimal(1)) > 0 || rate.compareTo(new BigDecimal(0)) < 0) {
                    throw new NumberFormatException("");
                }
            } catch (NumberFormatException e) {
                numberString = "";
                setWarningMessage(CreamToolkit.GetResource().getString("DiscountRateError"));
                WarningState.setExitState(IdleState.class);
                return WarningState.class;
            }
            dis = DiscountType.queryByDiscountRate(rate);
            String discountID = (dis != null) ? dis.getID() : "";

            curLineItem.setDiscountRateID(discountID);
            curLineItem.setDiscountRate(rate);
            curLineItem.setUnitPrice(curLineItem.getOriginalPrice().multiply(rate));
            curLineItem.setAmount(curLineItem.getUnitPrice().multiply(curLineItem.getQuantity()));
            curLineItem.setAfterDiscountAmount(curLineItem.getUnitPrice().multiply(curLineItem.getQuantity()));
            curLineItem.setDiscountType("D");

            try {
                app.getCurrentTransaction().changeLineItem(-1, curLineItem);
            } catch (LineItemNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("LineItem not found at " + this);
            }
            CreamToolkit.showText(app.getCurrentTransaction(), 0);
            return IdleState.class;
        }

		if (sinkState != null)
			return sinkState.getClass();
		else
		    return null;
    }

    public String getNumberString() {
        return numberString;
    }
}

 
