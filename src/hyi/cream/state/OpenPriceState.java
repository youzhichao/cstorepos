
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;
import java.math.*;
import jpos.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class OpenPriceState extends State {

    private String openPrice            = "";
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static OpenPriceState openPriceState = null;
	private Thread warningThread = null;
	private ToneIndicator tone          = null;

    public static OpenPriceState getInstance() {
        try {
            if (openPriceState == null) {
                openPriceState = new OpenPriceState();
            }
        } catch (InstantiationException ex) {
        }
        return openPriceState;
    }

    /**
     * Constructor
     */
    public OpenPriceState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
		//System.out.println("This is OpenState's Entry!");
		if (sourceState instanceof PluReadyState) {         
            openPrice = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
        } else if (sourceState instanceof OpenPriceState) {
			if (event.getSource() instanceof ClearButton) {
			    try {
					if (tone != null) {
						tone.clearOutput();
						tone.release();
					}
				} catch (JposException je) { }
                openPrice = "";              
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
                app.getWarningIndicator().setMessage("");
            } else if (event.getSource() instanceof NumberButton) {
                try {
					if (tone != null) {
						tone.clearOutput();
						tone.release();
					}
				} catch (JposException je) { }
                NumberButton pb = (NumberButton)event.getSource();
                openPrice = openPrice + pb.getNumberLabel();  
                app.getMessageIndicator().setMessage(openPrice);
                app.getWarningIndicator().setMessage("");
            }
        }
    }


    public Class exit(EventObject event, State sinkState) {
        //System.out.println("OpenPriceState exit");
        BigDecimal price = null;
        //app.getMessageIndicator().setMessage("");
        if (event.getSource() instanceof EnterButton) {
            Transaction curTran = app.getCurrentTransaction();  
            if (openPrice.equals("")) {
                price = curTran.getCurrentLineItem().getUnitPrice();
                if (price.compareTo(new BigDecimal(0)) == 0) {
					//System.out.println("########################################");
					POSPeripheralHome posHome = POSPeripheralHome.getInstance();
					try {
						tone = posHome.getToneIndicator();
					} catch (NoSuchPOSDeviceException ne) { CreamToolkit.logMessage(ne + "!"); }
					try {
						if (!tone.getDeviceEnabled())
							tone.setDeviceEnabled(true);
						if (!tone.getClaimed()) {
							 tone.claim(0);
						//tone.claim(0);
							 tone.setAsyncMode(true);
							 tone.sound(99999, 500);
						}
					} catch (JposException je) {System.out.println(je);}
                    app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
                    return OpenPriceState.class;
                }
            } else {          

                if (!CreamToolkit.checkInput(openPrice, new BigDecimal(0))) {
                    openPrice = "";
                    app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                    return OpenPriceState.class;
                }
                price = new BigDecimal(openPrice);
                if (price.compareTo(new BigDecimal(CreamProperties.getInstance().getProperty("MaxPrice"))) == 1) {
                    POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyPrice"));
                    openPrice = "";
                    return OpenPriceState.class;
                }
            }
            LineItem lineItem = curTran.getCurrentLineItem();

            lineItem.setUnitPrice(price.setScale(2, BigDecimal.ROUND_HALF_UP));
    		lineItem.setOriginalPrice(price.setScale(2, BigDecimal.ROUND_HALF_UP));

            lineItem.setAfterDiscountAmount(lineItem.getAmount());
            
            try {
                curTran.changeLineItem(-1, lineItem);
            } catch (LineItemNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("LineItem not found at " + this);
			}
			try {
				if (tone != null) {
					tone.clearOutput();
					tone.release();
				}
			} catch (JposException je) { } 
            app.getWarningIndicator().setMessage("");
			return MixAndMatchState.class;
        } else {
            if (!(event.getSource() instanceof NumberButton)
				&& !(event.getSource() instanceof ClearButton)) {
				//System.out.println("########################################");
				POSPeripheralHome posHome = POSPeripheralHome.getInstance();
				try {
					tone = posHome.getToneIndicator();
				} catch (NoSuchPOSDeviceException ne) { CreamToolkit.logMessage(ne + "!"); }
				try {
					if (!tone.getDeviceEnabled())
						tone.setDeviceEnabled(true);
					if (!tone.getClaimed()) {
						 tone.claim(0);
					//tone.claim(0);
						 tone.setAsyncMode(true);
						 tone.sound(99999, 500);
					}
				} catch (JposException je) {System.out.println(je);}
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
			}
			return OpenPriceState.class;
		}
	}
}

 


