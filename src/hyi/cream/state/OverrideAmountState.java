package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;
import java.math.*;

/**
 * A Class class.
 * <P>
 * @author zzy
 */
public class OverrideAmountState extends State {
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private String tempPrice = "";
    private Transaction trans = app.getCurrentTransaction();
	static OverrideAmountState overrideAmountState = null;
    private LineItem curLineItem = null;
    private boolean computeDiscountRate;

    public static OverrideAmountState getInstance() {
        try {
            if (overrideAmountState == null) {
                overrideAmountState = new OverrideAmountState();
            }
        } catch (InstantiationException ex) {
        }
        return overrideAmountState;
    }
	
	public OverrideAmountState() throws InstantiationException {
        computeDiscountRate = CreamProperties.getInstance().getProperty("ComputeDiscountRate", "no")
            .equalsIgnoreCase("yes");
    }
	
	public void entry(EventObject event, State sourceState) {
		curLineItem = trans.getCurrentLineItem();
        if (sourceState instanceof CashierRightsCheckState)
			app.getMessageIndicator().setMessage("请输入价格，按[确认]键结束");
        //if (sourceState instanceof Validate)
        //    app.getMessageIndicator().setMessage("请输入价格，按[回车]键结束");
	}
	
	public Class exit(EventObject event, State sinkState) {
		if (event.getSource() instanceof NumberButton) {
			NumberButton pb = (NumberButton)event.getSource();
                //System.out.println(" OverrideAmountState exit number" + pb.getNumberLabel());
			if (pb.getNumberLabel().equals("-") || (pb.getNumberLabel().equals(".") && tempPrice.length() == 0)) {
                //System.out.println(" OverrideAmountState exit number 1 " +tempPrice);
			} else { 
				tempPrice = tempPrice + pb.getNumberLabel();
                //System.out.println(" OverrideAmountState exit number 2" +tempPrice);
			}
			app.getMessageIndicator().setMessage(tempPrice);
			return OverrideAmountState.class;
		}

        if (event.getSource() instanceof ClearButton) {
            if (tempPrice.length() == 0)
                return IdleState.class;
            tempPrice = "";
            app.getMessageIndicator().setMessage(tempPrice);
            return OverrideAmountState.class;
        }

		if (event.getSource() instanceof EnterButton) {
            //System.out.println(" OverrideAmountState exit enterbutton" +tempPrice);
            
//            if (app.getReturnSaleMode()) {
//                curLineItem.setUnitPrice(new BigDecimal(tempPrice));
//                curLineItem.setAmount(curLineItem.getUnitPrice().multiply(curLineItem.getQuantity()));
//                curLineItem.setAfterDiscountAmount(curLineItem.getAmount());
//                TaxType taxType = TaxType.queryByTaxID(curLineItem.getTaxType());
//                // acculate tax
//                int round = 0;
//                String taxRound = taxType.getRound();
//                if (taxRound.equalsIgnoreCase("R")) {
//                    round = BigDecimal.ROUND_HALF_UP;
//                } else if (taxRound.equalsIgnoreCase("C")) {
//                    round = BigDecimal.ROUND_UP;
//                } else if (taxRound.equalsIgnoreCase("D")) {
//                    round = BigDecimal.ROUND_DOWN;
//                }
//                String taxTp = taxType.getType();
//                BigDecimal taxPercent = taxType.getPercent();
//                BigDecimal taxAmount = new BigDecimal(0);
//                int taxDigit = taxType.getDecimalDigit().intValue();
//                if (taxTp.equalsIgnoreCase("1")) {
//                    BigDecimal unit = new BigDecimal("1");
//                    taxAmount = ((curLineItem.getAmount().multiply(taxPercent)).divide(
//                            unit.add(taxPercent),2,round));
//                } else if (taxTp.equalsIgnoreCase("2")) {
//                    BigDecimal unit = new BigDecimal("1");
//                    taxAmount = (curLineItem.getAmount().multiply(taxPercent)).setScale(taxDigit, round);
//                } else {
//                    BigDecimal tax = new BigDecimal("0.00");
//                    taxAmount = tax.setScale(taxDigit, round);
//                }
//                //- acculate tax
//                curLineItem.setAfterSIAmount(curLineItem.getAmount());
//                curLineItem.setTaxAmount(taxAmount);
//    
//                curLineItem.setDiscountType("O");
//                try {
//                    app.getCurrentTransaction().changeLineItem(-1, curLineItem);
//                } catch (LineItemNotFoundException e) {
//                    CreamToolkit.logMessage(e.toString());
//                    CreamToolkit.logMessage("LineItem not found at " + this);
//                }
//                CreamToolkit.showText(app.getCurrentTransaction(), 0);
//                tempPrice = "";
//                return IdleState.class;
//            }

            if (tempPrice.trim().length() <= 0)
                tempPrice = "0";

            // Check number format
            try {
                BigDecimal d = new BigDecimal(tempPrice);
                //Bruce/20030826/
                //检查是否为一个很大或很小的数
                if (d.compareTo(new BigDecimal("999999.99")) > 0
                    || d.compareTo(new BigDecimal("-999999.99")) < 0)
                    throw new NumberFormatException("");
            } catch (NumberFormatException e) {
                app.getMessageIndicator().setMessage("请输入价格，按[确认]键结束");
                tempPrice = "";
                return OverrideAmountState.class;
            }

            //Bruce/20030324
            //if (CreamProperties.getInstance().getProperty("CanOverrideAmount").equalsIgnoreCase("yes")) {
            String overrideAmountRangeLimit =
                CreamProperties.getInstance().getProperty("OverrideAmountRangeLimit");
            if (overrideAmountRangeLimit != null) {
                BigDecimal pluRangeValue = curLineItem.getOriginalPrice().multiply(
                    new BigDecimal(CreamProperties.getInstance().getProperty("OverrideAmountRangeLimit")));
                BigDecimal plusRangePrice =
                    curLineItem.getOriginalPrice().add(pluRangeValue);
                BigDecimal minusRangePrice =
                    curLineItem.getOriginalPrice().subtract(pluRangeValue);
                System.out.println(
                    "OverrideAmountState plusRangePrice = "
                        + plusRangePrice
                        + " minusRangePrice = "
                        + minusRangePrice);
                if ((new BigDecimal(tempPrice)).compareTo(plusRangePrice) > 0
                    || (new BigDecimal(tempPrice)).compareTo(minusRangePrice) < 0) {
                    app.getMessageIndicator().setMessage("请输入价格，按[确认]键结束");
                    tempPrice = "";
                    return OverrideAmountState.class;
                }
            }

            if (tempPrice.trim().length() > 0)
    			curLineItem.setUnitPrice(new BigDecimal(tempPrice));
            else
                curLineItem.setUnitPrice(new BigDecimal("0.00"));

            curLineItem.setQuantity(new BigDecimal("1.00"));
			curLineItem.setAmount(curLineItem.getUnitPrice().multiply(curLineItem.getQuantity()));
            curLineItem.setAfterDiscountAmount(curLineItem.getAmount());
            curLineItem.setAddRebateAmount(curLineItem.getAfterDiscountAmount().multiply(curLineItem.getRebateRate()));
            
            TaxType taxType = TaxType.queryByTaxID(curLineItem.getTaxType());
            // acculate tax
            int round = 0;
            String taxRound = taxType.getRound();
            if (taxRound.equalsIgnoreCase("R")) {
                round = BigDecimal.ROUND_HALF_UP;
            } else if (taxRound.equalsIgnoreCase("C")) {
                round = BigDecimal.ROUND_UP;
            } else if (taxRound.equalsIgnoreCase("D")) {
                round = BigDecimal.ROUND_DOWN;
            }
            String taxTp = taxType.getType();
            BigDecimal taxPercent = taxType.getPercent();
            BigDecimal taxAmount = new BigDecimal(0);
            int taxDigit = taxType.getDecimalDigit().intValue();
            if (taxTp.equalsIgnoreCase("1")) {
                BigDecimal unit = new BigDecimal("1");
                taxAmount = ((curLineItem.getAmount().multiply(taxPercent)).divide(
                            unit.add(taxPercent),2,round));
            } else if (taxTp.equalsIgnoreCase("2")) {
                //BigDecimal unit = new BigDecimal("1");
                taxAmount = (curLineItem.getAmount().multiply(taxPercent)).setScale(taxDigit, round);
            } else {
                BigDecimal tax = new BigDecimal("0.00");
                taxAmount = tax.setScale(taxDigit, round);
            }
            //- acculate tax
            curLineItem.setAfterSIAmount(curLineItem.getAmount());
            curLineItem.setTaxAmount(taxAmount);
            curLineItem.setDiscountType("O");
            
            //Bruce/2003-08-08
            if (computeDiscountRate) {
                try {
                    BigDecimal rate = curLineItem.getUnitPrice().divide(curLineItem.getOriginalPrice(),
                        2, BigDecimal.ROUND_HALF_UP);
                    curLineItem.setDiscountRate(rate);
                    DiscountType dis = DiscountType.queryByDiscountRate(rate);
                    if (dis != null) {
                        curLineItem.setDiscountRateID(dis.getID());
                    }
                } catch (ArithmeticException e) {
                //初始价格为0，不设置折扣信息 zhaohong 2003-10-30     
                }
            }
                
            try {
                app.getCurrentTransaction().changeLineItem(-1, curLineItem);
            } catch (LineItemNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("LineItem not found at " + this);
            }
            CreamToolkit.showText(app.getCurrentTransaction(), 0);
			tempPrice = "";
			return IdleState.class;
		}
	
		return OverrideAmountState.class;
	}
}