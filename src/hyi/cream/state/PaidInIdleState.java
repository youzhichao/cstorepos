// Copyright (c) 2000 HYI
package hyi.cream.state;

import java.util.*;
import java.math.*;
import java.text.*;
import java.awt.*;
import jpos.*;

import hyi.cream.state.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.*;
import hyi.cream.event.*;
import hyi.cream.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PaidInIdleState extends State implements PopupMenuListener {
    private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private Transaction trans             = app.getCurrentTransaction();
	private ResourceBundle res            = CreamToolkit.GetResource(); 
    private PopupMenuPane p               = app.getPopupMenuPane();
    private ArrayList reasonArray         = new ArrayList();
    private String pluNo                  = "";
    private PaidInButton paidInButton   = null;
    
    static PaidInIdleState paidInIdleState      = null;

    public static PaidInIdleState getInstance() {
        try {
            if (paidInIdleState == null) {
                paidInIdleState = new PaidInIdleState();
            }
        } catch (InstantiationException ex) {
        }
        return paidInIdleState;
    }

    /**
     * Constructor
     */
    public PaidInIdleState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
        //System.out.println("PaidInIdleState entry");

        if (event != null && event.getSource() instanceof PaidInButton) {
            paidInButton = ((PaidInButton)event.getSource());
            ArrayList menu = new ArrayList();
            Iterator paidInItem = Reason.queryByreasonCategory("10");
            if (paidInItem == null)
                return;
            Reason r = null;
            //String menuString = "";
            //PLU plu = null;
            int i = 1;
            while (paidInItem.hasNext()) {
                r = (Reason)paidInItem.next();
                reasonArray.add(r);
                menu.add(i + "." + r.getreasonName());
                i++;
            }

            p.setMenu(menu);
            p.setVisible(true);
            p.addToList(this);

            trans.setDealType2("4");
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSelect"));
        } else {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PaidInSelect"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("PaidInIdleState exit");
        
		app.getMessageIndicator().setMessage("");
        app.getWarningIndicator().setMessage("");

        Iterator paidInItem = Reason.queryByreasonCategory("10");
        if (paidInItem == null)
            return IdleState.class;

        //  check enter button
        if (event.getSource() instanceof EnterButton) {
                                     
            //  accounting daifu amount and count
            Object lineItems[] = trans.getLineItemsArrayLast();
            LineItem lineItem = null;
            BigDecimal amount = new BigDecimal(0);
            int count = 0;
            for (int i = 0; i < lineItems.length; i++) {
                lineItem = (LineItem)lineItems[i];
                amount = amount.add(lineItem.getAmount());
                count = count + 1;
            }              

            //  set to transaction
            trans.setDaiShouAmount(amount);

            return SummaryState.class;
        }

        //  check cancel button
        if (event.getSource() instanceof CancelButton) {
            return CancelState.class;
        }

        //  check select button
        if (event.getSource() instanceof SelectButton) {
            return PaidInReadyState.class;
        }

        //  check clear button
        if (event.getSource() instanceof ClearButton) {
            if (trans.getCurrentLineItem() == null) {

                trans.setDealType2("0");
                return IdleState.class;
            } else {
                return PaidInIdleState.class;
            }
        }

        return sinkState.getClass();
    }

    public String getPluNo() {
        return pluNo;
    }

    public void doSomething() {
        if (p.getSelectedMode()) {
            int index = p.getSelectedNumber();
            pluNo = ((Reason)reasonArray.get(index)).getreasonNumber();
            POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        }
    }
}

