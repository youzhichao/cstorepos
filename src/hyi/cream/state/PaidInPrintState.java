/**
 * State class
 * @since 2000
 * @author slackware
 */
 

package hyi.cream.state;

//for event processing
import java.util.*;
import hyi.cream.*;
import jpos.*;

//for transaction
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;

//for print
import hyi.cream.util.*;

//for maths calculation
import java.math.*;

public class PaidInPrintState extends State {
    static PaidInPrintState paidInPrintState = null;

    public static PaidInPrintState getInstance() {
        try {
            if (paidInPrintState == null) {
                paidInPrintState = new PaidInPrintState();
            }
        } catch (InstantiationException ex) {
        }
        return paidInPrintState;
    }

    /**
     * Constructor
     */
    public PaidInPrintState() throws InstantiationException {
    }

	public void entry (EventObject event, State sourceState) {
        //System.out.println("PaidInPrintState entry!");
        
		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        if (posTerminal.getCurrentTransaction().getDealType2().equals("4")) {
            CreamToolkit.showText(posTerminal.getCurrentTransaction(), 0);
            return;
        }

		if (sourceState instanceof PaidInOpenPriceState) {
			Transaction cTransaction = posTerminal.getCurrentTransaction();
			Object[] lineItemArrayLast = cTransaction.getLineItemsArrayLast();
			if (lineItemArrayLast.length > 1) {
				if (!CreamPrinter.getInstance().getHeaderPrinted() ) {
					CreamPrinter.getInstance().printHeader();
					CreamPrinter.getInstance().setHeaderPrinted(true);
			    }
				LineItem lineItem = (LineItem)lineItemArrayLast[lineItemArrayLast.length-1];
                if (lineItem.getDetailCode().equals("V"))
					CreamPrinter.getInstance().printLineItem(lineItem);
				lineItem = (LineItem)lineItemArrayLast[lineItemArrayLast.length-2];
				CreamPrinter.getInstance().printLineItem(lineItem);
			}
			if (!cTransaction.getBuyerNumber().equals("")
				&& !posTerminal.getBuyerNumberPrinted()) {
				posTerminal.setBuyerNumberPrinted(true);
			}
		}
	}

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("PaidInPrintState exit!");

        if (sinkState != null)
            return sinkState.getClass();
        else
            return null;
	}          
}

