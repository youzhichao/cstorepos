
// Copyright (c) 2000 HYI
package hyi.cream.state;

import java.util.*;
import java.math.*;
import java.text.*;
import java.awt.*;
import jpos.*;

import hyi.cream.state.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.*;
import hyi.cream.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PaidInReadyState extends State {        
    private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private Transaction curTran           = app.getCurrentTransaction(); 
	private ResourceBundle res            = CreamToolkit.GetResource();
    private LineItem lineItem             = null;
    private Reason r                      = null;
    private String number                 = "";
    private boolean deviceClaimed         = false;
    private Class exitState               = null;

    static PaidInReadyState paidInReadyState      = null;  

    public static PaidInReadyState getInstance() {
        try {
            if (paidInReadyState == null) {
                paidInReadyState = new PaidInReadyState();
            }
        } catch (InstantiationException ex) {
        }
        return paidInReadyState;
    }

    /**
     * Constructor
     */
    public PaidInReadyState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
        //System.out.println("PaidInReadyState entry");
        
		try {
            if (sourceState instanceof PaidInIdleState
                && event.getSource() instanceof SelectButton) {
                number = ((PaidInIdleState)sourceState).getPluNo();
                //System.out.println("number = " + number);
                r = Reason.queryByreasonNumberAndreasonCategory(number, "10");
                lineItem = new LineItem();
                lineItem.setDescription(r.getreasonName());
                lineItem.setDetailCode("N");


                lineItem.setPluNumber(String.valueOf(number));
                lineItem.setQuantity(new BigDecimal(0));
                //lineItem.setTaxType("0");
                lineItem.setTerminalNumber(curTran.getTerminalNumber());
                lineItem.setTransactionNumber(new Integer(curTran.getNextTransactionNumber()));
                
                lineItem.setUnitPrice(new BigDecimal("0.00"));

				/*
				 * Meyer/2003-02-21/
				 */
				lineItem.setItemNumber(String.valueOf(number));  
				lineItem.setOriginalPrice(new BigDecimal("0.00"));            

                lineItem.setAmount(new BigDecimal("0.00")); 
                try {
                    curTran.addLineItem(lineItem);
                    exitState = PaidInOpenPriceState.class;
                } catch (TooManyLineItemsException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("Too many LineItem exception at " + this);
                    POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyLineItems"));
                    exitState = PaidInIdleState.class;
                }
            }
        } catch (InstantiationException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Instantiation LineItem exception at " + this);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("PaidInReadyState exit");

		app.getMessageIndicator().setMessage("");
        return exitState;
    }
}

