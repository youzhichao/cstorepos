// Copyright (c) 2000 HYI
package hyi.cream.state;

import java.util.*;
import java.math.*;
import java.text.*;
import java.awt.*;
import jpos.*;

import hyi.cream.state.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.*;
import hyi.cream.event.*;
import hyi.cream.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PaidOutIdleState extends State implements PopupMenuListener {
    private POSTerminalApplication app    = POSTerminalApplication.getInstance();
    private Transaction trans             = app.getCurrentTransaction();
	private ResourceBundle res            = CreamToolkit.GetResource(); 
    private PopupMenuPane p               = app.getPopupMenuPane();
    private ArrayList reasonArray         = new ArrayList();
    private String pluNo                  = "";
    private PaidOutButton paidOutButton   = null;
    
    static PaidOutIdleState paidOutIdleState      = null;

    public static PaidOutIdleState getInstance() {
        try {
            if (paidOutIdleState == null) {
                paidOutIdleState = new PaidOutIdleState();
            }
        } catch (InstantiationException ex) {
        }
        return paidOutIdleState;
    }

    /**
     * Constructor
     */
    public PaidOutIdleState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
        //System.out.println("PaidOutIdleState entry");

        if (event != null && event.getSource() instanceof PaidOutButton) {
            paidOutButton = ((PaidOutButton)event.getSource());
            ArrayList menu = new ArrayList();
            Iterator paidOutItem = Reason.queryByreasonCategory("11");
            if (paidOutItem == null)
                return;
            Reason r = null;
            //String menuString = "";
            //PLU plu = null;
            int i = 1;
            while (paidOutItem.hasNext()) {
                r = (Reason)paidOutItem.next();
                reasonArray.add(r);
                menu.add(i + "." + r.getreasonName());
                i++;
            }

            p.setMenu(menu);
            p.setVisible(true);
            p.addToList(this);

            trans.setDealType2("4");
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSelect"));
        } else {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PaidOutSelect"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("PaidOutIdleState exit");
        
		app.getMessageIndicator().setMessage("");
        app.getWarningIndicator().setMessage("");

        Iterator paidOutItem = Reason.queryByreasonCategory("11");
        if (paidOutItem == null)
            return IdleState.class;

        //  check enter button
        if (event.getSource() instanceof EnterButton) {
                                     
            //  accounting daifu amount and count
            Object lineItems[] = trans.getLineItemsArrayLast();
            LineItem lineItem = null;
            BigDecimal amount = new BigDecimal(0);
            int count = 0;
            for (int i = 0; i < lineItems.length; i++) {
                lineItem = (LineItem)lineItems[i];
                amount = amount.add(lineItem.getAmount());
                count = count + 1;
            }              

            //  set to transaction
            trans.setDaiFuAmount(amount);

            return SummaryState.class;
        }

        //  check cancel button
        if (event.getSource() instanceof CancelButton) {
            return CancelState.class;
        }

        //  check select button
        if (event.getSource() instanceof SelectButton) {
            return PaidOutReadyState.class;
        }

        //  check clear button
        if (event.getSource() instanceof ClearButton) {
            if (trans.getCurrentLineItem() == null) {

                trans.setDealType2("0");
                return IdleState.class;
            } else {
                return PaidOutIdleState.class;
            }
        }

        return sinkState.getClass();
    }

    public String getPluNo() {
        return pluNo;
    }

    public void doSomething() {
        if (p.getSelectedMode()) {
            int index = p.getSelectedNumber();
            pluNo = ((Reason)reasonArray.get(index)).getreasonNumber();
            POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        }
    }
}

