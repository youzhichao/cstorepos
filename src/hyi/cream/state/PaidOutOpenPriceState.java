// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;
import java.math.*;
import jpos.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PaidOutOpenPriceState extends State {

    private String openPrice            = "";
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    private Transaction curTran         = app.getCurrentTransaction();
	private Thread warningThread        = null;
	private ToneIndicator tone          = null;
    
    static PaidOutOpenPriceState paidOutOpenPriceState = null;

    public static PaidOutOpenPriceState getInstance() {
        try {
            if (paidOutOpenPriceState == null) {
                paidOutOpenPriceState = new PaidOutOpenPriceState();
            }
        } catch (InstantiationException ex) {
        }
        return paidOutOpenPriceState;
    }

    /**
     * Constructor
     */
    public PaidOutOpenPriceState() throws InstantiationException {
    }

	public void entry(EventObject event, State sourceState) {
		//System.out.println("PaidOutOpenPriceState entry!");

        Object eventSource = null;
        if (event != null) {
            eventSource = event.getSource();
        }

		if (sourceState instanceof PaidOutReadyState) {
            openPrice = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
        }

        if (sourceState instanceof PaidOutOpenPriceState
			&& eventSource instanceof ClearButton) {
            try {
                if (tone != null) {
                    tone.clearOutput();
                    tone.release();
                }
            } catch (JposException je) { }
            openPrice = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
            app.getWarningIndicator().setMessage("");
        }

        if (sourceState instanceof PaidOutOpenPriceState
            && eventSource instanceof NumberButton) {
            try {
                if (tone != null) {
                    tone.clearOutput();
                    tone.release();
                }
            } catch (JposException je) { }
            NumberButton pb = (NumberButton)event.getSource();
            openPrice = openPrice + pb.getNumberLabel();
            app.getMessageIndicator().setMessage(openPrice);
            app.getWarningIndicator().setMessage("");
        }
    }


    public Class exit(EventObject event, State sinkState) {
        //System.out.println("PaidOutOpenPriceState exit");

        BigDecimal price = null;
        //app.getMessageIndicator().setMessage("");
        if (event.getSource() instanceof EnterButton) { 

            if (openPrice.equals("")) {
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                return PaidOutOpenPriceState.class;
            }

            if (!CreamToolkit.checkInput(openPrice, new BigDecimal(0))) {
                openPrice = "";
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                return PaidOutOpenPriceState.class;
            }
            
            price = new BigDecimal(openPrice);
            if (price.compareTo(new BigDecimal(CreamProperties.getInstance().getProperty("MaxPrice"))) == 1) {
                POSTerminalApplication.getInstance().getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("TooManyPrice"));
                openPrice = "";
                return PaidOutOpenPriceState.class;
            }
            LineItem lineItem = curTran.getCurrentLineItem();
            lineItem.setUnitPrice(price.setScale(2, BigDecimal.ROUND_HALF_UP).negate());
			/*
			 * Meyer/2003-02-21/
			 */
			lineItem.setOriginalPrice(price.setScale(2, BigDecimal.ROUND_HALF_UP).negate()); 
			
            lineItem.setAmount(price.setScale(2, BigDecimal.ROUND_HALF_UP).negate());
            try {
                curTran.changeLineItem(-1, lineItem);
            } catch (LineItemNotFoundException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("LineItem not found at " + this);
			}
			try {
				if (tone != null) {
					tone.clearOutput();
					tone.release();
				}
			} catch (JposException je) { }
            app.getWarningIndicator().setMessage("");
			return PaidOutIdleState.class;
        }

        if (!(event.getSource() instanceof NumberButton)
            && !(event.getSource() instanceof ClearButton)) {
            POSPeripheralHome posHome = POSPeripheralHome.getInstance();
            try {
                tone = posHome.getToneIndicator();
                if (!tone.getDeviceEnabled())
                    tone.setDeviceEnabled(true);
                if (!tone.getClaimed()) {
                     tone.claim(0);
                     tone.setAsyncMode(true);
                     tone.sound(99999, 500);
                }                    
            } catch (NoSuchPOSDeviceException ne) {
                CreamToolkit.logMessage(ne + "!");
            } catch (JposException je) {
                System.out.println(je);
            }
            app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("pricemessage"));
			return PaidOutOpenPriceState.class;
        }

        return sinkState.getClass();
	}
}




