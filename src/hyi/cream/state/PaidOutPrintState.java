/**
 * State class
 * @since 2000
 * @author slackware
 */
 

package hyi.cream.state;

//for event processing
import java.util.*;
import hyi.cream.*;
import jpos.*;

//for transaction
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;

//for print
import hyi.cream.util.*;

//for maths calculation
import java.math.*;

public class PaidOutPrintState extends State {
    static PaidOutPrintState paidOutPrintState = null;

    public static PaidOutPrintState getInstance() {
        try {
            if (paidOutPrintState == null) {
                paidOutPrintState = new PaidOutPrintState();
            }
        } catch (InstantiationException ex) {
        }
        return paidOutPrintState;
    }

    /**
     * Constructor
     */
    public PaidOutPrintState() throws InstantiationException {
    }

	public void entry (EventObject event, State sourceState) {
        //System.out.println("PaidOutPrintState entry!");
        
		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        if (posTerminal.getCurrentTransaction().getDealType2().equals("4")) {
            CreamToolkit.showText(posTerminal.getCurrentTransaction(), 0);
            return;
        }

		if (sourceState instanceof PaidOutOpenPriceState) {
			Transaction cTransaction = posTerminal.getCurrentTransaction();
			Object[] lineItemArrayLast = cTransaction.getLineItemsArrayLast();
			if (lineItemArrayLast.length > 1) {
				if (!CreamPrinter.getInstance().getHeaderPrinted() ) {
					CreamPrinter.getInstance().printHeader();
					CreamPrinter.getInstance().setHeaderPrinted(true);
			    }
				LineItem lineItem = (LineItem)lineItemArrayLast[lineItemArrayLast.length-1];
                if (lineItem.getDetailCode().equals("V"))
					CreamPrinter.getInstance().printLineItem(lineItem);
				lineItem = (LineItem)lineItemArrayLast[lineItemArrayLast.length-2];
				CreamPrinter.getInstance().printLineItem(lineItem);
			}
			if (!cTransaction.getBuyerNumber().equals("")
				&& !posTerminal.getBuyerNumberPrinted()) {
				posTerminal.setBuyerNumberPrinted(true);
			}
		}
	}

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("PaidOutPrintState exit!");

        if (sinkState != null)
            return sinkState.getClass();
        else
            return null;
	}
}

