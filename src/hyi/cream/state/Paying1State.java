// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;
import hyi.cream.util.*;

import java.util.*;
import java.math.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class Paying1State extends State {

    private String paymentID             = "";
    private String payNumber             = "";
    private boolean paymentAvailable     = false;
    private POSTerminalApplication app   = POSTerminalApplication.getInstance();

    public static Payment lastestPayment = null;
    static Paying1State paying1State = null;

    public static Paying1State getInstance() {
        try {
            if (paying1State == null) {
                paying1State = new Paying1State();
            }
        } catch (InstantiationException ex) {
        }
        return paying1State;
    }

    /**
     * Constructor
     */
    public Paying1State() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("Paying1State entry");
        String payno = "";
        String payamt = "";
        boolean notUse = false;

        Transaction curTransaction = app.getCurrentTransaction();
		Object eventSource = event.getSource();
        
		if (sourceState instanceof SummaryState) {
            if (eventSource instanceof PaymentButton) {
                paymentID = ((PaymentButton)eventSource).getPaymentID();
            } else if (eventSource instanceof EnterButton) {
				paymentID = "00";
            }
            //gllg 2008-06-12 
			//payNumber = curTransaction.getBalance().toString();
			payNumber = curTransaction.getBalanceWithItemDsct().toString();
            //System.out.println("payNumber:" + payNumber);
        } else if (sourceState instanceof Numbering2State) {
            if (eventSource instanceof PaymentButton) {
                paymentID = ((PaymentButton)eventSource).getPaymentID();
            //} else if (eventSource instanceof PaymentMenu2Button) {
            //    paymentID = ((PaymentMenu2Button) event.getSource()).getPaymentID();
            } else if (eventSource instanceof EnterButton) {
                paymentID = "00";
            }
            payNumber = ((Numbering2State)sourceState).getNumberString();
        }  else if (sourceState instanceof ScanTicketState) {
            paymentID = ScanTicketState.getPayID();
            payNumber = "1";
            notUse = true;
        }


        //Bruce/2003-12-17
        //负项销售状态时，输入的支付金额前面自动加上一个减号
        if (app.getReturnItemState() && !payNumber.startsWith("-"))
            payNumber = "-" + payNumber;
                
        //System.out.println("payment id is " + paymentID);
        //System.out.println("payment number " + payNumber);

		Payment curPayment = Payment.queryByPaymentID(paymentID);
        if (curPayment == null) {
            paymentAvailable = false;
            return;
        }
        if (notUse)
            curPayment.setDenomination(ScanTicketState.getAmt());
        if (curPayment.getPaymentID().equalsIgnoreCase(
                CreamProperties.getInstance().getProperty("RebatePaymentID"))
                ) {
            String tmp = CreamProperties.getInstance().getProperty("OnlyMemberCanUseRebate", "no");
            
            if (tmp.equalsIgnoreCase("yes") && curTransaction.getMemberID() == null) {
                paymentAvailable = false;
                setWarningMessage(CreamToolkit.GetResource().getString("OnlyMemberUseRebate"));
                return;
            }
            
//            //每次最多能用还元金支付2000元
//            if (new BigDecimal(payNumber).compareTo(new BigDecimal(2000)) > 0) {
//                paymentAvailable = false;
//                setWarningMessage(CreamToolkit.GetResource().getString("TooManyRebate"));
//                return;
//            }
        }
       
        //Bruce/20030416
        //一般交易不允许使用“赊帐”支付。
        String payName = curPayment.getScreenName();
        if (payName.indexOf("赊") != -1) {
            setWarningMessage(CreamToolkit.GetResource().getString("CannotUseCreditPayment"));
            paymentAvailable = false;
            return;
        }

		int state = curTransaction.setPayments(curPayment);
		//System.out.println(curPayment); 
        /**
         *  if payment is null, then payment arraylist is clear
         *  if payment is not exist in payment arraylist and
         *        payment arraylist size less than 4,
         *     then payment arraylist add the payment, return 2
         *  if payment is exist in payment arraylist,
         *     then return 1
         *  if payment is not exist in payment arraylist and
         *        payment arraylist size more than 4,
         *     then return 0
         **/

		if (state == 0) {
			return;
		}
		lastestPayment = curPayment;
		curTransaction.setLastestPayment(lastestPayment);
		//System.out.println(lastestPayment);
        paymentAvailable = true;
        if (state == 1) {
            for (int i = 1; i < 5; i++) {
                payno = "PAYNO" + i;
                payamt = "PAYAMT" + i;
                if (curTransaction.getFieldValue(payno) == null) {
                    break;
                }
				if (((String)curTransaction.getFieldValue(payno)).equals(paymentID)) {
					Payment payment = Payment.queryByPaymentID(paymentID);
					//BigDecimal denomination = new BigDecimal(payment.getDenomination().doubleValue());
                    BigDecimal denomination = payment.getDenomination();
                    curTransaction.setFieldValue(
                        payamt,
                        ((BigDecimal)curTransaction.getFieldValue(payamt))
                            .add((new BigDecimal(payNumber)).multiply(denomination))
                            .setScale(2, BigDecimal.ROUND_HALF_UP));
                }
			}
		}
        ArrayList paynoArray = new ArrayList();
        ArrayList payamtArray = new ArrayList();
        if (state == 2) {
            if (curPayment.getPaymentType().charAt(4) == '0') {
				paynoArray.add(paymentID);
				Payment payment = Payment.queryByPaymentID(paymentID);
				//BigDecimal denomination = new BigDecimal(payment.getDenomination().doubleValue());
                BigDecimal denomination = payment.getDenomination();
				payamtArray.add((new BigDecimal(payNumber)).multiply(denomination).setScale(2, BigDecimal.ROUND_HALF_UP));
				for (int i = 1; i < 5; i++) {
                    payno = "PAYNO" + i;
                    payamt = "PAYAMT" + i;
					if (curTransaction.getFieldValue(payno) == null) {
                        break;
                    }
                    paynoArray.add(curTransaction.getFieldValue(payno));
                    payamtArray.add(curTransaction.getFieldValue(payamt));
                }
                for (int i = 0; i < paynoArray.size(); i++) {
                    payno = "PAYNO" + (i + 1);
                    payamt = "PAYAMT" + (i + 1);
                    curTransaction.setFieldValue(payno, paynoArray.get(i));
                    curTransaction.setFieldValue(payamt, payamtArray.get(i));
                }
            } else {
                for (int i = 1; i < 5; i++) {
                    payno = "PAYNO" + i;
                    payamt = "PAYAMT" + i;
					if (curTransaction.getFieldValue(payno) == null) {
						curTransaction.setFieldValue(payno, paymentID);
						Payment payment = Payment.queryByPaymentID(paymentID);
                        //BigDecimal denomination = new BigDecimal(payment.getDenomination().doubleValue());
                        BigDecimal denomination = payment.getDenomination();
                        curTransaction.setFieldValue(
                            payamt,
                            (new BigDecimal(payNumber)).multiply(denomination).setScale(
                                2,
                                BigDecimal.ROUND_HALF_UP));
                        break;
                    }
                }
            }
        }
    }

	public Class exit(EventObject event, State sinkState) {
		//System.out.println("Paying1State exit");
        if (!paymentAvailable) {
            WarningState.setExitState(SummaryState.class);
			return Warning2State.class;
			
		} else if (lastestPayment.getPaymentType().substring(3, 4).equals("1")) {
			return SlippingState.class;
		} else {
			return Paying3State.class;
        }
    }

    public void setPaymentAvailable(boolean paymentAvailable) {
        this.paymentAvailable = paymentAvailable;
    }

    public boolean getPaymentAvailable() {
        return paymentAvailable;
    }
}

