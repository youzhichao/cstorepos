/**
 * State class
 * @since 2000
 * @author slackware
 */
 
package hyi.cream.state;

//for event processing
import java.util.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;

   /**
	 * For slipping purpose.
	 */

public class Paying2State extends State {
    static Paying2State paying2State = null;

    public static Paying2State getInstance() {
        try {
            if (paying2State == null) {
                paying2State = new Paying2State();
            }
        } catch (InstantiationException ex) {
        }
        return paying2State;
    }

    /**
     * Constructor
     */
    public Paying2State() throws InstantiationException {
    }
    
   /**
	 * Nothing to do, just passig by.
	 */
	public void entry (EventObject event, State sourceState) {
		//System.out.println("This is Paying2State's Entry!");
	    //KeylockWarningState	Keylock
	}

   /**
	 *For card reading purpose.
	 */
	public Class exit(EventObject event, State sinkState) {
		//System.out.println("This is Paying2State's Exit!");
		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
		//null	null
		if (sinkState == null && event == null) {
		//CHAR[0]:外币
		//CHAR[1]:已开过发票（目前只有提货卷）
		//CHAR[2]:No use
		//CHAR[3]:认证
		//CHAR[4]:找零（不找零表示该支付可能产生溢收）
		//CHAR[5]:No use
		//CHAR[6]:自动结清余额CHAR[7]:刷卡
		//If currentTransaction's latest Payment belongs to需刷卡,
		//return CardReadingState.class
			Iterator itr = posTerminal.getCurrentTransaction().getPayments();
			Payment pay = null;
			/*while (itr.hasNext()) {
				if (!itr.hasNext())
					pay = (Payment)itr.next();
				else
                    itr.next();
			}*/
			pay = posTerminal.getCurrentTransaction().getLastestPayment();
			//System.out.println(pay + "" + this);
			if (pay.getPaymentType().endsWith("1"))
				return CardReadingState.class;
		//otherwise return Paying3State.class.
			return Paying3State.class;
		} else {
			return null;
		}
	}	//KeylockWarningState	Keylock

}


