/**
 * State class
 * @since 2000
 * @author slackware
 */
 
package hyi.cream.state;

import java.util.*;
import java.awt.Toolkit;
import java.math.*;

import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.dac.*;

public class Paying3State extends State {
	//private ShiftReport shift = null;
	//private ZReport z = null;
	private static int transactionNumber = -1;
	//private BigDecimal [] payments = {new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), new BigDecimal(0),}; 
	static Paying3State paying3State = null;
	private static Class exitState = null;
    private BigDecimal cashAmt = new BigDecimal(0.00);
    private BigDecimal changeAmountUpperLimit;

	public static Paying3State getInstance() {
		try {
			if (paying3State == null) {
				paying3State = new Paying3State();
			}
		} catch (InstantiationException ex) { }
		return paying3State;
	}

	/**
	 * Constructor
	 */
	public Paying3State() throws InstantiationException {
        //Bruce/2003-12-01
        //增加property "ChangeAmountUpperLimit"(default is 100)，如果找零金额大于等于此值，则报警
        changeAmountUpperLimit = new BigDecimal(CreamProperties.getInstance().getProperty(
            "ChangeAmountUpperLimit", "100"));

		if (paying3State != null)
			throw new InstantiationException(this.getClass().getName());
		else {
			//shift = ShiftReport.getCurrentShift();
			//z = ZReport.getCurrentZReport();
			paying3State = this;
		}
	}

	public void entry (EventObject event, State sourceState) {
        //Bruce/2003-12-01
		//System.out.println("This is Paying3State's Entry!");
		//POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
		//posTerminal.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("DataTransfers"));
		//KeylockWarningState	Keylock
	}

	//Calculate the total支付金额, if支付金额 < 应付金额,
	//return SummaryState.class
	//Otherwise print payment information on receipt, cut paper,
	//store currentTransaction objectl
	//If the current ShfitReport's SystemDateTime is not '1900/01/01 00:00:00'(表示已经做过交班),
	//then count up ShiftNumber in CreamProperties,
	//and create/insert a new one. Do the same thing on ZReport.l
	//Accumulate data into ShiftReport, ZReport, and CategorySales.
	//(Ref. Data Accumulation Flow).


	public Class exit(EventObject event, State sinkState) {
		//System.out.println("This is Paying3State's Exit!");
		//long start = System.currentTimeMillis();
		//null	null
        cashAmt = new BigDecimal(0.00);

		POSTerminalApplication app = POSTerminalApplication.getInstance();
	 	Transaction currTran = app.getCurrentTransaction();
		try {
		    //System.out.println("transactionNumber=" + transactionNumber);
		    //System.out.println("cTransac.getTransactionNumber().intValue=" + cTransac.getTransactionNumber().intValue());
			if (sinkState == null && event == null) {
				/*
				 * Calculate the total payment amount
				 */
		
//				boolean nextPayment = false;
                boolean nextPayment = true;
                
                //gllg BigDecimal subTotalAmt = currTran.getSalesAmount();//.add(cTransac.getDaiFuAmount().add(cTransac.getDaiShouAmount())) ;
                BigDecimal subTotalAmt = currTran.getSalesAmtWithItemDsct();
                BigDecimal totalPayAmt = new BigDecimal(0); //各种支付方式的付款金额的总和
		
        		//System.out.println(" 金额" +  cTransac.getDaiFuAmount() + "|" + subTotalAmt );
				//BigDecimal currentPayment = new BigDecimal(0);
				//Paying3State: Calc. 找零金额，溢收，发票金额
				//currentPayment = cTransac.getPayAmount1();
				
                for (int i = 1; nextPayment && i < 5; i++ ) {
                    String payno = "PAYNO" + i;
                    String payamt = "PAYAMT" + i;
                    
                    String payID = (String)currTran.getFieldValue(payno);
                    Payment payment = Payment.queryByPaymentID(payID);
                    
                    BigDecimal amt = (BigDecimal)currTran.getFieldValue(payamt);
                    if (amt != null) {
                        totalPayAmt = totalPayAmt.add(amt);
                        boolean isRebate = payment.getPaymentID().equalsIgnoreCase(CreamProperties.getInstance().getProperty("RebatePaymentID"));

                        //Bruce/2003-12-17
                        //负项销售状态时，判断金额大小要完全相反
                        if (!app.getReturnItemState() && totalPayAmt.doubleValue() > subTotalAmt.doubleValue()
                            || app.getReturnItemState() && totalPayAmt.doubleValue() < subTotalAmt.doubleValue()) {
                            if (payment.getPaymentType().charAt(4) == '0') {//不可找零
                                currTran.setSpillAmount(totalPayAmt.subtract(subTotalAmt));
                                currTran.setChangeAmount(new BigDecimal(0));
                            } else {//可找零
                                currTran.setSpillAmount(new BigDecimal(0));
                                    //Bruce/2003-12-01
                                    //增加property "ChangeAmountUpperLimit"(default is 100)，如果找零金额大于等于此值，则报警
                                BigDecimal chgAmount = totalPayAmt.subtract(subTotalAmt);
                                if (!app.getReturnItemState() && chgAmount.compareTo(changeAmountUpperLimit) >= 0) {
                                    app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("InputAmountIsTooLarge"));
                                    exitState = SummaryState.class;
                                    currTran.clearPaymentInfo();
                                    return exitState;
                                }
                                currTran.setChangeAmount(chgAmount);
                            }
                            nextPayment = false;
                        //} else if (totalPayAmt.doubleValue() == subTotalAmt.doubleValue()) {
                        } else if (totalPayAmt.doubleValue() == subTotalAmt.doubleValue()) {
                            currTran.setChangeAmount(new BigDecimal(0));
                            currTran.setSpillAmount(new BigDecimal(0));
                            nextPayment = false;
                        } else
                            nextPayment = true;
                        
                        if (payment.getPaymentID().equals("00")) //现金 
                            cashAmt = amt;
                            
                        if (payment.getPaymentType().charAt(1) == '1') {//已开过发票（目前只有提货卷）
                            if (currTran.getBalance().compareTo(new BigDecimal(0)) == 0) {
                                currTran.setNetSalesAmount(currTran.getNetSalesAmount().subtract(amt).add(currTran.getSpillAmount()));
                            }
                        }
                    }
                    
                } // end for
                

                //Bruce/20030417 (Modified for 1ck1)
                // 若property "DontAllowGenerateSpillAmount" 为 yes，表示不允许生成溢收
                if (CreamProperties.getInstance().getProperty("DontAllowGenerateSpillAmount", "no").equalsIgnoreCase("yes")
                    && !currTran.getSpillAmount().equals(new BigDecimal(0))) {
                    app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("DontAllowGenerateSpillAmount"));
                    exitState = SummaryState.class;
                    return exitState;
                }

                //System.out.println("nextPayment:"+nextPayment);
				if (!app.getReturnItemState() && nextPayment) {
                    app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("CashWarning"));
                    Toolkit.getDefaultToolkit().beep();
					exitState = SummaryState.class;
                } else {
                    countSpecialRebate(currTran);
                    
                    exitState = hyi.cream.state.DrawerOpenState.class;
				    //System.out.println("cTransac.getPayAmount2()=" + cTransac.getPayAmount2());

                    //Bruce/2003-12-02
                    //如果用repaint()的话，很奇怪，不会马上画出来。所以只好搞一个forceRepaint()。
					app.getPayingPane().forceRepaint();
					//app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("DataTransfers"));
				}
				return exitState;
			} else
				return null;
		} finally {
			 //System.out.println("transactionNumber=" + transactionNumber);
			 //System.out.println("cTransac.getTransactionNumber().intValue=" + cTransac.getTransactionNumber().intValue());
			 //if (exitState != SummaryState.class)
			 transactionNumber = currTran.getTransactionNumber().intValue();
		}
	}  //KeylockWarningState	Keylock

    /*
     * 如果现金支付的金额大于产生还元金的明细金额总和，额外再增加还元金
     */
    private void countSpecialRebate(Transaction tran) {
        Iterator itr = tran.getLineItems();
        BigDecimal sum = new BigDecimal(0.0);
        String specialRebateRate = CreamProperties.getInstance().getProperty("SpecialRebateRate","0.00");
        
        while (itr.hasNext()) {
            LineItem li = (LineItem)itr.next();
            if(li.getRebateRate().compareTo(new BigDecimal(0.00)) > 0) {
                sum = sum.add(li.getAfterDiscountAmount());
            }
        }
    }
    
}

   
    
	/*if(shift.getAccountingDate().compareTo(initDate) != 0) {
				//int currentShiftNumber = Integer.parseInt(CreamProperties.getInstance().getProperty("ShiftNumber"));
				//CreamProperties.getInstance().setProperty("ShiftNumber", Integer.toString(++currentShiftNumber));
				//CreamProperties.getInstance().deposit();
				shift = ShiftReport.createShiftReport();
				shift.setBeginTransactionNumber(Integer.decode(CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber")));
				if (!shift.insert())
					shift.queryByPrimaryKey();
			}*/

			/*boolean createShift = false;
			if (shift == null)
				createShift = true;
			else if(shift.getAccountingDate().compareTo(initDate) != 0)
				createShift = true;
			if (createShift) {
				int currentShiftNumber = Integer.parseInt(CreamProperties.getInstance().getProperty("ShiftNumber"));
				CreamProperties.getInstance().setProperty("ShiftNumber", Integer.toString(++currentShiftNumber));
				CreamProperties.getInstance().deposit();
				shift = ShiftReport.createShiftReport();
				if (!shift.insert())
					shift.queryByPrimaryKey();
			}*/

				/*boolean createZ = false;
			if (z == null)
				createZ = true;
			else if(z.getAccountingDate().compareTo(initDate) != 0)
				createZ = true;
			if (createZ) {
				int currentZNumber = Integer.parseInt(CreamProperties.getInstance().getProperty("ZNumber"));
				CreamProperties.getInstance().setProperty("ZNumber", Integer.toString(++currentZNumber));
				CreamProperties.getInstance().deposit();
				z = ZReport.createZReport();
				if (!z.insert())
					z.queryByPrimaryKey();//##
				Iterator itr = CategorySales.constructFromCategory();
				while (itr.hasNext()) {
					CategorySales catSale = (CategorySales)itr.next();
					if (!catSale.insert())
						catSale.queryByPrimaryKey();//##
				}
			}*/

				/*Iterator itr = CategorySales.constructFromCategory();
				while (itr.hasNext()) {
					CategorySales catSale = (CategorySales)itr.next();
					if (!catSale.insert())
						catSale.queryByPrimaryKey();//##
				}*/















				
	/**
			 * Transaction statistic
			 */
			/*if (!cTransac.getDealType3().equals("2")) {
//TransactionCountGrandTotal (only in ZReport)	Add one. Except作废, 退货交易need to minus one.
				int tranCount = z.getTransactionCountGrandTotal().intValue();
				z.setTransactionCountGrandTotal(new Integer(++tranCount));
//TransactionCount	Add one. Except作废, 退货交易need to minus one.
				tranCount = shift.getTransactionCount().intValue();
				shift.setTransactionCount(new Integer(++tranCount));
				tranCount = z.getTransactionCount().intValue();
				z.setTransactionCount(new Integer(++tranCount));
			} else {
//TransactionVoidAmount	Add InvoiceAmount of current transaction if it is a 作废交易.
//TransactionVoidCount	Add one if current transaction is a 作废交易.
				shift.setTransactionVoidAmount(shift.getTransactionVoidAmount().add(cTransac.getInvoiceAmount()));
				int voidCount = shift.getTransactionVoidCount().intValue();
				shift.setTransactionVoidCount(new Integer(++voidCount));
				z.setTransactionVoidAmount(z.getTransactionVoidAmount().add(cTransac.getInvoiceAmount()));
				voidCount = z.getTransactionVoidCount().intValue();
				z.setTransactionVoidCount(new Integer(++voidCount));
			}

			if (cTransac.getDealType2().equals("1")) {
//TransactionCountGrandTotal (only in ZReport)	Add one. Except作废, 退货交易need to minus one.
				int tranCount = z.getTransactionCountGrandTotal().intValue();
				z.setTransactionCountGrandTotal(new Integer(--tranCount));
//TransactionCount	Add one. Except作废, 退货交易need to minus one.
				tranCount = shift.getTransactionCount().intValue();
				shift.setTransactionCount(new Integer(--tranCount));
				tranCount = z.getTransactionCount().intValue();
				z.setTransactionCount(new Integer(--tranCount));
			}*/

