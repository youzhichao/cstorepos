// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   PeiDaState.java

package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.*;
import hyi.cream.uibeans.Indicator;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import jpos.ToneIndicator;

// Referenced classes of package hyi.cream.state:
//            State, IdleState

public class PeiDaState extends State
{

    private String annotatedid;
    private POSTerminalApplication app;
    private ResourceBundle res;
    static PeiDaState PeiDaState = null;
    private ToneIndicator tone;
    private String pluCode;
    private PLU plu;
    private LineItem lineItem;
    private Transaction curTransaction;

    public static PeiDaState getInstance()
    {
        try
        {
            if(PeiDaState == null)
                PeiDaState = new PeiDaState();
        }
        catch(InstantiationException instantiationexception) { }
        return PeiDaState;
    }

    public PeiDaState()
        throws InstantiationException
    {
        annotatedid = "";
        app = POSTerminalApplication.getInstance();
        res = CreamToolkit.GetResource();
        tone = null;
        pluCode = "";
        plu = null;
        lineItem = null;
        curTransaction = app.getCurrentTransaction();
    }

    public void entry(EventObject event, State sourceState)
    {
        if(sourceState instanceof IdleState)
            annotatedid = "";
        app.getWarningIndicator().setMessage("");
    }

    public Class exit(EventObject event, State sinkState)
    {
        if(!app.getTransactionEnd())
        {
            app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("PeiDaMessage"));
            return hyi.cream.state.IdleState.class;
        }
        Transaction trans = app.getCurrentTransaction();
        String type = trans.getAnnotatedType();
        if(type != null && type.equalsIgnoreCase("P"))
        {
            trans.setAnnotatedType("");
            trans.setAnnotatedId(annotatedid);
            return hyi.cream.state.IdleState.class;
        } else
        {
            trans.setAnnotatedType("P");
            annotatedid = genPeiDaNumber();
            trans.setAnnotatedId(annotatedid);
            app.getWarningIndicator().setMessage("");
            return hyi.cream.state.IdleState.class;
        }
    }

    private String genPeiDaNumber()
    {
        String number = "";
        String storeNumber = CreamProperties.getInstance().getProperty("StoreNumber");
        if(storeNumber.length() > 5)
            storeNumber = storeNumber.substring(storeNumber.length() - 5, storeNumber.length());
        else
            for(; storeNumber.length() < 5; storeNumber = "0" + storeNumber);
        String terminalNumber = CreamProperties.getInstance().getProperty("TerminalNumber");
        if(terminalNumber.length() > 2)
            terminalNumber = terminalNumber.substring(terminalNumber.length() - 2, terminalNumber.length());
        else
            for(; terminalNumber.length() < 2; terminalNumber = "0" + terminalNumber);
        String date = (new SimpleDateFormat("yyMMdd")).format(new Date());
        date = date.substring(1, date.length());
        String peiDaNumber = CreamProperties.getInstance().getProperty("PeiDaNumber");
        String dateInPeiDa = "";
        try
        {
            dateInPeiDa = peiDaNumber.substring(7, 12);
        }
        catch(StringIndexOutOfBoundsException stringindexoutofboundsexception) { }
        if(peiDaNumber != null && dateInPeiDa.equals(date))
        {
            int count = 0;
            try
            {
                number = peiDaNumber.substring(12, peiDaNumber.length());
                count = Integer.parseInt(number);
                count++;
            }
            catch(Exception e)
            {
                count = 1;
            }
            for(number = String.valueOf(count); number.length() < 3; number = "0" + number);
            number = date + number;
        } else
        {
            number = date + "001";
        }
        number = storeNumber + terminalNumber + number;
        return number;
    }

}
