/*jadclipse*/// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) radix(10) lradix(10) 
// Source File Name:   PluReadyState.java
package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.TooManyLineItemsException;
import hyi.cream.dac.Dep;
import hyi.cream.dac.LimitQtySold;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Member;
import hyi.cream.dac.PLU;
import hyi.cream.dac.Rebate;
import hyi.cream.dac.TaxType;
import hyi.cream.dac.Transaction;
import hyi.cream.inline.Client;
import hyi.cream.inline.ClientCommandException;
import hyi.cream.uibeans.BarCodeButton;
import hyi.cream.uibeans.DaiShouButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.uibeans.InStoreCodeButton;
import hyi.cream.uibeans.Indicator;
import hyi.cream.uibeans.PluButton;
import hyi.cream.uibeans.PluMenuButton;
import hyi.cream.uibeans.TaxedAmountButton;
import hyi.cream.uibeans.WeiXiuButton;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import java.awt.Color;
import java.awt.Toolkit;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import jpos.JposException;
import jpos.Scanner;

// Referenced classes of package hyi.cream.state:
//            State, IdleState, GetMemberNumberState, PluSpecState, 
//            NumberingState, WarningState

/**
 * PluReadyState class.
 *
 * @author dai, bruce, zhao
 */
public class PluReadyState extends State {

    private String number;
    private String numberString;
    private PLU plu;
    private POSTerminalApplication app;
    private Transaction curTransaction;
    private LineItem lineItem;
    private boolean deviceClaimed;
    private Class exitState;
    private BigDecimal quan;
    private BigDecimal zero;
    private ResourceBundle res;
    private Color lineItemColor;
    private Color defaultColor;
    private String pluSign3;
    private int daishouNumber;
    private boolean isDaishouPlu;
    private int daishouDetailNumber;
    private String daishouDetailName;
    private String rebatePaymentID;
    private String memUseRebate;
    long start;
    long end;
    
    public PluReadyState() {
        number = "";
        numberString = "";
        plu = null;
        app = POSTerminalApplication.getInstance();
        curTransaction = app.getCurrentTransaction();
        lineItem = null;
        deviceClaimed = false;
        exitState = null;
        quan = new BigDecimal("1.00");
        zero = new BigDecimal(0.0D);
        res = CreamToolkit.GetResource();
        lineItemColor = new Color(70, 206, 255);
        defaultColor = new Color(70, 206, 255);
        pluSign3 = "";
        daishouNumber = 0;
        isDaishouPlu = false;
        daishouDetailNumber = 0;
        daishouDetailName = "";
        start = 0L;
        end = 0L;
        rebatePaymentID = CreamProperties.getInstance().getProperty("RebatePaymentID");
        if (rebatePaymentID != null)
            rebatePaymentID = rebatePaymentID.trim();
        memUseRebate = CreamProperties.getInstance().getProperty("OnlyMemberCanUseRebate", "yes");
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof PluButton) {
            if (!curTransaction.getLockEnable()) {
                exitState = hyi.cream.state.IdleState.class;
                return;
            }
            PluButton p = (PluButton) event.getSource();
            lineItemColor = p.getPluColor();
        } else {
            lineItemColor = defaultColor;
        }
        try {
            quan = new BigDecimal("1.00");
            Object eventSource = event.getSource();
            if (sourceState instanceof IdleState) {
                if (eventSource instanceof PluButton)
                    number = ((PluButton) eventSource).getPluCode();
                else if (eventSource instanceof PluMenuButton) {
                    number = ((PluMenuButton) eventSource).getPluNo();
                    ((PluMenuButton) eventSource).clear();
                } else if (eventSource instanceof Scanner) {
                    ((Scanner) eventSource).setDecodeData(true);
                    //if (((Scanner)eventSource).getScanDataType() == jpos.ScannerConst.SCAN_SDT_UNKNOWN
                    //    ||((Scanner)eventSource).getScanDataType() == jpos.ScannerConst.SCAN_SDT_Code39) {
                    //    setWarningMessage(res.getString("InputWrong"));
                    //    exitState = WarningState.class;
                    //    return;
                    //}
                    number = new String(((Scanner) eventSource).getScanDataLabel());

                    //Bruce/20030401
                    //1ck1 specific
                    //若扫描进来的是以2991打头，则检查是否为会员编号
                    if (number.startsWith("2991") || number.startsWith("2990")) {
                        //若已经在交易输入过程中，不允许输入会员编号，发出警讯
                        //if (!app.getTransactionEnd()) {
                        //Bruce/2003-06-23 若画面上看不到任何商品（例如可能被更正掉），仍允许输入会员编号。
                        if (app.getCurrentTransaction().getDisplayedLineItemsArray().size() != 0) {
                            setWarningMessage(res.getString("CannotInputMemberID"));
                            exitState = hyi.cream.state.WarningState.class;
                            return;
                        }
                        Member member = Member.queryByMemberID(number);
                        if (member != null) {
                            app.getCurrentTransaction().setMemberID(member.getMemberCardID());
                            GetMemberNumberState.setMember(member); // will be used by ShowMemberInfoState
                            app.getCurrentTransaction().setBeforeTotalRebateAmt(member.getMemberActionBonus());
                            exitState = hyi.cream.state.ShowMemberInfoState.class;
                        } else {
                            setWarningMessage(res.getString("WrongMemberID"));
                            exitState = hyi.cream.state.WarningState.class;
                        }
                        return;
                    }
                    // lxf 20030417 电子秤
                    //System.out.println("1");
                    if (processScaleBarCode(number))
                        return;
                    exitState = hyi.cream.state.IdleState.class;
                } else if (eventSource instanceof DaiShouButton) {
                    isDaishouPlu = true;
                    daishouNumber = ((DaiShouButton) eventSource).getDaiShouNumber();
                    daishouDetailNumber = ((DaiShouButton) eventSource).getDaishouDetailNumber();
                    daishouDetailName = ((DaiShouButton) eventSource).getDaishouDetailName();
                    number = ((DaiShouButton) eventSource).getPluNumber();
                    /*plu = PLU.queryByPluNumber(number);
                    lineItem = new LineItem();
                    lineItem.setDescription(r.getreasonName());
                    lineItem.setDetailCode("I");
                    lineItem.setPluNumber(r.getreasonNumber());
                    lineItem.setQuantity(new BigDecimal(0));
                    //lineItem.setTaxType("0");
                    lineItem.setTerminalNumber(curTransaction.getTerminalNumber());
                    lineItem.setTransactionNumber(new Integer(curTransaction.getNextTransactionNumber()));
                    lineItem.setUnitPrice(new BigDecimal("0.00"));
                    lineItem.setAmount(new BigDecimal("0.00"));
                    curTransaction.addLineItem(lineItem);
                    exitState = OpenPriceState.class;
                    return;*/
                // lxf/20030401
                } else if (eventSource instanceof WeiXiuButton) {
                    String type = app.getCurrentTransaction().getAnnotatedType();
                    if (type != null && !type.equals("")) {
                        setExitState(hyi.cream.state.IdleState.class);
                        return;
                    }
                    number = ((WeiXiuButton) eventSource).getPluCode();
                    plu = PLU.queryByPluNumber(number);
                    sub(plu);
                    if (exitState.getName().equalsIgnoreCase("hyi.cream.state.OpenPriceState") || exitState.getName().equalsIgnoreCase("hyi.cream.state.MixAndMatchState"))
                        setExitState(hyi.cream.state.WeiXiuState.class);
                    return;
                }
                plu = PLU.queryByPluNumber(number);
                if (plu != null) {
                    number = numberString;
                    Map map = StateMachine.getInstance().getDsCodeMap();
                    if (map.containsKey(plu.getPluNumber())) {
                        String[] arr = (String[]) map.get(plu.getPluNumber());
                        isDaishouPlu = true;
                        daishouNumber = Integer.parseInt(arr[0]);
                        daishouDetailNumber = Integer.parseInt(arr[1]);
                        daishouDetailName = arr[2];
                    }
                }
                sub(plu);
            } else if (sourceState instanceof PluSpecState) {
                String pluAttr = ((PluSpecState) sourceState).getPluAttr();
                if (eventSource instanceof PluButton)
                    number = ((PluButton) eventSource).getPluCode(pluAttr);
                plu = PLU.queryByPluNumber(number);
                quan = new BigDecimal(((PluSpecState) sourceState).getNumberString());
                sub(plu);
                ((PluSpecState) sourceState).setNumberString("1");
            } else if (sourceState instanceof NumberingState) {
                numberString = ((NumberingState) sourceState).getNumberString();
                if (eventSource instanceof PluButton) {
                    quan = new BigDecimal(numberString);
                    number = ((PluButton) eventSource).getPluCode();
                    plu = PLU.queryByPluNumber(number);
                    sub(plu);
                } else if (eventSource instanceof PluMenuButton) {
                    quan = new BigDecimal(numberString);
                    number = ((PluMenuButton) eventSource).getPluNo();
                    plu = PLU.queryByPluNumber(number);
                    sub(plu);
                } else if (eventSource instanceof Scanner) {
                    if (((Scanner) eventSource).getScanDataType() == 0 || ((Scanner) eventSource).getScanDataType() == 108) {
                        setWarningMessage(res.getString("InputWrong"));
                        exitState = hyi.cream.state.WarningState.class;
                        return;
                    }
                    quan = new BigDecimal(numberString);
                    number = new String(((Scanner) eventSource).getScanDataLabel());
                    plu = PLU.queryByPluNumber(number);
                    sub(plu);
                } else if (eventSource instanceof BarCodeButton) {
                    // lxf 20030417 电子秤
                    //System.out.println("2");
                    if (!processScaleBarCode(numberString)) {
                        plu = PLU.queryBarCode(numberString);
                        if (plu == null)
                            plu = PLU.queryByInStoreCode(numberString);
                        number = numberString;
                        sub(plu);
                    }
                } else if ((eventSource instanceof InStoreCodeButton) || (eventSource instanceof EnterButton)) {
                    if (!processScaleBarCode(numberString)) {
                        // lxf 20030417 电子秤
                        //System.out.println("3");
                        plu = PLU.queryByInStoreCode(numberString);
                        if (plu == null)
                            plu = PLU.queryBarCode(numberString);
                        if (plu != null) {
                            number = numberString;
                            Map map = StateMachine.getInstance().getDsCodeMap();
                            if (map.containsKey(plu.getPluNumber())) {
                                String[] arr = (String[]) map.get(plu.getPluNumber());
                                isDaishouPlu = true;
                                daishouNumber = Integer.parseInt(arr[0]);
                                daishouDetailNumber = Integer.parseInt(arr[1]);
                                daishouDetailName = arr[2];
                            }
                        }
                        sub(plu);
                    }
                } else if (eventSource instanceof TaxedAmountButton) {
                    lineItem = new LineItem();
                    String taxID = ((TaxedAmountButton) eventSource).getTaxID();
                    String depID = ((TaxedAmountButton) eventSource).getDepID();
                    setExitState(hyi.cream.state.MixAndMatchState.class);
                    lineItem.setTerminalNumber(curTransaction.getTerminalNumber());
                    lineItem.setQuantity(new BigDecimal("1.00"));
                    lineItem.setPluNumber("");
                    lineItem.setUnitPrice(new BigDecimal(numberString));
                    /*
                     * Meyer/2003-02-21/
                     */
                    lineItem.setOriginalPrice(new BigDecimal(numberString));
                    lineItem.setItemNumber("");
                    lineItem.setCategoryNumber("98");
                    lineItem.setMicroCategoryNumber(depID);
                    lineItem.setDetailCode("S");
                    lineItem.setTransactionNumber(new Integer(Transaction.getNextTransactionNumber()));
                    lineItem.setTaxType(taxID);
                    TaxType taxType = TaxType.queryByTaxID(taxID);
                    if (taxType == null) {
                        setWarningMessage(res.getString("WrongTaxPlu"));
                        setExitState(hyi.cream.state.WarningState.class);
                        WarningState.setExitState(hyi.cream.state.IdleState.class);
                        return;
                    }
                    //lineItem.setDescription(taxType.getTaxName());
                    Dep dep = Dep.queryByDepID(depID);
                    if (dep == null) {
                        setWarningMessage(res.getString("WrongTaxPlu"));
                        setExitState(hyi.cream.state.WarningState.class);
                        WarningState.setExitState(hyi.cream.state.IdleState.class);
                        return;
                    }
                    lineItem.setDescription(dep.getDepID() + ":" + dep.getDepName());
                    int round = 0;
                    String taxRound = taxType.getRound();
                    if (taxRound.equalsIgnoreCase("R"))
                        round = 4;
                    else if (taxRound.equalsIgnoreCase("C"))
                        round = 0;
                    else if (taxRound.equalsIgnoreCase("D"))
                        round = 1;
                    String taxTp = taxType.getType();
                    BigDecimal taxPercent = taxType.getPercent();
                    BigDecimal taxAmount = new BigDecimal(0.0D);
                    int taxDigit = taxType.getDecimalDigit().intValue();
                    if (taxTp.equalsIgnoreCase("1")) {
                        BigDecimal unit = new BigDecimal("1");
                        taxAmount = lineItem.getAmount().multiply(taxPercent).divide(unit.add(taxPercent), 2).setScale(taxDigit, round);
                    } else if (taxTp.equalsIgnoreCase("2")) {
                        //BigDecimal unit = new BigDecimal("1");
                        taxAmount = lineItem.getAmount().multiply(taxPercent).setScale(taxDigit, round);
                    } else {
                        BigDecimal tax = new BigDecimal("0.00");
                        taxAmount = tax.setScale(taxDigit, round);
                    }
                    lineItem.setAfterDiscountAmount(lineItem.getAmount());
                    lineItem.setAfterSIAmount(lineItem.getAmount());
                    lineItem.setTaxAmount(taxAmount);
                    /*String fieldName = "TAXAMT" + taxID;
                    BigDecimal fieldValue = (BigDecimal)curTransaction.getFieldValue(fieldName);
                    if (fieldValue == null) {
                        fieldValue = new BigDecimal(0);
                    }
                    fieldValue = fieldValue.add(taxAmount);
                    curTransaction.setFieldValue(fieldName, fieldValue);

                    lineItem.setTerminalNumber(curTransaction.getTerminalNumber());
                    lineItem.setQuantity(new BigDecimal("1.00"));
                    lineItem.setUnitPrice(new BigDecimal(numberString));
                    lineItem.setDescription("");
                    lineItem.setPluNumber("");
                    lineItem.setDetailCode("S");*/
                    lineItem.setTransactionNumber(new Integer(Transaction.getNextTransactionNumber()));
                    if (lineItemColor != null)
                        lineItem.setLineItemColor(lineItemColor);
                    curTransaction.addLineItem(lineItem);
                    //Bruce/20021227/
                    POSTerminalApplication.getInstance().setTransactionEnd(false);
                }
            }
        } catch (InstantiationException e2) {
            isDaishouPlu = false;
            CreamToolkit.logMessage(e2.toString());
            CreamToolkit.logMessage("Instantiation exception at " + this);
        } catch (TooManyLineItemsException e3) {
            isDaishouPlu = false;
            CreamToolkit.logMessage(e3.toString());
            CreamToolkit.logMessage("Too many LineItem exception at " + this);
            app.getWarningIndicator().setMessage(res.getString("TooManyLineItems"));
            exitState = hyi.cream.state.IdleState.class;
        } catch (JposException e) {
            isDaishouPlu = false;
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Jpos exception at " + this);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        Toolkit.getDefaultToolkit().beep();
        return exitState;
    }

    public Class getExitState() {
        return exitState;
    }

    public void setExitState(Class exitState) {
        this.exitState = exitState;
    }

    public void sub(PLU plu) {
        if (plu != null) {
            if (plu.getAttribute2() != null)
                pluSign3 = Integer.toBinaryString(plu.getAttribute2().intValue());
            else
                pluSign3 = "";
            for (; pluSign3.length() < 8; pluSign3 = "0" + pluSign3)
                ;
            ////CreamToolkit.logMessage("PluReadyState sub : plusign3 = " + pluSign3);
            int len = pluSign3.length();
            if (len != 0) {
                if (pluSign3.startsWith("1"))
                    setExitState(hyi.cream.state.OpenPriceState.class);
                else
                    setExitState(hyi.cream.state.MixAndMatchState.class);
            } else {
                setExitState(hyi.cream.state.MixAndMatchState.class);
            }
            boolean s = createLineItem(quan);
            if (!s)
                return;
            try {
                if (lineItemColor != null)
                    lineItem.setLineItemColor(lineItemColor);
                curTransaction.addLineItem(lineItem);
                //Bruce/20021227/
                POSTerminalApplication.getInstance().setTransactionEnd(false);
                //showText(lineItem);
            } catch (TooManyLineItemsException e) {
                isDaishouPlu = false;
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Too many LineItem exception at " + this);
                app.getWarningIndicator().setMessage(res.getString("TooManyLineItems"));
                exitState = hyi.cream.state.IdleState.class;
            }
        } else if (plu == null) {
            isDaishouPlu = false;
            setWarningMessage(res.getString("PluWaring") + " : " + number);
            number = "";
            setExitState(hyi.cream.state.WarningState.class);
            WarningState.setExitState(hyi.cream.state.IdleState.class);
        }
    }

    public boolean createLineItem(BigDecimal quantity) {
        BigDecimal taxAmount = new BigDecimal("0.00");
        try {
            lineItem = new LineItem();
            // check return item
            if (app.getReturnItemState()) {
                quantity = quantity.negate();
                lineItem.setDetailCode("R");
            } else {
                lineItem.setDetailCode("S");
            }
            // set lineItem properties
            //lineItem.setTransactionNumber(new Integer(Transaction.getNextTransactionNumber()));
            lineItem.setTransactionNumber(curTransaction.getTransactionNumber());
            lineItem.setTerminalNumber(curTransaction.getTerminalNumber());
            lineItem.setLineItemSequence(curTransaction.getLineItemsArrayLast().length + 1);
            lineItem.setRemoved(false);
            lineItem.setCategoryNumber(plu.getCategoryNumber());
            lineItem.setMicroCategoryNumber(plu.getMicroCategoryNumber());
            lineItem.setMidCategoryNumber(plu.getMidCategoryNumber());
            lineItem.setThinCategoryNumber(plu.getThinCategoryNumber());
            lineItem.setPluNumber(plu.getPluNumber());
            lineItem.setDescription(plu.getScreenName());
            lineItem.setDescriptionAndSpecification(plu.getNameAndSpecification());
            lineItem.setQuantity(quantity.setScale(2, 4));
            lineItem.setTaxType(plu.getTaxType());
            lineItem.setSmallUnit(plu.getSmallUnit());
            //Bruce/2003-08-08
            lineItem.setInvCycleNo(plu.getInvCycleNo());
            lineItem.setSizeCategory(plu.getSizeCategory());
            lineItem.setItemBrand(plu.getItemBrand());
            lineItem.setSize(plu.getSize());
            lineItem.setColor(plu.getColor());
            lineItem.setStyle(plu.getStyle());
            lineItem.setSeason(plu.getSeason());
            //Meyer/2003-02-19/ Add 2 Field: itemNumber, originalPrice
            lineItem.setItemNumber(plu.getInStoreCode());
            lineItem.setOriginalPrice(plu.getUnitPrice());
            TaxType taxType = TaxType.queryByTaxID(plu.getTaxType());
            if (taxType == null) {
                setWarningMessage(res.getString("WrongTaxPlu"));
                setExitState(hyi.cream.state.WarningState.class);
                WarningState.setExitState(hyi.cream.state.IdleState.class);
                return false;
            }
            int round = 0;
            String taxRound = taxType.getRound();
            if (taxRound.equalsIgnoreCase("R"))
                round = 4;
            else if (taxRound.equalsIgnoreCase("C"))
                round = 0;
            else if (taxRound.equalsIgnoreCase("D"))
                round = 1;
            String taxTp = taxType.getType();
            Object salingPriceDetail[] = plu.getSalingPriceDetail();
            BigDecimal pluPrice = (BigDecimal) salingPriceDetail[0];
            String memberID = POSTerminalApplication.getInstance().getCurrentTransaction().getMemberID();
            if (memberID == null)
                memberID = "";
            boolean isMemberPriceExist = memberID.length() > 0 && !plu.getMemberPrice().equals(zero);
            if ("SPECIAL_PRICE".equalsIgnoreCase((String) salingPriceDetail[1])) {
                //Bruce/20030519
                //如果使用的是特卖价，而且是会员，且会员价非为空的话，则OriginalPrice填会员价
                lineItem.setDiscountType("D");
                if (isMemberPriceExist)
                    lineItem.setOriginalPrice(plu.getMemberPrice());
            }
            //限量促销
            LimitQtySold lqs = null;
//          System.out.println("is Limit Quantity Sale [" + plu.isQuantityLimited() + "]");
            if (plu.isQuantityLimited())
                try {
                    //查询限价销售信息            
                    Client client = Client.getInstance();
                    client.processCommand("queryLimitQtySold " + lineItem.getItemNumber() + " 1 " + memberID);
                    lqs = (LimitQtySold) client.getReturnObject();
                    if (lqs != null && lqs.isSaleable()) {
//                      System.out.println("Limit Quantity Sold:\n rebate rate[" + lqs.getLimitRebateRate() + "] + price [" + lqs.getLimitPrice() + "]");
                        lineItem.setDiscountType("L");
                        pluPrice = lqs.getLimitPrice();
                        if (isMemberPriceExist)
                            lineItem.setOriginalPrice(plu.getMemberPrice());
                    }
                } catch (ClientCommandException clientcommandexception) {
                }
                //限量销售商品每次只能录入一单品
            if (lineItem.getDiscountType() != null && lineItem.getDiscountType().equals("L") && quantity.intValue() > 1) {
                setWarningMessage(res.getString("LimitQtyNumber"));
                setExitState(hyi.cream.state.WarningState.class);
                WarningState.setExitState(hyi.cream.state.IdleState.class);
                return false;
            }
            BigDecimal taxPercent = taxType.getPercent();
            int taxDigit = taxType.getDecimalDigit().intValue();
            if (taxTp.equalsIgnoreCase("1")) {
                lineItem.setUnitPrice(pluPrice);
                BigDecimal unit = new BigDecimal("1");
                taxAmount = lineItem.getAmount().multiply(taxPercent).divide(unit.add(taxPercent), 2).setScale(taxDigit, round);
            } else if (taxTp.equalsIgnoreCase("2")) {
                BigDecimal unit = new BigDecimal("1");
                lineItem.setUnitPrice(pluPrice.multiply(unit.add(taxPercent)));
                taxAmount = lineItem.getAmount().multiply(taxPercent).setScale(taxDigit, round);
            } else {
                lineItem.setUnitPrice(pluPrice);
                BigDecimal tax = new BigDecimal("0.00");
                taxAmount = tax.setScale(taxDigit, round);
            }
            lineItem.setTaxAmount(taxAmount);
            lineItem.setAfterDiscountAmount(lineItem.getAmount());
            lineItem.setAfterSIAmount(lineItem.getAmount());
            lineItem.setTaxAmount(taxAmount);
            //  check daishou and daifu
            if (isDaishouPlu) {
                lineItem.setDetailCode("I");
                lineItem.setDaishouNumber(daishouNumber);
                lineItem.setDiscountNumber(String.valueOf(daishouNumber));
                lineItem.setDaishouDetailName(daishouDetailName);
                lineItem.setDaishouDetailNumber(daishouDetailNumber);
                lineItem.setTaxAmount(new BigDecimal(0.0D));
                isDaishouPlu = false;
            }

            /*if (pluSign3.charAt(1) == '1') {
                lineItem.setDetailCode("I");
            } else if (pluSign3.charAt(3) == '1') {
                lineItem.setDetailCode("Q");
            } else if (pluSign3.charAt(4) == '1') {
                lineItem.setDetailCode("B");
            } else {
                lineItem.setDetailCode("S");
            }*/
            
            if (needCheckRebate(curTransaction)) {
                BigDecimal rate;
                if ("L".equalsIgnoreCase(lineItem.getDiscountType()))
                    rate = lqs.getLimitRebateRate();
                else
                    rate = Rebate.getRebateRate(lineItem.getItemNumber());
                lineItem.setRebateRate(rate);
                BigDecimal itemRebate = lineItem.getAfterDiscountAmount().multiply(rate).setScale(1, 4);
                lineItem.setAddRebateAmount(itemRebate);
            } else {
                lineItem.setAddRebateAmount(new BigDecimal(0.0D));
            }
            return true;
        } catch (InstantiationException e) {
            isDaishouPlu = false;
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Instantiation exception at " + this);
            return false;
        }
    }

    /**
     * @author ll 2003.04.17
     * 分析电子称PLU
     * @param number 
     */
    public boolean processScaleBarCode(String number) {
        try {
            //System.out.println("number : " + number);
            java.util.List formatList = app.getScaleBarCodeFormat();
            if (formatList == null || formatList.isEmpty())
                return false;
            int totalLength = ((Integer) formatList.get(0)).intValue();
            if (number.length() != totalLength)
                return false;
            String pref = (String) formatList.get(1);
            if (!number.startsWith(pref))
                return false;
            //System.out.println("find format : " + formatList);
            //int prefLength = pref.length();
            int iStart = ((Integer) formatList.get(2)).intValue();
            int iEnd = ((Integer) formatList.get(3)).intValue();
            int wStart = ((Integer) formatList.get(4)).intValue();
            int wEnd = ((Integer) formatList.get(5)).intValue();
            int wComma = ((Integer) formatList.get(6)).intValue();
            int pStart = ((Integer) formatList.get(7)).intValue();
            int pEnd = ((Integer) formatList.get(8)).intValue();
            int pComma = ((Integer) formatList.get(9)).intValue();
            String itemNo = number.substring(iStart, iEnd);
            String weight = number.substring(wStart, wEnd);
            weight = weight.substring(0, wComma) + "." + weight.substring(wComma, weight.length());
            String price = number.substring(pStart, pEnd);
            price = price.substring(0, pComma) + "." + price.substring(pComma, price.length());
            BigDecimal wCount = new BigDecimal(weight);
            wCount = wCount.setScale(2, 4);
            BigDecimal pCount = new BigDecimal(price);
            pCount = pCount.setScale(2, 4);
            plu = PLU.queryByInStoreCode(itemNo);
            //if (plu != null)
            //    System.out.println("plu is not null");
            sub2(plu, wCount, pCount);

            /*
            sub(plu);
            LineItem lineItem = curTransaction.getCurrentLineItem();
            lineItem.setWeight(wCount);
            lineItem.setAmount(pCount);
            */
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void sub2(PLU plu, BigDecimal weight, BigDecimal price) {
        if (plu != null) {
            if (plu.getAttribute2() != null)
                pluSign3 = Integer.toBinaryString(plu.getAttribute2().intValue());
            else
                pluSign3 = "";
            for (; pluSign3.length() < 8; pluSign3 = "0" + pluSign3)
                ;
            int len = pluSign3.length();
            if (len != 0) {
                if (pluSign3.startsWith("1"))
                    setExitState(hyi.cream.state.OpenPriceState.class);
                else
                    setExitState(hyi.cream.state.MixAndMatchState.class);
            } else {
                setExitState(hyi.cream.state.MixAndMatchState.class);
            }
            boolean s = createLineItem(weight);
            if (!s)
                return;
            try {
                if (lineItemColor != null)
                    lineItem.setLineItemColor(lineItemColor);
                lineItem.setWeight(weight);
                lineItem.setAmount(price);
                lineItem.setAfterDiscountAmount(price);
                curTransaction.addLineItem(lineItem);
                //Bruce/20021227/
                POSTerminalApplication.getInstance().setTransactionEnd(false);
                //showText(lineItem);
            } catch (TooManyLineItemsException e) {
                isDaishouPlu = false;
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Too many LineItem exception at " + this);
                app.getWarningIndicator().setMessage(res.getString("TooManyLineItems"));
                exitState = hyi.cream.state.IdleState.class;
            }
        } else if (plu == null) {
            isDaishouPlu = false;
            setWarningMessage(res.getString("PluWaring") + " : " + number);
            number = "";
            setExitState(hyi.cream.state.WarningState.class);
            WarningState.setExitState(hyi.cream.state.IdleState.class);
        }
    }

    /**
     * 该交易是否要计算还元金
     */
    private boolean needCheckRebate(Transaction tran) {
        boolean rtn = false;
        if (rebatePaymentID != null && rebatePaymentID.length() > 0)
            if (memUseRebate.equalsIgnoreCase("yes")) {
                if (tran.getMemberID() != null && tran.getMemberID().length() > 0)
                    rtn = true;
            } else {
                rtn = true;
            }
        return rtn;
    }
}

/*******************************************************************************
 * DECOMPILATION REPORT ***
 * 
 * DECOMPILED FROM: D:\workspace7\CStorePOS\classes\cream.jar
 * 
 * 
 * TOTAL TIME: 344 ms
 * 
 * 
 * JAD REPORTED MESSAGES/ERRORS:
 * 
 * 
 * EXIT STATUS: 0
 * 
 * 
 * CAUGHT EXCEPTIONS:
 * 
 ******************************************************************************/
