/**
 * State class
 * @since 2000
 * @author slackware
 */

package hyi.cream.state;

//for event processing
import java.text.SimpleDateFormat;
import java.util.*;
import hyi.cream.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.inline.*;

public class PluUpdateState extends State {
    static PluUpdateState paying2State = null;
    POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
    boolean connected = false;
    private ResourceBundle res = CreamToolkit.GetResource();

    public static PluUpdateState getInstance() {
        try {
            if (paying2State == null) {
                paying2State = new PluUpdateState();
            }
        } catch (InstantiationException ex) {
        }
        return paying2State;
    }

    /**
     * Constructor
     */
    public PluUpdateState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
        //System.out.println("This is PluUpdateState's Entry!");
        //KeylockWarningState	Keylock
        //connected = DacTransfer.getInstance().isConnected(100);
        posTerminal.getMessageIndicator().setMessage("请按确认键执行下传，清除键退出。");
        //if (!connected)
           //POSButtonHome.getInstance().buttonPressed(new POSButtonEvent(new ClearButton(0,0,0,"")));
         //System.out.println("This is PluUpdateState's Entry!");

    }


    public Class exit(EventObject event, State sinkState) {
        //System.out.println("This is PluUpdateState's Exit!");
        //connected = DacTransfer.getInstance().isConnected(100);
        connected = Client.getInstance().isConnected();
        if (!connected) {
            posTerminal.getWarningIndicator().setMessage("无法连线，请稍候再试。");
            return ConfigState.class;
        } /*else {
            if (DacTransfer.getInstance().isDateShiftRunning(DacTransfer.DATE_BIZDATE) ||
               Trigger.getInstance().getRunning()) {
                posTerminal.getWarningIndicator().setMessage("程序忙，请稍候再试。");
                return ConfigState.class;
            }
        }*/
        if (event.getSource() instanceof ClearButton) {
            return ConfigState.class;
        } else if (event.getSource() instanceof EnterButton) {
            try {
                StateMachine.getInstance().setEventProcessEnabled(false);

				Client.getInstance().downloadMaster();

                //Bruce/20030505
                // Update "MasterVersion"
                String masterVersion = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                CreamProperties.getInstance().setProperty("MasterVersion", masterVersion);
                CreamProperties.getInstance().deposit();

                posTerminal.getMessageIndicator().setMessage(res.getString("ProgramDownloading"));
                Trigger.getInstance().downloadProgramFiles();

                if (Client.getInstance().getNumberOfFilesGotOrMoved() > 0) {
                    POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                        CreamToolkit.GetResource().getString("UpdateCompletedAndReboot"));
                    DacTransfer.shutdown(5);
                } else {
                    POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                        CreamToolkit.GetResource().getString("UpdateCompleted"));
                }

            } catch (ClientCommandException e) {
                e.printStackTrace(CreamToolkit.getLogger());
                posTerminal.getMessageIndicator().setMessage(
                    res.getString("MasterDownloadFailed"));
            } finally {
                StateMachine.getInstance().setEventProcessEnabled(true);
            }
        }
        return ConfigState.class;
    }
}
