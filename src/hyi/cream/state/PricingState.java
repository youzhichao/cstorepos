
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

import java.math.BigDecimal;
import java.util.*;
import java.awt.event.*;
import jpos.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PricingState extends State implements PopupMenuListener {

    private String pluNumber            = "";
    private String number               = "";
    private int numberType              = 0;
    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    private PopupMenuPane p             = app.getPopupMenuPane();
    private ResourceBundle res          = CreamToolkit.GetResource();

    static PricingState pricingState    = null;

    public static PricingState getInstance() {
        try {
            if (pricingState == null) {
                pricingState = new PricingState();
            }
        } catch (InstantiationException ex) {
        }
        return pricingState;
    }

    /**
     * Constructor
     */
    public PricingState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("PricingState entry");
        Object eventSource = event.getSource();
        if (sourceState instanceof Numbering4State) {
            Numbering4State n = (Numbering4State)sourceState;
            number = n.getNumberString();
            numberType = n.getNumberType();
        } else if (sourceState instanceof PriceLookupState
                   && eventSource instanceof jpos.Scanner) {
            try {
                ((jpos.Scanner)eventSource).setDecodeData(true);
                number = new String(((jpos.Scanner)eventSource).getScanDataLabel());
                numberType = Numbering4State.PLUNO;
            } catch (JposException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Jpos exception at " + this);
            }
        } else if (sourceState instanceof PricingState
                   && eventSource instanceof jpos.Scanner) {
            try {
                ((jpos.Scanner)eventSource).setDecodeData(true);
                pluNumber = new String(((jpos.Scanner)eventSource).getScanDataLabel());
                numberType = Numbering4State.PLUNO;
            } catch (JposException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Jpos exception at " + this);
            }
        }
        PLU plu = null;

        //Bruce/20030505
        // 不再分别是输入条码还是货号
        //if (numberType == Numbering4State.PLUNO) {
        //    plu = PLU.queryByPluNumber(number);
        //} else if (numberType == Numbering4State.BARCODE) {
        //    plu = PLU.queryBarCode(number);
        //} else if (numberType == Numbering4State.INSTORECODE) {
        //    plu = PLU.queryByInStoreCode(number);
        //}
        plu = PLU.queryByPluNumber(number);
        if (plu == null)
            plu = PLU.queryByInStoreCode(number);

        if (plu != null) {            
            ArrayList menu = new ArrayList();
            menu.add(res.getString("PricingPluNumber") + plu.getPluNumber());
            menu.add(res.getString("PricingItemNumber") + plu.getInStoreCode());
            menu.add(res.getString("PricingPluName") + plu.getScreenName());
            menu.add(res.getString("PricingPluPrice") + plu.getUnitPrice());
            //menu.add(res.getString("PricingPluMemberPrice") + plu.getMemberPrice());
            
            BigDecimal specialPrice = plu.getSpecialPriceIfWithinTimeLimit();
            if (specialPrice != null)
                menu.add(res.getString("PricingPluSpecialPrice") + specialPrice);

            //menu.add(res.getString("PricingPluDiscountPrice1") + plu.getDiscountPrice1());
            //menu.add(res.getString("PricingPluDiscountPrice2") + plu.getDiscountPrice2());
            TaxType taxType = TaxType.queryByTaxID(plu.getTaxType());
            if (taxType != null)
                menu.add(res.getString("PricingPluTaxType") + taxType.getTaxName());
            else
                menu.add(res.getString("PricingPluTaxType") + plu.getTaxType());

            p.setMenu(menu);
            p.setVisible(true);
            p.addToList(this); 
            p.setInputEnabled(false);
            app.getMessageIndicator().setMessage(res.getString("Warning"));
        } else {
            app.getWarningIndicator().setMessage(res.getString("PricingWarning"));
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("PriceLookMessage"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("PricingState exit");
        app.getMessageIndicator().setMessage("");
        if (event.getSource() instanceof ClearButton) {
            p.setVisible(false);
            p.setInputEnabled(true);
            app.getWarningIndicator().setMessage("");
        }
        return sinkState.getClass();
    }

    public void doSomething() {
        if (!p.getSelectedMode()) {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        }
    }
}

