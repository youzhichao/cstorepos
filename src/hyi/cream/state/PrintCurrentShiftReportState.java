package hyi.cream.state;

import java.text.*;
import java.util.*;
import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;
import hyi.cream.dac.*;
import hyi.cream.event.POSButtonEvent;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PrintCurrentShiftReportState extends State implements PopupMenuListener {

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    static PrintCurrentShiftReportState PrintCurrentShiftReportState = null;
    private ArrayList menu = new ArrayList();
    private List shiftReports = new ArrayList();
    private PopupMenuPane popup;

    public static PrintCurrentShiftReportState getInstance() {
        try {
            if (PrintCurrentShiftReportState == null) {
                PrintCurrentShiftReportState = new PrintCurrentShiftReportState();
            }
        } catch (InstantiationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return PrintCurrentShiftReportState;
    }

    /**
     * Constructor
     */
    public PrintCurrentShiftReportState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        app.getMessageIndicator().setMessage(res.getString("ConfirmPrintShiftReport"));

        popup = POSTerminalApplication.getInstance().getPopupMenuPane();
        menu.clear();
        shiftReports.clear();
        Iterator iter = ShiftReport.queryRecentReports();
        if (iter != null) {
            int idx = 1;
            while (iter.hasNext()) {
                ShiftReport shift = (ShiftReport)iter.next();
                String signOffDateTime = sdf.format(shift.getSignOffSystemDateTime());
                if (signOffDateTime.startsWith("1970-01-01"))
                    menu.add(idx + ". " + res.getString("CurrentShiftReport"));
                else
                    menu.add(idx + ". " + signOffDateTime);
                shiftReports.add(shift);
                idx++;
            }
        }
        popup.setMenu(menu);
        popup.setVisible(true);
        popup.show();
        popup.addToList(this);
        popup.clear();
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof ClearButton) {
            app.getMessageIndicator().setMessage("");
        }
        if (sinkState == null) {
            return null;
        } else {
            return sinkState.getClass();
        }
    }

    /**
     * 当在PopupMenuPane按数字键加回车，或者按了清除键了以后，都会callback此方法。
     */
    public void doSomething() {
        int selectIndex = popup.getSelectedNumber();
        if (menu != null && menu.size() > 0) {
            System.out.println("Shift> Select " + selectIndex);
            if (selectIndex >= 0 && selectIndex < 20) {
                ShiftReport shift = (ShiftReport)shiftReports.get(selectIndex);
                shift = ShiftReport.queryByZNumberAndShiftNumber(
                    shift.getZSequenceNumber(), shift.getSequenceNumber());
                CreamPrinter.getInstance().printShiftReport(shift, this);
            }
            if (selectIndex >= -1 && selectIndex < 10) {
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome.getInstance().buttonPressed(e);
                menu.clear();
                shiftReports.clear();
            } else {
                popup.setVisible(true);
                popup.show();
                popup.addToList(this);
                popup.clear();
            }
            return;
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        }
    }
}

 
