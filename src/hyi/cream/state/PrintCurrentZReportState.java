// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   PrintCurrentZReportState.java

// Copyright (c) 2000 hyi
package hyi.cream.state;

import hyi.cream.POSButtonHome;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.ZReport;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.Indicator;
import hyi.cream.uibeans.PopupMenuListener;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

// Referenced classes of package hyi.cream.state:
//            State

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PrintCurrentZReportState extends State implements PopupMenuListener {

    private POSTerminalApplication app;
    private ResourceBundle res;
    static PrintCurrentZReportState printCurrentZReportState = null;
    private ArrayList menu;
    private List zReports;
    private PopupMenuPane popup;
    private Class exitState;

    public static PrintCurrentZReportState getInstance() {
        try {
            if (printCurrentZReportState == null)
                printCurrentZReportState = new PrintCurrentZReportState();
        } catch (InstantiationException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        return printCurrentZReportState;
    }

    public PrintCurrentZReportState() throws InstantiationException {
        app = POSTerminalApplication.getInstance();
        res = CreamToolkit.GetResource();
        menu = new ArrayList();
        zReports = new ArrayList();
    }

    public void entry(EventObject event, State sourceState) {
        exitState = sourceState.getClass();
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        app.getMessageIndicator().setMessage(res.getString("ConfirmPrintZReport"));
        popup = POSTerminalApplication.getInstance().getPopupMenuPane();
        menu.clear();
        zReports.clear();
        Iterator iter = ZReport.queryRecentReports();
        if (iter != null) {
            for (int idx = 1; iter.hasNext(); idx++) {
                ZReport z = (ZReport) iter.next();
                String endDateTime = sdf.format(z.getEndSystemDateTime());
                if (endDateTime.startsWith("1970-01-01"))
                    menu.add(idx + ". " + res.getString("CurrentZReport"));
                else
                    menu.add(idx + ". " + endDateTime);
                zReports.add(z);
            }

        }
        popup.setMenu(menu);
        popup.setVisible(true);
        // popup.show();
        popup.addToList(this);
        popup.clear();
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof ClearButton)
            app.getMessageIndicator().setMessage("");
        if (sinkState == null){
            //Bruce/2003-09-25/
            //如果在statechart3.conf中：
            // hyi.cream.state.PrintCurrentZReportState, hyi.cream.event.POSButtonEvent, hyi.cream.uibeans.ClearButton,
            //也就是没有sink state的时候，那就从哪里来就到哪里去
            //这样可以解决喜士多的“打印X帐”在Keylock2的位置也有的问题
            //但是喜士多的keylock2.conf的文字要改成“打印X/Z帐”
            return exitState;
        } else {
            return sinkState.getClass();
        }
    }

    /**
     * 当在PopupMenuPane按数字键加回车，或者按了清除键了以后，都会callback此方法。
     */
    public void doSomething() {
        int selectIndex = popup.getSelectedNumber();
        if (menu != null && menu.size() > 0) {
            if (selectIndex >= 0 && selectIndex < 20) {
                ZReport z = (ZReport) zReports.get(selectIndex);
                z = ZReport.queryBySequenceNumber(z.getSequenceNumber());
                DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String endDateTime = sdf.format(z.getEndSystemDateTime());
                
                //Ignore ReceiptPrint flag   gllg Oct 13, 2008
                CreamPrinter printer = CreamPrinter.getInstance();
                boolean originalPrinterEnableState = printer.getPrintEnabled();
                printer.setPrintEnabled(true);
                if (endDateTime.startsWith("1970-01-01"))
                    printer.printXReport(z, this);
                else
                    printer.printZReport(z, this);
                printer.setPrintEnabled(originalPrinterEnableState);
            }
            if (selectIndex >= -1 && selectIndex < 10) {
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome.getInstance().buttonPressed(e);
                menu.clear();
                zReports.clear();
            } else {
                popup.setVisible(true);
                // popup.show();
                popup.addToList(this);
                popup.clear();
            }
            return;
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
            return;
        }
    }

}
