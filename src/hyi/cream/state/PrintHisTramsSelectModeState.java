package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.uibeans.SelectButton;
import hyi.cream.util.CreamToolkit;

import java.util.EventObject;
import java.util.ResourceBundle;

public class PrintHisTramsSelectModeState extends State {

	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();
	private String numberString = "";
	private int selectType;
    //记录从哪个state进来的
    private static Class sourceState = null;

	public void entry(EventObject event, State sourceState) {
		if (event.getSource() instanceof SelectButton)
			PrintHisTramsSelectModeState.sourceState = sourceState.getClass();
		app.getMessageIndicator().setMessage(res.getString("PrintHisTramsSelect"));
	}

	public Class exit(EventObject event, State sinkState) {
		selectType = 0;
		app.getWarningIndicator().setMessage("");
		if (event.getSource() instanceof NumberButton) {
			app.getWarningIndicator().setMessage("");
			NumberButton pb = (NumberButton) event.getSource();
			numberString = pb.getNumberLabel();
			if (numberString.equals("1") || numberString.equals("2")) {
				selectType = Integer.parseInt(numberString);
				return PrintHisTransState.class;
			} else {
				String msg = res.getString("PleaseReInput");
				app.getWarningIndicator().setMessage(msg.substring(1, msg.length()));
			}
		} else if (event.getSource() instanceof ClearButton) {
			return sourceState;
		}
		return PrintHisTramsSelectModeState.class;
	}
	
	public int getSelectType() {
		return selectType;
	}
	
    public static Class getSourceState() {
        return sourceState;
    }
}
