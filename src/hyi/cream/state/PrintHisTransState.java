package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.POSButtonHome;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.Store;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.ZReport;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamPrinter;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.NumberConfuser;
import hyi.cream.util.CreamProperties;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.*;

public class PrintHisTransState extends State implements PopupMenuListener {

	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();
	private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");	
	private String numberString = "";
	private int step = 1; // 1: 2: 3: 
	private String start = "";
	private String end = "";
	private int selectType = 0;
	private int startNo = 0;
	private int endNo = 0;

	private ArrayList transNoList;
	private PopupMenuPane p =app.getPopupMenuPane();
	private int index=1;
	private Transaction tran;
	private ArrayList menu = new ArrayList();

	public PrintHisTransState() throws InstantiationException {
		app = POSTerminalApplication.getInstance();
		res = CreamToolkit.GetResource();
		transNoList = new ArrayList();
            try {
                tran = new Transaction();
            } catch (InstantiationException e) {
            }
        }
	public void entry(EventObject event, State sourceState) {
		if (sourceState instanceof PrintHisTramsSelectModeState) {
			selectType = ((PrintHisTramsSelectModeState) sourceState).getSelectType();
		}

		if (event.getSource() instanceof NumberButton && sourceState instanceof PrintHisTransState) {
			NumberButton pb = (NumberButton) event.getSource();
			numberString = numberString + pb.getNumberLabel();
		}
		String message = "";
		if (step == 1) {
			if (selectType == 1)
				message = res.getString("InputStartTransNumber") +numberString;
			else if (selectType == 2)
				message = res.getString("InputStartTime") +numberString;
		} else if (step == 2) {
			if (selectType == 1)
				message = res.getString("InputEndTransNumber")+numberString;
			else if (selectType == 2)
				message = res.getString("InputEndTime") +numberString;
		} else if (step == 3){
				step = 1;
				startNo = 0;
				endNo = 0;
				if (selectType == 1) {
					if ("yes".equalsIgnoreCase(CreamProperties.getInstance().getProperty("ConfuseTranNumber"))) {
						startNo = Integer.parseInt(NumberConfuser.defuse(start));
						endNo = Integer.parseInt(NumberConfuser.defuse(end));
					} else {
						try {
							startNo = Integer.parseInt(start);
						} catch (Exception e) {
							// input error
						}
						try {
							endNo = Integer.parseInt(end);
						} catch (Exception e) {
							// input error
						}
					}
		
				} else if (selectType == 2) {
					List list = Transaction.getTransactionsByTime(parseDateStr2(start), parseDateStr2(end));
					int size = list.size();
					if (size >= 2) {
						startNo = ((Long) list.get(0)).intValue();
						endNo = ((Long) list.get(size - 1)).intValue();
					} else if (size == 1) {
						startNo = endNo = ((Long) list.get(0)).intValue();
					}
				}
				if(startNo > endNo){
					int temp = endNo;
					endNo = startNo;
					startNo = temp;
				}
		
				transNoList = Transaction.queryPrintHistoryTransactionNumber(startNo, endNo);
				if(transNoList.size()==0){
					message = "(" +startNo + "," + endNo + ")" + res.getString("betweenTran");
				} else {
					index = 1;
					tran = Transaction.queryByTransactionNumber(transNoList.get(index-1).toString());
					queryAndShowTran(transNoList, index);
					
				}
//			
//			message = "";
//			message = MessageFormat.format(res.getString("StartPrintTrans"),
//					new String[]{ start, end});
		}
//		if (flag == true)
//			message = "";
		app.getMessageIndicator().setMessage(message);
	}

	public Class exit(EventObject event, State sinkState) {
//		flag = false;
		if (event.getSource() instanceof NumberButton) {
			app.getWarningIndicator().setMessage("");
			return PrintHisTransState.class;
		} else if (event.getSource() instanceof ClearButton) {
			if (numberString.equals("")) {
				if (step == 1)
					return PrintHisTramsSelectModeState.class;
				step--;
			}
			numberString = "";
			return PrintHisTransState.class;
		} else if (event.getSource() instanceof EnterButton) {
		


			if (selectType == 1) {
//				Transaction trans;
//				if ("yes".equalsIgnoreCase(CreamProperties.getInstance().getProperty("ConfuseTranNumber")))
//					trans = Transaction.queryByTransactionNumber(NumberConfuser.defuse(numberString));
//				else
//					trans = Transaction.queryByTransactionNumber(numberString);
//				if (trans == null) {
//					app.getWarningIndicator().setMessage(numberString+res.getString("TransactionNotFound")
//							+res.getString("PleaseReInput"));
//					numberString = "";
//				} else {
					if (step == 1)
						start = numberString;
					else if (step == 2)
						end = numberString;
					numberString = "";
					step++;
//				}
			} else if (selectType == 2) {
				if (step == 1)
					start = numberString;
				else if (step == 2)
					end = numberString;
//				String errMsg = parseDateStr(numberString);
//				if (errMsg != null)
//					app.getWarningIndicator().setMessage(errMsg+ res.getString("PleaseReInput"));
//				else
					step++;
				numberString = "";
			}
		}
		return PrintHisTransState.class;
	}
	
	private void printTrans(Transaction tran) {
		// int i = popup.getCurrTranNo();
		int i = tran.getTransactionNumber().intValue();
		app.getMessageIndicator().setMessage(res.getString("PrintTran") + String.valueOf(i));
		boolean isPageCut = true;

		if (CreamProperties.getInstance().getProperty("IsHisTransPrintNeedCut", "false").trim().equals("false"))
			isPageCut = false;
		if (i == endNo)
			isPageCut = true;
		try {
//			Transaction tran = Transaction.queryByTransactionNumber(String.valueOf(i));
//			trans.add(tran);
			//backup need to restore
			CreamPrinter printer = CreamPrinter.getInstance();
			boolean originalPrinterEnableState = printer.getPrintEnabled();
			printer.setPrintEnabled(true);
			printer.reprint(i, isPageCut, true);
			printer.setPrintEnabled(originalPrinterEnableState);
			app.getMessageIndicator().setMessage("");
		} catch (Exception e) {
		}
	}
//	private String parseDateStr(String dateStr) {
//		String errMsg = null;
//		if (dateStr.length() < 12)
//			errMsg = res.getString("InputTimeError");
//		Calendar c1 = Calendar.getInstance();
//		try {
//			Date date = new SimpleDateFormat("yyyyMMddHHmm").parse(dateStr);
//			c1.setTime(date);
//		} catch (ParseException e) {
////			e.printStackTrace();
//			errMsg = res.getString("InputTimeError");
//		}
//
////		Calendar c2 = Calendar.getInstance();
////		c2.setTime(new Date());
////		c2.add(Calendar.MONTH, -3);
////		if (c1.before(c2))
////		errMsg = res.getString("InputTimeError");
//		return errMsg;
//	}

	private String parseDateStr2(String dateStr) {
		dateStr = dateStr.substring(0, 4) + "-" + dateStr.substring(4, 6) + "-"
		+ dateStr.substring(6, 8) + " " + dateStr.substring(8, 10)
		+ ":" + dateStr.substring(10, 12);
		return dateStr;
	}

	/**
	 * 当在PopupMenuPane按数字键加回车，或者按了清除键了以后，都会callback此方法。
	 */
//	public void doSomething() {
//	int selectIndex = popup.getSelectedNumber();
//	if (menu != null && menu.size() > 0) {
//	if (selectIndex == 0) {
//	printTrans();
//	}
//	if (selectIndex >= -1 && selectIndex < 10) {
//	POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
//	POSButtonHome.getInstance().buttonPressed(e);
//	menu.clear();
//	trans.clear();
//	} else {
//	popup.setVisible(true);
//	// popup.show();
//	popup.addToList(this);
//	popup.clear();
//	}
//	return;
//	} else {
//	POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
//	POSButtonHome.getInstance().buttonPressed(e);
//	return;
//	}
//	}

	public void doSomething() {
		CreamToolkit.logMessage("p.getSelectedNumber=" + p.getSelectedNumber());
		int selectIndex = p.getSelectedNumber() + 1;
		if (selectIndex == 1) {
			index = 1;
			tran = Transaction.queryByTransactionNumber(transNoList.get(index-1).toString());
		} else if (selectIndex == 2) {
			if(index > 1){
				index--;
			}
			tran = Transaction.queryByTransactionNumber(transNoList.get(index-1).toString());
		} else if (selectIndex == 3) {
				if(index < transNoList.size()){
					index++;
				}
			tran = Transaction.queryByTransactionNumber(transNoList.get(index-1).toString());
		} else if (selectIndex == 4) {
			index = transNoList.size();
			tran = Transaction.queryByTransactionNumber(transNoList.get(index-1).toString());
		} else if(selectIndex == 5){
			String dealType =tran.getDealType1()+tran.getDealType2()+tran.getDealType3();
			if((("*00").equals(dealType)) || (("000").equals(dealType)) || (("004").equals(dealType))){
				printTrans(tran);
				app.getMessageIndicator().setMessage("");
			}
			else{
				app.getMessageIndicator().setMessage(res.getString("noprint"));
			}
			
		} else if (p.getSelectedNumber() == -1){
	        POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
	        POSButtonHome.getInstance().buttonPressed(e);
	        return;
		}
		queryAndShowTran(transNoList, index);

	}

	private void queryAndShowTran(ArrayList transNoList2, int index) {
		menu.clear();
		Transaction tran = Transaction.queryByTransactionNumber(transNoList2.get(index-1).toString());
		if (tran != null) {
			menu.add(res.getString("menufirst"));
			menu.add("------------------------------------------");
			
			menu.add(index + "/" + transNoList.size() + " "
					+ df.format(tran.getSystemDateTime())
					+ " " + res.getString("cashier1") + tran.getCashierName());
			String dealType =tran.getDealType1()+tran.getDealType2()+tran.getDealType3();
			if(("000").equals(dealType))
				dealType = res.getString("normalTran");
			else if(("*00").equals(dealType))
				dealType = res.getString("beVoidTran");
			else if(("004").equals(dealType))
				dealType = res.getString("partRefundPositive");
			else if (("040").equals(dealType))
				dealType = res.getString("paidInOut");
            else if (("080").equals(dealType))
            	dealType = res.getString("Sign-on");
            else if (("090").equals(dealType))
            	dealType = res.getString("Sign-off");
            else if (("0B0").equals(dealType))
            	dealType = res.getString("onDuty");
            else if (("0C0").equals(dealType))
            	dealType = res.getString("offDuty");
            else if (("0D0").equals(dealType))
            	dealType = res.getString("endOfDate");
            else if (("0G0").equals(dealType))
            	dealType = res.getString("CashOut");
            else if (("0H0").equals(dealType))
            	dealType = res.getString("CashIn");
            else if (("001").equals(dealType))
            	dealType = res.getString("previousNegative");
            else if (("002").equals(dealType))
            	dealType = res.getString("ReprintNegative");
            else if (("003").equals(dealType))
            	dealType = res.getString("CancelNegative");
            else if (("034").equals(dealType))
            	dealType = res.getString("refundNegative");
           

			menu.add("     " + res.getString("machine1") + tran.getTerminalNumber()
					+ " " + res.getString("tranNoLabel") + tran.getTransactionNumber() 
					+ " " + res.getString("typeLabel") + dealType);
			Object[] objs = tran.getLineItemsArrayLast();
	
			for (int i = 0; i < objs.length; i++) {
				LineItem lineItem = (LineItem) objs[i];
				menu.add(lineItem.getDescription() + " " + lineItem.getUnitPrice() + (lineItem.getQuantity().intValue() == 1 ? "" : " x " + lineItem.getQuantity().intValue()));
			}
			if (tran.getDaiShouAmount().doubleValue() != 0)
				menu.add(res.getString("daishou1") + tran.getDaiShouAmount());
			if (tran.getDaiShouAmount2().doubleValue() != 0)
				menu.add(res.getString("daishou21") + tran.getDaiShouAmount2());
			if (tran.getGrossSalesAmtWithDsct().doubleValue() != 0)
				menu.add(res.getString("saleTot") + tran.getGrossSalesAmtWithDsct());
			if (tran.getTotalMMAmount().doubleValue() != 0)
				menu.add(res.getString("mnmTot") + tran.getTotalMMAmount());
			if (tran.getSIAmtList() != null && tran.getSIAmtList().size() != 0)
				menu.add(res.getString("dsctType") + tran.getSIDescription());
			if (tran.getSalesAmtWithItemDsct().doubleValue() != 0)
				menu.add(res.getString("total3") + tran.getSalesAmtWithItemDsct());
			if (tran.getPayName1() != null)
				menu.add(tran.getPayName1() + ":" + tran.getPayAmount1());
			if (tran.getPayAmount2().doubleValue() != 0)
				menu.add(tran.getPayName2() + ":" + tran.getPayAmount2());
			if (tran.getPayAmount3().doubleValue() != 0)
				menu.add(tran.getPayName3() + ":" + tran.getPayAmount3());
			if (tran.getPayAmount4().doubleValue() != 0)
				menu.add(tran.getPayName4() + ":" + tran.getPayAmount4());
			if (tran.getChangeAmount().doubleValue() != 0)
				menu.add(res.getString("change1") + tran.getChangeAmount());
			if (tran.getSpillAmount().doubleValue() != 0)
				menu.add(res.getString("over1") + tran.getSpillAmount());
		} else {
			// TODO show error message
		}
		if(menu.size()<=5){
			menu.add("");
			}
		p.setMenu(menu);
		p.setVisible(true);
		p.addToList(this);
		p.clear();
//		flag = true;
	}
}



