/**
 * State class
 * @since 2000
 * @author slackware
 */
 

package hyi.cream.state;

//for event processing
import java.util.*;
import hyi.cream.*;
import jpos.*;

//for transaction
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;

//for print
import hyi.cream.util.*;

//for maths calculation
import java.math.*;

public class PrintPluState extends State {
    static PrintPluState printPluState = null;
	
    public static PrintPluState getInstance() {
        try {
            if (printPluState == null) {
                printPluState = new PrintPluState();
            }
        } catch (InstantiationException ex) {
        }
        return printPluState;
    }

    /**
     * Constructor
     */
    public PrintPluState() throws InstantiationException {
    }

	public void entry (EventObject event, State sourceSate) {
        //System.out.println("PrintPluState entry");
		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        if (posTerminal.getCurrentTransaction().getDealType2().equals("4")) {
            return;
        }

//MixAndMatchState VoidState OpenPriceState	null
		if (sourceSate.getClass().getName().endsWith("MixAndMatchState")
			|| sourceSate.getClass().getName().endsWith("VoidState")
			|| sourceSate.getClass().getName().endsWith("OpenPriceState")
            || sourceSate.getClass().getName().endsWith("DaiShou2ReadyState")) {
            //System.out.println("This is PrintPluState's Entry1!");
			Transaction cTransaction = posTerminal.getCurrentTransaction();
			Object[] lineItemArrayLast = cTransaction.getLineItemsArrayLast();
//If lastLineItem of currentTransaction is printed, do nothing and return.
//If lastLineItem of currentTransaction is 指定更正，then print all line items out.		
			if (lineItemArrayLast.length > 1) {
				if (!CreamPrinter.getInstance().getHeaderPrinted() ) {
					CreamPrinter.getInstance().printHeader();
					CreamPrinter.getInstance().setHeaderPrinted(true);
				}
				LineItem lineItem = (LineItem)lineItemArrayLast[lineItemArrayLast.length - 2];
				/*(if (lineItem.getDetailCode().equals("V")) {
					CreamPrinter.getInstance().printLineItem(lineItem);
				}*/
				//lineItem = (LineItem)lineItemArrayLast[lineItemArrayLast.length - 2];
				CreamPrinter.getInstance().printLineItem(lineItem);
//Otherwise, print the second last LineItem of currentTransaction to printer
//if it has not been printed yet. (Ref. Receipt Layout)
//Print小计and cut/feed paper if needed.
			}
			if (!cTransaction.getBuyerNumber().equals("")
				&& !posTerminal.getBuyerNumberPrinted()) {
				//CreamPrinter.getInstance().printBuyerNumber(cTransaction.getBuyerNumber());
				posTerminal.setBuyerNumberPrinted(true);
			}
            //System.out.println("This is PrintPluState's Exit1!");
		}
        CreamToolkit.showText(posTerminal.getCurrentTransaction(), 0);
	}

	public Class exit(EventObject event, State sinkState) {    
        //System.out.println("PrintPluState exit!");
		//Numbering2State	NumberButton	Do nothing
        if (sinkState != null)
            return sinkState.getClass();
        else
            return null;
	}             
}

