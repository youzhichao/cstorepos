// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.event.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;
import jpos.*;

/**
 * A Class class.
 * <P>
 * @author Slackware
 */
public class ReprintState extends State {
    private Transaction previousTran = null;
    private String number = "";
    private int    previousNumber  = 0;
    private ResourceBundle res = CreamToolkit.GetResource();
    private boolean isGenerateVoidTran ;
    /**
     * Constructor
     */
    public ReprintState() {

    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("This is ReprintState's Entry!");
        this.isGenerateVoidTran = ((ReprintButton) event.getSource()).isGenerateVoidTran();
        POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        number = String.valueOf(Transaction.getNextTransactionNumber());
        /*if (posTerminal.getTrainingMode()) {
            POSButtonHome.getInstance().buttonPressed(new POSButtonEvent(new ClearButton(0, 0, 0, "")));
            return;
        }*/
        //IdleState    CancelButton
        previousNumber = Integer.parseInt(number) - 1;
        previousTran = posTerminal.getCurrentTransaction();
        previousTran = Transaction.queryByTransactionNumber(String.valueOf(previousNumber), true);
        if (!previousTran.getDealType1().equals("0")
            || !previousTran.getDealType2().equals("0")
            || (!previousTran.getDealType3().equals("0") && !previousTran.getDealType3().equals("4"))
            ) {
            posTerminal.getWarningIndicator().setMessage(res.getString("ReturnWarning"));
            POSButtonHome.getInstance().buttonPressed(new POSButtonEvent(new ClearButton(0, 0, 0, "")));
            return;
        }
        if (!previousTran.getDealType1().equals("*")) {

        } else {
            posTerminal.getWarningIndicator().setMessage(res.getString("ReturnWarning"));
            POSButtonHome.getInstance().buttonPressed(new POSButtonEvent(new ClearButton(0, 0, 0, "")));
            return;
        }
        ResourceBundle res = CreamToolkit.GetResource();
        posTerminal.getMessageIndicator().setMessage(res.getString("ReprintConfirm"));
        previousTran.setDealType1("0");
        previousTran.setDealType2("0");
        previousTran.setDealType3("2");
        Iterator iter = LineItem.queryByTransactionNumber(String.valueOf(previousNumber));
        while (iter.hasNext()) {
            LineItem li = (LineItem)iter.next();
            try {
                posTerminal.getCurrentTransaction().setLockEnable(true);
                posTerminal.getCurrentTransaction().addLineItem(li);
            } catch (TooManyLineItemsException e) {
                System.out.println(e);
            }
        }

    }

    public Class exit(EventObject event, State sinkState) {
        POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        if (event.getSource() instanceof EnterButton) {
            //if generate void transaction    gllg  Oct 14, 2008
            if (!isGenerateVoidTran) {
                posTerminal.getMessageIndicator().setMessage("重印中请稍候...");

                CreamPrinter printer = CreamPrinter.getInstance();
                boolean originalPrinterEnableState = printer.getPrintEnabled();
                printer.setPrintEnabled(true);
                printer.reprint(previousTran);
                printer.setPrintEnabled(originalPrinterEnableState);

                posTerminal.getCurrentTransaction().clearLineItem();
                posTerminal.getCurrentTransaction().Clear();
                posTerminal.getCurrentTransaction().setDealType1("0");
                posTerminal.getCurrentTransaction().setDealType2("0");
                posTerminal.getCurrentTransaction().setDealType3("0");
                posTerminal.getItemList().setItemIndex(0);
                posTerminal.getItemList().setTransaction(posTerminal.getCurrentTransaction());
                posTerminal.getCurrentTransaction().Clear();
                posTerminal.getItemList().repaint();
                return sinkState.getClass();
            }

            if (!previousTran.getDealType1().equals("*")) {
                previousTran.setDealType1("*");
                previousTran.setDealType3("0");
                if (posTerminal.getTrainingMode()) {
                    previousTran.Clear();
                } else {
                    previousTran.update();
                }
            }
            posTerminal.getMessageIndicator().setMessage("重印中请稍候...");
            previousTran.setTransactionNumber(previousTran.getTransactionNumber().intValue() + 2);
            //gllg
            //Bruce/20081010/ Always print the receipt out while reprinting
            CreamPrinter printer = CreamPrinter.getInstance();
            boolean originalPrinterEnableState = printer.getPrintEnabled();
            printer.setPrintEnabled(true);
            printer.reprint(previousTran);
            printer.setPrintEnabled(originalPrinterEnableState);

            Transaction abolishTran = previousTran;
            abolishTran.setDealType1("0");
            abolishTran.setDealType3("2");
            abolishTran.setTransactionNumber(Integer.parseInt(number));
            abolishTran.setVoidTransactionNumber(new Integer(previousNumber));
            abolishTran.setCustomerCount(0 - abolishTran.getCustomerCount().intValue());
            abolishTran.makeNegativeValue();

            if (posTerminal.getTrainingMode()) {
                abolishTran.Clear();
            } else {
                //abolish old token details and insert with new transactionNumber corresponding with old ones  gllg
                dealTokenDtl(abolishTran);
                CreamToolkit.logMessage("store() in ReprintState exit() L134");
                abolishTran.store();
            }

            Transaction newTran = abolishTran;
            newTran.makeNegativeValue();
            newTran.setDealType1("0");
            newTran.setTransactionNumber(Integer.parseInt(number) + 1);
            newTran.setVoidTransactionNumber(null);
            newTran.setDealType3("0");
            if (posTerminal.getTrainingMode()) {
                newTran.Clear();
            } else {
            	CreamToolkit.logMessage("store() in ReprintState exit() L147");
                newTran.store();
            }

            /*
            String nextInvoice = CreamProperties.getInstance().getProperty("NextInvoiceNumber");
            int nextTran = Integer.parseInt(number) + 2;
            int nextIn =  Integer.parseInt(nextInvoice) + 2;
            CreamProperties.getInstance().put("NextInvoiceNumber", String.valueOf(nextIn));
            CreamProperties.getInstance().put("NextTransactionSequenceNumber", String.valueOf(nextTran));
            CreamProperties.getInstance().deposit();
            */

            //
            ResourceBundle res = CreamToolkit.GetResource();
            posTerminal.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("ReprintEnd"));
            posTerminal.setChecked(false);
        }
        if (event.getSource() instanceof ClearButton) {
            posTerminal.getCurrentTransaction().clearLineItem();
            posTerminal.getCurrentTransaction().Clear();
            posTerminal.getCurrentTransaction().setDealType1("0");
            posTerminal.getCurrentTransaction().setDealType1("0");
            posTerminal.getCurrentTransaction().setDealType1("0");
            posTerminal.getItemList().setItemIndex(0);
        }
        posTerminal.getItemList().setTransaction(posTerminal.getCurrentTransaction());
        posTerminal.getCurrentTransaction().Clear();
        posTerminal.getItemList().repaint();
        return  sinkState.getClass();
    }

    private void dealTokenDtl(Transaction abolishTran) {
        Iterator it = TokenTranDtl.queryByTranNo(String.valueOf(previousNumber));
        if (it != null) {
            try {
                TokenTranDtl ttd;
                while (it.hasNext()) {
                    ttd = (TokenTranDtl) it.next();
                    ttd.setTransactionNumber(abolishTran.getTransactionNumber().intValue());
                    //ttd.setSystemDate(abolishTran.getSystemDateTime());
                    ttd.setSaleQuantity(ttd.getSaleQuantity().negate());
                    ttd.setTokenQuantity(ttd.getTokenQuantity().negate());
                    ttd.setTokenAmount(ttd.getTokenAmount().negate());
                    ttd.insert();

                    ttd.setTransactionNumber(abolishTran.getTransactionNumber().intValue() + 1);
                    //ttd.setSystemDate(abolishTran.getSystemDateTime());
                    ttd.setSaleQuantity(ttd.getSaleQuantity().negate());
                    ttd.setTokenQuantity(ttd.getTokenQuantity().negate());
                    ttd.setTokenAmount(ttd.getTokenAmount().negate());
                    ttd.insert();
                }
            } catch (Exception te) {
                System.out.println("te = " + te);
                te.printStackTrace(CreamToolkit.getLogger());
            }

        }
    }
}

