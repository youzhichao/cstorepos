package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class ReturnAllState extends State {
    static ReturnAllState returnAllState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Transaction trans = app.getCurrentTransaction();
    private Transaction curTran = null;
    private Transaction oldTran = null;
    private String transactionID = "";
    private ArrayList siArray = new ArrayList();
    private ResourceBundle res = CreamToolkit.GetResource();

    public static ReturnAllState getInstance() {
        try {
            if (returnAllState == null) {
                returnAllState = new ReturnAllState();
            }
        } catch (InstantiationException ex) {
        }
        return returnAllState;
    }

    public ReturnAllState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof ClearButton
            && sourceState instanceof ReturnSummaryState) {
            app.getMessageIndicator().setMessage(res.getString("ReturnAll"));
            trans.makeNegativeValue();
            trans.fireEvent(new TransactionEvent(trans, TransactionEvent.TRANS_INFO_CHANGED));                

        } else if (event.getSource() instanceof EnterButton
                    && sourceState instanceof ReturnSelect1State) { 

            app.getMessageIndicator().setMessage(res.getString("ReturnAll"));
            transactionID = ((ReturnSelect1State)sourceState).getTransactionID();
            oldTran = ((ReturnSelect1State)sourceState).getOldTran();
            siArray = ((ReturnSelect1State)sourceState).getSIArray();
            curTran = trans;
            curTran.setDealType3("4");  //Bruce/20080613/
            curTran.setReturnAll(true);  //Bruce/20080613/ 全部退货标记
            trans.makeNegativeValue();
            trans.fireEvent(new TransactionEvent(trans, TransactionEvent.TRANS_INFO_CHANGED));                

        }
    }

    public Class exit(EventObject event, State sinkState) {
    	
    	if(event.getSource() instanceof ClearButton
    			&& sinkState instanceof ReturnShowState ){
    		 trans.makeNegativeValue();
    		 trans.fireEvent(new TransactionEvent(trans, TransactionEvent.TRANS_INFO_CHANGED));                
    	}
        return sinkState.getClass();
    }

    public Transaction getShowTran() {
        return curTran;
    }

    public Transaction getOldTran() {
        return oldTran;
    }
    
    public String getTransactionID() {
        return transactionID;
    }
    
    public ArrayList getSIArray() {
        return siArray;
    }
    
}

