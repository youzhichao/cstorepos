package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class ReturnOneState extends State {
	static ReturnOneState returnOneState = null;
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private Transaction curTran = app.getCurrentTransaction();
	private Transaction oldTran = null;
	private String transactionID = "";
	private ArrayList selectArray = new ArrayList();
    private ArrayList siArray = new ArrayList();
	private ResourceBundle res = CreamToolkit.GetResource();

	public static ReturnOneState getInstance() {
		try {
			if (returnOneState == null) {
				returnOneState = new ReturnOneState();
			}
		} catch (InstantiationException ex) {
		}
		return returnOneState;
	}

	/**
	 * Constructor
	 */
	public ReturnOneState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {

		if (sourceState instanceof MixAndMatchState) {
			selectArray = ((MixAndMatchState) sourceState).getSelectArray();
			if (selectArray.size() > 0) {
				app.getMessageIndicator().setMessage(res.getString("ReturnPLU2"));
			} else {
				app.getMessageIndicator().setMessage(res.getString("ReturnPLU"));
			}
			curTran.setTotalMMAmount(curTran.getTaxMMAmount());
            
            clearSIInfo(curTran);

            for (int i = 0; i < siArray.size(); i++) {
                DiscountState.getInstance().processDiscount((SI)siArray.get(i),curTran, true);
            }            
            
		} else {
			if (event.getSource() instanceof EnterButton && sourceState instanceof ReturnSelect1State) {
				transactionID = ((ReturnSelect1State) sourceState).getTransactionID();
				oldTran = ((ReturnSelect1State) sourceState).getOldTran();
				curTran.setDealType3("4");  //Bruce
                siArray = ((ReturnSelect1State) sourceState).getSIArray();
                
				app.getMessageIndicator().setMessage(res.getString("ReturnPLU"));
				selectArray.clear();

			} else if (event.getSource() instanceof ClearButton) {
                app.getWarningIndicator().setMessage("");
				if (selectArray.size() > 0) {
					app.getMessageIndicator().setMessage(res.getString("ReturnPLU2"));
				} else {
					app.getMessageIndicator().setMessage(res.getString("ReturnPLU"));
				}
                                
			} else if (event.getSource() instanceof EnterButton && sourceState instanceof ReturnSelect2State) {
				if (selectArray.size() > 0) {
					app.getMessageIndicator().setMessage(res.getString("ReturnPLU2"));
				} else {
					app.getMessageIndicator().setMessage(res.getString("ReturnPLU"));
				}
			}
		}
		app.getItemList().selectFinished();
	}

	public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");
        
        if (event.getSource() instanceof NumberButton && sinkState instanceof ReturnSelect2State) {
            return ReturnSelect2State.class;
		}

		if (event.getSource() instanceof EnterButton) {
			if (selectArray.size() == 0) {
				app.getMessageIndicator().setMessage(res.getString("ReturnPLU4"));
				return ReturnOneState.class;
			}

			//  get old pay cash amount in old transaction
			String fieldValue = "";
			BigDecimal oldAmount = new BigDecimal(0);
			//BigDecimal oldAmount = null;
			for (int i = 1; i < 5; i++) {
				fieldValue = (String) oldTran.getFieldValue("PAYNO" + i);
				if (fieldValue.equals("00")) {
					oldAmount = (BigDecimal) oldTran.getFieldValue("PAYAMT" + i);
					break;
				}
			}
			oldAmount = oldAmount.subtract(oldTran.getChangeAmount());

			//  get new return amount in current transaction
			LineItem lineItem = null;
			BigDecimal returnAmount = new BigDecimal(0);
            Iterator iter = oldTran.getLineItems();
			int n = 1;
			while (iter.hasNext()) {
				lineItem = (LineItem) iter.next();
				if (selectArray.contains(new Integer(n))) {
					if ((lineItem.getDetailCode().equals("S")
						|| lineItem.getDetailCode().equals("R")
						|| lineItem.getDetailCode().equals("I")  //代售
                        || lineItem.getDetailCode().equals("O")) //代收
						&& !lineItem.getRemoved()) {
						returnAmount = returnAmount.add(lineItem.getAfterDiscountAmount());
					}
				}
				n = n + 1;
			}

			//  check old pay cash and return amount
			if (returnAmount.compareTo(oldAmount) == 1) {
				app.getMessageIndicator().setMessage(res.getString("ReturnPLU3"));
				return ReturnOneState.class;
			} 

			app.getItemList().selectFinished();

			return ReturnSummaryState.class;

		}

		return sinkState.getClass();
	}

    //清除所有SI信息
    private void clearSIInfo(Transaction t) {
        t.initSIInfo();
        t.setSIAmtList(null, null);
        try {
            t.removeSILineItem();
        } catch (LineItemNotFoundException le) { le.printStackTrace(CreamToolkit.getLogger()); }
        
    }
    
	public Transaction getOldTran() {
		return oldTran;
	}

	public ArrayList getSelectArray() {
		return selectArray;
	}

	public String getTransactionID() {
		return transactionID;
	}
    
    public ArrayList getSIArray() {
        return siArray;
    }    
}
