package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class ReturnSelect1State extends State {
    static ReturnSelect1State returnSelect1State = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Transaction trans = app.getCurrentTransaction();
    private Transaction oldTran = null;
    private String numberString = "";
    private String transactionID = "";
    private String returnType;
    private ArrayList siArray = new ArrayList();
    private ResourceBundle res = CreamToolkit.GetResource();

    public static ReturnSelect1State getInstance() {
        try {
            if (returnSelect1State == null) {
                returnSelect1State = new ReturnSelect1State();
            }
        } catch (InstantiationException ex) {
        }
        return returnSelect1State;
    }

    /**
     * Constructor
     */
    public ReturnSelect1State() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        app.getWarningIndicator().setMessage("");

        if (event.getSource() instanceof NumberButton) {
            if (sourceState instanceof ReturnShowState) { 
                ReturnShowState rsState = (ReturnShowState)sourceState;
                oldTran = rsState.getOldTran();
                siArray = rsState.getSIArray();
                transactionID = rsState.getTransactionID();
            }
                
            returnType = ((NumberButton)event.getSource()).getNumberLabel();
            app.getMessageIndicator().setMessage(returnType);
        } else if (event.getSource() instanceof ClearButton
                    && sourceState instanceof ReturnAllState) {
            app.getMessageIndicator().setMessage(res.getString("ReturnSelect1"));
        }                 
    }

    public Class exit(EventObject event, State sinkState) {
		CreamToolkit.logMessage("ReturnSelect1State | returnType : " + returnType); 
        if (event.getSource() instanceof EnterButton) {
            app.getMessageIndicator().setMessage("");
            if (returnType.equals("1")) {
                if (canReturnAll()) 
				    return ReturnAllState.class;
                else {
                    app.getWarningIndicator().setMessage(res.getString("DontAllowDaiShou2Return"));
                    return ReturnShowState.class;
                }
            } else if (returnType.equals("2")) {

                // block partial refund  gllg
                Iterator it = TokenTranDtl.queryByTranNo(oldTran.getTransactionNumber().toString());
                while (it.hasNext()) {
                    app.getWarningIndicator().setMessage("该交易使用过折价劵支付不能部分退！");
                    return ReturnShowState.class;
                }                
                
                //Bruce/20030423/
                // Check property "DontAllowPartialReturn" for 1ck1
                String x = CreamProperties.getInstance().getProperty("DontAllowPartialReturn", "no");
                if (x.equalsIgnoreCase("yes")) {
                    app.getWarningIndicator().setMessage(res.getString("DontAllowPartialReturn"));
                    return ReturnShowState.class;
                } else {
                    app.getWarningIndicator().setMessage("");
                    return ReturnOneState.class;
                }
            } else {
                app.getWarningIndicator().setMessage("");
                return ReturnShowState.class;
            }
        }

        if (event.getSource() instanceof ClearButton) {
            app.getWarningIndicator().setMessage("");
            return ReturnShowState.class;
        }

        return sinkState.getClass();
    }

    private boolean canReturnAll() {
        boolean b = true;
        String tmp = CreamProperties.getInstance().getProperty("CanDaiShou2Return", "yes");
        if (tmp.equalsIgnoreCase("no")) {
            //看交易中是否有代收
            if (oldTran.getDaiShouAmount2().compareTo(new BigDecimal(0)) == 1) 
                b = false;
        }
        
        return b;
    }
    
    public String getTransactionID() {
        return transactionID;
    }
   
    public Transaction getOldTran() {
        return oldTran;
    }    
    
    public ArrayList getSIArray() {
        return siArray;
    }
}

