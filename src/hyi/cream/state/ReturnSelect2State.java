package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class ReturnSelect2State extends State {
    static ReturnSelect2State returnSelect2State = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Transaction trans = app.getCurrentTransaction();
    private String numberString = "";
    private ArrayList selectArray = new ArrayList();
    private ResourceBundle res = CreamToolkit.GetResource();

    public static ReturnSelect2State getInstance() {
        try {
            if (returnSelect2State == null) {
                returnSelect2State = new ReturnSelect2State();
            }
        } catch (InstantiationException ex) {
        }
        return returnSelect2State;
    }

    public ReturnSelect2State() throws InstantiationException {

    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof NumberButton
            && sourceState instanceof ReturnOneState) {
            numberString = ((NumberButton)event.getSource()).getNumberLabel();
            app.getMessageIndicator().setMessage(numberString);
            app.getWarningIndicator().setMessage("");
            app.getItemList().setSelectionMode(1);
        } else if (event.getSource() instanceof NumberButton
            && sourceState instanceof ReturnSelect2State) {
            numberString = numberString + ((NumberButton)event.getSource()).getNumberLabel();
            app.getMessageIndicator().setMessage(numberString);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof EnterButton) {
            if (CreamToolkit.checkInput(numberString, 0)) {
                String tmp = CreamProperties.getInstance().getProperty("CanDaiShou2Return", "yes");
                
                app.getMessageIndicator().setMessage("");
                Object[] lineItems = trans.getLineItemsArrayLast();
                LineItem li = null;
                for (int i = 0; i < lineItems.length; i++) {
                    if (Integer.parseInt(numberString) == (i + 1)) {
                        li = ((LineItem)lineItems[i]);
                        
                        if (tmp.equalsIgnoreCase("no") && li.getDetailCode().equalsIgnoreCase("O")) {
                        //代收交易不允许退
                            numberString = "";
                            app.getWarningIndicator().setMessage(res.getString("DontAllowDaiShou2Return"));
                            return ReturnOneState.class;                 
                        }
                        
                        if (selectArray.contains(new Integer(i + 1))) {
                            selectArray.remove(new Integer(i + 1));
                            li.setRemoved(false);
                            app.getItemList().setSelectedItem(i + 1);
                            try {
                                trans.changeLineItem(i, li);
                            } catch (LineItemNotFoundException e) {
                            }
                            break;
                        } else {
                            selectArray.add(new Integer(i + 1));
                            app.getItemList().setSelectedItem(i + 1);
                            li.setRemoved(true);
                            try {
                                trans.changeLineItem(i, li);
                            } catch (LineItemNotFoundException e) {
                            }
                            break;
                        }
                    }
                }
                numberString = "";
                return MixAndMatchState.class;
            } else {                        
                numberString = "";
                app.getWarningIndicator().setMessage(res.getString("InputWrong"));
                return ReturnOneState.class;
            }
        }

        if (event.getSource() instanceof ClearButton) {
            if (numberString.equals("")) {
                return ReturnOneState.class;
            } else {
                numberString = "";
                app.getMessageIndicator().setMessage("");
                return ReturnOneState.class;
            }
        }

        return sinkState.getClass();
    }

    public ArrayList getSelectArray() {
        return selectArray;
    }
}

