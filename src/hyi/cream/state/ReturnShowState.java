package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class ReturnShowState extends State {
    static ReturnShowState returnShowState = null;
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private Transaction trans = app.getCurrentTransaction();
    private Transaction oldTran = null;
    private String transactionID = "";
    private ArrayList siArray = new ArrayList();
    private ResourceBundle res = CreamToolkit.GetResource();

    public static ReturnShowState getInstance() {
        try {
            if (returnShowState == null) {
                returnShowState = new ReturnShowState();
            }
        } catch (InstantiationException ex) {
        }
        return returnShowState;
    }

    /**
     * Constructor
     */
    public ReturnShowState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof EnterButton
            && sourceState instanceof ReturnNumberState) {
            ReturnNumberState rtnState = (ReturnNumberState)sourceState;
            transactionID = rtnState.getTransactionID();     
            oldTran = rtnState.getOldTran();
            Transaction tmpTran = (Transaction)oldTran.deepClone();
            
            fillLineItems(tmpTran);
        } else if (event.getSource() instanceof ClearButton
            && sourceState instanceof ReturnOneState) {
            
            Object[] l = app.getCurrentTransaction().getLineItemsArrayLast();
            for (int i = 0; i < l.length; i++) {
                ((LineItem)l[i]).setRemoved(false);
            }
            ItemList i = app.getItemList();
            i.selectFinished();
            
            trans.Clear();
            trans.clearLineItem();
            
            Transaction tmpTran = (Transaction)oldTran.deepClone();
            fillLineItems(tmpTran);
            app.getItemList().setItemIndex(0);
        } else if (event.getSource() instanceof ClearButton
                    && sourceState instanceof ReturnSummaryState) {

            trans.Clear();
            trans.clearLineItem();
            
            Transaction tmpTran = (Transaction)oldTran.deepClone();
            fillLineItems(tmpTran);
            app.getItemList().setItemIndex(0);
        }
        
        app.getMessageIndicator().setMessage(res.getString("ReturnSelect1"));
    }

    public Class exit(EventObject event, State sinkState) {
        if (event.getSource() instanceof NumberButton) {
            if (((NumberButton)event.getSource()).getNumberLabel().equals("1")
                || ((NumberButton)event.getSource()).getNumberLabel().equals("2")) {
                app.getMessageIndicator().setMessage("");
                return ReturnSelect1State.class;
            } else {
                return ReturnShowState.class;
            }
        } else if (event.getSource() instanceof ClearButton) {
            transactionID = "";                 
            app.getMessageIndicator().setMessage("");
            app.getWarningIndicator().setMessage("");
            trans.Clear();
            trans.clearLineItem();
            trans.setTotalMMAmount(new BigDecimal(0));
            return ReturnNumberState.class;
        }

        return sinkState.getClass();
    }

    private void fillLineItems(Transaction sourceTran) {
        LineItem lineItem = null;
        BigDecimal mmAmount = new BigDecimal(0);
        
        Iterator iter = sourceTran.getLineItemsSimpleVersion();
        
        while (iter.hasNext()) {
            lineItem = (LineItem)iter.next();
            
            //    "S":销售
            //    "R":退货
            //    "D":SI折扣
            //    "T":ItemDiscount折扣
            //    "M":Mix & Match折扣
            //    "B":退瓶
            //    "I":代售
            //    "O":代收
            //    "Q":代付
            //    "V":指定更正
            //    "E":立即更正      
                   
            if (lineItem.getDetailCode().equals("M")) {
                mmAmount = mmAmount.add(lineItem.getAmount());
            } else if (lineItem.getDetailCode().equals("D")) {
                String siID = lineItem.getPluNumber();
                SI siDac = SI.queryBySIID(siID);
                siDac.setValue(lineItem.getUnitPrice());
            //    trans.setSIAmtList(siDac.getSIID(), lineItem.getAmount());
                trans.setSIAmtList(siID, lineItem.getAmount());
                siArray.add(siDac);
            }
            
            if ((!lineItem.getDetailCode().equals("S")
                 && !lineItem.getDetailCode().equals("R")
                 && !lineItem.getDetailCode().equals("I")
                 && !lineItem.getDetailCode().equals("O"))
                || lineItem.getRemoved()) {
                continue;
            }
            
            try {
                trans.setLockEnable(true);
                trans.addLineItem(lineItem);
            } catch (TooManyLineItemsException e) {
            }
        }

        trans.setTotalMMAmount(mmAmount.negate());
        app.getItemList().setItemIndex(0);
        app.getItemList().setTransaction(trans);
        trans.fireEvent(new TransactionEvent(trans, TransactionEvent.TRANS_INFO_CHANGED));
    }
    
    
    public String getTransactionID() {
        return transactionID;
    }

    public Transaction getOldTran() {
        return oldTran;
    }
    
    public ArrayList getSIArray() {
        return siArray;
    }
}

