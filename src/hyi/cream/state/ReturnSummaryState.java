package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.inline.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class ReturnSummaryState extends State {
	static ReturnSummaryState returnSummaryState = null;
	private POSTerminalApplication app = POSTerminalApplication.getInstance();
	private ResourceBundle res = CreamToolkit.GetResource();
	private Transaction trans = app.getCurrentTransaction();
	private Transaction oldTran = null;
	private Transaction curTran = null;
	private Transaction newTran = null;
	private Transaction showTran = null;

	private ArrayList selectArray = new ArrayList();
	private ArrayList siArray = new ArrayList();
	private boolean state = false;
	private String transactionID = "";
	private BigDecimal returnQty = new BigDecimal(0);
    //交易SI折扣信息
	private Hashtable siAmtList = new Hashtable();
    
    public static ReturnSummaryState getInstance() {
		try {
			if (returnSummaryState == null) {
				returnSummaryState = new ReturnSummaryState();
			}
		} catch (InstantiationException ex) {
		}
		return returnSummaryState;
	}

	public ReturnSummaryState() throws InstantiationException {
	}

	public void entry(EventObject event, State sourceState) {
		app.getMessageIndicator().setMessage("");

		if (sourceState instanceof ReturnAllState) {
			ReturnAllState ras = (ReturnAllState) sourceState;
			oldTran = ras.getOldTran();
			curTran = ras.getShowTran();
			showTran = trans;
			transactionID = ras.getTransactionID();

			curTran.setCustomerCount(-1);
			state = false;
		} else if (sourceState instanceof ReturnOneState) {
			oldTran = ((ReturnOneState) sourceState).getOldTran();
			newTran = (Transaction) trans.clone();

            //把折扣信息保存一份副本，准备重新加入到将要生成的　dealType (0,0,4) 的交易中
            siAmtList = new Hashtable(newTran.getSIAmtList());

			transactionID = ((ReturnOneState) sourceState).getTransactionID();
			selectArray = ((ReturnOneState) sourceState).getSelectArray();
			siArray = ((ReturnOneState) sourceState).getSIArray();

			showTran = (Transaction)trans.clone();
			BigDecimal amount = new BigDecimal(0);

			showTran.initSIInfo();
			showTran.setSIAmtList(null, null);

			amount = showTran.getSalesAmount().subtract(oldTran.getSalesAmount());
			
            showTran.setGrossSalesAmount(amount);
			showTran.setSalesAmount(amount);

			showTran.setTotalMMAmount(showTran.getMixAndMatchTotalAmount().subtract(oldTran.getTaxMMAmount()).negate());
			showTran.setDaiShouAmount(new BigDecimal(0));
			state = true;
		}

		PayingPaneBanner payingPane = app.getPayingPane();
		payingPane.setTransaction(showTran);
		app.getItemList().setVisible(false);
		app.getPayingPane().setVisible(true);
		CreamToolkit.showText(showTran, 1);
	}

	public Class exit(EventObject event, State sinkState) {
		//  state true means 部分退  false means 全退
		if (state) {
			if (event.getSource() instanceof ClearButton) {
				app.getPayingPane().setVisible(false);
				app.getItemList().setVisible(true);
				return ReturnOneState.class;
			} else if (event.getSource() instanceof EnterButton) {

                app.getMessageIndicator().setMessage(res.getString("DataTransfers"));
				//作废以前的交易
				cancelTran(oldTran);
				//生成负向交易
				curTran = makeReturnTran((Transaction) oldTran.deepClone());
				CreamToolkit.logMessage("store() in returnSummaryState exit() L109");
				curTran.store();
                
                //设置新交易的头信息
                setNewTranHead(newTran);
                //打印头信息
                CreamPrinter.getInstance().printHeader(newTran);

                //生成新的不包含已退明细的交易并打印
                setNewTranItemAndPrint(newTran);
                app.setReturnQty(returnQty);
                
                //把折扣信息加入
                Iterator itr = siAmtList.keySet().iterator();
                String siNo = null;
                while (itr.hasNext()) {
                   siNo = (String)itr.next();
                   newTran.setSIAmtList(siNo, (BigDecimal)siAmtList.get(siNo));

                } 
               
                //newTran.setChangeAmount(oldTran.getChangeAmount().subtract(showTran.getSalesAmount()));
                newTran.setChangeAmount(oldTran.getChangeAmount().subtract(showTran.getSalesAmtWithItemDsct()));
                CreamToolkit.logMessage("store() in returnSummaryState exit() L132");
				newTran.store();
                
                //打印Payment
                CreamPrinter.getInstance().printPayment(newTran);
				return DrawerOpenState.class;
			}
		} else {
			if (event.getSource() instanceof ClearButton) {
				app.getPayingPane().setVisible(false);
				app.getItemList().setVisible(true);
				return ReturnShowState.class;
			} else if (event.getSource() instanceof EnterButton) {
				app.getMessageIndicator().setMessage(res.getString("DataTransfers"));
				//作废以前的交易
				cancelTran(oldTran);
                System.out.println("oldTran = " + oldTran.getTransactionNumber());
                //get old token payment detail  gllg
                Iterator it = TokenTranDtl.queryByTranNo(oldTran.getTransactionNumber().toString());

                //生成新的负向交易
				curTran = makeReturnTran(oldTran);
                //insert a negative token detail corresponding with old ones  gllg
                if (it != null) {
                    try {
                        while (it.hasNext()) {
                            TokenTranDtl ttd = (TokenTranDtl) it.next();
                            ttd.setTransactionNumber(curTran.getTransactionNumber().intValue());
                            //ttd.setSystemDate(curTran.getSystemDateTime());
                            ttd.setSaleQuantity(ttd.getSaleQuantity().negate());
                            ttd.setTokenQuantity(ttd.getTokenQuantity().negate());
                            ttd.setTokenAmount(ttd.getTokenAmount().negate());
                            ttd.insert();
                        }
                    } catch (Exception te) {
                        System.out.println("te = " + te);
                        te.printStackTrace(CreamToolkit.getLogger());
                    }

                }
                System.out.println("curTran = " + curTran.getTransactionNumber());
                app.setReturnQty(returnQty);
                CreamToolkit.logMessage("store() in returnSummaryState exit() L174");
				curTran.store();

				app.setIsAllReturn(true);
				CreamToolkit.showText(showTran, 1);

				return DrawerOpenState.class;
			}
		}
		return sinkState.getClass();
	}

	private void cancelTran(Transaction t) {
		t.setDealType1("*");
		t.setDealType2("0");
		t.setDealType3("0");
 		t.update();
	}

	/**
     * 产生退货交易，该交易为以前交易的负向交易
	 * @param t 把该交易转换成退货交易
	 * @return Transaction 退货交易(DealType 为 0,3,4)
	 */
    private Transaction makeReturnTran(Transaction t) {
        t.setDealType1("0");
		t.setDealType2("3");
		t.setDealType3("4");

		t.setTransactionNumber(Transaction.getNextTransactionNumber());
		t.setVoidTransactionNumber(new Integer(transactionID));
		t.setStoreNumber(CreamProperties.getInstance().getProperty("StoreNumber"));
		t.setSystemDateTime(new Date());
		t.setTerminalNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("TerminalNumber")));
		t.setTerminalPhysicalNumber(CreamProperties.getInstance().getProperty("TerminalPhysicalNumber"));
		t.setZSequenceNumber(new Integer(ZReport.getCurrentZNumber()));
		t.setSignOnNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("ShiftNumber")));
		t.setTransactionType("00");
		t.setCashierNumber(CreamProperties.getInstance().getProperty("CashierNumber"));
		t.setInvoiceID(CreamProperties.getInstance().getProperty("InvoiceID"));
		t.setInvoiceCount(1);
		t.setBuyerNumber("");

		t.setTotalMMAmount(
			t.getMixAndMatchAmount0()
                .add(t.getMixAndMatchAmount1())
				.add(t.getMixAndMatchAmount2())
				.add(t.getMixAndMatchAmount3())
				.add(t.getMixAndMatchAmount4()));
		t.makeNegativeValue();

		returnQty = new BigDecimal(0);
		Iterator itera = t.getLineItems();
		LineItem newLineItem = null;

		while (itera.hasNext()) {
			newLineItem = (LineItem) itera.next();

			newLineItem.setTerminalNumber(t.getTerminalNumber());
			newLineItem.setTransactionNumber(t.getTransactionNumber());

            if (newLineItem.getDetailCode().equalsIgnoreCase("M")
                || newLineItem.getDetailCode().equalsIgnoreCase("D")) {
                continue;         
            } else if (!(newLineItem.getDetailCode().equalsIgnoreCase("I")
                        || newLineItem.getDetailCode().equals("O"))) {
                newLineItem.setDetailCode("R");
            }
            
            //2003-06-16 Updated by zhaohong  如果是限价销售,实际销售数量减 1
			if (newLineItem.getDiscountType() != null && newLineItem.getDiscountType().equals("L")) {
				try {
					Client.getInstance().processCommand("queryLimitQtySold " + newLineItem.getItemNumber() + " -1 ");
				} catch (ClientCommandException e) {
				}
			}
			returnQty = returnQty.add(newLineItem.getQuantity());
		} // end while   

		return t;
	}


    //设置新交易的头信息
    private void setNewTranHead (Transaction t) {
        t.setDealType1("0");
        t.setDealType2("0");
        t.setDealType3("4");
        t.setStoreNumber(CreamProperties.getInstance().getProperty("StoreNumber"));
        t.setSystemDateTime(new Date());
        t.setTerminalNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("TerminalNumber")));
        t.setTransactionNumber(Transaction.getNextTransactionNumber());
        t.setSignOnNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("ShiftNumber")));
        t.setTransactionType("00");
        t.setTerminalPhysicalNumber(CreamProperties.getInstance().getProperty("TerminalPhysicalNumber"));
        t.setCashierNumber(CreamProperties.getInstance().getProperty("CashierNumber"));
        t.setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
        t.setInvoiceID(CreamProperties.getInstance().getProperty("InvoiceID"));
        t.setZSequenceNumber(new Integer(ZReport.getCurrentZNumber()));
        t.setInvoiceCount(1);
        t.setBuyerNumber("");
        t.setDetailCount(t.getDetailCount().intValue() - selectArray.size());

        t.setPayAmount1(oldTran.getPayAmount1());
        t.setPayAmount2(oldTran.getPayAmount2());
        t.setPayAmount3(oldTran.getPayAmount3());
        t.setPayAmount4(oldTran.getPayAmount4());
        
        t.setPayNumber1(oldTran.getPayNumber1());
        t.setPayNumber2(oldTran.getPayNumber2());
        t.setPayNumber3(oldTran.getPayNumber3());
        t.setPayNumber4(oldTran.getPayNumber4());

    }
    
    //设置并打印新交易的明细信息
	private void setNewTranItemAndPrint(Transaction t) {
		returnQty = new BigDecimal(0);

        //如果Transaction t 的currentLineItem != null  就把它加到lineItems 里面。
        try {
            t.addLineItem(null);
        } catch (Exception e) {
            
        }
        
        Object[] lines = t.getLineItemsArray();
        
        LineItem newLineItem = null;
		//trandetail 的 sequenceNumber
        int seq = 1;

        for (int i = 0; i < lines.length ; i ++) {
        
			//newLineItem = (LineItem) itor.next();
            newLineItem = (LineItem)lines[i];
			//    "S":销售
			//    "R":退货
			//    "D":SI折扣
			//    "T":ItemDiscount折扣
			//    "M":Mix & Match折扣
			//    "B":退瓶
			//    "I":代售
			//    "O":代收
			//    "Q":代付
			//    "V":指定更正
			//    "E":立即更正

            //去掉无效的行
            if (newLineItem.getRemoved()
                || !(newLineItem.getDetailCode().equals("S")
                    || newLineItem.getDetailCode().equals("R")
                    || newLineItem.getDetailCode().equals("I")
                    || newLineItem.getDetailCode().equals("O")
                    || newLineItem.getDetailCode().equals("M")
                    || newLineItem.getDetailCode().equals("D"))
                ) {
                try {
                    t.removeLineItem(newLineItem);  
                    continue;
                } catch (LineItemNotFoundException e) {
                    continue;                    
                }
            }
            

			newLineItem.setTerminalNumber(t.getTerminalNumber());
			newLineItem.setTransactionNumber(new Integer(Transaction.getNextTransactionNumber()));
			//改写明细序号
            newLineItem.setLineItemSequence(seq++);
//            newLineItem.setAfterSIAmount(newLineItem.getAmount());
			newLineItem.setPrinted(false);

	        if (newLineItem.getDetailCode().equalsIgnoreCase("M")
                || newLineItem.getDetailCode().equalsIgnoreCase("D")) {
                continue;         
            } else if (!(newLineItem.getDetailCode().equalsIgnoreCase("I")
                        || newLineItem.getDetailCode().equals("O"))) {
				newLineItem.setDetailCode("R");
			}

			//如果是限量销售，并且不被包含在所选择的退货明细中，把限销数量加上来
			if (newLineItem.getDiscountType() != null && newLineItem.getDiscountType().equals("L")) {
				try {
					Client.getInstance().processCommand(
						"queryLimitQtySold " + newLineItem.getItemNumber() + " " + newLineItem.getQuantity());
				} catch (ClientCommandException e) {
				}
			}
            //打印明细
			CreamPrinter.getInstance().printLineItem(newLineItem);
        } //end for
        t.initForAccum();
        t.accumulate();
	}

    

}
