package hyi.cream.state;

import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.CombPromotionHeader;
import hyi.cream.dac.CombSaleDetail;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.PLU;
import hyi.cream.dac.Payment;
import hyi.cream.dac.TokenPayment;
import hyi.cream.dac.TokenTranDtl;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.ClearButton;
import hyi.cream.uibeans.TicketButton;
import hyi.cream.uibeans.NumberButton;
import hyi.cream.uibeans.EnterButton;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jpos.JposException;
import jpos.Scanner;

public class ScanTicketState extends State {

    POSTerminalApplication app = POSTerminalApplication.getInstance();
    
    static public Map tokenNumberPluNumber = new HashMap();

    static public List payMentList = new ArrayList();//单笔交易中此种支付的list,下笔交易开始时要清空
    
    private Class exitState = null;

    private String number;//get scanner.number

    private String storeID = "";//店号
    
    private String posNumber = "";//机号

    static public int itemSeq = 0;//交易中抵用券序号,下笔交易开始时要归0
    
    static ScanTicketState scanneTicketState = null;

    public Map pluQtyMap = null; // 此交易的商品品号与数量的Map, 品号不重复，数量累加
    
    public Map tempPluQtyMap = null;

    static public BigDecimal amt = null;//支付的金额
    
    static private String payID = "";
    
    static private String payAmt = "";//支付的金额 （String)

    private String tmpNumber = "";
    
    public ScanTicketState() throws InstantiationException {
        init();
    }

    public static ScanTicketState getInstance() {
        try {
            if (scanneTicketState == null) {
                scanneTicketState = new ScanTicketState();
            }
        } catch (InstantiationException ex) {
        }
        return scanneTicketState;
    }

    public void entry(EventObject event, State sourceState) {

        if (sourceState instanceof SummaryState) {
            number = "";
            tmpNumber = "";
            payID = ((TicketButton) event.getSource()).getPaymentID();
            
            if (payMentList.size() == 0)
            mapClone(IdleState.getPluQtyMap());
            app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("ScanTicket"));
            exitState = SummaryState.class;
        }
        if (sourceState instanceof WarningState) {
            number = "";
            tmpNumber = "";
            app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("ScanTicket"));
            exitState = SummaryState.class;
        }
             
    }

    public Class exit(EventObject event, State sinkState) {
        Object eventSource = event.getSource();
        app.getWarningIndicator().setMessage("");

        if (eventSource instanceof jpos.Scanner) {
            try {
                ((Scanner) eventSource).setDecodeData(true);
                number = new String(((Scanner) eventSource).getScanDataLabel());
                if (match(number)){
                    exitState = Paying1State.class;
                } else {
                    exitState = WarningState.class;
                 //  setWarningMessage(CreamToolkit.GetResource().getString("cannotUse"));
                }

                setWarningMessage(CreamToolkit.GetResource().getString("cannotUse"));
            } catch (JposException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Jpos exception at " + this);
            }
        }

        if (eventSource instanceof NumberButton) {
            NumberButton nb = (NumberButton) event.getSource();
            tmpNumber = tmpNumber + nb.getNumberLabel();
            app.getMessageIndicator().setMessage(tmpNumber);
            return ScanTicketState.class;
        }

        if (eventSource instanceof EnterButton) {
            try {
                number = tmpNumber;
                if (match(number)){
                    exitState = Paying1State.class;
                } else {
                    exitState = WarningState.class;
                 //  setWarningMessage(CreamToolkit.GetResource().getString("cannotUse"));
                }

                setWarningMessage(CreamToolkit.GetResource().getString("cannotUse"));
            } catch (Exception e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("exception " + this);
            }
        }

                

        
        return exitState;
    }

    private boolean match(String ticketNoumber) {
        Iterator itr = TokenPayment.queryByInStoreIDTokenNo(ticketNoumber,
                storeID);
        if (itr == null)
            return false;
        if (pluQtyMap == null)
            return false;
        //itemSeq = 0;
        while (itr.hasNext()) {
            TokenPayment tp = (TokenPayment) itr.next();
            String pluCode = tp.getPluNumber();
            BigDecimal qty = tp.getMatchQuantity();
            if (pluCode != null && !pluCode.equals("") && qty != null) {        
                if (pluQtyMap.containsKey(pluCode)) {
                    int matchQty = qty.intValue();
                    int realQty = ((Integer) pluQtyMap.get(pluCode)).intValue();
                    if (realQty >= matchQty) {
                        pluQtyMap.put(pluCode, Integer.valueOf(String
                                .valueOf(realQty - matchQty)));
                        amt = tp.getTokenAmount() == null ? new BigDecimal("0")
                                : tp.getTokenAmount();
                        payAmt = amt.toString();
                        itemSeq += 1;
                        if (!tokenNumberPluNumber.containsKey(number+pluCode)) {
                            TokenTranDtl ttd = createTokTranDtl(pluCode);
                            payMentList.add(ttd);
                            tokenNumberPluNumber.put(number+pluCode,ttd);
                        } else {
                            TokenTranDtl ttd = (TokenTranDtl)tokenNumberPluNumber.get(number+pluCode);
                            ttd.setTokenQuantity(ttd.getTokenQuantity().add(new BigDecimal(1)));
                            ttd.setTokenAmount((ttd.getTokenAmount().add(new BigDecimal(payAmt))));
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public TokenTranDtl createTokTranDtl(String pluCode) {
        TokenTranDtl ttd = null;
            try {
                ttd = new TokenTranDtl();
                ttd.setStoreNumber(storeID);
                ttd.setPosNumber(Integer.parseInt(posNumber));
                ttd.setTransactionNumber(app.getCurrentTransaction().getTransactionNumber().intValue());
                //ttd.setSystemDate(new Date());
                ttd.setItemSequence(itemSeq);
                ttd.setTokenNumber(number);
                ttd.setPluNumber(pluCode);
                ttd.setTokenQuantity(new BigDecimal(1));
                ttd.setTokenAmount(new BigDecimal(payAmt));
                ttd.setStatus("0");
                ttd.setSaleQuantity(new BigDecimal(tempPluQtyMap.get(pluCode).toString()));
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        return ttd;
    }
    private void init() {
        storeID = CreamProperties.getInstance().getProperty("StoreNumber");
        posNumber = CreamProperties.getInstance().getProperty("TerminalNumber");
    }
    
    public static BigDecimal getAmt() {
        return amt;
    }
    
    
    static public String getPayID() {
        return payID;
    }
    
    static public String getPayAmt() {
        return payAmt;
    }
    
    static public List getPayMentList() {
        return payMentList;
    }
    
    static public void setItemSeq(int seq) {
        itemSeq = seq;
    }
    
    static public int getItemSeq() {
        return itemSeq;
    }
    
    public void saveTokenPayList() {
        for (int i = 0; i < payMentList.size(); i++) {
            TokenTranDtl ttd = (TokenTranDtl)payMentList.get(i);
            if (ttd != null) {
                CreamToolkit.logMessage(ttd.getStoreID());
                CreamToolkit.logMessage(ttd.getPosNumber()+"");
                CreamToolkit.logMessage(ttd.getTransactionNumber()+"");
                CreamToolkit.logMessage(ttd.getSystemDate()+"");
                CreamToolkit.logMessage(ttd.getItemSequence()+"");
                CreamToolkit.logMessage(ttd.getTokenAmount()+"");
                CreamToolkit.logMessage(ttd.getTokenNumber()+"");
                CreamToolkit.logMessage(ttd.getTokenQuantity()+"");
                CreamToolkit.logMessage(ttd.getStatus()+"");
                CreamToolkit.logMessage(ttd.getSaleQuantity()+"");
            }
            ttd.insert();
        }
        clearSome();
    }
    
    private void mapClone(Map source) {
        if (pluQtyMap == null)
            pluQtyMap = new HashMap();
        else
            pluQtyMap.clear();
        if (tempPluQtyMap == null)
            tempPluQtyMap = new HashMap();
        else
            tempPluQtyMap.clear();
        Iterator it = source.keySet().iterator();
        while (it.hasNext()) {
            String name = it.next().toString();
            Integer value = (Integer)source.get(name);
            pluQtyMap.put(name, value);
            tempPluQtyMap.put(name, value);
        }
    }
    
    public  void clearSome(){
        tokenNumberPluNumber.clear();
        payMentList.clear();
        app.getCurrentTransaction().payMentList.clear();
        itemSeq = 0;
    }
}
