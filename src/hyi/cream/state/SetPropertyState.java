// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.awt.Toolkit;
import java.io.IOException;
import java.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class SetPropertyState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance(); 
	private ResourceBundle res          = CreamToolkit.GetResource();
    private String numberString         = "";
    private String property             = "";
    private String text                 = "";
    
    //private static final String daiShouVersion =
    //    CreamProperties.getInstance().getProperty("DaiShouVersion") == null?
    //    "" : CreamProperties.getInstance().getProperty("DaiShouVersion"); 
    
    static SetPropertyState setPropertyState = null;

    public static SetPropertyState getInstance() {
        try {
            if (setPropertyState == null) {
                setPropertyState = new SetPropertyState();
            }
        } catch (InstantiationException ex) {
        }
        return setPropertyState;
    }

    /**
     * Constructor
     */
    public SetPropertyState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (event.getSource() instanceof SelectButton
            && sourceState instanceof ConfigState) {
            property = ((ConfigState)sourceState).getProperty();
            if (property.equalsIgnoreCase("ReceiptPrint")) {
                text = res.getString("SetProperty1");
            } else if (property.equalsIgnoreCase("CheckDrawerClose")) {
                text = res.getString("SetProperty1");
            } else if (property.equalsIgnoreCase("DrawerCloseTime")) {
                text = res.getString("SetProperty2");
            } else if (property.equalsIgnoreCase("ZPrint")) {   
                text = res.getString("SetProperty1");
            } else if (property.equalsIgnoreCase("ShiftPrint")) {
                text = res.getString("SetProperty1");
            } else if (property.equalsIgnoreCase("SetPOSNumber")) {
                text = res.getString("InputPOSNumber");
            } else if (property.equalsIgnoreCase("SetBeginningTransactionNumber")) {
                text = res.getString("InputBeginningTransactionNumber");
            } else if (property.equalsIgnoreCase("ClearAllTransaction")) {
                text = res.getString("SetProperty1");
            } else if (property.equalsIgnoreCase("SetRemoteBackupIP")) {
                text = res.getString("SetRemoteMachineIP");
            }
            numberString = "";
            app.getMessageIndicator().setMessage(text);
        }                       

        if (event.getSource() instanceof NumberButton
            && sourceState instanceof SetPropertyState) {
            numberString = numberString + ((NumberButton)event.getSource()).getNumberLabel();
            app.getMessageIndicator().setMessage(numberString);
        }

        if (event.getSource() instanceof ClearButton
            && sourceState instanceof SetPropertyState) {
            numberString = "";
            app.getMessageIndicator().setMessage(text);
        }

        if (event.getSource() instanceof EnterButton
            && sourceState instanceof SetPropertyState) {
            numberString = "";         
            app.getMessageIndicator().setMessage(text);
        }
    }

    public Class exit(EventObject event, State sinkState) { 

        if (event.getSource() instanceof EnterButton) {
            CreamProperties prop = CreamProperties.getInstance();
            
            if (numberString.equals("")) {
                return SetPropertyState.class;
            }

            if (!property.equalsIgnoreCase("SetRemoteBackupIP") && !CreamToolkit.checkInput(numberString, 0)) {
                numberString = "";
                return SetPropertyState.class;
            }

            //  set DrawerCloseTime property
            if (property.equalsIgnoreCase("DrawerCloseTime")) {
                prop.setProperty(property, numberString);
                prop.deposit();
                return ConfigState.class;

            //Bruce/20030318
            } else if (property.equalsIgnoreCase("SetPOSNumber")) {
                int posno;
                try {
                    posno = Integer.parseInt(numberString);
                    if (posno <= 0 || posno > 30)
                        return SetPropertyState.class;
                } catch (NumberFormatException e2) {
                    return SetPropertyState.class;
                }
                
                try {
                    Process p = Runtime.getRuntime().exec("/bin/sh /root/setposno.sh " + posno);
                    POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                        CreamToolkit.GetResource().getString("ReadyToRestart"));
                    p.waitFor();
                    DacTransfer.shutdown(1);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                return ConfigState.class;

            //Bruce/20030402
            } else if (property.equalsIgnoreCase("SetBeginningTransactionNumber")) {
                try {
                    int tranNo = Integer.parseInt(numberString);
                    if (tranNo > 0) {
                        prop.setProperty("NextTransactionNumber", tranNo + "");
                        prop.deposit();
                        POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                            CreamToolkit.GetResource().getString("ReadyToRestart"));
                        DacTransfer.shutdown(1);
                        return ConfigState.class;
                    } else {
                        return SetPropertyState.class;
                    }
                } catch (NumberFormatException e2) {
                    Toolkit.getDefaultToolkit().beep();
                    return SetPropertyState.class;
                }

            //Bruce/20030402
            } else if (property.equalsIgnoreCase("ClearAllTransaction")) {
                if (numberString.equals("1")) {
                    try {
                        new Transaction().deleteAll();
                        new LineItem().deleteAll();
                        new ZReport().deleteAll();
                        new ShiftReport().deleteAll();
                        new CashForm().deleteAll();
                        new DepSales().deleteAll();
                        //if (!daiShouVersion.equals("2")){
                        new DaishouSales().deleteAll();
                        new DaiShouSales2().deleteAll();
                        //}
                        new CategorySales().deleteAll();
                        prop.setProperty("NextTransactionNumber", "1");
                        prop.deposit();
                        POSTerminalApplication.getInstance().getWarningIndicator().setMessage(
                            CreamToolkit.GetResource().getString("ReadyToRestart"));
                        DacTransfer.shutdown(1);
                    } catch (InstantiationException e) {
                        Toolkit.getDefaultToolkit().beep();
                        e.printStackTrace();
                    }
                }
                return ConfigState.class;

			} else if (property.equalsIgnoreCase("SetRemoteBackupIP")) {
System.out.println("setIP");
                prop.setProperty("MySQLRemoteBackupDirectory", numberString + "::backup_path/");
                prop.deposit();

                return ConfigState.class;

                //  set about 'yes' or 'no' property
			} else {
				boolean returnConfig = false;
				if (numberString.equals("1")) {
                    prop.setProperty(property, "yes");
                    prop.deposit();
					returnConfig = true;
				} else if (numberString.equals("2")) {
                    prop.setProperty(property, "no");
                    prop.deposit();
					returnConfig = true;
				} else {
                    app.getWarningIndicator().setMessage(res.getString("InputAgain"));
                }
				//System.out.println(property + "|" + prop.getProperty(property));
				if (property.compareTo("ReceiptPrint") == 0) {
					boolean print = true;
					String value = prop.getProperty(property);
					if (value.compareTo("yes") == 0)
						print = true;
					else
						print = false;
					CreamPrinter.getInstance().setPrintEnabled(print);
				}
				if (returnConfig)
					return ConfigState.class;
				else {
                    return SetPropertyState.class;
                }
            }
        }

        if (event.getSource() instanceof ClearButton) {
            if (numberString.equals("")) {
                return ConfigState.class;
            } else {
                return SetPropertyState.class;
            }
		}

        return sinkState.getClass();
    }
}


