
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;
import java.math.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class SetQuantityState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    private String numberString         = "";
    private BigDecimal quantity         = new BigDecimal("0");
    private boolean warning             = false;
    static SetQuantityState setQuantityState = null;
    private LineItem curLineItem        = null;
    private boolean isDaishou           = false;

    public static SetQuantityState getInstance() {
        try {
            if (setQuantityState == null) {
                setQuantityState = new SetQuantityState();
            }
        } catch (InstantiationException ex) {
        }
        return setQuantityState;
    }

    /**
     * Constructor
     */
    public SetQuantityState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        curLineItem = app.getCurrentTransaction().getCurrentLineItem();
        if (curLineItem == null)
            return;
        if (curLineItem.getDetailCode().equals("I")) {
            isDaishou = true;
        } else {
            isDaishou = false;
        }
        BigDecimal pluMaxQuantity = new BigDecimal(CreamProperties.getInstance().getProperty("PluMaxQuantity"));
        Object eventSource = event.getSource();
        if (eventSource instanceof QuantityButton && sourceState instanceof NumberingState) {
            numberString = ((NumberingState)sourceState).getNumberString();
        }
        quantity = new BigDecimal(numberString);
        if (curLineItem == null || curLineItem.getPrinted() 
            || !(curLineItem.getDetailCode().equals("S") || curLineItem.getDetailCode().equals("I")) 
            || (curLineItem.getWeight() != null && curLineItem.getWeight().compareTo(new BigDecimal(0)) != 0)) { //Bruce/2003-08-14
            setWarningMessage(CreamToolkit.GetResource().getString("NumberWarning"));
            setWarning(true);
        } else if (curLineItem.getRemoved()){
            setWarningMessage(CreamToolkit.GetResource().getString("NumberWarning"));
            setWarning(true);
        }else if (quantity.intValue() <= 0) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("number1"));
            setWarning(true);
        } else if (quantity.compareTo(pluMaxQuantity) == 1) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("number2") + pluMaxQuantity);
            setWarning(true);
        } else if (curLineItem.getDiscountType() != null && curLineItem.getDiscountType().equalsIgnoreCase("L") && quantity.intValue() > 1) {
            //限量销售商品每次只能录入一单品
			app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("LimitQtyNumber"));
			setWarning(true);
        } else {
            quantity = quantity.setScale(2, 4);
            curLineItem.setQuantity(quantity);

            //calc pluQtyMap first
            IdleState.getInstance().calcPluQty();

            setWarning(false);
            CreamToolkit.showText(app.getCurrentTransaction(), 0);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (getWarning()) {
            return WarningState.class;
        } else {

            Transaction curTransaction = app.getCurrentTransaction();
            if (curTransaction == null || curLineItem == null)
                return IdleState.class;

            if (isDaishou) {
                curTransaction.setDaiShouAmount(curLineItem.getAmount());
                curLineItem.setAfterDiscountAmount(curLineItem.getAmount());

                try {
                    app.getCurrentTransaction().changeLineItem(-1, curLineItem);
                } catch (LineItemNotFoundException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("LineItem not found at " + this);
                }

                return IdleState.class;
            } else {
                
                if ("C".equals(curLineItem.getDiscountType())) {
                    curLineItem.setAfterDiscountAmount(
                        curLineItem.getAfterItemDsctUnitPrice().multiply(curLineItem.getQuantity())
                            .setScale(2, BigDecimal.ROUND_HALF_UP));
                } else {
                    //Bruce/20030324
                    curLineItem.setAfterDiscountAmount(curLineItem.getAmount());
                    curLineItem.setAfterSIAmount(curLineItem.getAmount());
                }

                //BigDecimal pluPrice = curLineItem.getAmount();
                BigDecimal pluPrice = curLineItem.getAfterDiscountAmount();

                TaxType taxType = TaxType.queryByTaxID(curLineItem.getTaxType());
                int round = 0;
                String taxRound = taxType.getRound();
                //System.out.println("***************************");
                //System.out.println("tax type = " + plu.getTaxType());
                //System.out.println("taxRound = " + taxRound);
                if (taxRound.equalsIgnoreCase("R")) {
                    round = BigDecimal.ROUND_HALF_UP;
                } else if (taxRound.equalsIgnoreCase("C")) {
                    round = BigDecimal.ROUND_UP;
                } else if (taxRound.equalsIgnoreCase("D")) {
                    round = BigDecimal.ROUND_DOWN;
                }
                String taxTp = taxType.getType();
                BigDecimal taxPercent = taxType.getPercent();
                BigDecimal taxAmount = null;
                int taxDigit = taxType.getDecimalDigit().intValue();
                if (taxTp.equalsIgnoreCase("1")) {
                    BigDecimal unit = new BigDecimal("1");
                    taxAmount = ((pluPrice.multiply(taxPercent)).divide(unit.add(taxPercent), 2)).setScale(taxDigit, round);
                } else if (taxTp.equalsIgnoreCase("2")) {
                    //BigDecimal unit = new BigDecimal("1");
                    taxAmount = (pluPrice.multiply(taxPercent)).setScale(taxDigit, round);
                } else {
                    BigDecimal tax = new BigDecimal("0.00");
                    taxAmount = tax.setScale(taxDigit, round);
                }
                curLineItem.setTaxAmount(taxAmount);
                //System.out.println("plu price = " + lineItem.getUnitPrice());
                //System.out.println("tax = " + lineItem.getTaxAmount());
                String fieldName = "TAXAMT" + (Integer.parseInt(curLineItem.getTaxType()) + 1);
                BigDecimal fieldValue = (BigDecimal)curTransaction.getFieldValue(fieldName);
                if (fieldValue != null) {
                    fieldValue = taxAmount.add(fieldValue);
                }
                curTransaction.setFieldValue(fieldName, fieldValue);
                curLineItem.setAddRebateAmount(curLineItem.getAfterDiscountAmount()
                    .multiply(curLineItem.getRebateRate()).setScale(1, BigDecimal.ROUND_HALF_UP));

                try {
                    app.getCurrentTransaction().changeLineItem(-1, curLineItem);
                } catch (LineItemNotFoundException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("LineItem not found at " + this);
                }

                return MixAndMatchState.class;
            }
        }
    }

    public void setWarning(boolean warning) {
        this.warning = warning;
    }

    public boolean getWarning() {
        return warning;
    }
}


