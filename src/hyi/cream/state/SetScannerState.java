// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;
import hyi.cream.uibeans.*;

import java.io.*;
import java.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class SetScannerState extends State implements PopupMenuListener {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
	private ResourceBundle res          = CreamToolkit.GetResource();
    private static ArrayList menu       = new ArrayList();
    private PopupMenuPane p             = app.getPopupMenuPane();
    private int selectItem              = 0;
    private static Properties pro       = new Properties();

    static {                    

        try {
            File f = new File(CreamToolkit.getConfigDir() + "scanner.conf");
            FileInputStream in = new FileInputStream(f);
            pro.load(in);
        } catch (FileNotFoundException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
        String line = pro.getProperty("ScannerList");

        StringTokenizer t = new StringTokenizer(line, ",");
        int i = 1;
        while (t.hasMoreElements()) {
            menu.add("0" + i + "." + t.nextToken());
            i++;
        }
    }

    static SetScannerState setScannerState = null;

    public static SetScannerState getInstance() {
        try {
            if (setScannerState == null) {
                setScannerState = new SetScannerState();
            }
        } catch (InstantiationException ex) {
        }
        return setScannerState;
    }

    /**
     * Constructor
     */
    public SetScannerState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("SetScannerState entry");
        if (event.getSource() instanceof SelectButton
            && sourceState instanceof ConfigState) {
            System.out.println("show pop");
            p.setMenu(menu);
            p.setVisible(true);
            p.show();
            p.addToList(this);
            p.clear();
            app.getMessageIndicator().setMessage(res.getString("SelectScannerType"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("SetScannerState exit");

        if (event.getSource() instanceof SelectButton) {
            pro.setProperty("ScannerType", ((String)menu.get(selectItem)).substring(3));

            String fileName = CreamToolkit.getConfigDir() + "scanner.conf";
            File propFile = new File(fileName);
            try {
                FileOutputStream outProp = new FileOutputStream(propFile);
                pro.store(outProp, "Scanner Configuration");
            } catch (IOException ie) {
                CreamToolkit.logMessage(ie + "!");
            }

            hyi.jpos.services.ARKScanner.ini();
            
            return ConfigState.class;
        }

        if (event.getSource() instanceof ClearButton) {
            return ConfigState.class;
		}

        return sinkState.getClass();
    }               

    public void doSomething() {
        if (p.getSelectedMode()) {
            selectItem = p.getSelectedNumber();
            POSButtonEvent e = new POSButtonEvent(new SelectButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        }
    }
}


