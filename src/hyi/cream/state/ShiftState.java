
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;
import hyi.cream.event.*;
import hyi.cream.inline.*;
import jpos.*;

import java.awt.Toolkit;
import java.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class ShiftState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static ShiftState shiftState = null;
    private	boolean success = true;
    private	boolean canDoShift = false;
    private ResourceBundle res = CreamToolkit.GetResource();

    public static ShiftState getInstance() {
        try {
            if (shiftState == null) {
                shiftState = new ShiftState();
            }
        } catch (InstantiationException ex) {
        }
        return shiftState;
    }

    /**
     * Constructor
     */
    public ShiftState() throws InstantiationException {
        if (shiftState == null)
            shiftState = this;
        else
            throw new InstantiationException(this + "");
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("ShiftState Enter");
        CreamToolkit.logMessage("ShiftState> Entering ShiftState...");
        canDoShift = false;
        ShiftReport shift = ShiftReport.getCurrentShift();
        Date initDate = CreamToolkit.getInitialDate();
        if (CreamProperties.getInstance().getProperty("CashierNumber").equals("")
            || shift == null) {
            POSButtonHome.getInstance().buttonPressed(new POSButtonEvent(new EnterButton(0,0,0,"")));
            CreamToolkit.logMessage("ShiftState> CashierNumber is empty");
            return;
        }
        if (shift != null) {
            if (shift.getAccountingDate().compareTo(initDate) != 0) {
                POSButtonHome.getInstance().buttonPressed(new POSButtonEvent(new EnterButton(0,0,0,"")));
                CreamToolkit.logMessage("ShiftState> No current ShiftReport");
                return;
            }
        }
        canDoShift = true;
        if (DacTransfer.getInstance().getStoZPrompt()) {
            POSButtonHome.getInstance().buttonPressed(new POSButtonEvent(new EnterButton(0,0,0,"")));
            CreamToolkit.logMessage("ShiftState> From Z");
            return;
        } else
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("ShiftConfirm"));
    }

    private void openDrawer() {
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        try {
            CashDrawer cashi = posHome.getCashDrawer();
            if (!posHome.getEventForwardEnabled())
                posHome.setEventForwardEnabled(true);
            cashi.setDeviceEnabled(true);
            cashi.openDrawer();
        } catch (NoSuchPOSDeviceException e) {
        } catch (JposException e) {
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (!canDoShift) {
            POSTerminalApplication.getInstance().getWarningIndicator().setMessage(res.getString("NoLoginWarning"));
            canDoShift = false;
            CreamToolkit.logMessage("ShiftState> Cannot do shift now");
            return hyi.cream.state.KeyLock1State.class;
        }
        ShiftReport shift = ShiftReport.getCurrentShift();
        if (event.getSource() instanceof EnterButton) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("shift"));
            if (shift.getUploadState().equals("2")) { //上次上传失败
                //success = DacTransfer.getInstance().uploadShiftReport(shift);
                CreamToolkit.logMessage("ShiftState> Start retransmitting shift");
                try {
                    Client.getInstance().processCommand("putShift " +
                        shift.getZSequenceNumber() + " " +
                        shift.getSequenceNumber());
                    success = true;
                } catch (ClientCommandException e) {
                    success = false;
                }
                if (success) {
                    shift.setUploadState("1");
                    shift.update();
                }
                return hyi.cream.state.KeyLock1State.class;
            }
            app.getCurrentTransaction().clear();
            app.getCurrentTransaction().init();
            app.getCurrentTransaction().setDealType2("9");
            CreamToolkit.logMessage("store() in ShiftState exit()");
            app.getCurrentTransaction().store();
            CreamToolkit.logMessage("ShiftState> Signoff record created.");

            // 现金清点单 only use when property "UseCashForm" is "yes"
            boolean useCashForm;
            String useCashFormStr = CreamProperties.getInstance().getProperty("UseCashForm");
            useCashForm = useCashFormStr != null && useCashFormStr.equalsIgnoreCase("yes");
            if (useCashForm) {
                openDrawer();
                SignOffForm p = SignOffForm.getInstance();
                p.setBounds();
                p.modify(
                	shift.getZSequenceNumber().intValue(),
                    shift.getSequenceNumber().intValue(), false);
            }

            //if (!DacTransfer.getInstance().isConnected()) {
            if (!Client.getInstance().isConnected()) {
                success = false;
                CreamToolkit.logMessage("ShiftState> Save ShiftReport/CashForm to floppy disk");

                // modify by lxf 2003.02.08
	            //  only use when property "FloppyContent" is "full"
	            boolean useSave;
	            String useSvaeStr = CreamProperties.getInstance().getProperty("FloppyContent");
	            useSave = useSvaeStr != null && useSvaeStr.equalsIgnoreCase("full");
                // 
                if (useSave) 
	                LineOffTransfer.getInstance().saveToDisk2(shift);
    			else            
	                LineOffTransfer.getInstance().saveToDisk(shift);
	                
                if (useCashForm) {
                    CashForm cf = CashForm.queryByZNumberAndShiftNumber(
                        shift.getZSequenceNumber().intValue(),
                        shift.getSequenceNumber().intValue());
                    if (cf != null)
		                LineOffTransfer.getInstance().saveToDisk2(cf);
                }
            }
            if (success) {
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("shiftEnd"));
                //return hyi.cream.state.DrawerOpenState3.class;
            } else {
                success = true;
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("ShiftNotUploadWarning"));
                CreamToolkit.logMessage("Shift upload failed.");
                //Bruce/2003-11-06
                if (CreamProperties.getInstance().getProperty("UseFloppyToSaveData", "no").equalsIgnoreCase("yes"))
                    return ShiftState2.class;
                try {
                    Toolkit.getDefaultToolkit().beep();
                    Toolkit.getDefaultToolkit().beep();
                    Toolkit.getDefaultToolkit().beep();
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                }
            }
            return hyi.cream.state.DrawerOpenState3.class;
        } else {
            CreamToolkit.logMessage("ShiftState> Exit state, not a EnterButton.");
            return sinkState.getClass();
        }
    }
}


