
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;
import hyi.cream.util.*;
import hyi.cream.inline.*;
import jpos.*;

import java.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class ShiftState2 extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    static ShiftState2 shiftState = null;
    private	boolean success = false;
    private ResourceBundle res = CreamToolkit.GetResource();


    public static ShiftState2 getInstance() {
        try {
            if (shiftState == null) {
                shiftState = new ShiftState2();
            }
        } catch (InstantiationException ex) {
        }
        return shiftState;
    }

    /**
     * Constructor
     */
    public ShiftState2() throws InstantiationException {
        if (shiftState == null)
            shiftState = this;
        else
            throw new InstantiationException(this + "");
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("ShiftState2 Enter");
        if (!success) {
            if (StateMachine.getFloppySaveError())
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("SaveFloppyErrorInsertAgain"));
            else
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("SaveFloppyReady"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("SaveFloppy"));
        if (event.getSource() instanceof ClearButton) {
            return hyi.cream.state.CancelShiftState.class;
        }
        if (!success) {
            //Bruce/20030114
            // 如果是在做Z的时候是因为还没做shift而先跑来这里的话,将shift存软盘后(in ShiftState)
            // 不需将硬盘中的档案删除（因为在ZState2的存软盘亦需将shift/cashform一并再次储存）。
            // 否则如果是单纯做shift的话，在存盘后就将serialized shift删除掉。
            success = LineOffTransfer.getInstance().saveToFloppyDisk(
                !DacTransfer.getInstance().getStoZPrompt());
        }
        StateMachine.setFloppySaveError(!success);
        //System.out.println("ShiftState2 Exit");
        if (success) {
            ShiftReport shift = ShiftReport.getCurrentShift();
            shift.setUploadState("1");
            shift.update();
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("shiftEnd"));
            success = false;
            return DrawerOpenState3.class;
        } else
           return ShiftState2.class;
    }
}


