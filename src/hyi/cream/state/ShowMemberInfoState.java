/*
 * Created on 2003-5-28
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

public class ShowMemberInfoState extends State implements PopupMenuListener {
    private Member member;
    static ShowMemberInfoState instance;
    private ArrayList menu = new ArrayList();
    private PopupMenuPane popup;
    
    public static ShowMemberInfoState getInstance() {
        try {
            if (instance == null) {
                instance = new ShowMemberInfoState();
            }
        } catch (InstantiationException ex) {
        }
        return instance;
    }

    public ShowMemberInfoState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        popup = POSTerminalApplication.getInstance().getPopupMenuPane();
        menu.clear();
        member = GetMemberNumberState.getMember();
        if (member != null) {
            menu.add("会员号: " + member.getID());
            menu.add("卡号: " + member.getMemberCardID());
            menu.add("姓名: " + member.getName());
            menu.add("电话: " + member.getTelephoneNumber());
            menu.add("身份证号: " + member.getIdentityID());
            menu.add("前日累计还元金: " + member.getMemberActionBonus());
            //menu.add("身份证号: ");
            //menu.add(" " + member.getIdentityID());
            //menu.add("");
            if (member.isNeedConfirm()) {
                menu.add("-> 查找不到此会员！请按：");
                menu.add("-> 1: 接受");
                menu.add("-> 2: 取消");
            } else { 
                menu.add("");
                menu.add("");
                menu.add("-> 按〔清除〕键关闭此窗口。");
            }
            popup.setMenu(menu);
            popup.setVisible(true);
            popup.show();
            popup.addToList(this);
            popup.clear();
        }
    }

    /**
     * 当在PopupMenuPane按数字键加回车，或者按了清除键了以后，都会callback此方法。
     */
    public void doSomething() {
        int selectIndex = popup.getSelectedNumber();
        if (member != null && member.isNeedConfirm()) {
            // selectIndex = 0 时表示选择 "1：接受"
            // selectIndex = 1 时表示选择 "2：取消"
            if (selectIndex == 1) {
                POSTerminalApplication.getInstance().getCurrentTransaction().setMemberID("");
            }
            if (selectIndex == 0 || selectIndex == 1) {
                POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
                POSButtonHome.getInstance().buttonPressed(e);
            } else {
                popup.setVisible(true);
                popup.show();
                popup.addToList(this);
                popup.clear();
            }
            return;
        } else {
            POSButtonEvent e = new POSButtonEvent(new ClearButton(0, 0, 0, ""));
            POSButtonHome.getInstance().buttonPressed(e);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        menu.clear();
        popup.setMenu(menu);
        popup.setVisible(false);
        return IdleState.class;
    }
}
