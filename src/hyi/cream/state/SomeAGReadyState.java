package hyi.cream.state;

import hyi.cream.*;
import java.util.*;

abstract public class SomeAGReadyState extends State {
    POSTerminalApplication app = POSTerminalApplication.getInstance();
    
    /**
     * Invoked when entering this state.
     *
     * <P>Just get a String from getPromptedMessage() and display it on the
     * warning indicator.
     *
     * @param event the triggered event object.
     * @param sourceState the ultimate source state
     */
    public void entry(EventObject event, State sourceState) {
		//System.out.println(this);
		app.getMessageIndicator().setMessage("");
        app.getWarningIndicator().setMessage(getPromptedMessage());
    }

    /**
     * Invoked when exiting this state.
     *
     * <P>Clear warningIndicator and return sinkState.getClass().
     *
	 * @param event the triggered event object.
     * @param sinkState the sink state
     */
	public Class exit(EventObject event, State sinkState) {
		app.getWarningIndicator().setMessage("");
        return sinkState.getClass();
    }

    /**
     * Get the prompted message which is used by entry().
     *
     * <P>This method should be implemented by derived class.
     *
     * @param event the triggered event object.
     * @param sinkState the sink state
     */
    abstract public String getPromptedMessage();
}

