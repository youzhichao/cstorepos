package hyi.cream.state;

//for event processing
import java.util.*;

public abstract class State {//The super class of all real state
    private String warningMessage = "";

    public abstract void entry (EventObject event, State sourceSate);

    public abstract Class exit(EventObject event, State sinkSate);

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }
}

