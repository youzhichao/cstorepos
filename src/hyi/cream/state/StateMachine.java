// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   StateMachine.java

package hyi.cream.state;

import hyi.cream.state.State;
import hyi.cream.NoSuchPOSDeviceException;
import hyi.cream.POSPeripheralHome;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.Cashier;
import hyi.cream.dac.ShiftReport;
import hyi.cream.dac.Transaction;
import hyi.cream.uibeans.PageDownButton;
import hyi.cream.uibeans.PageUpButton;
import hyi.cream.uibeans.PopupMenuPane;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import jpos.CashDrawer;
import jpos.JposException;
import jpos.Keylock;
import jpos.MSR;
import jpos.POSPrinter;
import jpos.Scanner;
import jpos.ToneIndicator;
import jpos.events.DataEvent;

// Referenced classes of package hyi.cream.state:
//            InitialState, LicenseNumberingState, State, KeyLock2State

public class StateMachine extends Thread {
    private Map dsCodeMap = new HashMap();
    private static final String COMMENT = "#";
    private static StateMachine instance = null;
    private static hyi.cream.state.State currentState = null;
    private static hyi.cream.state.State nextState = null;
    private static Hashtable allStateInstance = null;
    private static boolean floppySaveError;
    private POSTerminalApplication app;
    private CreamProperties prop;
    private boolean traceState;
    private ResourceBundle res;
    private Map stateChartMap;
    private String checkPrinterPower;
    private boolean suspended;
    private boolean eventProcessEnabled;
    private boolean keyWarning;
    private int turnable;
    private String warning;
    private int oldKey;
    private boolean paymentMenuVisible;

    public static boolean getFloppySaveError() {
        return floppySaveError;
    }

    public static void setFloppySaveError(boolean b) {
        floppySaveError = b;
    }

    public StateMachine() throws InstantiationException {
        app = POSTerminalApplication.getInstance();
        prop = CreamProperties.getInstance();
        res = CreamToolkit.GetResource();
        stateChartMap = new HashMap();
        checkPrinterPower = null;
        eventProcessEnabled = false;
        keyWarning = false;
        turnable = 0;
        warning = "";
        oldKey = 0;
        if (instance != null) {
            throw new InstantiationException(toString());
        } else {
            initStateMachine();
            instance = this;

            new Thread() {
                public void run() {
                    Robot robot;
                    try {
                        robot = new Robot();
                    } catch (AWTException e) {
                        e.printStackTrace();
                        return;
                    }
                    readDaishouCode();
//                    while (true) {
//                        try {
//                            sleep(5000);
//                            robot.mouseMove(20, 20);
//                            robot.mousePress(InputEvent.BUTTON1_MASK);
//                            robot.mouseRelease(InputEvent.BUTTON1_MASK);
//                        } catch (InterruptedException e) {
//                        }
//                    }
                }
            }.start();

            start();
            return;
        }
    }

    private void initStateMachine() {
        CreamProperties prop = CreamProperties.getInstance();
        checkPrinterPower = prop.getProperty("CheckPrinterPower");
        if (checkPrinterPower == null)
            checkPrinterPower = "yes";
        constructStateChart();
        String traceStateStr = prop.getProperty("TraceState", "no");
        if (traceStateStr != null && traceStateStr.equalsIgnoreCase("yes"))
            traceState = true;
        currentState = InitialState.getInstance();
        allStateInstance = new Hashtable();
        allStateInstance.put(currentState.getClass().getName(), currentState);
//        try {
//            Class.forName("Protector");
//            if (!Protector.checkUserLicense()) {
//                currentState = LicenseNumberingState.getInstance();
//                currentState.entry(null, null);
//                app.setBeginState(false);
//                return;
//            }
//        } catch (Exception exception) {
//        }
        currentState.entry(null, null); // entry InitialState
        if (!isTriggerless(currentState.getClass().getName()))
            return;
        Class sink = currentState.exit(null, null);// exit InitialState if triggerless
        if (allStateInstance.containsKey(sink.getName()))
            nextState = (hyi.cream.state.State) allStateInstance.get(sink.getName());
        else
            try {
                nextState = (hyi.cream.state.State) Class.forName(sink.getName()).newInstance();
                allStateInstance.put(sink.getName(), nextState);
            } catch (ClassNotFoundException ce) {
                CreamToolkit.logMessage(ce + "!");
                return;
            } catch (IllegalAccessException ie) {
                CreamToolkit.logMessage(ie + "!");
                return;
            } catch (InstantiationException ne) {
                CreamToolkit.logMessage(ne + "!");
                return;
            }
        nextState.entry(null, currentState);// entry sink state
        currentState = nextState;
    }

    void checkPrinterStatus() {
        Runnable check = new Runnable() {

            public void run() {
                POSPeripheralHome posHome = POSPeripheralHome.getInstance();
                POSPrinter printer = null;
                String originalMsg = app.getWarningIndicator().getMessage();
                try {
                    printer = posHome.getPOSPrinter();
                } catch (NoSuchPOSDeviceException e) {
                    CreamToolkit.logMessage(e.getMessage());
                }
                try {
                    if (!printer.getClaimed())
                        printer.claim(1000);
                    if (!printer.getDeviceEnabled())
                        printer.setDeviceEnabled(true);
                    if (checkPrinterPower.equals("yes"))
                        do {
                            if (printer.getPowerState() != 2001) {
                                app.getWarningIndicator().setMessage(res.getString("PrinterDetectError"));
                                java.awt.Toolkit.getDefaultToolkit().beep();
                            } else {
                                app.getWarningIndicator().setMessage(res.getString("WaitForPrinterDetection"));
                                try {
                                    Thread.sleep(2500L);
                                } catch (InterruptedException ie) {
                                    ie.printStackTrace();
                                }
                                synchronized (StateMachine.getInstance()) {
                                    StateMachine.getInstance().notifyAll();
                                }
                                app.getWarningIndicator().setMessage(originalMsg);
                                return;
                            }
                            try {
                                Thread.sleep(1000L);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        } while (true);
                } catch (JposException e) {
                    CreamToolkit.logMessage(e.toString());
                }
            }

        };
        (new Thread(check)).start();
    }

    public hyi.cream.state.State getStateInstance(String nextClassName) {
        if (nextClassName == null) {
            System.out.println("StateMachine> getStateInstance(null).");
            return null;
        }
        if (allStateInstance.containsKey(nextClassName))
            return (hyi.cream.state.State) allStateInstance.get(nextClassName);
        try {
        	hyi.cream.state.State ns = (hyi.cream.state.State) Class.forName(nextClassName).newInstance();
            allStateInstance.put(nextClassName, ns);
            return ns;
        } catch (ClassNotFoundException e) {
            CreamToolkit.logMessage(e.getMessage());
        } catch (IllegalAccessException e) {
            CreamToolkit.logMessage(e.getMessage());
        } catch (InstantiationException e) {
            CreamToolkit.logMessage(e.getMessage());
        }
        return null;
    }

    public void setEventProcessEnabled(boolean eventProcessEnabled) {
        this.eventProcessEnabled = eventProcessEnabled;
    }

    public boolean getEventProcessEnabled() {
        return eventProcessEnabled;
    }

    /**
     * Construct state chart from a text file.
     * 
     * <P>Data structure of stateChartMap:
     * <PRE>
     *   Key                     Value
     * -------------  -----------------------------------------------------------------
     * source state   ArrayList object contains: Object[3]
     * class name     +----------+-----------------------+----------------------------+
     *                | CondType | Event Source Classes  |      Sink State Class      |
     *                +----------+-----------------------+----------------------------+
     *                |"|" or "&"| "", class name or     | "" or class name           |
     *                | or null  | Object[]              |                            |
     *                +----------+-----------------------+----------------------------+
     *                |          |                       |                            |
     *                +----------+-----------------------+----------------------------+
     *                |          |                       |                            |
     *                +----------+-----------------------+----------------------------+
     *                |  ...     |      ...              |     ...                    |
     * 
     *                 CondType:
     *                      "|" is OR-style, like "xxxx | xxxx | xxxxx"
     *                      "&" is AND-style, like "!xxxx & !xxxx & !xxxxx"
     *                      null is normal style
     * </PRE>
     */
    
    public void constructStateChart() {
        try {
            java.io.File stateChartFile = CreamToolkit.getConfigurationFile(hyi.cream.state.StateMachine.class);
            LineNumberReader chartDefFile = new LineNumberReader(new FileReader(stateChartFile));
            String line;
            while ((line = chartDefFile.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith(COMMENT)) { // ignore lines start with '#'
                    // Format of line:
                    // [source state class name], [event class name], [event source class name], [sink state class name]
                    StringTokenizer tokener = new StringTokenizer(line, ",", true);
                    try {
                        String sourceStateClassName = tokener.nextToken().trim();
                        if (!sourceStateClassName.equals(",")) {// ignore the lines without source state
                            tokener.nextToken();                // ","
                            String eventClassName = tokener.nextToken().trim();
                            if (eventClassName.equals(","))
                                eventClassName = "";
                            else
                                tokener.nextToken();            // ","
                            String eventSourceClassName = tokener.nextToken().trim();
                            if (eventSourceClassName.equals(","))
                                eventSourceClassName = "";
                            else
                                tokener.nextToken();            // ","
                            String sinkStateClassName;
                            try {
                                sinkStateClassName = tokener.nextToken().trim();
                            } catch (NoSuchElementException e) {
                                sinkStateClassName = "";        // lack of last element
                            }
                            // check the invalid syntax
                            if (eventSourceClassName.indexOf("|") != -1 && eventSourceClassName.indexOf("&") != -1)
                                throw new NoSuchElementException();// xxx | xxx & xxx
                            if (eventSourceClassName.indexOf("|") != -1 && eventSourceClassName.indexOf("!") != -1)
                                throw new NoSuchElementException();// xxx | !xxx
                            if (eventSourceClassName.indexOf("&") != -1 && eventSourceClassName.indexOf("!") == -1)
                                throw new NoSuchElementException();// xxx & xxx
                                                                   // what if "xxx & !xxx" ? Look "THERE"
                            // condType
                            String condType = eventSourceClassName.indexOf("|") == -1 ? eventSourceClassName.indexOf("&") == -1 ? eventSourceClassName.indexOf("!") == -1 ? null : "&" : "&" : "|";
                            // event source classes
                            StringTokenizer tokener2 = new StringTokenizer(eventSourceClassName, "&|");
                            List eventSourceClasses = new ArrayList();
                            String eventStateClassName2;
                            for (; tokener2.hasMoreTokens(); eventSourceClasses.add(eventStateClassName2)) {
                                eventStateClassName2 = tokener2.nextToken().trim();
                                if (!eventStateClassName2.startsWith("!") && condType != null && condType.equals("&"))
                                    throw new NoSuchElementException();
                                if (eventStateClassName2.startsWith("!"))
                                    eventStateClassName2 = eventStateClassName2.substring(1);
                            }

                            // retrieve original value
                            Object value = stateChartMap.get(sourceStateClassName);
                            List valueList = ((List) (value == null ? ((List) (new ArrayList())) : (List) value));
                            // construct new value
                            valueList.add(((Object) (new Object[] { condType, eventSourceClasses.size() != 0 ? eventSourceClasses.size() != 1 ? eventSourceClasses.toArray() : eventSourceClasses.get(0) : "", sinkStateClassName })));
                            // put into stateChartMap
                            stateChartMap.put(sourceStateClassName, valueList);
                        }
                    } catch (NoSuchElementException e) {
                        CreamToolkit.logMessage("Statechart format error at line " + chartDefFile.getLineNumber());
                    }
                }
            }
            chartDefFile.close();
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    /**
     * Determine the sink state class from a source state and an event source class.
     * 
     * @return Three cases:
     *      <P>1. "" if sink state is determined by source state's exit().
     *      <P>2. null if this event will not make any state change
     *      <P>3. otherwise return the sink state class name
     */
    private String getSinkState(String sourceStateClassName, String eventSourceClassName) {
        // assertion
        if (sourceStateClassName == null || eventSourceClassName == null)
            return null;
        List value = (List) stateChartMap.get(sourceStateClassName);
        if (value == null)
            return null;
        Iterator iter = value.iterator();
        label0: while (iter.hasNext()) {
            Object entry[] = (Object[]) iter.next();
            String condType = (String) entry[0];
            Object eventSourceClasses0 = entry[1];
            String sinkClassName = (String) entry[2];
            if (eventSourceClasses0.equals(eventSourceClassName))
                return sinkClassName;
            if (condType != null && condType.equals("|")) {
                Object eventSourceClasses[] = (Object[]) eventSourceClasses0;
                for (int i = 0; i < eventSourceClasses.length; i++)
                    if (eventSourceClasses[i].equals(eventSourceClassName))
                        return sinkClassName;

                continue;
            }
            if (condType == null || !condType.equals("&"))
                continue;
            if (eventSourceClasses0 instanceof Object[]) {
                Object eventSourceClasses[] = (Object[]) eventSourceClasses0;
                for (int i = 0; i < eventSourceClasses.length; i++)
                    if (eventSourceClasses[i].equals(eventSourceClassName))
                        continue label0;

            } else if (!eventSourceClasses0.equals(eventSourceClassName))
                return sinkClassName;
            return sinkClassName;
        }
        return null;
    }

    public boolean isTriggerless(String stateClassName) {
        List value = (List) stateChartMap.get(stateClassName);
        if (value == null)
            return false;
        for (Iterator iter = value.iterator(); iter.hasNext();) {
            Object entry[] = (Object[]) iter.next();
            if (entry[1].equals(""))     // if contains a empty event source class entry
                return true;
        }

        return false;
    }

    public static StateMachine getInstance() {
        try {
            if (instance == null)
                instance = new StateMachine();
        } catch (InstantiationException ie) {
            ie.printStackTrace(CreamToolkit.getLogger());
            return instance;
        }
        return instance;
    }

    public Class getCurrentState() {
        return currentState.getClass();
    }

    public boolean getKeyWarning() {
        return keyWarning;
    }

    public void setKeyWarning(boolean keyWarning) {
        this.keyWarning = keyWarning;
    }

    public void setTurnable(int turnable) {
        this.turnable = turnable;
    }

    public boolean getTurnable() {
        if (turnable != 0) {
            if (turnable == 1)
                return true;
            if (turnable == -1)
                return false;
        }
        if (app.getTrainingMode() && !getKeyWarning())
            //if (!app.getScanCashierNumber()
            //    || (app.getScanCashierNumber()
            //        && app.getChecked())) {
            return true;
        if (getCurrentState().equals(hyi.cream.state.IdleState.class) && PopupMenuPane.getInstance().isShowing()) {
            if (!app.getTrainingMode())
                return false;
        } else if (getCurrentState().equals(hyi.cream.state.CashierRightsCheckState.class)) {
            String tmp = CreamProperties.getInstance().getProperty("TurnKeyAtOverrideAmount");
            boolean needTurnKey = tmp != null && tmp.equalsIgnoreCase("yes");
            if (needTurnKey)
                return true;
        }
        /*if (getCurrentState().equals(OpenPriceState.class)
            || getCurrentState().equals(DaiFuOpenPriceState.class)
            || getCurrentState().equals(PaidInOpenPriceState.class)
            || getCurrentState().equals(PaidOutOpenPriceState.class)
            || getCurrentState().equals(PriceLookupState.class)) {
            return false;
        }*/
        return (getCurrentState().equals(hyi.cream.state.IdleState.class) || getCurrentState().equals(hyi.cream.state.KeyLock1State.class) || getCurrentState().equals(hyi.cream.state.ConfigState.class) || getCurrentState().equals(hyi.cream.state.KeyLock2State.class) || getCurrentState().equals(hyi.cream.state.KeyLock3State.class) || getCurrentState().equals(hyi.cream.state.CheckKeyLockState.class) || getCurrentState().equals(hyi.cream.state.CashierState.class)) && ((app.getTransactionEnd() || app.getCurrentTransaction().getDisplayedLineItemsArray().size() == 0) && !getKeyWarning() && (!app.getScanCashierNumber() || app.getScanCashierNumber() && app.getChecked()));
        /*if (getCurrentState().equals(IdleState.class)) {
        if (app.getKeyPosition() == 1) {
            if (!app.getScanCashierNumber()
                || (app.getScanCashierNumber()
                    && app.getChecked())) {
                return true;
            }
        }
    }*/

        /* ((((getCurrentState().equals(IdleState.class)
            || getCurrentState().equals(KeyLock1State.class)
            || getCurrentState().equals(ConfigState.class)
            || getCurrentState().equals(KeyLock2State.class)
            || getCurrentState().equals(KeyLock3State.class)
            || getCurrentState().equals(CheckKeyLockState.class)
            || getCurrentState().equals(CashierState.class))
            && (app.getTransactionEnd()
               || app.getCurrentTransaction().getCurrentLineItem() == null)
            && !getKeyWarning())
            || app.getKeyPosition() == 1)
             && (!app.getScanCashierNumber()
                 || (app.getScanCashierNumber()
                     && app.getChecked()))) {
             return true;
         } else if ((app.getTrainingMode()
             && !getKeyWarning())
             && (!app.getScanCashierNumber()
             || (app.getScanCashierNumber()
                 && app.getChecked()))) {
             return true;
         } else {
             return false;
         }*/
    }

    private LinkedList eventQueue = new LinkedList();

    /**
     * Event handling thread.
     */
    public void run() {
        EventObject event;
        for (;;) {
            synchronized (eventQueue) {
                if (eventQueue.size() == 0) {
                    try {                        eventQueue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                event = (EventObject) eventQueue.removeFirst();
            }
            try {
                consumeEvent(event);
                // clearEvent(); // stop too fast event
            } catch (Exception e) {
                CreamToolkit.logMessage(e);
            }
        }
    }

    public void processEvent(EventObject event) {
        synchronized (eventQueue) {
            // logger.debug("eventQueue.add()");
            // if (DataUpdateMonitor.getInstance().isDownload())
            // {
            // logger.debug("主檔更新中不可操作");
            // }
            eventQueue.add(event);
            eventQueue.notifyAll();
        }
    }
    
    public void consumeEvent(EventObject event) {
    	System.out.println("=============== buttonname ==============" + new SimpleDateFormat("yyyy/MM/dd HH:mm:sss ").format(new java.util.Date())
    			+ event.getSource().getClass().getName());
        if (!getEventProcessEnabled())
            return;
        if (app.getBeginState())
            if (event.getSource() instanceof Keylock)
                try {
                    if (((Keylock) event.getSource()).getKeyPosition() == 1) {
                        app.setBeginState(false);
                        app.setKeyPosition(1);
                        //Bruce/20030324
                        String keylockMessage = CreamProperties.getInstance().getProperty("postype");
                        if (keylockMessage != null)
                            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("TurnKeyContinue(" + keylockMessage + ")"));
                        else
                            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("TurnKeyContinue(ibm)"));
                        return;
                    } else {
                        return;
                    }
                } catch (JposException e) {
                    System.out.println(e);
                }
            else
                return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        if (app.getKeyPosition() == 1 && !(event.getSource() instanceof Keylock))
            return;
        if (getKeyWarning() && !(event.getSource() instanceof Keylock)) {
            app.getWarningIndicator().setMessage(this.res.getString("TurnKeyLock"));
            if (!(event.getSource() instanceof CashDrawer))
                return;
        }
        if (PopupMenuPane.getInstance().isShowing() && (event.getSource() instanceof Scanner) && !getCurrentState().equals(hyi.cream.state.KeyLock1State.class) && !getCurrentState().equals(hyi.cream.state.KeyLock2State.class) && !getCurrentState().equals(hyi.cream.state.KeyLock3State.class))
            return;
        // check key lock
        if (event.getSource() instanceof Keylock) {
            if (getKeyWarning()) {
                try {
                    if (((Keylock) event.getSource()).getKeyPosition() == app.getKeyPosition()) {
                        app.getWarningIndicator().setMessage(warning);
                        setKeyWarning(false);
                        setTurnable(0);
                        ToneIndicator tone = posHome.getToneIndicator();
                        tone.clearOutput();
                    }
                } catch (JposException je) {
                    System.out.println(je);
                } catch (NoSuchPOSDeviceException e) {
                    System.out.println(e);
                }
                return;
            }
            try {
                if (((Keylock) event.getSource()).getKeyPosition() == 1) {
                    oldKey = app.getKeyPosition();
                    app.setKeyPosition(1);
                    PopupMenuPane p = app.getPopupMenuPane();
                    ArrayList menu = new ArrayList();
                    ResourceBundle res = CreamToolkit.GetResource();
                    menu.add("");
                    menu.add("");
                    menu.add("");
                    menu.add("");
                    menu.add(res.getString("SystemSuspended"));
                    p.setMenu(menu);
                    p.setVisible(true);
                    p.addToList(this);
                    p.setInputEnabled(false);
                    setSuspended(true);
                    //app.getItemList().setVisible(true);
                    app.getWarningIndicator().setMessage(res.getString("LockState"));
                    app.getMessageIndicator().setMessage("");
                    return;
                }
            } catch (JposException e) {
                System.out.println(e);
            }
            app.getPopupMenuPane().setInputEnabled(true);
            setSuspended(false);
            try {
                //if (((Keylock)event.getSource()).getKeyPosition() == 5) {
                //    this.setKeyWarning(true);
                //    this.setTurnable(0);
                //}
                if (((Keylock) event.getSource()).getKeyPosition() == 6)
                    //if (oldKey == 2) {
                    if (app.getTransactionEnd() || app.getCurrentTransaction().getDisplayedLineItemsArray().size() == 0) {
                        if (!app.getScanCashierNumber() || app.getScanCashierNumber() && app.getChecked())
                            setTurnable(1);
                        else
                            setTurnable(-1);
                    } else {
                        setTurnable(-1);
                    }
                if (((Keylock) event.getSource()).getKeyPosition() == 2 && app.getKeyPosition() == 1) {
                    app.getPopupMenuPane().setVisible(false);
                    if (oldKey == 2)
                        setTurnable(1);
                    else if (oldKey == 6) {
                        if (getCurrentState().equals(hyi.cream.state.ConfigState.class))
                            setTurnable(1);
                        else
                            setTurnable(-1);
                    } else {
                        setTurnable(1);
                    }
                    //} else if (oldKey == 6) {
                    //    setTurnable(1);
                    //} else {
                    //    setTurnable(-1);
                    //}
                }
            } catch (JposException e) {
                System.out.println(e);
            }
            if (!getTurnable()) {
                String ignoreKeylockWithinTransaction = prop.getProperty("IgnoreKeylockWithinTransaction", "no");
                if (ignoreKeylockWithinTransaction.equalsIgnoreCase("yes") && getCurrentState().equals(hyi.cream.state.IdleState.class))
                    //&& !app.getTransactionEnd()
                    //&& app.getCurrentTransaction().getDisplayedLineItemsArray().size() != 0) {
                    return;
                warning = app.getWarningIndicator().getMessage();
                app.getWarningIndicator().setMessage(this.res.getString("TurnKeyLock"));
                try {
                    ToneIndicator tone = posHome.getToneIndicator();
                    if (!tone.getDeviceEnabled())
                        tone.setDeviceEnabled(true);
                    tone.setAsyncMode(true);
                    tone.sound(0x1869f, 500);
                } catch (NoSuchPOSDeviceException ne) {
                    CreamToolkit.logMessage(ne + "!!");
                } catch (JposException je) {
                    System.out.println(je);
                }
                setKeyWarning(true);
                return;
            }
            try {
                if (app.getKeyPosition() == 1)
                    if (((Keylock) event.getSource()).getKeyPosition() == 6)
                        app.setKeyPosition(6);
                    else
                        ((Keylock) event.getSource()).getKeyPosition();
            } catch (JposException e) {
                System.out.println(e);
            }
            if (app.getTrainingMode()) {
                Transaction trans = app.getCurrentTransaction();
                trans.Clear();
                trans.setDealType2("0");
                trans.setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
                app.getPopupMenuPane().setInputEnabled(true);
                app.getItemList().setItemIndex(0);
                app.getItemList().setTransaction(app.getCurrentTransaction());
                currentState = KeyLock2State.getInstance();
                app.getWarningIndicator().setMessage("");
                app.setTraningMode(false);
                app.setChecked(true);
                try {
                    ToneIndicator tone = posHome.getToneIndicator();
                    tone.clearOutput();
                } catch (JposException je) {
                    System.out.println(je);
                } catch (NoSuchPOSDeviceException e) {
                    System.out.println(e);
                }
            }
        }
        //  check scan cashier card
        if (app.getScanCashierNumber()) {
            if (!app.getChecked()) {
                //try {
                //    if (((Scanner)event.getSource()).getScanDataType() != ScannerConst.SCAN_SDT_Code39) {
                //        app.getWarningIndicator().setMessage(res.getString("InputWrong"));
                //        return;
                //    }
                //} catch (JposException e) {
                //    System.out.println(e);
                //}
                if (event.getSource() instanceof Scanner) {
                    String id = "";
                    try {
                        id = new String(((Scanner) event.getSource()).getScanDataLabel());
                        //Bruce/2003-06-26/
                        // Modify for CStore. Ref. ARKScanner class.
                        if (id.length() > 7)
                            id = id.substring(0, 7);
                    } catch (JposException e) {
                        System.out.println(e);
                    }
                    Cashier c = Cashier.queryByCashierID(id);
                    if (c == null) {
                        app.getWarningIndicator().setMessage(this.res.getString("CashierNumberWrong"));
                    } else {
                        if (!app.getTrainingMode()) {
                            app.getCurrentTransaction().setSalesman(id);
                            app.getSystemInfo().setSalesManNumber(id);
                        }
                        app.setChecked(true);
                        if (app.getKeyPosition() == 1)
                            app.getMessageIndicator().setMessage(this.res.getString("CashierNumberRight"));
                        else if (app.getKeyPosition() == 2 || app.getTrainingMode()) {
                            if (CreamProperties.getInstance().getProperty("CashierNumber").equals("") || ShiftReport.getCurrentShift() == null) {
                                if (!app.getTrainingMode())
                                    app.getMessageIndicator().setMessage(this.res.getString("InputCashierNumber"));
                            } else {
                                app.getMessageIndicator().setMessage(this.res.getString("SelectPlu"));
                            }
                        } else if (app.getKeyPosition() == 3 && !app.getTrainingMode() || app.getKeyPosition() == 4 || app.getKeyPosition() == 5)
                            app.getMessageIndicator().setMessage(this.res.getString("InputSelect"));
                        app.getWarningIndicator().setMessage("");
                    }
                    return;
                }
                if (event.getSource() instanceof Keylock) {
                    int keyp = 0;
                    try {
                        keyp = ((Keylock) event.getSource()).getKeyPosition();
                    } catch (JposException e) {
                        System.out.println(e);
                    }
                    if (app.getTrainingMode()) {
                        app.setTraningMode(false);
                        app.setChecked(true);
                    } else if (app.getKeyPosition() == 1 && keyp == 2) {
                        app.setKeyPosition(2);
                    } else {
                        app.getWarningIndicator().setMessage(this.res.getString("TurnKeyLock"));
                        return;
                    }
                } else if (!(event.getSource() instanceof CashDrawer))
                    return;
            } else if (event.getSource() instanceof Keylock)
                try {
                    if (((Keylock) event.getSource()).getKeyPosition() == 2)
                        app.setKeyPosition(2);
                } catch (JposException e) {
                    System.out.println(e);
                }
        } else if (event.getSource() instanceof Keylock)
            try {
                if (((Keylock) event.getSource()).getKeyPosition() == 2)
                    app.setKeyPosition(2);
            } catch (JposException e) {
                System.out.println(e);
            }
        if ((event instanceof DataEvent) && (event.getSource() instanceof MSR)) {
            MSR msr = (MSR) event.getSource();
            try {
                msr.setDeviceEnabled(true);
            } catch (JposException jposexception) {
            }
        }
        //Bruce/20030323/ Refactoring...
        do {
            String eventSourceName;
            if (event == null)
                eventSourceName = "";
            else if (event.getSource() instanceof Keylock)
                eventSourceName = "jpos.Keylock";
            else
                eventSourceName = event.getSource().getClass().getName();
            // sinkStateByStatechart has three cases:
            //   1. "" if sink state is determined by source state's exit().
            //   2. null if this event will not make any state change
            //   3. otherwise return the sink state class name
            String sinkStateByStatechart = getSinkState(currentState.getClass().getName(), eventSourceName);
            if (sinkStateByStatechart == null) {
                if (currentState.getClass().getName().endsWith("IdleState") && !(event.getSource() instanceof PageUpButton) && !(event.getSource() instanceof PageDownButton))
                    sinkStateByStatechart = "hyi.cream.state.WarningState";
                else
                    return;    // if cannot make state change
            } else if (event != null && event.getClass().getName().endsWith("POSButtonEvent") && event.getSource().getClass().getName().endsWith("ClearButton"))
                try {
                    Scanner scanner = POSPeripheralHome.getInstance().getScanner();
                    try {
                        if (!scanner.getDataEventEnabled())
                            scanner.setDataEventEnabled(true);
                        scanner.clearInput();
                    } catch (JposException je) {
                    }
                } catch (NoSuchPOSDeviceException ne) {
                    ne.printStackTrace(CreamToolkit.getLogger());
                }
                // determine the parameter "sinkState" passing to currentState.exit()
                hyi.cream.state.State sinkState;
            if (sinkStateByStatechart.equals(""))
                sinkState = null;
            else
                sinkState = getStateInstance(sinkStateByStatechart);
            if (traceState)
                System.out.print(new SimpleDateFormat("yyyy/MM/dd HH:mm:sss ").format(new java.util.Date())+" State>> -> " + currentState.getClass().getName() + ".exit()!");
            Class sinkStateClassReturnFromExit = currentState.exit(event, sinkState);
            String sinkStateReturnFromExit;
            if (sinkStateClassReturnFromExit == null)
                sinkStateReturnFromExit = null;
            else
                sinkStateReturnFromExit = sinkStateClassReturnFromExit.getName();
            if (traceState)
                System.out.println(" <Ret>");
            // Determine the sink state:
            // if statechart doc does not specifies the sink state, use the return value of exit()
            if (sinkStateByStatechart.equals(""))
                sinkState = getStateInstance(sinkStateReturnFromExit);
            if (sinkState == null) {
                CreamToolkit.logMessage("Err> Cannot get State: " + sinkStateReturnFromExit);
                return;
            }
            // entry() the sink state.
            if (traceState)
                System.out.print(new SimpleDateFormat("yyyy/MM/dd HH:mm:sss ").format(new java.util.Date())+" State> -> " + sinkState.getClass().getName() + ".entry()!");
            sinkState.entry(event, currentState);
            if (traceState)
                System.out.println(" <Ret>");
            // skip to next state
            currentState = sinkState;
            if (event != null && event.getClass().getName().equals("hyi.jpos.events.DataEvent") && event.getSource().getClass().getName().endsWith("Scanner")) {
                Scanner scanner = (Scanner) event.getSource();
                try {
                    scanner.setDataEventEnabled(true);
                } catch (JposException jposexception1) {
                }
            }
            event = null;
        } while (isTriggerless(currentState.getClass().getName()));
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    // 读取 daishou*.conf 的Code
    private void readDaishouCode() {
        final String prefix = "daishou";
        final String postfix = ".conf";
        File file = new File(CreamToolkit.getConfigDir());
        if (file != null) {
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                file = files[i];
                if (file.isFile() && file.getName().startsWith(prefix) && file.getName().endsWith(postfix)) {
                    String filename = file.getName();
                    String daishouNumber = filename.substring(prefix.length(), filename.length() - postfix.length());
                    if (daishouNumber != null && !daishouNumber.equals(""))
                        readFromFile(file, daishouNumber);
                }
            }
        }
    }

    public void readFromFile(File propFile, String daishouNumber) {
        try {
            FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = null;
            if (CreamProperties.getInstance().getProperty("Locale").equals("zh_TW")) {
                inst = new InputStreamReader(filein, "BIG5");
            } else if (CreamProperties.getInstance().getProperty("Locale").equals("zh_CN")) {
                inst = new InputStreamReader(filein, "GBK");
            }
            BufferedReader in = new BufferedReader(inst);
            String line;
            char ch = ' ';
            int i;

            do {
                do {
                    line = in.readLine();
                    if (line == null) {
                        break;
                    }
                    while (line.equals("")) {
                        line = in.readLine();
                    }
                    i = 0;
                    do {
                        ch = line.charAt(i);
                        i++;
                    } while ((ch == ' ' || ch == '\t') && i < line.length());
                } while (ch == '#' || ch == ' ' || ch == '\t');

                if (line == null) {
                    break;
                }
                String[] arr = split(line);
                if (arr.length == 2) {
                    String number = arr[arr.length - 1];
                    String daishoudetailNumber = arr[0].substring(0, 2);
                    String daishouNmae = arr[0].substring(3, arr[0].length());
                    String[] detail = new String[3];
                    detail[0] = daishouNumber;
                    detail[1] = daishoudetailNumber;
                    detail[2] = daishouNmae;
                    dsCodeMap.put(number, detail);
                }
            } while (true);
        } catch (FileNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("File is not found: " + propFile + ", at " + this);
        } catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("IO exception at " + this);
        }
    }

    private String[] split(String line) {
        List tokens = new ArrayList();
        StringTokenizer tokener = new StringTokenizer(line, ", ", false);
        while (tokener.hasMoreTokens())
            tokens.add(tokener.nextToken());
        String[] strings = new String[tokens.size()];
        System.arraycopy(tokens.toArray(), 0, strings, 0, tokens.size());
        return strings;
    }

    public Map getDsCodeMap() {
        return dsCodeMap;
    }

    public boolean getPaymentMenuVisible() {
        return paymentMenuVisible;
    }

    public void setPaymentMenuVisible(boolean paymentMenuVisible) {
        this.paymentMenuVisible = paymentMenuVisible;
    }
}
