// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.state.*;
import hyi.cream.uibeans.*;

import java.awt.*;
import java.util.*;
import java.math.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */

public class SummaryState extends State {

	POSTerminalApplication app   = POSTerminalApplication.getInstance();
	Transaction trans            = app.getCurrentTransaction();
    private Class exitState      = null;
	//DiscountState discountState  = null;
	//ArrayList array              = null;
    static SummaryState summaryState = null;
	/**
	 * Meyer/2003-02-26
	 *   		Get MixAndMatchVersion
	 */
	private static final String MIX_AND_MATCH_VERSION = 
		CreamProperties.getInstance().getProperty("MixAndMatchVersion");


    public static SummaryState getInstance() {
        try {
            if (summaryState == null) {
                summaryState = new SummaryState();
            }
        } catch (InstantiationException ex) {
        }
        return summaryState;
    }

    /**
     * Constructor
     */
    public SummaryState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {

        if (sourceState != null
            && !(sourceState instanceof SummaryState)) {
            exitState = sourceState.getClass();
            if (sourceState instanceof Warning2State
                || sourceState instanceof Paying3State
                || sourceState instanceof Numbering2State
                || sourceState instanceof DiscountState) {
                    exitState = IdleState.class;
            }
        }
        if (sourceState instanceof ScanTicketState)
            exitState = IdleState.class;

		app.getMessageIndicator().setMessage("");
		if (sourceState instanceof IdleState 
           || sourceState instanceof DiscountState ) {

		    //gllg
		    ScanTicketState.getInstance().clearSome();

            POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
			//posTerminal.getPopupMenuPane().setVisible(false);
			
            if (!(sourceState instanceof DiscountState)) {
                trans.initSIInfo();
                int custID = ((AgeLevelButton)event.getSource()).getAgeID();
                trans.setCustomerAgeLevel(custID);
            }
                
            trans.initForAccum();
            trans.accumulate();

            if (sourceState instanceof IdleState){
            	//Bruce/2003-12-24
            	trans.saveOriginalLineItemAfterDiscountAmount();
            }
            
            trans.setChangeAmount(new BigDecimal(0));
			//app.getLineItemIndicator().setVisible(false);
			trans.setPayCash(new BigDecimal(0));
            PayingPaneBanner payingPane = app.getPayingPane();
            payingPane.setTransaction(posTerminal.getCurrentTransaction());
			//Panel p = app.getItemListPanel();
			app.getItemList().setVisible(false);
			app.getPayingPane().setVisible(true);
            
		} else	if (sourceState instanceof DaiFuIdleState
            || sourceState instanceof PaidOutIdleState
            || sourceState instanceof PaidInIdleState) {
			POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();

            trans.initForAccum();
            trans.initSIInfo();
            trans.accumulate();
            trans.setChangeAmount(new BigDecimal(0));
            trans.setPayCash(new BigDecimal(0));
            
            //app.getLineItemIndicator().setVisible(false);
            app.getItemList().setVisible(false);
            app.getPayingPane().setTransaction(posTerminal.getCurrentTransaction());
            app.getPayingPane().setVisible(true);
		}

        //if (sourceState instanceof Paying3State) {
        //    app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("CashWarning"));
        //}

		/*if (sourceState instanceof DiscountState) {
			 DiscountState d =(DiscountState)sourceState;
			 this.discountState = d;
			 this.array = d.getArrayValue();
		}*/
		app.getPayingPane().repaint();
		//System.out.println(trans.getValues());

        CreamToolkit.showText(trans, 1);
    }

	public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");

        if (event.getSource() instanceof TicketButton) {
            return ScanTicketState.class;
        }
		if (event.getSource() instanceof ClearButton) {
			trans.initPaymentInfo();

			//app.getLineItemIndicator().setVisible(false);
			app.getPayingPane().setVisible(false);
            app.getPayingPane().clear();
			app.getItemList().setVisible(true);
			//app.getButtonPanel().showLayer(5);
            //System.out.println(exitState);
            app.getWarningIndicator().setMessage("");
            app.getMessageIndicator().setMessage("");
            return exitState;
		}
		//System.out.println(trans.getValues());
		if (event.getSource() instanceof PaymentButton 
            || event.getSource() instanceof EnterButton) {

            //Bruce/2003-12-01
            //现金支付也必须看是否为“自动结清余额”
            String payID;
            if (event.getSource() instanceof PaymentButton) 
			    payID = ((PaymentButton)event.getSource()).getPaymentID();
//            else if (event.getSource() instanceof PaymentMenu2Button)
//                payID = ((PaymentMenu2Button) event.getSource()).getPaymentID();
            else
                payID = "00"; 
            //if (payID.equals("00")) {
            //    return Paying1State.class;
            //}

			Payment payment = Payment.queryByPaymentID(payID);
            if (payment == null) {
                return Warning2State.class;
            }

            //Bruce/20030416
            //一般交易不允许使用“赊帐”支付。
            String payName = payment.getScreenName();
            if (payName.indexOf("赊") != -1) {
                setWarningMessage(CreamToolkit.GetResource().getString("CannotUseCreditPayment"));
                return Warning2State.class;
            }

			String paymentType = payment.getPaymentType();
			int len = paymentType.length();
			if (paymentType.substring(len - 2, len - 1).equals("1")) {
				return Paying1State.class;
			} else {
				setWarningMessage(CreamToolkit.GetResource().getString("SummaryWarning"));
                return Warning2State.class;
            }
		}

        if (event.getSource() instanceof NumberButton) {
            if (trans.getDealType2().equals("4")
                || (trans.getDaiFuAmount() != null
                    && trans.getDaiFuAmount().compareTo(new BigDecimal(0)) != 0)) {
                return SummaryState.class;
            } else {
                return Numbering2State.class;
            }
        }

		return sinkState.getClass();
	}


	/*
	private void clearSI(Transaction cTransac, ArrayList d) {
	    int i = 0;
		while(i < 15 && i < d.size()) {
			//
			BigDecimal next = (BigDecimal)d.get(i++);
			cTransac.setSIDiscountAmount0(cTransac.getSIDiscountAmount0().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIDiscountAmount1(cTransac.getSIDiscountAmount1().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIDiscountAmount2(cTransac.getSIDiscountAmount2().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIDiscountAmount3(cTransac.getSIDiscountAmount3().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIDiscountAmount4(cTransac.getSIDiscountAmount4().subtract(next));
			//
			next = (BigDecimal)d.get(i++);
			cTransac.setSIPercentOffAmount0(cTransac.getSIPercentOffAmount0().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIPercentOffAmount1(cTransac.getSIPercentOffAmount1().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIPercentOffAmount2(cTransac.getSIPercentOffAmount2().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIPercentOffAmount3(cTransac.getSIPercentOffAmount3().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIPercentOffAmount4(cTransac.getSIPercentOffAmount4().subtract(next));
			//
			next = (BigDecimal)d.get(i++);
			cTransac.setSIPercentPlusAmount0(cTransac.getSIPercentPlusAmount0().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIPercentPlusAmount1(cTransac.getSIPercentPlusAmount1().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIPercentPlusAmount2(cTransac.getSIPercentPlusAmount2().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIPercentPlusAmount3(cTransac.getSIPercentPlusAmount3().subtract(next));
			next = (BigDecimal)d.get(i++);
			cTransac.setSIPercentPlusAmount4(cTransac.getSIPercentPlusAmount4().subtract(next));
		}
		while(i < 30 && i < d.size()) {
			Integer next = (Integer)d.get(i++);
			cTransac.setSIDiscountCount0(cTransac.getSIDiscountCount0().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIDiscountCount1(cTransac.getSIDiscountCount1().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIDiscountCount2(cTransac.getSIDiscountCount2().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIDiscountCount3(cTransac.getSIDiscountCount3().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIDiscountCount4(cTransac.getSIDiscountCount4().intValue() - next.intValue());

			next = (Integer)d.get(i++);
			cTransac.setSIPercentOffCount0(cTransac.getSIPercentOffCount0().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIPercentOffCount1(cTransac.getSIPercentOffCount1().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIPercentOffCount2(cTransac.getSIPercentOffCount2().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIPercentOffCount3(cTransac.getSIPercentOffCount3().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIPercentOffCount4(cTransac.getSIPercentOffCount4().intValue() - next.intValue());

			next = (Integer)d.get(i++);
			cTransac.setSIPercentPlusCount0(cTransac.getSIPercentPlusCount0().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIPercentPlusCount1(cTransac.getSIPercentPlusCount1().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIPercentPlusCount2(cTransac.getSIPercentPlusCount2().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIPercentPlusCount3(cTransac.getSIPercentPlusCount3().intValue() - next.intValue());
			next = (Integer)d.get(i++);
			cTransac.setSIPercentPlusCount4(cTransac.getSIPercentPlusCount4().intValue() - next.intValue());
	    }
	}*/
}


