/**
 * State class
 * @since 2000
 * @author slackware
 */
 
package hyi.cream.state;

//for event processing
import java.util.*;

import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

public class ToGoOrHereState extends State {
    static ToGoOrHereState toGoOrHereState = null;

    public static ToGoOrHereState getInstance() {
        try {
            if (toGoOrHereState == null) {
                toGoOrHereState = new ToGoOrHereState();
            }
        } catch (InstantiationException ex) {
        }
        return toGoOrHereState;
    }

    /**
     * Constructor
     */
    public ToGoOrHereState() throws InstantiationException {
    }

    public void entry (EventObject event, State sourceState) {
		//System.out.println("This is ToGoOrHereState's Entry!");
		POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
		 //IdleState	InOutStoreButton
		if (sourceState.getClass().getName().endsWith("IdleState")
			&& event.getSource().getClass().getName().endsWith("InOutStoreButton")) {
			//Set corresponding内食or外带property of the currentTransaction.
			InOutStoreButton inOutStoreButton = (InOutStoreButton)event.getSource();
			posTerminal.getCurrentTransaction().setInOutStore(inOutStoreButton.getInOutCode());
		}
        //KeylockWarningState	Keylock
    }

	public Class exit(EventObject event, State sinkState) {
		//System.out.println("This is ToGoOrHereState's Exit!");
		//IdleState	null	Do nothing
		if (sinkState != null && event == null) {
			if (sinkState.getClass().getName().endsWith("IdleState"))
				return sinkState.getClass();
			else
			    return null;
		} else
            return null;
	}	//KeylockWarningState	Keylock

}


