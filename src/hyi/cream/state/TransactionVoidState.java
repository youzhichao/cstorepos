package hyi.cream.state;

import java.util.*;
import java.math.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.inline.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author 
 */
public class TransactionVoidState extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    private ResourceBundle res          = CreamToolkit.GetResource();

    static TransactionVoidState transactionVoidState = null;

    public static TransactionVoidState getInstance() {
        try {
            if (transactionVoidState == null) {
                transactionVoidState = new TransactionVoidState();
            }
        } catch (InstantiationException ex) {
        }
        return transactionVoidState;
    }

    /**
     * Constructor
     */
    public TransactionVoidState() throws InstantiationException {
    }              

    public void entry(EventObject event, State sourceState) {
        //System.out.println("TransactionVoidState.entry");
        Transaction curTran = app.getCurrentTransaction();
        if (sourceState instanceof VoidReadyState
            && event.getSource() instanceof EnterButton) {
            
            if (app.getTrainingMode()) {
                curTran.Clear();
                return;
            }
            String transactionID = String.valueOf(curTran.getNextTransactionNumber() - 1);
            //System.out.println("  ******* " + transactionID);
            Transaction oldTran = Transaction.queryByTransactionNumber(transactionID);
            if (oldTran == null) {
                app.getMessageIndicator().setMessage(res.getString("ReturnWarning"));
            } else {

                //  cancel this transaction
                oldTran.setDealType1("*");
                oldTran.setDealType2("0");
                oldTran.setDealType3("0");
                oldTran.update();

                //  set to transaction
                Transaction trans = app.getCurrentTransaction();
                trans.Clear();
                trans.setDealType2("0");
                trans.setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));

                curTran = Transaction.queryByTransactionNumber(transactionID);
                curTran.clearLineItem();
                curTran.setDealType1("0"); 
                curTran.setDealType2("0");
                curTran.setDealType3("1");
                curTran.setVoidTransactionNumber(new Integer(curTran.getNextTransactionNumber() - 1));
                curTran.setStoreNumber(CreamProperties.getInstance().getProperty("StoreNumber"));
                curTran.setSystemDateTime(new Date());
                curTran.setTerminalNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("TerminalNumber")));
                curTran.setTransactionNumber(new Integer(Transaction.getNextTransactionNumber()));
                curTran.setSignOnNumber(Integer.parseInt(CreamProperties.getInstance().getProperty("ShiftNumber")));
                curTran.setTransactionType("00");
                curTran.setTerminalPhysicalNumber(CreamProperties.getInstance().getProperty("TerminalPhysicalNumber"));
                curTran.setCashierNumber(CreamProperties.getInstance().getProperty("CashierNumber"));
                curTran.setInvoiceID(CreamProperties.getInstance().getProperty("InvoiceID"));
                curTran.setInvoiceNumber(CreamProperties.getInstance().getProperty("InvoiceNumber"));
                curTran.setInvoiceCount(1);
                curTran.setBuyerNumber("");

                //  set to LineItem
                Iterator iter = LineItem.queryByTransactionNumber(transactionID);
//                int count = 0;
                if (iter != null) {
                    LineItem oldLineItem = null;
                    LineItem newLineItem = null;
                    while (iter.hasNext()) {
                        oldLineItem = (LineItem)iter.next();
                        newLineItem = oldLineItem;
                 
                        //2003-06-13 Updated by zhaohong  如果是限价销售,实际销售数量减 1
						if (oldLineItem.getDiscountType() != null && oldLineItem.getDiscountType().equals("L")){
							try{
								Client.getInstance().processCommand("queryLimitQtySold " + oldLineItem.getItemNumber() + " -1 "); 
    
							}catch(ClientCommandException e){
							}
						}
                        
                        newLineItem.setTerminalNumber(new Integer(CreamProperties.getInstance().getProperty("TerminalNumber")));
                        newLineItem.setTransactionNumber(new Integer(Transaction.getNextTransactionNumber()));
                        try {
                            if (newLineItem.getDetailCode() != null && newLineItem.getDetailCode().equals("D")) {
                                SI siDac = SI.queryBySIID(newLineItem.getPluNumber());
                                curTran.setSIAmtList(siDac.getSIID(), newLineItem.getAmount());
                            } 
                            curTran.setLockEnable(true);
                            curTran.addLineItem(newLineItem);
//                            count++;
                        } catch (TooManyLineItemsException e) {
                        }
                    }
                }

                curTran.setLockEnable(true);
                curTran.makeNegativeValue();

                //cancel related tokendtl
                cancelTokenDtl(curTran, oldTran);
                CreamToolkit.logMessage("store() in TransactionVoidState entry()");
                curTran.store();

                transactionID = "";
                app.getMessageIndicator().setMessage(res.getString("TransactionEnd"));
            }
        }
    }

    private void cancelTokenDtl(Transaction curTran, Transaction oldTran) {
        Iterator it = TokenTranDtl.queryByTranNo(oldTran.getTransactionNumber().toString());
        if (it != null) {
            try {
                TokenTranDtl ttd;
                while (it.hasNext()) {
                    ttd = (TokenTranDtl) it.next();
                    ttd.setTransactionNumber(curTran.getTransactionNumber().intValue());
                    //ttd.setSystemDate(oldTran.getSystemDateTime());
                    ttd.setSaleQuantity(ttd.getSaleQuantity().negate());
                    ttd.setTokenQuantity(ttd.getTokenQuantity().negate());
                    ttd.setTokenAmount(ttd.getTokenAmount().negate());
                    ttd.insert();
                }
            } catch (Exception te) {
                System.out.println("te = " + te);
                te.printStackTrace(CreamToolkit.getLogger());
            }

        }
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("TransactionVoidState.exit");

        if (sinkState instanceof InitialState) {
            app.setChecked(false);
        }
        
        return sinkState.getClass();
    }
}

