// Copyright (c) 2000 HYI
package hyi.cream.state;

import java.util.*;

import hyi.cream.*;
import hyi.cream.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class VersionState extends State {
	private POSTerminalApplication app  = POSTerminalApplication.getInstance(); 

	static VersionState versionState = null;

    public static VersionState getInstance() {
        try {
            if (versionState == null) {
                versionState = new VersionState();
            }
        } catch (InstantiationException ex) {
        }
        return versionState;
    }

    /**
     * Constructor
     */
    public VersionState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("Warning"));
        app.getWarningIndicator().setMessage(CreamProperties.getInstance().getProperty("version"));
    }

    public Class exit(EventObject event, State sinkState) {
        return sinkState.getClass();
    }
}
