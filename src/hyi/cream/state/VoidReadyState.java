
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;
import java.math.*;

/**
 * 前笔误打 State class.
 *
 * @author dai
 */
public class VoidReadyState extends State {

    private POSTerminalApplication app   = POSTerminalApplication.getInstance();
    private Transaction trans            = app.getCurrentTransaction(); 
    private java.util.ResourceBundle res = CreamToolkit.GetResource();

    static VoidReadyState voidReadyState = null;

    public static VoidReadyState getInstance() {
        try {
            if (voidReadyState == null) {
                voidReadyState = new VoidReadyState();
            }
        } catch (InstantiationException ex) {
        }
        return voidReadyState;
    }

    /**
     * Constructor
     */
    public VoidReadyState() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        //System.out.println("VoidReadyState entry");

        String transactionID = String.valueOf(trans.getNextTransactionNumber() - 1);
        Iterator iter = LineItem.queryByTransactionNumber(transactionID);
        trans.setDealType1("0");
        trans.setDealType2("0");
        trans.setDealType3("1");
        LineItem lineItem = null;
        while (iter.hasNext()) {
            lineItem = (LineItem)iter.next();

            /*  check lineitem detail code
                "S":销售
                "R":退货
                "D":SI折扣
                "M":Mix & Match折扣
                "B":退瓶
                "I":代收
                "O":营业外商品
                "Q":代付
                "V":指定更正
                "E":立即更正
                "N":PaidIn
                "T":PaidOut
            */
            String s = lineItem.getDetailCode();
            if (s.equals("D")) {
                continue;
            } else if (s.equals("M")) {
                trans.setTotalMMAmount(lineItem.getAmount().negate());
                continue;
            } /*else if (s.equals("V") || s.equals("E")) {
                continue;
            }*/

            //lineItem.setDescription(PLU.queryByPluNumber(lineItem.getPluNumber()).getScreenName());
            try {
                trans.setLockEnable(true);
                trans.addLineItem(lineItem);
            } catch (TooManyLineItemsException e) {
            }
        }

        //app.getItemList().setTransaction(Transaction.queryByTransactionNumber(transactionID));

        app.getMessageIndicator().setMessage(res.getString("VoidConfirm"));
    }

    public Class exit(EventObject event, State sinkState) {
        //System.out.println("VoidReadyState exit");

        // init newCashierID
        app.setNewCashierID("");
        if (event.getSource() instanceof ClearButton) {
            trans.clearLineItem();
			trans.Clear();
            trans.setDealType1("0");
            trans.setDealType2("0");
            trans.setDealType3("0");
			trans.setInvoiceNumber(CreamProperties.getInstance().getProperty("NextInvoiceNumber"));
            app.getItemList().setItemIndex(0);
			
            return IdleState.class;
        }
        if (event.getSource() instanceof EnterButton) {
            app.getMessageIndicator().setMessage(res.getString("DataTransfers"));
        }
        
        return sinkState.getClass();
    }
}


