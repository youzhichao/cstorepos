
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.inline.*;
import hyi.cream.uibeans.*;

import java.util.*;
import java.math.*;

/**
 * 更正State.
 *
 * @author dai
 */
public class VoidState extends State {

    private POSTerminalApplication app   = POSTerminalApplication.getInstance();
    private String removeNumber          = "";
    private boolean pluAvailable         = false;
    static VoidState voidState = null;

    public static VoidState getInstance() {
        try {
            if (voidState == null) {
                voidState = new VoidState();
            }
        } catch (InstantiationException ex) {
        }
        return voidState;
    }

    /**
     * Constructor
     */
    public VoidState() throws InstantiationException {
    }

    private LineItem createVoidLineItem(LineItem removeItem) {
        Transaction curTransaction = app.getCurrentTransaction();
        LineItem voidItem = (LineItem)removeItem.clone();
        
        voidItem.makeNegativeValue();

        voidItem.setRemoved(true);
        voidItem.setLineItemSequence(curTransaction.getLineItemsArrayLast().length + 1);
        if (voidItem.getDiscountType() != null && voidItem.getDiscountType().equals("L")){
            try{
                Client.getInstance().processCommand("queryLimitQtySold " + voidItem.getItemNumber() + " -1 "); // + memberID);
            }catch(ClientCommandException e){
            }
        }

        CreamToolkit.logMessage("Void " + voidItem.toString());
        return voidItem;
    }

    public void entry(EventObject event, State sourceState) {
        Transaction curTransaction = app.getCurrentTransaction();
        
        if (sourceState instanceof NumberingState) {
            removeNumber = ((NumberingState)sourceState).getNumberString();
            if (!CreamToolkit.checkInput(removeNumber, 0)) {
                return;
			}
			Object[] obj = curTransaction.getDisplayedLineItemsArray().toArray();
            if (Integer.parseInt(removeNumber.trim()) > obj.length
                || Integer.parseInt(removeNumber.trim()) < 1) {
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("VoidWarning"));
                setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
                WarningState.setExitState(IdleState.class);
                setPluAvailable(false);
				return;
			}
			//Object [] obj2 = curTransaction.getDisplayedLineItemsArray().toArray();
			LineItem removeItem = (LineItem)obj[Integer.parseInt(removeNumber) - 1];
            if (removeItem.getRemoved()) { 
                setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
                WarningState.setExitState(IdleState.class);
                setPluAvailable(false);
                return;
            }
            removeItem.setRemoved(true);

            try {
                LineItem voidItem = createVoidLineItem(removeItem);
                voidItem.setDetailCode("V");
                voidItem.setPrinted(false);
             
                curTransaction.addLineItem(voidItem);
            } catch (TooManyLineItemsException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Too many LineItem at " + this);
            }
            
            setPluAvailable(true);

        } else if (sourceState instanceof IdleState) {
            ItemList itemList = app.getItemList();
            int[] squ = itemList.getSelectedItems();
            if (squ != null && squ.length > 0) {
                removeNumber = squ[0] + "";
                Object[] obj = curTransaction.getDisplayedLineItemsArray().toArray();
                LineItem removeItem = (LineItem)obj[Integer.parseInt(removeNumber)];
                if (removeItem.getRemoved() ) {
                    setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
                    WarningState.setExitState(IdleState.class);
                    setPluAvailable(false);
                    return;
                }
                removeItem.setRemoved(true);

                try {
                    LineItem voidItem = createVoidLineItem(removeItem);
                    voidItem.setPrinted(false);
                    voidItem.setDetailCode("V");
                    curTransaction.addLineItem(voidItem);

                } catch (TooManyLineItemsException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("Too many LineItem at " + this);
                }
            } else {
                //立即更正是否需要打印
                boolean printImmediateRemove = CreamProperties.getInstance().getProperty("PrintImmediateRemove","no").equalsIgnoreCase("yes");
               
                LineItem removeItem = curTransaction.getCurrentLineItem();
                if (removeItem == null) {
                    setPluAvailable(false);
                    setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
                    WarningState.setExitState(IdleState.class);
                    return;
                }
                if (removeItem.getRemoved()) {
                    setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
                    WarningState.setExitState(IdleState.class);
                    setPluAvailable(false);
                    return;
                }
                
                removeItem.setPrinted(!printImmediateRemove);
                removeItem.setRemoved(true);

                try {
                    LineItem voidItem = createVoidLineItem(removeItem);
                    voidItem.setDetailCode("E");
                    voidItem.setPrinted(!printImmediateRemove); 

                    curTransaction.addLineItem(voidItem);
                } catch (TooManyLineItemsException e) {
                    CreamToolkit.logMessage(e.toString());
                    CreamToolkit.logMessage("Too many LineItem at " + this);
                }
            }
            setPluAvailable(true);
        } else {
            setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
            WarningState.setExitState(IdleState.class);
            setPluAvailable(false);
        }
    }

    public Class exit(EventObject event, State sinkState) {
        if (getPluAvailable()) {
            return MixAndMatchState.class;
        } else {
            setWarningMessage(CreamToolkit.GetResource().getString("NoVoidPlu"));
            return WarningState.class;
        }
    }

    public void setPluAvailable(boolean pluAvailable) {
        this.pluAvailable = pluAvailable;
    }

    public boolean getPluAvailable() {
        return pluAvailable;
    }
}

 
