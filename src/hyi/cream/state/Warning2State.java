// Copyright (c) 2000 HYI
package hyi.cream.state;

import java.awt.*;
import java.util.*;
import jpos.*;

import hyi.cream.uibeans.*;
import hyi.cream.*;
import hyi.cream.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class Warning2State extends State {

    private String printString          = "";
    private POSTerminalApplication app  = POSTerminalApplication.getInstance(); 
	private ToneIndicator tone          = null;
    
    static Warning2State warning2State  = null;

    public static Warning2State getInstance() {
        try {
            if (warning2State == null) {
                warning2State = new Warning2State();
            }
        } catch (InstantiationException ex) {
        }
        return warning2State;
    }

    /**
     * Constructor
     */
    public Warning2State() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {

		POSPeripheralHome posHome = POSPeripheralHome.getInstance();
		//System.out.println("Waringing");
		try {
			tone = posHome.getToneIndicator();
		} catch (NoSuchPOSDeviceException ne) { CreamToolkit.logMessage(ne + "!"); }
		try {
			if (!tone.getDeviceEnabled())
				tone.setDeviceEnabled(true);
			//int sound = 99999;
			tone.setAsyncMode(true);
			tone.sound(99999, 500);
		} catch (JposException je) { }
        
        printString = sourceState.getWarningMessage();

		//app.getWarningIndicator().clear();

        if (printString.equals("")) {
			printString = CreamToolkit.GetResource().getString("Warning");
		}

        app.getWarningIndicator().setMessage(printString);
    }

    public Class exit(EventObject event, State sinkState) {
        app.getWarningIndicator().setMessage("");
        app.getMessageIndicator().setMessage("");  
		try {
			tone.clearOutput();
		} catch (JposException je) { }
        return sinkState.getClass();
    }
}

 
