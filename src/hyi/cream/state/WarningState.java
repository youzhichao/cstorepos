
// Copyright (c) 2000 HYI
package hyi.cream.state;

import java.awt.*;
import java.util.*;
import jpos.*;

import hyi.cream.uibeans.*;
import hyi.cream.*;
import hyi.cream.util.*;
/**
 * A Class class.
 * <P>
 * @author dai
 */
public class WarningState extends State {

    private String printString          = "";
	private POSTerminalApplication app  = POSTerminalApplication.getInstance();
	private ToneIndicator tone          = null;
    private static Class exitState      = null;

	static WarningState warningState = null;


    public static WarningState getInstance() {
        try {
            if (warningState == null) {
                warningState = new WarningState();
            }
        } catch (InstantiationException ex) {
        }
        return warningState;
    }

    /**
     * Constructor
     */
	public WarningState() throws InstantiationException {
		 if (warningState == null)
			  warningState =this;
		 else
		     throw new InstantiationException(this + "");
    }

    public void entry(EventObject event, State sourceState) {
		System.out.println("WarningState entry");
        if (exitState == null) {
			exitState = sourceState.getClass();
		}
        if (sourceState instanceof SetQuantityState
            || sourceState instanceof PluReadyState) {
            exitState = IdleState.class;
        }
		//System.out.println( exitState );

		POSPeripheralHome posHome = POSPeripheralHome.getInstance();
		//System.out.println("Waringing");
		try {
			tone = posHome.getToneIndicator();
			if (!tone.getDeviceEnabled())
				tone.setDeviceEnabled(true);
			//int sound = 99999;
			tone.setAsyncMode(true);
			tone.sound(99999, 500);    
		} catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
		} catch (JposException je) {
        }
		printString = sourceState.getWarningMessage();
        sourceState.setWarningMessage("");
		if (printString.equals("") && this.getWarningMessage().compareTo("") == 0) {
			printString = CreamToolkit.GetResource().getString("Warning");
		} else if (this.getWarningMessage().compareTo("") != 0) {
			printString = getWarningMessage();
			this.setWarningMessage("");
		}
        app.getWarningIndicator().setMessage(printString);
    }

    public Class exit(EventObject event, State sinkState) {
        System.out.println("WarningState exit");
        app.getWarningIndicator().setMessage("");
		app.getMessageIndicator().setMessage("");
        if (tone != null) {
            try {
                tone.clearOutput();
            } catch (JposException je) {
                System.out.println(je);
            }
        }
        Class cla = null;
        try {
            cla = Class.forName(exitState.getName());
        } catch (ClassNotFoundException e) {
        }
        exitState = null;
        return cla;
    }

    public static void setExitState(Class es) {
        exitState = es;
    }
}
