// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   WeiXiuState.java

package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;
import java.io.PrintStream;
import java.util.*;
import jpos.JposException;
import jpos.ToneIndicator;

// Referenced classes of package hyi.cream.state:
//            State, IdleState

public class WeiXiuState extends State
{

    private String annotatedid;
    private POSTerminalApplication app;
    private ResourceBundle res;
    static WeiXiuState weiXiuState = null;
    private ToneIndicator tone;
    private String pluCode;
    private PLU plu;
    private LineItem lineItem;
    private Transaction curTransaction;
    private boolean exit;

    public static WeiXiuState getInstance()
    {
        try
        {
            if(weiXiuState == null)
                weiXiuState = new WeiXiuState();
        }
        catch(InstantiationException instantiationexception) { }
        return weiXiuState;
    }

    public WeiXiuState()
        throws InstantiationException
    {
        annotatedid = "";
        app = POSTerminalApplication.getInstance();
        res = CreamToolkit.GetResource();
        tone = null;
        pluCode = "";
        plu = null;
        lineItem = null;
        curTransaction = app.getCurrentTransaction();
        exit = false;
    }

    public void entry(EventObject event, State sourceState)
    {
        if(sourceState instanceof IdleState)
        {
            annotatedid = "";
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("WeiXiuMessage"));
        } else
        if(sourceState instanceof WeiXiuState)
            if(event.getSource() instanceof ClearButton)
            {
                if(annotatedid == "")
                {
                    exit = true;
                } else
                {
                    try
                    {
                        if(tone != null)
                        {
                            tone.clearOutput();
                            tone.release();
                        }
                    }
                    catch(JposException jposexception) { }
                    annotatedid = "";
                    app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("WeiXiuMessage"));
                    app.getWarningIndicator().setMessage("");
                }
            } else
            if(event.getSource() instanceof NumberButton)
            {
                try
                {
                    if(tone != null)
                    {
                        tone.clearOutput();
                        tone.release();
                    }
                }
                catch(JposException jposexception1) { }
                NumberButton pb = (NumberButton)event.getSource();
                annotatedid += pb.getNumberLabel();
                app.getMessageIndicator().setMessage(annotatedid);
                app.getWarningIndicator().setMessage("");
            }
    }

    public Class exit(EventObject event, State sinkState)
    {
        if(exit)
        {
            exit = false;
            app.getCurrentTransaction().setAnnotatedType("");
            app.getCurrentTransaction().setAnnotatedId("");
            Iterator it = app.getCurrentTransaction().getLineItems();
            List list = new Vector();
            while(it.hasNext()) 
            {
                Object object = it.next();
                if(it.hasNext())
                    list.add(object);
            }
            app.getCurrentTransaction().clearLineItem();
            for(int i = 0; i < list.size(); i++)
                try
                {
                    app.getCurrentTransaction().setLockEnable(true);
                    app.getCurrentTransaction().addLineItem((LineItem)list.get(i));
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

            return hyi.cream.state.IdleState.class;
        }
        String type = app.getCurrentTransaction().getAnnotatedType();
        if(type != null && type.equalsIgnoreCase("P"))
            return hyi.cream.state.IdleState.class;
        if(event.getSource() instanceof EnterButton)
        {
            Transaction curTran = app.getCurrentTransaction();
            if(annotatedid.equals("") || annotatedid.length() != 13)
            {
                POSPeripheralHome posHome = POSPeripheralHome.getInstance();
                try
                {
                    tone = posHome.getToneIndicator();
                }
                catch(NoSuchPOSDeviceException ne)
                {
                    CreamToolkit.logMessage(ne + "!");
                }
                try
                {
                    if(!tone.getDeviceEnabled())
                        tone.setDeviceEnabled(true);
                    if(!tone.getClaimed())
                    {
                        tone.claim(0);
                        tone.setAsyncMode(true);
                        tone.sound(0x1869f, 500);
                    }
                }
                catch(JposException je)
                {
                    System.out.println(je);
                }
                app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WeiXiuNumberLengthError"));
                return hyi.cream.state.WeiXiuState.class;
            } else
            {
                app.getCurrentTransaction().setAnnotatedType("W");
                app.getCurrentTransaction().setAnnotatedId(annotatedid);
                app.getWarningIndicator().setMessage("");
                return hyi.cream.state.IdleState.class;
            }
        }
        if(!(event.getSource() instanceof NumberButton) && !(event.getSource() instanceof ClearButton))
        {
            POSPeripheralHome posHome = POSPeripheralHome.getInstance();
            try
            {
                tone = posHome.getToneIndicator();
            }
            catch(NoSuchPOSDeviceException ne)
            {
                CreamToolkit.logMessage(ne + "!");
            }
            try
            {
                if(!tone.getDeviceEnabled())
                    tone.setDeviceEnabled(true);
                if(!tone.getClaimed())
                {
                    tone.claim(0);
                    tone.setAsyncMode(true);
                    tone.sound(0x1869f, 500);
                }
            }
            catch(JposException je)
            {
                System.out.println(je);
            }
            app.getWarningIndicator().setMessage(CreamToolkit.GetResource().getString("WeiXiuMessage"));
        }
        return hyi.cream.state.WeiXiuState.class;
    }

}
