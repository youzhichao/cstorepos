// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.inline.*;
import java.util.*;
import java.awt.Toolkit;
import java.rmi.*;

/**
 * ZState state class.
 *
 * @author dai, Bruce
 * @version 1.7
 */
public class ZState extends State {

    /**
     * /Bruce/1.7/2002-04-27/
     *    增加代收明细帐存盘.
     */
    transient public static final String VERSION = "1.7";

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private static ZState zState = null;
    private boolean success = true;
    private boolean isShiftCompleted = true;

    public static ZState getInstance() {
        try {
            if (zState == null) {
                zState = new ZState();
            }
        } catch (InstantiationException ex) {
        }
        return zState;
    }

    /**
     * Constructor
     */
    public ZState() throws InstantiationException {
    }
	
	// modify by lxf 2003.02.08  (new flag FloppyContent)
    public void entry(EventObject event, State sourceState) {
        app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("z"));
        CreamToolkit.logMessage("ZState | entry | start ");
        Date initDate = CreamToolkit.getInitialDate();
        ShiftReport shift = ShiftReport.getCurrentShift();
        if (!CreamProperties.getInstance().getProperty("CashierNumber").equals("")
           || shift != null) {//Cashier has signed off
            if (shift !=  null) {
                if (shift.getAccountingDate().compareTo(initDate) == 0) {
                     isShiftCompleted = false;
                     return;
                }
            }
        }
        app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("z"));
        //作日结
        ZReport z = ZReport.getOrCreateCurrentZReport();
        app.getCurrentTransaction().clear();
        app.getCurrentTransaction().init();
        app.getCurrentTransaction().setDealType2("D");
        CreamToolkit.logMessage("store() in zState entry()");
        app.getCurrentTransaction().store();
        //if (!DacTransfer.getInstance().isConnected()) {

        //  only use when property "FloppyContent" is "full"
        boolean useSaveFull;
        String useSvaeStr = CreamProperties.getInstance().getProperty("FloppyContent");
        useSaveFull = useSvaeStr != null && useSvaeStr.equalsIgnoreCase("full");

        if (!Client.getInstance().isConnected()) {
            CreamToolkit.logMessage("Save Z report to floppy disk");
            if (useSaveFull)
	            LineOffTransfer.getInstance().saveToDisk2(z);
            else
	            LineOffTransfer.getInstance().saveToDisk(z);

            CreamToolkit.logMessage("Save depsales to floppy disk");
            ArrayList objArray = new ArrayList();
            try {
	            for (Iterator it = DepSales.queryBySequenceNumber(z.getSequenceNumber());it.hasNext();) {
					objArray.add(it.next());            	
	            }
				if (!objArray.isEmpty()) {
		            if (useSaveFull)
			            LineOffTransfer.saveToDisk2(objArray.toArray());
		            else
			            LineOffTransfer.getInstance().saveToDisk(objArray.toArray());
				}
            } catch (Exception e) {
            	e.printStackTrace(CreamToolkit.getLogger());
            }

            CreamToolkit.logMessage("Save daishousales to floppy disk");
            objArray.clear();
            try {
                for (Iterator it = DaishouSales.getCurrentDaishouSales(z.getSequenceNumber().intValue()); it.hasNext();)
					objArray.add(it.next());            	
                
				if (!objArray.isEmpty()) {
		            if (useSaveFull)
			            LineOffTransfer.saveToDisk2(objArray.toArray());
		            else
			            LineOffTransfer.getInstance().saveToDisk(objArray.toArray());
				}
            } catch (Exception e) {
            	e.printStackTrace(CreamToolkit.getLogger());
            }

            CreamToolkit.logMessage("Save daishousales2 to floppy disk");
            objArray.clear();
            try {
                Iterator it = DaiShouSales2.getCurrentDaishouSales(z.getSequenceNumber().intValue());
                if (it != null) {
                    while (it.hasNext())
                        objArray.add(it.next());
                
                    if (!objArray.isEmpty()) {
                        if (useSaveFull)
                            LineOffTransfer.saveToDisk2(objArray.toArray());
                        else
                            LineOffTransfer.getInstance().saveToDisk(objArray.toArray());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }

			/*
            if (it != null && it.hasNext()) {
	            if (useSaveFull)
		            DacTransfer.getInstance().saveToDisk2(DepSales.cloneForSC(it));
	            else
		            DacTransfer.getInstance().saveToDisk(DepSales.cloneForSC(it));
            }

            CreamToolkit.logMessage("Save daishousales to floppy disk");
            it = DaishouSales.getCurrentDaishouSales(z.getSequenceNumber().intValue());
            if (it != null && it.hasNext()) {
	            if (useSaveFull)
		            DacTransfer.getInstance().saveToDisk2(DaishouSales.cloneForSC(it));
	            else
		            DacTransfer.getInstance().saveToDisk(DaishouSales.cloneForSC(it));
            }
            */
            CreamToolkit.logMessage("ZState | entry | end ");
        }
    }

    public Class exit(EventObject event, State sinkState) {
        CreamToolkit.logMessage("ZState | exit | connected : " + Client.getInstance().isConnected());
        if (!isShiftCompleted) {
            isShiftCompleted = true;
            DacTransfer.getInstance().setStoZPrompt(true);
            return ShiftState.class;
        }
        ZReport z = ZReport.getOrCreateCurrentZReport();
        
        String zPrint = CreamProperties.getInstance().getProperty("ZPrint");
        if (zPrint == null) {
           zPrint = "no";
        }
        if (zPrint.equals("yes"))
            CreamPrinter.getInstance().printZReport(z, this);
        
        if (Client.getInstance().isConnected()) {
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("zEnd"));
            app.setChecked(false);
//            return KeyLock3State.class;
            return ConfirmZState.getSourceState();
        } else {
            //Bruce/2003-11-06
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("ZNotUploadWarning"));
            CreamToolkit.logMessage("Z upload failed.");
            if (CreamProperties.getInstance().getProperty("UseFloppyToSaveData", "no").equalsIgnoreCase("yes"))
                return ZState2.class;

            try {
                Toolkit.getDefaultToolkit().beep();
                Toolkit.getDefaultToolkit().beep();
                Toolkit.getDefaultToolkit().beep();
                Thread.sleep(2500);
            } catch (InterruptedException e) {
            }
//            return KeyLock3State.class;
            return ConfirmZState.getSourceState();
        }
    }
}

