
// Copyright (c) 2000 HYI
package hyi.cream.state;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

import java.util.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class ZState2 extends State {

    private POSTerminalApplication app  = POSTerminalApplication.getInstance();
    private static ZState2 zState = null;
    private	boolean success = false;

    public static ZState2 getInstance() {
        try {
            if (zState == null) {
                zState = new ZState2();
            }
        } catch (InstantiationException ex) {
        }
        return zState;
    }

    /**
     * Constructor
     */
    public ZState2() throws InstantiationException {
    }

    public void entry(EventObject event, State sourceState) {
        if (!success) {
            if (StateMachine.getFloppySaveError())
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("SaveFloppyErrorInsertAgain"));
            else
                app.getMessageIndicator().setMessage(
                    CreamToolkit.GetResource().getString("SaveFloppyReady"));
        }
    }

    public Class exit(EventObject event, State sinkState) {
        ZReport z = ZReport.getOrCreateCurrentZReport();
        Date initDate = CreamToolkit.getInitialDate();
        app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("SaveFloppy"));
        if (event.getSource() instanceof ClearButton) {
            return hyi.cream.state.CancelZState.class;
        }
        if (!success)  {
        	CreamToolkit.logMessage("ZState2 | start to saveToFloppyDisk...");
            success = LineOffTransfer.getInstance().saveToFloppyDisk(true);
        }
    	CreamToolkit.logMessage("ZState2 | saveToFloppyDisk : " + success);
        StateMachine.setFloppySaveError(!success);
        if (success) {
            String zPrint = CreamProperties.getInstance().getProperty("ZPrint");
            if (zPrint == null) {
               zPrint = "no";
            }
            if (zPrint.equals("yes"))
                CreamPrinter.getInstance().printZReport(z, this);
            app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("zEnd"));
            success = false;
            app.setChecked(false);
//            return KeyLock3State.class;
            return ConfirmZState.getSourceState();
        } else
           return ZState2.class;
    }
}


