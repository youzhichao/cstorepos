package hyi.cream.uibeans;


/**
 * 客层键.
 */
public class AgeLevelButton extends POSButton {
    private String ageID;

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param ageLevelLabel age level label on button.
     * @param ageID age level ID.
     */
	public AgeLevelButton(int row, int column, int level, String ageLevelLabel, String ageID) {
		super(row, column, level, ageLevelLabel);
        this.ageID = ageID;
	}

	public AgeLevelButton(int row, int column, int level, String ageLevelLabel, int keyCode, String ageID) {
		super(row, column, level, ageLevelLabel, keyCode);
		this.ageID = ageID;
	}

    public int getAgeID() {
        return Integer.parseInt(ageID);
	}
	
	/*
	public int getKeyCode() {
		return keyCode;
	}*/
}

