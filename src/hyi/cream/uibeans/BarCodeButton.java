
// Copyright (c) 2000 HYI
package hyi.cream.uibeans;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class BarCodeButton extends POSButton {

    /**
     * Constructor
     */
	public BarCodeButton(int row, int column, int level, String label) {
		super(row, column, level, label);
    }

	public BarCodeButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}

 