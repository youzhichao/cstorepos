
package hyi.cream.uibeans;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import hyi.cream.uibeans.*;
import hyi.cream.util.*;

public class ButtonPanel extends Container {
    private int row                  = 0;
    private int col                  = 0;
	static ButtonPanel buttonPanel   = null;
	//private ScreenButton[][] s       = new ScreenButton[10][6];
	private static HashMap buttonMap = new HashMap();
	private Hashtable tabButtons     = new Hashtable();
    private static int curLayer      = 0;
	private boolean show             = false;

	//add in Third iteration
	private Panel headPanel          = new Panel();
	private Panel mainPanel          = new Panel();
	private Panel subPanel1          = new Panel();
	private Panel subPanel2          = new Panel();
	private Panel subPanel3          = new Panel();
	private Panel subPanel4          = new Panel();
	private Panel subPanel5          = new Panel();

	//Layout
	private CardLayout card          = new CardLayout();
	private GridLayout grid          = new GridLayout(5, 6);

	//Tab buttons
	private ScreenButton tab1        = new ScreenButton("3tab1.jpg");
	private ScreenButton tab2        = new ScreenButton("tab2.jpg");
	private ScreenButton tab3        = new ScreenButton("tab3.jpg");
	private ScreenButton tab4        = new ScreenButton("tab4.jpg");
	private ScreenButton tab5        = new ScreenButton("tab5.jpg");
	private ScreenButton currentTabButton = null;

	//Tab Button Names
	private String tab1Name          = "Tab1";
	private String tab2Name          = "Tab2";
	private String tab3Name          = "Tab3";
	private String tab4Name          = "Tab4";
	private String tab5Name          = "Tab5";
	private String defaltTab         = tab1Name;

	//Colors definition
	private Color subPanelColor      = new Color(0, 0, 0);
	private Color headPanelColor     = new Color(195,193,199);


	static public ButtonPanel getInstance() {
        try {
            if (buttonPanel == null) {
                buttonPanel = new ButtonPanel();
            }
        } catch (InstantiationException e) {
			CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Instantiation exception at " + ButtonPanel.class);
        }
  		return buttonPanel;
	}

    public ButtonPanel() throws InstantiationException {
        if (buttonPanel != null) {
            throw new InstantiationException();
        } else {
            buttonPanel = this;
		}
	}

	ScreenButton getCurrentTabButton() {
		return currentTabButton;
	}
	
	void setCurrentTabButton(ScreenButton currentTabButton) {
		this.currentTabButton = currentTabButton;
	}

	public void showDefault() {
	    //headPanel setting
		headPanel.setBackground(headPanelColor);
		headPanel.setBounds(15, 9, 400, 50);
		headPanel.setLayout(null);

		//sub Panel setting
		subPanel1.setBackground(subPanelColor);
		subPanel2.setBackground(subPanelColor);
		subPanel3.setBackground(subPanelColor);
		subPanel4.setBackground(subPanelColor);
		subPanel5.setBackground(subPanelColor);
		subPanel1.setLayout(null);
		subPanel2.setLayout(null);
		subPanel3.setLayout(null);
		subPanel4.setLayout(null);
		subPanel5.setLayout(null);

        //mainPanel setting
		mainPanel.setBounds(15, 90, 395, 315);
		mainPanel.setBackground(Color.black);
		mainPanel.add(subPanel1);
		mainPanel.add(subPanel2);
		mainPanel.add(subPanel3);
		mainPanel.add(subPanel4);
		mainPanel.add(subPanel5);
		mainPanel.setLayout(card);

		//card layout setting		
		card.addLayoutComponent(subPanel1, tab1Name);
		card.addLayoutComponent(subPanel2, tab2Name);
		card.addLayoutComponent(subPanel3, tab3Name);
		card.addLayoutComponent(subPanel4, tab4Name);
		card.addLayoutComponent(subPanel5, tab5Name);

		//tab button 1
		tab1.setBounds(15, 55, 75, 35);
		tab1.setName(tab1Name);
		tab1.addActionListener(tabListener);
		this.add(tab1);
		if (!tabButtons.containsKey(tab1Name))
			tabButtons.put(tab1Name, tab1);

		//tab button 2
		tab2.setBounds(95, 55, 75, 35);
		tab2.setName(tab2Name);
		tab2.addActionListener(tabListener);
		this.add(tab2);
		if (!tabButtons.containsKey(tab2Name))
			tabButtons.put(tab2Name, tab2);

		//tab button 3
		tab3.setBounds(175, 55, 75, 35);
		tab3.setName(tab3Name);
		tab3.addActionListener(tabListener);
		this.add(tab3);
		if (!tabButtons.containsKey(tab3Name))
			tabButtons.put(tab3Name, tab3);

		//tab button 4
		tab4.setBounds(255, 55, 75, 35);
		tab4.setName(tab4Name);
		tab4.addActionListener(tabListener);
		this.add(tab4);
		if (!tabButtons.containsKey(tab4Name))
			tabButtons.put(tab4Name, tab4);

		//tab button 5
		tab5.setBounds(335, 55, 75, 35);
		tab5.setName("Tab5");
		tab5.addActionListener(tabListener);
		this.add(tab5);
		if (!tabButtons.containsKey("Tab5"))
			tabButtons.put("Tab5", tab5);


	  	this.add(headPanel);
		this.add(mainPanel);		
		setCurrentTabButton(tab1);
		card.show(mainPanel, getCurrentTabButton().getName());
		repaint();
	}

	
	ActionListener tabListener = new TabListener();
	class TabListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			ScreenButton sb = (ScreenButton)e.getSource();
			if (sb.getName().equalsIgnoreCase(tab1Name)) {
				card.show(mainPanel, tab1Name);
			} else if (sb.getName().equalsIgnoreCase(tab2Name)){
				card.show(mainPanel, tab2Name);
			} else if (sb.getName().equalsIgnoreCase(tab3Name)){
				card.show(mainPanel, tab3Name);
			} else if (sb.getName().equalsIgnoreCase(tab4Name)){
				card.show(mainPanel, tab4Name);
			} else if (sb.getName().equalsIgnoreCase(tab5Name)){
				card.show(mainPanel, tab5Name);
			}
			ScreenButton fsb = getCurrentTabButton();
			if (fsb.getName().equals(sb.getName())) {
				return;
			}
			fsb.imageFile = fsb.imageFile.substring(1, fsb.imageFile.length());
			fsb.repaint();
			if (!sb.imageFile.startsWith("3")) {
				sb.imageFile = "3" + sb.imageFile;
			}
			setCurrentTabButton(sb);
			sb.repaint();
		}
	}

	public void add(int row, int col, ScreenButton p) {
        this.row = row;
        this.col = col;
        int w = p.getPreferredSize().width;
        int h = p.getPreferredSize().height;
        int x = w * col + 12;
		int y = h * row + 15;
        setLayout(null);

        /**
         *  x: x coordinate of point at top left corner
         *  y: y coordinate of point at top left corner
         *  w: width of screen
         *  h: height of screen
		 **/
        p.setBounds(x, y, w, h);

		//  if display, then call method: showLayer()
        p.setVisible(false);
        super.add(p);
        createLayer(p);
	}

    //  check this button's layer and then add
	//  key = (layer * 100 + row) * 100 + column

	
    public void createLayer(ScreenButton p) {
        Set keysSet = buttonMap.keySet();
        int len = 0;
        int layer = 0;
        while (true) {
            len = (layer * 100 + row) * 100 + col;
            if (keysSet.contains(new Integer(len))) {
                layer = layer + 1;
            } else {
				break;
            }
        }
        add(row, col, layer, p);
	}

    /**
     * Add a ScreenButton into a centain layer.
     *
     *  @param layer layer number(0-based).
     */
	public void add(int row, int col, int layer, ScreenButton p) {
		this.row = row;
		this.col = col;
		int w = p.getPreferredSize().width;
		int h = p.getPreferredSize().height;
		int r = 10;
		int c = 10;
		int x = 0;
		int y = 0;
		if ( layer != -1) {
			x = w * col + 15 * col + c;
			y = h * row + 10 * row + r;
			p.setBounds(x, y, w, h);
		} else {
			x = w * col;
			y = 1;
			p.setBounds(x, y, w - 2, h - 2);
		}
		switch (layer) {
			case -1:
				headPanel.add(p);
				break;
			case 0:
			    break;
			case 1:
				subPanel1.add(p);
				break;
			case 2:
		    	subPanel2.add(p);
				break;
			case 3:
				subPanel3.add(p);
				break;
			case 4:
				subPanel4.add(p);
				break;
			case 5:
				subPanel5.add(p);
				break;
			default:
				break;
		}
		buttonMap.put(new Integer((layer * 100 + row) * 100 + col), p);
    }

    /**
     * Show up all buttons in the given layer.
     *
     * @param layer layer number
	 */

	public ScreenButton getTabButtonByName(String desireButton) {
		return (ScreenButton)tabButtons.get(desireButton);
	}

	/*public void showLayer(int layer) {
		 String desireButton = "";
		 switch (layer) {
			case -1:
				break;
			case 0:
				break;
			case 1:
				card.show(mainPanel, tab1Name);
				desireButton = tab1Name;
				break;
			case 2:
				card.show(mainPanel, tab2Name);
				desireButton = tab2Name;
				break;
			case 3:
				card.show(mainPanel, tab3Name);
				desireButton = tab3Name;
				break;
			case 4:
				card.show(mainPanel, tab4Name);
				desireButton = tab4Name;
				break;
			case 5:
				card.show(mainPanel, tab5Name);
				desireButton = tab5Name;
				break;
			default:
				break;

		 }
		getTabButtonByName(tab1Name).repaint();
		getTabButtonByName(tab2Name).repaint();
		getTabButtonByName(tab3Name).repaint();
		getTabButtonByName(tab4Name).repaint();
		getTabButtonByName(tab5Name).repaint();

		 ScreenButton fsb = getCurrentTabButton();
		 if (fsb.getName().equals(desireButton)) {
			return;
		 }
		 ScreenButton sb = getTabButtonByName(desireButton);
		 fsb.imageFile = fsb.imageFile.substring(1, fsb.imageFile.length());
		 fsb.repaint();
		 if (!sb.imageFile.startsWith("3")) {
			sb.imageFile = "3" + sb.imageFile;
		 }
		 setCurrentTabButton(sb);
		 sb.repaint();

	}*/

    public Dimension getPreferredSize() {
		return new Dimension(450, 500);
    }

    public Dimension getMinimumSize() {
		return new Dimension(450, 500);
    }

	public void paint(Graphics g) {
    	Image backImage = CreamToolkit.getImage("buttonpanel.jpg");
        MediaTracker tracker = new MediaTracker(this);
        tracker.addImage(backImage, 0);
        try {
            tracker.waitForID(0);
        } catch(InterruptedException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Interrupted exception at " + this);
		}		
        g.drawImage(backImage, 0, 0, this);
        super.paint(g);
    }
}

/*public void showLayer(int layer) {
		// param 'show' for initialize
		if (layer == curLayer && show) {
			return;
		} else {
			show = true;
		}
		Object[] keyArray = (buttonMap.keySet()).toArray();
		for(int i = 0; i < keyArray.length; i++) {
			System.out.println(keyArray[i]);
			int l = ((Integer)keyArray[i]).intValue();
			String ls = String.valueOf(l);
			int lslen = ls.length();
			//  account row and column of button in map
			if (lslen > 4) {
				ls = ls.substring(lslen - 4);
			} else {
				while (ls.length() < 4) {
					ls = "0" + ls;
				}
			}
			col = Integer.parseInt(ls.substring(2));
			row = Integer.parseInt(ls.substring(0, 2));

			//  account key of button in current layer at same position
			int curlen = curLayer * 10000 + row * 100 + col;
			if (l <= (layer * 10000 + 9999) && l >= (layer * 10000)) {
				// if this button is exist then hide
				if (buttonMap.containsKey(new Integer(curlen)) && curLayer != layer) {
                    ((ScreenButton)buttonMap.get(new Integer(curlen))).hide();
                }
                ScreenButton p = (ScreenButton)buttonMap.get(new Integer(l));
				//  if current button is not show then show
                //    else it not show repeatedly 
				if (!p.isShowing()) {
                    p.show();
                }
            }
        }
		curLayer = layer;
	}*/
