package hyi.cream.uibeans;


/**
 * 统一编号键.
 */
public class BuyerNoButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param buyerNoLabel buyer number label on button.
     */
	public BuyerNoButton(int row, int column, int level, String buyerNoLabel) {
		super(row, column, level, buyerNoLabel);
    }
    
    public BuyerNoButton(int row, int column, int level, String buyerNoLabel, int keyCode) {
		super(row, column, level, buyerNoLabel, keyCode);
    }
}

 