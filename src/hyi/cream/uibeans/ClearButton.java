
package hyi.cream.uibeans;

/**
 * 清除键.
 */
public class ClearButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param clearLabel clear label on button.
     */
	public ClearButton(int row, int column, int level, String clearLabel) {
        super(row, column, level, clearLabel);
	}

	public ClearButton(int row, int column, int level, String clearLabel, int keyCode) {
        super(row, column, level, clearLabel, keyCode);
	}
}

 