package hyi.cream.uibeans;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.*;
import java.util.List;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.event.SystemInfoEvent;
import hyi.cream.util.*;

/**
 * DAC object viewer bean.
 */
public class DacViewer extends Canvas {
    private static final int BORDER_X = 5;
    private static final int BORDER_Y = 4;
    private static final int FONT_INSET = 2;

    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ResourceBundle res = CreamToolkit.GetResource();
    private int offScreenH;
    private int offScreenW;
    private Image offScreenImage;
    private Color backgroundColor = new Color(165, 178, 255);
    private Color gridColor = Color.black;
    private List dacList = new ArrayList();
    private String[] dispalyFieldList;
    private String[] dispalyFieldNameList;
    private int[] dispalyFieldWidthList;
    private int numRow;
    private int currentIdx;
    private String backLabel = "";

    /**
     * 翻页处理。
     */
    public boolean keyDataListener(int keyCode) {
        int pageUpCode = app.getPOSButtonHome().getPageUpCode();
        int pageDownCode = app.getPOSButtonHome().getPageDownCode();
        if (keyCode == pageUpCode) {
            if (currentIdx > 0) {
                currentIdx -= numRow;
                if (currentIdx < 0)
                    currentIdx = 0;
                app.getSystemInfo().setPageNumber("" + (currentIdx / numRow + 1));
                repaint();
                return true;
            }
        } else if (keyCode == pageDownCode) {
            if (currentIdx + numRow < dacList.size()) {
                currentIdx += numRow;
                app.getSystemInfo().setPageNumber("" + (currentIdx / numRow + 1));
                repaint();
                return true;
            }
        }
        return false;
    }

    public void update(Graphics g) {
        paint(g);
    }

    public void paint(Graphics g) {
        if (offScreenImage == null || offScreenW != getWidth() || offScreenH != getHeight()) {
            offScreenImage = createImage(getWidth(), getHeight());
            offScreenW = getWidth();
            offScreenH = getHeight();
        }
        Graphics og = offScreenImage.getGraphics();

        // paint background
        og.setColor(backgroundColor);
        og.fill3DRect(0, 0, offScreenW, offScreenH, true);

        int rectStartY = BORDER_Y;
        int rectHeight = offScreenH - 2 * BORDER_Y;
        int rectStartX = BORDER_X;
        int rectWidth = offScreenW - 2 * BORDER_X;
        og.setColor(Color.white);
        og.fillRect(rectStartX, rectStartY, rectWidth, rectHeight);

        if (dispalyFieldList == null || dispalyFieldNameList == null
            || dispalyFieldWidthList == null) {
            g.drawImage(offScreenImage, 0, 0, null);
            og.dispose();
            return;
        }

        Font headerFont = app.getItemList().getHeaderFont();
        Font font = app.getItemList().getFont();
        FontMetrics fm = og.getFontMetrics(headerFont);
        int rowHeight = fm.getAscent() + FONT_INSET * 2;

        // paint back label
        if (app.getItemList().isAntiAlias())
            ((Graphics2D)og).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Font bigFont = new Font("SimHei", Font.BOLD, 128);
        FontMetrics fm2 = og.getFontMetrics(bigFont);
        int backLabelW = fm2.stringWidth(backLabel);
        int backLabelH = fm2.getAscent();
        og.setColor(new Color(230, 235, 255));
        og.setFont(bigFont);
        og.drawString(backLabel,
            rectStartX + (rectWidth - backLabelW) / 2,
            rectStartY + (rectHeight - backLabelH) / 2 + backLabelH);

        // paint grid line and header text
        og.setColor(gridColor);
        og.setFont(headerFont);
        og.drawLine(rectStartX, rectStartY + rowHeight, rectStartX + rectWidth - 1, rectStartY + rowHeight);
        int x = rectStartX;
        for (int i = 0; i < dispalyFieldWidthList.length; i++) {
            int labelW = fm.stringWidth(dispalyFieldNameList[i]);
            int gridW = rectWidth * dispalyFieldWidthList[i] / 100;
            og.drawString(dispalyFieldNameList[i], x + (gridW - labelW) / 2,
                rectStartY + fm.getAscent());
            if (i >= dispalyFieldWidthList.length - 1)
                break;
            x += gridW;
            og.drawLine(x, rectStartY, x, rectStartY + rectHeight - 1);
        }

        // paint DAC objects
        og.setFont(font);
        rectStartY += rowHeight;
        rectHeight -= rowHeight;
        fm = og.getFontMetrics(font);
        rowHeight = fm.getAscent() + FONT_INSET * 2;
        numRow = rectHeight / rowHeight;            // 一页能显示几行
        rowHeight = rectHeight / numRow;            // 再平均分配row height
        int idx = currentIdx;
        int y = rectStartY + rowHeight;
        while (idx < dacList.size() && idx < currentIdx + numRow) {
            DacBase dac = (DacBase)dacList.get(idx);
            x = rectStartX + 1;
            for (int i = 0; i < dispalyFieldList.length; i++) {
                try {
                    Object value = dac.getClass().getMethod("get" + dispalyFieldList[i], new Class[0])
                        .invoke(dac, new Object[0]);
                    if (value == null)
                        value = "";
                    int gridW = rectWidth * dispalyFieldWidthList[i] / 100;
                    if (value instanceof BigDecimal) {      // 靠右显示
                        String displayString = value.toString();
                        og.drawString(displayString,
                            x + (gridW - fm.stringWidth(displayString)) - 1, y);
                    } else { 
                        og.drawString(value.toString(), x, y);
                    }
                    x += gridW;
                } catch (Exception e) {
                    CreamToolkit.logMessage("Method name: get" + dispalyFieldList[i]);
                    e.printStackTrace(CreamToolkit.getLogger());
                }
            }
            y += rowHeight;
            idx++;
        }

        g.drawImage(offScreenImage, 0, 0, null);
        og.dispose();
    }

    public void clearDacList() {
        dacList.clear();
    }

    public void addDacToDacList(DacBase dac) {
        int pageDownCode = app.getPOSButtonHome().getPageDownCode();
        dacList.add(dac);
        while (keyDataListener(pageDownCode)) {
            // 翻到最后一页
        }
        repaint();

        app.getSystemInfo().fireEvent(new SystemInfoEvent(this)); // make ProductInfoBanner paint
    }

    public List getDacList() {
        return dacList;
    }
    
    /**
     * 设置显示的DAC fields
     */
    public void setDispalyFieldList(String[] dispalyFieldList) {
        this.dispalyFieldList = dispalyFieldList;
    }

    /**
     * 设置显示的DAC fields名称
     */
    public void setDispalyFieldNameList(String[] dispalyFieldNameList) {
        this.dispalyFieldNameList = dispalyFieldNameList;
    }

    /**
     * 设置显示的DAC fields宽度比例
     */
    public void setDispalyFieldWidthList(int[] dispalyFieldWidthList) {
        this.dispalyFieldWidthList = dispalyFieldWidthList;
    }

    /**
     * 背景大标题字符
     */
    public String getBackLabel() {
        return backLabel;
    }

    /**
     * 设置背景大标题字符
     */
    public void setBackLabel(String backLabel) {
        this.backLabel = backLabel;
    }

    public int getCurrentIdx() {
        return currentIdx;
    }

    public void setCurrentIdx(int currentIdx) {
        this.currentIdx = currentIdx;
    }
}
