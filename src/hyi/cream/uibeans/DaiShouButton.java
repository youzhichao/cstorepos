package hyi.cream.uibeans;

import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*;  

import jpos.*;
import jpos.events.*;

/**
 * 代收键.    ③
 */
public class DaiShouButton extends POSButton implements ActionListener,
            PopupMenuListener {
    private ResourceBundle res = CreamToolkit.GetResource();
    private POSTerminalApplication app = POSTerminalApplication.getInstance();
    private ArrayList menu = new ArrayList();                                 
    private ArrayList submenu = new ArrayList();
    private static ArrayList reasonArray = new ArrayList();
    private static int selectItem = 0;
    private int level = 0;
    private int daishouNumber = 0;
    private String pluNumber = "";
    private int daishouDetailNumber = 0;
    private String daishouDetailName = "";
    
    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
    public DaiShouButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
     * @param keyCode key code
    */
    public DaiShouButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }

    /**
     * When user presses a keyboard button, it comes a JavaPOS's data event.
     * If it is matched with the key code, then it fires POSButtonEvent to
     * POSButtonListener.
     */
	public void dataOccurred(DataEvent e) {

        if (!POSTerminalApplication.getInstance().getChecked()
            && POSTerminalApplication.getInstance().getScanCashierNumber()) {
            return;
        }

        if (!POSTerminalApplication.getInstance().getEnabledPopupMenu()
            || !(POSTerminalApplication.getInstance().getKeyPosition() == 2
                || POSTerminalApplication.getInstance().getKeyPosition() == 3)) {
            return;
        }
        try {
            POSKeyboard p = (POSKeyboard)e.getSource();
			if (getKeyCode() == p.getPOSKeyData()) {
                Iterator daiShouItem = Reason.queryByreasonCategory("08");
                Reason r = null;
                reasonArray.clear();
                int i = 1;
                menu.clear();
                while (daiShouItem.hasNext()) {
                    r = (Reason)daiShouItem.next();
                    reasonArray.add(r);
                    menu.add(i + "." + r.getreasonName());
                    i++;
                }

                PopupMenuPane pop = app.getPopupMenuPane();
                pop.setMenu(menu);
                pop.setVisible(true);
                level = 1;
                pop.addToList(this);
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSelect"));
            }
        } catch (JposException ex) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Jpos exception at " + this);
        }
    }

    public void doSomething() {
        if (level == 0) {
            return;
        } else if (level == 1) {
            if (app.getPopupMenuPane().getSelectedMode()) {
                selectItem = app.getPopupMenuPane().getSelectedNumber() + 1;
                daishouNumber = selectItem;
                String filename = "daishou" + selectItem + ".conf";
                submenu = readFromFile(filename);

                PopupMenuPane pop = app.getPopupMenuPane();
                pop.setMenu(submenu);
                pop.setVisible(true);
                level = 2;
                pop.addToList(this); 
                //menu.clear();
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSelect"));
            } else {
                menu.clear();
                app.getPopupMenuPane().finished();
            }
        } else if (level == 2) {
            if (app.getPopupMenuPane().getSelectedMode()) {
                selectItem = app.getPopupMenuPane().getSelectedNumber();
                daishouDetailNumber = selectItem + 1;
                daishouDetailName = app.getPopupMenuPane().getSelectedDescription();
                pluNumber = app.getPopupMenuPane().getPluNumber(selectItem);
                firePOSButtonEvent(new POSButtonEvent(this)); 
                submenu.clear();
            } else {
                submenu.clear();
                PopupMenuPane pop = app.getPopupMenuPane();
                pop.setMenu(menu);
                pop.setVisible(true);
                pop.repaint();
                level = 1;
                pop.addToList(this);
                app.getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputSelect"));
            }
        }
    }    

    public ArrayList readFromFile(String filename) {
		if (filename.indexOf(File.separator) == -1)
			filename = CreamToolkit.getConfigDir() + filename;
		File propFile = new File(filename);
        ArrayList menuArrayList = new ArrayList();
        try {
            FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = null;
            if (CreamProperties.getInstance().getProperty("Locale").equals("zh_TW")) {
                inst = new InputStreamReader(filein, "BIG5");
            } else if (CreamProperties.getInstance().getProperty("Locale").equals("zh_CN")) {
                inst = new InputStreamReader(filein, "GBK");
            }
            BufferedReader in = new BufferedReader(inst);
            String line;
            char ch = ' ';
            int i;

            do {
                do {
                    line = in.readLine();
                    if (line == null) {
                        break;
                    }
                    while (line.equals("")) {
                        line = in.readLine();
                    }
                    i = 0;
                    do {
                        ch = line.charAt(i);
                        i++;
                    } while ((ch == ' ' || ch == '\t')&& i < line.length());
                } while (ch == '#' || ch == ' ' || ch == '\t');

                if (line == null) {
                    break;
                }
                menuArrayList.add(line);
            } while (true);
        } catch (FileNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("File is not found: " + propFile + ", at " + this);
            return null;
        } catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("IO exception at " + this);
            return null;
        }
        return menuArrayList;
    }

    public int getDaiShouNumber() {
        return daishouNumber;
    }

    public String getPluNumber() {
        return pluNumber;
    }                     

    public int getDaishouDetailNumber() {
        return daishouDetailNumber;
    }

    public String getDaishouDetailName() {
        return daishouDetailName;
    }

    public Reason getReason() {
        return (Reason)reasonArray.get(selectItem);
    }
}

