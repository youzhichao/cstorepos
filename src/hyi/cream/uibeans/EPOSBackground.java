package hyi.cream.uibeans;

import java.awt.*;

import hyi.cream.*;
/**
 * ePOS application background frame.
 *
 * @author Bruce You
 * @version 1.0
 */

public class EPOSBackground extends Panel {
    private static final EPOSBackground instance = new EPOSBackground();

    private static final int GAP = 5;
    private static final int XGAP = 5;
    private static final int YGAP = 10;
    private static final int XGAP_INNER = XGAP + 5;
    private static final int YGAP_INNER = YGAP + 5;
    private static final int XGAP_INNER2 = XGAP_INNER + 5;
    private static final int YGAP_INNER2 = YGAP_INNER + 5;
    private static final int TABCONTROL_HEIGHT = 30;
    private static final int XGAP_INNER3 = XGAP_INNER2 + 5;
    private static final int YGAP_INNER3_TOP = YGAP_INNER2 + TABCONTROL_HEIGHT + 20;
    private static final int YGAP_INNER3_BOTTOM = YGAP_INNER2 + 5;
    private static final int XGAP_INNER4 = XGAP_INNER3 + 5;
    private static final int AREA6_HEIGHT = 50;
    private static final int AREA9_BANNER_HEIGHT = 20;
    private static final int AREA11_HEIGHT = 24;
    private static final int INDICATOR_HEIGHT = 28;
    private static final int CONNECTION_STATUS_WIDTH = 150;

    // 底下三块的占比
    private static final int AREA9_PERCENTAGE = 30;
    private static final int AREA10_PERCENTAGE = 37;
    private static final int AREA8_PERCENTAGE = 33;

    private int paying_banner_height = 100;
    // PayingPane : paying1 & paying 的宽占比
    private static final int PAYINGPANE_W_PERCENTAGE = 50;
    // PayingPane : paying2 & paying 的高占比
    private static final int PAYINGPANE_H_PERCENTAGE = 40;

    private Image logoImage;
    private Image infoImage;

    private Image offScreenSalesMode;
    private Image offScreenPaymentMode;
    private Graphics og;
    private int offScreenW;
    private int offScreenH;
    private boolean enablePainting;
    private boolean offScreenImageInvalid = true;
    public Object waitForDrawing = new Object();

    public int itemListH;
    public int itemListW;
    public int itemListY;
    public int itemListX;
    public int subtotalH;
    public int subtotalW;
    public int subtotalY;
    public int subtotalX;
    public int screenBannerH;
    public int screenBannerW;
    public int screenBannerY;
    public int screenBannerX;
    public int screenBanner2H;
    public int screenBanner2W;
    public int screenBanner2Y;
    public int screenBanner2X;
    public int productInfoH;
    public int productInfoW;
    public int productInfoY;
    public int productInfoX;
    public int warningIndicatorH;
    public int warningIndicatorW;
    public int warningIndicatorY;
    public int warningIndicatorX;
    public int informationIndicatorH;
    public int informationIndicatorW;
    public int informationIndicatorY;
    public int informationIndicatorX;
    public int messageIndicatorH;
    public int messageIndicatorW;
    public int messageIndicatorY;
    public int messageIndicatorX;
    public int connectMessageH;
    public int connectMessageW;
    public int connectMessageY;
    public int connectMessageX;
    public int pageMessageH;
    public int pageMessageW;
    public int pageMessageY;
    public int pageMessageX;

    public int payingBannerX;
    public int payingBannerY;
    public int payingBannerW;
    public int payingBannerH;
    public int payingBanner1X;
    public int payingBanner1Y;
    public int payingBanner1W;
    public int payingBanner1H;
    public int payingBanner2X;
    public int payingBanner2Y;
    public int payingBanner2W;
    public int payingBanner2H;
    
    private String versionString;

    private EPOSBackground() {
        logoImage = Toolkit.getDefaultToolkit().getImage(EPOSBackground.class.getResource("ePOSLogo.gif"));
        infoImage = Toolkit.getDefaultToolkit().getImage(EPOSBackground.class.getResource("info.gif"));
        MediaTracker tracker = new MediaTracker(this);
        tracker.addImage(logoImage, 0);
        try {
            tracker.waitForID(0);
        } catch (InterruptedException e) {
        }
        tracker.addImage(infoImage, 0);
        try {
            tracker.waitForID(0);
        } catch (InterruptedException e) {
        }

        //Bruce/20080626/ Get version string.
        versionString = POSTerminalApplication.getVersion();
        int i = versionString.indexOf("ePOS");
        if (i != -1)
            versionString = versionString.substring(i + 4).trim();
        versionString = "[" + "Ver:" + versionString + "]";
    }

    public static EPOSBackground getInstance() {
        return instance;
    }

    private Color getColor1() {
        return new Color(12, 24, 84);
    }

    private Color getColor2() {
        return new Color(78, 91, 195);
    }

    public Color getColor3() {
        return new Color(230, 235, 255);
    }

    public Color getColor4() {
        return new Color(165, 178, 255);
    }

    private Color getColor5() {
        return new Color(75, 94, 189);
    }

    public Color getColor6() {
        return new Color(230, 235, 255);
    }

    private Color getColor7() {
        return new Color(167, 179, 255);
    }

    public Color getColor8() {
        return new Color(230, 235, 255);
    }

    private Color getColor12() {
        return new Color(214, 219, 249);
    }

    private Color getLineColor() {
        return new Color(14, 23, 82);
    }

    public Color getTextBackgroundColor() {
        return Color.white;
    }

    public void update(Graphics g) {
        if (isShowing()) {
            paint(g);
        }
    }

    private void paintPaymentModeBackgroundAndCalcUIBeansSize() {
        int x, y, w, h, border;

        // first copy image from sales mode image
        offScreenPaymentMode = createImage(this.getWidth(), this.getHeight());
        Graphics ogPayingPane = offScreenPaymentMode.getGraphics();
        ogPayingPane.drawImage(offScreenSalesMode, 0, 0, this);

        ogPayingPane.setColor(getColor4());
        ogPayingPane.fillRoundRect(
            XGAP_INNER2,
            YGAP_INNER2 + TABCONTROL_HEIGHT + 200,
            this.getWidth() - XGAP_INNER2 * 2,
            this.getHeight() - YGAP_INNER2 * 2 - TABCONTROL_HEIGHT - 200,
            55,
            55);
        ogPayingPane.setColor(getLineColor());
        ogPayingPane.drawArc(XGAP_INNER2, YGAP_INNER2 + TABCONTROL_HEIGHT, 115, 155, 90, 90);
        ogPayingPane.drawLine(
            XGAP_INNER2,
            YGAP_INNER2 + TABCONTROL_HEIGHT + 155 / 2,
            XGAP_INNER2,
            this.getHeight() - YGAP_INNER2 - 55 / 2);
        ogPayingPane.drawArc(XGAP_INNER2, this.getHeight() - YGAP_INNER2 - 55, 55, 55, 180, 90);
        ogPayingPane.drawLine(
            XGAP_INNER2 + 55 / 2,
            this.getHeight() - YGAP_INNER2,
            this.getWidth() - XGAP_INNER2 - 55 / 2,
            this.getHeight() - YGAP_INNER2);
        ogPayingPane.drawArc(
            this.getWidth() - XGAP_INNER2 - 55,
            this.getHeight() - YGAP_INNER2 - 55,
            55,
            55,
            270,
            90);
        ogPayingPane.drawLine(
            this.getWidth() - XGAP_INNER2,
            YGAP_INNER2 + TABCONTROL_HEIGHT,
            this.getWidth() - XGAP_INNER2,
            this.getHeight() - YGAP_INNER2 - 55 / 2);
        ogPayingPane.drawLine(
            XGAP_INNER2 + 110 / 2,
            YGAP_INNER2 + TABCONTROL_HEIGHT,
            this.getWidth() - XGAP_INNER2,
            YGAP_INNER2 + TABCONTROL_HEIGHT);
        ogPayingPane.setColor(getColor5());
        ogPayingPane.fillRect(
            XGAP_INNER3,
            YGAP_INNER3_TOP + AREA6_HEIGHT,
            this.getWidth() - XGAP_INNER3 * 2,
            screenBannerY + screenBannerH - YGAP_INNER3_TOP - AREA6_HEIGHT);

        // Total Amount Area
        x = XGAP_INNER3;
        w = this.getWidth() - XGAP_INNER3 * 2;
        y = AREA6_HEIGHT + YGAP_INNER3_TOP + AREA9_BANNER_HEIGHT + 80;
        h = screenBannerY + screenBannerH - y;
        ogPayingPane.setColor(getColor5());
        ogPayingPane.fillRect(x, y, w, h);
        x += 5;
        y += 5;
        w -= 10;
        h -= 10;
        ogPayingPane.setColor(getColor4());
        ogPayingPane.fillRect(x, y, w, h);
        x += 2;
        y += 2;
        w -= 4;
        h -= 4;
        ogPayingPane.setColor(getTextBackgroundColor());
        ogPayingPane.fillRect(x, y, w, h);
        payingBanner2X = x;
        payingBanner2W = w;

        // Amount Area & Paying Area
        x = XGAP_INNER3 + 80 / 2;
        y = AREA6_HEIGHT + YGAP_INNER3_TOP;
        w = (this.getWidth() - XGAP_INNER3 * 2 - 80 / 2 - 25) * PAYINGPANE_W_PERCENTAGE / 100;

        paying_banner_height =
            Math.round((screenBannerY + screenBannerH - x) * PAYINGPANE_H_PERCENTAGE / 100 - 0.5f);
        h = paying_banner_height;

        ogPayingPane.setColor(getColor4());
        ogPayingPane.fillRect(x, y, w, AREA9_BANNER_HEIGHT);
        ogPayingPane.setColor(getColor5());
        drawInfoBanner(ogPayingPane, x, y, w, AREA9_BANNER_HEIGHT, "AMOUNT");
        drawInfoFrame(ogPayingPane, x, y + AREA9_BANNER_HEIGHT + 1, w, h);
        ogPayingPane.setColor(getColor4());
        border = 4;
        drawInfoFrame(
            ogPayingPane,
            x + border,
            y + AREA9_BANNER_HEIGHT + 1 + border,
            w - border * 2,
            h - border * 2);
        border += 2;
        ogPayingPane.setColor(getTextBackgroundColor());
        drawInfoFrame(
            ogPayingPane,
            x + border,
            y + AREA9_BANNER_HEIGHT + 1 + border,
            w - border * 2,
            h - border * 2);

        payingBanner1X = x + border + 2;
        payingBanner1Y = y + AREA9_BANNER_HEIGHT + 1 + border + 2;
        payingBanner1W = w - border * 2 - 4;
        payingBanner1H = h - border * 2 - 4;

        x += w + 5;
        w = (this.getWidth() - XGAP_INNER3 * 2 - 80 / 2 - 25) - w - 5;
        ogPayingPane.setColor(getColor4());
        ogPayingPane.fillRect(x, y, w, AREA9_BANNER_HEIGHT);
        ogPayingPane.setColor(getColor5());
        drawInfoBanner(ogPayingPane, x, y, w, AREA9_BANNER_HEIGHT, "PAYMENT");
        drawInfoFrame(ogPayingPane, x, y + AREA9_BANNER_HEIGHT + 2, w, h);
        ogPayingPane.setColor(getColor4());
        border = 4;
        drawInfoFrame(
            ogPayingPane,
            x + border,
            y + AREA9_BANNER_HEIGHT + 1 + border,
            w - border * 2,
            h - border * 2);
        border += 2;
        ogPayingPane.setColor(getTextBackgroundColor());
        drawInfoFrame(
            ogPayingPane,
            x + border,
            y + AREA9_BANNER_HEIGHT + 1 + border,
            w - border * 2,
            h - border * 2);

        payingBannerX = x + border + 2;
        payingBannerY = y + AREA9_BANNER_HEIGHT + 1 + border + 2;
        payingBannerW = w - border * 2 - 4;
        payingBannerH = h - border * 2 - 4;

        payingBanner2Y = y + AREA9_BANNER_HEIGHT + 1 + border + 2 + h;
        payingBanner2H = screenBannerY + screenBannerH - payingBanner2Y - border * 2 - 4;
    }

    private void paintSalesModeBackgroundAndCalcUIBeansSize() {
        int x, y, w, h, border;

        // Area 1
        og.setColor(getColor1());
        og.fillRect(0, 0, this.getWidth(), this.getHeight());

        // Area 2
        og.setColor(getColor2());
        og.fillRoundRect(XGAP, YGAP, this.getWidth() - XGAP * 2, this.getHeight() - YGAP * 2, 230, 230);
        og.fillRoundRect(
            XGAP + 400,
            YGAP,
            this.getWidth() - XGAP * 2 - 400,
            this.getHeight() - YGAP * 2,
            55,
            55);
        og.fillRoundRect(
            XGAP,
            YGAP + 250,
            this.getWidth() - XGAP * 2,
            this.getHeight() - YGAP * 2 - 250,
            55,
            55);

        // Area 3
        og.setColor(getColor3());
        og.fillRoundRect(
            XGAP_INNER,
            YGAP_INNER,
            this.getWidth() - XGAP_INNER * 2,
            this.getHeight() - YGAP_INNER * 2,
            230,
            230);

        og.fillRoundRect(
            XGAP_INNER + 400,
            YGAP_INNER,
            this.getWidth() - XGAP_INNER * 2 - 400,
            this.getHeight() - YGAP_INNER * 2,
            55,
            55);
        og.fillRoundRect(
            XGAP_INNER,
            YGAP_INNER + 250,
            this.getWidth() - XGAP_INNER * 2,
            this.getHeight() - YGAP_INNER * 2 - 250,
            55,
            55);

        screenBanner2X = XGAP_INNER + 110; // + 240;
        screenBanner2Y = YGAP_INNER; // + 2;
        screenBanner2W = this.getWidth() - XGAP_INNER * 2 - 135; // - 260;
        screenBanner2H = 30 + 2;

        // Area 4
        og.setColor(getColor4());
        og.fillRoundRect(
            XGAP_INNER2,
            YGAP_INNER2 + TABCONTROL_HEIGHT,
            this.getWidth() - XGAP_INNER2 * 2,
            this.getHeight() - YGAP_INNER2 * 2 - TABCONTROL_HEIGHT,
            110,
            155);
        og.fillRoundRect(
            XGAP_INNER2,
            YGAP_INNER2 + TABCONTROL_HEIGHT + 200,
            this.getWidth() - XGAP_INNER2 * 2,
            this.getHeight() - YGAP_INNER2 * 2 - TABCONTROL_HEIGHT - 200,
            55,
            55);
        og.fillRect(
            XGAP_INNER2 + 400,
            YGAP_INNER2 + TABCONTROL_HEIGHT,
            this.getWidth() - XGAP_INNER2 * 2 - 400,
            this.getHeight() - YGAP_INNER2 * 2 - TABCONTROL_HEIGHT - 200);
        og.setColor(getLineColor());
        og.drawArc(XGAP_INNER2, YGAP_INNER2 + TABCONTROL_HEIGHT, 115, 155, 90, 90);
        og.drawLine(
            XGAP_INNER2,
            YGAP_INNER2 + TABCONTROL_HEIGHT + 155 / 2,
            XGAP_INNER2,
            this.getHeight() - YGAP_INNER2 - 55 / 2);
        og.drawArc(XGAP_INNER2, this.getHeight() - YGAP_INNER2 - 55, 55, 55, 180, 90);
        og.drawLine(
            XGAP_INNER2 + 55 / 2,
            this.getHeight() - YGAP_INNER2,
            this.getWidth() - XGAP_INNER2 - 55 / 2,
            this.getHeight() - YGAP_INNER2);
        og.drawArc(this.getWidth() - XGAP_INNER2 - 55, this.getHeight() - YGAP_INNER2 - 55, 55, 55, 270, 90);
        og.drawLine(
            this.getWidth() - XGAP_INNER2,
            YGAP_INNER2 + TABCONTROL_HEIGHT,
            this.getWidth() - XGAP_INNER2,
            this.getHeight() - YGAP_INNER2 - 55 / 2);
        og.drawLine(
            XGAP_INNER2 + 110 / 2,
            YGAP_INNER2 + TABCONTROL_HEIGHT,
            this.getWidth() - XGAP_INNER2,
            YGAP_INNER2 + TABCONTROL_HEIGHT);

        // Area 5
        og.setColor(getColor5());
        int area5Height = (this.getHeight() - YGAP_INNER3_TOP - YGAP_INNER3_BOTTOM) * 70 / 100;
        og.fillRoundRect(XGAP_INNER3, YGAP_INNER3_TOP, this.getWidth() - XGAP_INNER3 * 2, area5Height, 80, 100);
        og.fillRect(XGAP_INNER3 + 300, YGAP_INNER3_TOP, this.getWidth() - XGAP_INNER3 * 2 - 300, 100);
        int area5SubtotalLeft =
            XGAP_INNER3 + (this.getWidth() - XGAP_INNER3 * 2) * (100 - AREA8_PERCENTAGE) / 100;
        og.fillRoundRect(
            area5SubtotalLeft,
            YGAP_INNER3_TOP,
            this.getWidth() - area5SubtotalLeft - XGAP_INNER3,
            this.getHeight() - YGAP_INNER3_TOP - YGAP_INNER3_BOTTOM,
            55,
            55);
        og.fillRoundRect(
            area5SubtotalLeft,
            YGAP_INNER3_TOP,
            50,
            this.getHeight() - YGAP_INNER3_TOP - YGAP_INNER3_BOTTOM,
            35,
            35);
        og.fillRect(XGAP_INNER3, YGAP_INNER3_TOP + 200, 100, area5Height - 200);
        og.fillRect(area5SubtotalLeft - 30, YGAP_INNER3_TOP, 100, area5Height + 30);
        og.setColor(getColor4());
        og.fillRoundRect(area5SubtotalLeft - 30 * 2, YGAP_INNER3_TOP + area5Height, 30 * 2, 30 * 2, 30, 30);

        // Area 6
        og.setColor(getColor6());
        og.fillRect(
            XGAP_INNER3 + 80 / 2,
            YGAP_INNER3_TOP,
            this.getWidth() - XGAP_INNER3 * 2 - 80 / 2 - 25,
            AREA6_HEIGHT);

        // Area 7
        og.setColor(getColor7());
        itemListX = XGAP_INNER4;
        itemListY = YGAP_INNER3_TOP + AREA6_HEIGHT;
        itemListW = this.getWidth() - XGAP_INNER4 * 2;
        itemListH = area5Height - AREA6_HEIGHT - GAP;
        og.fillRect(
            XGAP_INNER4,
            YGAP_INNER3_TOP + AREA6_HEIGHT,
            this.getWidth() - XGAP_INNER4 * 2,
            area5Height - AREA6_HEIGHT - GAP);
        int area5Bottom = YGAP_INNER3_TOP + AREA6_HEIGHT + area5Height - AREA6_HEIGHT - GAP;
        itemListX++;
        itemListY++;
        itemListW -= 2;
        itemListH -= 2;

        // Area 8
        og.setColor(getColor8());
        subtotalX = area5SubtotalLeft - 2;
        subtotalY = area5Bottom + 1;
        subtotalW = this.getWidth() - area5SubtotalLeft - XGAP_INNER3 - 3;
        subtotalH = this.getHeight() - area5Bottom - YGAP_INNER3_BOTTOM - 5 - 2;
        og.fillRoundRect(subtotalX, subtotalY, subtotalW, subtotalH, 55, 55);
        og.fillRect(subtotalX, subtotalY, subtotalW, 50);
        og.fillRoundRect(
            subtotalX,
            subtotalY,
            50,
            this.getHeight() - area5Bottom - YGAP_INNER3_BOTTOM - 5 - 2,
            35,
            35);
        subtotalX++;
        subtotalY++;
        subtotalW -= 2;
        subtotalH -= 2;

        // Area 9
        og.setColor(getColor5());
        int area9Width = (this.getWidth() - XGAP_INNER3 * 2) * AREA9_PERCENTAGE / 100;
        x = XGAP_INNER3;
        y = YGAP_INNER3_TOP + area5Height + GAP;
        w = area9Width;
        h =
            this.getHeight()
                - (YGAP_INNER3_TOP + area5Height + GAP + AREA9_BANNER_HEIGHT + 1)
                - YGAP_INNER2
                - AREA11_HEIGHT;

        drawInfoBanner(og, x, y, w, AREA9_BANNER_HEIGHT, "INFO");
        drawInfoFrame(og, x, y + AREA9_BANNER_HEIGHT + 1, w, h);
        og.setColor(getColor4());
        border = 4;
        drawInfoFrame(og, x + border, y + AREA9_BANNER_HEIGHT + 1 + border, w - border * 2, h - border * 2);
        og.setColor(getTextBackgroundColor());
        border += 2;
        screenBannerX = x + border;
        screenBannerY = y + AREA9_BANNER_HEIGHT + 1 + border;
        screenBannerW = w - border * 2;
        screenBannerH = h - border * 2;
        drawInfoFrame(og, screenBannerX, screenBannerY, screenBannerW, screenBannerH);
        screenBannerX++;
        screenBannerY++;
        screenBannerW -= 2;
        screenBannerH -= 2;

        // get version string and width
        Font versionFont = new Font("SimHei", Font.PLAIN, 11);
        og.setFont(versionFont);

        int versionStringWidth = og.getFontMetrics().stringWidth(versionString);

        // area page
        pageMessageX = screenBannerX + 10;
        pageMessageY = screenBannerY + screenBannerH + 8;
        pageMessageW = screenBannerW - 10 - versionStringWidth;
        pageMessageH = AREA11_HEIGHT - 4;

        // Area 10
        og.setColor(getColor5());
        int area10Width = (this.getWidth() - XGAP_INNER3 * 2) * AREA10_PERCENTAGE / 100 - 8;
        area9Width += 2;
        x = XGAP_INNER3 + area9Width;
        y = YGAP_INNER3_TOP + area5Height + GAP;
        w = area10Width;
        h =
            this.getHeight()
                - (YGAP_INNER3_TOP + area5Height + GAP + AREA9_BANNER_HEIGHT + 1)
                - YGAP_INNER3_BOTTOM;
        drawInfoBanner(og, x, y, w, AREA9_BANNER_HEIGHT, "PRODUCT");
        drawInfoFrame(og, x, y + AREA9_BANNER_HEIGHT + 1, w, h);
        og.setColor(getColor4());
        border = 4;
        drawInfoFrame(og, x + border, y + AREA9_BANNER_HEIGHT + 1 + border, w - border * 2, h - border * 2);
        og.setColor(getTextBackgroundColor());
        border += 2;
        productInfoX = x + border;
        productInfoY = y + AREA9_BANNER_HEIGHT + 1 + border;
        productInfoW = w - border * 2;
        productInfoH = h - border * 2;
        drawInfoFrame(og, productInfoX, productInfoY, productInfoW, productInfoH);
        productInfoX++;
        productInfoY++;
        productInfoW -= 2;
        productInfoH -= 2;

        // Area 12
        og.setColor(getColor12());
        x = XGAP_INNER3 + 80 / 2 - 6;
        y = YGAP_INNER3_TOP - 10;
        w = this.getWidth() - XGAP_INNER3 * 2 - 80 / 2 - 5 + 6;
        h = INDICATOR_HEIGHT;
        og.fillRoundRect(x, y, w, h, 13, 13);
        og.fillRect(x, y + 16, w, h - 16);
        og.drawImage(infoImage, x + GAP, y + (h - infoImage.getHeight(this)) / 2, this);
        og.fillRect(x + CONNECTION_STATUS_WIDTH, y + INDICATOR_HEIGHT, w - CONNECTION_STATUS_WIDTH, h);
        // info indicator background
        og.setColor(getColor5()); // border color
        og.drawArc(x, y, 13, 13, 90, 90);
        og.drawLine(x, y + 6, x, y + h);
        og.drawLine(x, y + h, x + w, y + h);
        og.drawLine(x + w, y + 6, x + w, y + h);
        og.drawArc(x + w - 13, y, 13, 13, 0, 90);
        og.drawLine(x + 7, y, x + w - 7, y);
        og.drawRect(x + CONNECTION_STATUS_WIDTH, y + INDICATOR_HEIGHT, w - CONNECTION_STATUS_WIDTH, h);

        connectMessageX = XGAP_INNER3 + 80 / 2;
        connectMessageY = y + INDICATOR_HEIGHT;
        connectMessageW = CONNECTION_STATUS_WIDTH - (XGAP_INNER3 + 80 / 2) + x;
        connectMessageH = INDICATOR_HEIGHT;
        connectMessageX++;
        connectMessageY++;
        connectMessageW -= 2;
        //connectMessageH -= 2;

        og.setColor(getTextBackgroundColor());
        border = 3;
        warningIndicatorX = x + GAP + infoImage.getWidth(this) + GAP;
        warningIndicatorY = y + border;
        warningIndicatorW = (w - (GAP + infoImage.getWidth(this) + GAP)) / 2 - 2;
        warningIndicatorH = h - border * 2;
        og.fillRect(warningIndicatorX, warningIndicatorY, warningIndicatorW, warningIndicatorH);
        og.setColor(getColor5()); // border color
        og.drawRect(warningIndicatorX, warningIndicatorY, warningIndicatorW, warningIndicatorH);
        og.drawLine(
            warningIndicatorX + warningIndicatorW + border,
            y,
            warningIndicatorX + warningIndicatorW + border,
            y + h);
        warningIndicatorX++;
        warningIndicatorY++;
        warningIndicatorW -= 2;
        warningIndicatorH -= 2;

        informationIndicatorX = warningIndicatorX + warningIndicatorW + border * 2 + 1;
        informationIndicatorY = warningIndicatorY;
        informationIndicatorW = w - (GAP + infoImage.getWidth(this) + GAP) - warningIndicatorW - border * 3 - 2;
        informationIndicatorH = warningIndicatorH;
        og.setColor(getTextBackgroundColor());
        og.fillRoundRect(informationIndicatorX, informationIndicatorY, informationIndicatorW, informationIndicatorH, 13, 13);
        og.fillRect(informationIndicatorX, informationIndicatorY + 7, informationIndicatorW, informationIndicatorH - 7);
        og.fillRect(informationIndicatorX, informationIndicatorY, 10, informationIndicatorH);
        og.setColor(getColor5()); // border color
        og.drawArc(informationIndicatorX + informationIndicatorW - 13, informationIndicatorY, 13, 13, 0, 90);
        og.drawLine(
            informationIndicatorX,
            informationIndicatorY,
            informationIndicatorX + informationIndicatorW - 7,
            informationIndicatorY);
        og.drawLine(
            informationIndicatorX,
            informationIndicatorY,
            informationIndicatorX,
            informationIndicatorY + informationIndicatorH);
        og.drawLine(
            informationIndicatorX,
            informationIndicatorY + informationIndicatorH,
            informationIndicatorX + informationIndicatorW,
            informationIndicatorY + informationIndicatorH);
        og.drawLine(
            informationIndicatorX + informationIndicatorW,
            informationIndicatorY + 7,
            informationIndicatorX + informationIndicatorW,
            informationIndicatorY + informationIndicatorH);
        informationIndicatorX++;
        informationIndicatorY++;
        informationIndicatorW -= 5;
        informationIndicatorH -= 2;

        messageIndicatorX = x + CONNECTION_STATUS_WIDTH + border;
        messageIndicatorY = y + INDICATOR_HEIGHT + border;
        messageIndicatorW = w - CONNECTION_STATUS_WIDTH - border * 2;
        messageIndicatorH = h - border * 2;
        og.setColor(getTextBackgroundColor());
        og.fillRect(messageIndicatorX, messageIndicatorY, messageIndicatorW, messageIndicatorH);
        og.setColor(getColor5()); // border color
        og.drawRect(messageIndicatorX, messageIndicatorY, messageIndicatorW, messageIndicatorH);
        messageIndicatorX++;
        messageIndicatorY++;
        messageIndicatorW -= 2;
        messageIndicatorH -= 2;

        // paint version string
        og.setFont(versionFont);
        og.setColor(Color.black);
        og.drawString(versionString, pageMessageX + pageMessageW,
            pageMessageY + pageMessageH - 4);
        
        og.drawImage(logoImage, 0, 0, this);
    }

    public void paint(Graphics g) {
        //System.out.println("PAINT> EPOSBackground...");
        if (offScreenSalesMode == null || offScreenW != getWidth() || offScreenH != getHeight()) {
            offScreenSalesMode = createImage(getWidth(), getHeight());
            offScreenW = getWidth();
            offScreenH = getHeight();
            offScreenImageInvalid = true;
        }

        og = offScreenSalesMode.getGraphics();
        ((Graphics2D)og).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        if (offScreenImageInvalid) {
            paintSalesModeBackgroundAndCalcUIBeansSize();
            paintPaymentModeBackgroundAndCalcUIBeansSize();
            POSTerminalApplication.getInstance().layoutUIBeans();
            offScreenImageInvalid = false;
        }

        if (isEnablePainting()) {
//            POSTerminalApplication app = POSTerminalApplication.getInstance();
//            TotalAmountBanner b = app.getTotalAmountBanner();
//            if (b != null && app.getPayingPaneVisible() && b.isDisplayable()) {
//                repaint();
//                System.out.println("PAINT> wait for totalAmountBanner invisible.");
//                return;
//            }
            
            g.drawImage(
                POSTerminalApplication.getInstance().getPayingPaneVisible()
                    ? offScreenPaymentMode
                    : offScreenSalesMode,
                0,
                0,
                this);
            //System.out.println("PAINT> EPOSBackground, paint buffered image");
        }
        og.dispose();
        synchronized (this) {
            notifyAll();
        }
        return;
    }

    public void drawInfoBanner(Graphics og, int x, int y, int w, int h, String info) {
        og.fillRect(x, y, w, 2);
        og.fillRect(x + w - 2, y, 2, h);
        og.fillRect(x, y, w / 2, h);
        og.fillArc(x + w / 2 - h, y - h, h * 2, h * 2, 270, 90);
        og.setFont(new Font("Arial", Font.BOLD, 14));
        og.drawString(info, x + w / 2 + 24, y + h - 3);
    }

    public void drawInfoFrame(Graphics og, int x, int y, int w, int h) {
        og.fillRoundRect(x, y, w, h, 25, 25);
        og.fillRect(x, y, w, 25);
        og.fillRect(x + w - 25, y, 25, h);
    }

    public boolean isEnablePainting() {
        return enablePainting;
    }

    public void setEnablePainting(boolean enablePainting) {
        this.enablePainting = enablePainting;
    }

}