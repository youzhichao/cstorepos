
package hyi.cream.uibeans;

/**
 * 数字键.
 */
public class EmptyButton extends POSButton {

    /**
     * Constructor.
     * @param row row position
     * @param column column position
     * @param label empty label
     */
	public EmptyButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

	public EmptyButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}

 
