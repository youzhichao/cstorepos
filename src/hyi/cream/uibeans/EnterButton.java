
package hyi.cream.uibeans;

/**
 * 确认键.
 */
public class EnterButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param enterLabel enter label on button.
     */
	public EnterButton(int row, int column, int level, String enterLabel) {
		super(row, column, level, enterLabel);
    }

	public EnterButton(int row, int column, int level, String enterLabel, int keyCode) {
        super(row, column, level, enterLabel, keyCode);
    }
}

 