package hyi.cream.uibeans;

/**
 *  内食外带键.
 */
public class InOutStoreButton extends POSButton {
    private String inOutCode;

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param inOutStoreLabel in/out store label on button.
     */
	public InOutStoreButton(int row, int column, int level, String inOutStoreLabel, String inOutCode) {
		super(row, column, level, inOutStoreLabel);
        this.inOutCode = inOutCode;
    }

	public InOutStoreButton(int row, int column, int level, String label, int keyCode, String inOutCode) {
        super(row, column, level, label, keyCode);
        this.inOutCode = inOutCode;
    }

    public String getInOutCode() {
        return inOutCode;
    }
}


 