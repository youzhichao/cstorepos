package hyi.cream.uibeans;

import hyi.cream.util.CreamProperties;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;

public class Indicator extends Canvas implements Runnable {

    private static final long serialVersionUID = 1L;

    private int offScreenH;
    private int offScreenW;
    public static final int SCROLL_MARQUEE = 0;
    public static final int SCROLL_WIGGLE = 1;
    public static final int SCROLL_MARQUEE2 = 2;

    private boolean display = true;
    private String message = "";
    private int mode = 0;
    private Image offscreen = null;
    private Graphics og = null;
    private int startx = 0;
    private int starty = 0;
    private String fontName;
    private String fontSize;
    private Font font;
    private Color msgColor = Color.black;
    private Thread thread;

    private boolean stopThread;

    public Indicator() {
        mode = SCROLL_WIGGLE;
        loadFont();
    }

    public Indicator(int mode) {
        this.mode = mode;
        loadFont();

//        if (mode == SCROLL_MARQUEE2) {
//            thread = new Thread(this);
//            thread.start();
//            System.out.println("Indicator> Start a SCROLL_MARQUEE2 thread...");
//        }
    }

    public Indicator(Color msgColor) {
        this();
        this.msgColor = msgColor;
    }

    private Color getMsgColor() {
        return msgColor;
    }

    private void loadFont() {
        if (font == null) {
            CreamProperties props = CreamProperties.getInstance();
            fontName = props.getProperty("IndicatorFont", "SimHei");
            fontSize = props.getProperty("IndicatorFontSize", "17");
            int size = 17;
            try {
                size = Integer.parseInt(fontSize);
            } catch (NumberFormatException e) {
            }
            font = new Font(fontName, Font.PLAIN, size);
        }
    }

    public void update(Graphics g) {
        if (isShowing()) {
            paint(g);
        }
    }

    public synchronized void setMessage(String message) {
        if (message.equals(this.message)) {
            repaint();
            return;
        }

        this.message = message;

        if (mode == SCROLL_MARQUEE2) {
            startx = getWidth();

        } else if  (mode == SCROLL_WIGGLE || mode == SCROLL_MARQUEE) {
            try {
                Graphics g = getGraphics();
                FontMetrics fm = g.getFontMetrics(font);
                int stringWidth = fm.stringWidth(message);
                if (stringWidth <= getWidth()) {
                    startx = Math.round((getWidth() - fm.stringWidth(message)) / 2);
                    repaint();
                    if (thread != null && thread.isAlive()) {
                        thread.interrupt();
                        stopThread = true;
                        thread = null;
                    }
                } else {
                    if (thread == null || !thread.isAlive()) {
                        thread = new Thread(this);
                        thread.start();
                    }
                }
            } catch (Exception e) {
                startx = 5;
                repaint();
            }
        }

        notifyAll(); // notify content change
    }

    public synchronized String getMessage() {
        return message;
    }

    synchronized public boolean getDisplay() {
        if (startx == getWidth())
            display = false;
        else
            display = true;
        return display;
    }

    synchronized public void setDisplay(boolean display) {
        this.display = display;
    }

    public void run() {
        thread = Thread.currentThread();

        int STEP_WIDTHS = 1;
        int n = 0;
        boolean direction = false;
        startx = 0;
        starty = 0;

        if (message == null)
            message = "";

        if (message.length() == 0)
            startx = getWidth();

        Graphics g = null;
        FontMetrics fm = null;

        while (true) {

            if (stopThread) {
                stopThread = false;
                return;
            }

            if (g == null) {
                try {
                    g = getGraphics();
                    if (g == null) {
                        Thread.sleep(3000);
                        continue;
                    }
                    if (font != null)
                        fm = g.getFontMetrics(font);

                    g.setColor(Color.white);
                    g.fillRect(0, 0, getWidth(), getHeight());
                } catch (InterruptedException e) {
                }
            }

            if (getWidth() <= 0) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }
                continue;
            }

            if (mode == SCROLL_MARQUEE2) {
                if (message.length() != 0) {
                    if ((0 - startx) <= fm.stringWidth(message)) {
                        startx = getWidth() - STEP_WIDTHS * n;
                    } else {
                        startx = getWidth();
                        n = 0;
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                        }
                    }
                    repaint();
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                    }
                    n++;
                } else {
                    repaint();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                    }
                }

            } else {
                if (fm.stringWidth(message) <= getWidth()) {
                    startx = Math.round((getWidth() - fm.stringWidth(message)) / 2);
                    repaint();
                    thread = null;
                    return; // end this thread

                    //// Bruce> Wait for content chagne.
                    //synchronized (this) {
                    //    try {
                    //        wait(20000);
                    //    } catch (InterruptedException e1) {
                    //    }
                    //}
                } else {
                    if (mode == SCROLL_MARQUEE) {
                        if ((0 - startx) <= fm.stringWidth(message)) {
                            startx = getWidth() - STEP_WIDTHS * n;
                        } else {
                            startx = getWidth();
                            n = 0;
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                            }
                        }
                        repaint();
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                        }
                        n++;
                        g.setColor(Color.white);
                    } else if (mode == SCROLL_WIGGLE) {
                        if (startx >= 0) {
                            direction = false;
                            startx = startx - STEP_WIDTHS;
                            try {
                                Thread.sleep(800);
                            } catch (InterruptedException e) {
                            }
                        } else if (startx <= (getWidth() - fm.stringWidth(message))) {
                            direction = true;
                            startx = startx + STEP_WIDTHS;
                            try {
                                Thread.sleep(800);
                            } catch (InterruptedException e) {
                            }
                        } else if (startx < 0 && startx > (getWidth() - fm.stringWidth(message))) {
                            if (direction) {
                                startx = startx + STEP_WIDTHS;
                            } else {
                                startx = startx - STEP_WIDTHS;
                            }
                        }
                        repaint();
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e) {
                        }
                        g.setColor(Color.white);
                    }
                }
            }
        }
    }

    public void paint(Graphics g) {
        paintIndicator(g);
    }

    public void paintIndicator(Graphics g) {
        if (g == null || !getDisplay() || getWidth() == 0 || getHeight() == 0)
            return;

        if (offscreen == null || offScreenW != getWidth() || offScreenH != getHeight()) {
            offscreen = createImage(getWidth(), getHeight());
            offScreenW = getWidth();
            offScreenH = getHeight();
        }

        FontMetrics fm = g.getFontMetrics(font);
        starty = (getHeight() - fm.getAscent()) / 2 + fm.getAscent() - 2;

        og = offscreen.getGraphics();
        og.setFont(font);
        ((Graphics2D)og).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        og.setColor(Color.white);
        og.fillRect(0, 0, getWidth(), getHeight());
        og.setColor(getMsgColor());
        og.drawString(message, startx, starty);

        g.drawImage(offscreen, 0, 0, this);
        og.dispose();
    }
}