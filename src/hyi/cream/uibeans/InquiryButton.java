
package hyi.cream.uibeans;

/**
 * 查价键.
 */
public class InquiryButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param inquiryLabel inquiry label on button.
     */
	public InquiryButton(int row, int column, int level, String inquiryLabel) {
        super(row, column, level, inquiryLabel);
    }               
    
	public InquiryButton(int row, int column, int level, String inquiryLabel, int keyCode) {
        super(row, column, level, inquiryLabel,keyCode);
    }
}

 
