package hyi.cream.uibeans;


/**
 * 项次键.
 */
public class ItemButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param buyerNoLabel buyer number label on button.
     */
	public ItemButton(int row, int column, int level, String buyerNoLabel) {
		super(row, column, level, buyerNoLabel);
    }
    
    public ItemButton(int row, int column, int level, String buyerNoLabel, int keyCode) {
		super(row, column, level, buyerNoLabel, keyCode);
    }
}

 