package hyi.cream.uibeans;


/**
 * ItemPricebutton2
 * author gllg
 */
public class ItemDiscount2Button extends POSButton {
    private static String priceType = "0";
    
	public ItemDiscount2Button(int row, int column, int level, String itemDiscount2Label, String priceType) {
		super(row, column, level, itemDiscount2Label);
        this.priceType = priceType;
	}

	public ItemDiscount2Button(int row, int column, int level, String itemDiscount2Label, int keyCode, String priceType) {
		super(row, column, level, itemDiscount2Label, keyCode);
		this.priceType = priceType;
	}
    
    public static int getPriceType(){
        return Integer.parseInt(priceType);
    }
}