
// Copyright (c) 2000 hyi
package hyi.cream.uibeans;

import hyi.cream.uibeans.*;


/**
 * A Class class.
 * <P>
 * @author gllg
 */
public class LastPageButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
    public LastPageButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
     * @param keyCode key code
    */
    public LastPageButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}