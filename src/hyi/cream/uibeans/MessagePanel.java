package hyi.cream.uibeans;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import hyi.cream.event.*;
import hyi.cream.*;
import hyi.cream.util.*;


public class MessagePanel extends Panel {

	private Image offscreen;
//	private Dimension size = new Dimension(610, 60);
	private Dimension size = null;
	private Indicator indicator;
    private Color backgroundColor = Color.yellow.darker().darker().darker();
    //Image ig = CreamToolkit.getImage("messagepanel.jpg");
	
	public Dimension getPreferredSize() {
		return size;
	}

	public Dimension getMinimumSize() {
        return size;
    }

	public void invalidate() {
        super.invalidate();
        offscreen = null;
	}

    public void add(Indicator indicator) {

        setLayout(new BorderLayout());
        this.indicator = indicator;
        add(this.indicator, BorderLayout.CENTER);
    }

	public void update(Graphics g) {
		paint(g);
	}

    /**
     * Overrides the paint methods of Components.
	 */
	public void paint(Graphics g) {
		size = new Dimension(getWidth(), getHeight());
		if (offscreen == null) { 
            offscreen = createImage(getSize().width, getSize().height);
		} else {
			g.drawImage(offscreen, 0, 0, this);
		}
		
		indicator.setBounds(0, 0, getWidth(), getHeight());
		Graphics og = offscreen.getGraphics();

		og.setColor(backgroundColor);
		og.setColor(Color.orange);
		
		g.drawImage(offscreen, 0, 0, null);
		og.dispose();
	}
	
}
