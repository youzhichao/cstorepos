
package hyi.cream.uibeans;

import jpos.*;
import jpos.events.*;
import java.lang.*;
import hyi.cream.event.*;

public class NumberButton extends POSButton {

    private String numberLabel;

    /**
     * Constructor.
     * @param row row position
     * @param column column position
     * @param numberLabel a string represents the number, such as, "0", "1",
     *          ".", "00", etc.
     */
	public NumberButton(int row, int column, int level, String numberLabel) {
		super(row, column, level, numberLabel);
        this.numberLabel = numberLabel;
	}

    public NumberButton(int row, int column, int level, String numberLabel, int keyCode) {
		super(row, column, level, numberLabel, keyCode);
		this.numberLabel = numberLabel;
	}

	public String getNumberLabel() {
		return numberLabel;
	}
}

 