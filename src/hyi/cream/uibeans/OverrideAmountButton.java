package hyi.cream.uibeans;


/**
 * 变价键.
 */
public class OverrideAmountButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param ageLevelLabel age level label on button.
     * @param ageID age level ID.
     */
	public OverrideAmountButton(int row, int column, int level, String overrideamountLabel) {
		super(row, column, level, overrideamountLabel);
	}

	public OverrideAmountButton(int row, int column, int level, String overrideamountLabel, int keyCode) {
		super(row, column, level, overrideamountLabel, keyCode);
		//CreamToolkit.logMessage("Create OverrideAmountButton, key code=" + keyCode);
	}
}