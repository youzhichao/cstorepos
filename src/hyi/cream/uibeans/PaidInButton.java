package hyi.cream.uibeans; 

import java.util.*;
import java.awt.*;
import java.awt.event.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.event.*; 

import jpos.*;
import jpos.events.*;

/**
 * PaidIn键.    ③
 */
public class PaidInButton extends POSButton /*implements ActionListener,
            PasswordDialogListener*/ {
   

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
    public PaidInButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
     * @param keyCode key code
    */
    public PaidInButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }

    /**
     * When user presses a keyboard button, it comes a JavaPOS's data event.
     * If it is matched with the key code, then it fires POSButtonEvent to
     * POSButtonListener.
     */
	/*public void dataOccurred(DataEvent e) {
        try {
            PINPad p = (PINPad)e.getSource();
			if (getKeyCode() == p.getPrompt()) {

                if (getAlready()) {
                    firePOSButtonEvent(new POSButtonEvent(this));
                    return;
                }

                d = app.getPasswordDialog();
                d.setLength(6);
                app.getMessageIndicator().setMessage(res.getString("Password"));
                d.setVisible(true);
                d.addToList(this);
            }
        } catch (JposException ex) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Jpos exception at " + this);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (getAlready()) {
            firePOSButtonEvent(new POSButtonEvent(this));
            return;
        }

        d = app.getPasswordDialog();
        d.setLength(6);
        app.getMessageIndicator().setMessage(res.getString("Password"));
        d.setVisible(true);
        d.addToList(this);
    }

    public void checking() {
        password = d.getPassword();

        //check password
        Password pwd = Password.queryByLevel(level);
        String passwordKey = "PASSWORD";
        String passwordValue = "";
        boolean passwordRight = false;
        for (int i = 0; i < 10; i++) {
            passwordKey = passwordKey + i;
            passwordValue = (String)pwd.getFieldValue(passwordKey);
            if (passwordValue != null &&
                passwordValue.equalsIgnoreCase(password)) {
                passwordRight = true;
                break;
            }
        }
        if (passwordRight) {
            firePOSButtonEvent(new POSButtonEvent(this));
        } else {
            app.getMessageIndicator().setMessage(res.getString("PasswordWrong"));
        }
    }

    public boolean getAlready() {
        return already;
    }

    public void setAlready(boolean already) {
        this.already = already;
    }*/
}

