
package hyi.cream.uibeans;

/**
 *  支付键.
 */
public class PayButton extends POSButton {
    private String payID;

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param payLabel pay label on button.
     * @param payID pay ID.
     */
    public PayButton(int row, int column, int level, String payLabel, String payID) {
		super(row, column, level, payLabel);
        this.payID = payID;
    }

    public String getPayID() {
        return payID;
    }
}

 