package hyi.cream.uibeans;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.math.*;
import java.lang.reflect.*;

import hyi.cream.event.*;
import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.dac.*;

public class PayingPane extends PayingPaneBanner 
{
	public PayingPane() 
		throws ConfigurationNotFoundException
	{ 
		super(CreamToolkit.getConfigurationFile(PayingPane.class));                                    
	}

	public void setVisible(boolean b) {
        super.setVisible(b);
		POSTerminalApplication.getInstance().setPayingPaneVisible(b);
	}

	public void repaint() {
		super.repaint(); //0, 0, 0, getWidth(), getHeight());
        PayingPaneBanner p1 = POSTerminalApplication.getInstance().getPayingPane1();
        if (p1 != null)
            p1.repaint();
        PayingPaneBanner p2 = POSTerminalApplication.getInstance().getPayingPane2();
        if (p2 != null) {
            p2.repaint();
        }
	}

    public void forceRepaint() {
        forcePaint();
        PayingPaneBanner p1 = POSTerminalApplication.getInstance().getPayingPane1();
        if (p1 != null)
            p1.forcePaint();
        PayingPaneBanner p2 = POSTerminalApplication.getInstance().getPayingPane2();
        if (p2 != null) {
            p2.forcePaint();
        }
    }

}
