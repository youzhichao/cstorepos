package hyi.cream.uibeans;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.math.*;
import java.lang.reflect.*;

import hyi.cream.event.*;
import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.dac.*;

public class PayingPaneBanner extends Canvas implements SystemInfoListener {                                       

    private int offScreenH;
    private int offScreenW;
    private POSTerminalApplication app            = POSTerminalApplication.getInstance();
    //private java.util.ResourceBundle res          = CreamToolkit.GetResource();
    private ArrayList headers                     = new ArrayList();
    private ArrayList fields                      = new ArrayList();
    private ArrayList types                       = new ArrayList();
    private ActionListener actionListener         = null;
    public static Transaction trans               = null;
    private Image offscreen                       = null;
	private Graphics og                           = null;
    private int paperNo                           = 0;
    static int paperMax                           = 0;
	//private int thisPaper                         = 0;
	static private Properties ageLevel            = null;
    private Font fh;
    private Font fv;
    //private File propFile;
    private boolean antiAlias = true;

    /**
     * When constructing an PayingPane object, it'll first read the configuration
     * file "PayingPane.conf," which defines all the properties of PayingPane,
     * including headers and fields.
     * The fields are the properties of the associated PayingPane objects.
	 */
    public PayingPaneBanner(File propFile) throws ConfigurationNotFoundException {
        //this.propFile = propFile;
        char ch = ' ';
        try {
			FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = new InputStreamReader(filein, CreamToolkit.getEncoding());
			BufferedReader in = new BufferedReader(inst);
            String line = "";

			line = in.readLine();
            while (line != null) {
                if (line != "") {
                    boolean available = false;
                    for (int i = 0; i < line.length(); i++) {
                        ch = line.charAt(i);
                        if (ch != ' ') {
                            available = true;
                            break;
                        }
                    }
                    if (available && line.startsWith("font")) {
                    	int fontSizeH = 16;
                    	String fontNameH = "STZhongSong";
						int fontSizeV = 16;
						String fontNameV = "STZhongSong";
						StringTokenizer t0 = new StringTokenizer(line, ",", true);
						while (t0.hasMoreTokens()) {
							String s = t0.nextToken();
							if (s.startsWith("fontNameH")) {
								fontNameH = s.substring("fontNameH".length() + 1, s.length());
							} else if (s.startsWith("fontSizeH")) {
								try {
									s = s.substring("fontSizeH".length() + 1, s.length());
									fontSizeH = Integer.parseInt(s);
								} catch (Exception e) {
									fontSizeH = 16;
								}
							} else if (s.startsWith("fontNameV")) {
								fontNameV = s.substring("fontNameV".length() + 1, s.length());
							} else if (s.startsWith("fontSizeV")) {
								try {
									s = s.substring("fontSizeV".length() + 1, s.length());
									fontSizeV = Integer.parseInt(s);
								} catch (Exception e) {
									fontSizeV = 16;
								}
                            } else if (s.startsWith("antiAlias")) {
                                antiAlias = s.substring("antiAlias".length() + 1, s.length()).equalsIgnoreCase("yes");
                            }
						}
						fv = new Font(fontNameV, Font.PLAIN, fontSizeV);
						fh = new Font(fontNameH, Font.PLAIN, fontSizeH);
						
                    } else if (available && ch != '#') {
                        StringTokenizer t1 = new StringTokenizer(line, ",");
                        headers.add(t1.nextToken());
                        fields.add(t1.nextToken());
                        types.add(t1.nextToken());
                    } 
                }
                line = in.readLine();
			}
			initAgeLevel();
		} catch (FileNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("File is not found: " + propFile.toString() + ", at " + this);
        } catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("IO exception: " + propFile.toString() + ", at " + this);
        }
	}

	public void initAgeLevel() {
		ageLevel = new Properties();
		try {
			FileInputStream age = new FileInputStream(CreamToolkit.getConfigurationFile("agelevel"));
			InputStreamReader inst = new InputStreamReader(age, CreamToolkit.getEncoding());
			BufferedReader in = new BufferedReader(inst);
			while (in.ready()) {
				String next = in.readLine();
				if (next == null)
				    continue;
				StringTokenizer token = new StringTokenizer(next, "=");
				while (token.hasMoreTokens()) {
					String key = token.nextToken();
					String value = "";
					if (token.hasMoreTokens())
					   value = token.nextToken();
					ageLevel.put(key, value);
				}
			}
			in.close();
			inst.close();
		} catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
		}
	}

	public boolean keyDataListener(int prompt) {
        int pageUpCode = app.getPOSButtonHome().getPageUpCode();
        int pageDownCode = app.getPOSButtonHome().getPageDownCode();
        if (prompt == pageUpCode) {
            if (paperNo <= 0) {
                return true;
            }
            paperNo--;
            repaint();
            return true;
        } else if (prompt == pageDownCode) {
            if (paperNo >= paperMax) {
                return true;
            }
            paperNo++;
            repaint();
            return true;
		} else {
            return false;
        }
	}

    /**
     * Sets the associated transaction object. It'll also register itself
     * as the transaction listener of the transaction object.
     * @param trans the transaction object
     */
    public void setTransaction(Transaction trans) {
		PayingPane.trans = trans;
    }

    /**
     * Returns the associated transaction object.
     */
    public Transaction getTransaction() {
        return trans;
    }

    //  0 means normal
    //  1 means cash in and cash out
    private int mode = 0;
    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getMode() {
        return mode;
    }

//    public void invalidate() {
//        super.invalidate();
//        offscreen = null;
//    }

    public void update(Graphics g) {
        paint(g);
    }

    /**
     * Overrides the paint methods of Components.
	 */
	Image ig = CreamToolkit.getImage("payingpane.jpg");
	String fontName = CreamProperties.getInstance().getProperty("PayingPaneFont");
    Font f = new Font(fontName, Font.BOLD, 18);

    public void forcePaint() {
        paint(getGraphics());
    }

	public void paint(Graphics g) {
        if (offscreen == null || offScreenW != getWidth() || offScreenH != getHeight()) {
            offscreen = createImage(getWidth(), getHeight());
            offScreenW = getWidth();
            offScreenH = getHeight();
        }

        og = offscreen.getGraphics();
        if (antiAlias)
            ((Graphics2D)og).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		og.setColor(EPOSBackground.getInstance().getTextBackgroundColor());
		og.fillRect(0, 0, getSize().width, getSize().height);
		og.setFont(f);
		FontMetrics fm = og.getFontMetrics(f);

		//int startx = 0;
		//int endx = getSize().width;
        String strObj;
        //int w = 0;
		int m = 0;
		java.util.List titleList = new Vector();
		java.util.List valueList = new Vector();
		
        //  check show mode
        if (getMode() == 1) {
            Iterator payments = Payment.getAllPayment();
            Payment payment = null;
            String paymentName = "";
            String paymentID = "";
            BigDecimal amount = null;
            String amt = "";
            // 显示支付方式
            while (payments.hasNext()) {
                payment = (Payment)payments.next();
                paymentName = payment.getPrintName();
                paymentID = payment.getPaymentID();
System.out.println("PPP" + paymentID);
                // 显示支付名
                titleList.add(paymentName);
                for (int i = 1; i <= 4; i++) {
                    String fieldName = "PAYNO" + i;
                    if (paymentID.equals((String)trans.getFieldValue(fieldName))) {
                        amount = (BigDecimal)trans.getFieldValue("PAYAMT" + i);
System.out.println("AMT:" + amount);
                        break;
                    }
                }

                if (amount != null &&
                    amount.compareTo(new BigDecimal(0)) == 1) 
                {
                    amt = amount.toString();
System.out.println("DISPLAY:" + amt);
					// 显示支付金额
                } else {
                    amt = "0.00";
					// 显示支付金额
                }
                valueList.add(amt);
                amount = null;
            }
        } else {

            //  fixed show item defined in configration file,
            //  dynamic show item: salesAmount, taxAmount, payment, spillAmount, si,
            //   if it exist
            try {
                for (m = 0; m < fields.size(); m++) {
                    if (((String)fields.get(m)).equals("SI")) {
                        Hashtable siHt = trans.getSIAmtList();
                        Enumeration siKey = siHt.keys();
                       // int i = 0;
                        while (siKey.hasMoreElements()) {
                            String siID = (String)siKey.nextElement();
                            BigDecimal si = (BigDecimal)siHt.get(siID);
                            //w = fm.stringWidth(strObj);
                            strObj = SI.queryBySIID(siID).getScreenName();
                            titleList.add(strObj);
                            valueList.add(si.toString());
                         //   i++;
                        }
                    //  check if "payment"
                    } else if (((String)fields.get(m)).equals("payment")) {
                        String s = "";
                        String st = "";
                        String payID = "";
                        int i = 1;
                        Payment payment = null;
                        Iterator paymentIter = trans.getPayments();
                        while (paymentIter.hasNext()) {
                            paymentIter.next();
                            s = "PAYNO" + i;
                            st = "PAYAMT" + i;
                            if ((String)trans.getFieldValue(s) != null) {
                                BigDecimal payAmount = ((BigDecimal)trans.getFieldValue(st)).setScale(2, 4);
                                strObj = payAmount.toString();
                                //w = fm.stringWidth(strObj);   
                                payID = (String)trans.getFieldValue(s);
                                if (payID.equals("")) {
                                    continue;
                                }
                                valueList.add(strObj);
                                payment = Payment.queryByPaymentID(payID);
                                strObj = payment.getScreenName();
                                titleList.add(strObj);
                            }
                            i++;
                        }
                    //  other
                    } else {
                        //  check daishou and daifu amount
                        if (((String)fields.get(m)).equals("DaiShouAmount")
                            || ((String)fields.get(m)).equals("DaiFuAmount")) {
                            if (trans.getDealType2() != null
                                && (trans.getDealType2().equals("4"))) 
                            {
                                continue;
                            }
                        }

                        //  check cashin, cashout and return transaction's agelevel
                        if (((String)fields.get(m)).equals("CustomerAgeLevel")) {
                            if (trans.getDealType2() != null
                                && (trans.getDealType2().equals("1")
                                    || trans.getDealType2().equals("2")
                                    || trans.getDealType2().equals("3")
                                    || trans.getDealType2().equals("4")
                                    || trans.getDealType2().equals("G")
                                    || trans.getDealType2().equals("H")
                                    || trans.getDealType2().equals("100"))) 
                            {
                                continue;
                            }
                        }
                        Method method = Transaction.class.getDeclaredMethod("get" +
                               fields.get(m).toString(),
                                 new Class[0]);
                        //Font shouldPay = null;	
                        //int unitWidth1 = 0;		
                        Object result = method.invoke(trans, new Object[0]);
                        if (result instanceof BigDecimal) {
                            BigDecimal bd = (BigDecimal)result;  
                            if (bd == null || bd.compareTo(new BigDecimal(0)) == 0) {
                                if (((String)types.get(m)).equals("y")) {
                                    strObj = "0.00";
                                    valueList.add(strObj);
                                    strObj = headers.get(m).toString();
                                    titleList.add(strObj);
                                }
                            } else {                    
                                if (((String)fields.get(m)).equals("TotalMMAmount")) {
                                    bd = bd.negate();
                                }
                                strObj = bd.toString();
                                valueList.add(strObj);
                                strObj = headers.get(m).toString();
                                titleList.add(strObj);
                            }
                        } else if (result instanceof Integer) {
                            Integer bd = (Integer)result;//
                            if (bd == null || bd.compareTo(new Integer(-1)) == 0) {
                                if (((String)types.get(m)).equals("y")) {
                                    strObj = "0";
									valueList.add(strObj);
                                    strObj = headers.get(m).toString();
									titleList.add(strObj);
                                }
                            } else {
                                //CustomerAgeLevel
                                if (((String)fields.get(m)).equals("CustomerAgeLevel")) {
                                    if (ageLevel.containsKey(bd.toString()))
                                        strObj = ageLevel.getProperty(bd.toString());
                                    else
                                        strObj = "unknown";
                                } else
                                    strObj = bd.toString();
									valueList.add(strObj);
                                strObj = headers.get(m).toString();
								titleList.add(strObj);
                            }
                        }
                    }
                }

				og.setColor(Color.black);
				java.util.List list = processBounds(titleList, valueList, og);
				for (int i = 0; i < titleList.size(); i++) {
					java.util.List list1 = (java.util.List) list.get(i);
					int startx1 = ((Integer) list1.get(0)).intValue();
					int starty1 = ((Integer) list1.get(1)).intValue();
					int startx2 = ((Integer) list1.get(2)).intValue();
					int starty2 = ((Integer) list1.get(3)).intValue();
					og.setFont(fh);
					fm = og.getFontMetrics(fh);
					og.drawString(titleList.get(i).toString() + ":", startx1
						, starty1 + fm.getAscent());
					og.setFont(fv);
					fm = og.getFontMetrics(fv);
					og.drawString(valueList.get(i).toString(), startx2
						, starty2 + fm.getAscent());
				}
            } catch (ConcurrentModificationException e2) {
                CreamToolkit.logMessage(e2.toString());
                CreamToolkit.logMessage("Concurrent modification exception at " + this);
                repaint();
                return;
            } catch (NoSuchMethodException e) {
                e.printStackTrace(CreamToolkit.getLogger());
                CreamToolkit.logMessage("No such method at " + this);
            } catch (InvocationTargetException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Invocation target exception at " + this);
            } catch (IllegalAccessException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Illegal access exception at " + this);
            }
        }
        g.drawImage(offscreen, 0, 0, null);
        og.dispose();

//        // When payingpane2 finished its paint, notify EPOSBackground to paint.
//        // Ref. POSTerminalApplication.setPayingPaneVisible()
//        if (propFile.toString().indexOf("payingpane2") != -1) {        
//            synchronized (EPOSBackground.getInstance().waitForDrawing) {
//                EPOSBackground.getInstance().waitForDrawing.notifyAll();
//            }
//        }
    }

    //  initialize paper number
    public void clear() {
        paperNo = 0;
        paperMax = 0;
    }

    public void addActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.add(actionListener,listener);
        enableEvents(AWTEvent.MOUSE_EVENT_MASK);
    }

    public void removeActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.remove(actionListener,listener);
    }
	
	/*
    public void processMouseEvent (MouseEvent e) {
        switch (e.getID()) {
		case MouseEvent.MOUSE_PRESSED:

            //  previous paper button
			if (e.getX() > 123 && e.getX() < 203 &&
				e.getY() > 390 && e.getY() < 410) {
                if (paperNo <= 0) {
                    return;
                }
                paperNo--;
                repaint();

            //  next paper button
            } else if (e.getX() > 214 && e.getX() < 284 &&
					   e.getY() > 390 && e.getY() < 410) {
                if (paperNo >= paperMax) {
                    return;
                }
                paperNo++;
                repaint();
            }
            break;
            
        case MouseEvent.MOUSE_RELEASED:
            break;
            
        case MouseEvent.MOUSE_ENTERED:
            break;
            
        case MouseEvent.MOUSE_EXITED:
            break;
        }
        super.processMouseEvent(e);
    }
    */
    
    private java.util.List processBounds(java.util.List titleList
    	, java.util.List valueList, Graphics og) 
    {
    	int marginW = 10;
    	int marginH = 0;
    	int leftMargin = 0;
    	java.util.List list = new Vector();
    	if (titleList.size() != valueList.size())
    		return list;
    	int maxHead = 0;
    	int maxValue = 0;
		FontMetrics fm = null;
		fm = og.getFontMetrics(fh);
		int fontHeightH = fm.getHeight();
		fm = og.getFontMetrics(fv);
		int fontHeightV = fm.getHeight();
		if (fontHeightH != fontHeightV)
			marginH = 5;
    	for (int i = 0; i < titleList.size(); i++) {
			fm = og.getFontMetrics(fh);
    		int length = fm.stringWidth(titleList.get(i).toString() + ":");
    		if (length > maxHead) 
    			maxHead = length;
			fm = og.getFontMetrics(fv);
			length = fm.stringWidth(valueList.get(i).toString());
			if (length > maxValue) 
				maxValue = length;
    	}
    	if (getWidth() > maxHead + maxValue + marginW) {
    		leftMargin = Math.round((getWidth() - maxHead - maxValue - marginW) 
    			/ 2 - 0.5f);
    		marginW = getWidth() - maxHead - maxValue - 2 * leftMargin; 
    	} else if (getWidth() > maxHead + maxValue) {
			leftMargin = Math.round((getWidth() - maxHead - maxValue - marginW) 
				/ 2 - 0.5f);
			marginW = getWidth() - maxHead - maxValue - 2 * leftMargin; 
    	}
    	//int rightHead = leftMargin + maxHead;
    	//int rightValue = getWidth() - leftMargin;
    	
    	int h = marginH;
    	for (int i = 0; i < titleList.size(); i++) {
    		java.util.List list1 = new Vector();
    		if (i > 0)
    			h += fontHeightV;
			fm = og.getFontMetrics(fh);
			int w = maxHead + leftMargin
				- fm.stringWidth(titleList.get(i).toString() + ":");
    		list1.add(new Integer(w));
    		list1.add(new Integer(h + (fontHeightV - fontHeightH) / 2));
			fm = og.getFontMetrics(fv);
    		w = getWidth() - leftMargin 
    			- fm.stringWidth(valueList.get(i).toString());
    		list1.add(new Integer(w));
    		list1.add(new Integer(h));
    		list.add(list1);
    	}
    	return list;
    }

    public void systemInfoChanged(SystemInfoEvent e) {
        repaint();
    }
}
