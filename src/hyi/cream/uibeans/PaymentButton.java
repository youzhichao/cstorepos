
package hyi.cream.uibeans;

/**
 *  支付键.
 */
public class PaymentButton extends POSButton {
    String payID;

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param payLabel pay label on button.
     * @param payID pay ID.
     */
	public PaymentButton(int row, int column, int level, String payLabel, String payID) {
        super(row, column, level, payLabel);
        this.payID = payID;
	}
/*int
int
int
class java.lang.String
int
int*/

	public PaymentButton(int row, int column, int level, String payLabel, int keyCode, String payID) {
		super(row, column, level, payLabel, keyCode);
        this.payID = payID;
    }

    public String getPaymentID() {
        return payID;
    }
}

 