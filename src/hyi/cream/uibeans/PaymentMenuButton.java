package hyi.cream.uibeans;

import java.util.*;
import java.awt.event.*;

import hyi.cream.state.StateMachine;
import hyi.cream.util.*;
import hyi.cream.*;
import hyi.cream.event.*;
import jpos.events.*;
import jpos.*;

/**
 * 支付Menu键
 */
public class PaymentMenuButton extends POSButton implements ActionListener, PopupMenuListener {

    private String payID;
    private ArrayList menuArrayList = new ArrayList();
    private PopupMenuPane popupMenu = PopupMenuPane.getInstance();
    private ArrayList allArrayList = new ArrayList();

    public PaymentMenuButton(int row, int column, int level, String paymentMenu2Label) {
        super(row, column, level, paymentMenu2Label);
    }


    public PaymentMenuButton(int row, int column, int level, String paymentMenu2Label, String payID) {
        super(row, column, level, paymentMenu2Label);
        this.payID = payID;
        loadPayment();
    }

    public PaymentMenuButton(int row, int column, int level, String payLabel, int keyCode, String payID) {
        super(row, column, level, payLabel, keyCode);
        this.payID = payID;
        loadPayment();
    }

    public String getPaymentID() {
        return popupMenu.getSelectedPartText(2);
    }

    public static String replaceAll(String text, String repl, String with) {
        if (text == null || repl == null || repl.length() == 0 || with == null) {
            return text;
        }

        StringBuffer buf = new StringBuffer(text.length());
        int start = 0, end = 0;
        while ((end = text.indexOf(repl, start)) != -1) {
            buf.append(text.substring(start, end)).append(with);
            start = end + repl.length();
        }
        buf.append(text.substring(start));
        return buf.toString();
    }
    
    private void loadPayment() {
        try {
            payID = "('" + replaceAll(payID, "/", "','") + "')";
            String sql = "SELECT payid, paycname, paytype FROM payment WHERE payid IN " + 
                    payID + " ORDER BY payid";
            System.out.println(sql);
            java.util.List list = CreamToolkit.getResultData2(sql);
            int count = 0;
            for (Iterator it = list.iterator(); it.hasNext();) {
                count++;
                Map record = (Map) it.next();
                allArrayList.add(String.valueOf(count) +  "." + 
                        (String) record.get("paycname") + "," +
                        (String) record.get("payid"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void clear() {
        popupMenu.setIndex(-1);
    }

    public ArrayList getMenu() {
        return menuArrayList;
    }

    public void setPopMenu(PopupMenuPane p) {
        popupMenu = p;
    }

    public void actionPerformed(ActionEvent e) {
        if (!StateMachine.getInstance().getPaymentMenuVisible() ||
            POSTerminalApplication.getInstance().getKeyPosition() != 2) {
            return;
        }
        popupMenu.setMenu(menuArrayList);
        //popupMenu.setBounds(215, 240, 291, 350);
        //!!!!!Bruce
        //popupMenu.setModal(true);
        popupMenu.setSelectMode(0);
        popupMenu.setVisible(true);
        if (!popupMenu.getSelectedMode()) {
            return;
        }
        super.actionPerformed(e);
        //firePOSButtonEvent(new POSButtonEvent(this));
    }

    public void dataOccurred(DataEvent e) {

        if (!POSTerminalApplication.getInstance().getChecked()
            && POSTerminalApplication.getInstance().getScanCashierNumber()) {
            return;
        }

        if (!StateMachine.getInstance().getPaymentMenuVisible() ||
                !(POSTerminalApplication.getInstance().getKeyPosition() == 2
                || POSTerminalApplication.getInstance().getKeyPosition() == 3)) {
            return;
        }

        try {
            POSKeyboard p = (POSKeyboard)e.getSource();
            if (getKeyCode() == p.getPOSKeyData()) {
                popupMenu = PopupMenuPane.getInstance();
                if (popupMenu.isVisible()) {
                    return;//
                }
                menuArrayList.clear();
                menuArrayList.addAll(allArrayList);
                popupMenu.setMenu(menuArrayList);
                popupMenu.centerPopupMenu();
                popupMenu.setSelectMode(0);
                //StateMachine.getInstance().setEventProcessEnabled(false);
                //popupMenu.setEventEnable(false);
                popupMenu.setVisible(true);
                if (!popupMenu.isVisible()) {
                    firePOSButtonEvent(new POSButtonEvent(this));
                } else {
                    popupMenu.addToList(this);
                }
            }
        } catch (JposException ex) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Jpos exception at " + this);
        }
    }

    public void doSomething() {
        if (popupMenu.getSelectedMode()) {
            firePOSButtonEvent(new POSButtonEvent(new PaymentButton(0,0,0,"", getPaymentID())));
        }
    }
}
