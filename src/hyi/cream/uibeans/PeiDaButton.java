// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   PeiDaButton.java

package hyi.cream.uibeans;


// Referenced classes of package hyi.cream.uibeans:
//            POSButton

public class PeiDaButton extends POSButton
{

    private String pluCode;

    public PeiDaButton(int row, int column, int level, String label)
    {
        super(row, column, level, label);
        pluCode = "";
    }

    public PeiDaButton(int row, int column, int level, String label, int keyCode)
    {
        super(row, column, level, label, keyCode);
        pluCode = "";
    }

    public PeiDaButton(int row, int column, int level, String label, int keyCode, String pluCode)
    {
        super(row, column, level, label, keyCode);
        this.pluCode = "";
        this.pluCode = pluCode;
    }

    public String getPluCode()
    {
        return pluCode;
    }
}
