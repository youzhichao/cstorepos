package hyi.cream.uibeans;

import java.awt.Color;
/**
 * 单品键.
 */
public class PluButton extends POSButton {
    private String pluCode     = "";
    private String pluCode1    = "";
    private String pluCode2    = "";
    private String pluCode3    = "";
    private String pluAttr1    = "";
    private String pluAttr2    = "";
	private String pluAttr3    = "";
	private Color pluColor;
    //private String keyCode;

    /**
     * Constructor.
     * @param row row position
     * @param column column position
     * @param pluLabel PLU label string on button.
     * @param pluCode PLU number.
     */
	public PluButton(int row, int column, int level, String pluLabel, String pluCode) {
		super(row, column, level, pluLabel);
        this.pluCode = pluCode;
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param pluLabel PLU label string on button.
     * @param pluCode PLU number.
     * @param pluAttr PLU attribute string.
     */
	public PluButton(int row, int column, int level, String pluLabel,
                     String pluCode, String pluAttr) {
        super(row, column, level, pluLabel);
        this.pluCode = pluCode;
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param pluLabel PLU label string on button.
     * @param pluCode1 first PLU number (default).
     * @param pluAttr1 first PLU attribute string.
     * @param pluCode2 second default PLU number.
     * @param pluAttr2 second PLU attribute string.
     */
	public PluButton(int row, int column, int level, String pluLabel,
                     String pluCode1, String pluAttr1,
                     String pluCode2, String pluAttr2) {
		super(row, column, level, pluLabel);
        this.pluCode1 = pluCode1;
        this.pluCode2 = pluCode2;
        this.pluAttr1 = pluAttr1;
        this.pluAttr2 = pluAttr2;
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param pluLabel PLU label string on button.
     * @param pluCode1 first PLU number (default).
     * @param pluAttr1 first PLU attribute string.
     * @param pluCode2 second PLU number.
     * @param pluAttr2 second PLU attribute string.
     * @param pluCode3 third PLU number.
     * @param pluAttr3 third PLU attribute string.
     */
	public PluButton(int row, int column, int level, String pluLabel,
                     String pluCode1, String pluAttr1, String pluCode2,
                     String pluAttr2, String pluCode3, String pluAttr3) {
		super(row, column, level, pluLabel);
        this.pluCode1 = pluCode1;
        this.pluCode2 = pluCode2;
        this.pluCode3 = pluCode3;
        this.pluAttr1 = pluAttr1;
        this.pluAttr2 = pluAttr2;
        this.pluAttr3 = pluAttr3;
    }

    /**
     * Constructor.
     * @param row row position
     * @param column column position
     * @param pluLabel PLU label string on button.
     * @param keyCode key code.
     * @param pluCode PLU number.
     */
    public PluButton(int row, int column, int level, String pluLabel, int keyCode,
                     String pluCode) {
		super(row, column, level, pluLabel, keyCode);
        this.pluCode = pluCode;
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param pluLabel PLU label string on button.
     * @param keyCode key code
     * @param pluCode PLU number.
     * @param pluAttr PLU attribute string.
     */
	public PluButton(int row, int column, int level, String pluLabel, int keyCode,
                     String pluCode, String pluAttr) {
        super(row, column, level, pluLabel, keyCode);
        this.pluCode = pluCode;
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param pluLabel PLU label string on button.
     * @param keyCode key code
     * @param pluCode1 first PLU number (default).
     * @param pluAttr1 first PLU attribute string.
     * @param pluCode2 second default PLU number.
     * @param pluAttr2 second PLU attribute string.
     */
	public PluButton(int row, int column, int level, String pluLabel, int keyCode,
                     String pluCode1, String pluAttr1,
                     String pluCode2, String pluAttr2) {
		super(row, column, level, pluLabel, keyCode);
        this.pluCode1 = pluCode1;
        this.pluCode2 = pluCode2;
        this.pluAttr1 = pluAttr1;
        this.pluAttr2 = pluAttr2;
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param pluLabel PLU label string on button.
     * @param keyCode key code
     * @param pluCode1 first PLU number (default).
     * @param pluAttr1 first PLU attribute string.
     * @param pluCode2 second PLU number.
     * @param pluAttr2 second PLU attribute string.
     * @param pluCode3 third PLU number.
     * @param pluAttr3 third PLU attribute string.
     */
	public PluButton(int row, int column, int level, String pluLabel, int keyCode,
                     String pluCode1, String pluAttr1, String pluCode2,
                     String pluAttr2, String pluCode3, String pluAttr3) {
        super(row, column, level, pluLabel, keyCode);
        this.pluCode1 = pluCode1;
        this.pluCode2 = pluCode2;
        this.pluCode3 = pluCode3;
        this.pluAttr1 = pluAttr1;
        this.pluAttr2 = pluAttr2;
        this.pluAttr3 = pluAttr3;
    }

    /**
     * Get default PLU code.
     *
     * @return PLU code.
     */
    public String getPluCode() {
        if (pluCode == "") {
            return pluCode1;
        } else {
            return pluCode;
        }
    }

    /**
     * Get PLU code by a PLU attribute string.
     *
     * @param pluAttr PLU attribute string
     *
     * @return PLU code.
     */
    public String getPluCode(String pluAttr) {
        if (pluAttr.equals(pluAttr1)) {
            return pluCode1;
        } else if (pluAttr.equals(pluAttr2)) {
            return pluCode2;
        } else if (pluAttr.equals(pluAttr3)) {
            return pluCode3;
        }
        return "";
	}

	public Color getPluColor() {
        return pluColor; 
	}

	public void setPluColor(Color c) {
        pluColor = c;
	}
}

 
