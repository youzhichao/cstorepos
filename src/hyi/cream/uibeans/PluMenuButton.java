// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   PluMenuButton.java

package hyi.cream.uibeans;

import hyi.cream.POSTerminalApplication;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.EventObject;
import jpos.JposException;
import jpos.POSKeyboard;
import jpos.events.DataEvent;

// Referenced classes of package hyi.cream.uibeans:
//            POSButton, PopupMenuListener, PopupMenuPane

/**
 * PLU Menu键
 */
public class PluMenuButton extends POSButton implements ActionListener, PopupMenuListener {

	private String defFilename;
	private PopupMenuPane popupMenu;
	private ArrayList menuArrayList;

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param pluMenuLabel PLU menu label on button.
     * @param defFilename definition file of the PLU menu.
     */
	public PluMenuButton(int row, int column, int level, String pluMenuLabel, String defFilename) {
		super(row, column, level, pluMenuLabel);
		popupMenu = PopupMenuPane.getInstance();
		menuArrayList = new ArrayList();
		this.defFilename = defFilename;
		setDefFile(defFilename);
	}

	public PluMenuButton(int row, int column, int level, String pluMenuLabel, int keyCode, String defFileName) {
		super(row, column, level, pluMenuLabel, keyCode);
		popupMenu = PopupMenuPane.getInstance();
		menuArrayList = new ArrayList();
		defFilename = defFileName;
		setDefFile(defFileName);
	}

	public PluMenuButton(int row, int column, int level, String pluMenuLabel, ArrayList menuArrayList) {
		super(row, column, level, pluMenuLabel);
		popupMenu = PopupMenuPane.getInstance();
		this.menuArrayList = new ArrayList();
		this.menuArrayList = menuArrayList;
	}

	public PluMenuButton(int row, int column, int level, String pluMenuLabel) {
		super(row, column, level, pluMenuLabel);
		popupMenu = PopupMenuPane.getInstance();
		menuArrayList = new ArrayList();
	}

	public String getPluNo() {
		return popupMenu.getPluNumber();
	}

	public void clear() {
		popupMenu.setIndex(-1);
	}

	public ArrayList getMenu() {
		return menuArrayList;
	}

	public void setPopMenu(PopupMenuPane p) {
		popupMenu = p;
	}

	public void setDefFile(String defFilename) {
		this.defFilename = defFilename;
		if (this.defFilename.indexOf(File.separator) == -1)
			this.defFilename = CreamToolkit.getConfigDir() + this.defFilename;
		File propFile = new File(this.defFilename);
		try {
			FileInputStream filein = new FileInputStream(propFile);
			InputStreamReader inst = null;
			if (CreamProperties.getInstance().getProperty("Locale").equals("zh_TW"))
				inst = new InputStreamReader(filein, "BIG5");
			else if (CreamProperties.getInstance().getProperty("Locale").equals("zh_CN"))
				inst = new InputStreamReader(filein, "GBK");
			BufferedReader in = new BufferedReader(inst);
			char ch = ' ';
			do {
				String line = in.readLine();
				if (line != null) {
					for (; line.equals(""); line = in.readLine())
						;
					int i = 0;
					do {
						ch = line.charAt(i);
						i++;
					} while ((ch == ' ' || ch == '\t') && i < line.length());
					if (ch == '#' || ch == ' ' || ch == '\t')
						continue;
				}
				if (line == null)
					break;
				menuArrayList.add(line);
			} while (true);
		} catch (FileNotFoundException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("File is not found: " + propFile + ", at " + this);
		} catch (IOException e) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("IO exception at " + this);
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (!POSTerminalApplication.getInstance().getEnabledPopupMenu() || POSTerminalApplication.getInstance().getKeyPosition() != 2)
			return;
		popupMenu.setMenu(menuArrayList);
		//popupMenu.setBounds(215, 240, 291, 350);
		// popupMenu.setModal(true);
		popupMenu.setSelectMode(0);
		popupMenu.setVisible(true);
		if (!popupMenu.getSelectedMode()) {
			return;
		} else {
			super.actionPerformed(e);
			//firePOSButtonEvent(new POSButtonEvent(this));
			return;
		}
	}

	public void dataOccurred(DataEvent e) {
		if (!POSTerminalApplication.getInstance().getChecked() && POSTerminalApplication.getInstance().getScanCashierNumber())
			return;
		if (!POSTerminalApplication.getInstance().getEnabledPopupMenu() || POSTerminalApplication.getInstance().getKeyPosition() != 2 && POSTerminalApplication.getInstance().getKeyPosition() != 3)
			return;
		try {
			POSKeyboard p = (POSKeyboard) e.getSource();
			if (getKeyCode() == p.getPOSKeyData()) {
				popupMenu = PopupMenuPane.getInstance();
				if (popupMenu.isVisible()) {// || !popupMenu.isEnabled() ) {
					return;//
				}
				popupMenu.setMenu(menuArrayList);
				popupMenu.centerPopupMenu();
				popupMenu.setSelectMode(0);
				popupMenu.setVisible(true);
				if (!popupMenu.isVisible())
					firePOSButtonEvent(new POSButtonEvent(this));
				else
					popupMenu.addToList(this);
			}
		} catch (JposException ex) {
			CreamToolkit.logMessage(e.toString());
			CreamToolkit.logMessage("Jpos exception at " + this);
		}
	}

	public void doSomething() {
		if (popupMenu.getSelectedMode())
			firePOSButtonEvent(new POSButtonEvent(this));
	}
}

	/*public void keyReleased(KeyEvent e) {
    }

	public void keyPressed(KeyEvent e) {
		char keyChar = e.getKeyChar();
		int prompt = (int)keyChar;
		System.out.println(e.getSource() + "++++");
	}

    public void keyTyped(KeyEvent e) {
		char keyChar = e.getKeyChar();
		int prompt = (int)keyChar;
		/*if (keyChar == ';' || keyChar == '%') {
            msr = true;
            return;
		}
		System.out.println(prompt +"|" + this);
	}*/
