package hyi.cream.uibeans;

/**
 * 增量键.   ②
 */
public class PlusButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
	public PlusButton(int row, int column, int level, String label) {
		super(row, column, level, label);
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param numberLabel a string represents the button label
     * @param keyCode key code
    */
	public PlusButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}

