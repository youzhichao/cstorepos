// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   PopupMenuPane.java

package hyi.cream.uibeans;

import hyi.cream.POSButtonHome;
import hyi.cream.POSTerminalApplication;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.state.StateMachine;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import jpos.Keylock;
import jpos.events.StatusUpdateEvent;

// Referenced classes of package hyi.cream.uibeans:
//            Indicator, NumberButton, PopupMenuListener, EPOSBackground, 
//            ClearButton

public class PopupMenuPane extends Window {
    private class FakeKeylock extends Keylock {

        int keyPos;

        public int getKeyPosition() {
            return keyPos;
        }

        FakeKeylock(int keyPos) {
            this.keyPos = keyPos;
        }
    }

    private static POSTerminalApplication app = POSTerminalApplication.getInstance();
    private static ArrayList selectMenu = new ArrayList();
    public static int SINGLE_SELECT = 0;
    public static int MULTIPLE_SELECT = 1;
    private int width;
    private int height;
    private ArrayList menuArrayList;
    private int selectMode;
    private boolean selectState;
    private boolean menuPageDown;
    private boolean menuPageUp;
    private boolean eventEnable;
    private boolean state;
    private int menuIndex;
    private Image offscreen;
    private Graphics og;
    private ActionListener actionListener;
    private String defFilename;
    private Frame owner;
    private int index;
    private Color itemColor;
    private ArrayList bufferedObjects;
    private boolean inputEnabled;
    private String message;
    private String warning;
    private String checkKey;
    static PopupMenuPane popupMenuPane = null;
    String itemNo;
    String fontName;
    Polygon nextPageHint;
    Polygon previousPageHint;
    //Image image = CreamToolkit.getImage("popupmenu.jpg");

    public static PopupMenuPane getInstance() {
        try {
            if (popupMenuPane == null)
                popupMenuPane = new PopupMenuPane(app);
        } catch (InstantiationException instantiationexception) {
        }
        return popupMenuPane;
    }

    public PopupMenuPane(Frame owner) throws InstantiationException {
        super(owner);
        menuArrayList = new ArrayList();
        selectState = false;
        menuPageDown = false;
        menuPageUp = false;
        eventEnable = true;
        state = false;
        menuIndex = 0;
        index = 0;
        itemColor = new Color(255, 255, 255);
        bufferedObjects = new ArrayList();
        inputEnabled = true;
        message = "";
        warning = "";
        checkKey = "ScanCashierNumber";
        itemNo = "";
        fontName = CreamProperties.getInstance().getProperty("PopupMenuPaneFont");
        nextPageHint = null;
        previousPageHint = null;
        // setTitle("ePOS Menu");
        menuArrayList = null;
        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                // 模拟按了[清除]再按[Keylock 2]
                StateMachine.getInstance().processEvent(new POSButtonEvent(new ClearButton(0, 0, 0, "Clear")));
                StateMachine.getInstance().processEvent(new StatusUpdateEvent(new FakeKeylock(2), 0));
            }

        });
    }

    public void setInputEnabled(boolean inputEnabled) {
        this.inputEnabled = inputEnabled;
    }

    public void clear() {
        itemNo = "";
    }

    public boolean keyDataListener(int prompt) {
        int pageUpCode = app.getPOSButtonHome().getPageUpCode();
        int pageDownCode = app.getPOSButtonHome().getPageDownCode();
        int clearCode = app.getPOSButtonHome().getClearCode();
        int enterCode = app.getPOSButtonHome().getEnterCode();
        ArrayList numberCodeArray = app.getPOSButtonHome().getNumberCode();
        ArrayList numberButtonArray = app.getPOSButtonHome().getNumberButton();
        // System.out.println("PopupM>> isSuspended=" +
        // StateMachine.getInstance().isSuspended());
        if (!inputEnabled && prompt != clearCode)
            return true;
        if (StateMachine.getInstance().getKeyWarning() || StateMachine.getInstance().isSuspended())
            return false;
        // check scanner cashier ID card
        if (!app.getChecked() && app.getScanCashierNumber())
            return false;
        if (message.equals(""))
            message = POSTerminalApplication.getInstance().getMessageIndicator().getMessage();
        if (prompt == pageUpCode) {
            menuPageUp = true;
            repaint();
            return true;
        }
        if (prompt == pageDownCode) {
            menuPageDown = true;
            repaint();
            return true;
        }
        if (numberCodeArray.contains(String.valueOf(prompt))) {
            itemNo += String.valueOf(((NumberButton) numberButtonArray.get(numberCodeArray.indexOf(String.valueOf(prompt)))).getNumberLabel());
            state = false;
            POSTerminalApplication.getInstance().getMessageIndicator().setMessage(itemNo);
            
            Iterator iter = ((ArrayList) bufferedObjects.clone()).iterator();
            while(iter.hasNext()){
            	PopupMenuListener pm = (PopupMenuListener) iter.next();
            	System.out.println("pm ="+pm.toString());
            	if( pm instanceof hyi.cream.state.PrintHisTransState ){
            		if (!CreamToolkit.checkInput(itemNo, 0)) {
            			itemNo = "";
            			POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
            			return true;
            		}
            		int menuIndex = Integer.parseInt(itemNo);
//            		itemNo = "";
//            		if (menuIndex > menuArrayList.size()) {
//            			app.getMessageIndicator().setMessage(message);
//            			itemNo = "";
//            			POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
//            			return true;
//            		}
            		setIndex(menuIndex - 1);
            		if (selectMode == SINGLE_SELECT) {
            			setVisible(false);
            			pm.doSomething();
            		}
            	}
            }
        } else {
            if (prompt == clearCode)
                if (itemNo.equals("")) {
                    setVisible(false);
                    message = "";
                    setIndex(-1);
                    Iterator iter = ((ArrayList) bufferedObjects.clone()).iterator();
                    bufferedObjects.clear();
                    for (; iter.hasNext(); ((PopupMenuListener) iter.next()).doSomething())
                        ;
                    return true;
                } else {
                    itemNo = "";
                    POSTerminalApplication.getInstance().getMessageIndicator().setMessage(message);
                    return true;
                }
            if (prompt == enterCode)
                if (!state) {
                    if (itemNo.equals("")) {
                        POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                        return true;
                    }
                    if (!CreamToolkit.checkInput(itemNo, 0)) {
                        itemNo = "";
                        POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                        return true;
                    }
                    int menuIndex = Integer.parseInt(itemNo);
                    itemNo = "";
                    if (menuIndex > menuArrayList.size()) {
                        app.getMessageIndicator().setMessage(message);
                        itemNo = "";
                        POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                        return true;
                    }
                    setIndex(menuIndex - 1);
                    if (selectMode == SINGLE_SELECT) {
                        setVisible(false);
                        Iterator iter = ((ArrayList) bufferedObjects.clone()).iterator();
                        bufferedObjects.clear();
                        for (; iter.hasNext(); ((PopupMenuListener) iter.next()).doSomething())
                            ;
                    } else if (selectMode == MULTIPLE_SELECT) {
                        if (selectMenu.contains(itemNo))
                            selectMenu.remove(menuIndex - 1);
                        else
                            selectMenu.add(String.valueOf(menuIndex - 1));
                        state = true;
                        repaint();
                    }
                } else {
                    state = false;
                    setVisible(false);
                    Iterator iter = ((ArrayList) bufferedObjects.clone()).iterator();
                    bufferedObjects.clear();
                    for (; iter.hasNext(); ((PopupMenuListener) iter.next()).doSomething())
                        ;
                    return true;
                }
        }
        // POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
        return true;
    }

    public void centerPopupMenu() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//        width = 350;
//        height = 350 + getInsets().top + getInsets().bottom;
        width = 400;
        height = 350 + getInsets().top + getInsets().bottom;
        setBounds((screenSize.width - width) / 2, (screenSize.height - height) / 2 + 50, width, height);
    }

    public void addToList(Object p) {
        bufferedObjects.clear();
        bufferedObjects.add(p);
    }

    public void setSelectMode(int selectMode) {
        this.selectMode = selectMode;
    }

    public void setMenu(ArrayList menu) {
        menuArrayList = menu;
        finished();
        repaint();
    }

    public void addMenu(Object menu) {
        menuArrayList.add(menu);
    }

    public boolean removeMenu(int ind, Object menu) {
        if (ind > menuArrayList.size() - 1) {
            return false;
        } else {
            menuArrayList.set(ind, menu);
            return true;
        }
    }

    public ArrayList getMenu() {
        return menuArrayList;
    }

    public void finished() {
        menuIndex = 0;
        menuPageDown = false;
        menuPageUp = false;
    }

    public Dimension getPreferredSize() {
        return new Dimension(500, 400);
    }

    public Dimension getMinimumSize() {
        return new Dimension(500, 400);
    }

    // public void invalidate() {
    // super.invalidate();
    // offscreen = null;
    //}

    public void update(Graphics g) {
        if (isShowing())
            paint(g);
    }

    public void paint(Graphics g) {
        if (offscreen == null)
            offscreen = createImage(getPreferredSize().width, getPreferredSize().height);
        og = offscreen.getGraphics();
        ((Graphics2D) og).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        //og.setClip(0, 0, getPreferredSize().width, getPreferredSize().height);

        /*MediaTracker tracker = new MediaTracker(this);
        tracker.addImage(image, 0);
        try {
            tracker.waitForID(0);
        } catch(InterruptedException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Interrupted exception at " + this);
        }*/
        int t = getInsets().top;
        int b = getInsets().bottom;
        //og.drawImage(image, 0, 0, this);
        og.setColor(EPOSBackground.getInstance().getColor4());
        og.fill3DRect(0, 0 + t, getSize().width, getSize().height - t - b, true);
        og.setColor(Color.black);
        og.fill3DRect(15, 15 + t, getSize().width - 33, getSize().height - 45 - t - b, false);
        og.setColor(Color.orange);
        og.draw3DRect(15, 15 + t, getSize().width - 33, getSize().height - 45 - t - b, false);
        //Bruce/20030318
        if (menuArrayList == null || menuArrayList.size() == 0) {
            g.drawImage(offscreen, 0, 0, this);
            og.dispose();
            return;
        }
        if (previousPageHint == null) {
            int mx = 225;
            int my = getHeight() - (t + b) - 25;
            int xx[] = { mx - 8, mx, mx + 8 };
            int yy[] = { my + 18 + t, my + t, my + 18 + t };
            previousPageHint = new Polygon(xx, yy, 3);
        }
        if (nextPageHint == null) {
            int mx = 255;
            int my = (getHeight() - (t + b) - 25) + 18;
            int xx[] = { mx - 8, mx, mx + 8 };
            int yy[] = { (my - 18) + t, my + t, (my - 18) + t };
            nextPageHint = new Polygon(xx, yy, 3);
        }
        Font f = new Font(fontName, 0, 18);
        FontMetrics fm = og.getFontMetrics(f);
        og.setFont(f);
        og.setColor(itemColor);
        if (menuPageDown && menuIndex + 10 < menuArrayList.size()) {
            menuIndex += 10;
            menuPageDown = false;
        } else {
            menuPageDown = false;
        }
        if (menuPageUp && menuIndex - 10 < menuArrayList.size() && menuIndex - 10 >= 0) {
            menuIndex -= 10;
            menuPageUp = false;
        } else {
            menuPageUp = false;
        }
        if (menuArrayList.size() > 10) {
            og.setColor(Color.darkGray);
            int currentPage = 1;
            if (menuIndex > 0)
                currentPage = (menuIndex + 10) / 10;
            int allPage = 1;
            if (menuArrayList.size() % 10 != 0)
                allPage = menuArrayList.size() / 10 + 1;
            else
                allPage = menuArrayList.size() / 10;
            if (currentPage != 1)
                og.fillPolygon(previousPageHint);
            if (currentPage != allPage)
                og.fillPolygon(nextPageHint);
        }
        int menusum;
        for (menusum = 0; menusum < 10 && menuIndex < menuArrayList.size(); menuIndex++) {
            boolean m = false;
            String str = menuArrayList.get(menuIndex).toString();
            if (selectMenu.contains(str))
                m = true;
            StringTokenizer tk = new StringTokenizer(str, ",");
            if (tk.countTokens() != 0)
                str = tk.nextToken();
            int strlength = fm.stringWidth(str);
            int stri = str.length();
            String printstr = str;
            while (strlength > width - 50) {
                printstr = printstr.substring(0, stri);
                strlength = fm.stringWidth(printstr);
                stri--;
            }
            //if (!printstr.equals(""))
            //  drawIt(og, 30, 26 + 32 * menusum - (28 - fm.getAscent()) / 2, m);

            if (m)
                og.setColor(Color.orange);
            else
                og.setColor(itemColor);
            og.drawString(printstr, 32, ((40 + 27 * menusum) - (28 - fm.getAscent()) / 2) + t);
            menusum++;
        }

        menuIndex -= menusum;
        g.drawImage(offscreen, 0, 0, this);
        og.dispose();
    }

    private void drawIt(Graphics g1, int i, int j, boolean flag) {
        /*Graphics2D g2 = (Graphics2D)g;
        double w = 190, h = 20, arcw = 4, arch = 4;
        //RoundRectangle2D.Double(double x, double y, double w, double h, double arcw, double arch)
        RoundRectangle2D r2D = new RoundRectangle2D.Double(x, y, w, h, arcw, arch);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        GradientPaint gp = new GradientPaint(new Point2D.Double(x, y - 30), Color.white, new Point2D.Double(x + 150, y + 50), new Color(0, 0, 200), true);
        if (m)
            g2.setPaint(Color.orange);
        else
            g2.setPaint(Color.cyan);
        BasicStroke sk = new BasicStroke(5,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND);
        g2.setStroke(sk);
        r2D.setFrame(x, y, w, h);
        g2.draw(r2D);
        g2.setPaint(new Color(2, 2, 2));
        r2D.setFrame(x + 1 , y , w + 1, h + 2);
        g2.fill(r2D);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);*/
    }

    public void addActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.add(actionListener, listener);
        enableEvents(16L);
    }

    public void removeActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.remove(actionListener, listener);
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getPluNumber() {
//      StringTokenizer t = new StringTokenizer(menuArrayList.get(index).toString(), ",.");
//      t.nextToken();
//      t.nextToken();
//      return t.nextToken();
    //最后一个逗号后面的部分为pluNumber  2003-08-21 ZhaoH
    //避免品名中含有'.'时取pluNumber出错。  
        String tmp = menuArrayList.get(index).toString();
        return tmp.substring(tmp.lastIndexOf(',') + 1);
    }

    public String getPluNumber(int index) {
//      StringTokenizer t = new StringTokenizer(menuArrayList.get(index).toString(), ",.");
//      t.nextToken();
//      t.nextToken();
//      return t.nextToken();
      //最后一个逗号后面的部分为pluNumber  2003-08-21 ZhaoH
      //避免品名中含有'.'时取pluNumber出错。
        String tmp = menuArrayList.get(index).toString();
        return tmp.substring(tmp.lastIndexOf(',') + 1);
    }

    public ArrayList getSelectMenu() {
        return selectMenu;
    }

    public boolean getSelectedState() {
        return selectState;
    }

    public String getSelectedDescription() {
//      StringTokenizer t = new StringTokenizer(menuArrayList.get(index).toString(), ",.");
//      t.nextToken();
//      return t.nextToken();
      //第1个'.'号和最后一个逗号之间的部分为pluDescription  2003-08-21 ZhaoH
      //避免品名中含有'.'时取pluDescription出错。
        String tmp = menuArrayList.get(index).toString();
        return tmp.substring(tmp.indexOf('.') + 1, tmp.lastIndexOf(','));
    }

    public int getSelectedNumber() {
        return index;
    }

    public boolean getSelectedMode() {
        return index != -1;
    }

    /*public void fireEvent(ActionEvent e) {
        Iterator iter = pl.iterator();
        while (iter.hasNext()) {
            POSButtonListener l = (POSButtonListener)iter.next();
            l.buttonPressed(e);
        }
    }*/
    
    public void processMouseEvent(MouseEvent e) {
        switch (e.getID()) {
        case 503:
        case 504:
        case 505:
        default:
            break;

        case 501:
            repaint();
            break;

        case 502:
            if (e.getX() > 235 && e.getX() < 275 && e.getY() > 10 && e.getY() < 70) {
                menuPageUp = true;
                repaint();
                break;
            }
            if (e.getX() > 230 && e.getX() < 285 && e.getY() > 90 && e.getY() < 140) {
                menuPageDown = true;
                repaint();
                break;
            }
            if (e.getX() > 235 && e.getX() < 275 && e.getY() > 290 && e.getY() < 330) {
                setVisible(false);
                setIndex(-1);
                actionListener.actionPerformed(new ActionEvent(this, 0, "close"));
                break;
            }
            if (e.getX() <= 23 || e.getX() >= 220)
                break;
            for (int i = 1; i <= menuArrayList.size() - menuIndex; i++) {
                if (e.getY() <= 22 + 32 * (i - 1) || e.getY() >= 38 + 32 * (i - 1))
                    continue;
                setIndex((menuIndex + i) - 1);
                if (selectMode == SINGLE_SELECT) {
                    setVisible(false);
                    break;
                }
                if (selectMode != MULTIPLE_SELECT)
                    continue;
                Object item = menuArrayList.get((menuIndex + i) - 1);
                if (selectMenu.contains(item))
                    selectMenu.remove(item);
                else
                    selectMenu.add(item);
                repaint();
                break;
            }

            actionListener.actionPerformed(new ActionEvent(this, 0, "select"));
            break;
        }
        super.processMouseEvent(e);
    }

    /**
     * 获得第index段文字，以,分段
     * @param part
     * @return
     */
    public String getSelectedPartText(int part) {
        
        String tmp = menuArrayList.get(index).toString();
        int count = 1;
        int point = 0;
        String ret = null;
        while ((point = tmp.indexOf(",")) >= 0) {
            tmp = tmp.substring(point + 1, tmp.length());
            count++;
            if (count == part)
                break;
        }
        
        if (count == part || count + 1 == part) {
            if (tmp.indexOf(",") >= 0)
                ret = tmp.substring(0, tmp.indexOf(','));
            else
                ret = tmp;
        }
        return ret;
    }
}
