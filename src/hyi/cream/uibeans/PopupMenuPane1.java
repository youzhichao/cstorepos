/**
 * author: gllg
 * Oct 21, 2008
 */
package hyi.cream.uibeans;

import hyi.cream.POSTerminalApplication;
import hyi.cream.SystemInfo;
import hyi.cream.dac.Transaction;
import hyi.cream.dac.Store;
import hyi.cream.dac.LineItem;
import hyi.cream.dac.PLU;
import hyi.cream.event.POSButtonEvent;
import hyi.cream.state.StateMachine;
import hyi.cream.util.CreamProperties;
import hyi.cream.util.CreamToolkit;
import hyi.cream.util.CreamPrinter;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.SimpleDateFormat;

import jpos.Keylock;
import jpos.events.StatusUpdateEvent;

// Referenced classes of package hyi.cream.uibeans:
//            Indicator, NumberButton, PopupMenuListener, EPOSBackground, 
//            ClearButton

public class PopupMenuPane1 extends Window {
    private static final int ENTRY_PER_PAGE = 1;

    private class FakeKeylock extends Keylock {

        int keyPos;

        public int getKeyPosition() {
            return keyPos;
        }

        FakeKeylock(int keyPos) {
            this.keyPos = keyPos;
        }
    }

    private static POSTerminalApplication app = POSTerminalApplication.getInstance();
    private static ArrayList selectMenu = new ArrayList();
    public static int SINGLE_SELECT = 0;
    public static int MULTIPLE_SELECT = 1;
//    public static int VERTICAL_SPACE = 27;
    private static int VERTICAL_SPACE = 18;
    private static int MAX_ENTRY = 16;
    private static int FIRST = -1;
    private static int MID = 0;
    private static int LAST = 1;


    private int width;
    private int height;
    private ArrayList menuArrayList;
    private ArrayList menuArrayList1;
    private int selectMode;
    private boolean selectState;
    private boolean menuPageDown;
    private boolean menuPageUp;
    private boolean eventEnable;
    private boolean state;
    private int menuIndex;
    private int menuIndex1;
    private Image offscreen;
    private Graphics og;
    private ActionListener actionListener;
    private String defFilename;
    private Frame owner;
    private int index;
    private Color itemColor;
    private ArrayList bufferedObjects;
    private boolean inputEnabled;
    private String message;
    private String warning;
    private String checkKey;
    static PopupMenuPane1 popupMenuPane1 = null;
    String itemNo;
    String fontName;
    Polygon nextPageHint;
    Polygon previousPageHint;
    private int spillState = -2;

    private int pageUpCode;
    private int pageDownCode;
    private int clearCode;
    private int enterCode;
    private int keyCode;
    private int firstPageCode;
    private int lastPageCode;
    private int cTranNo;

    public int getCurrTranNo() {
        return cTranNo;
    }

    public void setCurrTranNo(int cTranNo) {
        this.cTranNo = cTranNo;
    }

    private ResourceBundle res = CreamToolkit.GetResource();
    //Image image = CreamToolkit.getImage("popupmenu.jpg");

    public static PopupMenuPane1 getInstance() {
        try {
            if (popupMenuPane1 == null)
                popupMenuPane1 = new PopupMenuPane1(app);
        } catch (InstantiationException instantiationexception) {
        }
        return popupMenuPane1;
    }

    public PopupMenuPane1(Frame owner) throws InstantiationException {
        super(owner);
        menuArrayList = new ArrayList();
        menuArrayList1 = new ArrayList();
        selectState = false;
        menuPageDown = false;
        menuPageUp = false;
        eventEnable = true;
        state = false;
        menuIndex = 0;
        menuIndex1 = 0;
        index = 0;
        itemColor = new Color(255, 255, 255);
        bufferedObjects = new ArrayList();
        inputEnabled = true;
        message = "";
        warning = "";
        checkKey = "ScanCashierNumber";
        itemNo = "";
        fontName = CreamProperties.getInstance().getProperty("PopupMenuPaneFont");
        nextPageHint = null;
        previousPageHint = null;
        // setTitle("ePOS Menu");
        menuArrayList = null;
        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                // 模拟按了[清除]再按[Keylock 2]
                StateMachine.getInstance().processEvent(new POSButtonEvent(new ClearButton(0, 0, 0, "Clear")));
                StateMachine.getInstance().processEvent(new StatusUpdateEvent(new FakeKeylock(2), 0));
            }

        });
    }

    public void setInputEnabled(boolean inputEnabled) {
        this.inputEnabled = inputEnabled;
    }

    public void clear() {
        itemNo = "";
    }

    public boolean keyDataListener(int prompt) {
        this.keyCode = prompt;
        pageUpCode = app.getPOSButtonHome().getPageUpCode();
        pageDownCode = app.getPOSButtonHome().getPageDownCode();
        clearCode = app.getPOSButtonHome().getClearCode();
        enterCode = app.getPOSButtonHome().getEnterCode();
        firstPageCode = app.getPOSButtonHome().getFirstPageCode();
        lastPageCode = app.getPOSButtonHome().getLastPageCode();
        ArrayList numberCodeArray = app.getPOSButtonHome().getNumberCode();
        ArrayList numberButtonArray = app.getPOSButtonHome().getNumberButton();
        // System.out.println("PopupM>> isSuspended=" +
        // StateMachine.getInstance().isSuspended());
        if (!inputEnabled && prompt != clearCode)
            return true;
        if (StateMachine.getInstance().getKeyWarning() || StateMachine.getInstance().isSuspended())
            return false;
        // check scanner cashier ID card
        if (!app.getChecked() && app.getScanCashierNumber())
            return false;
        if (message.equals(""))
            message = POSTerminalApplication.getInstance().getMessageIndicator().getMessage();
        if (prompt == pageUpCode) {
            menuPageUp = true;
            repaint();
            return true;
        }
        if (prompt == pageDownCode) {
            menuPageDown = true;
            repaint();
            return true;
        }
        if (prompt == firstPageCode) {
            repaint();
            return true;
        }
        if (prompt == lastPageCode) {
            repaint();
            return true;
        }
        if (numberCodeArray.contains(String.valueOf(prompt))) {
            itemNo += String.valueOf(((NumberButton) numberButtonArray.get(numberCodeArray.indexOf(String.valueOf(prompt)))).getNumberLabel());
            state = false;
            POSTerminalApplication.getInstance().getMessageIndicator().setMessage(itemNo);
        } else {
            if (prompt == clearCode)
                if (itemNo.equals("")) {
                    setVisible(false);
                    message = "";
                    setIndex(-1);
                    Iterator iter = ((ArrayList) bufferedObjects.clone()).iterator();
                    bufferedObjects.clear();
                    for (; iter.hasNext(); ((PopupMenuListener) iter.next()).doSomething())
                        ;
                    return true;
                } else {
                    itemNo = "";
                    POSTerminalApplication.getInstance().getMessageIndicator().setMessage(message);
                    return true;
                }

            //--------------------------------
            if (prompt == enterCode) {
//                state = false;
//                setVisible(false);
//                Iterator iter = ((ArrayList) bufferedObjects.clone()).iterator();
//                bufferedObjects.clear();
//                if (iter.hasNext()) {
//                    setIndex(0);
//                    ((PopupMenuListener) iter.next()).doSomething();
//                }
                this.printTrans();
                return true;
            }
            
            //--------------------------------

            if (prompt == enterCode)
                if (!state) {
                    if (itemNo.equals("")) {
                        POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                        return true;
                    }
                    if (!CreamToolkit.checkInput(itemNo, 0)) {
                        itemNo = "";
                        POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                        return true;
                    }
                    int menuIndex = Integer.parseInt(itemNo);
                    itemNo = "";
                    if (menuIndex > menuArrayList.size()) {
                        app.getMessageIndicator().setMessage(message);
                        itemNo = "";
                        POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
                        return true;
                    }
                    setIndex(menuIndex - 1);
                    if (selectMode == SINGLE_SELECT) {
                        setVisible(false);
                        Iterator iter = ((ArrayList) bufferedObjects.clone()).iterator();
                        bufferedObjects.clear();
                        for (; iter.hasNext(); ((PopupMenuListener) iter.next()).doSomething())
                            ;
                    } else if (selectMode == MULTIPLE_SELECT) {
                        if (selectMenu.contains(itemNo))
                            selectMenu.remove(menuIndex - 1);
                        else
                            selectMenu.add(String.valueOf(menuIndex - 1));
                        state = true;
                        repaint();
                    }
                } else {
                    state = false;
                    setVisible(false);
                    Iterator iter = ((ArrayList) bufferedObjects.clone()).iterator();
                    bufferedObjects.clear();
                    for (; iter.hasNext(); ((PopupMenuListener) iter.next()).doSomething())
                        ;
                    return true;
                }
        }
        // POSTerminalApplication.getInstance().getMessageIndicator().setMessage(CreamToolkit.GetResource().getString("InputWrong"));
        return true;
    }

    public void centerPopupMenu() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        width = 350;
        height = 350 + getInsets().top + getInsets().bottom;
        setBounds((screenSize.width - width) / 2, (screenSize.height - height) / 2 + 50, width, height);
    }

    public void addToList(Object p) {
        bufferedObjects.clear();
        bufferedObjects.add(p);
    }

    public void setSelectMode(int selectMode) {
        this.selectMode = selectMode;
    }

    public void setMenu(ArrayList menu) {
        menuArrayList = menu;
        finished();
        repaint();
    }

    public void addMenu(Object menu) {
        menuArrayList.add(menu);
    }

    public boolean removeMenu(int ind, Object menu) {
        if (ind > menuArrayList.size() - 1) {
            return false;
        } else {
            menuArrayList.set(ind, menu);
            return true;
        }
    }

    public ArrayList getMenu() {
        return menuArrayList;
    }

    public void finished() {
        menuIndex = 0;
        menuIndex1 = 0;
        menuPageDown = false;
        menuPageUp = false;
    }

    public Dimension getPreferredSize() {
        return new Dimension(500, 400);
    }

    public Dimension getMinimumSize() {
        return new Dimension(500, 400);
    }

    // public void invalidate() {
    // super.invalidate();
    // offscreen = null;
    //}

    public void update(Graphics g) {
        if (isShowing())
            paint(g);
    }

    public void paint(Graphics g) {
        //----------------------------------------------
        if ((keyCode != 0 && spillState == LAST && keyCode == pageDownCode && menuIndex == menuArrayList.size() - 1)
                || (keyCode != 0 && spillState == FIRST && keyCode == pageUpCode && menuIndex == 0)) {
            return;
        }
        if (keyCode == 0 && menuIndex == 0) {
            menuIndex1 = 0;
            spillState = -2;
        }

        if (keyCode == pageDownCode && menuIndex == 0 && menuIndex1 == 0 && spillState != -2) {
            menuIndex1 = MAX_ENTRY;
            spillState = MID;
        }
        
        //----------------------------------------------


        if (offscreen == null)
            offscreen = createImage(getPreferredSize().width, getPreferredSize().height);
        og = offscreen.getGraphics();
        ((Graphics2D) og).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        //og.setClip(0, 0, getPreferredSize().width, getPreferredSize().height);

        /*MediaTracker tracker = new MediaTracker(this);
        tracker.addImage(image, 0);
        try {
            tracker.waitForID(0);
        } catch(InterruptedException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Interrupted exception at " + this);
        }*/
        int t = getInsets().top;
        int b = getInsets().bottom;
        //og.drawImage(image, 0, 0, this);
        og.setColor(EPOSBackground.getInstance().getColor4());
        og.fill3DRect(0, 0 + t, getSize().width, getSize().height - t - b, true);
        og.setColor(Color.black);
        og.fill3DRect(15, 15 + t, getSize().width - 33, getSize().height - 45 - t - b, false);
        og.setColor(Color.orange);
        og.draw3DRect(15, 15 + t, getSize().width - 33, getSize().height - 45 - t - b, false);
       
        if (menuArrayList == null || menuArrayList.size() == 0) {
            g.drawImage(offscreen, 0, 0, this);
            og.dispose();
            return;
        }
        if (previousPageHint == null) {
            int mx = 225;
            int my = getHeight() - (t + b) - 25;
            int xx[] = { mx - 8, mx, mx + 8 };
            int yy[] = { my + 18 + t, my + t, my + 18 + t };
            previousPageHint = new Polygon(xx, yy, 3);
        }
        if (nextPageHint == null) {
            int mx = 255;
            int my = (getHeight() - (t + b) - 25) + 18;
            int xx[] = { mx - 8, mx, mx + 8 };
            int yy[] = { (my - 18) + t, my + t, (my - 18) + t };
            nextPageHint = new Polygon(xx, yy, 3);
        }
//        Font f = new Font(fontName, 0, 18);
        Font f = new Font(fontName, 0, 15);
        FontMetrics fm = og.getFontMetrics(f);
        og.setFont(f);
        og.setColor(itemColor);
        if (menuPageDown && menuIndex + ENTRY_PER_PAGE < menuArrayList.size()) {
            menuIndex += ENTRY_PER_PAGE;
            menuPageDown = false;
        } else {
            menuPageDown = false;
        }
        if (menuPageUp && menuIndex - ENTRY_PER_PAGE < menuArrayList.size() && menuIndex - ENTRY_PER_PAGE >= 0) {
            menuIndex -= ENTRY_PER_PAGE;
            menuPageUp = false;
        } else {
            menuPageUp = false;
        }

        //--------------------------------------
        if (keyCode != 0 && keyCode == firstPageCode) {
            menuIndex = 0;
            menuPageDown = true;
            menuPageUp = false;
            menuIndex1 = 0;
            spillState = -2;

        } else if (keyCode != 0 && keyCode == lastPageCode) {
            menuIndex = menuArrayList.size() - 1;
            menuPageDown = false;
            menuPageUp = true;
            menuIndex1 = 0;
            spillState = -2;
        }
        //--------------------------------------

        if (menuArrayList.size() > ENTRY_PER_PAGE) {
            og.setColor(Color.darkGray);
            int currentPage = 1;
            if (menuIndex > 0)
                currentPage = (menuIndex + ENTRY_PER_PAGE) / ENTRY_PER_PAGE;
            int allPage = 1;
            if (menuArrayList.size() % ENTRY_PER_PAGE != 0)
                allPage = menuArrayList.size() / ENTRY_PER_PAGE + 1;
            else
                allPage = menuArrayList.size() / ENTRY_PER_PAGE;
            if (currentPage != 1)
                og.fillPolygon(previousPageHint);
            if (currentPage != allPage)
                og.fillPolygon(nextPageHint);
            //-----------------------

            

            //-----------------------
        }
        int menusum;
        for (menusum = 0; menusum < ENTRY_PER_PAGE && menuIndex < menuArrayList.size(); /*menuIndex++*/) {
            boolean m = false;
            if (pageUpCode == keyCode) {
                if (menuIndex < menuArrayList.size() - 1 && (spillState == MID || spillState == LAST))
                    menuIndex++;
            }
            Transaction currTran = (Transaction) (((Object[])(menuArrayList.get(menuIndex)))[1]);
            setCurrTranNo(currTran.getTransactionNumber().intValue());
            String str = (((Object[])(menuArrayList.get(menuIndex)))[0]).toString();
            if (selectMenu.contains(str))
                m = true;
            StringTokenizer tk = new StringTokenizer(str, ",");
            if (tk.countTokens() != 0)
                str = tk.nextToken();
            int strlength = fm.stringWidth(str);
            int stri = str.length();
            String printstr = str;
            while (strlength > width - 50) {
                printstr = printstr.substring(0, stri);
                strlength = fm.stringWidth(printstr);
                stri--;
            }
            //if (!printstr.equals(""))
            //  drawIt(og, 30, 26 + 32 * menusum - (28 - fm.getAscent()) / 2, m);

            if (m)
                og.setColor(Color.orange);
            else
                og.setColor(itemColor);
            
            getDealTypeMap().get("000");

            //---------------------------------------------------
            menuArrayList1.clear();
            menuArrayList1.add(new Object[]{printstr, new Integer(32), new Integer(((40 + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            String tranType = "";
            if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("0") && currTran.getDealType3().equalsIgnoreCase("0"))
                tranType = res.getString("normalTran");
            else if (currTran.getDealType1().equalsIgnoreCase("*") && currTran.getDealType2().equalsIgnoreCase("0") && currTran.getDealType3().equalsIgnoreCase("0"))
                tranType = res.getString("beVoidTran");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("4") && currTran.getDealType3().equalsIgnoreCase("0"))
                tranType = res.getString("paidInOut");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("8") && currTran.getDealType3().equalsIgnoreCase("0"))
                tranType = res.getString("Sign-on");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("9") && currTran.getDealType3().equalsIgnoreCase("0"))
                tranType = res.getString("Sign-off");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("B") && currTran.getDealType3().equalsIgnoreCase("0"))
                tranType = res.getString("onDuty");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("C") && currTran.getDealType3().equalsIgnoreCase("0"))
                tranType = res.getString("offDuty");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("D") && currTran.getDealType3().equalsIgnoreCase("0"))
                tranType = res.getString("endOfDate");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("G") && currTran.getDealType3().equalsIgnoreCase("0"))
                tranType = res.getString("CashOut");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("H") && currTran.getDealType3().equalsIgnoreCase("0"))
                tranType = res.getString("CashIn");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("0") && currTran.getDealType3().equalsIgnoreCase("1"))
                tranType = res.getString("previousNegative");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("0") && currTran.getDealType3().equalsIgnoreCase("2"))
                tranType = res.getString("ReprintNegative");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("0") && currTran.getDealType3().equalsIgnoreCase("3"))
                tranType = res.getString("CancelNegative");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("3") && currTran.getDealType3().equalsIgnoreCase("4"))
                tranType = res.getString("refundNegative");
            else if (currTran.getDealType1().equalsIgnoreCase("0") && currTran.getDealType2().equalsIgnoreCase("0") && currTran.getDealType3().equalsIgnoreCase("4"))
                tranType = res.getString("partRefundPositive");


            menuArrayList1.add(new Object[]{currTran.getStoreName() + "   " + res.getString("typeLabel") + tranType, new Integer(32), new Integer(((40 + 1 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            menuArrayList1.add(new Object[]{res.getString("cashier1") + currTran.getCashierName() + "   " + res.getString("tranNoLabel") + currTran.getTransactionNumber(), new Integer(32), new Integer(((40 + 2 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            menuArrayList1.add(new Object[]{res.getString("tel1") + Store.getTelephoneNumber(), new Integer(32), new Integer(((40 + 3 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            menuArrayList1.add(new Object[]{res.getString("address1") + Store.getAddress(), new Integer(32), new Integer(((40 + 4 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            menuArrayList1.add(new Object[]{"", new Integer(32), new Integer(((40 + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            Object[] objs = currTran.getLineItemsArrayLast();
//            currTran.getLineItems();

            for (int i = 0; i < objs.length; i++) {
                LineItem lineItem = (LineItem) objs[i];
                menuArrayList1.add(new Object[]{lineItem.getDescription() + " " + lineItem.getUnitPrice() + (lineItem.getQuantity().intValue() == 1 ? "" : " x " + lineItem.getQuantity().intValue()), new Integer(32), new Integer(((40 + (i + 1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            }
            menuArrayList1.add(new Object[]{"", new Integer(32), new Integer(((40 + (objs.length +1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            int tmp1 = objs.length +1;
            if (currTran.getDaiShouAmount().doubleValue() != 0)
                menuArrayList1.add(new Object[]{res.getString("daishou1") + currTran.getDaiShouAmount(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            if (currTran.getDaiShouAmount2().doubleValue() != 0)
                menuArrayList1.add(new Object[]{res.getString("daishou21") + currTran.getDaiShouAmount2(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            if (currTran.getGrossSalesAmtWithDsct().doubleValue() != 0)
                menuArrayList1.add(new Object[]{res.getString("saleTot") + currTran.getGrossSalesAmtWithDsct(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            if (currTran.getTotalMMAmount().doubleValue() != 0)
                menuArrayList1.add(new Object[]{res.getString("mnmTot") + currTran.getTotalMMAmount(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            if (currTran.getSIAmtList() != null && currTran.getSIAmtList().size() != 0)
                menuArrayList1.add(new Object[]{res.getString("dsctType") + currTran.getSIDescription(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            if (currTran.getSalesAmtWithItemDsct().doubleValue() != 0)
                menuArrayList1.add(new Object[]{res.getString("total3") + currTran.getSalesAmtWithItemDsct(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            if (currTran.getPayName1() != null)
                menuArrayList1.add(new Object[]{currTran.getPayName1() + ":" + currTran.getPayAmount1(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            if (currTran.getPayAmount2().doubleValue() != 0)
                menuArrayList1.add(new Object[]{currTran.getPayName2() + ":" + currTran.getPayAmount2(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            if (currTran.getPayAmount3().doubleValue() != 0)
                menuArrayList1.add(new Object[]{currTran.getPayName3() + ":" + currTran.getPayAmount3(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            if (currTran.getPayAmount4().doubleValue() != 0)
                menuArrayList1.add(new Object[]{currTran.getPayName4() + ":" + currTran.getPayAmount4(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            if (currTran.getChangeAmount().doubleValue() != 0)
                menuArrayList1.add(new Object[]{res.getString("change1") + currTran.getChangeAmount(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});
            if (currTran.getSpillAmount().doubleValue() != 0)
                menuArrayList1.add(new Object[]{res.getString("over1") + currTran.getSpillAmount(), new Integer(32), new Integer(((40 + (++tmp1) * VERTICAL_SPACE + 5 * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t)});


            int allEntry = menuArrayList1.size();

            int offset = 0, j = 0;
            if (allEntry <= MAX_ENTRY) {
                menuIndex1 = 0;
                for (int i = menuIndex1; i < allEntry; i++) {
                    offset = ((40 + j * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t;
                    og.drawString((String) (((Object[]) (menuArrayList1.get(i)))[0]), ((Integer) (((Object[]) (menuArrayList1.get(i)))[1])).intValue(), offset);
                    j++;
                }
                menuIndex1 = 0;
                spillState = -2;

            } else {
//                if (menuIndex1 == 0) {
                if (spillState == -2)
                    spillState = FIRST;
                if (spillState == FIRST) {
                    for (int i = menuIndex1; i < MAX_ENTRY; i++) {
                        offset = ((40 + j * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t;
                        og.drawString((String) (((Object[]) (menuArrayList1.get(i)))[0]), ((Integer) (((Object[]) (menuArrayList1.get(i)))[1])).intValue(), offset);
                        j++;
                    }
                    menuIndex1 += MAX_ENTRY;
                    if (menuIndex1 < allEntry) {
                        og.setColor(Color.darkGray);
//                        og.fillPolygon(previousPageHint);
                        og.fillPolygon(nextPageHint);
                        og.setColor(itemColor);
                        spillState = MID;
                        if (menuIndex > 0) {
                            menuIndex--;
                        }
                        break;
                    }
                    else {
                        og.setColor(Color.darkGray);
                        og.fillPolygon(previousPageHint);
                        og.setColor(itemColor);
                        spillState = LAST;
                    }


                } else {
                    if (spillState == MID) {
                        if (keyCode == pageUpCode) {
                            if (menuIndex1 > MAX_ENTRY )
                                menuIndex1 -= MAX_ENTRY;
                            for (int i = menuIndex1 - MAX_ENTRY; i < menuIndex1; i++) {
                                offset = ((40 + j * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t;
                                og.drawString((String) (((Object[]) (menuArrayList1.get(i)))[0]), ((Integer) (((Object[]) (menuArrayList1.get(i)))[1])).intValue(), offset);
                                j++;
                            }
                            if (menuIndex1 - MAX_ENTRY == 0) {
                                menuIndex1 = 0;
                                og.setColor(Color.darkGray);
                                og.fillPolygon(nextPageHint);
                                og.setColor(itemColor);
                                spillState = FIRST;
                            } else {
                                menuIndex1 -= MAX_ENTRY;
                                og.setColor(Color.darkGray);
                                og.fillPolygon(previousPageHint);
                                og.fillPolygon(nextPageHint);
                                og.setColor(itemColor);
                                spillState = MID;
                                if (menuIndex < allEntry - 1)
                                   menuIndex++;
                                break;
                            }
                        } else if (keyCode == pageDownCode) {
                            for (int i = menuIndex1; i < (menuIndex1 + MAX_ENTRY < allEntry ? menuIndex1 + MAX_ENTRY : allEntry); i++) {
                                offset = ((40 + j * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t;
                                og.drawString((String) (((Object[]) (menuArrayList1.get(i)))[0]), ((Integer) (((Object[]) (menuArrayList1.get(i)))[1])).intValue(), offset);
                                j++;
                            }
                            if (menuIndex1 + MAX_ENTRY >= allEntry) {
                                menuIndex1 = (allEntry / MAX_ENTRY) * MAX_ENTRY;
                                og.setColor(Color.darkGray);
                                og.fillPolygon(previousPageHint);
                                og.setColor(itemColor);
                                spillState = LAST;
                            } else {
                                menuIndex1 += MAX_ENTRY;
                                og.setColor(Color.darkGray);
                                og.fillPolygon(previousPageHint);
                                og.fillPolygon(nextPageHint);
                                og.setColor(itemColor);
                                spillState = MID;
                                if (menuIndex > 0)
                                    menuIndex--;
                                break;
                            }
                        }

                    } else if (spillState == LAST) {
                        if (menuArrayList1.size() - menuIndex1 > MAX_ENTRY)
                            menuIndex1 -= MAX_ENTRY;
                        for (int i = menuIndex1 - MAX_ENTRY; i < menuIndex1; i++) {
                            offset = ((40 + j * VERTICAL_SPACE + VERTICAL_SPACE * menusum) - (28 - fm.getAscent()) / 2) + t;
                            og.drawString((String) (((Object[]) (menuArrayList1.get(i)))[0]), ((Integer) (((Object[]) (menuArrayList1.get(i)))[1])).intValue(), offset);
                            j++;
                        }
                        if (menuIndex1 - MAX_ENTRY == 0) {
                            menuIndex1 = 0;
                            og.setColor(Color.darkGray);
                            og.fillPolygon(nextPageHint);
                            og.setColor(itemColor);
                            spillState = FIRST;
                        } else {
//                            menuIndex1 -= MAX_ENTRY;
                            og.setColor(Color.darkGray);
                            og.fillPolygon(previousPageHint);
                            og.fillPolygon(nextPageHint);
                            og.setColor(itemColor);
                            spillState = MID;
//                            if (menuIndex < allEntry - 1)
//                                menuIndex++;
                            break;
                        }
                    }


                }


            }



            //---------------------------------------------------
            menusum++;

            menuIndex++;
            break;
            
        }

        menuIndex -= menusum;
        
        g.drawImage(offscreen, 0, 0, this);
        og.dispose();
    }

    HashMap dealTypeMap;
    private HashMap getDealTypeMap() {
		if (dealTypeMap == null){
			dealTypeMap = new HashMap();
		//			dealTypeMap.put(arg0, arg1)
		//			......
    	}
    	
    	return dealTypeMap;
	}

	private void drawIt(Graphics g1, int i, int j, boolean flag) {
        /*Graphics2D g2 = (Graphics2D)g;
        double w = 190, h = 20, arcw = 4, arch = 4;
        //RoundRectangle2D.Double(double x, double y, double w, double h, double arcw, double arch)
        RoundRectangle2D r2D = new RoundRectangle2D.Double(x, y, w, h, arcw, arch);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        GradientPaint gp = new GradientPaint(new Point2D.Double(x, y - 30), Color.white, new Point2D.Double(x + 150, y + 50), new Color(0, 0, 200), true);
        if (m)
            g2.setPaint(Color.orange);
        else
            g2.setPaint(Color.cyan);
        BasicStroke sk = new BasicStroke(5,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND);
        g2.setStroke(sk);
        r2D.setFrame(x, y, w, h);
        g2.draw(r2D);
        g2.setPaint(new Color(2, 2, 2));
        r2D.setFrame(x + 1 , y , w + 1, h + 2);
        g2.fill(r2D);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);*/
    }

    public void addActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.add(actionListener, listener);
        enableEvents(16L);
    }

    public void removeActionListener(ActionListener listener) {
        actionListener = AWTEventMulticaster.remove(actionListener, listener);
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getPluNumber() {
//      StringTokenizer t = new StringTokenizer(menuArrayList.get(index).toString(), ",.");
//      t.nextToken();
//      t.nextToken();
//      return t.nextToken();
    //最后一个逗号后面的部分为pluNumber  2003-08-21 ZhaoH
    //避免品名中含有'.'时取pluNumber出错。
        String tmp = menuArrayList.get(index).toString();
        return tmp.substring(tmp.lastIndexOf(',') + 1);
    }

    public String getPluNumber(int index) {
//      StringTokenizer t = new StringTokenizer(menuArrayList.get(index).toString(), ",.");
//      t.nextToken();
//      t.nextToken();
//      return t.nextToken();
      //最后一个逗号后面的部分为pluNumber  2003-08-21 ZhaoH
      //避免品名中含有'.'时取pluNumber出错。
        String tmp = menuArrayList.get(index).toString();
        return tmp.substring(tmp.lastIndexOf(',') + 1);
    }

    public ArrayList getSelectMenu() {
        return selectMenu;
    }

    public boolean getSelectedState() {
        return selectState;
    }

    public String getSelectedDescription() {
//      StringTokenizer t = new StringTokenizer(menuArrayList.get(index).toString(), ",.");
//      t.nextToken();
//      return t.nextToken();
      //第1个'.'号和最后一个逗号之间的部分为pluDescription  2003-08-21 ZhaoH
      //避免品名中含有'.'时取pluDescription出错。
        String tmp = menuArrayList.get(index).toString();
        return tmp.substring(tmp.indexOf('.') + 1, tmp.lastIndexOf(','));
    }

    public int getSelectedNumber() {
        return index;
    }

    public boolean getSelectedMode() {
        return index != -1;
    }

    /*public void fireEvent(ActionEvent e) {
        Iterator iter = pl.iterator();
        while (iter.hasNext()) {
            POSButtonListener l = (POSButtonListener)iter.next();
            l.buttonPressed(e);
        }
    }*/

    public void processMouseEvent(MouseEvent e) {
        switch (e.getID()) {
        case 503:
        case 504:
        case 505:
        default:
            break;

        case 501:
            repaint();
            break;

        case 502:
            if (e.getX() > 235 && e.getX() < 275 && e.getY() > ENTRY_PER_PAGE && e.getY() < 70) {
                menuPageUp = true;
                repaint();
                break;
            }
            if (e.getX() > 230 && e.getX() < 285 && e.getY() > 90 && e.getY() < 140) {
                menuPageDown = true;
                repaint();
                break;
            }
            if (e.getX() > 235 && e.getX() < 275 && e.getY() > 290 && e.getY() < 330) {
                setVisible(false);
                setIndex(-1);
                actionListener.actionPerformed(new ActionEvent(this, 0, "close"));
                break;
            }
            if (e.getX() <= 23 || e.getX() >= 220)
                break;
            for (int i = 1; i <= menuArrayList.size() - menuIndex; i++) {
                if (e.getY() <= 22 + 32 * (i - 1) || e.getY() >= 38 + 32 * (i - 1))
                    continue;
                setIndex((menuIndex + i) - 1);
                if (selectMode == SINGLE_SELECT) {
                    setVisible(false);
                    break;
                }
                if (selectMode != MULTIPLE_SELECT)
                    continue;
                Object item = menuArrayList.get((menuIndex + i) - 1);
                if (selectMenu.contains(item))
                    selectMenu.remove(item);
                else
                    selectMenu.add(item);
                repaint();
                break;
            }

            actionListener.actionPerformed(new ActionEvent(this, 0, "select"));
            break;
        }
        super.processMouseEvent(e);
    }

    /**
     * 获得第index段文字，以,分段
     * @param part
     * @return
     */
    public String getSelectedPartText(int part) {

        String tmp = (((Object[])(menuArrayList.get(index)))[0]).toString();
        int count = 1;
        int point = 0;
        String ret = null;
        while ((point = tmp.indexOf(",")) >= 0) {
            tmp = tmp.substring(point + 1, tmp.length());
            count++;
            if (count == part)
                break;
        }

        if (count == part || count + 1 == part) {
            if (tmp.indexOf(",") >= 0)
                ret = tmp.substring(0, tmp.indexOf(','));
            else
                ret = tmp;
        }
        return ret;
    }

    private void printTrans() {
        int i = this.getCurrTranNo();
        app.getMessageIndicator().setMessage(res.getString("PrintTran") + String.valueOf(i));
        boolean isPageCut = true;
        try {
            CreamPrinter printer = CreamPrinter.getInstance();
            boolean originalPrinterEnableState = printer.getPrintEnabled();
            printer.setPrintEnabled(true);
            printer.reprint(i, isPageCut, true);
            printer.setPrintEnabled(originalPrinterEnableState);

        } catch (Exception e) {
        }
        app.getMessageIndicator().setMessage(res.getString("printTranHint"));

    }
}