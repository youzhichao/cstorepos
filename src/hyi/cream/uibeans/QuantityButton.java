
package hyi.cream.uibeans;

/**
 * 数量键.
 */
public class QuantityButton extends POSButton {

    /**
     * Constructor.
     * @param row row position.
     * @param column column position.
     * @param quantityLabel quantity label on button.
     */
    public QuantityButton(int row, int column, int level, String quantityLabel) {
        super(row, column, level, quantityLabel);
    }
    
    public QuantityButton(int row, int column, int level, String quantityLabel, int keyCode) {
		super(row, column, level, quantityLabel, keyCode);
    }
}

 