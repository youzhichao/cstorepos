
package hyi.cream.uibeans;

/**
 * 指定更正(删除)键.
 */
public class RemoveButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param removeLabel remove label on button.
     */
	public RemoveButton(int row, int column, int level, String removeLabel) {
        super(row, column, level, removeLabel);
	}

	public RemoveButton(int row, int column, int level, String removeLabel, int keyCode) {
        super(row, column, level, removeLabel, keyCode);
	}
}

 