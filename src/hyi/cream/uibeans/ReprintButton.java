package hyi.cream.uibeans;

/**
 * 重印键.   ③
 */
public class ReprintButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */

    static private String generateVoidTransaction = "yes";

    public ReprintButton(int row, int column, int level, String label) {
        super(row, column, level, label);
        this.generateVoidTransaction = "yes";
    }

    public ReprintButton(int row, int column, int level, String label, String generateVoidTransaction) {
        super(row, column, level, label);
        this.generateVoidTransaction = generateVoidTransaction == null || "".equalsIgnoreCase(generateVoidTransaction.trim()) ? "yes" : generateVoidTransaction.trim();
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param numberLabel a string represents the button label
     * @param keyCode key code
    */
    public ReprintButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
        this.generateVoidTransaction = "yes";
    }

    public ReprintButton(int row, int column, int level, String label, int keyCode, String generateVoidTransaction) {
        super(row, column, level, label, keyCode);
        this.generateVoidTransaction = generateVoidTransaction == null || "".equalsIgnoreCase(generateVoidTransaction.trim()) ? "yes" : generateVoidTransaction.trim();
    }

    static public boolean isGenerateVoidTran() {
        return generateVoidTransaction.equalsIgnoreCase("yes") || generateVoidTransaction.equalsIgnoreCase("true");
    }

}

