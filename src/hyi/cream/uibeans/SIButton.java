
package hyi.cream.uibeans;

import java.math.*;

/**
 * 折扣键(Select Item).
 */
public class SIButton extends POSButton {
    private String siID;

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param siLabel SI label on button.
     * @param siID SI ID.
     */
	public SIButton(int row, int column, int level, String siLabel, String siID) {
		super(row, column, level, siLabel);
        this.siID = siID;
    }
    
    public SIButton(int row, int column, int level, String siLabel, int keyCode, String siID) {
		super(row, column, level, siLabel, keyCode);
        this.siID = siID;
    }

	public String getSiID() {
        return new String(siID);
    }
}


 