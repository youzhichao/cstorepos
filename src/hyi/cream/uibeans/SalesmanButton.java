package hyi.cream.uibeans;

/**
 * 售货员键.
 * 
 * @author bruce
 */
public class SalesmanButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param label a string represents the button label
    */
    public SalesmanButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    /**
     * Constructor.
     *
     * @param row row position
     * @param column column position
     * @param numberLabel a string represents the button label
     * @param keyCode key code
    */
    public SalesmanButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}
