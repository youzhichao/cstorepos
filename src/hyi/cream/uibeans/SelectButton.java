package hyi.cream.uibeans;

/**
 * 指定更正(删除)键.
 */
public class SelectButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param removeLabel remove label on button.
     */
	public SelectButton(int row, int column, int level, String selectLabel) {
        super(row, column, level, selectLabel);
    }
}


