package hyi.cream.uibeans;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.util.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.inline.*;


/**
 * 收银员现金清点单输入画面。
 *
 * <p>Copyright: Copyright (c) 2002 HYI Ltd., Co.</p>
 *
 * @author Bruce
 * @version 1.0
 */
public class SignOffForm extends Dialog /*implements KeyListener*/ {

    private static final int heightShift = 20;

    private static final int TF_COL_CASH = 4;
    private static final int TF_COL_OTHER_COUNT = 4;
    private static final int TF_COL_OTHER = 8;
    private static final int G0 = 3;
    private static final int G1 = 24 * 3 + 12 + 4;
    private static final int G1A = 24 * 2 + 16;
    private static final int G2 = 12 * TF_COL_CASH + 6 - 6;
    private static final int G2A = 12 * TF_COL_OTHER + 3;
    private static final int G3 = 24 + 6 - 6;
    private static final int G4 = 2;
    private static final int G1B = G1 + 12;
    private static final int G3A = 12 * TF_COL_CASH + 6;
    private static final int G3B = 26;

    //   100元 [XXXX]       5角:[XXXX]         XXXX [XXX]张 [XXXXX.XX] 元
    //G0  G1    G2    G3    G1A   G2   G3  G4  G1B   G3A G3B G2A       G3

    private static final Color gridBgColor = new Color(127, 127, 255);
    private static final int RIGHT_ALIGN = 0;
    private static final int LEFT_ALIGN = 1;

    private static final int EDIT_CASHFORM_STATE = 0;
    private static final int YESNO_CASHFORM_STATE = 1;

    /*
    private static final int KEY_UP = 33;
    private static final int KEY_DOWN = 34;
    private static final int KEY_ENTER = 10;
    private static final int KEY_CLEAR = 27;
    private static final int KEY_NUM_PERIOD = 110;
    private static final int KEY_NUM_0 = 96;
    private static final int KEY_NUM_00 = (int)'z';
    private static final int KEY_NUM_1 = 97;
    private static final int KEY_NUM_2 = 98;
    private static final int KEY_NUM_3 = 99;
    private static final int KEY_NUM_4 = 100;
    private static final int KEY_NUM_5 = 101;
    private static final int KEY_NUM_6 = 102;
    private static final int KEY_NUM_7 = 103;
    private static final int KEY_NUM_8 = 104;
    private static final int KEY_NUM_9 = 105;
    */

    static SignOffForm signOffForm;

    private ResourceBundle res = CreamToolkit.GetResource();
    private Font font;

    private String[] paymentTitle = new String[10];
    private NumberLabel[] numberLabels = new NumberLabel[33];
    private NumberLabel totalNumberLabel = new NumberLabel();
    private CashForm cashForm;
    private int currentFieldIdx = 0;
    private int numberOfFields;
    private int bottomY;
    private int state;
    private String answer;

    private int pageUpCode;
    private int pageDownCode;
    private int clearCode;
    private int enterCode;
    private ArrayList numberCodeArray;
    private ArrayList numberButtonArray;
    Rectangle titleRect = new Rectangle(0, 0, 10, 10);

    class NumberLabel {
        int x;
        int y;
        int width;
        int height;
        int len = 4;                    // chars count
        boolean selected;
        int intValue;
        BigDecimal bigDecimalValue;     // contains either bigDecimalValue or intValue
        String stringValue;             // this is for editting buffer
        boolean invalid;                // for painting
        String extraData;               // for storing payment id

        void setSelected(boolean selected) {
            this.selected = selected;
            this.invalid = true;
        }

        void applyChange() {
            if (stringValue == null || stringValue.length() == 0)
                return;
            try {
                if (bigDecimalValue != null)    // 如果bigDecimalValue有值，说明原本为BigDecimal型
                    bigDecimalValue = new BigDecimal(stringValue);
                else
                    intValue = Integer.parseInt(stringValue);
            } catch (NumberFormatException e) {
            }
            stringValue = null;
        }

        void setBounds(int x, int y, int width, int height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        void paint(Graphics g) {
            if (!selected) {
                g.setColor(Color.white);
                g.clearRect(x, y, width, height);
                g.draw3DRect(x, y, width, height, true);
                if (stringValue != null)
                    g.drawString(padBlank(stringValue, RIGHT_ALIGN, len), x + 4, y + height - 4);
                else if (bigDecimalValue != null)
                    g.drawString(padBlank(bigDecimalValue, RIGHT_ALIGN, len), x + 4, y + height - 4);
                else
                    g.drawString(padBlank(intValue, RIGHT_ALIGN, len), x + 4, y + height - 4);
            } else {
                g.setColor(Color.white);
                g.fill3DRect(x, y, width, height, true);
                g.setColor(Color.blue);
                if (stringValue != null)
                    g.drawString(padBlank(stringValue, RIGHT_ALIGN, len), x + 4, y + height - 4);
                else if (bigDecimalValue != null)
                    g.drawString(padBlank(bigDecimalValue, RIGHT_ALIGN, len), x + 4, y + height - 4);
                else
                    g.drawString(padBlank(intValue, RIGHT_ALIGN, len), x + 4, y + height - 4);
                g.setColor(Color.white);
            }
        }
    }

    public SignOffForm(Frame owner) {
        super(owner, true); // modal dialog
        //this.addKeyListener(this);

        String fontName = CreamProperties.getInstance().getProperty("PayingPaneFont");
        //font = new Font(fontName, Font.BOLD, 18);
        font = new Font(fontName, Font.PLAIN, 20);
        setFont(font);
        setBackground(Color.black);

        for (int i = 0; i < 33; i++) {
            numberLabels[i] = new NumberLabel();
        }
        numberLabels[0].selected = true;
    }

    public static SignOffForm getInstance() {
        if (signOffForm == null)
            signOffForm = new SignOffForm(POSTerminalApplication.getInstance());
        return signOffForm;
    }

    private void appendDigits(int idx, String digit) {
        if (numberLabels[idx].stringValue == null) {
            numberLabels[idx].stringValue = "";
            numberLabels[idx].invalid = true;
        }
        if (numberLabels[idx].stringValue.length() < numberLabels[idx].len) {
            numberLabels[idx].stringValue += digit;
            numberLabels[idx].invalid = true;
        }
    }

    public boolean keyDataListener(int keyCode) {
        //System.out.println("keyPressed=" + keyCode);

        if (state == EDIT_CASHFORM_STATE) {
            if (keyCode == clearCode) {
                if (numberLabels[currentFieldIdx].stringValue == null) {
                    numberLabels[currentFieldIdx].setSelected(false);
                    state = YESNO_CASHFORM_STATE;
                    answer = "";
                } else {                        
                    numberLabels[currentFieldIdx].stringValue = null;
                    numberLabels[currentFieldIdx].invalid = true;
                }
            } else if (numberCodeArray.contains(String.valueOf(keyCode))) {
                appendDigits(currentFieldIdx,
                    ((NumberButton)numberButtonArray.get(numberCodeArray.indexOf(String.valueOf(keyCode)))).getNumberLabel());
            } else if (keyCode == pageDownCode || keyCode == enterCode) {
                if (currentFieldIdx < numberOfFields - 1) {
                    numberLabels[currentFieldIdx].applyChange();
                    numberLabels[currentFieldIdx].setSelected(false);
                    numberLabels[++currentFieldIdx].setSelected(true);
                } else {
                    numberLabels[currentFieldIdx].applyChange();
                    numberLabels[currentFieldIdx].setSelected(false);
                    state = YESNO_CASHFORM_STATE;
                    answer = "";
                }
            } else if (keyCode == pageUpCode) {
                if (currentFieldIdx > 0) {
                    numberLabels[currentFieldIdx].applyChange();
                    numberLabels[currentFieldIdx].setSelected(false);
                    numberLabels[--currentFieldIdx].setSelected(true);
                }
            } else {
                Toolkit.getDefaultToolkit().beep();
            }
        } else {
            if (numberCodeArray.contains(String.valueOf(keyCode))) {
                String num = ((NumberButton)numberButtonArray.get(numberCodeArray.indexOf(String.valueOf(keyCode)))).getNumberLabel();
                if (num.equals("1")) {          // OK
                    answer = "1";
                } else if (num.equals("2")) {   // continue modifying
                    answer = "2";
                }
            } else if (keyCode == enterCode) {
                if (answer.equals("2")) {
                    //currentFieldIdx = this.numberOfFields - 1;
                    numberLabels[currentFieldIdx].setSelected(true);
                    state = EDIT_CASHFORM_STATE;
                } else if (answer.equals("1")) {
                    updateCashForm();
                    hide();
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            } else {
                Toolkit.getDefaultToolkit().beep();
            }
        }
        repaint();
        return true;
    }

    public void setBounds() {
        final int width = 620;
        final int height = 342;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width - width) / 2, (screenSize.height - height) / 2,
            width, height);
    }

    public CashForm getCashForm() {
        return cashForm;
    }

    public void setCashForm(CashForm cashForm) {
        this.cashForm = cashForm;
        setCashData();
    }

    /**
     * 初始现金清点明细。
     *
     *  numberLabels [0]:100 [1]:50 [2]:20 [3]:10 [4]:5 [5]:2 [6]: 1
     *               [7]:0.5 [8]:0.2 [9]:0.1 [10]:0.05 [11]:0.02 [12]:0.01
     *  numberLabels [13,15,...]  其他支付张数
     *  numberLabels [14,16,...]  其他支付金额
     */
    public void setCashData() {
        numberLabels[0].intValue = cashForm.getHundred().intValue();
        numberLabels[1].intValue = cashForm.getFifty().intValue();
        numberLabels[2].intValue = cashForm.getTwenty().intValue();
        numberLabels[3].intValue = cashForm.getTen().intValue();
        numberLabels[4].intValue = cashForm.getFive().intValue();
        numberLabels[5].intValue = cashForm.getTwo().intValue();
        numberLabels[6].intValue = cashForm.getOne().intValue();
        numberLabels[7].intValue = cashForm.getFiveJiao().intValue();
        numberLabels[8].intValue = cashForm.getTwoJiao().intValue();
        numberLabels[9].intValue = cashForm.getOneJiao().intValue();
        numberLabels[10].intValue = cashForm.getFiveCents().intValue();
        numberLabels[11].intValue = cashForm.getTwoCents().intValue();
        numberLabels[12].intValue = cashForm.getOneCents().intValue();

        int idx = 13;
        numberOfFields = idx;
        java.util.List otherPaymentIDList = new ArrayList();
        java.util.List otherPaymentNameList = new ArrayList();
        Iterator iter = Payment.getAllPayment();
        while (iter.hasNext()) {
            Payment p = (Payment)iter.next();
            String id = p.getPaymentID();
            if (id.equals("0") || id.equals("00")) // skip "CASH"
                continue;
            otherPaymentIDList.add(id);
            otherPaymentNameList.add(p.getScreenName());
            try {
                Integer count = (Integer)CashForm.class.getDeclaredMethod(
                    "getPay" + id + "Count", new Class[0]).invoke(cashForm, new Object[0]);
                if (count == null)
                    numberLabels[idx].intValue = 0;
                else
                    numberLabels[idx].intValue = count.intValue();
                numberLabels[idx].extraData = id;   // store payment id into extraData
                idx++;

                BigDecimal amount = (BigDecimal)CashForm.class.getDeclaredMethod(
                    "getPay" + id + "Amount", new Class[0]).invoke(cashForm, new Object[0]);
                if (amount == null)
                    numberLabels[idx].bigDecimalValue = new BigDecimal("0.00");
                else
                    numberLabels[idx].bigDecimalValue = amount;
                numberLabels[idx].len = 9;
                idx++;
                numberOfFields = idx;
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            } catch (NoSuchMethodException e) {
            }
        }

        // set other payments' title
        String[] paymentNames = new String[otherPaymentNameList.size()];
        System.arraycopy(otherPaymentNameList.toArray(), 0,
            paymentNames, 0, otherPaymentNameList.size());
        setTitleOfOtherPayment(paymentNames);

        /*
         *    tf[0] - tf[12] : cash
         *    tf[13]  first payment count
         *    tf[14]  first payment amount
         *    tf[15]  second payment count
         *    tf[16]  second payment amount
         *    ....
         */
        //this.tf = new TextField[cashData.length + otherPaymentCount.length +
        //    otherPaymentAmount.length];
    }

    /**
     * 设置其他支付的名称。
     *
     * @param paymentTitle 其他支付的名称
     */
    public void setTitleOfOtherPayment(String[] paymentTitle) {
        this.paymentTitle = paymentTitle;
    }

    private String padBlank(String value, int align, int digits) {
        StringBuffer fld = new StringBuffer(value);
        while (fld.length() < digits) {    // pad space
            if (align == RIGHT_ALIGN)
                fld.insert(0, ' ');
            else
                fld.append(' ');
        }
        return fld.toString();
    }

    private String padBlank(int value, int align, int digits) {
        StringBuffer fld = new StringBuffer("" + value);
        while (fld.length() < digits) {    // pad space
            if (align == RIGHT_ALIGN)
                fld.insert(0, ' ');
            else
                fld.append(' ');
        }
        return fld.toString();
    }

    private String padBlank(BigDecimal value, int align, int digits) {
        value = value.setScale(2, BigDecimal.ROUND_HALF_UP);
        StringBuffer fld = new StringBuffer(value.toString());
        while (fld.length() < digits) {    // pad space
            if (align == RIGHT_ALIGN)
                fld.insert(0, ' ');
            else
                fld.append(' ');
        }
        return fld.toString();
    }

    private BigDecimal computeTotal() {

        // paint total amount
        int total =
            numberLabels[0].intValue * 10000  + numberLabels[1].intValue * 5000  + numberLabels[2].intValue * 2000 +
            numberLabels[3].intValue *  1000  + numberLabels[4].intValue *  500  + numberLabels[5].intValue *  200 +
            numberLabels[6].intValue *   100  + numberLabels[7].intValue *   50  + numberLabels[8].intValue *   20 +
            numberLabels[9].intValue *    10  + numberLabels[10].intValue *   5  + numberLabels[11].intValue *   2 +
            numberLabels[12].intValue *    1;
        BigDecimal t = new BigDecimal(new BigInteger("" + total), 2);
        for (int i = 14; i < numberOfFields; i += 2) {
            t = t.add(numberLabels[i].bigDecimalValue);
        }
        return t;
    }

    public void update(Graphics g) {
        paint(g);
    }

    public void paint(Graphics g) {
        Rectangle r = g.getClipBounds();
        if (r.intersects(titleRect)) {
            paintFrameBorderAndLabels(g);
        }

        // draw numberLabels and totalNumberLabel
        for (int i = 0; i < numberLabels.length; i++) {
            if (numberLabels[i].invalid) {
                numberLabels[i].paint(g);
                numberLabels[i].invalid = false;
            }
        }
        totalNumberLabel.bigDecimalValue = computeTotal();  // recalc total amount
        totalNumberLabel.paint(g);

        if (state == EDIT_CASHFORM_STATE) {
            g.clearRect(140, bottomY + 24, 400, 32);
            g.drawString(res.getString("CashFormHint"), 140, bottomY + 24 * 2);
            g.drawString("  ", 140 + 14 * 24, bottomY + 30);
            // show guideline message...
        } else {
            g.clearRect(140, bottomY + 24, 400, 32);
            g.drawString(res.getString("1_OK_2_Modify"), 140, bottomY + 24 * 2);
            g.drawString("           ", 140 + 12 * 24 + 12, bottomY + 30);
            g.drawString(answer, 140 + 12 * 24 + 12, bottomY + 24 * 2);
        }
        //numberLabels[currentFieldIdx].paint(g);
        //numberLabels[nextFieldIdx].paint(g);
        //currentFieldIdx = nextFieldIdx;

    }

    public void paintFrameBorderAndLabels(Graphics g) {
        FontMetrics fm = g.getFontMetrics();

        // draw border
        g.clearRect(0, 0, getWidth(), getHeight());
        Insets insets = this.getInsets();
        int ix = insets.left;
        int iy = insets.top;
        int iw = getWidth() - ix - insets.right;
        int ih = getHeight() - iy - insets.bottom;

        g.setColor(Color.white);
        g.draw3DRect(ix, iy, iw, ih, true);
        g.draw3DRect(ix + 1, iy + 1, iw - 2, ih - 2, true);

        // draw title
        g.setColor(gridBgColor);
        g.fill3DRect(ix + 2, iy + 2, iw - 4, fm.getHeight(), true);
        titleRect = new Rectangle(ix + 2, iy + 2, iw - 4, fm.getHeight());
        g.setFont(getFont());
        g.setColor(Color.white);
        g.drawString(res.getString("CashForm"), (getWidth() - 24 * 7) / 2, fm.getHeight() + iy - 2);

        // measure the number labels' width and height
        Rectangle2D rect = fm.getStringBounds("1234", g);
        int xb = G0 + G1 - 4;
        int yb = (int)rect.getHeight() - 4;
        int wb = (int)rect.getWidth() + 8;
        int hb = (int)rect.getHeight();

        rect = fm.getStringBounds("123456.78", g);
        int wb9 = (int)rect.getWidth() + 8;

        // draw cash detail
        g.setColor(Color.white);
        int fh = fm.getHeight() + 6;
        int y = fm.getHeight() * 5 / 2 + iy;
        g.drawString(res.getString("100Dollar"), G0, y);
        numberLabels[0].setBounds(xb, y - yb, wb, hb);
        y += fh;
        g.drawString(res.getString("50Dollar"), G0, y);
        numberLabels[1].setBounds(xb, y - yb, wb, hb);
        y += fh;
        g.drawString(res.getString("20Dollar"), G0, y);
        g.draw3DRect(xb, y - yb, wb, hb, false);
        numberLabels[2].setBounds(xb, y - yb, wb, hb);
        y += fh;
        g.drawString(res.getString("10Dollar"), G0, y);
        g.draw3DRect(xb, y - yb, wb, hb, false);
        numberLabels[3].setBounds(xb, y - yb, wb, hb);
        y += fh;
        g.drawString(res.getString("5Dollar"), G0, y);
        numberLabels[4].setBounds(xb, y - yb, wb, hb);
        y += fh;
        g.drawString(res.getString("2Dollar"), G0, y);
        numberLabels[5].setBounds(xb, y - yb, wb, hb);
        y += fh;
        g.drawString(res.getString("1Dollar"), G0, y);
        numberLabels[6].setBounds(xb, y - yb, wb, hb);

        y = fm.getHeight() * 5 / 2 + iy;
        int g0 = G0 + G1 + G2 + G3;
        xb = g0 + G1A;
        g.drawString(res.getString("5Jiao"), g0, y);
        numberLabels[7].setBounds(xb, y - yb, wb, hb);
        y += fh;
        g.drawString(res.getString("2Jiao"), g0, y);
        numberLabels[8].setBounds(xb, y - yb, wb, hb);
        y += fh;
        g.drawString(res.getString("1Jiao"), g0, y);
        numberLabels[9].setBounds(xb, y - yb, wb, hb);
        y += fh;
        y += fh;
        g.drawString(res.getString("5Cent"), g0, y);
        numberLabels[10].setBounds(xb, y - yb, wb, hb);
        y += fh;
        g.drawString(res.getString("2Cent"), g0, y);
        numberLabels[11].setBounds(xb, y - yb, wb, hb);
        y += fh;
        g.drawString(res.getString("1Cent"), g0, y);
        numberLabels[12].setBounds(xb, y - yb, wb, hb);
        bottomY = y;

        //y += fh;
        //inputNumberLabel.setBounds(xb + G2 + G3 + G4 + G1 + G3A + G3B, y - yb, wb, hb);

        int i;

        g0 = G0 + G1 + G2 + G3 + G1A + G2 + G3 + G4;
        xb = g0 + G1B - 4;
        y = fm.getHeight() * 5 / 2 + iy;
        for (i = 13; i < numberOfFields; i++) {
            int idx = (i - 13) / 2;
            g.drawString(paymentTitle[idx] + ":", g0, y);
            numberLabels[i].setBounds(xb, y - yb, wb, hb);
            g.drawString(res.getString("Sheet"), g0 + G1B + G3A, y);
            i++;
            numberLabels[i].setBounds(xb + G3A + G3B, y - yb, wb9, hb);
            g.drawString(res.getString("DollarUnit"), g0 + G1B + G3A + G3B + G2A, y);
            y += fh;
        }

        g.drawString(res.getString("Total2"), g0, bottomY);
        //g.drawString(padBlank(computeTotal(), RIGHT_ALIGN, TF_COL_OTHER),
        //    G0 + G1 + G2 + G3 + G1A + G2 + G3 + G4 + G1, bottomY);
        totalNumberLabel.setBounds(G0 + G1 + G2 + G3 + G1A + G2 + G3 + G4 + G1 - 4,
            bottomY - yb, wb9, hb);
        totalNumberLabel.bigDecimalValue = computeTotal();
        totalNumberLabel.len = 9;
        g.drawString(res.getString("DollarUnit"), g0 + G1 + G2A, bottomY);

        for (i = 0; i < numberOfFields; i++) 
            numberLabels[i].paint(g);
        totalNumberLabel.paint(g);
    }

    public void edit() {
        
        //Bruce/2003-02-17/
        // 为了防止在现金清点单输入期间，用户会连续按多次回车键导致系统重起，
        // 所以将POSButtonHome中的enterCode暂时改掉，好让例如Tec6400KB146中的
        // POSButtonHome.getInstance().getEnterCode()拿到的变成是ClearButton
        // 的key code。也就是在现金清点单输入期间，必须改成按多次清除键才能让系统
        // 重起。
        int origEnterCode = enterCode;
        POSButtonHome posButtonHome = POSButtonHome.getInstance();
        posButtonHome.setEnterCode(posButtonHome.getClearCode());

        state = EDIT_CASHFORM_STATE;
        numberLabels[0].selected = true;
        for (int i = 1; i < 23; i++)
            numberLabels[i].selected = false;
        show();

        // 把enterCode改回来
        posButtonHome.setEnterCode(origEnterCode);
    }

    private void updateCashForm() {
        // set new property values of jkd, then update()
        cashForm.setHundred(new Integer(numberLabels[0].intValue));
        cashForm.setFifty(new Integer(numberLabels[1].intValue));
        cashForm.setTwenty(new Integer(numberLabels[2].intValue));
        cashForm.setTen(new Integer(numberLabels[3].intValue));
        cashForm.setFive(new Integer(numberLabels[4].intValue));
        cashForm.setTwo(new Integer(numberLabels[5].intValue));
        cashForm.setOne(new Integer(numberLabels[6].intValue));
        cashForm.setFiveJiao(new Integer(numberLabels[7].intValue));
        cashForm.setTwoJiao(new Integer(numberLabels[8].intValue));
        cashForm.setOneJiao(new Integer(numberLabels[9].intValue));
        cashForm.setFiveCents(new Integer(numberLabels[10].intValue));
        cashForm.setTwoCents(new Integer(numberLabels[11].intValue));
        cashForm.setOneCents(new Integer(numberLabels[12].intValue));
        cashForm.setPay00Amount(cashForm.getTotalCash());   // computeTotal());

        for (int i = 13; i < numberOfFields; i++) {
            try {
                String paymentID = numberLabels[i].extraData;
                Method m = CashForm.class.getDeclaredMethod(
                    "setPay" + paymentID + "Count",
                    new Class[] {Integer.class});
                m.invoke(cashForm, new Object[] {new Integer(numberLabels[i].intValue)});
                i++;

                m = CashForm.class.getDeclaredMethod(
                    "setPay" + paymentID + "Amount",
                    new Class[] {BigDecimal.class});
                m.invoke(cashForm, new Object[] {numberLabels[i].bigDecimalValue});
            } catch (Exception e) {
            }
        }
        cashForm.update();
    }

    /**
     * 打开并修改某张介款单。如果该介款单不存在，则会自动新增一条记录。
     *
     * @param zNumber Z number
     * @param shiftNumber Shift Number
     * @param viewOnly true if don't want to edit data
     */                                                                
    public void modify(int zNumber, int shiftNumber, boolean viewOnly) {

        // Get key code definitions
        POSButtonHome posButtonHome = POSButtonHome.getInstance();
        pageUpCode = posButtonHome.getPageUpCode();
        pageDownCode = posButtonHome.getPageDownCode();
        clearCode = posButtonHome.getClearCode();
        enterCode = posButtonHome.getEnterCode();
        numberCodeArray = posButtonHome.getNumberCode();
        numberButtonArray = posButtonHome.getNumberButton();

        // query CashForm
        CashForm cashForm = CashForm.queryByZNumberAndShiftNumber(zNumber, shiftNumber);
        if (cashForm == null) {
            try {
                cashForm = new CashForm();
                cashForm.setStoreNumber(CreamProperties.getInstance().getProperty("StoreNumber"));
                cashForm.setTerminalNumber(Transaction.currentTransaction.getTerminalNumber());
                cashForm.setSequenceNumber(new Integer(shiftNumber));
                cashForm.setZSequenceNumber(new Integer(zNumber));
                cashForm.setUploadState("0");
                cashForm.setCashierID(Transaction.currentTransaction.getCashierNumber());
                cashForm.setHundred(new Integer(0));
                cashForm.setFifty(new Integer(0));
                cashForm.setTwenty(new Integer(0));
                cashForm.setTen(new Integer(0));
                cashForm.setFive(new Integer(0));
                cashForm.setTwo(new Integer(0));
                cashForm.setOne(new Integer(0));
                cashForm.setFiveJiao(new Integer(0));
                cashForm.setTwoJiao(new Integer(0));
                cashForm.setOneJiao(new Integer(0));
                cashForm.setFiveCents(new Integer(0));
                cashForm.setTwoCents(new Integer(0));
                cashForm.setOneCents(new Integer(0));
                cashForm.setPay00Amount(new BigDecimal(0));
                cashForm.setPay01Amount(new BigDecimal(0));
                cashForm.setPay02Amount(new BigDecimal(0));
                cashForm.setPay03Amount(new BigDecimal(0));
                cashForm.setPay04Amount(new BigDecimal(0));
                cashForm.setPay05Amount(new BigDecimal(0));
                cashForm.setPay06Amount(new BigDecimal(0));
                cashForm.setPay07Amount(new BigDecimal(0));
                cashForm.setPay08Amount(new BigDecimal(0));
                cashForm.setPay09Amount(new BigDecimal(0));
                cashForm.setPay10Amount(new BigDecimal(0));
                cashForm.setPay11Amount(new BigDecimal(0));
                cashForm.setPay12Amount(new BigDecimal(0));
                cashForm.setPay13Amount(new BigDecimal(0));
                cashForm.setPay14Amount(new BigDecimal(0));
                cashForm.setPay15Amount(new BigDecimal(0));
                cashForm.setPay16Amount(new BigDecimal(0));
                cashForm.setPay17Amount(new BigDecimal(0));
                cashForm.setPay18Amount(new BigDecimal(0));
                cashForm.setPay19Amount(new BigDecimal(0));
                cashForm.setPay20Amount(new BigDecimal(0));
                cashForm.setPay21Amount(new BigDecimal(0));
                cashForm.setPay22Amount(new BigDecimal(0));
                cashForm.setPay23Amount(new BigDecimal(0));
                cashForm.setPay24Amount(new BigDecimal(0));
                cashForm.setPay25Amount(new BigDecimal(0));
                cashForm.setPay26Amount(new BigDecimal(0));
                cashForm.setPay27Amount(new BigDecimal(0));
                cashForm.setPay28Amount(new BigDecimal(0));
                cashForm.setPay29Amount(new BigDecimal(0));
                cashForm.setPay01Count(new Integer(0));
                cashForm.setPay02Count(new Integer(0));
                cashForm.setPay03Count(new Integer(0));
                cashForm.setPay04Count(new Integer(0));
                cashForm.setPay05Count(new Integer(0));
                cashForm.setPay06Count(new Integer(0));
                cashForm.setPay07Count(new Integer(0));
                cashForm.setPay08Count(new Integer(0));
                cashForm.setPay09Count(new Integer(0));
                cashForm.setPay10Count(new Integer(0));
                cashForm.setPay11Count(new Integer(0));
                cashForm.setPay12Count(new Integer(0));
                cashForm.setPay13Count(new Integer(0));
                cashForm.setPay14Count(new Integer(0));
                cashForm.setPay15Count(new Integer(0));
                cashForm.setPay16Count(new Integer(0));
                cashForm.setPay17Count(new Integer(0));
                cashForm.setPay18Count(new Integer(0));
                cashForm.setPay19Count(new Integer(0));
                cashForm.setPay20Count(new Integer(0));
                cashForm.setPay21Count(new Integer(0));
                cashForm.setPay22Count(new Integer(0));
                cashForm.setPay23Count(new Integer(0));
                cashForm.setPay24Count(new Integer(0));
                cashForm.setPay25Count(new Integer(0));
                cashForm.setPay26Count(new Integer(0));
                cashForm.setPay27Count(new Integer(0));
                cashForm.setPay28Count(new Integer(0));
                cashForm.setPay29Count(new Integer(0));
                cashForm.insert(false);
            } catch (InstantiationException e) {
                return;
            }
        }
        setCashForm(cashForm);

        // start viewing/editting
        //state = (viewOnly ? VIEW_JIEKUANDAN_STATE : JIEKUANDAN_STATE);
        currentFieldIdx = 0;
        edit();

		// 2003/02/10 Added by Meyer 
		// 即时上传收银员现金清点单 
        Client client = Client.getInstance();
        if (client != null && client.isConnected()) {
            try {
                POSTerminalApplication.getInstance().getMessageIndicator().setMessage(
                    res.getString("UploadingCashForm") + cashForm.getSequenceNumber());
                client.processCommand("putCashForm " +
                    cashForm.getZSequenceNumber() + " "  + cashForm.getSequenceNumber());
                cashForm.setUploadState("1");
                cashForm.update();
            } catch (ClientCommandException e) {
                cashForm.setUploadState("2");
                cashForm.update();
            }
        }		
    }

    /*public static void main(String[] args) {
        Frame f = new Frame();              // dialog container
        SignOffForm p = new SignOffForm(f);
        p.setBounds();
        p.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        p.modify(0, 0, false);
        System.exit(0);
    }*/
}
