package hyi.cream.uibeans;

public class TicketButton extends POSButton {

    public TicketButton(int row, int column, int level, String payLabel, String payID)
    {
        super(row, column, level, payLabel);
        this.payID = payID;
    }

    public TicketButton(int row, int column, int level, String payLabel, int keyCode, String payID)
    {
        super(row, column, level, payLabel, keyCode);
        this.payID = payID;
    }

    public String getPaymentID()
    {
        return payID;
    }

    String payID;
}
