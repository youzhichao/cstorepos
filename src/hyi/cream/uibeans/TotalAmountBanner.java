package hyi.cream.uibeans;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.math.BigDecimal;
import java.awt.*;
import java.text.*;
import hyi.cream.event.*;
import hyi.cream.*;
import hyi.cream.util.*;
import hyi.cream.inline.*;

public class TotalAmountBanner extends Canvas implements SystemInfoListener {

    private String amountPainted = "";
    private String mmAmountPainted = "";
    private String salesAmountPainted = "";
    private int offScreenH;
    private int offScreenW;
    private ArrayList lineTitles = new ArrayList();
    private ArrayList lineFields = new ArrayList();
    private ArrayList lineWidths = new ArrayList();
    private SystemInfo systemInfo;
    //private String backImage           = "screenbanner.jpg";
    //Image ig = CreamToolkit.getImage(backImage);
    private Image offscreen;
    private Dimension size = null;
    private Color valueColor = Color.white;
    private Color headerColor = Color.white;
    private String fontName;
    private int fontSize = 16;
    private Color backgroundColor = Color.black;
    private Font f1;
    private Font f2;

    public TotalAmountBanner(File propFile, Color bgColor, Color valueColor, Color headerColor)
        throws ConfigurationNotFoundException {
        this(propFile);
        this.backgroundColor = bgColor;
        setBackground(backgroundColor);
        this.headerColor = headerColor;
        this.valueColor = valueColor;
    }

    public TotalAmountBanner(File propFile) throws ConfigurationNotFoundException {
        char ch = '#';
        try {
            FileInputStream filein = new FileInputStream(propFile);
            InputStreamReader inst = new InputStreamReader(filein, CreamToolkit.getEncoding());
            BufferedReader in = new BufferedReader(inst);
            int i = 0;
            String line = "";
            boolean getLine = false;
            while ((line = in.readLine()) != null) {
                getLine = true;
                do {
                    if (!getLine)
                        line = in.readLine();
                    getLine = false;
                    while (line.equals("")) {
                        line = in.readLine();
                    }
                    i = 0;
                    do {
                        ch = line.charAt(i);
                        i++;
                    } while ((ch == ' ' || ch == '\t') && i < line.length());
                }
                while (ch == '#' || ch == ' ' || ch == '\t');

                String s = "";
                if (line.startsWith("font")) {
                    StringTokenizer t0 = new StringTokenizer(line, ",", true);
                    while (t0.hasMoreTokens()) {
                        s = t0.nextToken();
                        if (s.startsWith("fontName")) {
                            fontName = s.substring("fontName".length() + 1, s.length());
                        } else if (s.startsWith("fontSize")) {
                            s = s.substring("fontSize".length() + 1, s.length());
                            try {
                                fontSize = Integer.parseInt(s);
                            } catch (Exception e) {
                                fontSize = 16;
                            }
                        }
                    }
                    f1 = new Font(fontName, Font.PLAIN, fontSize);
                    f2 = new Font(fontName, Font.BOLD, fontSize);
                }
            }
        } catch (FileNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("File not found: " + propFile + ", at " + this);
        } catch (IOException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("IO exception at " + this);
        }
    }

    /**
     * Sets the associated transaction object. It'll also register itself
     * as the system information listener of the system information object.
     * 
     ** @param systemInfo the sytem information object
     */
    public void setSystemInfo(SystemInfo systemInfo) {
        this.systemInfo = systemInfo;
    }

    /**
     * Returns the associated transaction object.
     */
    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    /**
     * Invoked when transaction has been changed.
     * 
     * @param e an event object represents the changes.
     */
    private boolean isChanged = false;
    public void systemInfoChanged(SystemInfoEvent e) {
        isChanged = true;
        repaint();
        /*if (POSTerminalApplication.getInstance().getCurrentTransaction().decreaseLockCount() == 0) {
        	POSTerminalApplication.getInstance().getCurrentTransaction().setLockEnable(true);
        }*/
    }

    //    public Dimension getPreferredSize() {
    //        return size;
    //    }
    //
    //    public Dimension getMinimumSize() {
    //        return size;
    //    }
    //
    //    public void invalidate() {
    //        super.invalidate();
    //        offscreen = null;
    //    }

    public void update(Graphics g) {
        paint(g);
    }

    public void paint(Graphics g) {
        if (getWidth() <= 0 || getHeight() <= 0) {
            EPOSBackground.getInstance().repaint();
            return;
        }

        int startx = 5;
        int starty = 0;
        //int width = getWidth() - 10;
        int marginy = 3;

        if (offscreen == null || offScreenW != getWidth() || offScreenH != getHeight()) {
            offscreen = createImage(getWidth(), getHeight());
            offScreenW = getWidth();
            offScreenH = getHeight();
            salesAmountPainted = "~";      // make it repaint, don't use buffered
        }

        Graphics og = offscreen.getGraphics();

        try {
            //gllg
            String  lineItemDsctAmtTotal = "";
            String salesAmount = "0.00";
            String mmAmount = "0.00";
            try {
                lineItemDsctAmtTotal = POSTerminalApplication.getInstance().getCurrentTransaction().getLineItemDsctAmtTotal4Display();
            } catch (Exception e) {
                lineItemDsctAmtTotal = "0.00";
            }
            
            if (POSTerminalApplication.getInstance().getCurrentTransaction().getDealType3().equals("4")){
                BigDecimal mmAmountBigDecimal = (new BigDecimal(systemInfo.getTotoalMMAmount())
                    .subtract(new BigDecimal(lineItemDsctAmtTotal).negate().setScale(2, BigDecimal.ROUND_HALF_UP)));
                mmAmount = mmAmountBigDecimal.toString();
                salesAmount = new BigDecimal(systemInfo.getSubtotoalAmount()).add(mmAmountBigDecimal).toString();
            } else {
                BigDecimal mmAmountBigDecimal = (new BigDecimal(systemInfo.getTotoalMMAmount())
                    .subtract(new BigDecimal(lineItemDsctAmtTotal).setScale(2, BigDecimal.ROUND_HALF_UP)));
                mmAmount = mmAmountBigDecimal.toString();
                salesAmount = new BigDecimal(systemInfo.getSubtotoalAmount()).add(mmAmountBigDecimal).toString();
            }
            
            String amount = systemInfo.getSubtotoalAmount();
            if (salesAmount.equals(salesAmountPainted)
                && mmAmount.equals(mmAmountPainted)
                && amount.equals(amountPainted)) {
                g.drawImage(offscreen, 0, 0, this);
                og.dispose();
                return;
            } else {
                salesAmountPainted = salesAmount;
                mmAmountPainted = mmAmount;
                amountPainted = amount;
            }

            //((Graphics2D)og).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            ((Graphics2D)og).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            og.setColor(backgroundColor);
            og.fillRect(0, 0, getSize().width - 2, getSize().height - 2);

            //java.util.List printList = new Vector();
            FontMetrics fm1 = og.getFontMetrics(f1);
            FontMetrics fm2 = og.getFontMetrics(f2);

            og.setColor(headerColor);
            String salesAmountStr = "应收:";

            og.setFont(f2);
            starty = getHeight() - marginy - fm2.getHeight();
            if (salesAmount.trim().equals("") || salesAmount.trim().equals("0"))
                salesAmount = "0.00";

            og.drawString(salesAmount,
            //startx + fm1.stringWidth(salesAmountStr) + 5,
            getWidth() - (startx + fm2.stringWidth(salesAmount) + 5), starty + fm2.getAscent() + 5);

            og.setFont(f1);
            starty = getHeight() - marginy - fm1.getHeight();
            og.drawString(salesAmountStr, startx, starty + fm2.getAscent() + 5);

            starty -= marginy;
            og.drawLine(0, starty, getWidth(), starty);

            String mmAmountStr = "折扣:";
            starty -= marginy + fm1.getHeight();
            og.drawString(mmAmountStr, startx, starty + fm1.getAscent());
            if (mmAmount.trim().equals("") || mmAmount.trim().equals("0"))
                mmAmount = "0.00";
            og.drawString(mmAmount,
            //startx + fm1.stringWidth(mmAmountStr) + 5,
            getWidth() - (startx + fm1.stringWidth(mmAmount) + 5), starty + fm1.getAscent());

            String amountStr = "小计:";
            starty -= marginy + fm1.getHeight();
            og.drawString(amountStr, startx, starty + fm1.getAscent());
            if (amount.trim().equals("") || amount.trim().equals("0"))
                amount = "0.00";

            og.drawString(amount,
            //startx + fm1.stringWidth(amountStr) + 5,
            getWidth() - (startx + fm1.stringWidth(amount) + 5), starty + fm1.getAscent());
            g.drawImage(offscreen, 0, 0, this);
            og.dispose();
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }

        //  for transaction event forward
        if (isChanged) {
            if (POSTerminalApplication.getInstance().getCurrentTransaction().decreaseLockCount() == 0) {
                POSTerminalApplication.getInstance().getCurrentTransaction().setLockEnable(true);
            }
            isChanged = false;
        }

        //        // When finishing its paint, notify EPOSBackground to paint.
        //        // Ref. POSTerminalApplication.setPayingPaneVisible()
        //        synchronized (EPOSBackground.getInstance().waitForDrawing) {
        //            EPOSBackground.getInstance().waitForDrawing.notifyAll();
        //            System.out.println("notifying..."+ this.toString());
        //        }

    }

    //    public void setVisible(boolean b) {
    //        super.setVisible(b);
    //        if (!b) {
    //            // When finishing its paint, notify EPOSBackground to paint.
    //            // Ref. POSTerminalApplication.setPayingPaneVisible()
    //            synchronized (EPOSBackground.getInstance().waitForDrawing) {
    //                EPOSBackground.getInstance().waitForDrawing.notifyAll();
    //                System.out.println("\nnotifying..." + this.toString());
    //            }
    //        }
    //    }
}
