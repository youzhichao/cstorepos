
package hyi.cream.uibeans;

/**
 * 交易類型按鍵
 */
public class TransactionTypeButton extends POSButton {

    /**
     * Constructor.
     *
     * @param row row position.
     * @param column column position.
     * @param label on button.
     */
	public TransactionTypeButton(int row, int column, int level, String label) {
        super(row, column, level, label);
    }

    public TransactionTypeButton(int row, int column, int level, String label, int keyCode) {
        super(row, column, level, label, keyCode);
    }
}