// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   WeiXiuButton.java

package hyi.cream.uibeans;


// Referenced classes of package hyi.cream.uibeans:
//            POSButton

public class WeiXiuButton extends POSButton
{

    private String pluCode;
    private static WeiXiuButton weiXiuButton = null;

    public static WeiXiuButton getInstance()
    {
        return weiXiuButton;
    }

    public WeiXiuButton(int row, int column, int level, String label)
    {
        super(row, column, level, label);
        pluCode = "";
        weiXiuButton = this;
    }

    public WeiXiuButton(int row, int column, int level, String label, int keyCode)
    {
        super(row, column, level, label, keyCode);
        pluCode = "";
        weiXiuButton = this;
    }

    public WeiXiuButton(int row, int column, int level, String label, int keyCode, String pluCode)
    {
        super(row, column, level, label, keyCode);
        this.pluCode = "";
        this.pluCode = pluCode;
        weiXiuButton = this;
    }

    public String getPluCode()
    {
        return pluCode;
    }

}
