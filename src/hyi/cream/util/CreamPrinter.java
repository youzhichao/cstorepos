package hyi.cream.util;

//import java.math.*;
//import java.io.*;
//import jpos.*;
//import java.text.*;

import java.util.*;
import java.math.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.state.*;

public abstract class CreamPrinter {

	protected static int ASCII_SEPARATOR = 128;
	private boolean headerPrinted;
	protected int currentLine = 1;
	protected int currentPage = 1;
	protected boolean printEnabled = true;
	protected ResourceBundle res;
	protected java.math.BigDecimal subTotal = new java.math.BigDecimal(0);
	protected BigDecimal total = new BigDecimal(0);

	public CreamPrinter() throws InstantiationException {
	}

	public static CreamPrinter getInstance() {
        return CreamPrinter_zh_CN.getInstance_zh_CN();
	}

	abstract public void printStamp();
	abstract public void printHeader();
	abstract public void printHeader(Transaction p0);
	abstract public void printLineItem(LineItem p0);
	abstract public void printSubtotal();
    abstract public void printCashForm(CashForm cf);
	abstract public void printPayment(Transaction p0);
	abstract public void printZReport(ZReport p0, State p1); 
	abstract public void printXReport(ZReport p0, State p1);
	abstract public void printShiftReport(ShiftReport p0, State p1);

	abstract public void printFirstReceipt();
	abstract public void printCancel(String cancelMsg);
	abstract public void printSlip(State p0);
	abstract public void reprint(int number);
	abstract public void reprint(Transaction p0);
    abstract public void reprint(int number, boolean isPageCut, boolean isHisTrans);

    //Properties
	public boolean getHeaderPrinted () {
		return headerPrinted;
	}

    public void setHeaderPrinted(boolean headerPrinted) {
		this.headerPrinted = headerPrinted;
	}

	public boolean getPrintEnabled() {
		return printEnabled;
	}

	public void setPrintEnabled(boolean printEnabled) {
		this.printEnabled = printEnabled;
	}

	public int getCurrentLine(){
		return currentLine;
	}

	public void setCurrentLine(int currentLine){
        this.currentLine = currentLine;
	}

	public int getCurrentPage(){
		return currentPage;
    }

    public void setCurrentPage(int currentPage){
        this.currentPage = currentPage;
	}
	
	public BigDecimal getSubTotal(){
        return subTotal;
    }

	public void setSubTotal (BigDecimal subTotal){
        this.subTotal = subTotal;
	}

	public BigDecimal getTotal(){
		return total;
    }

	public void setTotal (BigDecimal total){
		this.total = total;
	}

    public void printInventoryHeader() {
    }

    public void printInventoryItem(Inventory inv) {
    }

    public void printInventoryFooter() {
    }
}
