package hyi.cream.util;

import java.util.*;
import java.text.*;
import java.math.*;
import java.io.*;

import jpos.*;
import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.state.*;

public class CreamPrinter_zh_CN extends CreamPrinter {

    private boolean firstPage = true;
    private boolean veryFirstTime = true;
    private int lineItemsPrintedInThisPage;
    private int linesPrintedInThisPage;
    public static final int MAX_CHAR_LINE = 24;
    public static int MAX_ITEM_LINES = 15;
    public static final int EndSpace = 24;
    public static int ITEM_LINES = 0;
    private static CreamPrinter_zh_CN instance = null;
    private static String storeAddr = hyi.cream.dac.Store.getAddress();
    private static String storeTel =
            hyi.cream.dac.Store.getAreaNumber() + "-" + hyi.cream.dac.Store.getTelephoneNumber();

    //交易需要需要打乱
    private static final String needConfuseTranNumber =
            CreamProperties.getInstance().getProperty("ConfuseTranNumber");

    private BigDecimal tax0Amount = new BigDecimal(0);
    private BigDecimal tax1Amount = new BigDecimal(0);
    private BigDecimal tax2Amount = new BigDecimal(0);
    private BigDecimal tax3Amount = new BigDecimal(0);
    private BigDecimal tax4Amount = new BigDecimal(0);
    private BigDecimal zero = new BigDecimal(0);
    private boolean printDaiFu =
            CreamProperties.getInstance().getProperty("PrintDaiFu", "no").equalsIgnoreCase("yes");

    public CreamPrinter_zh_CN() throws InstantiationException {
        if (instance != null)
            throw new InstantiationException(this.toString());
        else {
            res = CreamToolkit.GetResource();
            instance = this;
        }
    }

    public static CreamPrinter_zh_CN getInstance_zh_CN() {
        try {
            if (instance == null)
                instance = new CreamPrinter_zh_CN();
        } catch (InstantiationException ie) {
            return instance;
        }
        return instance;
    }

    public void printCashForm(CashForm j) {
        if (!getPrintEnabled())
            return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        //POSTerminalApplication posTerminal =
        //    POSTerminalApplication.getInstance();
        POSPrinter printer = null;
        StringWriter data = new StringWriter();
        ReportGenerator r = new ReportGenerator(j);
        data = (StringWriter) r.generate(data, false);
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            CreamProperties prop = CreamProperties.getInstance();
            prop.setProperty("Print_Started", "true");
            prop.deposit();

            String printerType = prop.getProperty("PrinterType");
            if (printerType != null && printerType.equalsIgnoreCase("OneStation")) {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, data.toString(), "");
                return;
            }

            if (prop.getProperty("postype").equalsIgnoreCase("tec")) {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|1C", "");
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|N", "");
            }

            if (ReceiptGenerator.isWorkable()) {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, data.toString(), "");
                pageUp();
                if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                    printer.cutPaper(99);
                }
                ;
                return;
            }

            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|4lF", "");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, data.toString(), "");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|10lF", "");
            //pageUp();
            if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                printer.cutPaper(99); // 让它不要卷太多
            }
        } catch (JposException je) {
            CreamToolkit.logMessage(je.toString());
        }
        printFirstReceipt();
    }

    public void printXReport(ZReport z, State sourceState) {
        if (!getPrintEnabled())
            return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        //POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        POSPrinter printer = null;
        StringWriter data = new StringWriter();
        ReportGenerator r = new ReportGenerator(z);
        data = (StringWriter) r.generate(data, true);
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            CreamProperties.getInstance().setProperty("Print_Started", "true");
            CreamProperties.getInstance().deposit();
            if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|1C", "");
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|N", "");
            }

            if (ReceiptGenerator.isWorkable()) {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, data.toString(), "");
                pageUp();
                if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                    printer.cutPaper(99);
                }
                ;
                return;
            }

            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|4lF", "");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, data.toString(), "");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|10lF", "");
            pageUp();
            if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                printer.cutPaper(99);
            }
        } catch (JposException je) {
            CreamToolkit.logMessage(je.toString());
        }
        printFirstReceipt();
    }

    public static void main(String[] args) {
        // -z [z_seq]
        // -s [z_seq] [shift_seq]
        if (args.length == 0 ||
                args[0].equals("-z") && args.length < 2 ||
                args[0].equals("-s") && args.length < 3) {
            System.out.println("Usage: -z [z_seq] or -s [z_seq] [shift_seq]");
            System.exit(0);
        }
        if (args[0].equals("-z")) {
            ZReport z = ZReport.queryBySequenceNumber(new Integer(args[1]));
            CreamPrinter.getInstance().printZReport(z, null);
            System.exit(0);
        }
        if (args[0].equals("-s")) {
            ShiftReport shift = ShiftReport.queryByZNumberAndShiftNumber(
                    new Integer(args[1]), new Integer(args[2]));
            CreamPrinter.getInstance().printShiftReport(shift, null);
            System.exit(0);
        }
    }


    public void printZReport(ZReport z, State sourceState) {
        if (!getPrintEnabled())
            return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        //POSTerminalApplication posTerminal =
        //    POSTerminalApplication.getInstance();
        POSPrinter printer = null;
        StringWriter data = new StringWriter();
        ReportGenerator r = new ReportGenerator(z);
        data = (StringWriter) r.generate(data);
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            CreamProperties.getInstance().setProperty("Print_Started", "true");
            CreamProperties.getInstance().deposit();
            if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|1C", "");
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|N", "");
            }

            if (ReceiptGenerator.isWorkable()) {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, data.toString(), "");
                pageUp();
                if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                    printer.cutPaper(99);
                }
                ;
                return;
            }

            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|4lF", "");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, data.toString(), "");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|10lF", "");
            pageUp();
            if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                printer.cutPaper(99);
            }
            ;
        } catch (JposException je) {
            CreamToolkit.logMessage(je.toString());
        }
        printFirstReceipt();
    }

    public void pageUp() {
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);

            CreamProperties prop = CreamProperties.getInstance();

            String printerType = prop.getProperty("PrinterType");
            if (ReceiptGenerator.isWorkable()) {
                // Print "P" section in receipt format definition file
                try {
                    ReceiptGenerator gen = ReceiptGenerator.getInstance();
                    Collection lines = gen.generateLines('P', new hyi.cream.dac.LineItem());
                    Iterator iter = lines.iterator();
                    while (iter.hasNext()) {
                        String line = iter.next().toString();
                        if (line.indexOf("\u000C") != -1) {
                            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, line, "");
                        } else if (line.indexOf("$CUT_PAPER_AT_MARK$") != -1) {
                            printer.cutPaper(100);  //遇黑点切纸
                        } else if (line.indexOf("$CUT_PAPER$") != -1) {
                            printer.cutPaper(99);   //立即切纸
                        } else {
                            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, line + "\n", "");
                        }
                    }
                } catch (InstantiationException e) {
                }
            } else if (printerType != null && printerType.equalsIgnoreCase("OneStation")) {
                ;
            } else if (prop.getProperty("postype").equalsIgnoreCase("ptn")) {
                ;
            } else {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|V", "");
            }
            CreamProperties.getInstance().setProperty("Print_Started", "false");
            CreamProperties.getInstance().deposit();
        } catch (JposException je) {
            CreamToolkit.logMessage(je.toString());
        }
    }

    public void printShiftReport(ShiftReport shift, State sourceState) {
        if (!getPrintEnabled())
            return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        //POSTerminalApplication posTerminal =
        //    POSTerminalApplication.getInstance();
        POSPrinter printer = null;
        StringWriter data = new StringWriter();
        ReportGenerator r = new ReportGenerator(shift);
        data = (StringWriter) r.generate(data);
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            CreamProperties.getInstance().setProperty("Print_Started", "true");
            CreamProperties.getInstance().deposit();
            if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|1C", "");
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|N", "");
            }

            if (ReceiptGenerator.isWorkable()) {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, data.toString(), "");
                pageUp();
                if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                    printer.cutPaper(99);
                }
                ;
                return;
            }

            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|4lF", "");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, data.toString(), "");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|10lF", "");
            pageUp();
            if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                printer.cutPaper(99);
            }
        } catch (JposException je) {
            CreamToolkit.logMessage(je.toString());
        }
        printFirstReceipt();
    }

    /*
    1234567890123456789012345678901234567890123456789012
    XXX YYMMDD HH:MM TT-NNNNNN SSSSSS CCCCCCC $999999.99
    
    XXX: 交易别
        Zxx: 支付类（xx为支付代号）
        Gyy: 折扣类（yy为折扣代号）
    YYMMDD: 公元日期
    HH:MM: 时间
    TT: 收银机机号
    NNNNNN: 交易序号
    SSSSSS: 店号
    CCCCCCC: 收银员
    $999999.99: 支付或折扣金额
    //*/
    String getSlipData(Transaction transac, State sourceState) {
        boolean discount = false;
        boolean returnItem = false;
        String space = " ";
        if (sourceState.getClass().getName().endsWith("Slipping2State")
                || sourceState.getClass().getName().endsWith("Slipping3State"))
            discount = true;
        if (sourceState.getClass().getName().endsWith("ReturnSlippingState"))
            returnItem = true;
        String slip = "";
        if (discount)
            slip += "G" + transac.getLastSIID() + space;
        else if (!returnItem)
            slip += "Z" + transac.getLastestPayment().getPaymentID() + space;
        else if (returnItem)
            slip += "RTN" + space;
        SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd HH:MM");
        Date currentTime_1 = new Date();
        slip += formatter.format(currentTime_1) + space;
        slip += CreamProperties.getInstance().getProperty("TerminalNumber") + "-";
        String number = CreamProperties.getInstance().getProperty("NextTransactionNumber");
        slip += getFormattedTransactionNumber(Integer.parseInt(number)) + space;
        slip += CreamProperties.getInstance().getProperty("StoreNumber") + space;
        slip += CreamProperties.getInstance().getProperty("CashierNumber") + space;
        if (discount)
            slip += "$" + transac.getSIAmtList().get(transac.getLastSIID());
        else if (!returnItem) {
            String payID = transac.getLastestPayment().getPaymentID();
            slip += "$" + transac.getPayAmountByID(transac.getCorrespondingID(payID)).setScale(2, 4);
        } else if (returnItem) {
            slip += "$" + transac.getSalesAmount();
        }
        slip += "\n";
        return slip;
    }

    String getFormattedTransactionNumber(int number) {
        //CreamProperties creamProperties = CreamProperties.getInstance();
        String NextTransactionSequenceNumber = String.valueOf(number);
        String zero = "";
        if (NextTransactionSequenceNumber.length() < 8) {
            int addBits = 8 - NextTransactionSequenceNumber.length();
            for (int i = 0; i < addBits; i++) {
                zero += "0";
            }
            NextTransactionSequenceNumber = zero + NextTransactionSequenceNumber;
        } else if (NextTransactionSequenceNumber.length() > 8) {
            int beginIndex = NextTransactionSequenceNumber.length() - 8;
            NextTransactionSequenceNumber =
                    NextTransactionSequenceNumber.substring(beginIndex, NextTransactionSequenceNumber.length());
        }
        return NextTransactionSequenceNumber;
    }

    public void printSlip(State sourceState) {
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            printer.printNormal(
                    POSPrinterConst.PTR_S_SLIP,
                    getSlipData(posTerminal.getCurrentTransaction(), sourceState));
        } catch (JposException je) {
            CreamToolkit.logMessage(je.toString());
        }
    }


    public void reprint(Transaction tran) {
        if (!getPrintEnabled())
            return;
        printHeader(tran); //.getTransactionNumber().intValue());
        //Iterator itr = p0.getLineItems();
        Object[] lines = tran.getLineItemsArrayLast();
        int length = tran.getLineItemsArrayLast().length;
        for (int i = 0; i < length; i++) {
            LineItem li = (LineItem) lines[i];
            if (li.getPrinted())
                li.setPrinted(false);
            if (li.getDetailCode().compareTo("M") == 0 || li.getDetailCode().compareTo("E") == 0)
                continue;
            else if (li.getDetailCode().compareTo("S") == 0 && li.getRemoved() && i < length - 1) {
                LineItem lineItem = (LineItem) lines[i + 1];
                if (lineItem.getDetailCode().compareTo("E") == 0)
                    continue;
            }
            printLineItem(li);
        }
        printPayment(tran);
    }

    public void reprint(int number) {
        if (!getPrintEnabled())
            return;
        String transactionNumber = String.valueOf(number);
        Transaction previousTransaction = Transaction.queryByTransactionNumber(transactionNumber);
        if (previousTransaction == null)
            return;
        reprint(previousTransaction);
    }

    public void printLineItemPartner(LineItem lineItem) {
        ResourceBundle res = CreamToolkit.GetResource();
        if (!getPrintEnabled())
            return;
        if (lineItem.getPrinted())
            return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        /***********************************************************************
         *Build a line item printing string from the line item object
         /***********************************************************************/
        ArrayList allContent = new ArrayList();
        String quantity = "";
        String voidFlag = "";
        if (lineItem.getDetailCode().equals("V")) {
            voidFlag = res.getString("Void") + "\n";
            allContent.add(voidFlag);
        }
        String lineItemContent = "";
        String printName = "";
        String totalPrice = "";
        //int totalPriceLength = 0;
        int printNameLength = 0;
        String tax = "";
        //int chars = 0;
        String spaces = "";

        //Tax related
        if (lineItem.getTaxType() != null && !lineItem.getTaxType().equals("")) {
            TaxType taxType = TaxType.queryByTaxID(lineItem.getTaxType());
            if (!taxType.getType().equals("0"))
                tax = "";
        }
        if (lineItem.getPluNumber() != null) {
            if (lineItem.getDescription() == null)
                lineItem.setDescription("");
            if (lineItem.getDescription().equals("")) {
                PLU plu = PLU.queryByPluNumber(lineItem.getPluNumber());
                if (plu == null) {
                    if (lineItem.getPluNumber().length() > 0) {
                        String cat = "";
                        if (lineItem.getDetailCode().equals("I"))
                            cat = "08";
                        else if (lineItem.getDetailCode().equals("Q"))
                            cat = "09";
                        else if (lineItem.getDetailCode().equals("N"))
                            cat = "10";
                        else if (lineItem.getDetailCode().equals("T"))
                            cat = "11";
                        Reason r = Reason.queryByreasonNumberAndreasonCategory(lineItem.getPluNumber(), cat);
                        if (r != null)
                            printName = r.getreasonName();
                    } else {
                        TaxType t = TaxType.queryByTaxID(lineItem.getTaxType());
                        if (t != null) {
                            printName = t.getTaxName();
                        }
                    }
                } else {
                    String name = CreamProperties.getInstance().getProperty("PluPrintField");
                    if (name == null)
                        name = "ScreenName";
                    if (name.equals("ScreenName"))
                        printName = plu.getScreenName();
                    else
                        printName = plu.getPrintName();
                    if (printName == null)
                        printName = "";
                }
            } else {
                printName = lineItem.getDescription();
            }
        } else {
            if (lineItem.getDescription() == null)
                printName = "";
            else {
                printName = lineItem.getDescription();
            }
        }
        if (lineItem.getDetailCode().equalsIgnoreCase("X"))
            totalPrice = lineItem.getAmount().setScale(0, 4) + tax;
        else if (
                lineItem.getDetailCode().equals("I")
                        || lineItem.getDetailCode().equals("Q")
                        || lineItem.getDetailCode().equals("N")
                        || lineItem.getDetailCode().equals("T")) {
            totalPrice = lineItem.getUnitPrice() + tax;
        } else
            totalPrice = lineItem.getAmount() + tax;
        for (int i = 0; i < printName.length(); i++) {
            //Character c = new Character(printName.charAt(i));
            if (printName.charAt(i) > ASCII_SEPARATOR) {
                printNameLength += 2;
            } else {
                printNameLength += 1;
            }
        }

        //totalPriceLength = totalPrice.length();
        //chars = totalPriceLength + printNameLength;
        spaces = ""; //Two spaces here
        for (int i = 0; i < MAX_CHAR_LINE - 6; i++) {
            spaces += " ";
        }

        BigDecimal qty = new BigDecimal(lineItem.getQuantity().intValue());
        if (qty.compareTo(lineItem.getQuantity()) != 0)
            qty = lineItem.getQuantity();
        quantity = "@" + lineItem.getUnitPrice() + "  x  " + qty;
        String pluNum = lineItem.getPluNumber();
        String pluSpace = "";
        for (int i = 0; i < 13 - pluNum.length(); i++) {
            pluSpace += " ";
        }

        spaces = spaces.substring(0, spaces.length() - 13 - 2);
        lineItemContent = pluNum + pluSpace + "  " + printName + "\n";
        String space = "";
        String newspace = "";
        for (int i = 0; i < 13 + 2; i++) {
            space = space + " ";
        }

        for (int i = 0; i < 32 - space.length() - quantity.length(); i++) {
            newspace = newspace + " ";
        }
        lineItemContent = lineItemContent + space + quantity + newspace + totalPrice + "\n";
        allContent.add(lineItemContent);
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            setSubTotal(getSubTotal().add(lineItem.getAmount()));
            setTotal(getTotal().add(lineItem.getAmount()));
            for (int i = 0; i < allContent.size(); i++) {
                String current = (String) allContent.get(i);
                setCurrentLine(this.getCurrentLine() + 1);
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, current, "");
            }
        } catch (JposException je) {
            je.printStackTrace(CreamToolkit.getLogger());
        }
        lineItem.setPrinted(true);
    }

    public void printLineItemOneStation(LineItem lineItem) {
        ResourceBundle res = CreamToolkit.GetResource();
        if (!getPrintEnabled())
            return;
        if (lineItem.getPrinted())
            return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        /***********************************************************************
         *Build a line item printing string from the line item object
         /***********************************************************************/
        ArrayList allContent = new ArrayList();
        String quantity = "";
        String voidFlag = "";
        if (lineItem.getDetailCode().equals("V")) {
            voidFlag = res.getString("Void") + "\n";
            allContent.add(voidFlag);
        }
        String lineItemContent = "";
        String printName = "";
        String totalPrice = "";
        //int totalPriceLength = 0;
        int printNameLength = 0;
        String tax = "";
        //int chars = 0;
        String spaces = "";

        //Tax related
        if (lineItem.getTaxType() != null && !lineItem.getTaxType().equals("")) {
            TaxType taxType = TaxType.queryByTaxID(lineItem.getTaxType());
            if (!taxType.getType().equals("0"))
                tax = "";
        }
        if (lineItem.getPluNumber() != null) {
            if (lineItem.getDescription() == null)
                lineItem.setDescription("");
            if (lineItem.getDescription().equals("")) {
                PLU plu = PLU.queryByPluNumber(lineItem.getPluNumber());
                if (plu == null) {
                    if (lineItem.getPluNumber().length() > 0) {
                        String cat = "";
                        if (lineItem.getDetailCode().equals("I"))
                            cat = "08";
                        else if (lineItem.getDetailCode().equals("Q"))
                            cat = "09";
                        else if (lineItem.getDetailCode().equals("N"))
                            cat = "10";
                        else if (lineItem.getDetailCode().equals("T"))
                            cat = "11";
                        Reason r = Reason.queryByreasonNumberAndreasonCategory(lineItem.getPluNumber(), cat);
                        if (r != null)
                            printName = r.getreasonName();
                    } else {
                        TaxType t = TaxType.queryByTaxID(lineItem.getTaxType());
                        if (t != null) {
                            printName = t.getTaxName();
                        }
                    }
                } else {
                    String name = CreamProperties.getInstance().getProperty("PluPrintField");
                    if (name == null)
                        name = "ScreenName";
                    if (name.equals("ScreenName"))
                        printName = plu.getScreenName();
                    else
                        printName = plu.getPrintName();
                    if (printName == null)
                        printName = "";
                }
            } else {
                printName = lineItem.getDescription();
            }
        } else {
            if (lineItem.getDescription() == null)
                printName = "";
            else {
                printName = lineItem.getDescription();
            }
        }
        if (lineItem.getDetailCode().equalsIgnoreCase("X"))
            totalPrice = lineItem.getAmount().setScale(0, 4) + tax;
        else if (
                lineItem.getDetailCode().equals("I")
                        || lineItem.getDetailCode().equals("Q")
                        || lineItem.getDetailCode().equals("N")
                        || lineItem.getDetailCode().equals("T")) {
            totalPrice = lineItem.getUnitPrice() + tax;
        } else
            totalPrice = lineItem.getAmount() + tax;
        for (int i = 0; i < printName.length(); i++) {
            //Character c = new Character(printName.charAt(i));
            if (printName.charAt(i) > ASCII_SEPARATOR) {
                printNameLength += 2;
            } else {
                printNameLength += 1;
            }
        }

        //totalPriceLength = totalPrice.length();
        //chars = totalPriceLength + printNameLength;
        spaces = ""; //Two spaces here
        for (int i = 0; i < MAX_CHAR_LINE - 6; i++) {
            spaces += " ";
        }

        BigDecimal qty = new BigDecimal(lineItem.getQuantity().intValue());
        if (qty.compareTo(lineItem.getQuantity()) != 0)
            qty = lineItem.getQuantity();
        quantity = "@" + lineItem.getUnitPrice() + "  x  " + qty;
        String pluNum = lineItem.getPluNumber();
        String pluSpace = "";
        for (int i = 0; i < 13 - pluNum.length(); i++) {
            pluSpace += " ";
        }

        spaces = spaces.substring(0, spaces.length() - 13 - 2);
        lineItemContent = pluNum + pluSpace + "  " + printName + "\n";
        String space = "";
        String newspace = "";
        for (int i = 0; i < 13 + 2; i++) {
            space = space + " ";
        }

        for (int i = 0; i < 32 - space.length() - quantity.length(); i++) {
            newspace = newspace + " ";
        }
        lineItemContent = lineItemContent + space + quantity + newspace + totalPrice + "\n";
        allContent.add(lineItemContent);
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            setSubTotal(getSubTotal().add(lineItem.getAmount()));
            setTotal(getTotal().add(lineItem.getAmount()));
            for (int i = 0; i < allContent.size(); i++) {
                String current = (String) allContent.get(i);
                setCurrentLine(this.getCurrentLine() + 1);
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, current, "");
            }
        } catch (JposException je) {
            je.printStackTrace(CreamToolkit.getLogger());
        }
        lineItem.setPrinted(true);
    }

    public void printLineItem(LineItem lineItem) {
        if (!getPrintEnabled())
            return;

        if (ReceiptGenerator.isWorkable()) {
            printLineItemByReceiptGenerator(lineItem);
            return;
        }

        CreamProperties prop = CreamProperties.getInstance();

        String printerType = prop.getProperty("PrinterType");
        if (printerType != null && printerType.equalsIgnoreCase("OneStation")) {
            printLineItemOneStation(lineItem);
            return;
        }

        if (prop.getProperty("postype").equalsIgnoreCase("ptn")) {
            printLineItemPartner(lineItem);
        } else {
            printLineItemNormal(lineItem);
        }
    }

    public void printLineItemByReceiptGenerator(LineItem lineItem) {
        if (!getPrintEnabled())
            return;
        if (lineItem.getPrinted())
            return;

        //如果不需印代付，而且此交易为代付交易的话，直接返回
        if (!printDaiFu && lineItem.getDetailCode().equals("Q")) {
            return;
        }

        ReceiptGenerator gen = ReceiptGenerator.getInstance();
        try {
            POSPrinter printer = POSPeripheralHome.getInstance().getPOSPrinter();

            Collection lines;
            Iterator iter;
            if (lineItemsPrintedInThisPage >= gen.getLineItemPerPage()) {
                lines = gen.generateLines('B', lineItem);
                iter = lines.iterator();
                while (iter.hasNext()) {
                    linesPrintedInThisPage++;
                    printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL,
                            /*linesPrintedInThisPage +*/
                            iter.next().toString() + "\n", "");
                    gen.setLinesPrinted(linesPrintedInThisPage);
                    //ZhaoHong 2003-06-05
                    gen.setLinesBeforPaymentPrinted(linesPrintedInThisPage);

                }
                lines = gen.generateLines('P', lineItem);
                iter = lines.iterator();
                while (iter.hasNext()) {
                    String line = iter.next().toString();
                    linesPrintedInThisPage++;
                    if (line.indexOf("\u000C") != -1) {
                        printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL,
                                /*linesPrintedInThisPage +*/
                                line, "");
                    } else if (line.indexOf("$CUT_PAPER_AT_MARK$") != -1) {
                        printer.cutPaper(100);  //遇黑点切纸
                    } else if (line.indexOf("$CUT_PAPER$") != -1) {
                        printer.cutPaper(99);   //立即切纸
                    } else {
                        printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL,
                                /*linesPrintedInThisPage +*/
                                line + "\n", "");
                    }
                    gen.setLinesPrinted(linesPrintedInThisPage);
                    //ZhaoHong 2003-06-05
                    gen.setLinesBeforPaymentPrinted(linesPrintedInThisPage);

                }
                gen.setPageNumber(gen.getPageNumber() + 1);
                printHeaderByReceiptGenerator();
                lineItemsPrintedInThisPage = 0;
            }

            lines = gen.generateLines('D', lineItem);
            iter = lines.iterator();
            while (iter.hasNext()) {
                linesPrintedInThisPage++;
                String line = iter.next().toString() + "\n";
                //System.out.println("Print>" + line);
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL,
                        /*linesPrintedInThisPage +*/
                        line, "");
                gen.setLinesPrinted(linesPrintedInThisPage);
                //ZhaoHong 2003-06-05
                gen.setLinesBeforPaymentPrinted(linesPrintedInThisPage);

            }
            lineItemsPrintedInThisPage++;

        } catch (JposException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (NoSuchPOSDeviceException e) {
            //e.printStackTrace(CreamToolkit.getLogger());
        }
        lineItem.setPrinted(true);
    }

    public void printLineItemNormal(LineItem lineItem) {

        ResourceBundle res = CreamToolkit.GetResource();
        if (!getPrintEnabled())
            return;
        if (lineItem.getPrinted())
            return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        /***********************************************************************
         *Build a line item printing string from the line item object
         /***********************************************************************/
        ArrayList allContent = new ArrayList();
        String quantity = "";
        String voidFlag = "";
        if (lineItem.getDetailCode().equals("V")) {
            voidFlag = res.getString("Void") + "\n";
            allContent.add(voidFlag);
            //System.out.println(lineItem.getQuantity());
        }
        if (lineItem.getQuantity().doubleValue() > 1) {
            BigDecimal qty = new BigDecimal(lineItem.getQuantity().intValue());
            if (qty.compareTo(lineItem.getQuantity()) != 0)
                qty = lineItem.getQuantity();
            //System.out.println( lineItem.getQuantity().scale());
            quantity = qty + " x @" + lineItem.getUnitPrice() + "\n";
            allContent.add(quantity);
        }
        String lineItemContent = "";
        String printName = "";
        String totalPrice = "";
        int totalPriceLength = 0;
        int printNameLength = 0;
        String tax = "";
        int chars = 0;
        String spaces = "";
        //end of Variable definition

        //Tax related
        if (lineItem.getTaxType() != null && !lineItem.getTaxType().equals("")) {
            TaxType taxType = TaxType.queryByTaxID(lineItem.getTaxType());
            //System.out.println("tax = " + taxType);
            //System.out.println("type = " + taxType.getType());
            if (!taxType.getType().equals("0"))
                tax = "T";
        }
        if (lineItem.getPluNumber() != null) {
            if (lineItem.getDescription() == null)
                lineItem.setDescription("");
            if (lineItem.getDescription().equals("")) {
                PLU plu = PLU.queryByPluNumber(lineItem.getPluNumber());
                if (plu == null) {
                    if (lineItem.getPluNumber().length() > 0) {
                        //'08'：代收项目'09'：代付项目'10'：PaidIn项目'11'：PaidOut项目
                        String cat = "";
                        if (lineItem.getDetailCode().equals("I"))
                            cat = "08";
                        else if (lineItem.getDetailCode().equals("Q"))
                            cat = "09";
                        else if (lineItem.getDetailCode().equals("N"))
                            cat = "10";
                        else if (lineItem.getDetailCode().equals("T"))
                            cat = "11";
                        Reason r = Reason.queryByreasonNumberAndreasonCategory(lineItem.getPluNumber(), cat);
                        if (r != null)
                            printName = r.getreasonName();
                    } else {
                        TaxType t = TaxType.queryByTaxID(lineItem.getTaxType());
                        if (t != null) {
                            printName = t.getTaxName();
                        }
                    }
                } else {
                    String name = CreamProperties.getInstance().getProperty("PluPrintField");
                    if (name == null)
                        name = "ScreenName";
                    if (name.equals("ScreenName"))
                        printName = plu.getScreenName();
                    else
                        printName = plu.getPrintName();
                    if (printName == null)
                        printName = "";
                }
            } else {
                printName = lineItem.getDescription();
            }
        } else {
            if (lineItem.getDescription() == null)
                printName = "";
            else {
                printName = lineItem.getDescription();
            }
        }
        if (lineItem.getDetailCode().equalsIgnoreCase("X"))
            totalPrice = lineItem.getAmount().setScale(0, 4) + tax;
        else if (
                lineItem.getDetailCode().equals("I")
                        || lineItem.getDetailCode().equals("Q")
                        || lineItem.getDetailCode().equals("N")
                        || lineItem.getDetailCode().equals("T")) {
            totalPrice = lineItem.getUnitPrice() + tax;
        } else
            totalPrice = lineItem.getAmount() + tax;
        for (int i = 0; i < printName.length(); i++) {
            //Character c = new Character(printName.charAt(i));
            if (printName.charAt(i) > ASCII_SEPARATOR) {
                printNameLength += 2;
            } else {
                printNameLength += 1;
            }
        }
        totalPriceLength = totalPrice.length();
        chars = totalPriceLength + printNameLength;
        spaces = "  "; //Two spaces here
        if (chars > MAX_CHAR_LINE - spaces.length()) {
            if (printNameLength == printName.length()) {
                int offset = printName.length() - (chars - MAX_CHAR_LINE) - spaces.length();
                printName = printName.substring(0, offset);
            } else if (printNameLength == printName.length() * 2) {
                int offset = printNameLength - (chars - MAX_CHAR_LINE) - spaces.length();
                printName = printName.substring(0, offset / 2);
            } else {
                int offset = chars - MAX_CHAR_LINE + spaces.length();
                int position = 0;
                offset = printNameLength - offset;
                //System.out.println ("offset = " + offset);
                for (int i = 0; i < printName.length(); i++) {
                    if (printName.charAt(i) > ASCII_SEPARATOR) {
                        position += 2;
                    } else {
                        position += 1;
                    }
                    //int x = position - offset;
                    if (position >= offset) {
                        position = i;
                        break;
                    }
                }
                if (position < printName.length()) {
                    String printName1 = printName.substring(0, position);
                    printNameLength = 0;
                    for (int i = 0; i < printName1.length(); i++) {
                        //Character c = new Character(printName1.charAt(i));
                        if (printName1.charAt(i) > ASCII_SEPARATOR) {
                            printNameLength += 2;
                        } else {
                            printNameLength += 1;
                        }
                    } //cut the error here
                    int differ = MAX_CHAR_LINE - (printNameLength + spaces.length() + totalPrice.length());
                    if (differ > 0) { //shorter than MAX_CHAR_LINE
                        if (differ == 2) {
                            printName1 = printName.substring(0, position + 1);
                            String printName2 = printName.substring(0, position + 2);
                            if (printName1.charAt(printName1.length() - 1) <= ASCII_SEPARATOR) {
                                if (printName2.charAt(printName2.length() - 1) < ASCII_SEPARATOR)
                                    printName1 = printName2;
                                else
                                    spaces += " ";
                            }
                        } else if (differ == 1) {
                            printName1 = printName.substring(0, position + 1);
                            if (printName1.charAt(printName1.length() - 1) > ASCII_SEPARATOR) {
                                printName1 = printName.substring(0, position);
                                spaces += " ";
                            }
                        }
                    } else if (differ < 0) { //longer than MAX_CHAR_LINE
                        if (differ == -2) {
                            if (printName1.charAt(printName1.length() - 1) <= ASCII_SEPARATOR) {
                                printName1 = printName.substring(0, position - 2);
                                if (printName1.charAt(printName1.length() - 2) > ASCII_SEPARATOR)
                                    spaces += " ";
                            }
                        } else if (differ == -1) {
                            printName1 = printName.substring(0, position - 1);
                            if (printName1.charAt(printName1.length() - 1) > ASCII_SEPARATOR)
                                spaces += " ";
                        }
                    }
                    printName = printName1;
                }
            }
        } else if (chars < MAX_CHAR_LINE - spaces.length()) {
            int spacesAmount = spaces.length();
            for (int i = 0; i < MAX_CHAR_LINE - chars - spacesAmount; i++)
                spaces += " ";
        } //
        lineItemContent = printName + spaces + totalPrice + "\n";
        allContent.add(lineItemContent);
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            if (this.getCurrentLine() > MAX_ITEM_LINES) {
                printSubtotal();
                pageUp();
                setCurrentLine(1);
                setCurrentPage(getCurrentPage() + 1);
                printHeader();
                setSubTotal(getSubTotal().add(lineItem.getAmount()));
                setTotal(getTotal().add(lineItem.getAmount()));
            } else {
                setSubTotal(getSubTotal().add(lineItem.getAmount()));
                setTotal(getTotal().add(lineItem.getAmount()));
            }
            for (int i = 0; i < allContent.size(); i++) {
                String current = (String) allContent.get(i);
                setCurrentLine(this.getCurrentLine() + 1);
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, current, "");
                ITEM_LINES = ITEM_LINES + 1;
            }
        } catch (JposException je) {
            je.printStackTrace(CreamToolkit.getLogger());
        }
        lineItem.setPrinted(true);
    }

    public void printHeader(Transaction tran) {
        if (!getPrintEnabled())
            return;

        if (ReceiptGenerator.isWorkable()) {
            printHeaderByReceiptGenerator(tran);
            return;
        }

        printHeader(tran.getTransactionNumber().intValue());
    }

    public void printHeader(int number) {
        if (!getPrintEnabled())
            return;

//        if (ReceiptGenerator.isWorkable()) {
//            Transaction tran = Transaction.queryByTransactionNumber("" + number);
//            printHeaderByReceiptGenerator(tran);
//            return;
//        }

        CreamProperties creamProperties = CreamProperties.getInstance();
        if (creamProperties.getProperty("Print_Started").equalsIgnoreCase("true"))
            pageUp();
        creamProperties.setProperty("Print_Started", "true");
        creamProperties.deposit();

        String NextTransactionSequenceNumber = getFormattedTransactionNumber(number);

        //需要打乱交易序号
        if (needConfuseTranNumber != null && needConfuseTranNumber.equalsIgnoreCase("yes"))
            NextTransactionSequenceNumber = NumberConfuser.confuse(NextTransactionSequenceNumber);

        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Date currentTime_1 = new Date();
        //POSTerminalApplication posTerminal =
        //    POSTerminalApplication.getInstance();
        //Transaction t = posTerminal.getCurrentTransaction();
        String dateString = formatter.format(currentTime_1);
        //MM/DD/YYYY hh:mm | 機:01 | 2
        String toplogo = "";
        String lf = "";
        for (int i = 0; i < 3; i++) {
            lf = lf + "\n";
        }
        toplogo =
                toplogo
                        + dateString
                        + "   "
                        + res.getString("Machine")
                        + ":"
                        + creamProperties.getProperty("TerminalNumber")
                        + "\n";
        String StoreName = CreamProperties.getInstance().getProperty("StoreName");
        String CurrentPage = "";
        if (getCurrentPage() > 9)
            CurrentPage += getCurrentPage();
        else
            CurrentPage = "0" + getCurrentPage();

        String Bspace = "";
        for (int i = 0; i < MAX_CHAR_LINE - 5 - StoreName.length() * 2; i++)
            Bspace = Bspace + " ";
        //        toplogo += printNames.getString("Store") + ":" + StoreName  + Bspace + printNames.getString("Page") + ":" +  CurrentPage +"\n";
        toplogo += StoreName + Bspace + res.getString("Page") + ":" + CurrentPage + "\n";

        String cashierNo = creamProperties.getProperty("CashierNumber");
        String cashierName = "";
        if (cashierNo != null) {
            Cashier cas = Cashier.queryByCashierID(cashierNo);
            cashierName = (cas != null) ? cas.getCashierName() : "";
        }
        toplogo += res.getString("Cashier") + ":" + cashierName;
        String space = "";
        for (int i = 0; i < MAX_CHAR_LINE - 24; i++)
            space += " ";
        CurrentPage = "";
        if (getCurrentPage() > 9)
            CurrentPage += getCurrentPage();
        else
            CurrentPage = "0" + getCurrentPage();
        toplogo += space + res.getString("Number") + ":" + NextTransactionSequenceNumber + "\n";
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            //printStampI();
            setSubTotal(new BigDecimal(0));
            if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|1C", "");
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|N", "");
            }
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|lF", "");
            if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec"))
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\n\n\n", "");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, toplogo, "");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|1C", "");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|N", "");
            printer.printTwoNormal(
                    POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL,
                    "电话:" + storeTel + "\n地址:" + storeAddr + "\n\n",
                    "");
            //printer.setDeviceEnabled(false);
        } catch (JposException je) {
            CreamToolkit.logMessage(je.toString());
        } //
    }

    /**
     * 打印盘点表头（包括：门市编号名称、盘点时间、仓位编号、盘点人员编号）
     */
    public void printInventoryHeader() {
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e.getMessage());
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);

            String content = "*** 盘  点  数  据 ***\n";
            content += res.getString("Store") + ":"
                    + POSTerminalApplication.getInstance().getSystemInfo().getStoreNameAndNumber() + "\n";
            content += new SimpleDateFormat("时间:yyyy/MM/dd HH:mm:ss").format(new Date()) + "\n";
            content += "工号:" + InventoryState.getCashierNumber();
            content += "  仓位:" + InventoryStockState.getStockNumber() + "\n\n";

            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, content, "");
            printer.setDeviceEnabled(false);
        } catch (JposException e) {
            CreamToolkit.logMessage(e.getMessage());
        }
    }

    /**
     * 打印盘点记录
     */
    public void printInventoryItem(Inventory inv) {
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e.getMessage());
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);

            StringBuffer content = new StringBuffer(inv.getItemSequence().toString());
            content.append("  " + inv.getItemNumber());
            content.append(" " + inv.getDescription() + "\n");
            content.append("    @" + inv.getUnitPrice());
            content.append(" x " + inv.getActStockQty());
            content.append(" = " + inv.getAmount() + "\n");

            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, content.toString(), "");
            printer.setDeviceEnabled(false);
        } catch (JposException e) {
            CreamToolkit.logMessage(e.getMessage());
        }
    }

    /**
     * 打印盘点小计
     */
    public void printInventoryFooter() {
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException e) {
            CreamToolkit.logMessage(e.getMessage());
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);

            BigDecimal total = new BigDecimal(0);
            List dacList = POSTerminalApplication.getInstance().getDacViewer().getDacList();
            if (dacList != null && dacList.size() > 0) {
                Iterator iter = dacList.iterator();
                while (iter.hasNext()) {
                    total = total.add(((Inventory) iter.next()).getAmount());
                }
            }
            String content = "\n\n盘点零售金额小计:" + total.toString() + "\n\n\n\n\n\n\n\n";
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, content, "");
            printer.setDeviceEnabled(false);
        } catch (JposException e) {
            CreamToolkit.logMessage(e.getMessage());
        }
    }

    public void printHeaderPartner(int number) {
        if (!getPrintEnabled())
            return;
        CreamProperties creamProperties = CreamProperties.getInstance();
        if (creamProperties.getProperty("Print_Started").equalsIgnoreCase("true"))
            pageUp();
        creamProperties.setProperty("Print_Started", "true");
        creamProperties.deposit();
        String NextTransactionSequenceNumber = getFormattedTransactionNumber(number);
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Date currentTime_1 = new Date();
        POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
        Transaction t = posTerminal.getCurrentTransaction();
        String dateString = formatter.format(currentTime_1);
        String toplogo = "";
        String Bspace = "";
        //Max Line Char =  40  for Epson U210 PM
        // YYYY-MM-DD  序:00000001  机:01
        toplogo += creamProperties.getProperty("TopLogo1", "                欢 迎 光 临    ") + "\n";
        toplogo += creamProperties.getProperty("TopLogo2", "              远东好佳好便利店 \n") + "\n";
        toplogo = replaceLineFeed(toplogo);
        //toplogo += "                欢 迎 光 临    \n";
        //toplogo += "              远东好佳好便利店 \n\n";
        toplogo =
                toplogo
                        + dateString
                        + "    "
                        + res.getString("Number")
                        + ":"
                        + NextTransactionSequenceNumber
                        + "    ";
        toplogo =
                toplogo + res.getString("Machine") + ":" + creamProperties.getProperty("TerminalNumber") + "\n";
        // end of first line
        // 店:泓远测试店 收银员:魏成勇  页:01
        String StoreName = CreamProperties.getInstance().getProperty("StoreName");
        Bspace = "";
        for (int i = 0; i < 16 - (StoreName.length() * 2); i++) // 8 个汉字
            Bspace = Bspace + " ";
        toplogo += res.getString("Store") + ":" + StoreName + Bspace + " ";

        String cashierID = t.getSalesman();
        if (cashierID == null || cashierID.trim().equals(""))
            cashierID = creamProperties.getProperty("CashierNumber");
        String cashierName = cashierID;
        if (cashierID != null) {
            Cashier cas = Cashier.queryByCashierID(cashierID);
            if (cas != null)
                cashierName = cas.getCashierName();
        } else {
            cashierName = "NotFound";
        }

        for (int i = 0; i < 8 - cashierName.length(); i++) // 4 个汉字
            Bspace = "";
        Bspace = Bspace + " ";
        toplogo += res.getString("Cashier") + ":" + cashierName + Bspace + " ";
        String CurrentPage = "";
        if (getCurrentPage() > 9)
            CurrentPage += getCurrentPage();
        else
            CurrentPage = "0" + getCurrentPage();
        toplogo += res.getString("Page") + ":" + CurrentPage + "\n";
        toplogo += "----------------------------------------\n"; // 42
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            setSubTotal(new BigDecimal(0));
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, toplogo, "");
            //                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\n", "");
            printer.setDeviceEnabled(false);
        } catch (JposException je) {
            CreamToolkit.logMessage(je.toString());
        } //
    }

    /**
     * Print current transaction header.
     */
    public void printHeaderByReceiptGenerator() {
        Transaction trans = POSTerminalApplication.getInstance().getCurrentTransaction();

        //如果不需印代付，而且此交易为代付交易的话，直接返回
        if (!printDaiFu && trans.getDaiFuAmount() != null && trans.getDaiFuAmount().compareTo(zero) != 0)
            return;

        printHeaderByReceiptGenerator(trans);
    }

    /**
     * Print a given transaction's header.
     *
     * @param trans Transaction DAC.
     */
    public void printHeaderByReceiptGenerator(Transaction trans) {
        if (!getPrintEnabled())
            return;

        //如果不需印代付，而且此交易为代付交易的话，直接返回
        if (!printDaiFu && trans.getDaiFuAmount() != null && trans.getDaiFuAmount().compareTo(zero) != 0) {
            return;
        }

        linesPrintedInThisPage = 0;
        ReceiptGenerator.getInstance().setLinesPrinted(0);

        //ZhaoHong 2003-06-05
        ReceiptGenerator.getInstance().setLinesBeforPaymentPrinted(0);

        CreamProperties creamProperties = CreamProperties.getInstance();
        //if (creamProperties.getProperty("Print_Started").equalsIgnoreCase("true"))
        //    pageUp();
        creamProperties.setProperty("Print_Started", "true");
        creamProperties.deposit();

        ReceiptGenerator gen = ReceiptGenerator.getInstance();
        try {
            POSPrinter printer = POSPeripheralHome.getInstance().getPOSPrinter();

            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            //added by zhaohong 2003-12-10 tec 的打印机打印出来字体变大了，需要重新设置    
            if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec")) {
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|1C", "");
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\u001B|N", "");
            }

            setSubTotal(new BigDecimal(0));
            lineItemsPrintedInThisPage = 0;

            Collection lines;
            Iterator iter;
            if (firstPage) {
                lines = gen.generateLines('T', trans);
                iter = lines.iterator();
                while (iter.hasNext()) {
                    linesPrintedInThisPage++;
                    printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL,
                            /*linesPrintedInThisPage +*/
                            iter.next().toString() + "\n", "");
                    gen.setLinesPrinted(linesPrintedInThisPage);
                    //ZhaoHong 2003-06-05
                    gen.setLinesBeforPaymentPrinted(linesPrintedInThisPage);
                }
                firstPage = false;
            }

            lines = gen.generateLines('H', trans);
            iter = lines.iterator();
            while (iter.hasNext()) {
                linesPrintedInThisPage++;
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL,
                        /*linesPrintedInThisPage +*/
                        iter.next().toString() + "\n", "");
                gen.setLinesPrinted(linesPrintedInThisPage);
                //ZhaoHong 2003-06-05
                gen.setLinesBeforPaymentPrinted(linesPrintedInThisPage);

            }
            printer.setDeviceEnabled(false);
        } catch (JposException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (NoSuchPOSDeviceException e) {
            //e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    public void printHeaderOneStation(int number) {
        if (!getPrintEnabled())
            return;
        CreamProperties creamProperties = CreamProperties.getInstance();
        if (creamProperties.getProperty("Print_Started").equalsIgnoreCase("true"))
            pageUp();
        creamProperties.setProperty("Print_Started", "true");
        creamProperties.deposit();
        String NextTransactionSequenceNumber = getFormattedTransactionNumber(number);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        Date currentTime_1 = new Date();
        String dateString = formatter.format(currentTime_1);
        String toplogo = "";
        String Bspace = "";
        //Max Line Char =  40  for Epson U210 PM

        // YYYY-MM-DD  序:00000001  机:01
        if (veryFirstTime) {
            toplogo += creamProperties.getProperty("TopLogo1", "        Welcome") + "\n";
            toplogo += creamProperties.getProperty("TopLogo2", "        ") + "\n";
            veryFirstTime = false;
        }
        toplogo += dateString
                + "    "
                + res.getString("Number")
                + ":"
                + NextTransactionSequenceNumber
                + "    ";
        toplogo += res.getString("Machine") + ":" + creamProperties.getProperty("TerminalNumber") + "\n";

        // end of first line
        // 店:泓远测试店 收银员:魏成勇  页:01
        String StoreName = CreamProperties.getInstance().getProperty("StoreName");
        Bspace = "";
        for (int i = 0; i < 16 - (StoreName.length() * 2); i++) // 8 个汉字
            Bspace = Bspace + " ";
        toplogo += res.getString("Store") + ":" + StoreName + Bspace + " ";

        String cashierID = creamProperties.getProperty("CashierNumber");
        String cashierName = cashierID;
        if (cashierID != null) {
            Cashier cas = Cashier.queryByCashierID(cashierID);
            if (cas != null)
                cashierName = cas.getCashierName();
        } else {
            cashierName = "NotFound";
        }

        for (int i = 0; i < 8 - cashierName.length(); i++) // 4 个汉字
            Bspace = "";
        Bspace = Bspace + " ";
        toplogo += res.getString("Cashier") + ":" + cashierName + Bspace + " ";

        String CurrentPage = "";
        if (getCurrentPage() > 9)
            CurrentPage += getCurrentPage();
        else
            CurrentPage = "0" + getCurrentPage();
        toplogo += res.getString("Page") + ":" + CurrentPage + "\n";
        toplogo += "----------------------------------------\n"; // 42
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            setSubTotal(new BigDecimal(0));
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, toplogo, "");
            //                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, "\n", "");
            printer.setDeviceEnabled(false);
        } catch (JposException je) {
            CreamToolkit.logMessage(je.toString());
        }
    }

    public void printHeader() {
        if (!getPrintEnabled())
            return;

        if (ReceiptGenerator.isWorkable()) {
            printHeaderByReceiptGenerator();
            return;
        }

        int number = Transaction.getNextTransactionNumber();
        //CreamProperties.getInstance().getProperty("NextTransactionSequenceNumber");
        CreamProperties prop = CreamProperties.getInstance();

        String printerType = prop.getProperty("PrinterType");
        if (printerType != null && printerType.equalsIgnoreCase("OneStation")) {
            printHeaderOneStation(number);
            return;
        }

        if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("ptn")) {
            printHeaderPartner(number);
        } else
            printHeader(number);
    }

    public void printCancel(String cancelMsg) {
        //Otherwise, print "cancel" mark on receipt, feed and cut paper,
        if (!getPrintEnabled())
            return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            String line = cancelMsg + "\n";
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, line, line);

            pageUp();

            // 因为如果是用receipt.conf的话，已经打印'P' section了,里面已经有包含切纸了
            if (!ReceiptGenerator.isWorkable()) {
                if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec"))
                    printer.cutPaper(100);
            }
            setCurrentLine(1);
            setCurrentPage(1);
            printer.setDeviceEnabled(false);
        } catch (JposException je) {
            System.out.println(je);
        } //CreamToolkit.logMessage(je + "!"); return; }//

    }

    public void printStamp() {
        if (!getPrintEnabled())
            return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001B|sL");
        } catch (JposException je) {
            CreamToolkit.logMessage(je.toString());
        } //
    }

    public void printStampI() {
        if (!getPrintEnabled())
            return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            printer.printTwoNormal(
                    POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL,
                    "\u001B|2C Welcome to\n\u001B|2C  C-Store",
                    "");
        } catch (JposException je) {
            CreamToolkit.logMessage(je.toString());
        } //
    }
    /*
    折扣:   -999999999999.99
    提货卷: -999999999999.99
    合計:   999999999.99
    
    現金:    999999999999.99
    信用卡:  999999999999.99
    禮卷:    999999999999.99
    找零:    999999999999.99
    溢收:    999999999999.99
    會員卡:  999999999999999
    
    TX1:   99999999999999.99
    TX2:   99999999999999.99
    
    TZ:    99999999999999.99
    //*/

    public void printPayment(Transaction transac) {
        if (!getPrintEnabled())
            return;
        if (ReceiptGenerator.isWorkable()) {
            printPaymentByReceiptGenerator(transac);
            return;
        }

        CreamProperties prop = CreamProperties.getInstance();

        String printerType = prop.getProperty("PrinterType");
        if (printerType != null && printerType.equalsIgnoreCase("OneStation")) {
            printPaymentOneStation(transac);
            return;
        }

        if (prop.getProperty("postype").equalsIgnoreCase("ptn")) {
            printPaymentPartner(transac);
        } else
            printPaymentNormal(transac);
    }

    public void printPaymentByReceiptGenerator() {
        Transaction trans = POSTerminalApplication.getInstance().getCurrentTransaction();
        printPaymentByReceiptGenerator(trans);
    }

    public void printPaymentByReceiptGenerator(Transaction trans) {
        if (!getPrintEnabled())
            return;

        //如果不需印代付，而且此交易为代付交易的话，直接返回
        if (!printDaiFu && trans.getDaiFuAmount() != null && trans.getDaiFuAmount().compareTo(zero) != 0) {
            System.out.println("printPaymentByReceiptGenerator: return cuz daifu=" + trans.getDaiFuAmount());
            return;
        }

        ReceiptGenerator gen = ReceiptGenerator.getInstance();
        try {
            POSPrinter printer = POSPeripheralHome.getInstance().getPOSPrinter();
            linesPrintedInThisPage += gen.getBlankLinesBeforePayment();
            Collection lines = gen.generateLines('B', trans);
            Iterator iter = lines.iterator();
            while (iter.hasNext()) {

                linesPrintedInThisPage++;
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL,
                        /*linesPrintedInThisPage +*/
                        iter.next().toString() + "\n", "");
                gen.setLinesPrinted(linesPrintedInThisPage);
            }

            lines = gen.generateLines('F', trans);
            iter = lines.iterator();
            while (iter.hasNext()) {
                String line = iter.next().toString();
                linesPrintedInThisPage++;
                if (line.indexOf("\u000C") != -1) {
                    printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL,
                            /*linesPrintedInThisPage +*/
                            line, "");
                } else if (line.indexOf("$CUT_PAPER_AT_MARK$") != -1) {
                    printer.cutPaper(100);  //遇黑点切纸
                } else if (line.indexOf("$CUT_PAPER$") != -1) {
                    printer.cutPaper(99);   //立即切纸
                } else {
                    printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL,
                            /*linesPrintedInThisPage +*/
                            line + "\n", "");
                }
                gen.setLinesPrinted(linesPrintedInThisPage);
            }
            lineItemsPrintedInThisPage = 0;
            firstPage = true;
            gen.setPageNumber(1);

        } catch (JposException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (NoSuchPOSDeviceException e) {
            //e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    public void printPaymentNormal(Transaction transac) {
        if (!getPrintEnabled())
            return;
        if (transac == null)
            return;
        ArrayList currentPayment = new ArrayList();
        String currentLine = new String("");
        String lf = "\n";
        currentPayment.add(lf);
        currentLine = res.getString("Subtotal") + ":" + transac.getGrossSalesAmount() + "\n";
        if (transac.getNetSalesAmount().compareTo(transac.getGrossSalesAmount()) != 0) {
            currentPayment.add(currentLine); //小计
        }

        if (transac.getTotalMMAmount() != null) {
            if (transac.getTotalMMAmount().doubleValue() != 0) {
                currentLine = res.getString("MM") + ":" + transac.getTotalMMAmount().negate() + "\n";
                currentPayment.add(currentLine); //折扣
                //currentPayment.add(lf);
            } else if (transac.getMixAndMatchTotalAmount().doubleValue() != 0) {
                currentLine =
                        res.getString("MM") + ":" + transac.getMixAndMatchTotalAmount().negate() + "\n";
                currentPayment.add(currentLine); //折扣
            }
        }

        Iterator iter = transac.getPayments();
        int currentPayNumber = 0;
        while (iter.hasNext()) {
            currentPayNumber++;
            Payment payment = (Payment) iter.next();
            if (payment.getPaymentType().charAt(1) != '0') { //已开发票（提货卷）
                BigDecimal amount = transac.getPayAmountByID(String.valueOf(currentPayNumber));
                if (amount.doubleValue() != 0) {
                    currentLine = payment.getPrintName() + ":" + amount + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }
        //合计
        currentLine = res.getString("Total") + ":" + "\u001B|2C" + transac.getNetSalesAmount() + "\n";
        currentPayment.add(currentLine); //合计

        //代售
        if (transac.getDaiShouAmount() != null) {
            if (transac.getDaiShouAmount().doubleValue() != 0) {
                currentLine = res.getString("DaiShou") + ":" + transac.getDaiShouAmount() + "\n";
                currentPayment.add(currentLine);
            }
        }
        //代收
        if (transac.getDaiShouAmount2() != null) {
            if (transac.getDaiShouAmount2().doubleValue() != 0) {
                currentLine = res.getString("DaiShou2") + ":" + transac.getDaiShouAmount2() + "\n";
                currentPayment.add(currentLine);
            }
        }
        //代付
        if (transac.getDaiFuAmount() != null) {
            if (transac.getDaiFuAmount().doubleValue() != 0) {
                currentLine = res.getString("DaiFu") + ":" + transac.getDaiFuAmount() + "\n";
                currentPayment.add(currentLine);
            }
        }

        currentPayment.add(lf);

        //现金
        //信用卡
        //禮卷

        if (transac.getPayAmount1() != null) {
            if (transac.getPayAmount1().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber1());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine = payment.getPrintName() + ":" + transac.getPayAmount1() + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        if (transac.getPayAmount2() != null) {
            if (transac.getPayAmount2().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber2());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine = payment.getPrintName() + ":" + transac.getPayAmount2() + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        if (transac.getPayAmount3() != null) {
            if (transac.getPayAmount3().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber3());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine = payment.getPrintName() + ":" + transac.getPayAmount3() + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        if (transac.getPayAmount4() != null) {
            if (transac.getPayAmount4().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber4());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine = payment.getPrintName() + ":" + transac.getPayAmount4() + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        if (transac.getChangeAmount() != null) {
            if (transac.getChangeAmount().doubleValue() != 0) {
                currentLine = res.getString("Change") + ":" + transac.getChangeAmount() + "\n";
                currentPayment.add(currentLine);
            }
        } //找零
        //溢收
        if (transac.getSpillAmount() != null) {
            if (transac.getSpillAmount().doubleValue() != 0) {
                currentLine = res.getString("Over") + ":" + transac.getSpillAmount() + "\n";
                currentPayment.add(currentLine);
            }
        } //溢收

        currentPayment.add(lf);
        /**
         * The end of string building
         */
        if (currentPayment.isEmpty())
            return;
        //Iterator itr = currentPayment.iterator();
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            for (int i = 0; i < currentPayment.size(); i++) {
                String line = (String) currentPayment.get(i);
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, line, "");
            }
            pageUp();
            printer.cutPaper(100);
            setCurrentLine(1);
            setCurrentPage(1);
            printer.setDeviceEnabled(false);
        } catch (JposException je) {
            CreamToolkit.logMessage(je + "!");
            return;
        }
        setTax0Amount(new BigDecimal(0));
        setTax1Amount(new BigDecimal(0));
        setTax2Amount(new BigDecimal(0));
        setTax3Amount(new BigDecimal(0));
        setTax4Amount(new BigDecimal(0));
    }

    private void printPaymentPartner(Transaction transac) {
        if (!getPrintEnabled())
            return;

        if (transac == null)
            return;
        ArrayList currentPayment = new ArrayList();
        String currentLine = new String("");
        //String lf = "\n";
        currentPayment.add("----------------------------------------\n");
        String space = "";
        for (int i = 0;
             i
                     < EndSpace
                     - (res.getString("Subtotal").length() * 2)
                     - 2
                     - transac.getGrossSalesAmount().toString().length();
             i++) {
            space += " ";
        }
        currentLine = res.getString("Subtotal") + ":" + " " + transac.getGrossSalesAmount() + space;
        if (transac.getNetSalesAmount().compareTo(transac.getGrossSalesAmount()) != 0) {
            currentPayment.add(currentLine); //小计
        }
        if (transac.getTotalMMAmount() != null) {
            if (transac.getTotalMMAmount().doubleValue() != 0) {
                currentLine = res.getString("MM") + ":" + transac.getTotalMMAmount().negate() + "\n";
                currentPayment.add(currentLine); //折扣
                //currentPayment.add(lf);
            } else if (transac.getMixAndMatchTotalAmount().doubleValue() != 0) {
                currentLine =
                        res.getString("MM") + ":" + transac.getMixAndMatchTotalAmount().negate() + "\n";
                currentPayment.add(currentLine); //折扣
            }
        }

        Iterator iter = transac.getPayments();
        int currentPayNumber = 0;
        while (iter.hasNext()) {
            currentPayNumber++;
            Payment payment = (Payment) iter.next();
            if (payment.getPaymentType().charAt(1) != '0') { //已开发票（提货卷）
                BigDecimal amount = transac.getPayAmountByID(String.valueOf(currentPayNumber));
                if (amount.doubleValue() != 0) {
                    currentLine = payment.getPrintName() + ":" + amount + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        space = "";
        for (int i = 0;
             i
                     < EndSpace
                     - (res.getString("Total").length() * 2)
                     - 2
                     - transac.getNetSalesAmount().toString().length();
             i++) {
            space += " ";
        }
        currentLine =
                res.getString("Total") + ":" + " " + transac.getNetSalesAmount().setScale(2, 4) + space;
        currentPayment.add(currentLine); //合计

        //代售
        if (transac.getDaiShouAmount() != null) {
            if (transac.getDaiShouAmount().doubleValue() != 0) {
                currentLine =
                        res.getString("DaiShou") + ":" + transac.getDaiShouAmount().setScale(2, 4) + "\n";
                currentPayment.add(currentLine);
            }
        }
        //代收
        if (transac.getDaiShouAmount2() != null) {
            if (transac.getDaiShouAmount2().doubleValue() != 0) {
                currentLine =
                        res.getString("DaiShou2") + ":" + transac.getDaiShouAmount2().setScale(2, 4) + "\n";
                currentPayment.add(currentLine);
            }
        }

        if (transac.getDaiFuAmount() != null) {
            if (transac.getDaiFuAmount().doubleValue() != 0) {
                currentLine = res.getString("DaiFu") + ":" + transac.getDaiFuAmount().setScale(2, 4) + "\n";
                currentPayment.add(currentLine);
            }
        }
        if (transac.getPayAmount1() != null) {
            if (transac.getPayAmount1().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber1());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine =
                            payment.getPrintName() + ":" + transac.getPayAmount1().setScale(2, 4) + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        if (transac.getPayAmount2() != null) {
            if (transac.getPayAmount2().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber2());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine =
                            payment.getPrintName() + ":" + transac.getPayAmount2().setScale(2, 4) + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        if (transac.getPayAmount3() != null) {
            if (transac.getPayAmount3().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber3());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine =
                            payment.getPrintName() + ":" + transac.getPayAmount3().setScale(2, 4) + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        if (transac.getPayAmount4() != null) {
            if (transac.getPayAmount4().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber4());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine =
                            payment.getPrintName() + ":" + transac.getPayAmount4().setScale(2, 4) + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }
        if (transac.getChangeAmount() != null) {
            if (transac.getChangeAmount().doubleValue() != 0) {
                int newLength =
                        res.getString("Total").length() + transac.getNetSalesAmount().toString().length() + 1;
                for (int i = 0; i < newLength + 3; i++)
                    space += " ";
                currentLine =
                        space + res.getString("Change") + ":" + transac.getChangeAmount().setScale(2, 4) + "\n";
                currentPayment.add(currentLine);
            }
        } //找零

        //溢收
        if (transac.getSpillAmount() != null) {
            if (transac.getSpillAmount().doubleValue() != 0) {
                currentLine = res.getString("Over") + ":" + transac.getSpillAmount().setScale(2, 4) + "\n";
                currentPayment.add(currentLine);
            }
        } //溢收
        if (currentPayment.isEmpty())
            return;
        //Iterator itr = currentPayment.iterator();
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            try {
                printer = posHome.getPOSPrinter();
            } catch (NoSuchPOSDeviceException ex) {
            }
            for (int i = 0; i < currentPayment.size(); i++) {
                String line = (String) currentPayment.get(i);
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, line, "");
            }
            //String BottomLogo = "\n             24 小时为您服务     \n";
            //BottomLogo += "             五 心 级 便 利 店     \n\n";
            CreamProperties prop = CreamProperties.getInstance();
            String bottomLogo = prop.getProperty("BottomLogo1", "\n             24 小时为您服务     \n");
            bottomLogo += prop.getProperty("BottomLogo2", "             五 心 级 便 利 店     \n\n");
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, replaceLineFeed(bottomLogo), "");
            setCurrentLine(1);
            setCurrentPage(1);
            printer.cutPaper(100);
            printer.setDeviceEnabled(false);
        } catch (JposException je) {
            CreamToolkit.logMessage(je + "!");
            return;
        }
    }

    public String replaceLineFeed(String data) {
        while (true) {
            int i = data.indexOf("\\n");
            if (i == -1)
                break;
            data = data.substring(0, i) + "\n" + data.substring(i + 2);
        }
        return data;
    }

    private void printPaymentOneStation(Transaction transac) {
        if (!getPrintEnabled())
            return;

        if (transac == null)
            return;
        ArrayList currentPayment = new ArrayList();
        String currentLine = new String("");
        //String lf = "\n";
        currentPayment.add("----------------------------------------\n");
        String space = "";
        for (int i = 0;
             i
                     < EndSpace
                     - (res.getString("Subtotal").length() * 2)
                     - 2
                     - transac.getGrossSalesAmount().toString().length();
             i++) {
            space += " ";
        }
        currentLine = res.getString("Subtotal") + ":" + " " + transac.getGrossSalesAmount() + space;
        if (transac.getNetSalesAmount().compareTo(transac.getGrossSalesAmount()) != 0) {
            currentPayment.add(currentLine); //小计
        }
        if (transac.getTotalMMAmount() != null) {
            if (transac.getTotalMMAmount().doubleValue() != 0) {
                currentLine = res.getString("MM") + ":" + transac.getTotalMMAmount().negate() + "\n";
                currentPayment.add(currentLine); //折扣
                //currentPayment.add(lf);
            } else if (transac.getMixAndMatchTotalAmount().doubleValue() != 0) {
                currentLine =
                        res.getString("MM") + ":" + transac.getMixAndMatchTotalAmount().negate() + "\n";
                currentPayment.add(currentLine); //折扣
            }
        }

        Iterator iter = transac.getPayments();
        int currentPayNumber = 0;
        while (iter.hasNext()) {
            currentPayNumber++;
            Payment payment = (Payment) iter.next();
            if (payment.getPaymentType().charAt(1) != '0') { //已开发票（提货卷）
                BigDecimal amount = transac.getPayAmountByID(String.valueOf(currentPayNumber));
                if (amount.doubleValue() != 0) {
                    currentLine = payment.getPrintName() + ":" + amount + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        space = "";
        for (int i = 0;
             i
                     < EndSpace
                     - (res.getString("Total").length() * 2)
                     - 2
                     - transac.getNetSalesAmount().toString().length();
             i++) {
            space += " ";
        }
        currentLine =
                res.getString("Total") + ":" + " " + transac.getNetSalesAmount().setScale(2, 4) + space;
        currentPayment.add(currentLine); //合计

        //代售
        if (transac.getDaiShouAmount() != null) {
            if (transac.getDaiShouAmount().doubleValue() != 0) {
                currentLine =
                        res.getString("DaiShou") + ":" + transac.getDaiShouAmount().setScale(2, 4) + "\n";
                currentPayment.add(currentLine);
            }
        }
        //代收
        if (transac.getDaiShouAmount2() != null) {
            if (transac.getDaiShouAmount2().doubleValue() != 0) {
                currentLine =
                        res.getString("DaiShou2") + ":" + transac.getDaiShouAmount2().setScale(2, 4) + "\n";
                currentPayment.add(currentLine);
            }
        }

        if (transac.getDaiFuAmount() != null) {
            if (transac.getDaiFuAmount().doubleValue() != 0) {
                currentLine = res.getString("DaiFu") + ":" + transac.getDaiFuAmount().setScale(2, 4) + "\n";
                currentPayment.add(currentLine);
            }
        }
        if (transac.getPayAmount1() != null) {
            if (transac.getPayAmount1().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber1());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine =
                            payment.getPrintName() + ":" + transac.getPayAmount1().setScale(2, 4) + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        if (transac.getPayAmount2() != null) {
            if (transac.getPayAmount2().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber2());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine =
                            payment.getPrintName() + ":" + transac.getPayAmount2().setScale(2, 4) + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        if (transac.getPayAmount3() != null) {
            if (transac.getPayAmount3().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber3());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine =
                            payment.getPrintName() + ":" + transac.getPayAmount3().setScale(2, 4) + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }

        if (transac.getPayAmount4() != null) {
            if (transac.getPayAmount4().doubleValue() != 0) {
                Payment payment = Payment.queryByPaymentID(transac.getPayNumber4());
                if (payment.getPaymentType().charAt(1) != '1') {
                    //已开过发票（目前只有提货卷）
                    currentLine =
                            payment.getPrintName() + ":" + transac.getPayAmount4().setScale(2, 4) + "\n";
                    currentPayment.add(currentLine);
                }
            }
        }
        if (transac.getChangeAmount() != null) {
            if (transac.getChangeAmount().doubleValue() != 0) {
                int newLength =
                        res.getString("Total").length() + transac.getNetSalesAmount().toString().length() + 1;
                for (int i = 0; i < newLength + 3; i++)
                    space += " ";
                currentLine =
                        space + res.getString("Change") + ":" + transac.getChangeAmount().setScale(2, 4) + "\n";
                currentPayment.add(currentLine);
            }
        } //找零

        //溢收
        if (transac.getSpillAmount() != null) {
            if (transac.getSpillAmount().doubleValue() != 0) {
                currentLine = res.getString("Over") + ":" + transac.getSpillAmount().setScale(2, 4) + "\n";
                currentPayment.add(currentLine);
            }
        } //溢收
        if (currentPayment.isEmpty())
            return;
        //Iterator itr = currentPayment.iterator();
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            try {
                printer = posHome.getPOSPrinter();
            } catch (NoSuchPOSDeviceException ex) {
            }
            for (int i = 0; i < currentPayment.size(); i++) {
                String line = (String) currentPayment.get(i);
                printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, line, "");
            }
            CreamProperties prop = CreamProperties.getInstance();

            String bottomLogo = "";
            String cashierID = transac.getSalesman();
            if (cashierID != null) {
                String cashierName = cashierID;
                Cashier cas = Cashier.queryByCashierID(cashierID);
                if (cas != null) {
                    cashierName = cas.getCashierName();
                    bottomLogo = res.getString("Salesman") + ":" + cashierName + "\n";
                }
            }

            bottomLogo += prop.getProperty("BottomLogo1", "      Thanks for coming.\n");
            bottomLogo += prop.getProperty("BottomLogo2", "      See you next time!\n");
            bottomLogo += prop.getProperty("TopLogo1", "        Welcome") + "\n";
            bottomLogo += prop.getProperty("TopLogo2", "        ") + "\n";
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, bottomLogo, "");
            setCurrentLine(1);
            setCurrentPage(1);
            printer.cutPaper(100);
            printer.setDeviceEnabled(false);
        } catch (JposException je) {
            CreamToolkit.logMessage(je + "!");
            return;
        }
    }

    public void printSubtotal() {
        if (!getPrintEnabled())
            return;
        if (getSubTotal().doubleValue() == 0)
            return;
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        String subtotalContent =
                "\n" + res.getString("Subtotal") + ":" + getSubTotal().setScale(2, 4) + "\n\n";
        //BigDecimal subTotal
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            printer.printTwoNormal(POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL, subtotalContent, "");
            if (CreamProperties.getInstance().getProperty("postype").equalsIgnoreCase("tec"))
                printer.cutPaper(100);
        } catch (JposException je) {
            CreamToolkit.logMessage(je + "!");
            return;
        } //
        //setSubTotal(new BigDecimal(0));
    }

    public void printFirstReceipt() {
        if (!getPrintEnabled())
            return;
        String command = "";
        //cut paper at the point;//feed 36 lines
        //command = "\u001B|2lF";
        //print the stammp
        command += "\u001B|V";
        command = "\u001B|8lF";
        command += "\u001B|P";
        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
        POSPrinter printer = null;
        try {
            printer = posHome.getPOSPrinter();
        } catch (NoSuchPOSDeviceException ne) {
            CreamToolkit.logMessage(ne + "!");
            return;
        }
        try {
            if (!printer.getClaimed())
                printer.claim(1000);
            if (!printer.getDeviceEnabled())
                printer.setDeviceEnabled(true);
            //command = "\u001B|2lF";
            //printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, command);
            //printer.cutPaper(100);
            printer.setDeviceEnabled(false);
        } catch (JposException je) {
            CreamToolkit.logMessage(je + "!");
            return;
        }
    }

    public BigDecimal getTax0Amount() {
        return tax0Amount;
    }

    public void setTax0Amount(BigDecimal amt) {
        tax0Amount = amt;
    }

    public BigDecimal getTax1Amount() {
        return tax1Amount;
    }

    public void setTax1Amount(BigDecimal amt) {
        tax1Amount = amt;
    }

    public BigDecimal getTax2Amount() {
        return tax2Amount;
    }

    public void setTax2Amount(BigDecimal amt) {
        tax2Amount = amt;
    }

    public BigDecimal getTax3Amount() {
        return tax3Amount;
    }

    public void setTax3Amount(BigDecimal amt) {
        tax3Amount = amt;
    }

    public BigDecimal getTax4Amount() {
        return tax4Amount;
    }

    public void setTax4Amount(BigDecimal amt) {
        tax4Amount = amt;
    }

    public void reprint(int number, boolean isPageCut, boolean isHisTrans) {
        if (!getPrintEnabled())
            return;
        String transactionNumber = String.valueOf(number);
        Transaction previousTransaction = Transaction.queryByTransactionNumber(transactionNumber);
        if (previousTransaction == null)
            return;
        reprint(previousTransaction, isPageCut, isHisTrans);
    }

    public void reprint(Transaction tran, boolean isPageCut, boolean isHisTrans) {
        if (!getPrintEnabled())
            return;
        printHeader(tran); //.getTransactionNumber().intValue());
        //Iterator itr = p0.getLineItems();
        Object[] lines = tran.getLineItemsArrayLast();
        int length = tran.getLineItemsArrayLast().length;
        for (int i = 0; i < length; i++) {
            LineItem li = (LineItem) lines[i];
            if (li.getPrinted())
                li.setPrinted(false);
            if (li.getDetailCode().compareTo("M") == 0 || li.getDetailCode().compareTo("E") == 0
                    || li.getDetailCode().compareTo("A") == 0)
                continue;
            else if (li.getDetailCode().compareTo("S") == 0 && li.getRemoved() && i < length - 1) {
                LineItem lineItem = (LineItem) lines[i + 1];
                if (lineItem.getDetailCode().compareTo("E") == 0)
                    continue;
            }
            printLineItem(tran, li, isPageCut, isHisTrans);
        }
        printPayment(tran, isPageCut);

    }

    public void printLineItem(Transaction tran, LineItem lineItem, boolean isPageCut, boolean isHisTrans) {

        if (!getPrintEnabled()) {
            return;
        }

        if (lineItem.getPrinted()) {
            return;
        }

        if (ReceiptGenerator.isWorkable()) {
            //printLineItemByReceiptGenerator(tran, lineItem, isPageCut, isHisTrans);
            printLineItemByReceiptGenerator(lineItem);
            return;
        }

        // 2004.10.21

        String printerType = CreamToolkit.GetResource().getString("PrinterType");
        if (printerType != null && printerType.equalsIgnoreCase("OneStation")) {
            printLineItemOneStation(lineItem);
            return;
        }


        printLineItemNormal(lineItem);

    }

    public void printPayment(Transaction transac, boolean isPageCut) {
        if (!getPrintEnabled())
            return;

        if (ReceiptGenerator.isWorkable()) {
            printPaymentByReceiptGenerator(transac);
            return;
        }


    }
}
