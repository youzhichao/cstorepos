// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) fieldsfirst 
// Source File Name:   CreamProperties.java

package hyi.cream.util;

import hyi.cream.dac.Store;
import hyi.cream.inline.Server;
import java.io.*;
import java.sql.*;
import java.util.*;

// Referenced classes of package hyi.cream.util:
//            CreamToolkit

/**
 * Class for accessing Cream properties. Now the properties is stored in 
 * "property" table.
 */
public class CreamProperties extends Properties {
    private static final long serialVersionUID = 1L;

    private static CreamProperties instance = null;
    private static Properties properties = new Properties();
    private static Properties origProperties = new Properties();
    private static Set modifiedPropertySet = Collections.synchronizedSet(new HashSet());
    private static Set newPropertySet = Collections.synchronizedSet(new HashSet());
    private static Connection connection = null;
    private static Statement statement = null;

    public CreamProperties() throws InstantiationException {
        if (instance != null) {
            throw new InstantiationException(toString());
        } else {
            initProperties();
            instance = this;
            return;
        }
    }

    public static CreamProperties getInstance() {
        try {
            if (instance == null)
                instance = new CreamProperties();
        } catch (InstantiationException ie) {
            return instance;
        }
        return instance;
    }

    private void initProperties() {
        if (Server.serverExist()) {
            try {
                File propFile = new File("cream.conf");
                // get cream.conf in current directory on server side
                if (!propFile.exists()) {
                    put("Locale", "zh_CN");
                    put("ServerDatabaseDriver", "com.mysql.jdbc.Driver");
                    put("ServerDatabaseURL", "jdbc:mysql://localhost/cake");
                    put("ServerDatabaseUser", "root");
                    put("ServerDatabaseUserPassword", "");
                    put("ServerDatabaseInitialConnections", "5");
                    put("ServerDatabaseConnectionIncrement", "5");
                    put("LogFile", "cream.log");
                    put("InlineClientUseJVM", "YES");
                    put("InlineServerLogFile", "/root/inline.log");
                    put("MaxLineItems", "50");
                    FileOutputStream outProp = new FileOutputStream(propFile);
                    store(outProp, "Cream Configuration");
                    outProp.close();
                    //System.out.println("Default Cream properties file is generated: " +
                    //    propFile.getAbsoluteFile());
                    //System.out.println("");
                }
                load(new FileInputStream(propFile));
            } catch (IOException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        } else {
            Properties dbConf = null;
            File connectionFile = new File("connection.conf");
            if (connectionFile.exists()) {
                try {
                    dbConf = new Properties();
                    FileInputStream fis = new FileInputStream(connectionFile);
                    dbConf.load(fis);
                    fis.close();
                } catch (Exception e) {
                }
            }
            
//            try {
//                Class.forName("com.mysql.jdbc.Driver").newInstance();
//            } catch (Exception e) {
//                //!!!!!Bruce> try an old one
//                try {
//                    Class.forName("org.gjt.mm.mysql.Driver").newInstance();
//                } catch (Exception e1) {
//                    e.printStackTrace(CreamToolkit.getLogger());
//                    // Cannot load any properties here!!!
//                    return;
//                }
//            }
            Properties dbProp = new Properties();
            if (dbConf != null) {
                dbProp.put("user", dbConf.getProperty("db.user", "root"));
                dbProp.put("password", dbConf.getProperty("db.password", ""));
                dbProp.put("autoReconnect", dbConf.getProperty("db.autoReconnect", "true"));
                dbProp.put("url", dbConf.getProperty("db.url", "jdbc:mysql://localhost/cream"));
                dbProp.put("driver", dbConf.getProperty("db.driver", "com.mysql.jdbc.Driver"));
            } else {
                dbProp.put("user", "root");
                dbProp.put("password", "");
                dbProp.put("autoReconnect", "true");
                dbProp.put("url", "jdbc:mysql://localhost/cream");
                dbProp.put("driver", "com.mysql.jdbc.Driver");
            }
            try {
                Class.forName(dbProp.getProperty("driver")).newInstance();
            } catch (Exception e) {
                e.printStackTrace(CreamToolkit.getLogger());
                // Cannot load any properties here!!!
                return;
            }
            
            ResultSet resultSet = null;
            try {
                connection = DriverManager.getConnection(dbProp.getProperty("url"), dbProp);
                statement = connection.createStatement();
                for (resultSet = statement.executeQuery("SELECT * FROM property"); resultSet.next(); origProperties.put(resultSet.getObject(1), resultSet.getObject(2)))
                    properties.put(resultSet.getObject(1), resultSet.getObject(2));

            } catch (SQLException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            } finally {
                try {
                    if (resultSet != null)
                        resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace(CreamToolkit.getLogger());
                }
            }
        }
    }

    /**
     * Store the properties into table "property".
     */
    public synchronized void deposit() {
        if (Server.serverExist()) {
            String fileName = "cream.conf";
            File propFile = new File(fileName);
            try {
                FileOutputStream outProp = new FileOutputStream(propFile);
                store(outProp, "Cream Configuration");
            } catch (IOException e) {
                e.printStackTrace(CreamToolkit.getLogger());
                return;
            }
        } else {
            try {
                // Test the database connection.
                try {
                    if (!Server.serverExist())
                        connection.setAutoCommit(true);
                } catch (SQLException e) {
                    Properties dbProp = new Properties();
                    dbProp.put("user", "root");
                    dbProp.put("password", "");
                    dbProp.put("autoReconnect", "true");
                    try {
                        connection = DriverManager.getConnection("jdbc:mysql://localhost/cream", dbProp);
                        statement = connection.createStatement();
                    } catch (SQLException e2) {
                        e2.printStackTrace(CreamToolkit.getLogger());
                        return;
                    }
                }
                synchronized (newPropertySet) {
                    for (Iterator iter = newPropertySet.iterator(); iter.hasNext();) {
                        String name = (String) iter.next();
                        try {
                            statement.executeUpdate("INSERT INTO property (name,value) values ('" + name + "','" + properties.getProperty(name) + "')");
                            origProperties.setProperty(name, properties.getProperty(name));
                        } catch (SQLException e) {
                            e.printStackTrace(CreamToolkit.getLogger());
                        }
                    }

                }
                synchronized (modifiedPropertySet) {
                    for (Iterator iter = modifiedPropertySet.iterator(); iter.hasNext();) {
                        String name = (String) iter.next();
                        try {
                            statement.executeUpdate("UPDATE property SET value='" + properties.getProperty(name) + "' WHERE name='" + name + "'");
                        } catch (SQLException e) {
                            e.printStackTrace(CreamToolkit.getLogger());
                        }
                    }

                }
                modifiedPropertySet.clear();
                newPropertySet.clear();
            } catch (ConcurrentModificationException e) {
                e.printStackTrace(CreamToolkit.getLogger());
            }
        }
    }

    public String getProperty(String name, String defaultValue) {
        String ret = getProperty(name);
        return ret != null ? ret : defaultValue;
    }

    public String getProperty(String name) {
        // Bruce> Get store id and name from "store" table instead
        if (name.equalsIgnoreCase("StoreNumber"))
            return Store.getStoreID();
        if (name.equalsIgnoreCase("StoreName"))
            return Store.getStoreChineseName();
        if (Server.serverExist())
            return super.getProperty(name);
        else
            return properties.getProperty(name);
    }

    public Object setProperty(String name, String value) {
        if (Server.serverExist())
            return super.setProperty(name, value);
        properties.setProperty(name, value);
        if (origProperties.containsKey(name))
            modifiedPropertySet.add(name);
        else
            newPropertySet.add(name);
        return value;
    }

    public boolean containsKey(String s) {
        if (Server.serverExist())
            return super.containsKey(s);
        else
            return properties.containsKey(s);
    }

    public void put(String name, String value) {
        if (Server.serverExist()) {
            super.put(name, value);
        } else {
            properties.put(name, value);
            if (origProperties.containsKey(name))
                modifiedPropertySet.add(name);
            else
                newPropertySet.add(name);
        }
    }

    public static void main(String args[]) {
        try {
            CreamProperties c = new CreamProperties();
            System.out.println("containsKey = " + c.containsKey("log"));
            System.out.println("get'log' = " + c.getProperty("log"));
            c.put("x", "x");
            c.setProperty("x", "xxx");
            c.deposit();
        } catch (InstantiationException instantiationexception) {
        }
    }

}
