package hyi.cream.util;

/**
 * ResourceBundle class for Cream.
 *
 */
public class CreamResource extends java.util.ListResourceBundle {

	static final Object[][] contents = {
	};

    protected Object[][] getContents() {
	    return contents;
    }
}