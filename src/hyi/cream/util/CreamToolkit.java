package hyi.cream.util;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.Date;
import java.text.*;
import java.sql.*;
import java.math.*;
import jpos.*;
import hyi.cream.dac.*;
import hyi.cream.*;
import hyi.cream.uibeans.*;
import hyi.cream.state.*;
import java.lang.reflect.*;

/**
 * CreamToolkit class.
 *
 * @author Dai, Slackware, Bruce
 * @version 1.6
 */
public class CreamToolkit {
    /**
     * Meyer/1.6/2003-01-22
     * 		增加2组get,set反射方法：
     * 			getAmountMethod,setAmountMethod
     * 			getCountMethod,setCountMethod
     *
     * /Bruce/1.6/2002-04-26/
     *    Fix a problem when property "DatabaseConnectionIncrement" does not
     *    exist.
     *
     * /Bruce/1.5/2002-04-01/
     *    Fix a problem when conn.setAutoCommit(true) is failed. (On inline
     *    server side)
     */
    transient public static final String VERSION = "1.6";

    static private Image image;

    static public final int LOG_NONE = -1;
    static public final int LOG_NORMAL = 0;
    static public final int LOG_VERBOSE = 1;

    static private Map connections =  Collections.synchronizedMap(new HashMap());
    static private Map connections2 = Collections.synchronizedMap(new HashMap());

    static private PrintWriter logger;
    static private int logLevel = LOG_NORMAL;
    static private SimpleDateFormat dateFormatter;
    static private boolean deviceClaimed = false;
    static private DateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
    static Properties dbProp = new Properties();

    static {
        CreamProperties prop = CreamProperties.getInstance();
        try {
            logger = new PrintWriter(
                new OutputStreamWriter(
                    new FileOutputStream(prop.getProperty("LogFile"), true), CreamToolkit.getEncoding()));
            dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss ");

            // Create connection pool
            int initialConnections;
            if (hyi.cream.inline.Server.serverExist()) {
                Class.forName(prop.getProperty("ServerDatabaseDriver")).newInstance();
                initialConnections = Integer.parseInt(
                    prop.getProperty("ServerDatabaseInitialConnections"));
                dbProp.put("user", prop.getProperty("ServerDatabaseUser"));
                dbProp.put("password", prop.getProperty("ServerDatabaseUserPassword"));
                dbProp.put("useUnicode", "TRUE");
                dbProp.put("characterEncoding", "GBK");
                dbProp.put("autoReconnect", "true");
            } else {
                Class.forName(prop.getProperty("DatabaseDriver")).newInstance();
                initialConnections = Integer.parseInt(
                    prop.getProperty("DatabaseInitialConnections"));
                dbProp.put("user", prop.getProperty("DatabaseUser"));
                dbProp.put("password", prop.getProperty("DatabaseUserPassword"));
                dbProp.put("autoReconnect", "true");
                String inlineClientUseJVM = prop.getProperty("InlineClientUseJVM");
                if (inlineClientUseJVM != null && inlineClientUseJVM.compareToIgnoreCase("YES") == 0) {
                    dbProp.put("useUnicode", "TRUE");
                    dbProp.put("characterEncoding", "GBK");
                }
            }

            for (int i = 0; i < initialConnections; i++) {
                if (hyi.cream.inline.Server.serverExist()) {
                    connections.put(
                        DriverManager.getConnection(prop.getProperty("ServerDatabaseURL"), dbProp),
                        Boolean.FALSE);
                } else {
                    connections.put(
                        DriverManager.getConnection(prop.getProperty("DatabaseURL"), dbProp),
                        Boolean.FALSE);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (Exception e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }


    public CreamToolkit() {
    }

    /**
     * Get image by a name.
     */
    static public Image getImage(String s) {
        image = Toolkit.getDefaultToolkit().getImage(
            getConfigDir() + s);
        return image;
    }

    static public Image getNextImage(String s) {
        image = Toolkit.getDefaultToolkit().getImage(
            getConfigDir() + "2" + s);
        return image;
    }

    /**
     * 取得configuration file(*.conf)所在目录。
     * 
     * @return String Configuration file directory.
     */
    static public String getConfigDir() {
        String configDir = CreamProperties.getInstance().getProperty("ConfigDir", "." + File.separator);
        if (!configDir.endsWith(File.separator))
            configDir += File.separator;
        return configDir;
    }

    /**
     * Get a configuration "File" by a String.
     */
    static public File getConfigurationFile(String c) {
        String baseDir = getConfigDir();
        if (c.equals("connectMsgBanner"))
            return new File(baseDir + "connmsg.conf");
        else if (c.equals("pageMsgBanner"))
            return new File(baseDir + "pagemsg.conf");
        else if (c.equals("productInfoBanner"))
            return new File(baseDir + "productinfo.conf");
        else if (c.equals("totalAmountBanner"))
            return new File(baseDir + "totalmsg.conf");
        else if (c.equals("screenBanner2"))
            return new File(baseDir + "screenbanner2.conf");
        else if (c.equals("payingPane1"))
            return new File(baseDir + "payingpane1.conf");
        else if (c.equals("payingPane2"))
            return new File(baseDir + "payingpane2.conf");
        else if (c.equals("agelevel"))
            return new File(baseDir + "agelevel.conf");
        else
            return null;
    }

    /**
     * Get a configuration "File" by a "Class" object.
     */
    static public File getConfigurationFile(Class c) {
        String baseDir = getConfigDir();
        if (c.equals(ItemList.class))
            return new File(baseDir + "itemlist.conf");
        else if (c.equals(PayingPane.class))
            return new File(baseDir + "payingpane.conf");
        else if (c.equals(ScreenBanner.class))
            return new File(baseDir + "screenbanner.conf");
        else if (c.equals(POSButtonHome.class))
            return new File(baseDir + "posbutton3.conf");
        else if (c.equals(POSPeripheralHome.class))
            return new File(baseDir + "posdevices3.conf");
        else if (c.equals(StateMachine.class))
            return new File(baseDir + "statechart3.conf");
        else if (c.equals(ShiftReport.class))
            return new File(baseDir + "shiftreport.conf");
        else if (c.equals(ZReport.class))
            return new File(baseDir + "zreport.conf");
        else if (c.equals(KeyLock1State.class))
            return new File(baseDir + "keylock1.conf");
        else if (c.equals(KeyLock2State.class))
            return new File(baseDir + "keylock2.conf");
        else if (c.equals(KeyLock3State.class))
            return new File(baseDir + "keylock3.conf");
        else if (c.equals(ConfigState.class))
            return new File(baseDir + "config.conf");
        else if (c.equals(CashForm.class))
            return new File(baseDir + "cashform.conf");
        else if (c.equals(Integer.class))
            return new File(baseDir + "agelevel.conf");
        else
            return null;
    }

    static public ResourceBundle GetResource() {
        //java.util.Locale.setDefault(new java.util.Locale("zh","TW",""));
        //java.util.Locale.setDefault(java.util.Locale.TRADITIONAL_CHINESE);
        //java.util.Locale.setDefault(new java.util.Locale("zh","CN",""));
        //java.util.Locale.setDefault(java.util.Locale.SIMPLIFIED_CHINESE);

        if (CreamProperties.getInstance().getProperty("Locale").equals("zh_TW")) {
            return java.util.ResourceBundle.getBundle("hyi.cream.util.CreamResource_zh_TW");
        } else if (CreamProperties.getInstance().getProperty("Locale").equals("zh_CN")) {
            return java.util.ResourceBundle.getBundle("hyi.cream.util.CreamResource_zh_CN");
        } else {
            return java.util.ResourceBundle.getBundle("hyi.cream.util.CreamResource");
        }
    }

    static public java.util.List getResultData2(String sql) {
        java.util.List data = new ArrayList();
        if (sql == null || sql.trim().equals(""))
            return data;
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            conn = getPooledConnection();
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Map record = new HashMap();
                int count = rs.getMetaData().getColumnCount();
                for (int i = 1; i <= count; i++)
                    record.put(rs.getMetaData().getColumnName(i), rs
                            .getObject(i));
                data.add(record);
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace(getLogger());
        } finally {
            try {
                rs.close();
                st.close();
            } catch (Exception e) {
            }
            releaseConnection(conn);
        }
        return data;
    }

    static public java.util.Date getInitialDate() {
        java.util.Date initDate = new java.util.Date(0);
        int timeOffset = 0;
        TimeZone timeZone = Calendar.getInstance().getTimeZone();
        timeOffset = timeOffset - timeZone.getRawOffset();
        initDate.setTime(timeOffset);
        return initDate;
    }

    static public void openLogger() {
        try {
            CreamProperties prop = CreamProperties.getInstance();
            logger = new PrintWriter(
                new OutputStreamWriter(
                    new FileOutputStream(prop.getProperty("LogFile"), true), CreamToolkit.getEncoding()));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    static public PrintWriter getLogger() {
        return logger;
    }

    static public int getLogLevel() {
        return logLevel;
    }

    static public void setLogLevel(int level) {
        logLevel = level;
    }

    static public void logMessage(String msg) {
        logMessage(LOG_NORMAL, msg);
    }

    static public void logVerboseMessage(String msg) {
        logMessage(LOG_VERBOSE, msg);
    }

    static public void logMessage(int logLevel, String msg) {
        if (getLogLevel() == LOG_VERBOSE || getLogLevel() == LOG_NORMAL) {
            java.util.Date currentTime = new java.util.Date();
            String dateString = dateFormatter.format(currentTime);
            if (logger != null) {
                logger.print(dateString);
                logger.println(msg);
                logger.flush();
            }
        }
    }

    /**
     * Log exception message.
     *
     * @since Bruce, 20070314 
     */
    static public void logMessage(Throwable e) {
        logMessage(LOG_NORMAL, e.getMessage());
        e.printStackTrace(getLogger());
        e.printStackTrace();
    }

    static public void closeLog() {
        if (logger != null) {
            logger.close();
            logger = null;
        }
    }

    /**
     * Write transaction log into directory "tranlog/[yyyy-MM-dd].sql" under current
     * working directory.
     * 
     * @param insertStatement INSERT statement string.
     */
    static public void writeTransactionLog(String insertStatement) {
        File tranLogDir = new File("tranlog");
        if (!tranLogDir.exists()) {
            if (!tranLogDir.mkdir()) {
                logMessage("Err> Create tranlog directory failed.");
                return;
            }
        }
        
        try {
            Writer tranLog = new FileWriter(tranLogDir + File.separator
                + yyyyMMdd.format(new Date()) + ".sql", true);
            tranLog.write(insertStatement);
            if (!insertStatement.endsWith(";") &&
                (insertStatement.startsWith("INSERT") || insertStatement.startsWith("insert")
                || insertStatement.startsWith("DELETE") || insertStatement.startsWith("delete")
                || insertStatement.startsWith("UPDATE") || insertStatement.startsWith("update")))
                tranLog.write(';');       
            tranLog.write('\n');
            tranLog.close();       
        } catch (IOException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
    }

    static public Connection getPooledConnection() throws SQLException {
        CreamProperties prop = CreamProperties.getInstance();
        Connection conn = null;
        Set connSet = connections.keySet();
        Iterator conns = connSet.iterator();

        synchronized (connections) {
            while (conns.hasNext()) {
                conn = (Connection)conns.next();
                Boolean b = (Boolean)connections.get(conn);
                if (b == Boolean.FALSE) {
                    try {
                        conn.setAutoCommit(true);
                    } catch (SQLException e) {
                        // Problem with the connection, replace it
                        logMessage("Failed on conn.setAutoCommit(true). Retrieve connection again.");
                        try {
                            if (hyi.cream.inline.Server.serverExist())
                                conn = DriverManager.getConnection(prop.getProperty("ServerDatabaseURL"), dbProp);
                            else
                                conn = DriverManager.getConnection(prop.getProperty("DatabaseURL"), dbProp);
                        } catch (SQLException e2) {
                            logMessage("Retrieve connection failed again.");
                            e2.printStackTrace(getLogger());
                            throw e;
                        }
                    }
                    // Update the HashMap to show this one's taken
                    connections.put(conn, Boolean.TRUE);
                    // Return the connection
                    return conn;
                }
            }
        }

        // If we get here, there were no free connection.
        // We've got to make more.
        int increment;
        try {
            increment = Integer.parseInt(prop.getProperty("DatabaseConnectionIncrement"));
        } catch (NumberFormatException e) {
            increment = 5; // default
        }
        logMessage("Databse connection is not enough, get another " + increment + " connections.");
        for (int i = 0; i < increment; i++) {
            if (hyi.cream.inline.Server.serverExist())
                connections.put(DriverManager.getConnection(prop.getProperty("ServerDatabaseURL"), dbProp), Boolean.FALSE);
            else
                connections.put(DriverManager.getConnection(prop.getProperty("DatabaseURL"), dbProp), Boolean.FALSE);
        }
        logMessage("Databse connection pool size= " + connections.size());

        // Recurse to get one of the new connections.
        return getPooledConnection();
    }

    static public Connection getPooledConnection2() throws SQLException {
        return null;
    }


    static public void releaseAll() throws Exception {
        //jos.system.curses.Window.end();
        CreamToolkit.logMessage("About to close local connection!");
        Connection conn = null;
        Set connSet = connections.keySet();
        Iterator conns = connSet.iterator();
        synchronized (connections) {
            while (conns.hasNext()) {
                conn = (Connection)conns.next();
                if (conn!= null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        connections.remove(conn);
                        continue;
                    }
                }
            }
        }
        /*
        CreamToolkit.logMessage("" + "About to close remote connection!");
        if (!DacTransfer.getInstance().isConnected(100))
            return;
        CreamToolkit.logMessage("" + "About to close all remote connections!");
        conn = null;
        connSet = connections2.keySet();
        conns = connSet.iterator();
        synchronized (connections2) {
            while (conns.hasNext()) {
                conn = (Connection)conns.next();
                if (conn!= null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        connections2.remove(conn);
                        continue;
                    }
                }
            }
        }
        */
    }

    static public void releaseConnection(Connection conn) {
        synchronized (connections) {
            Boolean b = (Boolean)connections.get(conn);
            if (b == Boolean.TRUE)
                connections.put(conn, Boolean.FALSE);
        }
    }

    static public void releaseConnection2(Connection conn) {
        if (conn == null)
            return;
        synchronized (connections2) {
            //CreamToolkit.logMessage("" + "!!!!!!!!connections2.size()= " + connections2.size());
            Boolean b = (Boolean)connections2.get(conn);
            try {
                if (b == null)
                    conn.close();
            } catch (SQLException se) {
            } finally {
                if (b == null)
                    return;
            }
            if (b.booleanValue())
                connections2.put(conn, Boolean.FALSE);
            //System.out. println("!!!!!@" + connections2.values());
        }
    }

    static public boolean checkInput(String number, int i) {
        try {
            Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    static public boolean checkInput(String number, java.math.BigDecimal b) {
        try {
            new java.math.BigDecimal(number);
        } catch (NumberFormatException e) {
            CreamToolkit.logMessage("" + e);
            return false;
        } catch (StringIndexOutOfBoundsException e) {
            CreamToolkit.logMessage("" + e);
            return false;
        }
        return true;
    }

    static public void showText(Transaction t, int mode) {
        if (mode == 1) {

            BigDecimal salesAmount = t.getSalesAmount();
            BigDecimal change = t.getChangeAmount();
            CreamToolkit.logMessage("tranno:"+t.getTransactionNumber()+";"+"change:"+ change);
            if (change == null) {
                change = new BigDecimal(0);
            }

            String zeros = "";

            //  check upString
            String saString = salesAmount.toString();
            int len = salesAmount.scale();
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            if (saString.endsWith(zeros)) {
                salesAmount = salesAmount.setScale(0, BigDecimal.ROUND_HALF_UP);
            } else {
                salesAmount = salesAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            saString = salesAmount.toString();

            //  check upString
            String pcString = change.toString();
            len = change.scale();
            zeros = "";
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            if (pcString.endsWith(zeros)) {
                change = change.setScale(0, BigDecimal.ROUND_HALF_UP);
            } else {
                change = change.setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            pcString = change.toString();

            //  create linedisplay string
            String oneString = "";
            String twoString = "";
            oneString = oneString + "CHANGE     ";
            while (pcString.length() < 9) {
                pcString = " " + pcString;
            }
            oneString = oneString + pcString;
            twoString = "SUBTOTAL:";
            while (saString.length() < 11) {
                saString = " " + saString;
            }
            twoString = twoString + saString;

            //  show text
            try {
                LineDisplay lineDisplay = POSTerminalApplication.getInstance().getPOSPeripheralHome().getLineDisplay();

                if (!deviceClaimed) {
                    if (!lineDisplay.getClaimed()){
                        lineDisplay.claim(0);
                    }
                    lineDisplay.setDeviceEnabled(true);
                    deviceClaimed = true;
                }

                lineDisplay.clearText();
                //System.out.println("CreamToolkit showText() 1 one = " + oneString);
                lineDisplay.displayText(oneString, 1);
                //System.out.println("CreamToolkit showText() 1 two = " + twoString);
                lineDisplay.displayText(twoString, 2);
            } catch (JposException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Jpos exception at show text on LineDisplay");
            } catch(NoSuchPOSDeviceException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("No such POSDevice: LineDisplay, at show text on LineDisplay");
            }
        } else {
            LineItem l = t.getCurrentLineItem();
            if (l == null) {
                return;
            }

            BigDecimal unitPrice = l.getUnitPrice();
            BigDecimal quantity = l.getQuantity();
            BigDecimal amount = l.getAmount();
            BigDecimal salesAmount = t.getSalesAmount();
            //System.out.println("CreamToolkit showText() --> sales amount = " + salesAmount);

            String zeros = "";

            //  check upString
            String upString = unitPrice.toString();
            int len = unitPrice.scale();
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            if (upString.endsWith(zeros)) {
                unitPrice = unitPrice.setScale(0, BigDecimal.ROUND_HALF_UP);
            } else {
                unitPrice = unitPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            upString = unitPrice.toString();

            //  check upString
            String qtString = quantity.toString();
            len = quantity.scale();
            zeros = "";
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            if (qtString.endsWith(zeros)) {
                quantity = quantity.setScale(0, BigDecimal.ROUND_HALF_UP);
            } else {
                quantity = quantity.setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            qtString = quantity.toString();

            //  check upString
            zeros = "";
            String atString = amount.toString();
            len = amount.scale();
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            if (atString.endsWith(zeros)) {
                amount = amount.setScale(0, BigDecimal.ROUND_HALF_UP);
            } else {
                amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            atString = amount.toString();

            //  check upString
            zeros = "";
            String saString = salesAmount.toString();
            len = salesAmount.scale();
            for (int i = 0; i < len; i++) {
                zeros = zeros + "0";
            }
            if (saString.endsWith(zeros)) {
                salesAmount = salesAmount.setScale(0, BigDecimal.ROUND_HALF_UP);
            } else {
                salesAmount = salesAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            saString = salesAmount.toString();

            //  create linedisplay string
            String oneString = "";
            String twoString = "";
            while (upString.length() < 6) {
                upString = upString + " ";
            }
            oneString = oneString + upString + "X";
            while (qtString.length() < 3) {
                qtString = " " + qtString;
            }
            oneString = oneString + qtString + "=";
            while (atString.length() < 9) {
                atString = " " + atString;
            }
            oneString = oneString + atString;
            twoString = "SUBTOTAL:";
            while (saString.length() < 11) {
                saString = " " + saString;
            }
            twoString = twoString + saString;

            //  show text
            try {
                LineDisplay lineDisplay = POSTerminalApplication.getInstance().getPOSPeripheralHome().getLineDisplay();

                if (!deviceClaimed) {
                    lineDisplay.claim(0);
                    lineDisplay.setDeviceEnabled(true);
                    deviceClaimed = true;
                }

                lineDisplay.clearText();
                //System.out.println("CreamToolkit showText() 2 one = " + oneString);
                lineDisplay.displayText(oneString, 1);
                //System.out.println("CreamToolkit showText() 2 two = " + twoString);
                lineDisplay.displayText(twoString, 2);
            } catch (JposException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("Jpos exception at show text on LineDisplay");
            } catch(NoSuchPOSDeviceException e) {
                CreamToolkit.logMessage(e.toString());
                CreamToolkit.logMessage("No such POSDevice: LineDisplay, at show text on LineDisplay");
            }
        }
    }

    public static Class getExitClass(int key) {
        switch (key) {
        case 6:
            return ConfigState.class;
        case 1:
            return null;
        case 2:
            return InitialState.class;
        case 3:
            return KeyLock2State.class;
        //case 4:
        //    return KeyLock1State.class;
        case 4:
            return KeyLock3State.class;
        }
        return null;
    }

    public static void shrinkFile(File file, long maxSize) {
        try {
            if (!file.exists())
                return;
            if (file.length() <= maxSize)
                return;

            File tempFile = File.createTempFile("smf", "log", file.getParentFile());
            BufferedInputStream inf = new BufferedInputStream(new FileInputStream(file));
            if (inf.skip(file.length() - maxSize) < 0) {
                inf.close();
                return;
            }

            BufferedOutputStream outf = new BufferedOutputStream(
                new java.io.FileOutputStream(tempFile));
            int c;
            while ((c = inf.read()) != -1) {
                outf.write(c);
            }
            inf.close();
            outf.close();

            file.delete();
            tempFile.renameTo(file);

        } catch (IOException e) {
        }
    }


	/**
	 * getAmount的反射方法
	 * @author Meyer
	 * @param invokeObj - Object, 调用方法的对象
	 * @param mehtodName - String, 调用的方法名
	 * @return result - BigDecimal 返回的amount
	 */
	public static BigDecimal getAmountMethod(Object invokeObj, String methodName) {
		BigDecimal result = null;
    	try {
            Method method = invokeObj.getClass().getDeclaredMethod(methodName, new Class[0]);
            result = (BigDecimal)method.invoke(invokeObj, new Object[0]);
        } catch (NoSuchMethodException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InvocationTargetException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (ClassCastException e) {
    		e.printStackTrace(CreamToolkit.getLogger());
    	}
        return result;
	}

	/**
	 * setAmount的反射方法
	 * @author Meyer
	 * @param invokeObj - Object, 调用方法的对象
	 * @param mehtodName - String, 调用的方法名
	 * @param amt - BigDecimal, 要设置的新值
	 */
	public static void setAmountMethod(Object invokeObj, String methodName, BigDecimal amt) {
		Class[] classArray = {BigDecimal.class};
		Object[] paramObj = {amt};
    	try {
            Method method = invokeObj.getClass().getDeclaredMethod(methodName, classArray);
            method.invoke(invokeObj, paramObj);
        } catch (NoSuchMethodException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InvocationTargetException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
	}


	/**
	 * getCount的反射方法
	 * @author Meyer
	 * @param invokeObj - Object, 调用方法的对象
	 * @param mehtodName - String, 调用的方法名
	 * @return result - int, 返回的count
	 */
	public static int getCountMethod(Object invokeObj, String methodName) {
		int result = 0;
    	try {
            Method method = invokeObj.getClass().getDeclaredMethod(methodName, new Class[0]);
            result = ((Integer)method.invoke(invokeObj, new Object[0])).intValue();
        } catch (NoSuchMethodException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InvocationTargetException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (ClassCastException e) {
    		e.printStackTrace(CreamToolkit.getLogger());
    	}
        return result;
	}


	/**
	 * setCount的反射方法
	 * @author Meyer
	 * @param invokeObj - Object, 调用方法的对象
	 * @param mehtodName - String, 调用的方法名
	 * @param count - int, 要设置的新值
	 */
	public static void setCountMethod(Object invokeObj, String methodName, int count) {
		Class[] classArray = {Integer.class};
		Object[] paramObj = {new Integer(count)};
    	try {
            Method method = invokeObj.getClass().getDeclaredMethod(methodName, classArray);
            method.invoke(invokeObj, paramObj);
        } catch (NoSuchMethodException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (InvocationTargetException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        } catch (IllegalAccessException e) {
            e.printStackTrace(CreamToolkit.getLogger());
        }
	}

    /**
     * Get language encoding.
     */
    public static String getEncoding() {
        return "GB2312";
    }

    /**
     * Get the check digit of EAN13 bar code.
     * 
     * @param barCode barCode number string
     * @return String the C/D.
     */
    static public String getEAN13CheckDigit(String barCode) {
        if (barCode.length() < 12)
            return null;
        int decode = 0;
        for (int i = 0; i < 12; i++) {
            int k = (int)barCode.charAt(11 - i) - 48;
            if (i % 2 == 0)
                k = (k * 3) % 10;
            else
                k %= 10;
            decode += k;
        }
        if (decode % 10 != 0)
            decode = 10 - decode % 10;
        else
            decode = 0;
        return String.valueOf(decode);
    }

    /**
     * Check if the EAN bar code's check digit is correct.
     * 
     * @param barCode EAN13 bar code number string.
     * @return boolean true if correct, false otherwise.
     */
    static public boolean checkEAN13(String barCode) {
        String cd = CreamToolkit.getEAN13CheckDigit(barCode);
        return (cd == null) ? false : barCode.endsWith(cd);
    }

    //public static void main(String[] args) {
    //    try {
    //        Class.forName("com.mysql.jdbc.Driver").newInstance();
    //        //con = DriverManager.getConnection("jdbc:mysql://192.168.1.46/cream1"/*sc.cstore.com.cn/cake"*/,"root","");
    //    } catch (Exception e) {
    //        e.printStackTrace();
    //    }
    //    CreamToolkit c = new CreamToolkit();
    //}
    //
}


