package hyi.jpos.services;


import java.util.*;

public interface BaseParsing {

    //An array to store all the static Jpos ESC sequence in it.
    //A static Jpos ESC sequence is the string that does not contain any
    //variable parameter. For example, "\u001B|100P" is not a static
    //Jpos ESC sequence because it contains a variable parameter: 100.
    public final static String[] JposESC = { "\u001B|rA", "\u001B|cA", "\u001B|1C", "\u001B|2C", "\u001B|3C",
                           "\u001B|4C", "\u001B|rC", "\u001B|rvC", "\u001B|iC", "\u001B|bC", "\u001B|N",
                           "\u001B|P", "\u001B|fP", "\u001B|sP", "\u001B|lF", "\u001B|sL", "\u001B|bL",
                           "\u001B|tL", "\u001B|uC", "\u001B|sC" };
    // Properties
    public String getCommandStart ();
    public String getCommandEnd ();

    // Methods

    //Method to get all the substring of data that starts with "\u001B|"
    //and store them in an ArrayList. Any substring in the ArrayList will
    //ends with an uppercase alphabetic character or another escape sequence.
    //(For later parsing purpose)
    public ArrayList getESCSequence (String data);

    //Method to get rid of the redundant escape sequence which does not end with a
    //uppercase alphabetic character.
    public ArrayList getValidESCSequence (String data);

    //Method to get all the valid escape sequence which is in accordance with
    //the Jpos specification.
    public ArrayList getJposESCSequence (String data);

    //if item is contained in data[], the method will return true,
    //otherwise false.
    public boolean contains (String data[], String item);

    public String getValidCommand (int station, String data);

    public String getValidCommand (int station, String data1, String data2);

}


