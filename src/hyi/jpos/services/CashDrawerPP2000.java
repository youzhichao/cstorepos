package hyi.jpos.services;

//for JavaCommAPI
import java.io.*;
import java.util.*;
import javax.comm.*;
import java.awt.*;

//for POSPrinterService14 interface
import jpos.*;
import jpos.services.*;

//for callback events
import jpos.events.*;

//for entry registry
import jpos.config.*;
import jpos.config.simple.*;


//import hyi.cream.util.*;
import hyi.jpos.util.*;


public class CashDrawerPP2000 extends DeviceService implements SerialPortEventListener,
	CashDrawerService14 {
    //--------------------------------------------------------------------------
    // Variables
    //--------------------------------------------------------------------------
    
    private OutputStreamWriter dataOutput = null;
    static private ShareOutputPort shareOutput = null;  //Singleton object;
    private String logicalName = null;//
    
    private JposEntry cashDrawerEntry = null;
    private OutputStream outputStream = null;
    private InputStream inputStream = null;
	private String portName = "COM2";
    private StringBuffer buffer = new StringBuffer();
	//private Object waitObject = new Object();
    
    private Object waitBeep = new Object();//for wait purpose;

    //--------------------------------------------------------------------------
    //private byte[] readPort = new byte[10];
	//private char statusCode = '0';

    //Constructor
	public CashDrawerPP2000 (SimpleEntry entry) {
        cashDrawerEntry = entry;
	}
	
	public CashDrawerPP2000 () {
	}

    // Capabilities
    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties 13
    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        throw new JposException (JposConst.JPOS_E_NOSERVICE, "No service available now!");
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    //Common method -- Not supported
    public void checkHealth(int level)  throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    // Properties specified
    public boolean getCapStatus() throws JposException {
        return true;
    }
    
	public boolean getDrawerOpened() throws JposException {
		try {
			if (!shareOutput.isSerialPortOpened()) {
                shareOutput.open(logicalName, portName, 0, this);
             }
        } catch (PortInUseException pe) {
			throw new JposException (JposConst.JPOS_E_FAILURE, "Port in use!", pe);
        }
		outputStream = shareOutput.getOutputStream();
		dataOutput = shareOutput.getOutputStreamWriter();
		inputStream = shareOutput.getInputStream();
		try {
			dataOutput.write("\u001Bu\u0000\r");
			dataOutput.flush();
		} catch (IOException ie)  {
            throw new JposException (JposConst.JPOS_E_FAILURE, "Fail to query the status!", ie);
		}
		catch (Exception e)  {
			e.printStackTrace();
		}
		synchronized (shareOutput.waitObject) {
			try {
				shareOutput.waitObject.wait(2500);
			} catch (InterruptedException ie) {
			    ie.printStackTrace();
			}
		}
	    if (shareOutput.getDrawerStatus() == 1)
			return true;
		else
			return false;
    }

    // Methods
	public void openDrawer() throws JposException {
        if (!getDeviceEnabled())
			throw new JposException(JposConst.JPOS_E_DISABLED,"Cash drawer is not enabled!");
		try {
            if (!shareOutput.isSerialPortOpened()) {
				shareOutput.open(logicalName, portName, 0, this);
			}
		} catch (PortInUseException pe) {
            throw new JposException (JposConst.JPOS_E_FAILURE, "Port in use", pe);
        }
		OutputStream out = shareOutput.getOutputStream();

        try {
           out.write(0x1B);
           out.write('p');
           out.write(0x00);
           out.write(0x19);
           out.write(0xF0);
        } catch (IOException e) {
           e.printStackTrace(System.err);
        }

//        dataOutput = shareOutput.getOutputStreamWriter();
//        inputStream = shareOutput.getInputStream();
//        String command = "";
//        try {
//            command ="\u001BG\r";
//            dataOutput.write(command);
//            dataOutput.flush();
//        } catch  (IOException e) {
//            throw new JposException (JposConst.JPOS_E_FAILURE , "IO Error at" + this, e);
//		}
		//System.out.println(getDrawerOpened());
		/*
		if (getDrawerOpened())
			return;
		else
			openDrawer();//*/
		//CASH_SUE_DRAWEROPEN (=1) The drawer is open.
		/*StatusUpdateEvent statusEvent = new StatusUpdateEvent(eventCallbacks.getEventSource(),
			CashDrawerConst.CASH_SUE_DRAWEROPEN);
		eventCallbacks.fireStatusUpdateEvent(statusEvent);
		*/
	}

	// If there's no StatusUpdateEvent listener for your cash drawer instance,
	// A java.lang.NullPointerException will occure
	//    at com.hyi.jpos.services.CashDrawerPP2000.waitForDrawerClose(CashDrawerPP2000.java:190)
	//    at com.hyi.jpos.services.CashDrawerPP2000.main(CashDrawerPP2000.java:359)


	public void waitForDrawerClose(int beepTimeout, int beepFrequency,
					   int beepDuration, int beepDelay) throws JposException {
	   if (!getDeviceEnabled())
			throw new JposException(JposConst.JPOS_E_DISABLED,"Cash drawer is not enabled!");
		/*
		if (!getDrawerOpened()) {
			synchronized (this) {
				try {
					this.wait(1500);
				} catch (InterruptedException ie) {
				}
				eventCallbacks.fireStatusUpdateEvent(new StatusUpdateEvent(eventCallbacks.getEventSource(),
					CashDrawerConst.CASH_SUE_DRAWERCLOSED));//
			}
			return;
		}
		*/
		
        new Beep(beepTimeout, beepDelay);
        synchronized (waitBeep) {
            try {
				waitBeep.wait();
            } catch (InterruptedException ie) {
                if (!getDrawerOpened())
                    return;
                else
                    throw new JposException (JposConst.JPOS_E_FAILURE, this.toString(), ie);
            }
        }
        eventCallbacks.fireStatusUpdateEvent(new StatusUpdateEvent(eventCallbacks.getEventSource(),
			CashDrawerConst.CASH_SUE_DRAWERCLOSED));//*/
    }

    class Beep extends Thread {
        private int beepTimeout;
        private int beepDelay;
        public Beep() {
            start();
        }
        
        public Beep(int beepTimeout, int beepDelay) {
            this.beepTimeout = beepTimeout;
            this.beepDelay = beepDelay;
            start();
        }
        public void run() {
            try {
                long start = System.currentTimeMillis();
                long waitTime = beepTimeout;
                try {
					while (waitTime > 0) {
                        if (!getDrawerOpened()) {
							synchronized (waitBeep) {
                                waitBeep.notifyAll();
                                return;
                            }
                        }
						waitTime = beepTimeout - (System.currentTimeMillis() - start);
                    }
                    while (true){
                        if (!getDrawerOpened()) {
							synchronized (waitBeep) {
                                waitBeep.notifyAll();
                                return;
                            }
                        }
                        Toolkit.getDefaultToolkit().beep();
						sleep(beepDelay);
					}
				} catch (JposException je) {
					je.printStackTrace();
                }
			} catch (InterruptedException e) {
                e.printStackTrace();
			}
        }
    }

	public void  open(String logicalName, EventCallbacks cb) throws JposException {
		super.open (logicalName, cb);
		if (cashDrawerEntry != null)
    		portName = (String)cashDrawerEntry.getPropertyValue("PORT_NAME");
		this.logicalName = logicalName;
		shareOutput = ShareOutputPort.getInstance();
    }

    public void close() throws JposException {
        if (getClaimed())
            release();
        super.close();
        this.logicalName = null;
        shareOutput = null;
    }

    public void claim(int timeout) throws JposException {
    
    }

    public void release() throws JposException {

	}

	public void serialEvent(SerialPortEvent event) {
	    switch(event.getEventType()) {
			case SerialPortEvent.BI:
            case SerialPortEvent.OE:   // for errorEventListener callback
            case SerialPortEvent.FE:   // need defined altogether with other devices */
            case SerialPortEvent.PE:
                /*
                 JPOS_ER_RETRY /Retry the asynchronous output. The error state is exited. The default.
                 JPOS_ER_CLEAR /Clear the asynchronous output or buffered input data. The error state is exited.
                */
                ErrorEvent error = new ErrorEvent(this.eventCallbacks.getEventSource(), 0 , 0, JposConst.JPOS_EL_OUTPUT, JposConst.JPOS_ER_CLEAR);
                eventCallbacks.fireErrorEvent(error);
                if (freezeEvents==false)
					state = JposConst.JPOS_S_ERROR;
                break;
            case SerialPortEvent.CD:
                break;
            case SerialPortEvent.CTS:
                break;
            case SerialPortEvent.DSR:
                break;
            case SerialPortEvent.RI:
                break;
			case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                /*
                 * for outputCompleteEventListener callback
                  Notifies the application that the queued output
                  request associated with theOutputID property has
                  completed successfully.
                */

                /*StatusUpdateEvent
                  PTR_SUE_IDLE All asynchronous output has finished, either
                  successfully or because output has been
                  cleared. The printer State is now
                  JPOS_S_IDLE. The FlagWhenIdle property
                  must be true for this event to be delivered, and
                  the property is automatically reset to false just
                  before the event is delivered.*/
                if (!freezeEvents)
                    state = JposConst.JPOS_S_BUSY;
                break;
			case SerialPortEvent.DATA_AVAILABLE:
				 //System.out.println("DATA_AVAILABLE");
				try {
					int init = inputStream.available();
					while (inputStream.available() > 0) {
						char x = (char)inputStream.read();
						if (init > 1) {
							if (x == 83 && buffer.length() > 0)
								buffer.setLength(0);
							buffer.append(x);
						} else {
							if (buffer.length() > 0)
								buffer.setLength(0);
							buffer.append(x);
						}
					}
				}
				catch (IOException e) { e.printStackTrace(); }
				Thread t = new Thread () {
					public void run () {
						synchronized (shareOutput.waitObject) {
						    shareOutput.waitObject.notifyAll();
						}
					}
				};
		     	if (buffer.length() == 0)
				   return;
				if (buffer.charAt(buffer.length() - 1) == '\r' ) {
					shareOutput.setStatusCode(buffer.charAt(1) + "");
				} else {
					shareOutput.setDrawerStatus(buffer.charAt(0));
				}
				t.start();
				break;
			default:
				break;
		}
	}

	public void deleteInstance() throws JposException { }

	public boolean getCapStatusMultiDrawerDetect() throws JposException {
        return false;
	}
//
//	public static void main(String[] args) {
//		CashDrawerPP2000 casha = new  CashDrawerPP2000();
//		casha.logicalName = "casha";
//		try {
//			casha.setDeviceEnabled(true);
//			shareOutput = ShareOutputPort.getInstance();
//			System.out.println( casha.getDrawerOpened());
//			casha.openDrawer();
//			Thread.currentThread().sleep(1000);
//			System.out.println( casha.getDrawerOpened());			
//		} catch (Exception je) {
//			je.printStackTrace();
//		}
//		System.exit(0);
//	}
}

