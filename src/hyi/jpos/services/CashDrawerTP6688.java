package hyi.jpos.services;

//for JavaCommAPI
import java.io.*;
import java.util.*;
import javax.comm.*;
import java.awt.*;

//for POSPrinterService14 interface
import jpos.*;
import jpos.services.*;

//for callback events
import jpos.events.*;

//for entry registry
import jpos.config.*;
import jpos.config.simple.*;


import hyi.cream.util.*;
import hyi.jpos.util.*;
//

public class CashDrawerTP6688 extends DeviceService implements SerialPortEventListener,
	CashDrawerService14 {
    //--------------------------------------------------------------------------
    // Variables
    //--------------------------------------------------------------------------
    
    private OutputStreamWriter dataOutput = null;
    static private ShareOutputPort shareOutput = null;  //Singleton object;
    private String logicalName = null;//
    
    private JposEntry cashDrawerEntry = null;
    private OutputStream outputStream = null;
    private InputStream inputStream = null;
	private String portName = "COM2";
    private StringBuffer buffer = new StringBuffer();
	private Object waitObject = new Object();
    
    private Object waitBeep = new Object();//for wait purpose;

    //--------------------------------------------------------------------------
    //private byte[] readPort = new byte[10];
	private char statusCode = '0';

    //Constructor
    public CashDrawerTP6688 (SimpleEntry entry) {
        cashDrawerEntry = entry;
	}
	
	public CashDrawerTP6688 () {
	}

    // Capabilities
    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties 13
    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        throw new JposException (JposConst.JPOS_E_NOSERVICE, "No service available now!");
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    //Common method -- Not supported
    public void checkHealth(int level)  throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    // Properties specified
    public boolean getCapStatus() throws JposException {
        return true;
    }
    
	public boolean getDrawerOpened() throws JposException {
		try {
			if (!shareOutput.isSerialPortOpened()) {
                shareOutput.open(logicalName, portName, 0, this);
             }
        } catch (PortInUseException pe) {
			throw new JposException (JposConst.JPOS_E_FAILURE, "Port in use!", pe);
        }
		outputStream = shareOutput.getOutputStream();
		dataOutput = shareOutput.getOutputStreamWriter();
		inputStream = shareOutput.getInputStream();
		try {
			dataOutput.write("\u001B\u001BO\r");
			dataOutput.flush();
			//System.out.println(.available());
		} catch (IOException ie)  {
            throw new JposException (JposConst.JPOS_E_FAILURE, "Fail to query the status!", ie);
		}//
		catch (Exception e)  {
			e.printStackTrace();
		}
		synchronized (waitObject) {
			try {
				waitObject.wait(800);
			} catch (InterruptedException ie) { throw new JposException (JposConst.JPOS_E_ILLEGAL, "Printer wait interrupted", ie); }
		}
		//System.out.println(shareOutput.getStatusCode());
		if (shareOutput.getStatusCode().length() > 8)
			statusCode = shareOutput.getStatusCode().charAt(3);
		else
		    return false;
        if (statusCode == '0')
            return true;
		else
			return false;	
    }

    // Methods
	public void openDrawer() throws JposException {
        if (!getDeviceEnabled())
			throw new JposException(JposConst.JPOS_E_DISABLED,"Cash drawer is not enabled!");
		try {
            if (!shareOutput.isSerialPortOpened()) {
				shareOutput.open(logicalName, portName, 0, this);
			}
		} catch (PortInUseException pe) {
            throw new JposException (JposConst.JPOS_E_FAILURE, "Port in use", pe);
        }
		outputStream = shareOutput.getOutputStream();
        dataOutput = shareOutput.getOutputStreamWriter();
		inputStream = shareOutput.getInputStream();
		String command = "";
        try {
            command ="\u001B\u001BG1\r";
            dataOutput.write(command);
            dataOutput.flush();
        } catch  (IOException e) {
            throw new JposException (JposConst.JPOS_E_FAILURE , "IO Error at" + this, e);
		}
		/*
		if (getDrawerOpened())
			return;
		else
			openDrawer();//*/
		//CASH_SUE_DRAWEROPEN (=1) The drawer is open.
        /*StatusUpdateEvent statusEvent = new StatusUpdateEvent(eventCallbacks.getEventSource(),
            CashDrawerConst.CASH_SUE_DRAWEROPEN);
        eventCallbacks.fireStatusUpdateEvent(statusEvent);
        //*/
    }
    
    
	public void waitForDrawerClose(int beepTimeout, int beepFrequency,
					   int beepDuration, int beepDelay) throws JposException {
        //System.out.println(getDrawerOpened() + "" + this);
        if (!getDeviceEnabled())
			throw new JposException(JposConst.JPOS_E_DISABLED,"Cash drawer is not enabled!");
		//System.out.println(getDrawerOpened() + "" + this);
		/*if (!getDrawerOpened()) {
			synchronized (this) {
				try {
					this.wait(1500);
				} catch (InterruptedException ie) {
				}
				eventCallbacks.fireStatusUpdateEvent(new StatusUpdateEvent(eventCallbacks.getEventSource(),
					CashDrawerConst.CASH_SUE_DRAWERCLOSED));//
			}
			return;
		}*/
		new Beep(beepTimeout, beepDelay);
        synchronized (waitBeep) {
            try {
				waitBeep.wait();
            } catch (InterruptedException ie) {
                if (!getDrawerOpened())
                    return;
                else
                    throw new JposException (JposConst.JPOS_E_FAILURE, this.toString(), ie);
            }
        }
        eventCallbacks.fireStatusUpdateEvent(new StatusUpdateEvent(eventCallbacks.getEventSource(),
			CashDrawerConst.CASH_SUE_DRAWERCLOSED));//*/
    }

    class Beep extends Thread {
        private int beepTimeout;
        private int beepDelay;
        public Beep() {
            start();
        }
        
        public Beep(int beepTimeout, int beepDelay) {
            this.beepTimeout = beepTimeout;
            this.beepDelay = beepDelay;
            start();
        }
        public void run() {
            try {
                long start = System.currentTimeMillis();
                long waitTime = beepTimeout;
                try {
					while (waitTime > 0) {
                        if (!getDrawerOpened()) {
							synchronized (waitBeep) {
                                waitBeep.notifyAll();
                                return;
                            }
                        }
						waitTime = beepTimeout - (System.currentTimeMillis() - start);
                    }
                    while (true){
                        if (!getDrawerOpened()) {
							synchronized (waitBeep) {
                                waitBeep.notifyAll();
                                return;
                            }
                        }
                        Toolkit.getDefaultToolkit().beep();
						sleep(beepDelay);
						//System.out.println(beepDelay);
					}
                } catch (JposException je) {
                        System.out.println(je + this .toString() );
                }
            } catch (InterruptedException e) {
						System.out.println(e + this .toString());
			}
        }
    }

	public void  open(String logicalName, EventCallbacks cb) throws JposException {
		//System.out.println(logicalName);
		super.open (logicalName, cb);
		if (cashDrawerEntry != null)
    		portName = (String)cashDrawerEntry.getPropertyValue("PORT_NAME");
		this.logicalName = logicalName;
		shareOutput = ShareOutputPort.getInstance();
    }

    public void close() throws JposException {
        if (getClaimed())
            release();
        super.close();
        this.logicalName = null;
        shareOutput = null;
    }

    public void claim(int timeout) throws JposException {
    
    }

    public void release() throws JposException {

	}

	public void serialEvent(SerialPortEvent event) {
	    switch(event.getEventType()) {
			case SerialPortEvent.BI:
            case SerialPortEvent.OE:   // for errorEventListener callback
            case SerialPortEvent.FE:   // need defined altogether with other devices */
            case SerialPortEvent.PE:
                /*
                 JPOS_ER_RETRY /Retry the asynchronous output. The error state is exited. The default.
                 JPOS_ER_CLEAR /Clear the asynchronous output or buffered input data. The error state is exited.
                */
                ErrorEvent error = new ErrorEvent(this.eventCallbacks.getEventSource(), 0 , 0, JposConst.JPOS_EL_OUTPUT, JposConst.JPOS_ER_CLEAR);
                eventCallbacks.fireErrorEvent(error);
                if (freezeEvents==false)
					state = JposConst.JPOS_S_ERROR;
                break;
            case SerialPortEvent.CD:
                break;
            case SerialPortEvent.CTS:
                break;
            case SerialPortEvent.DSR:
                break;
            case SerialPortEvent.RI:
                break;
			case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                /*
                 * for outputCompleteEventListener callback
                  Notifies the application that the queued output
                  request associated with theOutputID property has
                  completed successfully.
                */

                /*StatusUpdateEvent
                  PTR_SUE_IDLE All asynchronous output has finished, either
                  successfully or because output has been
                  cleared. The printer State is now
                  JPOS_S_IDLE. The FlagWhenIdle property
                  must be true for this event to be delivered, and
                  the property is automatically reset to false just
                  before the event is delivered.*/
                if (!freezeEvents)
                    state = JposConst.JPOS_S_BUSY;
                break;
			case SerialPortEvent.DATA_AVAILABLE:
				 //System.out.println("DATA_AVAILABLE");
				 /** for POSPrinter status copying */				 
				 try {
					 while (inputStream.available() > 0) {
						 char next = (char)inputStream.read();
						 //System.out.println(next);
						 if (next == 'S' && buffer.length() > 0)
							 buffer.setLength(0);
						 buffer.append(next);
					 }
					 //System.out.print(new String("\nThe event char is "+readPort[0]+this.readPort[1]+this.readPort[2]+this.readPort[3]+this.readPort[4]+this.readPort[5]+this.readPort[6]+this.readPort[7]+this.readPort[8]+this.readPort[9]));
                 }
				 catch (IOException e) {}
				 Thread t = new Thread () {
					  public void run () {
						  synchronized (waitObject) {
							  waitObject.notifyAll();
						  }
					  }
				 };
				 if (buffer.charAt(buffer.length() - 1) == '\r' ) {
					 shareOutput.setStatusCode(buffer.toString());
					 t.start();
				 }
				break;
			default:
				break;
		}
	}

	public void deleteInstance() throws JposException { }

/*
	public static void main(String[] args) {
						 CashDrawerTP6688 casha = new  CashDrawerTP6688();
						 casha.logicalName = "casha";
					
					 try {	 casha.setDeviceEnabled(true);
					 //this.logicalName = logicalName;
		shareOutput = ShareOutputPort.getInstance();
		//

					 //System.out.println( casha.getDrawerOpened());
					 
					 casha.openDrawer();
					 Thread.sleep(1000);
					 //System.out.println(casha .getDrawerOpened());
casha.waitForDrawerClose(1000, 0, 0, 1000);
						 //
							 	 //
					  } catch (Exception je) { System.out.println( je);}
					  System.exit(0);
					 
						POSTerminalApplication posTerminal = POSTerminalApplication.getInstance();
                        POSPeripheralHome posHome = POSPeripheralHome.getInstance();
						CashDrawer cash = null;
                        try {
                            cash = posHome.getCashDrawer();
                        } catch (NoSuchPOSDeviceException ne) { CreamToolkit.logMessage(ne.toString()); return; }
                    //set posHome enabled to prepare for event forwarding
                        if (!posHome.getEventForwardEnabled())
							posHome.setEventForwardEnabled(true);
                    //drive the cash drawer
                        try {
                            if (!cash.getDeviceEnabled())
								cash.setDeviceEnabled(true);
							 System.out.println("LLLLLdfdfdfdL");
							 cash.openDrawer();
							 System.out.println("LLLLLLLLL");
							cash.addStatusUpdateListener(posHome);
					 //Start a timer.
							cash.waitForDrawerClose(1000, 0, 0, 1000);
							cash.removeStatusUpdateListener(posHome);
                            cash.setDeviceEnabled(false);
                        } catch (JposException je) {
                            CreamToolkit.logMessage(je.toString());
							return;
						}
					
	}
*/
}

