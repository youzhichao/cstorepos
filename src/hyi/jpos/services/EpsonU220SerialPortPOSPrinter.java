package hyi.jpos.services;

import hyi.cream.util.CreamToolkit;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.TooManyListenersException;

import javax.comm.CommPortIdentifier;
import javax.comm.ParallelPortEvent;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.SerialPortEvent;
import javax.comm.SerialPortEventListener;
import javax.comm.UnsupportedCommOperationException;

import jpos.JposConst;
import jpos.JposException;
import jpos.POSPrinterConst;
import jpos.config.simple.SimpleEntry;
import jpos.services.EventCallbacks;
import jpos.services.POSPrinterService14;

/**
 * EpsonU220 serial port POSPrinter.
 * 
 * @date 2008/03/12
 * @author Bruce You
 */
public class EpsonU220SerialPortPOSPrinter extends DeviceService implements
    SerialPortEventListener, POSPrinterService14 {

    private boolean DEBUG = true;
    private static EpsonU220SerialPortPOSPrinter instance;

    private OutputStreamWriter dataOutput;
	private InputStream inputStream;
	private OutputStream outputStream;

	/** serial port current feedback status got asyncronously */
	private byte[] readPort = new byte[10];
	private byte statusCode;

	/** AsyncMode flag definition */
	private boolean asyncMode = false;
	private int characterSet = POSPrinterConst.PTR_CS_ASCII;
	private boolean flagWhenIdle = true;
	private int outputID;

	private RandomAccessFile fontFile;
	private int[] bitmaps = new int[16];

	private String toplogo;
	private String bottomlogo;

    private SerialPort serialPort;
    private String devicePortName;
    private int baudRate;
    private int dataBits;
    private int stopBits;
    private int parity;

	public String getTopLogo() throws JposException {
		if (toplogo != null)
			return toplogo;
		else
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Null pointer logo detected!");
	}

	public String getBottomLogo() throws JposException {
		if (bottomlogo != null)
			return bottomlogo;
		else
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Null pointer logo detected!");
	}

	public EpsonU220SerialPortPOSPrinter() throws IOException {
//		if (System.getProperties().get("os.name").toString().indexOf("Windows") != -1)
//			outputStream = new FileOutputStream("lpt1");
//		else
//			outputStream = new FileOutputStream("/dev/lp0");
//
//		// openPrinter();
//		dataOutput = new OutputStreamWriter(outputStream);
//		outputStream.write(0x1B);
//		outputStream.write('@');
		instance = this;
	}

    /**
     * constructor with JposEntry parameter
     */
    public EpsonU220SerialPortPOSPrinter(SimpleEntry entry) throws IOException {
        this();
        loadParametersFromJposEntry(entry);
    }

    public void loadParametersFromJposEntry(SimpleEntry entry) {
        if (entry != null) {
            devicePortName = (String)entry.getPropertyValue("DevicePortName");
            if (DEBUG)
                System.out.println("EpsonU220SerialPortPOSPrinter(): Read DevicePortName=" + devicePortName);
            if (devicePortName == null)
                devicePortName = "COM1";

            Integer baudRateObj = (Integer)entry.getPropertyValue("BaudRate");
            if (DEBUG)
                System.out.println("EpsonU220SerialPortPOSPrinter(): Read BaudRate=" + baudRateObj);
            baudRate = (baudRateObj == null) ? 9600 : baudRateObj.intValue();  

            Integer dataBitsObj = (Integer)entry.getPropertyValue("DataBits");
            if (DEBUG)
                System.out.println("EpsonU220SerialPortPOSPrinter(): Read DataBits=" + dataBitsObj);
            dataBits = (dataBitsObj == null) ? 8 : dataBitsObj.intValue();
            
            Integer stopBitsObj = (Integer)entry.getPropertyValue("StopBits");
            if (DEBUG)
                System.out.println("EpsonU220SerialPortPOSPrinter(): Read StopBits=" + stopBitsObj);
            stopBits = (stopBitsObj == null) ? 8 : stopBitsObj.intValue();

            Integer parityObj = (Integer)entry.getPropertyValue("Parity");
            if (DEBUG)
                System.out.println("EpsonU220SerialPortPOSPrinter(): Read Parity=" + parityObj);
            parity = (parityObj == null) ? 8 : parityObj.intValue();
        }
    }

    private void openComPort(String logicalName) throws JposException {
        if (DEBUG)
            System.out.println("EpsonU220SerialPortPOSPrinter.openComPort()> open port=" + devicePortName);
        Enumeration portList = CommPortIdentifier.getPortIdentifiers();
        boolean foundPort = false;
        CommPortIdentifier portId = null;
        while (portList.hasMoreElements()) {
            portId = (CommPortIdentifier)portList.nextElement();
            if (DEBUG)
                System.out.println("EpsonU220SerialPortPOSPrinter.openComPort()> Iterating " + portId.getName());
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL &&
                portId.getName().equals(devicePortName)) {
                if (DEBUG)
                    System.out.println("EpsonU220SerialPortPOSPrinter.openComPort()> Matched.");
                    foundPort = true;
                    break;
            }
        }
        if (!foundPort)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Serial port " + devicePortName + " is not available!");

        try {
            serialPort = (SerialPort)portId.open(logicalName, 15000);
            outputStream = serialPort.getOutputStream();
            inputStream = serialPort.getInputStream();
            dataOutput = new OutputStreamWriter(outputStream,  CreamToolkit.getEncoding());

            serialPort.setSerialPortParams(baudRate, dataBits, stopBits, parity);
            //serialPort.setSerialPortParams(9600, //19200,
            //    SerialPort.DATABITS_8,
            //    SerialPort.STOPBITS_1,
            //    SerialPort.PARITY_NONE);
            //serialPort.setFlowControlMode(SerialPort. FLOWCONTROL_XONXOFF_IN);
            serialPort.notifyOnDataAvailable(true);
            serialPort.notifyOnOutputEmpty(true);
            serialPort.notifyOnBreakInterrupt(true);
            serialPort.notifyOnCarrierDetect(true);
            serialPort.notifyOnCTS(true);
            serialPort.notifyOnDSR(true);
            serialPort.notifyOnFramingError(true);
            serialPort.notifyOnOverrunError(true);
            serialPort.notifyOnParityError(true);
            serialPort.notifyOnRingIndicator(true);
            serialPort.addEventListener(this);
        } catch (PortInUseException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, e.toString(), e);
        } catch (IOException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, e.toString(), e);
        } catch (UnsupportedCommOperationException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, e.toString(), e);
        } catch (TooManyListenersException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, e.toString(), e);
        }
    }
    
	static public EpsonU220SerialPortPOSPrinter getInstance() {
		return instance;
	}

	/**
	 * Service13 Capabilities
	 */
	public int getCapPowerReporting() throws JposException {
		return JposConst.JPOS_PR_STANDARD;
	}

	// --------------------------------------------------------------------------

	/**
	 * Service12 Capabilities
	 */
	public int getCapCharacterSet() throws JposException {
		return POSPrinterConst.PTR_CCS_ASCII;
	}

	// Journal and Receipt stations can print at the same time.
	public boolean getCapConcurrentJrnRec() throws JposException {
		return true;
	}

	// If true, then the Journal and Slip stations can print at the same time.
	public boolean getCapConcurrentJrnSlp() throws JposException {
		return false;
	}

	// If true, then the Receipt and Slip stations can print at the same time.
	public boolean getCapConcurrentRecSlp() throws JposException {
		return false;
	}

	// If true, then the printer has a "cover open” sensor.
	public boolean getCapCoverSensor() throws JposException {
		return false;
	}

	// If true, then the journal can print dark plus an alternate color.
	public boolean getCapJrn2Color() throws JposException {
		return false;
	}

	// If true, then the journal can print bold characters.
	public boolean getCapJrnBold() throws JposException {
		return false;
	}

	// If true, then the journal can print double high characters.
	public boolean getCapJrnDhigh() throws JposException {
		return false;
	}

	// If true, then the journal can print double wide characters.
	public boolean getCapJrnDwide() throws JposException {
		return true;
	}

	// If true, then the journal can print double high / double wide characters.
	public boolean getCapJrnDwideDhigh() throws JposException {
		return false;
	}

	// If true, then the journal has an outputStream-of-paper sensor.
	public boolean getCapJrnEmptySensor() throws JposException {
		return true;
	}

	// If true, then the journal can print italic characters.
	public boolean getCapJrnItalic() throws JposException {
		return false;
	}

	// If true, then the journal has a low paper sensor.
	public boolean getCapJrnNearEndSensor() throws JposException {
		return true;
	}

	// If true, then the journal print station is present.
	public boolean getCapJrnPresent() throws JposException {
		return true;
	}

	// If true, then the journal can underline characters.
	public boolean getCapJrnUnderline() throws JposException {
		return false;
	}

	// If true, then the receipt can print dark plus an alternate color.
	public boolean getCapRec2Color() throws JposException {
		return false;
	}

	// If true, then the receipt has bar code printing capability.
	public boolean getCapRecBarCode() throws JposException {
		return false;
	}

	// If true, then the receipt can print bitmaps.
	public boolean getCapRecBitmap() throws JposException {
		return true;
	}

	// If true, then the receipt can print bold characters.
	public boolean getCapRecBold() throws JposException {
		return false;
	}

	// If true, then the receipt can print double high characters.
	public boolean getCapRecDhigh() throws JposException {
		return false;
	}

	// If true, then the receipt can print double wide characters.
	public boolean getCapRecDwide() throws JposException {
		return true;
	}

	// If true, then the receipt can print double wide/high characters.
	public boolean getCapRecDwideDhigh() throws JposException {
		return false;
	}

	// If true, then the receipt has an outputStream-of-paper sensor.
	public boolean getCapRecEmptySensor() throws JposException {
		return true;
	}

	// If true, then the receipt can print italic characters.
	public boolean getCapRecItalic() throws JposException {
		return false;
	}

	// If true, then the receipt can print in rotated 90° left mode.
	public boolean getCapRecLeft90() throws JposException {
		return false;
	}

	// If true, then the receipt has a low paper sensor.
	public boolean getCapRecNearEndSensor() throws JposException {
		return true;
	}

	// If true, then the receipt can perform paper cuts.
	public boolean getCapRecPapercut() throws JposException {
		return true;
	}

	// If true, then the receipt print station is present.
	public boolean getCapRecPresent() throws JposException {
		return true;
	}

	// If true, then the receipt can print in a rotated 90° right mode.
	public boolean getCapRecRight90() throws JposException {
		return false;
	}

	// If true, then the receipt can print in a rotated upside down mode.
	public boolean getCapRecRotate180() throws JposException {
		return false;
	}

	// If true, then the receipt has a stamp capability.
	public boolean getCapRecStamp() throws JposException {
		return true;
	}

	// If true, then the receipt can underline characters.
	public boolean getCapRecUnderline() throws JposException {
		return false;
	}

	// If true, then the slip can print dark plus an alternate color.
	public boolean getCapSlp2Color() throws JposException {
		return false;
	}

	// If true, then the slip has bar code printing capability.
	public boolean getCapSlpBarCode() throws JposException {
		return false;
	}

	// If true, then the slip can print bitmaps.
	public boolean getCapSlpBitmap() throws JposException {
		return false;
	}

	// If true, then the slip can print bold characters.
	public boolean getCapSlpBold() throws JposException {
		return false;
	}

	// If true, then the slip can print double high characters.
	public boolean getCapSlpDhigh() throws JposException {
		return false;
	}

	// If true, then the slip can print double wide characters.
	public boolean getCapSlpDwide() throws JposException {
		return true;
	}

	// If true, then the slip can print double high / double wide characters.
	public boolean getCapSlpDwideDhigh() throws JposException {
		return false;
	}

	// If true, then the slip has a "slip in" sensor.
	public boolean getCapSlpEmptySensor() throws JposException {
		return true;
	}

	/*
	 * If true, then the slip is a full slip station. It can print full-length
	 * forms. If false, then the slip is a “validation” type station. This
	 * usually limits the number of print lines, and disables access to the
	 * receipt and/or journal stations while the validation slip is being used.
	 */
	public boolean getCapSlpFullslip() throws JposException {
		return false;
	}

	// If true, then the slip can print italic characters.
	public boolean getCapSlpItalic() throws JposException {
		return false;
	}

	// If true, then the slip can print in a rotated 90° left mode.
	public boolean getCapSlpLeft90() throws JposException {
		return false;
	}

	// If true, then the slip has a “slip near end” sensor.
	public boolean getCapSlpNearEndSensor() throws JposException {
		return false;
	}

	// If true, then the slip print station is present.
	public boolean getCapSlpPresent() throws JposException {
		return true;
	}

	// If true, then the slip can print in a rotated 90° right mode.
	public boolean getCapSlpRight90() throws JposException {
		return false;
	}

	// If true, then the slip can print in a rotated upside down mode.
	public boolean getCapSlpRotate180() throws JposException {
		return false;
	}

	// If true, then the slip can underline characters.
	public boolean getCapSlpUnderline() throws JposException {
		return false;
	}

	// If true, then printer transactions are supported by each station.
	public boolean getCapTransaction() throws JposException {
		return true;
	}

	// the end of Service12 capabilities

	/**
	 * Service13 Properties
	 */
	public int getPowerNotify() throws JposException {
		return JposConst.JPOS_PN_DISABLED;
	}

	public void setPowerNotify(int powerNotify) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "No Service!");
	}

	public int getPowerState() throws JposException {
		return JposConst.JPOS_PS_UNKNOWN;
	}

	/**
	 * Service12 Properties
	 */

	// Sync/Async mode
	public boolean getAsyncMode() throws JposException {
		return asyncMode;
	}

	public void setAsyncMode(boolean asyncMode) throws JposException {
		if (getAsyncMode() && !asyncMode) {
			this.asyncMode = false;
			System.out.println("POSPrinter runs in SyncMode!");
			return;
		} else if (!getAsyncMode() && asyncMode) {
			this.asyncMode = true;
			System.out.println("POSPrinter runs in AsyncMode!");
			return;
		} else
			return;
		// throw new JposException(JposConst.JPOS_E_ILLEGAL, "Expected mode
		// can't be obtained: Change to the same mode!");
	}

	public int getCharacterSet() throws JposException {
		return characterSet;
	}

	public void setCharacterSet(int characterSet) throws JposException {
		this.characterSet = characterSet;
	}

	// Holds the character set numbers. It consists of ASCII numeric set numbers
	// separated by commas.
	public String getCharacterSetList() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE);
	}

	// If true, then the printer’s cover is open.
	public boolean getCoverOpen() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE);
	}

	private int errorLevel = POSPrinterConst.PTR_EL_NONE;

	public int getErrorLevel() throws JposException {
		return errorLevel;
	}

	private int errorStation = 0;

	/*
	 * Holds the station or stations that were printing when an error was
	 * detected. This property will be set to one of the following values:
	 * PTR_S_JOURNAL PTR_S_RECEIPT PTR_S_SLIP PTR_S_JOURNAL_RECEIPT
	 * PTR_S_JOURNAL_SLIP PTR_S_RECEIPT_SLIP PTR_TWO_RECEIPT_JOURNAL
	 * PTR_TWO_SLIP_JOURNAL PTR_TWO_SLIP_RECEIPT This property is only valid if
	 * the ErrorLevel is not equal to PTR_EL_NONE. It is set just before
	 * delivering an ErrorEvent.
	 */
	public int getErrorStation() throws JposException {
		return errorStation;
	}

	private String errorString = "";

	/*
	 * Holds a vendor-supplied description of the current error. This property
	 * is set just before delivering an ErrorEvent. If no description is
	 * available, the property is set to an empty string. When the error is
	 * cleared, then the property is changed to an empty string.
	 */
	public String getErrorString() throws JposException {
		return errorString;
	}

	/*
	 * If true, a StatusUpdateEvent will be enqueued when the device is in the
	 * idle state. This property is automatically reset to false when the status
	 * event is delivered. The main use of idle status event that is controlled
	 * by this property is to give the application control when all outstanding
	 * asynchronous outputs have been processed. The event will be enqueued if
	 * the outputs were completed successfully or if they were cleared by the
	 * clearOutput method or by an ErrorEvent handler. If the State is already
	 * set to JPOS_S_IDLE when this property is set to true, then a
	 * StatusUpdateEvent is enqueued immediately. The application can therefore
	 * depend upon the event, with no race condition between the starting of its
	 * last asynchronous output and the setting of this flag. This property is
	 * initialized to false by the open method.
	 */
	public boolean getFlagWhenIdle() throws JposException {
		return flagWhenIdle;
	}

	public void setFlagWhenIdle(boolean flagWhenIdle) throws JposException {
		this.flagWhenIdle = flagWhenIdle;
	}

	/*
	 * Holds the fonts and/or typefaces that are supported by the printer. The
	 * string consists of font or typeface names separated by commas. This
	 * property is initialized by the open method.
	 */
	public String getFontTypefaceList() throws JposException {
		return "";
	}

	public boolean getJrnEmpty() throws JposException {
		if (!getCapJrnEmptySensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(), ie);
		}
		statusCode = readPort[0];
		if ((statusCode & 0x01) == 0x00)
			return false;
		else
			return true;
	}

	public boolean getJrnLetterQuality() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setJrnLetterQuality(boolean jrnLetterQuality)
			throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	// Holds the number of characters that may be printed on a journal line.
	private int jrnLineChars = 24;

	public int getJrnLineChars() throws JposException {
		return jrnLineChars;
	}

	// This property is initialized to the printer’s default line character
	// width
	// when the device is first enabled following the open method.
	public void setJrnLineChars(int jrnLineChars) throws JposException {
		if (jrnLineChars < 0 || jrnLineChars > 24)
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Invalid agrument:" + jrnLineChars);
		this.jrnLineChars = jrnLineChars;
	}

	public String getJrnLineCharsList() throws JposException {
		return "12,24";
	}

	public int getJrnLineHeight() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setJrnLineHeight(int jrnLineHeight) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getJrnLineSpacing() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setJrnLineSpacing(int jrnLineSpacing) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getJrnLineWidth() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getJrnNearEnd() throws JposException {
		if (!getCapJrnNearEndSensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(),
					ie);
		}
		statusCode = readPort[0];
		if ((statusCode & 0x01) == 0x00)
			return false;
		else
			return true;
	}

	public int getMapMode() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setMapMode(int mapMode) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public String getRecBarCodeRotationList() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getRecEmpty() throws JposException {
		if (!getCapRecEmptySensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(),
					ie);
		}
		statusCode = readPort[0];// if statusCode equals zero, the paper is
									// present
		if ((statusCode & 0x02) == 0x00)
			return false;
		else
			return true;
	}

	public boolean getRecLetterQuality() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRecLetterQuality(boolean recLetterQuality)
			throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	private int recLineChars = 24;

	public int getRecLineChars() throws JposException {
		return recLineChars;
	}

	public void setRecLineChars(int recLineChars) throws JposException {
		if (recLineChars < 0 || recLineChars > 24)
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Invalid agrument:" + jrnLineChars);
		this.recLineChars = recLineChars;
	}

	public String getRecLineCharsList() throws JposException {
		return "12,24";
	}

	public int getRecLineHeight() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRecLineHeight(int recLineHeight) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getRecLineSpacing() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRecLineSpacing(int recLineSpacing) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	private int recLinesToPaperCut = 0;

	public int getRecLinesToPaperCut() throws JposException {
		return recLinesToPaperCut;
	}

	public int getRecLineWidth() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getRecNearEnd() throws JposException {
		if (!getCapRecNearEndSensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(),
					ie);
		}
		statusCode = readPort[0]; // if statusCode equals zero, the paper is
									// present
		if ((statusCode & 0x02) == 0x00)
			return false;
		else
			return true;
	}

	public int getRecSidewaysMaxChars() throws JposException {
		return 0;
	}

	public int getRecSidewaysMaxLines() throws JposException {
		return 0;
	}

	public int getRotateSpecial() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRotateSpecial(int rotateSpecial) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public String getSlpBarCodeRotationList() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getSlpEmpty() throws JposException {
		if (!getCapSlpEmptySensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(),
					ie);
		}
		statusCode = readPort[0];// if statusCode equals zero, the paper is
									// present
		if ((statusCode & 0x20) == 0x00)
			return false;
		else
			return true;

	}

	public boolean getSlpLetterQuality() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setSlpLetterQuality(boolean recLetterQuality)
			throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	// --------------------------------------------------------------------------
	private int slpLineChars = 55;

	public int getSlpLineChars() throws JposException {
		return slpLineChars;
	}

	public void setSlpLineChars(int slpLineChars) throws JposException {
		if (slpLineChars < 0 || slpLineChars > 55)
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Invalid agrument:" + slpLineChars);
		this.slpLineChars = slpLineChars;
	}

	// --------------------------------------------------------------------------

	public String getSlpLineCharsList() throws JposException {
		return "27,55";
	}

	public int getSlpLineHeight() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setSlpLineHeight(int slpLineHeight) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpLinesNearEndToEnd() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpLineSpacing() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setSlpLineSpacing(int slpLineSpacing) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpLineWidth() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpMaxLines() throws JposException {
		return 1;
	}

	public boolean getSlpNearEnd() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpSidewaysMaxChars() throws JposException {
		return 0;
	}

	public int getSlpSidewaysMaxLines() throws JposException {
		return 0;
	}

	// --------------------------------------------------------------------------
	public int getOutputID() throws JposException {
		return outputID;
	}

	// new...begin Service12 Methods
	// first sector the synchronized method
	synchronized public void beginInsertion(int timeout) throws JposException {
		if (timeout < -1)
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Illegal parameter:" + timeout);
		if (!getCapSlpPresent())
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not supported");
	}

	synchronized public void endInsertion() throws JposException {
	}

	synchronized public void beginRemoval(int timeout) throws JposException {
		if (timeout < -1)
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Illegal parameter:" + timeout);
		if (!getCapSlpPresent())
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not supported");
	}

	synchronized public void endRemoval() throws JposException {
	}

	// end of first sector the synchronized method

	public void clearOutput() throws JposException {
		outputID = 0;
	}

	public void printBarCode(int station, String data, int symbology,
			int height, int width, int alignment, int textPosition)
			throws JposException {
		throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not support!");
	}// end of printBarCode

	public void printBitmap(int station, String fileName, int width,
			int alignment) throws JposException {
		throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not support!");
	}// end of printBitmap

	public void printImmediate(String str) {
		Iterator iter = new CharIterator(str);
		while (iter.hasNext()) {
			String c = (String) iter.next();
			if (isChineseChar(c)) {
				printChineseCharacter(c);
			} else {
				printAlphanumeric((byte) c.charAt(0));
			}
		}
	}

	public void printImmediate(int station, String data) throws JposException {
        if (data.equals(""))
            return;
        printdata(data);
	}

	// Main
	public void printNormal(int station, String data) throws JposException {
		if (data.equals(""))
			return;
		printdata(data);
	}

	public void printTwoNormal(int stations, String data1, String data2)
			throws JposException {
		if (data1.equals("") && data2.equals(""))
			return;
		printdata(data1);
	}

	public void rotatePrint(int station, int rotation) throws JposException {
		throw new JposException(JposConst.JPOS_E_FAILURE,
				"Not supported in DRJST51!");
	}

	public void setBitmap(int bitmapNumber, int station, String fileName,
			int width, int alignment) throws JposException {
		throw new JposException(JposConst.JPOS_E_FAILURE,
				"Not supported in DRJST51!");
	}

	public void setLogo(int location, String data) throws JposException {
		// The logo to be set. Location may be PTR_L_TOP or PTR_L_BOTTOM.
		if (location == POSPrinterConst.PTR_L_TOP)
			toplogo = data;
		else if (location == POSPrinterConst.PTR_L_BOTTOM)
			bottomlogo = data;
		else
			throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"An invalid location was specified:" + location + ".");
	}

	public void transactionPrint(int station, int control) throws JposException {
		throw new JposException(JposConst.JPOS_E_ILLEGAL,
				"Transaction mode is not supported!");
	}

	public void validateData(int station, String data) throws JposException {
	}

	// The end of Service12 Methods

	// --------------------------------------------------------------------------

	// new...begin BaseService Methods supported by all device services.
	public void claim(int timeout) throws JposException {
		super.claim(timeout);
	}

	public void release() throws JposException {
		if (!getClaimed())
			return;
		try {
			if (outputStream != null)
				outputStream.close();
			outputStream = null;
			if (inputStream != null)
				inputStream.close();
			inputStream = null;
			if (dataOutput != null)
				dataOutput.close();
			dataOutput = null;
			super.release();
		} catch (IOException e) {
		}
	}

	// --------------------------------------------------------------------------
	// Close the device and unlink it from the device control!
	// --------------------------------------------------------------------------

	public void close() throws JposException {
		super.close();
		if (getClaimed())
			release();
		if (getAsyncMode())
			clearOutput();
		asyncMode = false;
	}

	// The end of close()
	public void checkHealth(int level) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, this.toString());
	}

	public void directIO(int command, int[] data, Object object)
			throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, this.toString());
	}

	public void open(String logicalName, EventCallbacks cb)
			throws JposException {
        if (DEBUG)
            System.out.println("EpsonU220SerialPortPOSPrinter.open()");
        super.open(logicalName, cb);
        this.eventCallbacks = cb;
        claimed = false;
        deviceEnabled = false;
        asyncMode = false;
		outputID = 0;
		openComPort(logicalName);
        initPrinter();
	}

	private void initPrinter()
    {
	    try {
            outputStream.write(0x1B);
            outputStream.write('@');
        } catch (IOException e) {
        }
    }

    // new...end BaseService Methods supported by all device services.

	// --------------------------------------------------------------------------
	/**
	 * serialEventListenner implementation, it is in another thread, executed
	 * automatically
	 */
	public void parallelEvent(ParallelPortEvent ev) {
	}

	public void deleteInstance() throws JposException {
	}

	private void printdata(String str) {
		try {
			outputStream.write(str.getBytes(CreamToolkit.getEncoding()));
			outputStream.flush();
		} catch (IOException ex) {
			ex.printStackTrace(System.err);
		}
	}

	private void printAlphanumeric(byte b) {
		try {
			outputStream.write(b);
			outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	private void printBitmap(int[] bytes) {
		// System.out.print("printBitmap(): ");
		try {
			// print bitmap
			outputStream.write(new byte[] { 0x1B, (byte) '*', 0x01, // 0: 8-dot single
															// density, 1: 8-dot
															// double density
					(byte) bytes.length, // number of dots in horizontal
											// direction
					0x00 // number of another 256 dots in horizontal
							// direction
					});
			for (int i = 0; i < bytes.length; i++) {
				outputStream.write(bytes[i]);
				// System.out.print(bytes[i] + ",");
			}
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	public void openDrawer() {
		try {
			outputStream.write(0x1B);
			outputStream.write('p');
			outputStream.write(0x00);
			outputStream.write(0x19);
			outputStream.write(0xF0);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	public void setRedColor() {
		try {
			outputStream.write(0x1B);
			outputStream.write('r');
			outputStream.write(0x01);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	public void setBlackColor() {
		try {
			outputStream.write(0x1B);
			outputStream.write('r');
			outputStream.write(0x00);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	public void cutPaper(int percentage) throws JposException {
		try {
            // feed to black mark
		    outputStream.write(0x1C);
		    outputStream.write(0x28);
		    outputStream.write(0x4C);
		    outputStream.write(0x02);
		    outputStream.write(0x00);
		    outputStream.write(0x42);
		    outputStream.write(0x31);

            // cut paper
            outputStream.write(0x1D);
            outputStream.write(0x56);
            outputStream.write(0x00);
            
            outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Inner class for iterating Chinese (in GB code) or alphanumeric
	 * characters.
	 */
	private class CharIterator implements Iterator {

		private byte[] str;
		private int index;
		private byte[] chineseChar = new byte[2];
		private byte[] alphanumChar = new byte[1];

		CharIterator(String str0) {
			try {
				if (str0 != null)
					str = str0.getBytes(CreamToolkit.getEncoding());
			} catch (UnsupportedEncodingException e) {
			}
		}

		public boolean hasNext() {
			return (str == null) ? false : (index < str.length);
		}

		public Object next() {
			if (!hasNext())
				return null;

			int b = str[index] & 0xff;
			// System.out.print("b=" + Integer.toHexString(b) + " ");
			if (index + 1 < str.length) {
				int b2 = str[index + 1] & 0xff;
				if ((b >= 0xA1 && b <= 0xA9 || b >= 0xB0 && b <= 0xF7) // first-byte
																		// is a
																		// GB
																		// code
						&& (b2 >= 0xA1 && b2 <= 0xFE) // second-byte is a GB
														// code
				) {
					chineseChar[0] = (byte) b;
					chineseChar[1] = (byte) b2;
					index += 2;
					try {
						return new String(chineseChar, "ISO-8859-1");
					} catch (UnsupportedEncodingException e) {
						return "";
					}
				}
			}
			alphanumChar[0] = (byte) b;
			index++;
			return new String(alphanumChar);
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	private boolean isChineseChar(String str) {
		return (str.length() == 2); // 2-byte if it is a Chinese char
	}

	/**
	 * Get the Chinese font bitmap into upperBits and lowerBits
	 */
	private void getBitmap(String c) {
		byte[] b = new byte[32]; // bitmap bytes read from font file
		int b1 = c.charAt(0);
		int b2 = c.charAt(1);
		int offset;
		offset = ((b1 - 0xA1) * (0xFE - 0xA0) + (b2 - 0xA1)) * 32;
		try {
			fontFile.seek(offset);
			fontFile.read(b);
		} catch (IOException e) {
			e.printStackTrace(System.err);
			return;
		}
		int[] bb = new int[8];
		int i, j, mask;
		for (i = 0; i < 8; i++) {
			mask = 0x80 >>> i;
			for (j = 0; j <= 30; j += 4) {
				bb[j / 4] = ((b[j] & mask) != 0 || (b[j + 2] & mask) != 0) ? 1
						: 0;
			}
			bitmaps[i] = bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4
					| bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
		}
		for (i = 0; i < 8; i++) {
			mask = 0x80 >>> i;
			for (j = 1; j <= 31; j += 4) {
				bb[j / 4] = ((b[j] & mask) != 0 || (b[j + 2] & mask) != 0) ? 1
						: 0;
			}
			bitmaps[i + 8] = bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4
					| bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
		}
	}

	private void getBitmap(String c, int[] upperBits, int[] lowerBits) {
		byte[] b = new byte[18]; // bitmap bytes read from font file
		int b1 = c.charAt(0);
		int b2 = c.charAt(1);
		int offset;

		if (b1 <= 0xA9)
			offset = ((b1 - 0xA1) * (0xFE - 0xA0) + (b2 - 0xA1)) * 18;
		else
			offset = (9 * (0xFE - 0xA0) + (b1 - 0xB0) * (0xFE - 0xA0) + (b2 - 0xA1)) * 18;
		try {
			fontFile.seek(offset);
			fontFile.read(b);
		} catch (IOException e) {
			e.printStackTrace(System.err);
			System.exit(2);
		}
		int[] bb = new int[8];
		int i, j, mask;
		for (i = 0; i < 8; i++) {
			mask = 0x80 >>> i;
			for (j = 0; j <= 14; j += 2) {
				bb[j / 2] = ((b[j] & mask) != 0) ? 1 : 0;
			}
			upperBits[i] = bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4
					| bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
			lowerBits[i] = ((b[16] & mask) != 0) ? 0x80 : 0x00;
		}
		for (i = 0; i < 8; i++) {
			mask = 0x80 >>> i;
			for (j = 1; j <= 15; j += 2) {
				bb[j / 2] = ((b[j] & mask) != 0) ? 1 : 0;
			}
			upperBits[i + 8] = bb[0] << 7 | bb[1] << 6 | bb[2] << 5
					| bb[3] << 4 | bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
			lowerBits[i + 8] = ((b[17] & mask) != 0) ? 0x80 : 0x00;
		}
	}

	private void printChineseCharacter(String c) {
		int[] upperBits = new int[16];
		int[] lowerBits = new int[16];

		getBitmap(c, upperBits, lowerBits);
		try {
			// define user-defined character
			outputStream.write(0x1B);
			outputStream.write('&');
			outputStream.write(0x02); // 2-byte in vertical direction
			outputStream.write('A'); // define 'A' to 'B'
			outputStream.write('B');
			for (int i = 0; i < 2; i++) {
				outputStream.write(0x08); // number of bytes in horizontal direction
				for (int j = 0; j < 8; j++) {
					outputStream.write(upperBits[8 * i + j]);
					outputStream.write(lowerBits[8 * i + j]);
				}
			}
			outputStream.write(0x1B);
			outputStream.write('%');
			outputStream.write(0x01);
			outputStream.write('A');
			outputStream.write('B');
			outputStream.write(0x1B);
			outputStream.write('?');
			outputStream.write('A');
			outputStream.write(0x1B);
			outputStream.write('?');
			outputStream.write('B');
			outputStream.write(0x1B);
			outputStream.write('%');
			outputStream.write(0x00);
			outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Print by bitmap command. With automatically adding an line-feed at end.
	 */
	public void printImmediateWithLineFeed(String str) {
		Arrays.fill(bitmaps, 0);
		Iterator iter = new CharIterator(str);
		while (iter.hasNext()) {
			String c = (String) iter.next();
			if (isChineseChar(c)) {
				getBitmap(c); // into upperBits
				printBitmap(bitmaps);
			} else {
				printAlphanumeric((byte) c.charAt(0));

			}
		}

	}

    public void serialEvent(SerialPortEvent serialportevent)
    {
    }
}
