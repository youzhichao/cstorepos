package hyi.jpos.services;

import hyi.cream.util.CreamToolkit;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;

import javax.comm.CommPortIdentifier;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.UnsupportedCommOperationException;

import jpos.JposConst;
import jpos.JposException;
import jpos.LineDisplay;
import jpos.LineDisplayConst;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.services.EventCallbacks;
import jpos.services.LineDisplayService14;

/**
 * Device service for Firich 2x20 LineDisplay.
 *
 * @author Bruce You
 */
public class Firich2x20LineDisplay implements LineDisplayService14 {

    static private LineDisplay claimedControl;
    static private Object mutex = new Object();
    static private char[] ch = new char[40];

    private boolean capDescriptors   = false;
    private int characterSet         = 0;
    private String characterSetList  = "";
    private boolean claimed          = false;
    private int columns              = 0;
    private int currentWindow        = 0;
    private int cursorColumn         = 0;
    private int cursorRow            = 0;
    private boolean cursorUpdate     = false;
    private int deviceBrightness     = 0;
    private int deviceColumns        = 0;
    private int deviceDescriptors    = 0;
    private boolean deviceEnabled    = false;
    private int deviceRows           = 0;
    private int deviceWindows        = 0;
    private int interCharacterWait   = 0;
    private int marqueeFormat        = 0;
    private int marqueeRepeatWait    = 0;
    private int marqueeType          = 0;
    private int marqueeUnitWait      = 0;
    private int rows                 = 0;
    private boolean opened           = false;

    protected String checkHealthText             = "";
    protected String deviceServiceDescription    = "JavaPOS Service from HYI Corporation";
    protected int deviceServiceVersion           = 1005000;
    protected boolean freezeEvents               = false;
    protected String physicalDeviceDescription   = "";
    protected String physicalDeviceName          = "";
    protected int state                          = JposConst.JPOS_S_CLOSED;
    protected int powerNotify                    = JposConst.JPOS_PN_DISABLED;
    protected int powerState                     = JposConst.JPOS_PS_UNKNOWN;

    protected EventCallbacks eventCallbacks      = null;
    protected JposEntry entry                    = null;

    static Enumeration portList;
    static CommPortIdentifier portId;
    static SerialPort serialPort;
    static OutputStream outputStream;

    /**
     * Constructor
     */
    public Firich2x20LineDisplay(SimpleEntry entry) {
        this.entry = entry;
    }

    // Capabilities
    public int getCapBlink() throws JposException {
        return LineDisplayConst.DISP_CB_NOBLINK;
    }

    public boolean getCapBrightness() throws JposException {
        return false;
    }

    public int getCapCharacterSet() throws JposException {
        return LineDisplayConst.DISP_CCS_ASCII;
    }

    public boolean getCapDescriptors() throws JposException {
        return capDescriptors;
    }

    public boolean getCapHMarquee() throws JposException {
        return true;
    }

    public boolean getCapICharWait() throws JposException {
        return false;
    }

    public boolean getCapVMarquee() throws JposException {
        return false;
    }

    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties
    public String  getCheckHealthText() throws JposException {
        return checkHealthText;
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    public void setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        if (claimed) {
            this.deviceEnabled = deviceEnabled;
        } else {
            CreamToolkit.logMessage("lineDisplay error: must claim it first");
        }
    }

    public String getDeviceServiceDescription() throws JposException {
        return deviceServiceDescription;
    }

    public int getDeviceServiceVersion() throws JposException {
        return deviceServiceVersion;
    }

    public boolean getFreezeEvents() throws JposException {
        return freezeEvents;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
        this.freezeEvents = freezeEvents;
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return physicalDeviceDescription;
    }

    public String getPhysicalDeviceName() throws JposException {
        return physicalDeviceName;
    }

    public int getState() throws JposException {
        return state;
    }

    public int getCharacterSet() throws JposException {
        return characterSet;
    }

    public void setCharacterSet(int characterSet) throws JposException {
        this.characterSet = characterSet;
    }

    public String getCharacterSetList() throws JposException {
        return characterSetList;
    }

    public int getColumns() throws JposException {
        return columns;
    }

    public int getCurrentWindow() throws JposException {
        return currentWindow;
    }

    public void setCurrentWindow(int currentWindow) throws JposException {
        this.currentWindow = currentWindow;
    }

    public int getCursorColumn() throws JposException {
        return cursorColumn;
    }

    public void setCursorColumn(int cursorColumn) throws JposException {
        this.cursorColumn = cursorColumn;
    }

    public int getCursorRow() throws JposException {
        return cursorRow;
    }

    public void setCursorRow(int cursorRow) throws JposException {
        this.cursorRow = cursorRow;
    }

    public boolean getCursorUpdate() throws JposException {
        return cursorUpdate;
    }

    public void setCursorUpdate(boolean cursorUpdate) throws JposException {
        this.cursorUpdate = cursorUpdate;
    }

    public int getDeviceBrightness() throws JposException {
        return deviceBrightness;
    }

    public void setDeviceBrightness(int deviceBrightness)
                       throws JposException {
        this.deviceBrightness = deviceBrightness;
    }

    public int getDeviceColumns() throws JposException {
        return deviceColumns;
    }

    public int getDeviceDescriptors() throws JposException {
        return deviceDescriptors;
    }

    public int getDeviceRows() throws JposException {
        return deviceRows;
    }

    public int getDeviceWindows() throws JposException {
        return deviceWindows;
    }

    public int getInterCharacterWait() throws JposException {
        return interCharacterWait;
    }

    public void setInterCharacterWait(int interCharacterWait)
                       throws JposException {
        this.interCharacterWait = interCharacterWait;
    }

    public int getMarqueeFormat() throws JposException {
        return marqueeFormat;
    }

    public void setMarqueeFormat(int marqueeFormat) throws JposException {
        this.marqueeFormat = marqueeFormat;
    }

    public int getMarqueeRepeatWait() throws JposException {
        return marqueeRepeatWait;
    }

    public void setMarqueeRepeatWait(int marqueeRepeatWait)
                       throws JposException {
        this.marqueeRepeatWait = marqueeRepeatWait;
    }

    public int getMarqueeType() throws JposException {
        return marqueeType;
    }

    public void setMarqueeType(int marqueeType) throws JposException {
        this.marqueeType = marqueeType;
    }

    public int getMarqueeUnitWait() throws JposException {
        return marqueeUnitWait;
    }

    public void setMarqueeUnitWait(int marqueeUnitWait)
                       throws JposException {
        this.marqueeUnitWait = marqueeUnitWait;
    }

    public int getRows() throws JposException {
        return rows;
    }

    public int getPowerNotify() throws JposException {
        return powerNotify;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        this.powerNotify = powerNotify;
    }

    public int getPowerState() throws JposException {
        return powerState;
    }

    public void connect() {
        portList = CommPortIdentifier.getPortIdentifiers();
        String devicePortName = (String)entry.getPropertyValue("DevicePortName").toString();
        CreamToolkit.logMessage("Firich2x20LineDisplay: Prepare connecting to " + devicePortName);
        while (portList.hasMoreElements()) {
            portId = (CommPortIdentifier) portList.nextElement();
            CreamToolkit.logMessage("Firich2x20LineDisplay: Iterating " + portId.getName());

            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL
                  && portId.getName().equalsIgnoreCase(devicePortName)) {

                if (portId.isCurrentlyOwned()) {
                    CreamToolkit.logMessage("lineDisplay error : The device is being use!");
                    //return;
                }

                CreamToolkit.logMessage("Firich2x20LineDisplay: Open " + portId.getName());
                try {
                    serialPort = (SerialPort)portId.open("Firich2x20LineDisplay", 10000);
                } catch (PortInUseException e1) {
                    CreamToolkit.logMessage(e1.toString());
                    CreamToolkit.logMessage("Port in use: " + portId + ", at " + this);
                    break;
                }
                try {
                    outputStream = serialPort.getOutputStream();
                } catch (IOException e2) {
                    CreamToolkit.logMessage(e2.toString());
                    CreamToolkit.logMessage("IO exception at " + this);
                    break;
                }
            }
        }
        try {
            serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8,
                                                 SerialPort.STOPBITS_1,
                                                 SerialPort.PARITY_NONE);
        } catch (UnsupportedCommOperationException e3) {
            CreamToolkit.logMessage(e3.toString());
            CreamToolkit.logMessage("Unsupported comm operation at " + this);
            return;
        }

        try
        {
            clearText();
        }
        catch (JposException e)
        {
            e.printStackTrace();
        }

        claimed = true;
        deviceEnabled = false;
        return;
    }

    // Methods supported by all device services.
    public void claim(int timeout) throws JposException {
        if (claimed) {
            //CreamToolkit.logMessage("lineDisplay error : device has been claimed");
            return;
        }
        //synchronized (mutex) {
            if (claimedControl == null) {
                claimedControl = (LineDisplay)eventCallbacks.getEventSource();
                connect();
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                    claimedControl = (LineDisplay)eventCallbacks.getEventSource();
                    connect();
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        // wait() returns when time is up or being notify().

                        /////////// plus checking claimedControl with "this"
                        if (claimedControl == null) {
                            claimedControl = (LineDisplay)eventCallbacks.getEventSource();
                            connect();
                            return;
                        }

                        // if wait not enough time, it should keep wait() again....
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    CreamToolkit.logMessage("lineDisplay error : device is busy");
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        //}
    }

    public void close() throws JposException {
        claimed = false;
        deviceEnabled = false;
        opened = false;
    }

    public void checkHealth(int level) throws JposException {
    }

    public void deleteInstance() throws JposException {
    }

    public void directIO(int command, int[] data, Object object)
                       throws JposException {
    }

    public void open(String logicalName, EventCallbacks cb)
                        throws JposException {
        System.out.println("--- Firich2x20LineDisplay open ........");
        eventCallbacks = cb;
        this.setCursorColumn(0);
        this.setCursorRow(0);
        String str = "                                        ";
        ch = str.toCharArray();
        opened = true;
        claimed = false;
        deviceEnabled = false;
    }

    synchronized public void release() throws JposException {
        synchronized (mutex) {
            if (!claimed) {
                CreamToolkit.logMessage("lineDisplay error : device has been release");
            }
            try {
                outputStream.write(27);
                outputStream.write('@');
                sleep();
            } catch (IOException e4) {
                CreamToolkit.logMessage(e4.toString());
                CreamToolkit.logMessage("IO exception at " + this);
            }
            serialPort.close();
            outputStream = null;
            portList = null;
            portId = null;
            serialPort = null;
            claimed = false;
            deviceEnabled = false;
            claimedControl = null;
            mutex.notifyAll();
        }
    }

    public void clearDescriptors() throws JposException {
        if (!capDescriptors) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL);
        } else {
            deviceServiceDescription = "";
            physicalDeviceDescription = "";
        }
    }

    public void clearText() throws JposException {
        if (deviceEnabled) {
            try {
                outputStream.write(27);
                outputStream.write('@');
                displayText("                    ", 1);
                displayText("                    ", 2);
                outputStream.flush();
            } catch (IOException e4) {
                CreamToolkit.logMessage(e4.toString());
                CreamToolkit.logMessage("IO exception at " + this);
            }
            setCursorColumn(0);
            setCursorRow(0);
        } else {
            CreamToolkit.logMessage("lineDisplay error : Not DeviceEnabled");
        }
    }

    public void createWindow(int viewportRow, int viewportColumn,
                       int viewportHeight, int viewportWidth, int windowHeight,
                       int windowWidth) throws JposException {
    }

    public void destroyWindow() throws JposException {
    }

    public void displayText(String data, int attribute) throws JposException {
        if (!claimed) {
            CreamToolkit.logMessage("lineDisplay error : Please first claim it!");
            return;
        }
        if (!deviceEnabled) {
            CreamToolkit.logMessage("lineDisplay error : Not setDeviceEnable");
            return;
        }
        for(; data.length() > 20; data = data.substring(0, data.length() - 2));
        for(; data.length() < 20; data = " " + data);
        try {
            byte b1[] = data.getBytes(CreamToolkit.getEncoding());
            if (attribute == 1) {
                outputStream.write(27);
                outputStream.write('Q');
                outputStream.write('A');
                outputStream.write(b1);
                outputStream.write(0x0D);
            } else {
                outputStream.write(27);
                outputStream.write('Q');
                outputStream.write('B');
                outputStream.write(b1);
                outputStream.write(0x0D);
            }
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        claimed = true;
        deviceEnabled = true;
    }

    public void displayTextAt(int y, int x, String data,
        int attribute) throws JposException {
        displayText(data, attribute);
    }

    public void refreshWindow(int window) throws JposException {
    }

    public void scrollText(int direction, int units) throws JposException {
        this.direction = direction;
        this.units = units;
        String s = "                                        ";
        char c[] = s.toCharArray();

        if (direction == DISP_ST_LEFT) {
            for(int i = 0; i < 20 - units; i++) {
                c[i] = ch[units + i];
                c[20 + i] = ch[20 + units + i];
            }
        } else if(direction == DISP_ST_RIGHT) {
            for(int i = 0; i < 20 - units; i++) {
                c[units + i] = ch[i];
                c[20 + units + i] = ch[20 + i];
            }
        }
    }

    public void setDescriptor(int descriptor, int attribute)
        throws JposException {
    }

//    private void displayState(int state, int flag) {
//        try {
//            outputStream.write(27);
//            outputStream.write(115);
//            outputStream.write(state);
//            outputStream.write(flag);
//            sleep();
//            outputStream.flush();
//        } catch (IOException e4) {
//            CreamToolkit.logMessage(e4.toString());
//            CreamToolkit.logMessage("IO exception at " + this);
//        }
//    }

    private void sleep() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static int DISP_ST_UP = 0;
    public static int DISP_ST_DOWN = 1;
    public static int DISP_ST_LEFT = 2;
    public static int DISP_ST_RIGHT = 3;
    private int direction = DISP_ST_LEFT;
    private int units = 0;
}


