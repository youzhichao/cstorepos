package hyi.jpos.services;

import java.awt.Toolkit;
import java.io.IOException;

import jpos.CashDrawerConst;
import jpos.JposConst;
import jpos.JposException;
import jpos.config.simple.SimpleEntry;
import jpos.events.StatusUpdateEvent;
import jpos.services.CashDrawerService14;
import jpos.services.EventCallbacks;

public class FirichCashDrawer extends DeviceService implements CashDrawerService14 {
    
    public FirichCashDrawer () {
        System.out.println("Construct FirichCashDrawer...");
    }
    
    public FirichCashDrawer (SimpleEntry entry) {
	}
	
    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        throw new JposException (JposConst.JPOS_E_NOSERVICE, "No service available now!");
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    public void checkHealth(int level)  throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available now!");
    }

    public boolean getCapStatus() throws JposException {
        return true;
    }

	public boolean getDrawerOpened() throws JposException {
        try {
            Process p = Runtime.getRuntime().exec("/root/get_firich_cash_drawer_status");
            return p.waitFor() == 1;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // Methods
	public void openDrawer() throws JposException {
        if (!getDeviceEnabled())
			throw new JposException(JposConst.JPOS_E_DISABLED,"Cash drawer is not enabled!");

        try {
            Runtime.getRuntime().exec("/root/open_firich_cash_drawer");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
	public void waitForDrawerClose(int beepTimeout, int beepFrequency,
        int beepDuration, int beepDelay) throws JposException {
        if (!getDeviceEnabled())
			throw new JposException(JposConst.JPOS_E_DISABLED,"Cash drawer is not enabled!");

        long start = System.currentTimeMillis();
        long waitTime = beepTimeout;
        while (true) {
            if (!getDrawerOpened()) {
                break;
            }
            if (waitTime > 0) {
            	waitTime = beepTimeout - (System.currentTimeMillis() - start);
                try {
                	Thread.sleep(100);
                } catch (Exception e) {
                }
            } else {
                Toolkit.getDefaultToolkit().beep();
                try {
                	Thread.sleep(beepDelay);
                } catch (Exception e) {
                }
            }
        }
        /*
		new Beep(beepTimeout, beepDelay);
        while (!drawerClosed) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
        }
        */

        //synchronized (waitBeep) {
        //    try {
		//		waitBeep.wait();
        //    } catch (InterruptedException ie) {
        //        if (!getDrawerOpened())
        //            return;
        //        else
        //            throw new JposException (JposConst.JPOS_E_FAILURE, this.toString(), ie);
        //    }
        //}
        eventCallbacks.fireStatusUpdateEvent(new StatusUpdateEvent(eventCallbacks.getEventSource(),
			CashDrawerConst.CASH_SUE_DRAWERCLOSED));
    }

    class Beep extends Thread {
        private int beepTimeout;
        private int beepDelay;
        public Beep() {
            start();
        }
        
        public Beep(int beepTimeout, int beepDelay) {
            this.beepTimeout = beepTimeout;
            this.beepDelay = beepDelay;
            start();
        }
        public void run() {
            try {
                long start = System.currentTimeMillis();
                long waitTime = beepTimeout;
                try {
					while (waitTime > 0) {
                        if (!getDrawerOpened()) {
                            return;
							//synchronized (waitBeep) {
                            //    waitBeep.notifyAll();
                            //    return;
                            //}
                        }
                        Thread.sleep(100);
						waitTime = beepTimeout - (System.currentTimeMillis() - start);
                    }
                    while (true){
                        if (!getDrawerOpened()) {
                            return;
							//synchronized (waitBeep) {
                            //    waitBeep.notifyAll();
                            //    return;
                            //}
                        }
                        Toolkit.getDefaultToolkit().beep();
						sleep(beepDelay);
						//System.out.println(beepDelay);
					}
                } catch (JposException je) {
                        System.out.println(je + this .toString() );
                }
            } catch (InterruptedException e) {
						System.out.println(e + this .toString());
			}
        }
    }

	public void open(String logicalName, EventCallbacks cb) throws JposException {
		super.open(logicalName, cb);
    }

    public void close() throws JposException {
        if (getClaimed())
            release();
        super.close();
    }

    public void claim(int timeout) throws JposException {
    }

    public void release() throws JposException {  
	}

	public void deleteInstance() throws JposException {
    }
}