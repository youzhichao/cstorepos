
// Copyright (c) 2000 hyi
package hyi.jpos.services;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class KBRead { 

    public native static char read();

    static {
        System.loadLibrary("kb");
    }
}

 
