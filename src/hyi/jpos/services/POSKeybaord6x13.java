package hyi.jpos.services;

import hyi.cream.POSButtonHome;
import hyi.cream.POSTerminalApplication;
import hyi.cream.dac.DacTransfer;
import hyi.cream.uibeans.*;
import hyi.cream.util.CreamToolkit;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import jpos.JposConst;
import jpos.JposException;
import jpos.POSKeyboard;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import jpos.events.DataEvent;
import jpos.services.EventCallbacks;
import jpos.services.POSKeyboardService14;

/**
 * POSKeybaord device service for 6x13 POS keyboard.
 * 
 * @author Bruce You
 */
public class POSKeybaord6x13 implements POSKeyboardService14, KeyListener, ContainerListener {

    static private POSKeyboard claimedControl;
    static private Object mutex = new Object();

    protected EventCallbacks eventCallbacks      = null;
    protected JposEntry entry                    = null;
    protected Container comp                     = null;
    protected Container comp2                    = null;
    protected Container comp3                    = null;
    protected Container comp4                    = null;

    private boolean claimed                      = false;
    private boolean deviceEnabled                = false;

    protected int posKeyData                     = 0;
    protected String checkHealthText             = "";
    protected String deviceServiceDescription    = "JavaPOS Service from HYI Corporation";
    protected int deviceServiceVersion           = 1005000;
    protected boolean freezeEvents               = false;
    protected String physicalDeviceDescription   = "";
    protected String physicalDeviceName          = "";
    protected int state                          = JposConst.JPOS_S_CLOSED;
    protected int powerNotify                    = JposConst.JPOS_PN_DISABLED;
    protected int powerState                     = JposConst.JPOS_PS_UNKNOWN;

    private Map keyMap = new HashMap();
    private int scannerPrefixCharValue;


    /**
     * Key code map:
     * Q   W   E   R   T   Y  F1  F2  F3   U   I  F4  F5 
     * 1   7  13  19  25  31  37  43  49  55  61  67  73
     * 
     * A   S   D   F   G   H  F6  F7  F8   J   K  F9  F10
     * 2   8  14  20  26  32  38  44  50  56  62  68  74
     * 
     * Z   X   C   V   B   N   7   8   9   M   <   o   O  
     * 3   9  15  21  27  33  39  45  51  57  63  69  75
     * 
     * q   w   e   r   t   y   4   5   6   u   i   p   P  
     * 4  10  16  22  28  34  40  46  52  58  64  70  76
     * 
     * a   s   d   f   g   h   1   2   3   j   k   l   L  
     * 5  11  17  23  29  35  41  47  53  59  65  71  77
     * 
     * z   x   c   v   b   n   0   /   .   m   , Enter Enter
     * 6  12  18  24  30  36  42  48  54  60  66  72  72
     */
    public POSKeybaord6x13(SimpleEntry entry) {
        this.entry = entry;

        String[] keys = new String[] {
            // 1 - 6
            "Shift-Q", "Shift-A", "Shift-Z", "Q", "A", "Z",
            // 7 - 12
            "Shift-W", "Shift-S", "Shift-X", "W", "S", "X",
            // 13 - 18
            "Shift-E", "Shift-D", "Shift-C", "E", "D", "C",
            // 19 - 24
            "Shift-R", "Shift-F", "Shift-V", "R", "F", "V",
            // 25 - 30
            "Shift-T", "Shift-G", "Shift-B", "T", "G", "B",
            // 31 - 36
            "Shift-Y", "Shift-H", "Shift-N", "Y", "H", "N",
            // 37 - 42
            "F1",  "F6", "7", "4", "1", "0",
            // 43 - 48
            "F2",  "F7", "8", "5", "2", "Slash",
            // 49 - 54
            "F3",  "F8", "9", "6", "3", "Period",
            // 55 - 60
            "Shift-U", "Shift-J", "Shift-M", "U", "J", "M",
            // 61 - 66
            "Shift-I", "Shift-K", "Shift-Comma", "I", "K", "Comma",
            // 67 - 72
            "F4",  "F9", "O", "P", "L", "Enter",  
            // 73 - 77
            "F5",  "F10", "Shift-O", "Shift-P", "Shift-L"
        };
        for (int i = 1; i <= keys.length; i++)
            keyMap.put(keys[i - 1], new Integer(i));
    }

    // Capabilities
    public boolean getCapKeyUp() throws JposException {
        return true;
    }

    public int getCapPowerReporting() throws JposException {
        return 0;
    }

    // Properties
    public boolean getAutoDisable() throws JposException {
        return true;
    }

    public void setAutoDisable(boolean autoDisable) throws JposException {
    }

    public int getDataCount() throws JposException {
        return 0;
    }

    public boolean getDataEventEnabled() throws JposException {
        return true;
    }

    public void setDataEventEnabled(boolean dataEventEnabled)
                       throws JposException {
    }

    public int getEventTypes() throws JposException {
        return 0;
    }

    public void setEventTypes(int eventTypes) throws JposException {
    }

    public int getPOSKeyData() throws JposException {
        return posKeyData;
    }

    public int getPOSKeyEventType() throws JposException {
        return 0;
    }

    public int getPowerNotify() throws JposException {
        return 0;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
    }

    public int getPowerState() throws JposException {
        return 0;
    }

    public String getCheckHealthText() throws JposException {
        return "";
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    public void setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        if (!claimed) {
            System.out.println("must claim it first");
            return;
        }
        this.deviceEnabled = deviceEnabled;
        if (deviceEnabled) {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                adkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                adkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                adkl(comp3);
            }
            if (entry.hasPropertyWithName("COMP4_CLASS_PROP_NAME")) {
                String className4 = (String)entry.getPropertyValue("COMP4_CLASS_PROP_NAME");
                comp4 = createComp(className4);
                adkl(comp4);
            }
        } else {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                rmkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                rmkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                rmkl(comp3);
            }
            if (entry.hasPropertyWithName("COMP4_CLASS_PROP_NAME")) {
                String className4 = (String)entry.getPropertyValue("COMP4_CLASS_PROP_NAME");
                comp4 = createComp(className4);
                rmkl(comp4);
            }
        }
    }

    public String getDeviceServiceDescription() throws JposException {
        return "";
    }

    public int getDeviceServiceVersion() throws JposException {
        return 0;
    }

    public boolean getFreezeEvents() throws JposException {
        return true;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return "";
    }

    public String getPhysicalDeviceName() throws JposException {
        return "";
    }

    public int getState() throws JposException {
        return 0;
    }

    // Methods
    public void clearInput() throws JposException {
    }

    /**
     * Indicate the ServiceInstance that its connection has been dropped
     * @since 0.1 (Philly 99 meeting)
     * @throws jpos.JposException if any error occurs
     */
    public void deleteInstance() throws JposException {
    }

    //--------------------------------------------------------------------------
    // Common overridden methods
    //

    public void open(String logicalName, EventCallbacks eventCallbacks)
                    throws JposException {
        this.eventCallbacks = eventCallbacks;
        claimed = false;
        deviceEnabled = false;

        scannerPrefixCharValue = 32; 
            //Integer.parseInt(GetProperty.getScannerPrefixCharValue("32"));
    }

    public void close() throws JposException {
        claimed = false;
        deviceEnabled = false;
        claimedControl = null;
    }

    public void claim(int timeout) throws JposException {
        if (claimed) {
            System.out.println("device has been claimed");
            return;
        }

        synchronized (mutex) {
            if (claimedControl == null) {
                Object obj = eventCallbacks.getEventSource();
                claimedControl = (POSKeyboard)obj;
                claimed = true;
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                    claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                    claimed = true;
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        if (claimedControl == null) {
                            claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                            claimed = true;
                            return;
                        }
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        }
        claimed = true;
    }

    public void checkHealth(int level) throws JposException {
    }

    public void directIO(int command, int[] data, Object object)
                       throws JposException {
    }

    public void release() throws JposException {
        synchronized (mutex) {
            if (!claimed) {
                System.out.println("device has been release");
            }
            claimed = false;
            deviceEnabled = false;
            claimedControl = null;
            mutex.notifyAll();
        }
    }

    public void keyReleased(KeyEvent e) {
    }

    private boolean checked = false;
    private int keyCode = 0;
    private String buttonCode = "";
    private boolean keylock = false;
    private boolean scanner = false;
    private int enterButtonCount = 0;
    public void keyPressed(KeyEvent e) {
        keyCode = e.getKeyCode();
        //System.out.println("POSKeyboard6x13> keyCode = " + keyCode);

        if (keyCode == scannerPrefixCharValue) {
            scanner = true;
            //System.out.println("POSKeyboard6x13> scanner begins...");
            return;
        } else if (scanner) {
            if (keyCode == 10) {
                //System.out.println("POSKeyboard6x13> scanner ends");
                scanner = false;
            }
            return;
        } else {
            if (e.getModifiers() == 0) {
                buttonCode = KeyEvent.getKeyText(keyCode);
            } else {
                buttonCode = KeyEvent.getKeyModifiersText(e.getModifiers()) + "-" + KeyEvent.getKeyText(keyCode);
            }
            //System.out.println("**************************");
            //System.out.println("POSKeyboard6x13> buttonCode = " + buttonCode);
            //System.out.println();
        }

        if (keyMap.containsKey(buttonCode)) {
            posKeyData = ((Integer)keyMap.get(buttonCode)).intValue();
            //System.out.println("POSKeyboard6x13> posKeyData = " + posKeyData);
        } else {
            //System.out.println("POSKeyboard6x13> not found key");
            return;
        }

        if (posKeyData == POSButtonHome.getInstance().getEnterCode()) {
            enterButtonCount++;
            if (enterButtonCount == 15) {
                CreamToolkit.logMessage("System is going to shutdown caused by pressing many Enter.");
                DacTransfer.shutdown(1);
            }
        } else {
            enterButtonCount = 0;
        }

        POSTerminalApplication app = POSTerminalApplication.getInstance();
        PopupMenuPane pop = app.getPopupMenuPane();
        PopupMenuPane1 pop1 = app.getPopupMenuPane1();
        SignOffForm signOffForm = app.getSignOffForm();
        ItemList itemList = app.getItemList();
        DacViewer dacViewer = app.getDacViewer();
        PayingPaneBanner payingPane = app.getPayingPane();

        //int pageUp = POSTerminalApplication.getInstance().getPOSButtonHome().getPageUpCode();
        //int pageDown = POSTerminalApplication.getInstance().getPOSButtonHome().getPageDownCode();

        if (signOffForm != null && signOffForm.isVisible()) {
            signOffForm.keyDataListener(posKeyData);
            return;
        } else if (pop.isVisible()) {
            pop.keyDataListener(posKeyData);
            return;
        } else if (pop1.isVisible()) {
            pop1.keyDataListener(posKeyData);
            return;
        } else if (itemList.isVisible()) {
            itemList.keyDataListener(posKeyData);
        } else if (payingPane.isVisible()) {
            payingPane.keyDataListener(posKeyData);
        }
        if (dacViewer.isVisible())
            dacViewer.keyDataListener(posKeyData);
        eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
    }

    synchronized public void keyTyped(KeyEvent e) {
    }

    public Container createComp(String className) {
        try {
            Class compClass = Class.forName(className);
            Method compMethod = compClass.getDeclaredMethod("getInstance",
                                                            new Class[0]);
            comp = (Container)(compMethod.invoke(null, new Object[0]));
        } catch (ClassNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Class is not found at " + this);
        } catch (IllegalAccessException ex) {
            CreamToolkit.logMessage(ex.toString());
            CreamToolkit.logMessage("Illegal access exception at " + this);
        } catch (NoSuchMethodException exc) {
            CreamToolkit.logMessage(exc.toString());
            CreamToolkit.logMessage("No such method at " + this);
        } catch (InvocationTargetException exce) {
            CreamToolkit.logMessage(exce.toString());
            CreamToolkit.logMessage("Invocation exception at " + this);
        }
        return comp;
    }

    public void adkl(Container comp) {
        comp.addKeyListener(this);
        comp.addContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                adkl(compo);
            } else {
                co.addKeyListener(this);
            }
        }
    }

    public void rmkl(Container comp) {
        comp.removeKeyListener(this);
        comp.removeContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                rmkl(compo);
            } else {
                co.removeKeyListener(this);
            }
        }
    }

    public void componentAdded(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            adkl((Container)e.getChild());
        } else {
            e.getChild().addKeyListener(this);
        }
    }

    public void componentRemoved(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            rmkl((Container)e.getChild());
        } else {
            e.getChild().removeKeyListener(this);
        }
    }
}