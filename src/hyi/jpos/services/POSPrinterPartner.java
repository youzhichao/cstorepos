package hyi.jpos.services;
import java.io.*;
import java.util.*;
import javax.comm.*;
import hyi.cream.util.*;
import jpos.*;
import jpos.services.*;
import jpos.events.*;
import hyi.jpos.loader.*;
import jpos.config.*;
import jpos.config.simple.*;

/**
 * TEC6400 class
 * @date 2002/3
 * @author Wissly
 */

public class POSPrinterPartner extends DeviceService implements ParallelPortEventListener, POSPrinterService14 {
    private Object waitObject = new Object();
    private StringParsingDRJST51 parse;
    private OutputStreamWriter dataOutput;
    private ParallelPort parallelPort;
    private CommPortIdentifier portId;
    private Enumeration portList;
    static private String logicalName;
    private boolean transactionJournalMode;
    private boolean transactionReceiptMode;
    private boolean transactionSlipMode;
    private StringBuffer transactionJournalBuffer = new StringBuffer();
    private StringBuffer transactionReceiptBuffer = new StringBuffer();
    private StringBuffer transactionSlipBuffer = new StringBuffer();
    private InputStream inputStream;

    /** serial port current feedback status got asyncronously */
    private byte[] readPort = new byte[10];
    private byte statusCode = 0x00;
    private JposEntry POSPrinterentry;
    private boolean insertionFlag = false;

    /** AsyncMode flag definition */
    private boolean asyncMode = false;
    private int characterSet = POSPrinterConst.PTR_CS_ASCII;
    private boolean flagWhenIdle = true;
    private int  outputID = 0;
    /**
     * Define Specified Font File
     */

    private RandomAccessFile fontFile;
    private OutputStream out = null;
    private int[] bitmaps = new int[16];

    private String toplogo = null;
    private String bottomlogo = null;
    static private POSPrinterPartner instance;

    public String getTopLogo() throws JposException {
        if (toplogo != null)
            return toplogo;
        else
            throw new JposException (JposConst.JPOS_E_ILLEGAL, "Null pointer logo detected!" );
    }

    public String getBottomLogo() throws JposException {
        if (bottomlogo != null)
            return bottomlogo;
        else
            throw new JposException (JposConst.JPOS_E_ILLEGAL, "Null pointer logo detected!" );
    }

    public POSPrinterPartner() throws IOException {
        if (System.getProperties().get("os.name").toString().indexOf("Windows") != -1)
            out = new FileOutputStream("lpt1");
        else
            out = new FileOutputStream("/dev/lp0");

        //openPrinter();
        dataOutput=new OutputStreamWriter(out);
        out.write(0x1B);
        out.write('@');
        instance = this;
    }

    private void openPrinter() {
        if (portId != null) {
            try {
                parallelPort = (ParallelPort) portId.open(logicalName, 10000);
                out = parallelPort.getOutputStream();
                return;
            } catch (PortInUseException e) {
            } catch (IOException e) {
            }
        }

        portList = CommPortIdentifier.getPortIdentifiers();
        while (portList.hasMoreElements()) {
            portId = (CommPortIdentifier) portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_PARALLEL
                && portId.getName().equalsIgnoreCase("LPT1")) {
                try {
                    parallelPort =
                        (ParallelPort) portId.open(logicalName, 10000);
                    out = parallelPort.getOutputStream();
                    return;
                } catch (PortInUseException e) {
                    CreamToolkit.logMessage("openPrinter(): " + e.getMessage());
                } catch (IOException e) {
                    CreamToolkit.logMessage("openPrinter(): " + e.getMessage());
                }
                break;
            }
        }
        portId = null;
    }


    static public POSPrinterPartner getInstance() {
         return instance;
    }

    /**
    * constructor with JposEntry parameter
    */
    public POSPrinterPartner(SimpleEntry entry) throws IOException {
        this();
        POSPrinterentry = entry;
    }
    /**
    * Service13 Capabilities
    */
    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_STANDARD;
    }
    //--------------------------------------------------------------------------

    /**
    * Service12 Capabilities
    */
    public int getCapCharacterSet() throws JposException {
        return POSPrinterConst.PTR_CCS_ASCII;
    }

    //Journal and Receipt stations can print at the same time.
    public boolean getCapConcurrentJrnRec() throws JposException {
        return true;
    }

    //If true, then the Journal and Slip stations can print at the same time.
    public boolean getCapConcurrentJrnSlp() throws JposException {
        return false;
    }
    //If true, then the Receipt and Slip stations can print at the same time.
    public boolean getCapConcurrentRecSlp() throws JposException {
        return false;
    }
    //If true, then the printer has a "cover open” sensor.
    public boolean getCapCoverSensor() throws JposException {
        return false;
    }
    //If true, then the journal can print dark plus an alternate color.
    public boolean getCapJrn2Color() throws JposException {
        return false;
    }
    //If true, then the journal can print bold characters.
    public boolean getCapJrnBold() throws JposException {
        return false;
    }

    //If true, then the journal can print double high characters.
    public boolean getCapJrnDhigh() throws JposException {
        return false;
    }

    //If true, then the journal can print double wide characters.
    public boolean getCapJrnDwide() throws JposException {
        return true;
    }

    //If true, then the journal can print double high / double wide characters.
    public boolean getCapJrnDwideDhigh() throws JposException {
        return false;
    }

    //If true, then the journal has an out-of-paper sensor.
    public boolean getCapJrnEmptySensor() throws JposException {
        return true;
    }
    //If true, then the journal can print italic characters.
    public boolean getCapJrnItalic() throws JposException {
        return false;
    }

    //If true, then the journal has a low paper sensor.
    public boolean getCapJrnNearEndSensor() throws JposException {
        return true;
    }

    //If true, then the journal print station is present.
    public boolean getCapJrnPresent() throws JposException {
        return true;
    }

    //If true, then the journal can underline characters.
    public boolean getCapJrnUnderline() throws JposException {
        return false;
    }

    //If true, then the receipt can print dark plus an alternate color.
    public boolean getCapRec2Color() throws JposException {
        return false;
    }
    //If true, then the receipt has bar code printing capability.
    public boolean getCapRecBarCode() throws JposException {
        return false;
    }

    //If true, then the receipt can print bitmaps.
    public boolean getCapRecBitmap() throws JposException {
        return true;
    }

    //If true, then the receipt can print bold characters.
    public boolean getCapRecBold() throws JposException {
        return false;
    }

    //If true, then the receipt can print double high characters.
    public boolean getCapRecDhigh() throws JposException {
        return false;
    }

    //If true, then the receipt can print double wide characters.
    public boolean getCapRecDwide() throws JposException {
        return true;
    }
    //If true, then the receipt can print double wide/high characters.
    public boolean getCapRecDwideDhigh() throws JposException {
        return false;
    }

    //If true, then the receipt has an out-of-paper sensor.
    public boolean getCapRecEmptySensor() throws JposException  {
        return true;
    }

    //If true, then the receipt can print italic characters.
    public boolean getCapRecItalic() throws JposException {
        return false;
    }

    //If true, then the receipt can print in rotated 90° left mode.
    public boolean getCapRecLeft90() throws JposException {
        return false;
    }

    //If true, then the receipt has a low paper sensor.
    public boolean getCapRecNearEndSensor() throws JposException {
        return true;
    }
    //If true, then the receipt can perform paper cuts.
    public boolean getCapRecPapercut() throws JposException {
        return true;
    }

    //If true, then the receipt print station is present.
    public boolean getCapRecPresent() throws JposException {
        return true;
    }

    //If true, then the receipt can print in a rotated 90° right mode.
    public boolean getCapRecRight90() throws JposException {
        return false;
    }

    //If true, then the receipt can print in a rotated upside down mode.
    public boolean getCapRecRotate180() throws JposException {
        return false;
    }

    //If true, then the receipt has a stamp capability.
    public boolean getCapRecStamp() throws JposException {
        return true;
    }

    //If true, then the receipt can underline characters.
    public boolean getCapRecUnderline() throws JposException {
        return false;
    }

    //If true, then the slip can print dark plus an alternate color.
    public boolean getCapSlp2Color() throws JposException {
        return false;
    }

    //If true, then the slip has bar code printing capability.
    public boolean getCapSlpBarCode() throws JposException {
        return false;
    }

    //If true, then the slip can print bitmaps.
    public boolean getCapSlpBitmap() throws JposException {
        return false;
    }

    //If true, then the slip can print bold characters.
    public boolean getCapSlpBold() throws JposException {
        return false;
    }

    //If true, then the slip can print double high characters.
    public boolean getCapSlpDhigh() throws JposException {
        return false;
    }

    //If true, then the slip can print double wide characters.
    public boolean getCapSlpDwide() throws JposException {
        return true;
    }

    //If true, then the slip can print double high / double wide characters.
    public boolean getCapSlpDwideDhigh() throws JposException {
        return false;
    }

    //If true, then the slip has a "slip in" sensor.
    public boolean getCapSlpEmptySensor() throws JposException {
        return true;
    }

    /*If true, then the slip is a full slip station. It can print full-length forms. If false,
     *then the slip is a “validation” type station. This usually limits the number of print
     *lines, and disables access to the receipt and/or journal stations while the validation
     *slip is being used.*/
    public boolean getCapSlpFullslip() throws JposException {
        return false;
    }

    //If true, then the slip can print italic characters.
    public boolean getCapSlpItalic() throws JposException {
        return false;
    }

    //If true, then the slip can print in a rotated 90° left mode.
    public boolean getCapSlpLeft90() throws JposException {
        return false;
    }

    //If true, then the slip has a “slip near end” sensor.
    public boolean getCapSlpNearEndSensor() throws JposException {
        return false;
    }

    //If true, then the slip print station is present.
    public boolean getCapSlpPresent() throws JposException {
        return true;
    }

    //If true, then the slip can print in a rotated 90° right mode.
    public boolean getCapSlpRight90() throws JposException {
        return false;
    }

    //If true, then the slip can print in a rotated upside down mode.
    public boolean getCapSlpRotate180() throws JposException {
        return false;
    }

    //If true, then the slip can underline characters.
    public boolean getCapSlpUnderline() throws JposException {
        return false;
    }

    //If true, then printer transactions are supported by each station.
    public boolean getCapTransaction() throws JposException {
        return true;
    }

    //the end of Service12 capabilities

    /**
    * Service13 Properties
    */
    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
        throw new  JposException(JposConst.JPOS_E_NOSERVICE, "No Service!");
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }


    /**
    * Service12 Properties
    */

    //Sync/Async mode
    public boolean getAsyncMode() throws JposException {
        return asyncMode;
    }

    public void setAsyncMode(boolean asyncMode) throws JposException {
        if (getAsyncMode() && !asyncMode) {
            this.asyncMode = false;
            System.out.println("POSPrinter runs in SyncMode!");
            return;
        } else if ( !getAsyncMode() && asyncMode ) {
            this.asyncMode = true;
            System.out.println("POSPrinter runs in AsyncMode!");
            return;
        } else
            return;
            //throw new  JposException(JposConst.JPOS_E_ILLEGAL, "Expected mode can't be obtained: Change to the same mode!");
    }

    public int getCharacterSet() throws JposException {
        return characterSet;
    }

    public void setCharacterSet(int characterSet) throws JposException {
        this.characterSet = characterSet;
    }


    //Holds the character set numbers. It consists of ASCII numeric set numbers
    //separated by commas.
    public String  getCharacterSetList() throws JposException {
         throw new JposException( JposConst.JPOS_E_NOSERVICE);
    }

    //If true, then the printer’s cover is open.
    public boolean getCoverOpen() throws JposException {
        throw new JposException( JposConst.JPOS_E_NOSERVICE);
    }

    //Holds the severity of the error condition. It has one of the following values:
    //PTR_EL_NONE No error condition is present.
    //PTR_EL_RECOVERABLE A recoverable error has occurred.(Example: Out of paper.)
    //PTR_EL_FATAL A non-recoverable error has occurred.(Example: Internal printer failure.)
    //--------------------------------------------------------------------------
    //This property is set just before delivering an ErrorEvent. When the error is
    //cleared, then the property is changed to PTR_EL_NONE.
    //--------------------------------------------------------------------------

    private int errorLevel = POSPrinterConst.PTR_EL_NONE;
    public int getErrorLevel() throws JposException {
        return errorLevel;
    }

    /*Holds the station or stations that were printing when an error was detected.
    This property will be set to one of the following values:
    PTR_S_JOURNAL PTR_S_RECEIPT
    PTR_S_SLIP PTR_S_JOURNAL_RECEIPT
    PTR_S_JOURNAL_SLIP PTR_S_RECEIPT_SLIP
    PTR_TWO_RECEIPT_JOURNAL PTR_TWO_SLIP_JOURNAL
    PTR_TWO_SLIP_RECEIPT
    This property is only valid if the ErrorLevel is not equal to PTR_EL_NONE. It is
    set just before delivering an ErrorEvent.
    */

    private int errorStation = 0;
    public int getErrorStation() throws JposException {
        return errorStation;
    }

    /*Holds a vendor-supplied description of the current error.
    This property is set just before delivering an ErrorEvent. If no description is
    available, the property is set to an empty string. When the error is cleared, then the
    property is changed to an empty string.*/
    private String errorString = "";
    public String  getErrorString() throws JposException {
        return errorString;
    }

    /*If true, a StatusUpdateEvent will be enqueued when the device is in the idle state.
    This property is automatically reset to false when the status event is delivered.
    The main use of idle status event that is controlled by this property is to give the
    application control when all outstanding asynchronous outputs have been
    processed. The event will be enqueued if the outputs were completed successfully
    or if they were cleared by the clearOutput method or by an ErrorEvent handler.
    If the State is already set to JPOS_S_IDLE when this property is set to true, then
    a StatusUpdateEvent is enqueued immediately. The application can therefore
    depend upon the event, with no race condition between the starting of its last
    asynchronous output and the setting of this flag.
    This property is initialized to false by the open method.
    */
    public boolean getFlagWhenIdle() throws JposException {
        return flagWhenIdle;
    }

    public void setFlagWhenIdle(boolean flagWhenIdle) throws JposException {
        this.flagWhenIdle = flagWhenIdle;
    }


   /*Holds the fonts and/or typefaces that are supported by the printer. The string
    consists of font or typeface names separated by commas.
    This property is initialized by the open method.
   */
    public String getFontTypefaceList() throws JposException {
        return "";
    }

    public boolean getJrnEmpty() throws JposException {
        if (!getCapJrnEmptySensor())
             return false;
        try {
            dataOutput.write ("\u001Dr\u0001");
            dataOutput.flush ();
            inputStream.read (readPort);
        } catch ( IOException ie ) {
            throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(), ie);
        }
        statusCode = readPort[0];
        if ((statusCode&0x01) == 0x00)
            return false;
        else
            return true;
    }

    public boolean getJrnLetterQuality() throws JposException {
        throw new  JposException( JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void  setJrnLetterQuality(boolean jrnLetterQuality) throws JposException{
        throw new  JposException( JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    //Holds the number of characters that may be printed on a journal line.
    private int jrnLineChars = 24;
    public int getJrnLineChars() throws JposException  {
        return jrnLineChars;
    }
    //This property is initialized to the printer’s default line character width
    //when the device is first enabled following the open method.
    public void setJrnLineChars(int jrnLineChars) throws JposException {
        if (jrnLineChars < 0 || jrnLineChars>24)
            throw new  JposException(JposConst.JPOS_E_ILLEGAL, "Invalid agrument:"+jrnLineChars);
        this.jrnLineChars = jrnLineChars;
    }

    public String getJrnLineCharsList() throws JposException {
        return "12,24";
    }

    public int getJrnLineHeight() throws JposException {
        throw new  JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setJrnLineHeight(int jrnLineHeight) throws JposException {
        throw new  JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getJrnLineSpacing() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setJrnLineSpacing(int jrnLineSpacing) throws JposException {
        throw new  JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getJrnLineWidth() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public boolean getJrnNearEnd() throws JposException {
        if (!getCapJrnNearEndSensor())
            return false;
        try {
            dataOutput.write ("\u001Dr\u0001");
            dataOutput.flush ();
            inputStream.read (readPort);
        } catch ( IOException ie ) {
            throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(), ie);
        }
        statusCode = readPort[0];
        if ((statusCode&0x01) == 0x00)
            return false;
        else
            return true;
    }

    public int getMapMode() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setMapMode(int mapMode) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }



    public String  getRecBarCodeRotationList() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public boolean getRecEmpty() throws JposException {
        if (!getCapRecEmptySensor())
            return false;
        try {
            dataOutput.write ("\u001Dr\u0001");
            dataOutput.flush ();
            inputStream.read (readPort);
        } catch ( IOException ie ) {
            throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(), ie);
        }
        statusCode = readPort[0];//if statusCode equals zero, the paper is present
        if ((statusCode&0x02) == 0x00)
            return false;
        else
            return true;
    }

    public boolean getRecLetterQuality() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setRecLetterQuality(boolean recLetterQuality) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    private int recLineChars = 24;
    public int getRecLineChars() throws JposException {
        return recLineChars;
    }

    public void setRecLineChars(int recLineChars) throws JposException {
        if (recLineChars < 0 || recLineChars>24)
            throw new  JposException(JposConst.JPOS_E_ILLEGAL, "Invalid agrument:"+jrnLineChars);
        this.recLineChars = recLineChars;
    }

    public String getRecLineCharsList() throws JposException {
        return "12,24";
    }

    public int getRecLineHeight() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setRecLineHeight(int recLineHeight) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getRecLineSpacing() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setRecLineSpacing(int recLineSpacing) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    private int recLinesToPaperCut = 0;
    public int getRecLinesToPaperCut() throws JposException {
        return recLinesToPaperCut;
    }

    public int getRecLineWidth() throws JposException {
        throw new  JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public boolean getRecNearEnd() throws JposException {
        if (!getCapRecNearEndSensor())
            return false;
        try {
            dataOutput.write ("\u001Dr\u0001");
            dataOutput.flush ();
            inputStream.read (readPort);
        } catch ( IOException ie ) {
            throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(), ie);
        }
        statusCode = readPort[0]; //if statusCode equals zero, the paper is present
        if ((statusCode&0x02) == 0x00)
            return false;
        else
            return true;
    }

    public int getRecSidewaysMaxChars() throws JposException {
        return 0;
    }

    public int getRecSidewaysMaxLines() throws JposException {
        return 0;
    }

    public int getRotateSpecial() throws JposException {
        throw new  JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setRotateSpecial(int rotateSpecial) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public String getSlpBarCodeRotationList() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public boolean getSlpEmpty() throws JposException {
        if (!getCapSlpEmptySensor())
            return false;
        try {
            dataOutput.write ("\u001Dr\u0001");
            dataOutput.flush ();
            inputStream.read (readPort);
        } catch ( IOException ie ) {
            throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(), ie);
        }
        statusCode = readPort[0];//if statusCode equals zero, the paper is present
        if ((statusCode&0x20) == 0x00)
            return false;
        else
            return true;

    }

    public boolean getSlpLetterQuality() throws JposException {
        throw new  JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setSlpLetterQuality(boolean recLetterQuality) throws JposException {
        throw new  JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    //--------------------------------------------------------------------------
    private int slpLineChars = 55;
    public int getSlpLineChars() throws JposException {
        return slpLineChars;
    }

    public void setSlpLineChars(int slpLineChars) throws JposException {
        if (slpLineChars<0 || slpLineChars>55)
            throw new JposException (JposConst.JPOS_E_ILLEGAL, "Invalid agrument:"+slpLineChars);
        this.slpLineChars = slpLineChars;
    }
    //--------------------------------------------------------------------------

    public String  getSlpLineCharsList() throws JposException {
        return "27,55";
    }

    public int getSlpLineHeight() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setSlpLineHeight(int slpLineHeight) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getSlpLinesNearEndToEnd() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getSlpLineSpacing() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public void setSlpLineSpacing(int slpLineSpacing) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int  getSlpLineWidth() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getSlpMaxLines() throws JposException {
        return 1;
    }

    public boolean getSlpNearEnd() throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
    }

    public int getSlpSidewaysMaxChars() throws JposException {
        return 0;
    }

    public int getSlpSidewaysMaxLines() throws JposException {
        return 0;
    }
    //--------------------------------------------------------------------------
    public int getOutputID() throws JposException {
        return outputID;
    }

    //new...begin Service12 Methods
    //first sector the synchronized method
    synchronized public void beginInsertion ( int timeout ) throws JposException {
        if ( timeout< -1 )
            throw new  JposException( JposConst.JPOS_E_ILLEGAL, "Illegal parameter:" + timeout);
        if ( ! getCapSlpPresent ( ) )
            throw new  JposException( JposConst.JPOS_E_ILLEGAL, "Not supported");
    }

    synchronized public void endInsertion() throws JposException {
        insertionFlag = true;
    }


    synchronized public void beginRemoval(int timeout) throws JposException {
        if ( timeout< -1 )
            throw new  JposException( JposConst.JPOS_E_ILLEGAL, "Illegal parameter:" + timeout);
        if ( ! getCapSlpPresent ( ) )
            throw new  JposException( JposConst.JPOS_E_ILLEGAL, "Not supported");
    }

    synchronized public void endRemoval() throws JposException {
        insertionFlag = true;
    }
    //end of first sector the synchronized method


    public void clearOutput() throws JposException {
        outputID = 0;
    }
    public void printBarCode(int station, String data, int symbology,
                                int height, int width, int alignment,
                                int textPosition) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not support!");
    }//end of printBarCode

    public void printBitmap(int station, String fileName, int width,
                               int alignment) throws JposException {
        throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not support!");
    }//end of printBitmap

    public void printImmediate(String str) {
        Iterator iter = new CharIterator(str);
        while (iter.hasNext()) {
            String c = (String) iter.next();
            if (isChineseChar(c)) {
                printChineseCharacter(c);
            } else {
                printAlphanumeric((byte) c.charAt(0));
            }
        }
    }

   public void printImmediate(int station, String data) throws JposException {
      if (!getClaimed())
           throw new JposException(JposConst.JPOS_E_NOTCLAIMED, "Please claim the printer before manipulation!");
      if (!getDeviceEnabled())
           throw new JposException(JposConst.JPOS_E_DISABLED, "Printer is not enabled!");
      String realCommand = parse.getValidCommand(station, data);
      if (realCommand == null)
            throw new JposException(JposConst.JPOS_E_ILLEGAL,
                "Invalid station argument:" + station);
      else if (data.equals(""))
            return;
        try {
            printImmediateWithLineFeed(realCommand);
            dataOutput.write (realCommand);
            dataOutput.flush ();
            synchronized (waitObject) {
                try {
                    waitObject.wait();
                } catch (InterruptedException ie) { throw new JposException (JposConst.JPOS_E_ILLEGAL); }
            }
        } catch (IOException ie) { throw new JposException (JposConst.JPOS_E_ILLEGAL);}
    }//end of printImmediate()



    //Main
    public void printNormal ( int station, String data ) throws JposException {
        if (data.equals(""))
        return;
         printdata(data);
    }
    public void printTwoNormal(int stations, String data1, String data2)
                       throws JposException {
         if (data1.equals("") && data2.equals(""))
         return;
         printdata(data1);
         //printImmediateWithLineFeed(data1);
    }
    public void rotatePrint (int station, int rotation) throws JposException {
        throw new JposException( JposConst.JPOS_E_FAILURE, "Not supported in DRJST51!");
    }//end of rotatePrint

    public void setBitmap (int bitmapNumber, int station, String fileName,
                             int width, int alignment) throws JposException {
        throw new JposException( JposConst.JPOS_E_FAILURE, "Not supported in DRJST51!");
    }//end of setBitmap


    public void setLogo (int location, String data) throws JposException {
        //The logo to be set. Location may be PTR_L_TOP or PTR_L_BOTTOM.
        if (location == POSPrinterConst.PTR_L_TOP)
            toplogo = data;
        else if (location == POSPrinterConst.PTR_L_BOTTOM)
            bottomlogo = data;
        else
            throw new JposException( JposConst.JPOS_E_ILLEGAL, "An invalid location was specified:"+location+"." );
    }//end of setLogo
    //transactionPrint(int station, int control) throws JposException
    public void transactionPrint(int station, int control) throws JposException {
        if (!getCapTransaction())
            throw new JposException (JposConst.JPOS_E_ILLEGAL,
                        "Transaction mode is not supported!");
        switch (station) {
            case POSPrinterConst.PTR_S_JOURNAL:
                if (!getCapJrnPresent())
                    throw new JposException (JposConst.JPOS_E_ILLEGAL,
                        "The station:"+station+" is not present");
                if (control==POSPrinterConst.PTR_TP_TRANSACTION)
                    transactionJournalMode = true;
                else if (control==POSPrinterConst.PTR_TP_NORMAL)
                    transactionJournalMode = false;
                else
                    throw new JposException( JposConst.JPOS_E_ILLEGAL,
                        "Error occured--illegal argument:"+ control);
                break;
            case POSPrinterConst.PTR_S_RECEIPT:
                if (!getCapRecPresent())
                    throw new JposException (JposConst.JPOS_E_ILLEGAL,
                        "The station:"+station+" is not present");
                if (control==POSPrinterConst.PTR_TP_TRANSACTION)
                    transactionReceiptMode = true;
                else if (control==POSPrinterConst.PTR_TP_NORMAL)
                    transactionReceiptMode = false;
                else
                    throw new JposException( JposConst.JPOS_E_ILLEGAL, "This");
                break;
            case POSPrinterConst.PTR_S_SLIP:
                if (!getCapSlpPresent())
                    throw new JposException (JposConst.JPOS_E_ILLEGAL,
                        "The station:"+station+" is not present");
                if (control==POSPrinterConst.PTR_TP_TRANSACTION)
                    transactionSlipMode = true;
                else if (control==POSPrinterConst.PTR_TP_NORMAL)
                    transactionSlipMode = false;
                else
                    throw new JposException( JposConst.JPOS_E_ILLEGAL,
                        "Error occured--illegal argument:"+ control);
                break;
            default:
                throw new JposException(JposConst.JPOS_E_ILLEGAL, "This station is not supported by hyi.jpos.POSPrinterDRJST51!");
        }
    }// end of transactionPrint()



    public void validateData (int station, String data) throws JposException {
        if (!(station==POSPrinterConst.PTR_S_JOURNAL
            || station==POSPrinterConst.PTR_S_RECEIPT
            || station==POSPrinterConst.PTR_S_SLIP ))
                throw new JposException( JposConst.JPOS_E_ILLEGAL,
                    "Invalid station argument:" + station );
        if (data.equals(""))
            return;
        String item = null;
        String[] commonillegal = { "\u001B|rA", "\u001B|cA", "\u001B|3C", "\u001B|4C",
                          "\u001B|rC", "\u001B|rvC", "\u001B|iC", "\u001B|bC" };
        ArrayList ESCSequence = parse.getESCSequence ( data );
        ArrayList JposESCSequence = parse.getJposESCSequence ( data );
        if ( ESCSequence.isEmpty ( ) )
            return;
        if ( JposESCSequence.size ( ) != ESCSequence.size ( ) )
            throw new JposException(JposConst.JPOS_E_FAILURE,
                "Escape sequence is not supported by the printer:" + data );
        for ( int k = 0; k < JposESCSequence.size ( ); k++ ) {
            item = ( String ) JposESCSequence.get( k );
            if ( parse.contains( commonillegal, item ) )
                throw new JposException(JposConst.JPOS_E_FAILURE,
                    "Not supported by the printer:" + data);
            else if ( item.substring( item.length() - 1 ).equals("B")
                    || item.substring( item.length() - 2 ).equals("uF")
                    || item.substring( item.length() - 2 ).equals("rF")
                    || item.substring( item.length() - 2 ).equals("fT")
                    || item.substring( item.length() - 2 ).equals("uC")
                    || item.substring( item.length() - 2 ).equals("sC")
                    || item.substring( item.length() - 2 ).equals("hC")
                    || item.substring( item.length() - 2 ).equals("vC") )
                throw new JposException(JposConst.JPOS_E_FAILURE,
                     "Not supported by the printer:" + data);
        }
        switch ( station ) {
            case POSPrinterConst.PTR_S_JOURNAL:
                for ( int k = 0; k < JposESCSequence.size() ; k++ ) {
                    item = ( String ) JposESCSequence.get( k );
                    if ( item.substring( item.length() - 1 ).equals( "P" )
                         || item.substring( item.length() - 2 ).equals( "sL" ) )
                         throw new JposException(JposConst.JPOS_E_FAILURE,
                               "Appointed manipulation is not supported at the specified station:"
                                  + "station=" + station + " string=" + data );
                }
                break;
            case POSPrinterConst.PTR_S_RECEIPT:
                for ( int k = 0; k < JposESCSequence.size() ; k++ ) {
                    item = ( String ) JposESCSequence.get( k );
                    if ( item.charAt ( item.length() - 1 ) == 'P' && item.length ( ) != 3 )
                        try {
                             if ( Integer.decode( item.substring ( 2, item.length ( ) - 1 ) ).intValue() < 0
                                 ||  Integer.decode( item.substring ( 2, item.length ( ) - 1 ) ).intValue() > 100 )
                                     throw new JposException(JposConst.JPOS_E_ILLEGAL,
                                         "Assigned value is illegal:" + item.substring ( 2, item.length ( ) - 1 ) );
                        } catch ( Exception e ) { System.out.println ( e ); }
                }
                break;
             case POSPrinterConst.PTR_S_SLIP:
                 for ( int k = 0; k < JposESCSequence.size ( ) ; k++ ) {
                     item = ( String ) JposESCSequence.get ( k );
                         if ( item.substring( item.length ( ) - 1 ).equals( "P" )
                             || item.substring( item.length ( ) - 2 ).equals( "sL" )
                             || item.substring( item.length ( ) - 2 ).equals( "lF" ) )
                             throw new JposException(JposConst.JPOS_E_FAILURE,
                                 "Appointed manipulation is not supported at the specified station:"
                                      + "station=" + station + " string=" + data );
                 }
                 break;
         }
    }
    //The end of Service12 Methods

    //--------------------------------------------------------------------------

    //new...begin BaseService Methods supported by all device services.
    public void claim(int timeout) throws JposException {
        super.claim(timeout);
    }


    public void release() throws JposException {
        if (!getClaimed())
            return;
        try {
            if (out != null)
                out.close();
            out = null;
            if (inputStream != null)
                inputStream.close();
            inputStream = null;
            if (dataOutput != null)
                dataOutput.close();
            dataOutput = null;
            super.release();
        } catch (IOException e) {
        }
    }

    //--------------------------------------------------------------------------
    //Close the device and unlink it from the device control!
    //--------------------------------------------------------------------------

    public void close() throws JposException  {
        super.close();
        if (getClaimed())
            release();
        if (getAsyncMode())
            clearOutput();
        asyncMode = false;
        this.logicalName = null;
    }

    // The end of close()
    public void checkHealth(int level) throws JposException {
        throw new JposException( JposConst.JPOS_E_NOSERVICE, this.toString());
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
        throw new JposException(JposConst.JPOS_E_NOSERVICE, this.toString());
    }

    public void  open(String logicalName, EventCallbacks cb) throws JposException {
        super.open (logicalName, cb);
        asyncMode = false;
        outputID=0;
        this.logicalName = logicalName;
    }


    //new...end BaseService Methods supported by all device services.


    //--------------------------------------------------------------------------
    /**
    * serialEventListenner implementation, it is in another thread,
    * executed automatically */
    public void parallelEvent(ParallelPortEvent ev) {
    }
    public void deleteInstance() throws JposException {}
    private void printdata(String str){
        try {
             out.write(str.getBytes(CreamToolkit.getEncoding()));
             out.flush();
        } catch (IOException ex) {
              ex.printStackTrace(System.err);
        }

    }
    private void printAlphanumeric(byte b) {
        try {
            out.write(b);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }
    private void printBitmap(int[] bytes) {
        //System.out.print("printBitmap(): ");
        try {
            // print bitmap
            out.write(new byte[] {
                0x1B, (byte) '*',
                0x01,                   // 0: 8-dot single density, 1: 8-dot double density
                (byte) bytes.length,    // number of dots in horizontal direction
                0x00                    // number of another 256 dots in horizontal direction
            });
            for (int i = 0; i < bytes.length; i++) {
                out.write(bytes[i]);
                //System.out.print(bytes[i] + ",");
            }
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }
    public void openDrawer() {
         try {
            out.write(0x1B);
            out.write('p');
            out.write(0x00);
            out.write(0x19);
            out.write(0xF0);
         } catch (IOException e) {
            e.printStackTrace(System.err);
         }
     }

    public void setRedColor() {
        try {
            out.write(0x1B);
            out.write('r');
            out.write(0x01);
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    public void setBlackColor() {
        try {
            out.write(0x1B);
            out.write('r');
            out.write(0x00);
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }
    public void cutPaper(int percentage) throws JposException {
       for (int i = 0; i < 6; i++) {
            printdata("\n");
        }
    }
    /**
     * Inner class for iterating Chinese (in GB code) or alphanumeric characters.
     */
    private class CharIterator implements Iterator {

        private byte[] str;
        private int index;
        private byte[] chineseChar = new byte[2];
        private byte[] alphanumChar = new byte[1];

        CharIterator(String str0) {
            try {
                if (str0 != null)
                    str = str0.getBytes(CreamToolkit.getEncoding());
            } catch (UnsupportedEncodingException e) {
            }
        }

        public boolean hasNext() {
            return (str == null) ? false : (index < str.length);
        }

        public Object next() {
            if (!hasNext())
                return null;

            int b = str[index] & 0xff;
            //System.out.print("b=" + Integer.toHexString(b) + " ");
            if (index + 1 < str.length) {
                int b2 = str[index + 1] & 0xff;
                //System.out.println("b2=" +Integer.toHexString(b2) + " ");
                if ((b >= 0xA1 && b <= 0xA9 || b >= 0xB0 && b <= 0xF7) // first-byte is a GB code
                    && (b2 >= 0xA1 && b2 <= 0xFE) // second-byte is a GB code
                ) {
                    chineseChar[0] = (byte) b;
                    chineseChar[1] = (byte) b2;
                    index += 2;
                    try {
                        return new String(chineseChar, "ISO-8859-1");
                    } catch (UnsupportedEncodingException e) {
                        return "";
                    }
                }
            }
            alphanumChar[0] = (byte) b;
            index++;
            return new String(alphanumChar);
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    private boolean isChineseChar(String str) {
        return (str.length() == 2); // 2-byte if it is a Chinese char
    }

    /**
     * Get the Chinese font bitmap into upperBits and lowerBits
     */
    private void getBitmap(String c) {
        byte[] b = new byte[32]; // bitmap bytes read from font file
        int b1 = c.charAt(0);
        int b2 = c.charAt(1);
        int offset;
        offset = ((b1 - 0xA1) * (0xFE - 0xA0) + (b2 - 0xA1)) * 32;
        try {
            fontFile.seek(offset);
            fontFile.read(b);
        } catch (IOException e) {
            e.printStackTrace(System.err);
            return;
        }
        int[] bb = new int[8];
        int i, j, mask;
        for (i = 0; i < 8; i++) {
            mask = 0x80 >>> i;
            for (j = 0; j <= 30; j += 4) {
                bb[j / 4] = ((b[j] & mask) != 0 || (b[j + 2] & mask) != 0) ? 1 : 0;
            }
            bitmaps[i] =
                bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4 | bb[4] << 3
                | bb[5] << 2 | bb[6] << 1 | bb[7];
        }
        for (i = 0; i < 8; i++) {
            mask = 0x80 >>> i;
            for (j = 1; j <= 31; j += 4) {
                bb[j / 4] = ((b[j] & mask) != 0 || (b[j + 2] & mask) != 0) ? 1 : 0;
            }
            bitmaps[i + 8] =
                bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4 | bb[4] << 3
                | bb[5] << 2 | bb[6] << 1 | bb[7];
        }
    }

    private void getBitmap(String c, int[] upperBits, int[] lowerBits) {
        byte[] b = new byte[18]; // bitmap bytes read from font file
        int b1 = c.charAt(0);
        int b2 = c.charAt(1);
        int offset;

        if (b1 <= 0xA9)
            offset = ((b1 - 0xA1) * (0xFE - 0xA0) + (b2 - 0xA1)) * 18;
        else
            offset = (9 * (0xFE - 0xA0) + (b1 - 0xB0) * (0xFE - 0xA0) + (b2 - 0xA1)) * 18;
        try {
            fontFile.seek(offset);
            fontFile.read(b);
        } catch (IOException e) {
            e.printStackTrace(System.err);
            System.exit(2);
        }
        int[] bb = new int[8];
        int i, j, mask;
        for (i = 0; i < 8; i++) {
            mask = 0x80 >>> i;
            for (j = 0; j <= 14; j += 2) {
                bb[j / 2] = ((b[j] & mask) != 0) ? 1 : 0;
            }
            upperBits[i] =
                bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4 | bb[4] << 3
                | bb[5] << 2 | bb[6] << 1 | bb[7];
            lowerBits[i] = ((b[16] & mask) != 0) ? 0x80 : 0x00;
        }
        for (i = 0; i < 8; i++) {
            mask = 0x80 >>> i;
            for (j = 1; j <= 15; j += 2) {
                bb[j / 2] = ((b[j] & mask) != 0) ? 1 : 0;
            }
            upperBits[i + 8] =
                bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4 | bb[4] << 3
                | bb[5] << 2 | bb[6] << 1 | bb[7];
            lowerBits[i + 8] = ((b[17] & mask) != 0) ? 0x80 : 0x00;
        }
    }
    private void printChineseCharacter(String c) {
        int[] upperBits = new int[16];
        int[] lowerBits = new int[16];

        getBitmap(c, upperBits, lowerBits);
        try {
            // define user-defined character
            out.write(0x1B);
            out.write('&');
            out.write(0x02); // 2-byte in vertical direction
            out.write('A'); // define 'A' to 'B'
            out.write('B');
            for (int i = 0; i < 2; i++) {
                out.write(0x08); // number of bytes in horizontal direction
                for (int j = 0; j < 8; j++) {
                    out.write(upperBits[8 * i + j]);
                    out.write(lowerBits[8 * i + j]);
                }
            }
            out.write(0x1B);
            out.write('%');
            out.write(0x01);
            out.write('A');
            out.write('B');
            out.write(0x1B);
            out.write('?');
            out.write('A');
            out.write(0x1B);
            out.write('?');
            out.write('B');
            out.write(0x1B);
            out.write('%');
            out.write(0x00);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Print by bitmap command. With automatically adding an line-feed at end.
     */
    public void printImmediateWithLineFeed(String str) {
        Arrays.fill(bitmaps, 0);
        Iterator iter = new CharIterator(str);
        while (iter.hasNext()) {
            String c = (String) iter.next();
            if (isChineseChar(c)) {
                getBitmap(c); // into upperBits
                printBitmap(bitmaps);
            } else {
                printAlphanumeric((byte) c.charAt(0));

            }
        }

    }
        public static void main(String[] args) throws IOException {
        }

}
