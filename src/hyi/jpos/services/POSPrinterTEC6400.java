package hyi.jpos.services;
import java.io.*;
import java.util.*;
import javax.comm.*;
import hyi.cream.util.*;
import jpos.*;
import jpos.services.*;
import jpos.events.*;
import hyi.jpos.loader.*;
import jpos.config.*;
import jpos.config.simple.*;

/**
 * 
 * @date 2003/11/19
 * @author ZhaoH
 */

public class POSPrinterTEC6400 extends DeviceService implements ParallelPortEventListener, POSPrinterService14 {
	private Object waitObject = new Object();
	private StringParsingTEC6400 parse;
	private OutputStreamWriter dataOutput;
	private ParallelPort parallelPort;

	static private String logicalName;

	private boolean transactionJournalMode;
	private boolean transactionReceiptMode;
	private boolean transactionSlipMode;
	private StringBuffer transactionJournalBuffer = new StringBuffer();
	private StringBuffer transactionReceiptBuffer = new StringBuffer();
	private StringBuffer transactionSlipBuffer = new StringBuffer();
	private InputStream inputStream;

	/** serial port current feedback status got asyncronously */
	private byte[] readPort = new byte[10];
	private byte statusCode = 0x00;
	private JposEntry POSPrinterentry;
	private boolean insertionFlag = false;

	/** AsyncMode flag definition */
	private boolean asyncMode = false;
	private int characterSet = POSPrinterConst.PTR_CS_ASCII;
	private boolean flagWhenIdle = true;
	private int outputID = 0;
	/**
	 * Define Specified Font File
	 */
	private final String FONT_FILENAME = "gb16x9.bin";
	private RandomAccessFile fontFile;
	private OutputStream out = null;
	/**                                      * */

	private String toplogo = null;
	private String bottomlogo = null;
	public String getTopLogo() throws JposException {

		if (toplogo != null)
			return toplogo;
		else
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Null pointer logo detected!");
	}

	public String getBottomLogo() throws JposException {
		if (bottomlogo != null)
			return bottomlogo;
		else
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Null pointer logo detected!");
	}
	//--------------------------------------------------------------------------
	//the end of logo related manipulation

	//constructor block here
	public POSPrinterTEC6400() throws IOException {
		parse = new StringParsingTEC6400(this);
		fontFile = new RandomAccessFile(FONT_FILENAME, "r");
		/**
		 * Without JavaComm Support
		out = new FileOutputStream("/dev/lp0");
		dataOutput=new OutputStreamWriter(out);
		*/

		CommPortIdentifier portId = null;
		Enumeration portList = CommPortIdentifier.getPortIdentifiers();
		boolean yolicaFlag = false;
		while (portList.hasMoreElements()) {
			portId = (CommPortIdentifier) portList.nextElement();
			if ((portId.getPortType() == CommPortIdentifier.PORT_PARALLEL) && (portId.getName().equals("LPT1"))) {
				yolicaFlag = true;
				break;
			}
		}
		if (!yolicaFlag)
			return;
		//throw new JposException(JposConst.JPOS_E_ILLEGAL, "Parallel port is not available!");

		try {
			parallelPort = (ParallelPort) portId.open(logicalName, 10000);
			out = parallelPort.getOutputStream();
			inputStream = parallelPort.getInputStream();
			dataOutput = new OutputStreamWriter(out, "GBK");
			//dataInput  = new InputStreamReader(inputStream, "GBK");
		} catch (PortInUseException e) {
			throw new IOException("Parallel port in use.");
		}

		try {
			parallelPort.addEventListener(this);
		} catch (TooManyListenersException e) {
			//ignore!
		}
	}

	/**
	* constructor with JposEntry parameter
	*/
	public POSPrinterTEC6400(SimpleEntry entry) throws IOException {
		this();
		POSPrinterentry = entry;
		parse = new StringParsingTEC6400(this);
	}

	/**
	* Service13 Capabilities
	*/
	public int getCapPowerReporting() throws JposException {
		return JposConst.JPOS_PR_STANDARD;
	}
	//--------------------------------------------------------------------------

	/**
	* Service12 Capabilities
	*/
	public int getCapCharacterSet() throws JposException {
		return POSPrinterConst.PTR_CCS_ASCII;
	}

	//Journal and Receipt stations can print at the same time.
	public boolean getCapConcurrentJrnRec() throws JposException {
		return true;
	}

	//If true, then the Journal and Slip stations can print at the same time.
	public boolean getCapConcurrentJrnSlp() throws JposException {
		return false;
	}

	//If true, then the Receipt and Slip stations can print at the same time.
	public boolean getCapConcurrentRecSlp() throws JposException {
		return false;
	}

	//If true, then the printer has a "cover open” sensor.
	public boolean getCapCoverSensor() throws JposException {
		return false;
	}
	//If true, then the journal can print dark plus an alternate color.
	public boolean getCapJrn2Color() throws JposException {
		return false;
	}
	//If true, then the journal can print bold characters.
	public boolean getCapJrnBold() throws JposException {
		return false;
	}

	//If true, then the journal can print double high characters.
	public boolean getCapJrnDhigh() throws JposException {
		return false;
	}

	//If true, then the journal can print double wide characters.
	public boolean getCapJrnDwide() throws JposException {
		return true;
	}

	//If true, then the journal can print double high / double wide characters.
	public boolean getCapJrnDwideDhigh() throws JposException {
		return false;
	}

	//If true, then the journal has an out-of-paper sensor.
	public boolean getCapJrnEmptySensor() throws JposException {
		return true;
	}
	//If true, then the journal can print italic characters.
	public boolean getCapJrnItalic() throws JposException {
		return false;
	}

	//If true, then the journal has a low paper sensor.
	public boolean getCapJrnNearEndSensor() throws JposException {
		return true;
	}

	//If true, then the journal print station is present.
	public boolean getCapJrnPresent() throws JposException {
		return true;
	}

	//If true, then the journal can underline characters.
	public boolean getCapJrnUnderline() throws JposException {
		return false;
	}

	//If true, then the receipt can print dark plus an alternate color.
	public boolean getCapRec2Color() throws JposException {
		return false;
	}

	//If true, then the receipt has bar code printing capability.
	public boolean getCapRecBarCode() throws JposException {
		return false;
	}

	//If true, then the receipt can print bitmaps.
	public boolean getCapRecBitmap() throws JposException {
		return true;
	}

	//If true, then the receipt can print bold characters.
	public boolean getCapRecBold() throws JposException {
		return false;
	}

	//If true, then the receipt can print double high characters.
	public boolean getCapRecDhigh() throws JposException {
		return false;
	}

	//If true, then the receipt can print double wide characters.
	public boolean getCapRecDwide() throws JposException {
		return true;
	}

	//If true, then the receipt can print double wide/high characters.
	public boolean getCapRecDwideDhigh() throws JposException {
		return false;
	}

	//If true, then the receipt has an out-of-paper sensor.
	public boolean getCapRecEmptySensor() throws JposException {
		return true;
	}

	//If true, then the receipt can print italic characters.
	public boolean getCapRecItalic() throws JposException {
		return false;
	}

	//If true, then the receipt can print in rotated 90° left mode.
	public boolean getCapRecLeft90() throws JposException {
		return false;
	}

	//If true, then the receipt has a low paper sensor.
	public boolean getCapRecNearEndSensor() throws JposException {
		return true;
	}

	//If true, then the receipt can perform paper cuts.
	public boolean getCapRecPapercut() throws JposException {
		return true;
	}

	//If true, then the receipt print station is present.
	public boolean getCapRecPresent() throws JposException {
		return true;
	}

	//If true, then the receipt can print in a rotated 90° right mode.
	public boolean getCapRecRight90() throws JposException {
		return false;
	}

	//If true, then the receipt can print in a rotated upside down mode.
	public boolean getCapRecRotate180() throws JposException {
		return false;
	}

	//If true, then the receipt has a stamp capability.
	public boolean getCapRecStamp() throws JposException {
		return true;
	}

	//If true, then the receipt can underline characters.
	public boolean getCapRecUnderline() throws JposException {
		return false;
	}

	//If true, then the slip can print dark plus an alternate color.
	public boolean getCapSlp2Color() throws JposException {
		return false;
	}

	//If true, then the slip has bar code printing capability.
	public boolean getCapSlpBarCode() throws JposException {
		return false;
	}

	//If true, then the slip can print bitmaps.
	public boolean getCapSlpBitmap() throws JposException {
		return false;
	}

	//If true, then the slip can print bold characters.
	public boolean getCapSlpBold() throws JposException {
		return false;
	}

	//If true, then the slip can print double high characters.
	public boolean getCapSlpDhigh() throws JposException {
		return false;
	}

	//If true, then the slip can print double wide characters.
	public boolean getCapSlpDwide() throws JposException {
		return true;
	}

	//If true, then the slip can print double high / double wide characters.
	public boolean getCapSlpDwideDhigh() throws JposException {
		return false;
	}

	//If true, then the slip has a "slip in" sensor.
	public boolean getCapSlpEmptySensor() throws JposException {
		return true;
	}

	/*If true, then the slip is a full slip station. It can print full-length forms. If false,
	 *then the slip is a “validation” type station. This usually limits the number of print
	 *lines, and disables access to the receipt and/or journal stations while the validation
	 *slip is being used.*/
	public boolean getCapSlpFullslip() throws JposException {
		return false;
	}

	//If true, then the slip can print italic characters.
	public boolean getCapSlpItalic() throws JposException {
		return false;
	}

	//If true, then the slip can print in a rotated 90° left mode.
	public boolean getCapSlpLeft90() throws JposException {
		return false;
	}

	//If true, then the slip has a “slip near end” sensor.
	public boolean getCapSlpNearEndSensor() throws JposException {
		return false;
	}

	//If true, then the slip print station is present.
	public boolean getCapSlpPresent() throws JposException {
		return true;
	}

	//If true, then the slip can print in a rotated 90° right mode.
	public boolean getCapSlpRight90() throws JposException {
		return false;
	}

	//If true, then the slip can print in a rotated upside down mode.
	public boolean getCapSlpRotate180() throws JposException {
		return false;
	}

	//If true, then the slip can underline characters.
	public boolean getCapSlpUnderline() throws JposException {
		return false;
	}

	//If true, then printer transactions are supported by each station.
	public boolean getCapTransaction() throws JposException {
		return true;
	}

	//the end of Service12 capabilities

	/**
	* Service13 Properties
	*/
	public int getPowerNotify() throws JposException {
		return JposConst.JPOS_PN_DISABLED;
	}

	public void setPowerNotify(int powerNotify) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "No Service!");
	}

	public int getPowerState() throws JposException {
		return JposConst.JPOS_PS_UNKNOWN;
	}

	/**
	* Service12 Properties
	*/

	//Sync/Async mode
	public boolean getAsyncMode() throws JposException {
		return asyncMode;
	}

	public void setAsyncMode(boolean asyncMode) throws JposException {
		if (getAsyncMode() && !asyncMode) {
			this.asyncMode = false;
			System.out.println("POSPrinter runs in SyncMode!");
			return;
		} else if (!getAsyncMode() && asyncMode) {
			this.asyncMode = true;
			System.out.println("POSPrinter runs in AsyncMode!");
			return;
		} else
			return;
		//throw new  JposException(JposConst.JPOS_E_ILLEGAL, "Expected mode can't be obtained: Change to the same mode!");
	}

	public int getCharacterSet() throws JposException {
		return characterSet;
	}

	public void setCharacterSet(int characterSet) throws JposException {
		this.characterSet = characterSet;
	}

	//Holds the character set numbers. It consists of ASCII numeric set numbers
	//separated by commas.
	public String getCharacterSetList() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE);
	}

	//If true, then the printer’s cover is open.
	public boolean getCoverOpen() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE);
	}

	//Holds the severity of the error condition. It has one of the following values:
	//PTR_EL_NONE No error condition is present.
	//PTR_EL_RECOVERABLE A recoverable error has occurred.(Example: Out of paper.)
	//PTR_EL_FATAL A non-recoverable error has occurred.(Example: Internal printer failure.)
	//--------------------------------------------------------------------------
	//This property is set just before delivering an ErrorEvent. When the error is
	//cleared, then the property is changed to PTR_EL_NONE.
	//--------------------------------------------------------------------------

	private int errorLevel = POSPrinterConst.PTR_EL_NONE;
	public int getErrorLevel() throws JposException {
		return errorLevel;
	}

	/*Holds the station or stations that were printing when an error was detected.
	This property will be set to one of the following values:
	PTR_S_JOURNAL PTR_S_RECEIPT
	PTR_S_SLIP PTR_S_JOURNAL_RECEIPT
	PTR_S_JOURNAL_SLIP PTR_S_RECEIPT_SLIP
	PTR_TWO_RECEIPT_JOURNAL PTR_TWO_SLIP_JOURNAL
	PTR_TWO_SLIP_RECEIPT
	This property is only valid if the ErrorLevel is not equal to PTR_EL_NONE. It is
	set just before delivering an ErrorEvent.
	*/

	private int errorStation = 0;
	public int getErrorStation() throws JposException {
		return errorStation;
	}

	/*Holds a vendor-supplied description of the current error.
	This property is set just before delivering an ErrorEvent. If no description is
	available, the property is set to an empty string. When the error is cleared, then the
	property is changed to an empty string.*/
	private String errorString = "";
	public String getErrorString() throws JposException {
		return errorString;
	}

	/*If true, a StatusUpdateEvent will be enqueued when the device is in the idle state.
	This property is automatically reset to false when the status event is delivered.
	The main use of idle status event that is controlled by this property is to give the
	application control when all outstanding asynchronous outputs have been
	processed. The event will be enqueued if the outputs were completed successfully
	or if they were cleared by the clearOutput method or by an ErrorEvent handler.
	If the State is already set to JPOS_S_IDLE when this property is set to true, then
	a StatusUpdateEvent is enqueued immediately. The application can therefore
	depend upon the event, with no race condition between the starting of its last
	asynchronous output and the setting of this flag.
	This property is initialized to false by the open method.
	*/
	public boolean getFlagWhenIdle() throws JposException {
		return flagWhenIdle;
	}

	public void setFlagWhenIdle(boolean flagWhenIdle) throws JposException {
		this.flagWhenIdle = flagWhenIdle;
	}

	/*Holds the fonts and/or typefaces that are supported by the printer. The string
	 consists of font or typeface names separated by commas.
	 This property is initialized by the open method.
	*/
	public String getFontTypefaceList() throws JposException {
		return "";
	}

	public boolean getJrnEmpty() throws JposException {
		if (!getCapJrnEmptySensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(), ie);
		}
		statusCode = readPort[0];
		if ((statusCode & 0x01) == 0x00)
			return false;
		else
			return true;
	}

	public boolean getJrnLetterQuality() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setJrnLetterQuality(boolean jrnLetterQuality) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	//Holds the number of characters that may be printed on a journal line.
	private int jrnLineChars = 24;
	public int getJrnLineChars() throws JposException {
		return jrnLineChars;
	}
	//This property is initialized to the printer’s default line character width
	//when the device is first enabled following the open method.
	public void setJrnLineChars(int jrnLineChars) throws JposException {
		if (jrnLineChars < 0 || jrnLineChars > 24)
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid agrument:" + jrnLineChars);
		this.jrnLineChars = jrnLineChars;
	}

	public String getJrnLineCharsList() throws JposException {
		return "12,24";
	}

	public int getJrnLineHeight() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setJrnLineHeight(int jrnLineHeight) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getJrnLineSpacing() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setJrnLineSpacing(int jrnLineSpacing) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getJrnLineWidth() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getJrnNearEnd() throws JposException {
		if (!getCapJrnNearEndSensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(), ie);
		}
		statusCode = readPort[0];
		if ((statusCode & 0x01) == 0x00)
			return false;
		else
			return true;
	}

	public int getMapMode() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setMapMode(int mapMode) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public String getRecBarCodeRotationList() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getRecEmpty() throws JposException {
		if (!getCapRecEmptySensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(), ie);
		}
		statusCode = readPort[0]; //if statusCode equals zero, the paper is present
		if ((statusCode & 0x02) == 0x00)
			return false;
		else
			return true;
	}

	public boolean getRecLetterQuality() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRecLetterQuality(boolean recLetterQuality) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	private int recLineChars = 24;
	public int getRecLineChars() throws JposException {
		return recLineChars;
	}

	public void setRecLineChars(int recLineChars) throws JposException {
		if (recLineChars < 0 || recLineChars > 24)
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid agrument:" + jrnLineChars);
		this.recLineChars = recLineChars;
	}

	public String getRecLineCharsList() throws JposException {
		return "12,24";
	}

	public int getRecLineHeight() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRecLineHeight(int recLineHeight) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getRecLineSpacing() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRecLineSpacing(int recLineSpacing) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	private int recLinesToPaperCut = 0;
	public int getRecLinesToPaperCut() throws JposException {
		return recLinesToPaperCut;
	}

	public int getRecLineWidth() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getRecNearEnd() throws JposException {
		if (!getCapRecNearEndSensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(), ie);
		}
		statusCode = readPort[0]; //if statusCode equals zero, the paper is present
		if ((statusCode & 0x02) == 0x00)
			return false;
		else
			return true;
	}

	public int getRecSidewaysMaxChars() throws JposException {
		return 0;
	}

	public int getRecSidewaysMaxLines() throws JposException {
		return 0;
	}

	public int getRotateSpecial() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setRotateSpecial(int rotateSpecial) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public String getSlpBarCodeRotationList() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public boolean getSlpEmpty() throws JposException {
		if (!getCapSlpEmptySensor())
			return false;
		try {
			dataOutput.write("\u001Dr\u0001");
			dataOutput.flush();
			inputStream.read(readPort);
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, this.toString(), ie);
		}
		statusCode = readPort[0]; //if statusCode equals zero, the paper is present
		if ((statusCode & 0x20) == 0x00)
			return false;
		else
			return true;

	}

	public boolean getSlpLetterQuality() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setSlpLetterQuality(boolean recLetterQuality) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	//--------------------------------------------------------------------------
	private int slpLineChars = 55;
	public int getSlpLineChars() throws JposException {
		return slpLineChars;
	}

	public void setSlpLineChars(int slpLineChars) throws JposException {
		if (slpLineChars < 0 || slpLineChars > 55)
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid agrument:" + slpLineChars);
		this.slpLineChars = slpLineChars;
	}
	//--------------------------------------------------------------------------

	public String getSlpLineCharsList() throws JposException {
		return "27,55";
	}

	public int getSlpLineHeight() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setSlpLineHeight(int slpLineHeight) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpLinesNearEndToEnd() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpLineSpacing() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public void setSlpLineSpacing(int slpLineSpacing) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpLineWidth() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpMaxLines() throws JposException {
		return 1;
	}

	public boolean getSlpNearEnd() throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, "Not Supported!");
	}

	public int getSlpSidewaysMaxChars() throws JposException {
		return 0;
	}

	public int getSlpSidewaysMaxLines() throws JposException {
		return 0;
	}
	//--------------------------------------------------------------------------
	public int getOutputID() throws JposException {
		return outputID;
	}

	//new...begin Service12 Methods
	//first sector the synchronized method
	synchronized public void beginInsertion(int timeout) throws JposException {
		if (timeout < -1)
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Illegal parameter:" + timeout);
		if (!getCapSlpPresent())
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not supported");
	}

	synchronized public void endInsertion() throws JposException {
		insertionFlag = true;
	}

	synchronized public void beginRemoval(int timeout) throws JposException {
		if (timeout < -1)
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Illegal parameter:" + timeout);
		if (!getCapSlpPresent())
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not supported");
	}

	synchronized public void endRemoval() throws JposException {
		insertionFlag = true;
	}

	public void clearOutput() throws JposException {
		outputID = 0;
	}

    /**
     * Cut paper.
     * When percentage is 99, it immediately cuts;
     * when percentage is 100, it will cut at black mark.
     */
	public void cutPaper(int percentage) throws JposException {
		//check out the situation to make sure that the printer can be operated
		if (percentage == 0)
			return;
		if (percentage < 0 || percentage > 100)
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Illegal argument:" + percentage + " detected!");

		try {
			if (percentage == 99) {
				dataOutput.write(0x1B);
				dataOutput.write('i');
				dataOutput.flush();
				return;
			}

            out.write(new byte[] { 0x1B, (byte)'N' });
            out.write(0x99);                // Journal and receipt both feed, sense and stop at Top Mark
            out.write(0x20);                // Max 32 lines
            for (int i = 0; i < 6; i++) {
                out.write('\n');
            }
            dataOutput.write(0x1B);
            dataOutput.write('i');
            dataOutput.flush();

			//out.write(new byte[] { 0x1B, (byte) 'N' });
			//for (int i = 0; i < 3; i++) {
			//	out.write('\n');
			//}
			//dataOutput.write(0x1B);
			//dataOutput.write('i');
			//dataOutput.flush();

		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_FAILURE, "IO error in cutting paper!", ie);
		}
	} //end of cutPaper

	public void printBarCode(
		int station,
		String data,
		int symbology,
		int height,
		int width,
		int alignment,
		int textPosition)
		throws JposException {
		throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not support!");
	} //end of printBarCode

	public void printBitmap(int station, String fileName, int width, int alignment) throws JposException {
		throw new JposException(JposConst.JPOS_E_ILLEGAL, "Not support!");
	} //end of printBitmap

	//synchronized printing method
	public void printImmediate(int station, String data) throws JposException {
		if (!getClaimed())
			throw new JposException(JposConst.JPOS_E_NOTCLAIMED, "Please claim the printer before manipulation!");
		if (!getDeviceEnabled())
			throw new JposException(JposConst.JPOS_E_DISABLED, "Printer is not enabled!");
		String realCommand = parse.getValidCommand(station, data);
		if (realCommand == null)
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid station argument:" + station);
		else if (data.equals(""))
			return;
		try {
			printImmediateWithLineFeed(realCommand);
			dataOutput.write(realCommand);
			dataOutput.flush();
			synchronized (waitObject) {
				try {
					waitObject.wait();
				} catch (InterruptedException ie) {
					throw new JposException(JposConst.JPOS_E_ILLEGAL);
				}
			}
		} catch (IOException ie) {
			throw new JposException(JposConst.JPOS_E_ILLEGAL);
		}
	} //end of printImmediate()

	//Main
	public void printNormal(int station, String data) throws JposException {
		//Check out if the station argument is legal;
		if (data.equals(""))
			return;
		String realCommand = parse.getValidCommand(station, data);
		if (realCommand == null)
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid arguments!");

		if (station == POSPrinterConst.PTR_S_JOURNAL && transactionJournalMode == true) {
			transactionJournalBuffer.append(realCommand);
			return;
		}
		if (station == POSPrinterConst.PTR_S_RECEIPT && transactionReceiptMode == true) {
			transactionReceiptBuffer.append(realCommand);
			return;
		}
		if (station == POSPrinterConst.PTR_S_SLIP && transactionSlipMode == true) {
			transactionSlipBuffer.append(realCommand);
			return;
		}
		try {
			printImmediateWithLineFeed(realCommand);
		} catch (Exception e) {
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "I/O error occured in printing", e);
		}

	}

	public void printTwoNormal(int stations, String data1, String data2) throws JposException {
		//System.out.println("Invoking PrintTwoNormal Method...");
		if (data1.equals("") && data2.equals(""))
			return;

		String realCommand = parse.getValidCommand(stations, data1, data2);
		//System.out.println(this + "2" );
		if (realCommand == null)
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid arguments!");
		try {
			//// realCommand = realCommand + "\u001B!\u0000";
			printImmediateWithLineFeed(realCommand);
			////                     dataOutput.write(realCommand);
			dataOutput.flush();
		} catch (IOException e) {
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "I/O error occured in printing", e);
		}
	}

	public void rotatePrint(int station, int rotation) throws JposException {
		throw new JposException(JposConst.JPOS_E_FAILURE, "Not supported in DRJST51!");
	} //end of rotatePrint

	public void setBitmap(int bitmapNumber, int station, String fileName, int width, int alignment)
		throws JposException {
		throw new JposException(JposConst.JPOS_E_FAILURE, "Not supported in DRJST51!");
	} //end of setBitmap

	public void setLogo(int location, String data) throws JposException {
		//The logo to be set. Location may be PTR_L_TOP or PTR_L_BOTTOM.
		if (location == POSPrinterConst.PTR_L_TOP)
			toplogo = data;
		else if (location == POSPrinterConst.PTR_L_BOTTOM)
			bottomlogo = data;
		else
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "An invalid location was specified:" + location + ".");
	} //end of setLogo

	//transactionPrint(int station, int control) throws JposException
	public void transactionPrint(int station, int control) throws JposException {
		if (!getCapTransaction())
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Transaction mode is not supported!");
		switch (station) {
			case POSPrinterConst.PTR_S_JOURNAL :
				if (!getCapJrnPresent())
					throw new JposException(JposConst.JPOS_E_ILLEGAL, "The station:" + station + " is not present");
				if (control == POSPrinterConst.PTR_TP_TRANSACTION)
					transactionJournalMode = true;
				else if (control == POSPrinterConst.PTR_TP_NORMAL)
					transactionJournalMode = false;
				else
					throw new JposException(JposConst.JPOS_E_ILLEGAL, "Error occured--illegal argument:" + control);
				break;
			case POSPrinterConst.PTR_S_RECEIPT :
				if (!getCapRecPresent())
					throw new JposException(JposConst.JPOS_E_ILLEGAL, "The station:" + station + " is not present");
				if (control == POSPrinterConst.PTR_TP_TRANSACTION)
					transactionReceiptMode = true;
				else if (control == POSPrinterConst.PTR_TP_NORMAL)
					transactionReceiptMode = false;
				else
					throw new JposException(JposConst.JPOS_E_ILLEGAL, "This");
				break;
			case POSPrinterConst.PTR_S_SLIP :
				if (!getCapSlpPresent())
					throw new JposException(JposConst.JPOS_E_ILLEGAL, "The station:" + station + " is not present");
				if (control == POSPrinterConst.PTR_TP_TRANSACTION)
					transactionSlipMode = true;
				else if (control == POSPrinterConst.PTR_TP_NORMAL)
					transactionSlipMode = false;
				else
					throw new JposException(JposConst.JPOS_E_ILLEGAL, "Error occured--illegal argument:" + control);
				break;
			default :
				throw new JposException(
					JposConst.JPOS_E_ILLEGAL,
					"This station is not supported by hyi.jpos.POSPrinterDRJST51!");
		}
	} // end of transactionPrint()

	public void validateData(int station, String data) throws JposException {
		if (!(station == POSPrinterConst.PTR_S_JOURNAL
			|| station == POSPrinterConst.PTR_S_RECEIPT
			|| station == POSPrinterConst.PTR_S_SLIP))
			throw new JposException(JposConst.JPOS_E_ILLEGAL, "Invalid station argument:" + station);
		if (data.equals(""))
			return;
		String item = null;
		String[] commonillegal =
			{ "\u001B|rA", "\u001B|cA", "\u001B|3C", "\u001B|4C", "\u001B|rC", "\u001B|rvC", "\u001B|iC", "\u001B|bC" };
		ArrayList ESCSequence = parse.getESCSequence(data);
		ArrayList JposESCSequence = parse.getJposESCSequence(data);
		if (ESCSequence.isEmpty())
			return;
		if (JposESCSequence.size() != ESCSequence.size())
			throw new JposException(
				JposConst.JPOS_E_FAILURE,
				"Escape sequence is not supported by the printer:" + data);
		for (int k = 0; k < JposESCSequence.size(); k++) {
			item = (String) JposESCSequence.get(k);
			if (parse.contains(commonillegal, item))
				throw new JposException(JposConst.JPOS_E_FAILURE, "Not supported by the printer:" + data);
			else if (
				item.substring(item.length() - 1).equals("B")
					|| item.substring(item.length() - 2).equals("uF")
					|| item.substring(item.length() - 2).equals("rF")
					|| item.substring(item.length() - 2).equals("fT")
					|| item.substring(item.length() - 2).equals("uC")
					|| item.substring(item.length() - 2).equals("sC")
					|| item.substring(item.length() - 2).equals("hC")
					|| item.substring(item.length() - 2).equals("vC"))
				throw new JposException(JposConst.JPOS_E_FAILURE, "Not supported by the printer:" + data);
		}
		switch (station) {
			case POSPrinterConst.PTR_S_JOURNAL :
				for (int k = 0; k < JposESCSequence.size(); k++) {
					item = (String) JposESCSequence.get(k);
					if (item.substring(item.length() - 1).equals("P")
						|| item.substring(item.length() - 2).equals("sL"))
						throw new JposException(
							JposConst.JPOS_E_FAILURE,
							"Appointed manipulation is not supported at the specified station:"
								+ "station="
								+ station
								+ " string="
								+ data);
				}
				break;
			case POSPrinterConst.PTR_S_RECEIPT :
				for (int k = 0; k < JposESCSequence.size(); k++) {
					item = (String) JposESCSequence.get(k);
					if (item.charAt(item.length() - 1) == 'P' && item.length() != 3)
						try {
							if (Integer.decode(item.substring(2, item.length() - 1)).intValue() < 0
								|| Integer.decode(item.substring(2, item.length() - 1)).intValue() > 100)
								throw new JposException(
									JposConst.JPOS_E_ILLEGAL,
									"Assigned value is illegal:" + item.substring(2, item.length() - 1));
						} catch (Exception e) {
							System.out.println(e);
						}
				}
				break;
			case POSPrinterConst.PTR_S_SLIP :
				for (int k = 0; k < JposESCSequence.size(); k++) {
					item = (String) JposESCSequence.get(k);
					if (item.substring(item.length() - 1).equals("P")
						|| item.substring(item.length() - 2).equals("sL")
						|| item.substring(item.length() - 2).equals("lF"))
						throw new JposException(
							JposConst.JPOS_E_FAILURE,
							"Appointed manipulation is not supported at the specified station:"
								+ "station="
								+ station
								+ " string="
								+ data);
				}
				break;
		}
	}
	//The end of Service12 Methods

	//--------------------------------------------------------------------------

	//new...begin BaseService Methods supported by all device services.
	public void claim(int timeout) throws JposException {
		super.claim(timeout);
	}

	public void release() throws JposException {
		if (!getClaimed())
			return;
		try {
			if (out != null)
				out.close();
			out = null;
			if (inputStream != null)
				inputStream.close();
			inputStream = null;
			if (dataOutput != null)
				dataOutput.close();
			dataOutput = null;
			super.release();
		} catch (IOException e) {
		}
	}

	//--------------------------------------------------------------------------
	//Close the device and unlink it from the device control!
	//--------------------------------------------------------------------------

	public void close() throws JposException {
		super.close();
		if (getClaimed())
			release();
		if (getAsyncMode())
			clearOutput();
		asyncMode = false;
		this.logicalName = null;
	}

	// The end of close()
	public void checkHealth(int level) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, this.toString());
	}

	public void directIO(int command, int[] data, Object object) throws JposException {
		throw new JposException(JposConst.JPOS_E_NOSERVICE, this.toString());
	}

	public void open(String logicalName, EventCallbacks cb) throws JposException {
		super.open(logicalName, cb);
		asyncMode = false;
		outputID = 0;
		this.logicalName = logicalName;
	}

	//new...end BaseService Methods supported by all device services.

	//--------------------------------------------------------------------------
	/**
	* serialEventListenner implementation, it is in another thread,
	* executed automatically */
	public void parallelEvent(ParallelPortEvent ev) {
	}

	public void deleteInstance() throws JposException {
	}
	private void printAlphanumeric(byte b) {
		try {
			out.write(b);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	private void printBitmap(int[] upperBits, int[] lowerBits) {
		//System.out.print("printBitmap(): ");
		try {
			// print bitmap
			out.write(new byte[] { 0x1B, (byte) '*', 0x03, // 0: 8-dot single density, 1: 8-dot double density
				 (byte) upperBits.length, // number of dots in horizontal direction
				0x00 // number of another 256 dots in horizontal direction
			});
			for (int i = 0; i < upperBits.length; i++) {
				out.write(upperBits[i]);
				out.write(lowerBits[i]);
				//System.out.print(bytes[i] + ",");
			}
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
		//System.out.println("");
	}

	/**
	 * Inner class for iterating Chinese (in GB code) or alphanumeric characters.
	 */
	private class CharIterator implements Iterator {

		private byte[] str;
		private int index;
		private byte[] chineseChar = new byte[2];
		private byte[] alphanumChar = new byte[1];

		CharIterator(String str0) {
			if (str0 != null) {
				try {
					str = str0.getBytes(CreamToolkit.getEncoding());
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}

		public boolean hasNext() {
			return (str == null) ? false : (index < str.length);
		}

		public Object next() {
			if (!hasNext())
				return null;

			int b = str[index] & 0xff;
			//System.out.print("b=" + Integer.toHexString(b) + " ");
			if (index + 1 < str.length) {
				int b2 = str[index + 1] & 0xff;
				//System.out.println("b2=" +Integer.toHexString(b2) + " ");
				if ((b >= 0xA1 && b <= 0xA9 || b >= 0xB0 && b <= 0xF7)
					&& // first-byte is a GB code
				 (b2 >= 0xA1 && b2 <= 0xFE) // second-byte is a GB code
				) {
					chineseChar[0] = (byte) b;
					chineseChar[1] = (byte) b2;
					index += 2;
					try {
						return new String(chineseChar, "ISO-8859-1");
					} catch (UnsupportedEncodingException e) {
						return "";
					}
				}
			}
			alphanumChar[0] = (byte) b;
			index++;
			return new String(alphanumChar);
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	private boolean isChineseChar(String str) {
		return (str.length() == 2); // 2-byte if it is a Chinese char
	}

	/**
	 * Get the Chinese font bitmap into upperBits and lowerBits
	 */
	private void getBitmap(String c, int[] upperBits, int[] lowerBits) {
		byte[] b = new byte[18]; // bitmap bytes read from font file
		int b1 = c.charAt(0);
		int b2 = c.charAt(1);
		int offset;

		if (b1 <= 0xA9)
			offset = ((b1 - 0xA1) * (0xFE - 0xA0) + (b2 - 0xA1)) * 18;
		else
			offset = (9 * (0xFE - 0xA0) + (b1 - 0xB0) * (0xFE - 0xA0) + (b2 - 0xA1)) * 18;
		try {
			fontFile.seek(offset);
			fontFile.read(b);
		} catch (IOException e) {
			e.printStackTrace(System.err);
			return;
		}
		int[] bb = new int[8];
		int i, j, mask;
		for (i = 0; i < 8; i++) {
			mask = 0x80 >>> i;
			for (j = 0; j <= 14; j += 2) {
				bb[j / 2] = ((b[j] & mask) != 0) ? 1 : 0;
			}
			upperBits[i] =
				bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4 | bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
			lowerBits[i] = ((b[16] & mask) != 0) ? 0x80 : 0x00;
		}
		for (i = 0; i < 8; i++) {
			mask = 0x80 >>> i;

			for (j = 1; j <= 15; j += 2) {

				bb[j / 2] = ((b[j] & mask) != 0) ? 1 : 0;
			}
			upperBits[i + 8] =
				bb[0] << 7 | bb[1] << 6 | bb[2] << 5 | bb[3] << 4 | bb[4] << 3 | bb[5] << 2 | bb[6] << 1 | bb[7];
			lowerBits[i + 8] = ((b[17] & mask) != 0) ? 0x80 : 0x00;
		}
	}

	/**
	 * Print by bitmap command. With automatically adding an line-feed at end.
	 */
	public void printImmediateWithLineFeed(String str) {

		int[] upperBits = new int[18];
		int[] lowerBits = new int[18];

		Iterator iter = new CharIterator(str);
		while (iter.hasNext()) {
			String c = (String) iter.next();

			if (isChineseChar(c)) {
				getBitmap(c, upperBits, lowerBits); // into upperBits and lowerBits
				printBitmap(upperBits, lowerBits);
			} else {

				printAlphanumeric((byte) c.charAt(0));
			}
		}
		//setLineSpace(8);
		////        printAlphanumeric((byte)'\n');
		//printLowerBits();
		//setLineSpace(3);
		//printAlphanumeric((byte)'\n');
		//setDefaultLineSpace();
	}

	public static void main(String[] args) throws IOException {
		POSPrinterDRJST51 pos = new POSPrinterDRJST51();
		try {
			System.out.println("Ready To Print...Below!");
			pos.open("printer", null);
			pos.claim(0);
			pos.setDeviceEnabled(true);
			System.out.println(pos.getPowerState() + "|" + JposConst.JPOS_PS_OFFLINE + "|" + JposConst.JPOS_PS_ONLINE);
			pos.getSlpEmpty();
			for (int i = 0; i < 5; i++)
				pos.printNormal(POSPrinterConst.PTR_S_SLIP, "lorere一二\u001B|2C--ererere");
			pos.printNormal(POSPrinterConst.PTR_S_JOURNAL_RECEIPT, "lorere一二\u001B|2C--ererere");
			pos.printNormal(POSPrinterConst.PTR_S_JOURNAL_RECEIPT, "流星花园--主题曲\n");
			System.exit(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
