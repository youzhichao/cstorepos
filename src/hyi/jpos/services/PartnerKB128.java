
// Copyright (c) 2000 HYI
package hyi.jpos.services;

import jpos.*;
import jpos.services.*;
import jpos.events.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.*;
import jpos.config.*;
import jpos.config.simple.*;
import java.io.*;
import java.text.*;
import java.util.*;

import hyi.cream.*;
import hyi.cream.dac.*;
import hyi.cream.util.*;
import hyi.cream.uibeans.*;

/**
 * A Class class.
 * <P>
 * @author dai
 */
public class PartnerKB128 implements POSKeyboardService14, KeyListener, ContainerListener {

    static private POSKeyboard claimedControl;
    static private Object mutex = new Object();

    protected EventCallbacks eventCallbacks      = null;
    protected JposEntry entry                    = null;
    protected Container comp                     = null;
    protected Container comp2                    = null;
    protected Container comp3                    = null;

    private boolean capDescriptors               = false;
    private boolean claimed                      = false;
    private int deviceDescriptors                = 0;
    private boolean deviceEnabled                = false;

    protected int posKeyData                     = 0;
    protected String checkHealthText             = "";
    protected String deviceServiceDescription    = "JavaPOS Service from HYI Corporation";
    protected int deviceServiceVersion           = 1005000;
    protected boolean freezeEvents               = false;
    protected String physicalDeviceDescription   = "";
    protected String physicalDeviceName          = "";
    protected int state                          = JposConst.JPOS_S_CLOSED;
    protected int powerNotify                    = JposConst.JPOS_PN_DISABLED;
    protected int powerState                     = JposConst.JPOS_PS_UNKNOWN;
    private int scannerPrefixCharValue;

    private int prompt;


    /**
     * Constructor
     */
    public PartnerKB128(SimpleEntry entry) {
        this.entry = entry;
    }

    // Capabilities
    public boolean getCapKeyUp() throws JposException {
        return true;
    }

    public int getCapPowerReporting() throws JposException {
        return 0;
    }

    // Properties
    public boolean getAutoDisable() throws JposException {
        return true;
    }

    public void setAutoDisable(boolean autoDisable) throws JposException {
    }

    public int getDataCount() throws JposException {
        return 0;
    }

    public boolean getDataEventEnabled() throws JposException {
        return true;
    }

    public void setDataEventEnabled(boolean dataEventEnabled)
                       throws JposException {
    }

    public int getEventTypes() throws JposException {
        return 0;
    }

    public void setEventTypes(int eventTypes) throws JposException {
    }

    public int getPOSKeyData() throws JposException {
        return posKeyData;
    }

    public int getPOSKeyEventType() throws JposException {
        return 0;
    }

    public int getPowerNotify() throws JposException {
        return 0;
    }

    public void setPowerNotify(int powerNotify) throws JposException {
    }

    public int getPowerState() throws JposException {
        return 0;
    }

    public String getCheckHealthText() throws JposException {
        return "";
    }

    public boolean getClaimed() throws JposException {
        return claimed;
    }

    public boolean getDeviceEnabled() throws JposException {
        return deviceEnabled;
    }

    private boolean isActive = false;
    private boolean fkey = false;
    private String fkeyString = "";
    private boolean msr = false;
    private boolean scanner = false;
    private HashMap fkeys = new HashMap();
    /*public void setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        if (!claimed) {
            System.out.println("must claim it first");
            return;
        }
        this.deviceEnabled = deviceEnabled;

        Thread ttt = new Thread(new Th());
        ttt.start();

        fkeys.put("279149126", "36000");
        fkeys.put("279151126", "127000");
        fkeys.put("279152126", "35000");
        fkeys.put("279153126", "33000");
        fkeys.put("279154126", "34000");

        Thread t = new Thread(new Runnable() {

            PopupMenuPane pop = POSTerminalApplication.getInstance().getPopupMenuPane();
            ItemList itemList = POSTerminalApplication.getInstance().getItemList();
            PayingPane payingPane = POSTerminalApplication.getInstance().getPayingPane();

            public void run() {
                char ch = ' ';
                while (PartnerKB128.this.isActive) {
                    System.out.println("### ready to read test.c ###");
                    ch = Passc.pr();
                    System.out.println("### read " + ch + " ###");
                    posKeyData = (int)ch;

                    //  check msr
                    if (ch == ';' || ch == '%') {
                        msr = true;
                        return;
                    }
                    if (msr && posKeyData == 10) {
                        msr = false;
                        return;
                    }

                    //  check scanner
                    if (ch == ' ') {
                        scanner = true;
                        return;
                    }
                    if (scanner && posKeyData == 10) {
                        scanner = false;
                        return;
                    }

                    //  check special key
                    if (posKeyData == 27) {
                        fkey = true;
                    }
                    if (fkey) {
                        fkeyString= fkeyString + posKeyData;
                    }
                    if (posKeyData == 126) {
                        fkey = false;
                        posKeyData = Integer.parseInt((String)PartnerKB128.this.fkeys.get(String.valueOf(posKeyData)));
                    }

                    if (!msr && !scanner && !fkey) {

                        if (pop.isVisible()) {
                            if (pop.keyDataListener(posKeyData)) {
                                return;
                            }
                        } else if (itemList.isVisible()) {
                            if (itemList.keyDataListener(posKeyData)) {
                                return;
                            }
                        } else if (payingPane.isVisible()) {
                            if (payingPane.keyDataListener(posKeyData)) {
                                return;
                            }
                        }

                        System.out.println("### fire event from keyboard ###");
                        eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
                    }
                }
            }
        });
        if (deviceEnabled) {
            isActive = true;
            System.out.println("### ready to read ###");
            t.start();
        } else {
            isActive = false;
        }
    }*/

    public void setDeviceEnabled(boolean deviceEnabled)
                       throws JposException {
        if (!claimed) {
            System.out.println("must claim it first");
            return;
        }
        this.deviceEnabled = deviceEnabled;
        if (deviceEnabled) {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                adkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                adkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                adkl(comp3);
            }
        } else {
            if (entry.hasPropertyWithName("COMP_CLASS_PROP_NAME")) {
                String className = (String)entry.getPropertyValue("COMP_CLASS_PROP_NAME");
                comp = createComp(className);
                rmkl(comp);
            }
            if (entry.hasPropertyWithName("COMP2_CLASS_PROP_NAME")) {
                String className2 = (String)entry.getPropertyValue("COMP2_CLASS_PROP_NAME");
                comp2 = createComp(className2);
                rmkl(comp2);
            }
            if (entry.hasPropertyWithName("COMP3_CLASS_PROP_NAME")) {
                String className3 = (String)entry.getPropertyValue("COMP3_CLASS_PROP_NAME");
                comp3 = createComp(className3);
                rmkl(comp3);
            }
        }
    }

    public String getDeviceServiceDescription() throws JposException {
        return "";
    }

    public int getDeviceServiceVersion() throws JposException {
        return 0;
    }

    public boolean getFreezeEvents() throws JposException {
        return true;
    }

    public void setFreezeEvents(boolean freezeEvents) throws JposException {
    }

    public String getPhysicalDeviceDescription() throws JposException {
        return "";
    }

    public String getPhysicalDeviceName() throws JposException {
        return "";
    }

    public int getState() throws JposException {
        return 0;
    }

    // Methods
    public void clearInput() throws JposException {
    }

    /**
     * Indicate the ServiceInstance that its connection has been dropped
     * @since 0.1 (Philly 99 meeting)
     * @throws jpos.JposException if any error occurs
     */
    public void deleteInstance() throws JposException {
    }

    //--------------------------------------------------------------------------
    // Common overridden methods
    //

    public void open(String logicalName, EventCallbacks eventCallbacks)
                    throws JposException {
        this.eventCallbacks = eventCallbacks;
        claimed = false;
        deviceEnabled = false;

        scannerPrefixCharValue = Integer.parseInt(
            CreamProperties.getInstance().getProperty("ScannerPrefixCharValue", "32"));
    }

    public void close() throws JposException {
        claimed = false;
        deviceEnabled = false;
        claimedControl = null;
    }

    public void claim(int timeout) throws JposException {
        if (claimed) {
            System.out.println("device has been claimed");
            return;
        }

        synchronized (mutex) {
                //System.out.println("claim 0");
            if (claimedControl == null) {
                //System.out.println("claim 0.1");
                Object obj = eventCallbacks.getEventSource();
                //System.out.println(obj);
                //System.out.println((PINPad)obj);
                claimedControl = (POSKeyboard)obj;
                //System.out.println("claim 0.2");
                claimed = true;
                return;
            }
            if (timeout < -1) {
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            } else if (timeout == JposConst.JPOS_FOREVER) {
                //System.out.println("claim 1");
                try {
                    while (claimedControl != null) {
                        mutex.wait();
                    }
                //System.out.println("claim 2");
                    claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                //System.out.println("claim 3");
                    claimed = true;
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }
            } else {
                try {
                    long start = System.currentTimeMillis();
                    long waitTime = timeout;

                    while (waitTime > 0) {
                        mutex.wait(waitTime);
                        if (claimedControl == null) {
                            claimedControl = (POSKeyboard)eventCallbacks.getEventSource();
                            claimed = true;
                            return;
                        }
                        waitTime = timeout - (System.currentTimeMillis() - start);
                    }
                    //System.out.println("device is busy");
                    return;
                } catch (InterruptedException ex1) {
                    CreamToolkit.logMessage(ex1.toString());
                    CreamToolkit.logMessage("Interrupted exception at " + this);
                }

                throw new JposException(JposConst.JPOS_E_TIMEOUT);
            }
        }
        claimed = true;
    }

    public void checkHealth(int level) throws JposException {
    }

    public void directIO(int command, int[] data, Object object)
                       throws JposException {
    }

    public void release() throws JposException {
        synchronized (mutex) {
            if (!claimed) {
                System.out.println("device has been release");
            }
            claimed = false;
            deviceEnabled = false;
            claimedControl = null;
            mutex.notifyAll();
        }
    }

    //event
    public void keyReleased(KeyEvent e) {
    }

    private boolean checked = true;
    private boolean shift = false;
    private boolean scannerState = false;
    private boolean msrState = false;
    private int enterButtonCount = 0;
    public void keyPressed(KeyEvent e) {
        POSTerminalApplication app = POSTerminalApplication.getInstance();
        PopupMenuPane pop = app.getPopupMenuPane();
        PopupMenuPane1 pop1 = app.getPopupMenuPane1();
        SignOffForm signOffForm = app.getSignOffForm();
        ItemList itemList = app.getItemList();
        DacViewer dacViewer = app.getDacViewer();
        PayingPaneBanner payingPane = app.getPayingPane();
        //PasswordDialog pd = POSTerminalApplication.getInstance().getPasswordDialog();

        //if (checked) {
        //   //    System.out.println("*** KeyCode = " + e.getKeyCode() + " ***");
        //    //System.out.println(e.getComponent());
        //    //return;
        //}

        if (e.getKeyCode() == POSButtonHome.getInstance().getEnterCode()) {
            enterButtonCount++;
            if (enterButtonCount == 10) {
                CreamToolkit.logMessage("System is going to shutdown caused by pressing many Enter.");
                DacTransfer.shutdown(1);
            }
        } else {
            enterButtonCount = 0;
        }


        //int pageUp = POSTerminalApplication.getInstance().getPOSButtonHome().getPageUpCode();
        //int pageDown = POSTerminalApplication.getInstance().getPOSButtonHome().getPageDownCode();

        posKeyData = e.getKeyCode() * 1000;

        ArrayList s = POSButtonHome.getInstance().getSpcButton();
        boolean isSpcButton = false;
        for (int i = 0; i < s.size(); i++) {
            if (posKeyData == ((POSButton)s.get(i)).getKeyCode()) {
                isSpcButton = true;
                //System.out.println("**** you get key code = special button ****");
                break;
            }
        }
        if (e.getKeyCode() != KeyEvent.VK_PAGE_UP &&
            e.getKeyCode() != KeyEvent.VK_PAGE_DOWN &&
            !isSpcButton) {
            return;
        }

        if (e.getKeyCode() == KeyEvent.VK_PAGE_UP ||
            e.getKeyCode() == KeyEvent.VK_PAGE_DOWN)
            posKeyData /= 1000;

        /*int keyCode = e.getKeyCode();
        int shiftCode = 16;
        int blank = 32;
        int msr1 = 59;  //means ;
        int msr2 = 53;  //means %

        //msr
        if (keyCode == shiftCode) {
            shift = true;
            System.out.println("    **** you get key code = shift ****");
            return;
        } else {
            if (shift) {
                if (keyCode == msr1
                    || keyCode == msr2) {
                    msrState = true;
                    System.out.println("    **** you get key code = msr begin ****");
                    return;
                }
            }
        }
        if (msrState && keyCode == 10) {
            msrState = false;
            System.out.println("    **** you get key code = msr end ****");
            return;
        }

        //scanner
        if (keyCode == 32) {
            scannerState = true;
            System.out.println("    **** you get key code = scanner begin ****");
            return;
        }
        if (scannerState && keyCode == 10) {
            scannerState = false;
            System.out.println("    **** you get key code = scanner end ****");
            return;
        }

        if (!msrState && !scannerState) {
            if (e.getKeyCode() == 10) {
                System.out.println("    **** you get key code = enter ****");
                return;
            }

            //  check pop , itemList, payingPane with pageUp and pageDown
            /*if (posKeyData == pageUp || posKeyData == pageDown) {
                if (pop.isVisible()) {
                    pop.keyDataListener(posKeyData);
                    return;
                } else if (itemList.isVisible()) {
                    itemList.keyDataListener(posKeyData);
                    return;
                } else if (payingPane.isVisible()) {
                    payingPane.keyDataListener(posKeyData);
                }
            }*/

            //  check barcode button
            //System.out.println("fire event = " + posKeyData);
            if (signOffForm.isVisible()) {
                signOffForm.keyDataListener(posKeyData);
                return;
            } else if (pop.isVisible()) {
                pop.keyDataListener(posKeyData);
                //System.out.println("**** you get key code = pop is visible ****");
                return;
            } else if (pop1.isVisible()) {
                pop1.keyDataListener(posKeyData);
                //System.out.println("**** you get key code = pop is visible ****");
                return;
            } else if (itemList.isVisible()) {
                itemList.keyDataListener(posKeyData);
            } else if (payingPane.isVisible()) {
                payingPane.keyDataListener(posKeyData);
            } 


            if (dacViewer.isVisible())
                dacViewer.keyDataListener(posKeyData);
            //System.out.println("keyPressed(): fire DataEvent code=" + posKeyData);
            eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
        //}
    }

    boolean check = true;
    synchronized public void keyTyped(KeyEvent e) {
        PopupMenuPane pop = POSTerminalApplication.getInstance().getPopupMenuPane();
        PopupMenuPane1 pop1 = POSTerminalApplication.getInstance().getPopupMenuPane1();
        SignOffForm signOffForm = POSTerminalApplication.getInstance().getSignOffForm();
        //ItemList itemList = POSTerminalApplication.getInstance().getItemList();
        //PayingPane payingPane = POSTerminalApplication.getInstance().getPayingPane();
        //PasswordDialog pd = POSTerminalApplication.getInstance().getPasswordDialog();
        char keyChar = e.getKeyChar();
        posKeyData = (int)keyChar;
        //if (check) {
        //    System.out.println("*** " + keyChar + " = " + posKeyData + " ***");
        //    //System.out.println(e.getComponent());
        //    //return;
        //}

        if (keyChar == ':') {
            msrState = false;
        }

        //msr
        if (keyChar == ';' || keyChar == '%') {
            msr = true;
            return;
        }
        if (msr && posKeyData == 10) {
            msr = false;
            return;
        }

        //scanner
        //if (keyChar == ' ') {
        //System.out.println("PartnerKey> get " + (int)keyChar + "scannerPrefixCharValue=" + scannerPrefixCharValue);
        if ((int)keyChar == scannerPrefixCharValue) {
            scanner = true;
            return;
        }
        if (scanner && posKeyData == 10) {
            scanner = false;
            return;
        }

        if (!msr && !scanner) {
            //  check password dialog if show
            /*if (pd.isVisible()) {
                if (pd.keyListener(prompt, keyChar)) {
                    return;
                }
            }*/

            //popup menu's selection
            if (signOffForm.isVisible()) {
                signOffForm.keyDataListener(posKeyData);
                return;
            } else if (pop.isVisible()) {
                if (pop.keyDataListener(keyChar)) {
                    return;
                }
            } else if (pop1.isVisible()) {
                if (pop1.keyDataListener(keyChar)) {
                    return;
                }
            }

            //System.out.println("keyTyped(): fire DataEvent code=" + posKeyData);
            eventCallbacks.fireDataEvent(new DataEvent(eventCallbacks.getEventSource(), 0));
            shift = false;
        }
    }

    public Container createComp(String className) {
        try {
            Class compClass = Class.forName(className);
            Method compMethod = compClass.getDeclaredMethod("getInstance",
                                                            new Class[0]);
            comp = (Container)(compMethod.invoke(null, new Object[0]));
        } catch (ClassNotFoundException e) {
            CreamToolkit.logMessage(e.toString());
            CreamToolkit.logMessage("Class is not found at " + this);
        } catch (IllegalAccessException ex) {
            CreamToolkit.logMessage(ex.toString());
            CreamToolkit.logMessage("Illegal access exception at " + this);
        } catch (NoSuchMethodException exc) {
            CreamToolkit.logMessage(exc.toString());
            CreamToolkit.logMessage("No such method at " + this);
        } catch (InvocationTargetException exce) {
            CreamToolkit.logMessage(exce.toString());
            CreamToolkit.logMessage("Invocation exception at " + this);
        }
        return comp;
    }

    public void adkl(Container comp) {
        comp.addKeyListener(this);
        comp.addContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                adkl(compo);
            } else {
                co.addKeyListener(this);
            }
        }
    }

    public void rmkl(Container comp) {
        comp.removeKeyListener(this);
        comp.removeContainerListener(this);
        Component[] comp1 = comp.getComponents();
        int count = comp.getComponentCount();
        for (int i = 0; i < count; i++) {
            Component co = comp1[i];
            if (co instanceof Container) {
                Container compo = (Container)co;
                rmkl(compo);
            } else {
                co.removeKeyListener(this);
            }
        }
    }

    public void componentAdded(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            adkl((Container)e.getChild());
        } else {
            e.getChild().addKeyListener(this);
        }
    }

    public void componentRemoved(ContainerEvent e) {
        if (e.getChild() instanceof Container) {
            rmkl((Container)e.getChild());
        } else {
            e.getChild().removeKeyListener(this);
        }
    }

    private int keyMode = 0;
    private int count1 = 0;
    private int count2 = 0;
    private int proKeyCode = 0;
}

/*class Th implements Runnable {
    public void run() {
        char ch = ' ';
        ch = Passc.pr();
        System.out.println("######## finished ###########");
    }
}

class Passc {

    static {
        System.loadLibrary("test");
    }

    public native static char pr();
}*/
