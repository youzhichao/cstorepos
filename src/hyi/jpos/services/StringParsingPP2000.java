package hyi.jpos.services;

import java.io.*;
import java.util.*;

import jpos.*;
import hyi.jpos.services.*;

public class StringParsingPP2000 extends StringParsing {
    //Properties    
	private final String LF = "\n";
	private final String CR = "\r";
	private final String PRINT = "P";
	private final String commandStart = "\u001B";
	private final String commandEnd = CR;
	private final String doubleSize = "\u000E";
	private final String LINE_FEED = "L";
	private final String CUT_PAPER = "C";
	private final String HALF_CUT_PAPER = "c";

	private boolean normalSize = true;
	private boolean TaiwanMode = true;
	private POSPrinterPP2000 printer;


    public StringParsingPP2000(POSPrinterPP2000 printer) {
        this.printer = printer;
    }
    public StringParsingPP2000 ( ) throws Exception{
        throw new Exception ( "This class must be attached to a relevant POS printer device!" );
    }
    public String getCommandStart ( ) {
        return commandStart;
    }
    public String getCommandEnd ( ) {
        return commandEnd;
    }
    
    //Methods
    //--------------------------------------------------------------------------
    //Method to parse PP2000 related instruction which can transform the Jpos
    //standard string to the control instruction of PP2000
    //--------------------------------------------------------------------------

	private String obtainFirstEscape (String data) {
		 ArrayList ESC = getESCSequence (data);
		 if (!ESC.isEmpty())
             return (String)ESC.get(0);
         else
             return "";
	}

	String getPrintFriendlyString(String data, String position, String m) {
		StringBuffer valid = new StringBuffer("");
		StringBuffer valid1 = new StringBuffer("");
		int start = 0;
		for (int i = 0; i < data.length(); i++) {
			if (data.substring(start, i + 1).endsWith(LF)) {
				String currentLine = data.substring(start, i + 1);
				valid.append (getCommandStart() + PRINT + position + m);
				valid.append (currentLine + getCommandEnd());
				start = i + 1;
				normalSize = true;
				valid1.setLength(0);
			} else {
				valid1.append(data.charAt(i));
			}
		}
		if (valid1.length() > 0)
		   valid.append(getCommandStart() + PRINT + position + m + valid1 + getCommandEnd());
		return valid.toString();
	}

	String getDoubleSizedString(String str) {
		String result = "";
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (ch != CR.charAt(0) && ch != LF.charAt(0)) {
                result += doubleSize + ch;
			}
		}
		return result;
	}
	//--------------------------------------------------------------------------
	//Method to parse PP2000 related instruction which can transform the Jpos
	//standard string to the command of PP2000 specificed.
	//--------------------------------------------------------------------------
	public String getValidCommand ( int station, String data1, String data2 ) {
		String dataI  = data1;
		String dataII = data2;
		StringBuffer validCommand = new StringBuffer ();
		String validString = null;
		switch ( station ) {
			case POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL:
				break;
			case POSPrinterConst.PTR_TWO_SLIP_JOURNAL:
				return null;
			case POSPrinterConst.PTR_TWO_SLIP_RECEIPT:
				return null;
			default:
				return null;
		}
		if ( dataI.equals ( "" ) )
			return null;
		else {
			if ( dataII.equals ( "" ) )
				dataII = dataI;
			else {
				if ( ! dataI.equals ( data2 ) )
				return null;
			}
		}
		try {
			if (validateData (POSPrinterConst.PTR_S_RECEIPT, dataI ).size ( ) !=
				validateData (POSPrinterConst.PTR_S_JOURNAL, dataII ).size ( ) )
				return null;
		} catch ( JposException je ) { je.printStackTrace(); }
		validString  = getValidCommand ( POSPrinterConst.PTR_S_JOURNAL_RECEIPT, dataI );
		validCommand.append ( validString );
		return validCommand.toString ();
	}

	public String getValidCommand (int station, String data) {
		//----------------------------------------------------------------------
		StringBuffer validCommand = new StringBuffer();
		String position = null;
		String escape = null;
		ArrayList PP2000Escape = new ArrayList();

		//----------------------------------------------------------------------
		switch (station) {
			case POSPrinterConst.PTR_S_JOURNAL:
				 position = "J";
				 break;
			case POSPrinterConst.PTR_S_RECEIPT:
				 position = "R";
				 break;
			case POSPrinterConst.PTR_S_SLIP:
				 position = "V";
				 break;
			case POSPrinterConst.PTR_S_JOURNAL_RECEIPT:
				 position = "B";
				 break;
			default:
				 return null;
		}
		if (data.equals(""))
			return "";
		String m = "1";
		try {
			if (station == POSPrinterConst.PTR_S_JOURNAL_RECEIPT) {
				PP2000Escape = validateData (POSPrinterConst.PTR_S_RECEIPT, data);
				//validCommand.append (getCommandStart() + PRINT + position + m);
			} else
				PP2000Escape = validateData (station, data);
			/* else if (station == POSPrinterConst.PTR_S_RECEIPT || station == POSPrinterConst.PTR_S_JOURNAL ) {
				PP2000Escape = validateData (station, data);
				validCommand.append (getCommandStart() + PRINT + position + m);
				//validCommand.append (getCommandStart () + "c0" + position + getCommandStart ( ) + "z\u0090");
			} else {
				PP2000Escape = validateData (station, data);
				validCommand.append (getCommandStart() + PRINT + position + m);
				//validCommand.append (getCommandStart () + "c0" + position);
			}*/
		} catch (JposException je) { je.printStackTrace(); }
		if (PP2000Escape.isEmpty ()) {
			validCommand.append(getPrintFriendlyString(data, position, m));
			return validCommand.toString();
		} //System.out.println(PP2000Escape);
		StringBuffer buffer = new StringBuffer();
		//Use the code below to parse the string
		for (int i = 0; i < data.length(); i ++) {
			if (data.substring(i).startsWith( "\u001B|")) {
				escape = obtainFirstEscape (data.substring (i));
				if (buffer.length() > 0) {
					if (!escape.endsWith("C")) {
						validCommand.append(getPrintFriendlyString(buffer.toString(), position, m));
						buffer.setLength(0);
					}
				}
				if (PP2000Escape.contains(escape)) {
					try {
						switch (escape.charAt( escape.length () - 1)) {
							case 'P' :
								int percent = 100;
								if ( escape.charAt( escape.length ( ) - 2 ) == 'f'
									|| escape.charAt( escape.length ( ) - 2 ) == 's') {
									int lines = printer.getRecLinesToPaperCut();
									if (lines < 10 && lines > 0)
										validCommand.append (getCommandStart() +  LINE_FEED + position + lines + getCommandEnd());
									else if (lines >= 10) {
										int cyc = lines / 9;
										for (int j = 0; j < cyc; j++)
											validCommand.append (getCommandStart() + LINE_FEED + position + "9" + getCommandEnd());
										if (lines % 9 > 0) {
											int rest = lines % 9;
											validCommand.append (getCommandStart() + LINE_FEED + position + rest + getCommandEnd());
										}
									}
									if (isNumber(escape.substring (2, escape.length () - 2)))
										percent = Integer.decode(escape.substring(2, escape.length() - 2)).intValue();
									String combine = "";
									if (escape.charAt( escape.length ( ) - 2 ) == 's') {
										if (percent == 100)
											combine = "V";
										else
											combine = "v";
									} else if (escape.charAt( escape.length ( ) - 2 ) == 'f') {
										if (percent == 100)
											combine = "J";
										else
											combine = "j";
									}
									if (combine.length() > 0)
										validCommand.append (getCommandStart () + combine + position + getCommandEnd());
								} else {
									if (isNumber(escape.substring (2, escape.length () - 1)))
										percent = Integer.decode(escape.substring(2, escape.length() - 1)).intValue();
									if (percent == 100) {
										validCommand.append (getCommandStart() + CUT_PAPER + getCommandEnd());
									} else {
                                       	validCommand.append (getCommandStart() + HALF_CUT_PAPER + getCommandEnd());
									}
                                }
                                break;
                            case 'L' :
								if ( escape.charAt( escape.length ( ) - 2 ) == 't' )
									validCommand.append (getValidCommand (station, printer.getTopLogo ()));
                                else if ( escape.charAt( escape.length ( ) - 2 ) == 'b' )
									validCommand.append (getValidCommand (station, printer.getBottomLogo()));
								else
									validCommand.append (getCommandStart () + "S" + getCommandEnd());
								break;
							case 'F' :
								if (!escape.equals("\u001B|lF")) {
									int times = Integer.decode(escape.substring (2, escape.length() - 2)).intValue();
									String feed = String.valueOf(times);
									if (times < 10 && times > 0) {
										validCommand.append (getCommandStart() + LINE_FEED + position + feed + getCommandEnd());
									} else {
										int cyc = times / 9;
										for (int j = 0; j < cyc; j++)
											validCommand.append (getCommandStart() + LINE_FEED + position + "9" + getCommandEnd());
										if (times % 9 > 0) {
											int rest = times % 9;
											String lines = String.valueOf(rest);
											validCommand.append (getCommandStart() + LINE_FEED + position + lines + getCommandEnd());
										}
									}
									//validCommand.append ("\u001Bd"+ new Character(feed));
								} else
									validCommand.append (getCommandStart() + LINE_FEED + position + "1" + getCommandEnd());
								break;
                            case 'C' :
								if (escape.charAt(escape.length () - 2) == '1')
									normalSize = true;
								else
									normalSize = false;
								break;
							case 'N' :
								normalSize = true;
							case 'V' :
                                validCommand.append (getCommandStart() + "V" + position + getCommandEnd());
						}
					} catch ( JposException je ) { je.printStackTrace(); }
					//----------------------------------------------------------
				} else {
					validCommand.append(escape); //
				}
				i += escape.length () - 1;
			} else {//not start with "ESC"
				if (data.charAt(i) == '\n') {
					buffer.append(data.charAt(i));
					normalSize = true;
				} else {
					String next = String.valueOf(data.charAt(i));
					if (!normalSize)
						next = getDoubleSizedString(String.valueOf(data.charAt(i)));
					buffer.append(next);
				}
			}
		}
		if (buffer.length() > 0) { //System.out.println(buffer);
			validCommand.append(getPrintFriendlyString(buffer.toString(), position, m));
			buffer.setLength(0);
		}
		if (station == POSPrinterConst.PTR_S_SLIP && !validCommand.toString().endsWith("\n"))
			validCommand.append(LF);
		return validCommand.toString ();
    }

    public ArrayList validateData (int station, String data ) throws JposException {
		if (!(station==POSPrinterConst.PTR_S_JOURNAL
			|| station==POSPrinterConst.PTR_S_RECEIPT
			|| station==POSPrinterConst.PTR_S_SLIP))
				throw new JposException(JposConst.JPOS_E_ILLEGAL,
					"Invalid station argument: station=" + station);
		String item = null;
		String[] common = {"\u001B|rA", "\u001B|cA", "\u001B|3C", "\u001B|4C",
						  "\u001B|rC", "\u001B|rvC", "\u001B|iC", "\u001B|bC",};
		ArrayList ESCSequence = getESCSequence (data);
		if (ESCSequence.isEmpty())
			return ESCSequence;
		ArrayList JposESCSequence = getJposESCSequence (data);
		ArrayList PP2000ESCSequence = JposESCSequence;
		for (int k = JposESCSequence.size() - 1; k > -1; k--) {
			item = (String)JposESCSequence.get(k);
			if (contains(common, item))
				PP2000ESCSequence.remove(k);
			else if (item.substring(item.length() - 1).equals("B")
					|| item.substring(item.length() - 2 ).equals("uF")
					|| item.substring( item.length() - 2 ).equals("rF")
					|| item.substring( item.length() - 2 ).equals("fT")
					|| item.substring( item.length() - 2 ).equals("uC")
					|| item.substring( item.length() - 2 ).equals("sC")
					|| item.substring( item.length() - 2 ).equals("hC")
					|| item.substring( item.length() - 2 ).equals("vC") )
				PP2000ESCSequence.remove(k);
		}
		switch (station) {
			case POSPrinterConst.PTR_S_JOURNAL:
				/*
				for ( int k = JposESCSequence.size( )-1 ; k > -1 ; k-- ) {
					item = ( String ) JposESCSequence.get( k );
						if ( item.substring( item.length() - 1 ).equals( "P" )
							|| item.substring( item.length() - 2).equals( "sL" ) )
							PP2000ESCSequence.remove( k );
				}*/
				break;
			case POSPrinterConst.PTR_S_RECEIPT:
				break;
            case POSPrinterConst.PTR_S_SLIP:
                for ( int k = JposESCSequence.size( )-1 ; k > -1 ; k-- ) {
                    item = ( String ) JposESCSequence.get( k );
                    if ( item.substring( item.length() - 1 ).equals("P")
                        || item.substring( item.length() - 2 ).equals("sL")
                        || item.substring( item.length() - 2 ).equals("lF") )
                        PP2000ESCSequence.remove( k );
                }
                break;
         }
         return PP2000ESCSequence;
    }
}
