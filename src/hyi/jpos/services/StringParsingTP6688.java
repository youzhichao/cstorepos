package hyi.jpos.services;

import java.io.*;
import java.util.*;
import jpos.*;

public class StringParsingTP6688 extends StringParsing {
    //Properties    
    private String LF = "\n";
	private String CR = "\r";
	private String PRINT = "P";
	private String commandStart = "\u001B\u001B";
	private String commandEnd = CR;
	private String doubleSize = "\u000E";
	private String LINE_FEED = "L";
	private String CUT_PAPER = "C";

	private boolean normalSize = true;
	//private String FF = "\u000C";
	//private String RS = "\u001E";
	//private String GS = "\u001D";
	//private String DLE = "\u0010";
	private boolean TaiwanMode = true;
	private POSPrinterTP6688 printer;


    public StringParsingTP6688 (POSPrinterTP6688 printer) {
        this.printer = printer;
    }
    public StringParsingTP6688 ( ) throws Exception{
        throw new Exception ( "This class must be attached to a relevant POS printer device!" );
    }
    public String getCommandStart ( ) {
        return commandStart;
    }
    public String getCommandEnd ( ) {
        return commandEnd;
    }
    
    //Methods
    //--------------------------------------------------------------------------
    //Method to parse TP6688 related instruction which can transform the Jpos
    //standard string to the control instruction of TP6688
    //--------------------------------------------------------------------------

	private String obtainFirstEscape (String data) {
		 ArrayList ESC = getESCSequence (data );
		 if (!ESC.isEmpty())
             return (String)ESC.get(0);
         else
             return "";
	}

	String getPrintFriendlyString(String data, String position, String m) {
		StringBuffer valid = new StringBuffer("");
		StringBuffer valid1 = new StringBuffer("");
		int start = 0;
		for (int i = 0; i < data.length(); i++) {
			if (data.substring(start, i + 1).endsWith(LF)) {
				String currentLine = data.substring(start, i + 1);
				//System.out.println("@" + currentLine + "@");
				//if (!normalSize) {
					//currentLine = getDoubleSizedString(currentLine);
				//}
				valid.append (getCommandStart() + PRINT + position + m);
				valid.append (currentLine + getCommandEnd());
				start = i + 1;
				normalSize = true;
				valid1.setLength(0);
				//System.out.println(start);
			} else {
				//System.out.println(start);
				valid1.append(data.charAt(i));
			}
		}
		if (valid1.length() > 0)
		   valid.append(getCommandStart() + PRINT + position + m + valid1 + getCommandEnd());
		return valid.toString();
	}

	String getDoubleSizedString(String str) {
		String result = "";
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			//String c = "";
			if (ch != CR.charAt(0) && ch != LF.charAt(0)) {
                result += doubleSize + ch;
			}
		}
		return result;
	}
	//--------------------------------------------------------------------------
	//Method to parse Epson RP-U420 related instruction which can transform the Jpos
	//standard string to the command of RP-U420 specificed.
	//--------------------------------------------------------------------------
    public String getValidCommand ( int station, String data1, String data2 ) {
        String dataI  = data1;
        String dataII = data2;
        StringBuffer validCommand = new StringBuffer ();
        String validString = null;
        switch ( station ) {
            case POSPrinterConst.PTR_TWO_RECEIPT_JOURNAL:
                break;
            case POSPrinterConst.PTR_TWO_SLIP_JOURNAL:
                return null;
            case POSPrinterConst.PTR_TWO_SLIP_RECEIPT:
                return null;
            default:
                return null;
        }
        if ( dataI.equals ( "" ) )
            return null;
        else {
            if ( dataII.equals ( "" ) )
                dataII = dataI;
            else {
                if ( ! dataI.equals ( data2 ) )
                return null;
            }
        }
        try {
            if ( validateData ( POSPrinterConst.PTR_S_RECEIPT, dataI ).size ( ) !=
                validateData ( POSPrinterConst.PTR_S_JOURNAL, dataII ).size ( ) )
                return null;
        } catch ( JposException je ) { System.out.println(je); }
        validString  = getValidCommand ( POSPrinterConst.PTR_S_JOURNAL_RECEIPT, dataI );
        validCommand.append ( validString );
        return validCommand.toString ();
    }

	public String getValidCommand (int station, String data) {
        //----------------------------------------------------------------------
		StringBuffer validCommand = new StringBuffer();
        String position = null;
        String escape = null;
        ArrayList TP6688Escape = new ArrayList();
        //----------------------------------------------------------------------
        switch (station) {
            case POSPrinterConst.PTR_S_JOURNAL:
				 position = "J";
                 break;
            case POSPrinterConst.PTR_S_RECEIPT:
				 position = "R";
                 break;
            case POSPrinterConst.PTR_S_SLIP:
				 position = "V";
                 break;
			case POSPrinterConst.PTR_S_JOURNAL_RECEIPT:
                 position = "B";
                 break;
			default:
                 return null;
        }
        if (data.equals(""))
			return "";
		String m = "1";
		try {
			if (station == POSPrinterConst.PTR_S_JOURNAL_RECEIPT) {
				TP6688Escape = validateData (POSPrinterConst.PTR_S_JOURNAL, data);
				//validCommand.append (getCommandStart() + PRINT + position + m);
			} else
		    	TP6688Escape = validateData (station, data);
			/* else if (station == POSPrinterConst.PTR_S_RECEIPT || station == POSPrinterConst.PTR_S_JOURNAL ) {
				TP6688Escape = validateData (station, data);
				validCommand.append (getCommandStart() + PRINT + position + m);
				//validCommand.append (getCommandStart () + "c0" + position + getCommandStart ( ) + "z\u0090");
			} else {
				TP6688Escape = validateData (station, data);
				validCommand.append (getCommandStart() + PRINT + position + m);
				//validCommand.append (getCommandStart () + "c0" + position);
			}*/
		} catch (JposException je) { je.printStackTrace(); }
		if (TP6688Escape.isEmpty ()) {
			validCommand.append(getPrintFriendlyString(data, position, m));
			///System.out.println("#" +validCommand + "#");
			return validCommand.toString();
		}
		//System.out.println(TP6688Escape);
		StringBuffer buffer = new StringBuffer();
		//Use the code below to parse the string
		for (int i = 0; i < data.length(); i ++) {
			if (data.substring(i).startsWith( "\u001B|")) {
				escape = obtainFirstEscape (data.substring (i));
				if (buffer.length() > 0) {
					if (!escape.endsWith("C")) {
						//System.out.println(buffer);
						validCommand.append(getPrintFriendlyString(buffer.toString(), position, m));
						buffer.setLength(0);
					}
				}			
				if (TP6688Escape.contains(escape)) {
					try {
                        switch (escape.charAt( escape.length () - 1)) {
                            case 'P' :
								if ( escape.charAt( escape.length ( ) - 2 ) == 'f'
									|| escape.charAt( escape.length ( ) - 2 ) == 's') {
									int lines = printer.getRecLinesToPaperCut();
									if (lines < 10)
										validCommand.append (getCommandStart() +  LINE_FEED + position + lines + getCommandEnd());
									//if (isNumber(escape.substring (2, escape.length () - 2))) {
										//int percent = Integer.decode(escape.substring(2, escape.length() - 2)).intValue();
										//if (percent == 100)
										validCommand.append (getCommandStart() + CUT_PAPER + getCommandEnd());
									//} else {
                                        //validCommand.append ( GS + "V\u0001" );
									//}
								} else {
									/*if (isNumber(escape.substring (2, escape.length ( ) - 1 ))) {
                                        int percent = Integer.decode(escape.substring(2, escape.length() - 1)).intValue();
                                        if ( percent == 100 ){
											validCommand.append ( GS + "V\u0001" );
											}
                                        else
                                            validCommand.append ( GS + "V\u0002" );
									} else {
                                        validCommand.append ( GS + "V\u0001" );
									}*/
									validCommand.append (getCommandStart() + CUT_PAPER + getCommandEnd());
                                }
                                break;
                            case 'L' :
								if ( escape.charAt( escape.length ( ) - 2 ) == 't' )
									validCommand.append (getValidCommand (station, printer.getTopLogo ()));
                                else if ( escape.charAt( escape.length ( ) - 2 ) == 'b' )
									validCommand.append (getValidCommand (station, printer.getBottomLogo()));
								else
									validCommand.append (getCommandStart () + "S" + getCommandEnd());
								break;
							case 'F' :
								if (!escape.equals("\u001B|lF")) {
									int times = Integer.decode(escape.substring (2, escape.length() - 2)).intValue();
									String feed = String.valueOf(times);
									if (times < 10) {
										validCommand.append (getCommandStart() + LINE_FEED + position + feed + getCommandEnd());
									} else {
										int cyc = times / 9;
										for (int j = 0; j < cyc; j++)
											validCommand.append (getCommandStart() + LINE_FEED + position + "9" + getCommandEnd());
										if (times % 9 > 0) {
											int rest = times % 9;
											String lines = String.valueOf(rest);
											validCommand.append (getCommandStart() + LINE_FEED + position + lines + getCommandEnd());
										}
									}
									//validCommand.append ("\u001Bd"+ new Character(feed));
								} else
									validCommand.append (getCommandStart() + LINE_FEED + position + "1" + getCommandEnd());
								break;
                            case 'C' :
								if (escape.charAt(escape.length () - 2) == '1')
									normalSize = true;
								else
									normalSize = false;
								break;
							case 'N' :
								normalSize = true;
							case 'V' :
                                validCommand.append (getCommandStart() + "V" + "R" + getCommandEnd());
						}
					} catch ( JposException je ) { je.printStackTrace(); }
					//----------------------------------------------------------
				} else {
					validCommand.append(escape); //
				}
				i += escape.length () - 1;
			} else {//not start with "ESC"
				if (data.charAt(i) == '\n') {
					buffer.append(data.charAt(i));
					normalSize = true;
				} else {
					String next = String.valueOf(data.charAt(i));
					if (!normalSize)
						next = getDoubleSizedString(String.valueOf(data.charAt(i)));
					buffer.append(next);
				}
			}
		}
		if (buffer.length() > 0) {
			//System.out.println(buffer);
			validCommand.append(getPrintFriendlyString(buffer.toString(), position, m));
			buffer.setLength(0);
		}
		if (station == POSPrinterConst.PTR_S_SLIP && !validCommand.toString().endsWith("\n"))
			validCommand.append(LF);
		//System.out.println("#" +validCommand + "#");
        return validCommand.toString ();
    }

    public ArrayList validateData ( int station, String data ) throws JposException {
        if (!( station==POSPrinterConst.PTR_S_JOURNAL
            || station==POSPrinterConst.PTR_S_RECEIPT
            || station==POSPrinterConst.PTR_S_SLIP ))
                throw new JposException( JposConst.JPOS_E_ILLEGAL,
                    "Invalid station argument: station=" + station);
        String item = null;
        String[] common = { "\u001B|rA", "\u001B|cA", "\u001B|3C", "\u001B|4C",
                          "\u001B|rC", "\u001B|rvC", "\u001B|iC", "\u001B|bC" };
        ArrayList ESCSequence = getESCSequence ( data );

        if ( ESCSequence.isEmpty ( ) )
            return ESCSequence;

        ArrayList JposESCSequence = getJposESCSequence ( data );

        ArrayList TP6688ESCSequence = JposESCSequence;

        for ( int k = JposESCSequence.size( )-1 ; k > -1 ; k-- ) {
            item = ( String ) JposESCSequence.get( k );
            if (contains(common, item))
                TP6688ESCSequence.remove( k );
            else if ( item.substring( item.length() - 1 ).equals("B")
                    || item.substring( item.length() - 2 ).equals("uF")
                    || item.substring( item.length() - 2 ).equals("rF")
                    || item.substring( item.length() - 2 ).equals("fT")
                    || item.substring( item.length() - 2 ).equals("uC")
                    || item.substring( item.length() - 2 ).equals("sC")
                    || item.substring( item.length() - 2 ).equals("hC")
                    || item.substring( item.length() - 2 ).equals("vC") )
                TP6688ESCSequence.remove( k );
        }
        switch (station) {
            case POSPrinterConst.PTR_S_JOURNAL:
                for ( int k = JposESCSequence.size( )-1 ; k > -1 ; k-- ) {
                    item = ( String ) JposESCSequence.get( k );
                        if ( item.substring( item.length() - 1 ).equals( "P" )
                            || item.substring( item.length() - 2 ).equals( "sL" ) )
                            TP6688ESCSequence.remove( k );
                }
                break;
            case POSPrinterConst.PTR_S_RECEIPT:
                break;
            case POSPrinterConst.PTR_S_SLIP:
                for ( int k = JposESCSequence.size( )-1 ; k > -1 ; k-- ) {
                    item = ( String ) JposESCSequence.get( k );
                    if ( item.substring( item.length() - 1 ).equals("P")
                        || item.substring( item.length() - 2 ).equals("sL")
                        || item.substring( item.length() - 2 ).equals("lF") )
                        TP6688ESCSequence.remove( k );
                }
                break;
         }
         return TP6688ESCSequence;
    }
}
