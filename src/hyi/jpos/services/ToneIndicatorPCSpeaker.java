package hyi.jpos.services;

/**
 * ToneIndicatorService of PCSpeaker version
 * NOTE: 
 * @since 2000
 * @author slackware
 */

import jpos.*;
import jpos.events.*;
import jpos.services.*;
import jpos.config.simple.*;

import java.awt.*;
import java.util.*;

public class ToneIndicatorPCSpeaker extends DeviceService implements ToneIndicatorService14 {

    //Static fields here to memorize the control, claimed controls.

    static private ToneIndicator claimedControl;
	static private Object mutex = new Object();
	static int running = 0;

	//Default constructor

    public ToneIndicatorPCSpeaker ( ) { }

    public ToneIndicatorPCSpeaker(SimpleEntry entry){}

    // Capabilities version 12

    public boolean getCapPitch() throws JposException {
        return capPitch;
    }

    public boolean getCapVolume() throws JposException {
        return capVolume;
    }

    // Capabilities of 13 to implement
    
    public int getCapPowerReporting() throws JposException {
        return JposConst.JPOS_PR_NONE;
    }

    // Properties version 12
    
    public boolean getAsyncMode() throws JposException {
        return asyncMode;
    }

    public void setAsyncMode (boolean asyncMode) throws JposException {
        this.asyncMode=asyncMode;
    }

    public int getInterToneWait() throws JposException {
        return interToneWait;
    }

    public void setInterToneWait(int interToneWait) throws JposException{
        this.interToneWait=interToneWait;
    }
    
    public int  getOutputID() throws JposException {
        return outputID;
    }

    public int  getTone1Duration() throws JposException {
        return tone1Duration;
    }

    public void setTone1Duration(int tone1Duration) throws JposException {
        this.tone1Duration=tone1Duration;
    }

    public int  getTone1Pitch() throws JposException {
        return tone1Pitch;
    }

    public void setTone1Pitch(int tone1Pitch) throws JposException {
        this.tone1Pitch=tone1Pitch;
    }

    public int  getTone1Volume() throws JposException {
        return tone1Volume;
    }

    public void setTone1Volume(int tone1Volume) throws JposException {
        this.tone1Volume=tone1Volume;
    }
    
    public int getTone2Duration() throws JposException {
        return tone2Duration;
    }
    
    public void setTone2Duration(int tone2Duration) throws JposException {
        this.tone2Duration=tone2Duration;
    }

    public int  getTone2Pitch() throws JposException {
        return tone2Pitch;
    }

    public void setTone2Pitch(int tone2Pitch) throws JposException {
        this.tone2Pitch = tone2Pitch;
    }

    public int getTone2Volume() throws JposException {
        return tone2Volume;
    }

    public void setTone2Volume(int tone2Volume) throws JposException {
        this.tone2Volume = tone2Volume;
    }

    // Properties of 13 to implement power related information

    public int getPowerNotify() throws JposException {
        return JposConst.JPOS_PN_DISABLED;
    }

    public int getPowerState() throws JposException {
        return JposConst.JPOS_PS_UNKNOWN;
    }

    public void setPowerNotify(int powerNotify) throws JposException {}

    //--------------------------------------------------------------------------
    // jpos.config.JposServiceInstance methods
    //--------------------------------------------------------------------------

    public void deleteInstance() throws JposException {}

    //--------------------------------------------------------------------------
    // Common overridden methods
    //--------------------------------------------------------------------------

    public void checkHealth(int level)  throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available!");
    }

    public void directIO(int command, int[] data, Object object) throws JposException {
        throw new JposException (JposConst.JPOS_E_ILLEGAL, "No service available!");
    }


    public void open(String logicalName, EventCallbacks cb) throws JposException {
        super .open(logicalName, cb);
        //Initialize the variables
        asyncMode = false;
        capPitch = false;
        capVolume = false;
        tone1Pitch = 0;
        tone1Volume = 0;
        tone1Duration = 0;
        tone2Pitch = 0;
        tone2Volume = 0;
        tone2Duration = 0;
        interToneWait = 0;
        outputID=0;
    }

    public void close() throws JposException {
            //Remove the control instance from the list, namely unlink them from the service
        if (getClaimed())
            release();     //If current device service is claimed, release it immediately
        clearOutput();     //If there are still any unfinished output in process, clear them.
        super.close();
        asyncMode = false;
        capPitch = false;
        capVolume = false;
        tone1Pitch = 0;
        tone1Volume = 0;
        tone1Duration = 0;
        tone2Pitch = 0;
        tone2Volume = 0;
        tone2Duration = 0;
        interToneWait = 0;
    }

    /*--------------------------------------------------------------------------
     * The claim()&release() part of the class
     * Some illustration of how it works.
     *
     *------------------------------------------------------------------------*/

  
    public void claim(int timeout) throws JposException {
        //It has been claimed by the instance itself
        if (blocked)
            throw new JposException (JposConst.JPOS_E_ILLEGAL,
                "Waiting in the queue!");
        if (claimed)
            throw new JposException (JposConst.JPOS_E_ILLEGAL,
                "Duplicate claimed by the same device control!");

        if (timeout<-1)
            throw new JposException(JposConst.JPOS_E_ILLEGAL,
                "Illegal timeout parameter detected:"+timeout+".");
        synchronized (mutex) {

        if (claimedControl == null) { // Nobody has claimed it
            claimedControl = (ToneIndicator)eventCallbacks.getEventSource();
            //Now the claim manipulation success, call claim() method of superclass
            //to set the claim property to true.
            super.claim(timeout);
            return;
        }

        // someone else has claimed it...

        if (timeout == 0) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL,
                "Device has been claimed by other control!");
        }
        else if (timeout == JposConst.JPOS_FOREVER) {
            try {
                blocked = true;
				while (claimedControl!=null) {
                     mutex.wait();
                }
                blocked = false;
                claimedControl =(ToneIndicator)eventCallbacks.getEventSource();
                super.claim(timeout);
                return;
            } catch (InterruptedException e) {
                 throw new JposException(JposConst.JPOS_E_ILLEGAL,"Claim interrupted in process at"+this.toString()+"!");
            }
        }
        else {
            try {
                long start = System.currentTimeMillis();
                long waitTime = timeout;
                while (waitTime > 0) {

                        mutex.wait(timeout);
                   
      // wait() returns when time is up or being notify().
                    if (claimedControl == null) {
                        claimedControl = (ToneIndicator)eventCallbacks.getEventSource();
                        super.claim(timeout);
                        return;
                    }
     // if wait not enough time, it should keep wait() again....


                    waitTime = timeout - (System.currentTimeMillis() - start);
                }

    // if still can't claim(), and time has elapsed.
    
                throw new JposException(JposConst.JPOS_E_TIMEOUT,"Time is up, claim failed!");
            }
		        catch (InterruptedException e) {
                //System.out.println("Here!");
                throw new JposException(JposConst.JPOS_E_ILLEGAL,
                    "Claim interrupted in process at"+this.toString()+"!");
            }
        }
        }
    }


    

//------------------------------------------------------------------------------

    public void release() throws JposException {
    
        if (!claimed) {
            throw new JposException (JposConst.JPOS_E_ILLEGAL,"Device is not claimed.");
        }
        
        synchronized (mutex) {
            if (claimedControl == null)
                throw new JposException(JposConst.JPOS_E_ILLEGAL);
            claimedControl = null;
            super.release();
            mutex.notifyAll();
        }
    }


    //--------------------------------------------------------------------------

    // Methods

    public void clearOutput() throws JposException {
		outputID--;
		running--;
		//unClearedID++;
		//if (threadList.isEmpty())
			//return;
		/*synchronized(threadList) {
			for (int i = 0; i < 1; i++) {
				  Beep t = (Beep)threadList.get(i);
				t.interrupt();
                try {
					t.join(150);
					unClearedID--;
					   // Wait for the beeping thread to die.
					  // Give a timeout value to advoid deadlock.
				} catch (InterruptedException e) {
					 //System.out.println(e+"join()in clearOuput() is interrupted!");
					 e.printStackTrace();
				} finally {
					//threadList.remove(0);

				}
					  // If that thread still doesn't die, just ignore it, we supposed that it'll
					  // stop execution shortly by itself because it has been interrupted and/or it
					  // maybe also waiting on locking collection object "threadList."
		   }
		   while(unClearedID > 0) {
			  Beep t = (Beep)threadList.get(0);
			  t.interrupt();
			  unClearedID--;
			  //threadList.remove(0);
		   }
		}
		//*/
	}

    public void sound(int numberOfCycles, int interSoundWait)throws JposException {

        if (claimedControl!=null) {
        /*if true here, the method will verify the current service instance if it is equal
         * to the element stored in the deviceControlClaimed Vector.
         */
            if (!eventCallbacks.getEventSource().equals(claimedControl)) {
                throw new JposException(JposConst.JPOS_E_CLAIMED,
                    "Device has been claimed by another device control of "+claimedControl.toString());
            }

        }
		if (getDeviceEnabled()) {
			if (!getAsyncMode()) {
				try {
                     state = JposConst.JPOS_S_BUSY;
                     for (int looptimes=0;looptimes<numberOfCycles;looptimes++){
                           Toolkit.getDefaultToolkit().beep();
                           Thread.sleep(interSoundWait);
                     }
					 state = JposConst.JPOS_S_IDLE;
					 		  //System.out.println("!"+numberOfCycles+"%");
                } catch(Exception e){System.out.print(e);}
            } else {
				 try {
					 new Beep(numberOfCycles, interSoundWait);
					  //System.out.println("!"+numberOfCycles+"!");
                } catch(Exception e){System.out.print(e);}
            }
        }
        else {
            throw new JposException(JposConst.JPOS_E_ILLEGAL,"Device control not enabled");
       
        }
        return;
    }

    //Inner class to create a new thread for sounding the PC Speaker

	class Beep extends Thread {

        private int numberOfCycles = 0;
        private int interSoundWait = 0;
        private int looptimes = 0;

        //Default constructor

        Beep(){start();}

        //Constructor II; Get necessary parameters to start the beep thread

		Beep(int numberOfCycles, int interSoundWait){
			if (running < 0)
			    running = 0;
		    running++;
            this.numberOfCycles = numberOfCycles;
            this.interSoundWait = interSoundWait;
            start();
		}

		public void run() {
			try {
				state = JposConst.JPOS_S_BUSY;
				if (running > 1)
					return;
				for (looptimes = 0; looptimes < numberOfCycles; looptimes++){
				   Toolkit.getDefaultToolkit().beep();
				   if (running < 1)
				       break;
				   sleep(interSoundWait);
				   if (looptimes == numberOfCycles - 1)
    				   running--;
				}			
				state = JposConst.JPOS_S_IDLE;
			} catch (Exception e) {

			}
		}

		/*
		public void run() {
           Thread thisThread = Thread.currentThread();
               threadList.add(thisThread);
               if (thisThread.isInterrupted()) {
                   threadList.remove(thisThread);
                   return;
               }
			   try {
				   state = JposConst.JPOS_S_BUSY;
				   for (looptimes=0; looptimes<numberOfCycles; looptimes++){
						//System.out.println("!"+looptimes+"!");
					   /*if (thisThread.isInterrupted()) {
						   threadList.remove(thisThread);
						   //System.out.println("!"+looptimes+"@");
						   return;
					   }////*
					   Toolkit.getDefaultToolkit().beep();
					   //System.out.println("!"+looptimes+"!");
					   sleep(interSoundWait);
						//System.out.println("!"+looptimes+")))))))");
				   }
				  // System.out.println("!"+looptimes+"******");
				   state = JposConst.JPOS_S_IDLE;
			  } catch (InterruptedException e){
				  //System.out.println("Cleared by clearOuput()"+e);
			  } finally {
			 //System.out.println("||||||||||||||||"+looptimes+"!");
				  if (looptimes==numberOfCycles) {
					  outputID++;
					  OutputCompleteEvent out =  new
						  OutputCompleteEvent(ToneIndicatorPCSpeaker.this,outputID);
					  eventCallbacks.fireOutputCompleteEvent(out);
				  }
				  threadList.remove(thisThread);
			  }
		}*/
    }

    public void soundImmediate() throws JposException {
   
        if (claimedControl!=null) {
            if (!eventCallbacks.getEventSource().equals(claimedControl)) {
                //System.out.println(this.eventCallbacks.getEventSource()+"VS"+claimedControl);
                return;
            }
        }
        if (getDeviceEnabled())
            if (!getAsyncMode()){
                Toolkit.getDefaultToolkit().beep();
            }
            else {
                new Beep(1,0);
            }
        else {
            throw new JposException(JposConst.JPOS_E_ILLEGAL,
                "Device control not enabled");
        }
    }

    // Private fields

    //System fields here-------------------------------------------------------

    private boolean asyncMode = false;
    private boolean capPitch = false;
    private boolean capVolume = false;
    private int tone1Pitch = 0;
    private int tone1Volume = 0;
    private int tone1Duration = 0;
    private int tone2Pitch = 0;
    private int tone2Volume = 0;
    private int tone2Duration = 0;
    private int interToneWait = 0;
	private int outputID = 0;
	private int unClearedID = 0;

    //User Fields here----------------------------------------
    
  
    private Object synchronize = new Object();
    private boolean blocked = false;
    private java.util.List threadList = Collections.synchronizedList(new ArrayList());
    
}

