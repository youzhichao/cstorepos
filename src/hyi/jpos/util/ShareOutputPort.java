package hyi.jpos.util;

import java.util.*;
import java.io.*;
import javax.comm.*;

import jpos.*;
import hyi.jpos.services.*;

public class ShareOutputPort {// Singleton class to implement the function of sharing one
                              // serial port between the printer and cash drawer
    private static ShareOutputPort instance=null;
	private static OutputStreamWriter dataOutput = null;
	private static InputStreamReader dataInput = null;

     /** input & output Stream attached to POSPrinter */
    private static OutputStream outputStream = null;
    private static InputStream inputStream = null;
    
    private static SerialPortEventListener currentListener = null;
    /** serial port current feedback status got asyncronously */
    //
    //--------------------------------------------------------------------------

    /** serialport related variables, used in open() */
    private Enumeration portList = null;
    private CommPortIdentifier portId = null;
	private SerialPort serialPort = null;
	private String statusCode = null;

    //--------------------------------------------------------------------------
    private String eventSource;
    private SerialPortEventListener source;

	public ShareOutputPort() throws InstantiationException {
		if (instance != null )
			throw new InstantiationException ("Singleton class here.");
		else {
			statusCode = "";
			instance = this;
		}
	}

	public static ShareOutputPort getInstance() {
		try {
            if (instance == null)
                instance = new ShareOutputPort();
		} catch (InstantiationException ie){
            return instance;
        }
            return instance;
	}


	public void setStatusCode(String code) {
		this.statusCode = code;
	}

	public String getStatusCode() {
        return statusCode; 
	}

    synchronized public SerialPortEventListener getCurrentListener() {
        return currentListener;
    }

    //Properties related method;
    public OutputStreamWriter getOutputStreamWriter () {
        return dataOutput;
	}

	public InputStreamReader getInputStreamReader () {
        return dataInput;
	}

    public OutputStream getOutputStream () {
        return outputStream;
    }

    public InputStream getInputStream () {
        return inputStream;
    }
    
    public String getOwnerName() {
        return portId.getCurrentOwner();
    }

    public void open(String logicalName, String portName, int timeout,
        SerialPortEventListener listener) throws JposException, PortInUseException {
        portList = CommPortIdentifier.getPortIdentifiers();
         //test if the aimed port exists
        boolean yolicaFlag = false;
        while (portList.hasMoreElements()) {
            portId = (CommPortIdentifier)portList.nextElement();
            //System.out.println(portId.getName()+portId.getCurrentOwner());
            if ((portId.getPortType() == CommPortIdentifier.PORT_SERIAL)&&
                (portId.getName().equals(portName))) {
                    yolicaFlag = true;
                    break;
            }
        }
        if (!yolicaFlag)
            throw new JposException(JposConst.JPOS_E_ILLEGAL, "Appointed serial port is not available!" + portName );
			//System.out.println("1"+ portId.getName() + "serialPort="  + serialPort + "portId.getCurrentOwner()=" + portId.getCurrentOwner());
            serialPort = (SerialPort)portId.open(logicalName, timeout);
			//System.out.println("2"+ portId.getName()+ "portId.getCurrentOwner()=" + portId.getCurrentOwner() + "|" +   serialPort);
        try {
        //get the output handler from which you can output a byte
            outputStream = serialPort.getOutputStream();
            inputStream = serialPort.getInputStream();
			dataOutput = new OutputStreamWriter(outputStream, "GBK");
			dataInput  = new InputStreamReader(inputStream, "GBK");
            //System.out.println(dataOutput.getEncoding());
       //get the input handler from which you can get status
        } catch (IOException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, this.toString(), e);
        }
        //set the port parameter, this can get from the entry properties
        try {
             serialPort.setSerialPortParams(9600,
                 SerialPort.DATABITS_8,
                 SerialPort.STOPBITS_1,
                 SerialPort.PARITY_NONE);
        } catch (UnsupportedCommOperationException e) {
               throw new JposException(JposConst.JPOS_E_ILLEGAL, this.toString(), e);
        }
       //add SerialEventListener, SerialPort.open() extends the addEventListener
        try {
            serialPort.addEventListener(listener);
            this.currentListener = listener;
        } catch (TooManyListenersException e) {
            throw new JposException(JposConst.JPOS_E_ILLEGAL, this.toString(), e);
		}
		  //System.out.println("3"+ serialPort);
			serialPort.notifyOnDataAvailable(true);
            serialPort.notifyOnOutputEmpty(true);
            serialPort.notifyOnBreakInterrupt(true);
            serialPort.notifyOnCarrierDetect(true);
            serialPort.notifyOnCTS(true);
            serialPort.notifyOnDSR(true);
            serialPort.notifyOnFramingError(true);
            serialPort.notifyOnOverrunError(true);
            serialPort.notifyOnParityError(true);
			serialPort.notifyOnRingIndicator(true);
			  //System.out.println("3"+ serialPort);
   }

    synchronized public boolean isSerialPortOpened () {
        if (serialPort==null)
            return false;
        else
            return true;
    }

    public void close() throws JposException  {
        outputStream = null;
        inputStream = null;
        serialPort.close();
        serialPort = null;
        portId = null;
        portList = null;
      /*serialPort.notifyOnDataAvailable(false);
        serialPort.notifyOnOutputEmpty(false);
        serialPort.notifyOnBreakInterrupt(false);
        serialPort.notifyOnCarrierDetect(false);
        serialPort.notifyOnCTS(false);
        serialPort.notifyOnDSR(false);
        serialPort.notifyOnFramingError(false);
        serialPort.notifyOnOverrunError(false);
        serialPort.notifyOnParityError(false);
        serialPort.notifyOnRingIndicator(false);*/
    }

}

